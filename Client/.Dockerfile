FROM debian:stable-slim AS mpuk
RUN addgroup --gid 2000 mpukgame && \
    useradd -g 2000 -u 2000 -ms /bin/sh mpukgame && \
    apt upgrade && \
    apt update && \
    apt install -y ca-certificates 

USER mpukgame

FROM mpuk AS server

WORKDIR /server
COPY --chown=mpukgame . .

# RUN chmod +x binary
# ENTRYPOINT [ "./binary" ]



