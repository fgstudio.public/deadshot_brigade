namespace Frostgate.RiftHunters
{
    public static class ComponentMenus
    {
        public const string Menu = "Frostgate";

        public static class RiftHunters
        {
            public const string Menu = ComponentMenus.Menu + "/" + "Rift Hunters";

            public static class UI { public const string Menu = RiftHunters.Menu + "/" + nameof(UI); }
        }
    }
}