﻿using Zenject;
using UnityEngine;
using Frostgate.RiftHunters.Meta;

namespace Frostgate.RiftHunters
{
    [DisallowMultipleComponent]
    [AddComponentMenu(ComponentMenus.RiftHunters.Menu + "/" + nameof(CommonInstaller))]
    public sealed class CommonInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<GearScoreProvider>().AsSingle();
            Container.Bind<UnitPropertiesProviderFactory>().AsSingle();
        }
    }
}