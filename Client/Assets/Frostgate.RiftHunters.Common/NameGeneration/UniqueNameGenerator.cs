﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Random = UnityEngine.Random;

namespace Frostgate.RiftHunters
{
    public sealed class UniqueNameGenerator : INameGenerator
    {
        private readonly bool _isConfigurationValid;

        private readonly List<IInternalNameGenerator> _readyGenerators;
        private readonly List<IInternalNameGenerator> _unreadyGenerators;

        public UniqueNameGenerator([NotNull] INamesRoster namesRoster)
        {
            _isConfigurationValid = IsRosterValid(namesRoster);
            if (_isConfigurationValid)
            {
                _readyGenerators = CreateGenerators(namesRoster);
                _unreadyGenerators = new List<IInternalNameGenerator>(_readyGenerators.Count);
            }
        }

        public string Generate() => _isConfigurationValid ? GenerateInternal() : Guid.NewGuid().ToString();

        private string GenerateInternal()
        {
            if (_readyGenerators.Count == 0)
                Reset();

            int rndIdx = Random.Range(0, _readyGenerators.Count);
            IInternalNameGenerator gen = _readyGenerators[rndIdx];

            if (!gen.TryGenerate(out string name))
            {
                _readyGenerators.Remove(gen);
                _unreadyGenerators.Add(gen);

                return GenerateInternal();
            }

            return name;
        }

        private List<IInternalNameGenerator> CreateGenerators(INamesRoster roster)
        {
            return new List<IInternalNameGenerator>
            {
                new SingleNameGenerator(roster.PredefinedNamesSet),
                new SingleNameGenerator(roster.FirstNamesSet),
                new CompositeNameGenerator(new SingleNameGenerator(roster.FirstNamesSet),
                    new SingleNameGenerator(roster.SecondNamesSet))
            };
        }

        private void Reset()
        {
            _unreadyGenerators.ForEach(g =>
            {
                g.Reset();
                _readyGenerators.Add(g);
            });

            _unreadyGenerators.Clear();
        }

        private bool IsRosterValid(INamesRoster roster) => roster.PredefinedNamesSet.Count > 0 ||
                                                           roster.FirstNamesSet.Count > 0 ||
                                                           roster.SecondNamesSet.Count > 0;

        private interface IInternalNameGenerator
        {
            bool TryGenerate(out string name);
            void Reset();
        }

        private sealed class SingleNameGenerator : IInternalNameGenerator
        {
            [NotNull] private readonly ICollection<string> _namesSet;
            private List<string> _unusedNames;

            public SingleNameGenerator([NotNull] ICollection<string> namesSet)
            {
                _namesSet = namesSet;
                Reset();
            }

            public bool TryGenerate(out string name)
            {
                name = null;

                if (_unusedNames.Count == 0)
                    return false;

                int rndIdx = Random.Range(0, _unusedNames.Count);
                name = _unusedNames[rndIdx];
                _unusedNames.Remove(name);

                return true;
            }

            public void Reset()
            {
                _unusedNames = new List<string>(_namesSet);
            }
        }

        private sealed class CompositeNameGenerator : IInternalNameGenerator
        {
            [NotNull] private readonly IInternalNameGenerator _generator1;
            [NotNull] private readonly IInternalNameGenerator _generator2;


            public CompositeNameGenerator([NotNull] IInternalNameGenerator generator1,
                [NotNull] IInternalNameGenerator generator2)
            {
                _generator1 = generator1;
                _generator2 = generator2;
            }

            public bool TryGenerate(out string name)
            {
                name = null;

                if (!_generator1.TryGenerate(out string name1))
                {
                    _generator1.Reset();
                    _generator1.TryGenerate(out name1);
                }

                if (!_generator2.TryGenerate(out string name2))
                    return false;

                name = name1 + name2;

                return true;
            }

            public void Reset()
            {
                _generator1.Reset();
                _generator2.Reset();
            }
        }
    }
}