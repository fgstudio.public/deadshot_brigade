﻿namespace Frostgate.RiftHunters
{
    public interface INameGenerator
    {
        string Generate();
    }
}