﻿using System.Collections.Generic;

namespace Frostgate.RiftHunters
{
    public interface INamesRoster
    {
        ICollection<string> PredefinedNamesSet { get; }
        ICollection<string> FirstNamesSet { get; }
        ICollection<string> SecondNamesSet { get; }
    }
}