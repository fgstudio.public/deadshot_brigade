namespace Frostgate.RiftHunters
{
    public static class AssetMenus
    {
        public const string Menu = "Frostgate";

        public static class RiftHunters
        {
            public const string Menu = ComponentMenus.Menu + "/" + "Rift Hunters";

            public static class Scene { public const string Menu = RiftHunters.Menu + "/" + nameof(Scene); }
        }
    }
}