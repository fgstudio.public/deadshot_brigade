using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Frostgate.RiftHunters.Editor.Tools
{
    public class DeleteMissingBehaviours : EditorWindow
    {
        [MenuItem("Tools/Frostgate/Delete Missing Scripts (in Active Scene)")]
        public static void Remove()
        {
            var objs = FindObjectsOfType<GameObject>();
            int count = objs.Sum(GameObjectUtility.RemoveMonoBehavioursWithMissingScript);
            Debug.Log($"Removed {count} missing scripts");
        }
    }
}
