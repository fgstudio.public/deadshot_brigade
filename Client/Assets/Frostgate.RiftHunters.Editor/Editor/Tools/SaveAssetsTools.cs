using System;
using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Editor.Tools
{
    public static class SaveAssetsTools
    {
        // [MenuItem("Frostgate/Configs/Save All Definitions")]
        // public static void SaveAll() =>
        //     ReSaveAssets(nameof(SaveAll),
        //         () => AssetsLoader.Resources.LoadAll<ScriptableObject>(Path.Definitions));

        [MenuItem("Tools/Frostgate/Save Selected Assets")]
        public static void SaveSelected() =>
            ReSaveAssets(nameof(SaveSelected),
                () => Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets));

        [MenuItem("Assets/🖫 Save this asset")]
        public static void SaveThis() => SaveSelected();

        private static void ReSaveAssets<T>(string tag, Func<IEnumerable<T>> getAssets) where T : Object
        {
            // Получение списка файлов ассетов может быть долгой операцией с фризом UI, так что покажем прогресс-бар сразу
            EditorUtility.DisplayProgressBar(nameof(ReSaveAssets), "scan files...", 0);
            var assets = getAssets().ToArray();
            Debug.Log($"[{nameof(SaveAssetsTools)}] {tag}: {assets.Length}");

            // Пометим ассеты как изменненые, чтобы функция SaveAssets перезаписала эти файлы
            AssetDatabase.StartAssetEditing();
            for (int i = 0; i < assets.Length; i++)
            {
                var asset = assets[i];

                if (asset is ISavableParametersConfig savableParametersConfig)
                    savableParametersConfig.SaveParameters();

                EditorUtility.DisplayProgressBar($"{nameof(ReSaveAssets)}", $"Saving {asset.name}...", (float)i / assets.Length);
                EditorUtility.SetDirty(asset);
            }
            AssetDatabase.StopAssetEditing();

            // Сохранение файлов ассетов это долгая операция с фризом UI, покажем соответствующий прогресс-бар
            EditorUtility.DisplayProgressBar(nameof(ReSaveAssets), "save files...", 0);
            AssetDatabase.ForceReserializeAssets(assets.Select(AssetDatabase.GetAssetPath));
            AssetDatabase.SaveAssets();

            // Очистим прогерсс-бар после завершения всех операций
            EditorUtility.ClearProgressBar();
        }
    }
}