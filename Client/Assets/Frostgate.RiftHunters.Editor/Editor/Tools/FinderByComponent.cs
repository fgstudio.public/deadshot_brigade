using System;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Editor.Tools
{
    public class FinderByComponent : EditorWindow
    {
        private static FinderByComponent _window;
        private static List<GameObject> _sceneGameObjects;
        private static Vector2 _scrollValue = Vector2.zero;
        private static List<string> _assembliesNames;
        private static string _queryInput;

        private void OnGUI()
        {
            _queryInput = EditorGUILayout.TextField("Component Name:",_queryInput);
            if (GUILayout.Button(new GUIContent("Search",
                    "Find all GameObjects in the scene with the specified UnityEngine component.")))
            {
                var type = FindComponentType(_queryInput);

                if (type != null)
                    SelectComponent(type);
            }

            _scrollValue = EditorGUILayout.BeginScrollView(_scrollValue);

            var grey = new GUIStyle(EditorStyles.label);
            grey.normal.textColor = Color.gray;

            if(_sceneGameObjects != null)
                foreach (var obj in _sceneGameObjects)
                    if (GUILayout.Button(obj.name, grey))
                    {
                        Selection.activeObject = obj;
                        EditorGUIUtility.PingObject(obj);
                    }

            EditorGUILayout.EndScrollView();
        }

        [MenuItem("Tools/Frostgate/Finder By Component")]
        private static void OpenScriptFinder()
        {
            _window = (FinderByComponent)GetWindow(typeof(FinderByComponent));
            _window.titleContent.text = nameof(FinderByComponent);
            _assembliesNames = GetAssembliesNames();
        }

        private static void SelectComponent(Type type)
        {
            _sceneGameObjects = new List<GameObject>();

            var foundComponents = FindObjectsOfType(type);
            foreach (var obj in foundComponents)
                _sceneGameObjects.Add(obj.GameObject());

            Selection.objects = _sceneGameObjects.OfType<Object>().ToArray();
        }

        private static List<string> GetAssembliesNames()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            List<string> assembliesNames = new List<string>();
            foreach (var assembly in assemblies)
            {
                var s = assembly.FullName.Split(",");
                assembliesNames.Add(s.First());
            }

            return assembliesNames;
        }

        private static Type FindComponentType(string query)
        {
            if (string.IsNullOrEmpty(query))
                return null;

            var componentName = query.Replace(" ", string.Empty);

            return _assembliesNames.Select(assemblyName =>
                Type.GetType($"UnityEngine.{componentName}, {assemblyName}")).FirstOrDefault(type => type != null);
        }
    }
}
