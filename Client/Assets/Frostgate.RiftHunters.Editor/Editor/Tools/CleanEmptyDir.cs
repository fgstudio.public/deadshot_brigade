using System;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;


namespace Frostgate.RiftHunters.Editor.Tools
{
    public class CleanEmptyDir : AssetModificationProcessor
    {
        private static string[] OnWillSaveAssets(string[] paths)
        {
            // TryDelete();
            return paths;
        }

        [MenuItem("Tools/Frostgate/♻ Clean Empty Dir")]
        private static void Perform() =>
            TryDelete();

        private static void TryDelete()
        {
            var counter = 0;
            EditorUtility.DisplayProgressBar(nameof(CleanEmptyDir), "clean...", 0);

            try
            {
                DeleteIfEmpty(new DirectoryInfo(Application.dataPath), ref counter);
                Debug.Log($"{nameof(CleanEmptyDir)}: {counter}");
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
            }

            EditorUtility.ClearProgressBar();
        }

        private static void DeleteIfEmpty(DirectoryInfo di, ref int counter)
        {
            foreach (var d in di.GetDirectories())
                DeleteIfEmpty(d, ref counter);

            if (!di.GetFileSystemInfos().Any())
            {
                // Если удалять файлы самому, windows удаляет их асинхронно и случаются ошибки
                //di.Delete();
                //var fi = new FileInfo($"{di.FullName}.meta");
                //fi.Delete();

                // Если удалять файлы через Unity, то нет возможности многопоточной оптимизации
                // Сканирование и удаление файлов это долго, поэтому отключаю пока удаление при сохранении
                var asset = di.FullName.Remove(0, length);
                AssetDatabase.MoveAssetToTrash(asset);
                Debug.Log($"[{nameof(CleanEmptyDir)}] {asset}");
                counter++;
            }
        }

        private static readonly int length = Environment.CurrentDirectory.Length + 1;
    }
}