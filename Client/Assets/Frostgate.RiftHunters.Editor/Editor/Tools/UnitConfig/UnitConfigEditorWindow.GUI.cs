using System.Linq;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Editor.Tools.Unit;
using UnityEditor;
using UnityEngine;

public partial class UnitConfigEditorWindow
{
    private bool _volumetricMode;

    private void OnGUI()
    {
        var activeObject = Selection.activeObject;
        if (activeObject is UnitConfig unitConfig)
            CurrentUnitConfig = unitConfig;

        if (activeObject is RangeWeaponConfig rangeWeaponConfig)
            _currentRangeWeaponConfig = rangeWeaponConfig;


        if (CurrentUnitConfig == null)
        {
            CurrentUnitConfig = AllUnits.FirstOrDefault();
            return;
        }

        const string helpMessage = "Утилита отображает на сцене область стрельбы выбранного юнита и оружия. " +
                                   "Можно выбирать юнитов и оружие в проекте вручную.\n" +
                                   "Можно настраивать габариты области стрельбы бегунками на сцене, или крутить в конфиге.\n" +
                                   "Область стрельбы отрисовывается приблизительно.";

        EditorGUILayout.HelpBox(helpMessage, MessageType.Info);

        SelectUnitGUI();

        const float spaceSize = 30;
        GUILayout.Space(spaceSize);

        ObjectGUI(CurrentUnitConfig);
        ObjectGUI(_currentRangeWeaponConfig);

        _volumetricMode = EditorGUILayout.Toggle("Volumetric Mode", _volumetricMode);
    }

    private void ObjectGUI(Object currentObject)
    {
        if (currentObject != null)
        {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField($"{currentObject.GetType().Name} -> {currentObject.name}");

            if (Selection.activeObject != currentObject && GUILayout.Button("Show In Inspector"))
                Selection.SetActiveObjectWithContext(currentObject, this);

            EditorGUILayout.EndHorizontal();
        }
    }

    private void SelectUnitGUI()
    {
        void ChooseNextUnit(int delta)
        {
            var index = AllUnits.IndexOf(CurrentUnitConfig);
            index = Mathf.Clamp(index + delta, 0, AllUnits.Count);
            Selection.activeObject = AllUnits[index];
        }

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Previous Unit"))
            ChooseNextUnit(-1);

        if (GUILayout.Button("Next Unit"))
            ChooseNextUnit(1);

        EditorGUILayout.EndHorizontal();
    }

    private void OnSceneGUI(SceneView sceneView)
    {
        var aimPoint = _currentUnitConfig != null
            ? _currentUnitConfig.ViewConfig.ShootPoint.localPosition
            : Vector3.zero;

        _currentRangeWeaponConfig.DrawHandles(aimPoint, _volumetricMode);
    }
}