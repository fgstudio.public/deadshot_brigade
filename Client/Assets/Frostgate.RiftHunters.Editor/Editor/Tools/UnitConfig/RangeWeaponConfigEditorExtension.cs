using Frostgate.RiftHunters.Core.Battle;
using UnityEditor;
using UnityEngine;

namespace Frostgate.RiftHunters.Editor.Tools.Unit
{
    public static class RangeWeaponConfigEditorExtension
    {
        private static readonly Vector3[] ShootingAreaMesh = new Vector3[4];
        private static readonly Color ShootingAreaRedColor = new(1, 0, 0, 0.25f);
        private static readonly Color ShootingAreaBlueColor = new(0, 0, 1, 0.25f);

        private static readonly GUIStyle LabelStyle = new()
        {
            normal = new GUIStyleState {textColor = Color.cyan}
        };

        private static int _currentVertexIndex;

        public static void DrawHandles(this RangeWeaponConfig rangeWeaponConfig, Vector3 aimPoint, bool volumetricMode)
        {
            if (rangeWeaponConfig == null)
                return;

            rangeWeaponConfig.DrawShootingArea(aimPoint, volumetricMode);
            const float maxValue = 100;

            var originHalfSize = rangeWeaponConfig.AttackOriginHalfSize;
            var minDistance = 0;
            var maxDistance = rangeWeaponConfig.MaxDistance;
            var attackAngle = rangeWeaponConfig.AttackAngle;
            var attackVerticalAngle = rangeWeaponConfig.AttackVerticalAngle;

            float AngleToScale(float angle) =>
                Mathf.Sqrt(angle);

            float ScaleToAngle(float scale) =>
                scale * scale;

            float ScaleToHandleSize(float scale) =>
                Mathf.Clamp(scale, 1, maxValue);

            float ScaleSlider(string name, float value, Vector3 offset, Vector3 dir)
            {
                Handles.color = Color.magenta;

                var result = Handles.ScaleSlider(value, aimPoint + offset, dir, Quaternion.identity,
                    ScaleToHandleSize(value), 0);

                Handles.Label(aimPoint + offset + dir * value, name, LabelStyle);
                return result;
            }

            // Контроллер начального размера области атаки
            var resultOriginHalfSize = ScaleSlider(nameof(originHalfSize), originHalfSize, Vector3.zero, Vector3.right);

            // Контроллер максимальной дистанции
            var resultMaxDistance = ScaleSlider(nameof(maxDistance), maxDistance, Vector3.forward * minDistance, Vector3.forward);

            // Контроллер минимальной дистанции
            var resultMinDistance = ScaleSlider(nameof(minDistance), minDistance, Vector3.down, Vector3.forward);

            // Контроллер угла атаки в горизонтальной плоскости
            var scaleAttackAngle = AngleToScale(attackAngle);
            scaleAttackAngle = ScaleSlider(nameof(attackAngle), scaleAttackAngle, Vector3.forward * maxDistance, Vector3.right);
            var resultAttackAngle = ScaleToAngle(scaleAttackAngle);

            // Контроллер угла атаки в вертикальной плоскости
            var scaleAttackVerticalAngle = AngleToScale(attackVerticalAngle);
            scaleAttackVerticalAngle = ScaleSlider(nameof(attackVerticalAngle), scaleAttackVerticalAngle, Vector3.forward * maxDistance, Vector3.up);
            var resultAttackVerticalAngle = ScaleToAngle(scaleAttackVerticalAngle);

            rangeWeaponConfig.SetShootingArea(resultOriginHalfSize, resultMinDistance, resultMaxDistance,
                resultAttackAngle, resultAttackVerticalAngle);
        }

        /// <summary>
        /// Приблизительная отрисовка области стрельбы.
        /// Будет выглядить как пирамида с плоскими гранями,
        /// тогда как в реальности дно и начало пирамиды имеют выпуклые сферические формы
        /// </summary>
        private static void DrawShootingArea(this RangeWeaponConfig rangeWeaponConfig, Vector3 aimPoint, bool volumetricMode)
        {
            if (rangeWeaponConfig == null)
                return;

            var originHalfSize = rangeWeaponConfig.AttackOriginHalfSize;
            var minDistance = 0;
            var maxDistance = rangeWeaponConfig.MaxDistance;

            var endXOffset = originHalfSize + maxDistance * Mathf.Tan(rangeWeaponConfig.AttackAngle * Mathf.Deg2Rad);
            var endYOffset = originHalfSize + maxDistance * Mathf.Tan(rangeWeaponConfig.AttackVerticalAngle * Mathf.Deg2Rad);


            if (volumetricMode)
                DrawVolumetricShootingArea(originHalfSize, minDistance, maxDistance, endXOffset, endYOffset, aimPoint);
            else
                DrawSimpleShootingArea(originHalfSize, minDistance, maxDistance, endXOffset, endYOffset, aimPoint);
        }

        private static void DrawSimpleShootingArea(float originHalfSize, float minDistance, float maxDistance,
            float endXOffset, float endYOffset, Vector3 aimPoint)
        {
            // Отображаем горизонтальный угол стрельбы
            var beginLeftPoint =
                new Vector3(-originHalfSize, 0, minDistance) + aimPoint;
            var beginRightPoint =
                new Vector3(originHalfSize, 0, minDistance) + aimPoint;
            var endLeftPoint =
                new Vector3(-endXOffset, 0, maxDistance) + aimPoint;
            var endRightPoint =
                new Vector3(endXOffset, 0, maxDistance) + aimPoint;

            Handles.color = ShootingAreaRedColor;
            DrawPolygon(beginLeftPoint, endLeftPoint, endRightPoint, beginRightPoint);

            // Отображаем вертикальный угол стрельбы
            var beginUpperPoint =
                new Vector3(0, originHalfSize, minDistance) + aimPoint;
            var beginLowerPoint =
                new Vector3(0, -originHalfSize, minDistance) + aimPoint;
            var endUpperPoint =
                new Vector3(0, endYOffset, maxDistance) + aimPoint;
            var endLowerPoint =
                new Vector3(0, -endYOffset, maxDistance) + aimPoint;

            Handles.color = ShootingAreaBlueColor;
            DrawPolygon(beginUpperPoint, beginLowerPoint, endLowerPoint, endUpperPoint);
        }

        private static void DrawVolumetricShootingArea(float originHalfSize, float minDistance, float maxDistance,
            float endXOffset, float endYOffset, Vector3 aimPoint)
        {
            // Вычисляем восемь вершин пирамиды отображающей габариты области стрельбы
            var beginUpperLeftPoint =
                new Vector3(-originHalfSize, originHalfSize, minDistance) + aimPoint;
            var beginUpperRightPoint =
                new Vector3(originHalfSize, originHalfSize, minDistance) + aimPoint;
            var beginLowerLeftPoint =
                new Vector3(-originHalfSize, -originHalfSize, minDistance) + aimPoint;
            var beginLowerRightPoint =
                new Vector3(originHalfSize, -originHalfSize, minDistance) + aimPoint;

            var endUpperLeftPoint =
                new Vector3(-endXOffset, endYOffset, maxDistance) + aimPoint;
            var endUpperRightPoint =
                new Vector3(endXOffset, endYOffset, maxDistance) + aimPoint;
            var endLowerLeftPoint =
                new Vector3(-endXOffset, -endYOffset, maxDistance) + aimPoint;
            var endLowerRightPoint =
                new Vector3(endXOffset, -endYOffset, maxDistance) + aimPoint;

            Handles.color = ShootingAreaRedColor;

            // Рисуем начало области стрельбы
            DrawPolygon(beginUpperLeftPoint, beginUpperRightPoint, beginLowerRightPoint, beginLowerLeftPoint);

            // Рисуем конец области стрельбы
            DrawPolygon(endLowerLeftPoint, endLowerRightPoint, endUpperRightPoint, endUpperLeftPoint);

            // Рисуем четыре боковые грани пирамиды
            // Верхняя
            DrawPolygon(beginUpperRightPoint, beginUpperLeftPoint, endUpperLeftPoint, endUpperRightPoint);
            // Нижняя
            DrawPolygon(beginLowerLeftPoint, beginLowerRightPoint, endLowerRightPoint, endLowerLeftPoint);

            Handles.color = ShootingAreaBlueColor;
            // Левая
            DrawPolygon(beginUpperLeftPoint, beginLowerLeftPoint, endLowerLeftPoint, endUpperLeftPoint);
            // Правая
            DrawPolygon(beginUpperRightPoint, endUpperRightPoint, endLowerRightPoint, beginLowerRightPoint);
        }

        private static void ClearShootingAreaMesh() =>
            _currentVertexIndex = 0;

        private static void AddVertex(Vector3 vertex) =>
            ShootingAreaMesh[_currentVertexIndex++] = vertex;

        private static void DrawPolygon(Vector3 vertex0, Vector3 vertex1, Vector3 vertex2, Vector3 vertex3)
        {
            ClearShootingAreaMesh();
            AddVertex(vertex0);
            AddVertex(vertex1);
            AddVertex(vertex2);
            AddVertex(vertex3);
            Handles.DrawAAConvexPolygon(ShootingAreaMesh);
        }
    }
}