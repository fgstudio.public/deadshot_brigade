using UnityEditor;
using UnityEngine;
using JetBrains.Annotations;
using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle;

/// <summary>
/// Тулза для отображения и настройки зоны прицеливания юнита.
/// Я бы предложил через это окно настраивать и прочие параметры юнита и оружий,
/// которые необходимо визуально отобразить на фоне вьюхи юнита.
/// </summary>
public partial class UnitConfigEditorWindow : EditorWindow
{
    private static readonly Vector3 origin = Vector3.zero;

    private GameObject _unitEditorView;
    [CanBeNull] private UnitConfig _currentUnitConfig;
    [CanBeNull] private RangeWeaponConfig _currentRangeWeaponConfig;

    private List<UnitConfig> _allUnits;
    private List<UnitConfig> AllUnits => _allUnits ??= LoadUnits();

    private UnitConfig CurrentUnitConfig
    {
        get => _currentUnitConfig;
        set
        {
            if (Equals(_currentUnitConfig, value))
                return;

            _currentUnitConfig = value;
            _currentRangeWeaponConfig = _currentUnitConfig != null
                ? _currentUnitConfig.DefaultWeapon : null;

            if (_unitEditorView != null)
                DestroyImmediate(_unitEditorView);

            if (_currentUnitConfig != null)
                _unitEditorView = Instantiate(_currentUnitConfig.View, origin, Quaternion.identity).gameObject;
        }
    }

    [MenuItem("Tools/Frostgate/UnitConfig")]
    private static void Init()
    {
        var window = GetWindow(typeof(UnitConfigEditorWindow));
        window.Show();

        const float wight = 450;
        const float height = 200;
        window.position = new Rect(window.position.position, new Vector2(wight, height));
    }

    private void OnFocus()
    {
        SceneView.duringSceneGui -= OnSceneGUI;
        SceneView.duringSceneGui += OnSceneGUI;
    }

    private void OnDestroy()
    {
        CurrentUnitConfig = null;
        SceneView.duringSceneGui -= OnSceneGUI;
    }

    private List<UnitConfig> LoadUnits()
    {
        var guids = AssetDatabase.FindAssets($"t: {nameof(UnitConfig)}");
        var units = new List<UnitConfig>();
        foreach (var guid in guids)
        {
            var unit = AssetDatabase.LoadAssetAtPath<UnitConfig>(AssetDatabase.GUIDToAssetPath(guid));

            if (unit != null)
                units.Add(unit);
        }

        return units;
    }
}