using UnityEditor;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Components;
using Frostgate.RiftHunters.Core.Utils.Inspector;
using Frostgate.RiftHunters.Core.Battle.Client.FX;

[CustomEditor(typeof(AutoDestroyFx))]
public sealed class AutoDestroyFxEditor : Editor
{
    private void OnEnable()
    {
        DestroyComponent destroyComponent = FindDestroyComponent();
        if (destroyComponent != null && destroyComponent is IPropertyBlockable propertyBlockable)
        {
            string propertyName = nameof(destroyComponent.Policy);
            propertyBlockable.BlockProperty(propertyName);
        }
    }

    private void OnDisable()
    {
        DestroyComponent destroyComponent = FindDestroyComponent();
        if (destroyComponent != null && destroyComponent is IPropertyBlockable propertyBlockable)
        {
            string propertyName = nameof(destroyComponent.Policy);
            propertyBlockable.UnblockProperty(propertyName);
        }
    }

    [CanBeNull]
    private DestroyComponent FindDestroyComponent()
    {
        SerializedProperty serializedProperty = serializedObject.FindProperty(AutoDestroyFx.AutoDestroyComponentName);
        return (serializedProperty?.objectReferenceValue as AutoDestroyComponent)?.DestroyComponent;
    }
}
