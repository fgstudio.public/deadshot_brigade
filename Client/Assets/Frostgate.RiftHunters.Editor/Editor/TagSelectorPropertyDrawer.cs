﻿using System;
using UnityEditor;
using UnityEngine;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Editor
{
    [CustomPropertyDrawer(typeof(TagSelectorAttribute))]
    public sealed class TagSelectorPropertyDrawer : PropertyDrawer
    {
        private const string Untagged = nameof(Untagged);

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Action<Rect, SerializedProperty, GUIContent> handlingDelegate =
                property.propertyType == SerializedPropertyType.String ? HandleStringProperty : HandleNoStringProperty;

            handlingDelegate.Invoke(position, property, label);
        }

        private void HandleStringProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            if (string.IsNullOrWhiteSpace(property.stringValue))
                property.stringValue = Untagged;

            property.stringValue = EditorGUI.TagField(position, label, property.stringValue);
            EditorGUI.EndProperty();
        }

        private void HandleNoStringProperty(Rect position, SerializedProperty property, GUIContent label) =>
            EditorGUI.PropertyField(position, property, label);
    }
}
