﻿using System;
using UnityEditor;
using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Editor
{
    [HelpURL("https://www.notion.so/frostgate/59ba6d9bdc3c4504b5787e636ec174ec")]
    [CustomEditor(typeof(BotSpawnArea))]
    internal sealed class BotSpawnAreaEditor : UnityEditor.Editor
    {
        private const int DiskCount = 4;

        private BotSpawnArea _component;

        public void OnSceneGUI()
        {
            _component ??= target as BotSpawnArea;
            if (!_component)
                return;

            Setup();
            DrawFlatDiscs(_component.Center, _component.Radius);
            DrawLines(_component.Center, _component.Radius);

            if (_component.HomePositionOffset != Vector3.zero)
                DrawHomePositionHandles();
        }

        private void Setup() =>
            Handles.color = Color.blue;

        private void DrawHomePositionHandles()
        {
            Vector3 homePos = _component.Center + _component.HomePositionOffset;

            DrawLine(_component.Center, homePos);
            DrawFlatDisc(homePos, _component.Radius);
            DrawPositionHandle(homePos, _component,
                pos => _component.HomePositionOffset = pos - _component.Center);
        }

        private void DrawFlatDiscs(Vector3 center, float radius)
        {
            float delta = radius / 4;

            DrawFlatDisc(center, radius);
            float newRadius = radius - delta;
            for (int i = 0; i < DiskCount - 1; i++)
            {
                DrawFlatDisc(center, newRadius);
                newRadius -= delta;
            }
        }

        private void DrawFlatDisc(Vector3 center, float radius) =>
            Handles.DrawWireDisc(center, Vector3.up, radius);

        private void DrawLines(Vector3 center, float length)
        {
            DrawLine(center, center + Vector3.forward * length);
            DrawLine(center, center + Vector3.right * length);
            DrawLine(center, center + Vector3.back * length);
            DrawLine(center, center + Vector3.left * length);
        }

        private void DrawLine(Vector3 aPoint, Vector3 bPoint) =>
            Handles.DrawLine(aPoint, bPoint);

        private void DrawPositionHandle(
            Vector3 currentPosition, Object source, Action<Vector3> setNewPosition)
        {
            Vector3 newPosition =
                Handles.PositionHandle(currentPosition, Quaternion.identity);

            if (newPosition != currentPosition)
            {
                setNewPosition(newPosition);
                EditorUtility.SetDirty(source);
            }
        }
    }
}
