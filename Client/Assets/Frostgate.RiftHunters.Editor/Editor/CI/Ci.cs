﻿using JetBrains.Annotations;
using Frostgate.RiftHunters.Editor.CI.Core;
using Frostgate.RiftHunters.Editor.CI.Workers;

namespace Frostgate.RiftHunters.Editor.CI
{
    public static class Ci
    {
        /// <summary>
        /// Сборка игрового Unity-сервера
        /// </summary>
        [UsedImplicitly]
        public static void BuildGameServer()
        {
            BuildPipeline pipeline = CreatePipeline()
                .AddPreBuildWorker(new MaterialRemovalWorker())
                .AddPreBuildWorker(new CommonPreBuildWorker())
                .AddPreBuildWorker(new ServerPreBuildWorker())
                .AddPreBuildWorker(new SelectStagePreBuildWorker())
                .SetBuildWorker(new ServerBuildWorker())
                .AddPostBuildWorker(new CommonPostBuildWorker());

            pipeline.Execute();
        }

        /// <summary>
        /// Сборка клиента под Android
        /// </summary>
        [UsedImplicitly]
        public static void BuildGameAndroid()
        {
            var pipeline = CreatePipeline()
                .AddPreBuildWorker(new CommonPreBuildWorker())
                .AddPreBuildWorker(new AndroidPreBuildWorker())
                .AddPreBuildWorker(new SelectStagePreBuildWorker())
                .SetBuildWorker(new AndroidBuildWorker())
                .AddPostBuildWorker(new CommonPostBuildWorker());

            pipeline.Execute();
        }

        /// <summary>
        /// Сборка клиента под MacOS
        /// </summary>
        [UsedImplicitly]
        public static void BuildGameMac()
        {
            var pipeline = CreatePipeline()
                .AddPreBuildWorker(new CommonPreBuildWorker())
                .AddPreBuildWorker(new SelectStagePreBuildWorker())
                .SetBuildWorker(new MacBuildWorker())
                .AddPostBuildWorker(new CommonPostBuildWorker());

            pipeline.Execute();
        }

        /// <summary>
        /// Сборка клиента под Windows
        /// </summary>
        [UsedImplicitly]
        public static void BuildGameWindows()
        {
            var pipeline = CreatePipeline()
                .AddPreBuildWorker(new CommonPreBuildWorker())
                .AddPreBuildWorker(new SelectStagePreBuildWorker())
                .SetBuildWorker(new WindowsBuildWorker())
                .AddPostBuildWorker(new CommonPostBuildWorker());

            pipeline.Execute();
        }

        private static BuildPipeline CreatePipeline()
        {
            var buildArgsProvider = new CommandLineBuildArgsProvider();
            var confFactory = new WorkerConfigurationFactory(buildArgsProvider);
            var execOrder = new ExecutionOrder(confFactory);

            return new BuildPipeline(execOrder);
        }
    }
}