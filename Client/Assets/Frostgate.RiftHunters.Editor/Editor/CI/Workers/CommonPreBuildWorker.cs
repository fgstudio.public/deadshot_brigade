﻿using Frostgate.RiftHunters.Editor.CI.Core;
using UnityEditor;

namespace Frostgate.RiftHunters.Editor.CI.Workers
{
    public sealed class CommonPreBuildWorker : IPreBuildWorker<CommonPreBuildConfiguration>
    {
        private CommonPreBuildConfiguration _configuration;

        public WorkerResult Execute(CommonPreBuildConfiguration configuration)
        {
            _configuration = configuration;
            SetApplicationVersion();

            return new WorkerResult(true);
        }

        private void SetApplicationVersion() => PlayerSettings.bundleVersion = _configuration.Version;
    }

    public sealed class CommonPreBuildConfiguration
    {
        public readonly string Version;
    }
}