﻿using System.Collections.Generic;
using System.IO;
using Frostgate.RiftHunters.Editor.CI.Core;

namespace Frostgate.RiftHunters.Editor.CI.Workers
{
    // Удаляет папки с дебаг-символами Burst
    public sealed class CommonPostBuildWorker : IPostBuildWorker<CommonPostBuildConfiguration>
    {
        private const string SearchPattern = "*Ship*";

        private CommonPostBuildConfiguration _configuration;

        public WorkerResult Execute(CommonPostBuildConfiguration configuration)
        {
            _configuration = configuration;
            RemoveDirectories(GetDirectories());

            return new WorkerResult(true);
        }

        private void RemoveDirectories(IEnumerable<string> paths)
        {
            foreach (string path in paths)
            {
                if (Directory.Exists(path))
                    Directory.Delete(path, true);
            }
        }

        private IEnumerable<string> GetDirectories() => Directory.GetDirectories(_configuration.CleanupDir,
            SearchPattern, SearchOption.TopDirectoryOnly);
    }

    public sealed class CommonPostBuildConfiguration
    {
        public readonly string CleanupDir;
    }
}
