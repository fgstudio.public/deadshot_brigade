﻿using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Editor.CI.Core;
using UnityEditor;
using UnityEngine;

namespace Frostgate.RiftHunters.Editor.CI.Workers
{
    public sealed class MaterialRemovalWorker : IPreBuildWorker<ServerPreBuildConfiguration>
    {
        private const string MaterialsFilter = "t:Material";
        private const string MaterialsFolder = "Assets/Frostgate.RiftHunters.Core";

        public WorkerResult Execute(ServerPreBuildConfiguration configuration)
        {
            DestroyAllMaterials();

            return new WorkerResult(true);
        }

        // Для значительного ускорения сборки сервака, сносим все материалы из проекта.
        private void DestroyAllMaterials()
        {
            Debug.Log(nameof(DestroyAllMaterials));
            Debug.Log("Destroy all Materials to speed up the dedicated server build.");

            var materials = AssetDatabase.FindAssets(MaterialsFilter, new[] { MaterialsFolder });

            materials.ForEach(DestroyMaterial);
            AssetDatabase.SaveAssets();
        }

        private void DestroyMaterial(string guid)
        {
            var info = new AssetInfo
            {
                Guid = guid,
                Path = AssetDatabase.GUIDToAssetPath(guid)
            };
            info.IsDeleted = AssetDatabase.DeleteAsset(info.Path);
            
            Debug.Log(info.ToString());
        }

        private struct AssetInfo
        {
            public string Guid { get; set; }
            public string Path { get; set; }
            public bool IsDeleted { get; set; }

            public override string ToString()
            {
                return "Asset\n" +
                       $"Guid: {Guid}\n" +
                       $"Path: {Path}\n" +
                       $"IsDeleted: {IsDeleted}\n";
            }
        }
    }
}