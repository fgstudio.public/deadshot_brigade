﻿using UnityEditor;

namespace Frostgate.RiftHunters.Editor.CI.Workers
{
    public sealed class ServerBuildWorker : CommonBuildWorker<ServerBuildConfiguration>
    {
        private ServerBuildConfiguration _configuration;

        protected override BuildPlayerOptions CreateOptions()
        {
            BuildPlayerOptions options = base.CreateOptions();

            options.target = BuildTarget.StandaloneLinux64;
            options.targetGroup = BuildTargetGroup.Standalone;
            options.subtarget = (int)StandaloneBuildSubtarget.Server;

            return options;
        }
    }

    public sealed class ServerBuildConfiguration : CommonBuildConfiguration
    {
    }
}