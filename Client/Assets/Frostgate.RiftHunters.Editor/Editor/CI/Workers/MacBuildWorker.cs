using UnityEditor;

namespace Frostgate.RiftHunters.Editor.CI.Workers
{
    public sealed class MacBuildWorker : CommonBuildWorker<MacBuildConfiguration>
    {
        protected override BuildPlayerOptions CreateOptions()
        {
            BuildPlayerOptions options = base.CreateOptions();

            options.target = BuildTarget.StandaloneOSX;
            options.targetGroup = BuildTargetGroup.Standalone;
            options.subtarget = (int)StandaloneBuildSubtarget.Player;

            return options;
        }
    }

    public sealed class MacBuildConfiguration : CommonBuildConfiguration
    {
    }
}
