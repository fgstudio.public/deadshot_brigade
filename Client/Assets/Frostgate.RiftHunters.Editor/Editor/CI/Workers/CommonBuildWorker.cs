﻿using Frostgate.RiftHunters.Editor.CI.Core;
using UnityEditor;
using UnityEditor.Build.Reporting;
using BuildPipeline = UnityEditor.BuildPipeline;

namespace Frostgate.RiftHunters.Editor.CI.Workers
{
    public class CommonBuildWorker<TConfiguration> : IBuildWorker<TConfiguration>
        where TConfiguration : CommonBuildConfiguration, new()
    {
        protected TConfiguration Configuration { get; private set; }

        public WorkerResult Execute(TConfiguration configuration)
        {
            Configuration = configuration;

            BuildReport report = Build();

            var isSuccess = report.summary.result == BuildResult.Succeeded;
            return new WorkerResult(isSuccess);
        }

        protected virtual BuildPlayerOptions CreateOptions()
        {
            return new BuildPlayerOptions
            {
                locationPathName = Configuration.OutputFileName,
                scenes = EditorBuildSettingsScene.GetActiveSceneList(EditorBuildSettings.scenes)
            };
        }

        private BuildReport Build()
        {
            BuildPlayerOptions options = CreateOptions();
            return BuildPipeline.BuildPlayer(options);
        }
    }

    public abstract class CommonBuildConfiguration
    {
        public readonly string OutputFileName;
    }
}
