﻿using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Editor.CI.Core;
using Frostgate.RiftHunters.Infrastructure;
using UnityEditor;
using UnityEngine;

namespace Frostgate.RiftHunters.Editor.CI.Workers
{
    public sealed class SelectStagePreBuildWorker : IPreBuildWorker<SelectStagePreBuildConfiguration>
    {
        public WorkerResult Execute(SelectStagePreBuildConfiguration configuration)
        {
            var prefs = GetScriptableAsset<Preferences>(nameof(Preferences));

            NetworkMode mode = GetScriptableAsset<NetworkMode>(configuration.Stage);
            prefs.NetworkMode = mode;

            SaveAsset(prefs);

            return new WorkerResult(mode != null);
        }

        private TAsset GetScriptableAsset<TAsset>(string name) where TAsset : ScriptableObject
        {
            string assetGuid = AssetDatabase.FindAssets($"{name} t:ScriptableObject")[0];
            string assetPath = AssetDatabase.GUIDToAssetPath(assetGuid);

            return AssetDatabase.LoadAssetAtPath<TAsset>(assetPath);
        }

        private void SaveAsset(ScriptableObject asset)
        {
            EditorUtility.SetDirty(asset);
            AssetDatabase.SaveAssetIfDirty(asset);
        }
    }

    public sealed class SelectStagePreBuildConfiguration
    {
        public readonly string Stage;
    }
}
