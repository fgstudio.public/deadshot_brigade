using UnityEditor;

namespace Frostgate.RiftHunters.Editor.CI.Workers
{
    public sealed class WindowsBuildWorker : CommonBuildWorker<WindowsBuildConfiguration>
    {
        protected override BuildPlayerOptions CreateOptions()
        {
            BuildPlayerOptions options = base.CreateOptions();

            options.target = BuildTarget.StandaloneWindows64;
            options.targetGroup = BuildTargetGroup.Standalone;
            options.subtarget = (int)StandaloneBuildSubtarget.Player;

            return options;
        }
    }

    public sealed class WindowsBuildConfiguration : CommonBuildConfiguration
    {
    }

}
