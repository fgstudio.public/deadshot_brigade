﻿using Frostgate.RiftHunters.Editor.CI.Core;
using UnityEditor;

namespace Frostgate.RiftHunters.Editor.CI.Workers
{
    public sealed class ServerPreBuildWorker : IPreBuildWorker<ServerPreBuildConfiguration>
    {
        public WorkerResult Execute(ServerPreBuildConfiguration configuration)
        {
            SetBuildSubtarget();
            return new WorkerResult(true);
        }

        private void SetBuildSubtarget()
        {
            EditorUserBuildSettings.standaloneBuildSubtarget = StandaloneBuildSubtarget.Server;
        }
    }

    public sealed class ServerPreBuildConfiguration
    {
    }
}
