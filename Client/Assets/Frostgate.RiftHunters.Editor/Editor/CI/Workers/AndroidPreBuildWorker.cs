﻿using Frostgate.RiftHunters.Editor.CI.Core;
using UnityEditor;

namespace Frostgate.RiftHunters.Editor.CI.Workers
{
    public sealed class AndroidPreBuildWorker : IPreBuildWorker<AndroidPreBuildConfiguration>
    {
        private AndroidPreBuildConfiguration _configuration;

        public WorkerResult Execute(AndroidPreBuildConfiguration configuration)
        {
            _configuration = configuration;

            SetKeystore();
            SetBuildAppBundle();
            SetVersionCode();

            return new WorkerResult(true);
        }

        private void SetKeystore()
        {
            PlayerSettings.Android.useCustomKeystore = _configuration.UseCustomKeystore;

            if (!_configuration.UseCustomKeystore)
                return;

            PlayerSettings.Android.keystoreName = _configuration.KeystoreName;
            PlayerSettings.Android.keystorePass = _configuration.KeystorePass;
            PlayerSettings.Android.keyaliasName = _configuration.KeyaliasName;
            PlayerSettings.Android.keyaliasPass = _configuration.KeyaliasPass;
        }

        private void SetBuildAppBundle()
        {
            PlayerSettings.Android.useAPKExpansionFiles = _configuration.BuildAppBundle;
            EditorUserBuildSettings.buildAppBundle = _configuration.BuildAppBundle;
        }

        private void SetVersionCode()
        {
            PlayerSettings.Android.bundleVersionCode = _configuration.VersionCode;
        }
    }

    public sealed class AndroidPreBuildConfiguration
    {
        public readonly int VersionCode;
        public readonly bool UseCustomKeystore;
        public readonly string KeystoreName;
        public readonly string KeystorePass;
        public readonly string KeyaliasName;
        public readonly string KeyaliasPass;
        public readonly bool BuildAppBundle;
    }
}