﻿using UnityEditor;

namespace Frostgate.RiftHunters.Editor.CI.Workers
{
    public sealed class AndroidBuildWorker : CommonBuildWorker<AndroidBuildConfiguration>
    {
        protected override BuildPlayerOptions CreateOptions()
        {
            BuildPlayerOptions options = base.CreateOptions();

            options.target = BuildTarget.Android;
            options.targetGroup = BuildTargetGroup.Android;
            options.subtarget = (int)StandaloneBuildSubtarget.Player;
            options.locationPathName = Configuration.BuildAppBundle
                ? Configuration.OutputAppBundleFileName
                : Configuration.OutputFileName;

            return options;
        }
    }

    public sealed class AndroidBuildConfiguration : CommonBuildConfiguration
    {
        public readonly bool BuildAppBundle;
        public readonly string OutputAppBundleFileName;
    }
}