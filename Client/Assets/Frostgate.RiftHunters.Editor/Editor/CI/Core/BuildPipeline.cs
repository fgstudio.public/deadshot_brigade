﻿using System;
using Frostgate.RiftHunters.Core;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public sealed class BuildPipeline
    {
        private readonly IExecutionOrder _executionOrder;

        public BuildPipeline(IExecutionOrder executionOrder)
        {
            _executionOrder = executionOrder;
        }

        public BuildPipeline AddPreBuildWorker<TConfiguration>([NotNull] IPreBuildWorker<TConfiguration> preBuildWorker)
            where TConfiguration : new()
        {
            OrderWorker(preBuildWorker);
            return this;
        }

        public BuildPipeline SetBuildWorker<TConfiguration>([NotNull] IBuildWorker<TConfiguration> buildWorker)
            where TConfiguration : new()
        {
            OrderWorker(buildWorker);
            return this;
        }

        public BuildPipeline AddPostBuildWorker<TConfiguration>(
            [NotNull] IPostBuildWorker<TConfiguration> preBuildWorker)
            where TConfiguration : new()
        {
            OrderWorker(preBuildWorker);
            return this;
        }

        public void Execute() => _executionOrder.ForEach(Execute);

        private void Execute(IExecutableWorker worker)
        {
            try
            {
                WorkerResult result = worker.Execute();
                if (!result.Success)
                    throw new PipelineException(worker.ToString(), result.Message);
            }
            catch (PipelineException e)
            {
                Debug.LogException(e);
                throw;
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                throw new PipelineException(worker.ToString(), e.Message, e);
            }
        }

        private void OrderWorker<TConfiguration>(IWorker<TConfiguration> worker) where TConfiguration : new() =>
            _executionOrder.OrderWorker(worker);
    }
}