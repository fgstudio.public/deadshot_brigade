﻿namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public interface IWorkerConfigurationFactory
    {
        TConfiguration GetConfiguration<TConfiguration>() where TConfiguration : new();
    }
}
