namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public interface IBuildArgsProvider
    {
        string[] Args { get; }
    }
}
