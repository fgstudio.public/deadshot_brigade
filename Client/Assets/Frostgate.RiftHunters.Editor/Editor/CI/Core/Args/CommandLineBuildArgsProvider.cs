using System;

namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public sealed class CommandLineBuildArgsProvider : IBuildArgsProvider
    {
        public string[] Args => Environment.GetCommandLineArgs();
    }
}
