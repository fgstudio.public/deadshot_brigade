﻿namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public interface IWorker<in TConfiguration> where TConfiguration : new()
    {
        WorkerResult Execute(TConfiguration configuration);
    }
}
