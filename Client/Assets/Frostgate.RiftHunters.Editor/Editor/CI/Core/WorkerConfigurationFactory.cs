﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public sealed class WorkerConfigurationFactory : IWorkerConfigurationFactory
    {
        private readonly IReadOnlyDictionary<string, string> _optionsMap;

        public WorkerConfigurationFactory(IBuildArgsProvider buildArgsProvider)
        {
            _optionsMap = ParseEnvArgs(buildArgsProvider.Args);
        }

        public TConfiguration GetConfiguration<TConfiguration>() where TConfiguration : new()
        {
            Type cType = typeof(TConfiguration);
            var conf = new TConfiguration();

            foreach (FieldInfo fInfo in cType.GetFields())
                if (_optionsMap.TryGetValue(fInfo.Name, out string optionValue))
                    fInfo.SetValue(conf, Convert.ChangeType(optionValue, fInfo.FieldType));

            return conf;
        }

        private IReadOnlyDictionary<string, string> ParseEnvArgs(string[] commandLineArgs)
        {
            const string CommandPrefix = "--";
            const string OptionRegexPattern = @"({0}\S+ \S+)";

            string regexPattern = string.Format(OptionRegexPattern, CommandPrefix);
            string cmdlArgs = string.Join(" ", commandLineArgs);

            Dictionary<string, string> optionsMap = Regex.Split(cmdlArgs, regexPattern)
                .Where(s => !string.IsNullOrWhiteSpace(s)) // Иногда попадаются пустые экземпляры, надо фильтровать
                .Select(o => // Значение в виде --Option Value
                {
                    string[] split = o.Split(' ');
                    return new KeyValuePair<string, string>(split[0].Replace(CommandPrefix, string.Empty), split[1]);
                })
                .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            return optionsMap;
        }
    }
}
