﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public interface IExecutionOrder : IEnumerable<IExecutableWorker>
    {
        void OrderWorker<TConfiguration>([NotNull] IWorker<TConfiguration> worker) where TConfiguration : new();
    }
}
