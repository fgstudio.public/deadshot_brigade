﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public sealed class ExecutionOrder : IExecutionOrder
    {
        private readonly IWorkerConfigurationFactory _configurationFactory;
        private readonly List<IExecutableWorker> _workersOrder = new();

        public ExecutionOrder([NotNull] IWorkerConfigurationFactory configurationFactory)
        {
            _configurationFactory = configurationFactory;
        }

        public IEnumerator<IExecutableWorker> GetEnumerator() => _workersOrder.GetEnumerator();

        public void OrderWorker<TConfiguration>(IWorker<TConfiguration> worker) where TConfiguration : new()
        {
            var executableWorker =
                new ExecutableWorker<TConfiguration>(worker, _configurationFactory.GetConfiguration<TConfiguration>());

            _workersOrder.Add(executableWorker);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
