﻿namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public interface IPostBuildWorker<in TConfiguration> : IWorker<TConfiguration> where TConfiguration : new()

    {
    }
}
