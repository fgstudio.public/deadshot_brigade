﻿namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public interface IBuildWorker<in TConfiguration> : IWorker<TConfiguration> where TConfiguration : new()
    {
    }
}
