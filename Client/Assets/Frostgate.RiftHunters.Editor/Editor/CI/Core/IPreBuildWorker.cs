﻿namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public interface IPreBuildWorker<in TConfiguration> : IWorker<TConfiguration> where TConfiguration : new()
    {
    }
}
