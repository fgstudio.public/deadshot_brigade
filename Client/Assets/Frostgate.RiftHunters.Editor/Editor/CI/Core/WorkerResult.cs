﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public readonly struct WorkerResult
    {
        public readonly bool Success;
        public readonly string Message;

        public WorkerResult(bool success, [NotNull] string message)
        {
            Success = success;
            Message = message;
        }

        public WorkerResult(bool success) : this(success, string.Empty)
        {
        }
    }
}