﻿namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public interface IExecutableWorker
    {
        WorkerResult Execute();
    }
}
