﻿using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public sealed class PipelineException : Exception
    {
        private const string ExcMessage = "Worker '{0}' failed with message '{1}'.";

        public PipelineException([NotNull] string workerName, [NotNull] string message) : base(String.Format(ExcMessage,
            workerName, message))
        {
        }

        public PipelineException([NotNull] string workerName, [NotNull] string message,
            [NotNull] Exception innerException) : base(String.Format(ExcMessage, workerName, message), innerException)
        {
        }
    }
}
