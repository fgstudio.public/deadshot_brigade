﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Editor.CI.Core
{
    public sealed class ExecutableWorker<TConfiguration> : IExecutableWorker where TConfiguration : new()
    {
        [NotNull] private readonly IWorker<TConfiguration> _worker;
        private readonly TConfiguration _configuration;

        public ExecutableWorker([NotNull] IWorker<TConfiguration> worker, TConfiguration configuration)
        {
            _worker = worker;
            _configuration = configuration;
        }

        public WorkerResult Execute() => _worker.Execute(_configuration);

        public override string ToString() => _worker.GetType().Name;
    }
}