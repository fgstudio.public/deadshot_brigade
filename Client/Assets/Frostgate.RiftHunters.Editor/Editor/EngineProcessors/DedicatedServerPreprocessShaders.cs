﻿#if UNITY_SERVER

using System.Collections.Generic;
using UnityEditor.Rendering;
using JetBrains.Annotations;
using UnityEditor.Build;
using UnityEngine;

namespace Frostgate.RiftHunters.Editor.EngineProcessors
{
    [UsedImplicitly]
    public sealed class DedicatedServerPreprocessShaders : IPreprocessShaders
    {
        int IOrderedCallback.callbackOrder => -9999;

        public DedicatedServerPreprocessShaders()
        {
            Debug.Log($"{nameof(DedicatedServerPreprocessShaders)} implicitly constructed.");
        }

        void IPreprocessShaders.OnProcessShader(Shader shader, ShaderSnippetData snippet,
            IList<ShaderCompilerData> data)
        {
            Debug.Log($"Shader '{shader.name}' '{data.Count}' variants found.");
            data.Clear();
            Debug.Log($"Shader '{shader.name}' variants removed.");
        }
    }
}

#endif