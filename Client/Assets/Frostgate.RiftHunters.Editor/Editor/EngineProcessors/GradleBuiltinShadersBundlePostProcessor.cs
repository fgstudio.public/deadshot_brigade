﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Android;
using UnityEngine;

#if UNITY_ANDROID

namespace Frostgate.RiftHunters.Editor.EngineProcessors
{
    public sealed class GradleBuiltinShadersBundlePostProcessor : IPostGenerateGradleAndroidProject
    {
        public int callbackOrder => 0;

        public void OnPostGenerateGradleAndroidProject(string path)
        {
            if (!EditorUserBuildSettings.buildAppBundle || !PlayerSettings.Android.useAPKExpansionFiles)
                return;

            DeleteFiles(FindBundles(path));
        }

        private IEnumerable<string> FindBundles(string rootPath)
        {
            var dirPath = Path.Combine(rootPath, "../UnityDataAssetPack/src/main/assets/aa/Android/");
            
            return Directory.GetFiles(dirPath).Where(p => p.Contains("unitybuiltinshaders"));
        }

        private void DeleteFiles(IEnumerable<string> paths)
        {
            foreach (var path in paths)
            {
                try
                {
                    Debug.Log($"Deleting file '{path}'.");
                    File.Delete(path);
                }
                catch (Exception e)
                {
                    Debug.LogError($"Failed deleting file '{path}'.");
                    Debug.LogException(e);
                }
            }
        }
    }
}

#endif