using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Core.Client.Audio
{
    public class AudioBigWorldFxActivator : MonoBehaviour
    {
        [SerializeField] private AudioClip _clip;

        private AudioService _audioService;

        [Inject]
        private void MonoConstructor(AudioService audioService) => _audioService = audioService;

        private void OnEnable() => _audioService.BigWorldFx.Play(_clip, transform.position);
    }
}