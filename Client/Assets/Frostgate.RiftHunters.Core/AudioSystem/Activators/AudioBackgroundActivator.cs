using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Core.Client.Audio
{
    public class AudioBackgroundActivator : MonoBehaviour
    {
        [SerializeField] private AudioClip _musicClip;
        [SerializeField] private AudioClip _ambientClip;

        private AudioService _audioService;

        [Inject]
        private void MonoConstructor(AudioService audioService) => _audioService = audioService;

        private void OnEnable()
        {
            _audioService.Ambient.Play(_ambientClip);
            _audioService.Music.Play(_musicClip);
        }
    }
}