using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Frostgate.RiftHunters.Core.Client.Audio
{
    public class AudioFXOnPointerDown : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private AudioClip _clip;

        private AudioService _audioService;

        [Inject]
        private void MonoConstructor(AudioService audioService) => _audioService = audioService;

        public void OnPointerDown(PointerEventData eventData) => _audioService.Fx.Play(_clip);
    }
}