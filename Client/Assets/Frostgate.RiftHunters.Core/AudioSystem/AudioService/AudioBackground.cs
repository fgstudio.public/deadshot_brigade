using System.Collections;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Client.Audio
{
    public class AudioBackground : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;

        private const float FadeDuration = 1f;

        public void Play(AudioClip clip)
        {
            if (!_audioSource.isPlaying)
            {
                _audioSource.clip = clip;
                _audioSource.Play();
            }
            else if (clip != _audioSource.clip)
            {
                StopAllCoroutines();
                StartCoroutine(SwitchClipRoutine(clip));
            }
        }

        private IEnumerator SwitchClipRoutine(AudioClip clip)
        {
            while (_audioSource.volume > 0)
            {
                yield return null;
                var maxDelta = Time.unscaledDeltaTime / FadeDuration;
                _audioSource.volume = Mathf.MoveTowards(_audioSource.volume, 0, maxDelta);
            }

            _audioSource.volume = 1;
            _audioSource.clip = clip;
            _audioSource.Play();
        }
    }
}