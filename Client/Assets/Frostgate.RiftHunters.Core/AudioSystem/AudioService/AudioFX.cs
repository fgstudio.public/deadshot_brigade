using UnityEngine;

namespace Frostgate.RiftHunters.Core.Client.Audio
{
    public class AudioFX : AudioSourcePool
    {
        public void Play(AudioClip clip)
        {
            var source = GetNextSource();
            source.clip = clip;
            source.Play();
        }
    }
}