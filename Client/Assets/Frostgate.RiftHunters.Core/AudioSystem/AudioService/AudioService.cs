using UnityEngine;

namespace Frostgate.RiftHunters.Core.Client.Audio
{
    public class AudioService : MonoBehaviour
    {
        public AudioBackground Music => _music;
        public AudioBackground Ambient => _ambient;
        public AudioWorldFX SmallWorldFx => _smallWorldFx;
        public AudioWorldFX BigWorldFx => _bigWorldFx;
        public AudioFX Fx => _fx;

        [SerializeField] private AudioBackground _music;
        [SerializeField] private AudioBackground _ambient;
        [SerializeField] private AudioWorldFX _smallWorldFx;
        [SerializeField] private AudioWorldFX _bigWorldFx;
        [SerializeField] private AudioFX _fx;
    }
}