using UnityEngine;

namespace Frostgate.RiftHunters.Core.Client.Audio
{
    public class AudioSourcePool : MonoBehaviour
    {
        [SerializeField] private AudioSource[] _audioSources;

        private int _index;

        protected AudioSource GetNextSource()
        {
            var source = _audioSources[_index];

            _index++;

            if (_index >= _audioSources.Length)
                _index = 0;

            return source;
        }
    }
}