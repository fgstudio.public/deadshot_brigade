using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Core.Client.Audio
{
    public class AudioServiceInstaller : MonoInstaller
    {
        [SerializeField] private AudioService _audioServicePrefab;

        public override void InstallBindings()
        {
            //TODO: надо спаунить и биндить только на клиенте
            var audioService = Instantiate(_audioServicePrefab, transform);
            Container.BindInstance(audioService).AsSingle();
        }
    }
}