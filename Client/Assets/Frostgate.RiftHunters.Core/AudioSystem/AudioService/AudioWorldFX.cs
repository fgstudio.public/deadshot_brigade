using UnityEngine;

namespace Frostgate.RiftHunters.Core.Client.Audio
{
    public class AudioWorldFX : AudioSourcePool
    {
        public void Play(AudioClip clip, Vector3 position)
        {
            var source = GetNextSource();
            source.transform.position = position;
            source.clip = clip;
            source.Play();
        }
    }
}