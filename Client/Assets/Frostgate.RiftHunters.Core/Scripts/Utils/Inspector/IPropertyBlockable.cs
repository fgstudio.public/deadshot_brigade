namespace Frostgate.RiftHunters.Core.Utils.Inspector
{
    public interface IPropertyBlockable
    {
        void BlockProperty(string propertyName);
        void UnblockProperty(string propertyName);
    }
}
