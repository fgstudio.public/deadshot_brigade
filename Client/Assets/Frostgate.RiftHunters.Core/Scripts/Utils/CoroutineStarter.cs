using System;
using System.Collections;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Utils
{
    public class CoroutineStarter : MonoBehaviour
    {
        public static CoroutineStarter I { get; private set; }

        private void Awake() => I = this;

        public Coroutine Execute(IEnumerator coroutine) =>
            StartCoroutine(coroutine);

        public void Break(Coroutine coroutine) => StopCoroutine(coroutine);

        public void Invoke(Action action, float delay) =>
            Execute(Invoker(action, delay));

        private IEnumerator Invoker(Action action, float delay)
        {
            yield return new WaitForSecondsRealtime(delay);
            action?.Invoke();
        }
    }
}