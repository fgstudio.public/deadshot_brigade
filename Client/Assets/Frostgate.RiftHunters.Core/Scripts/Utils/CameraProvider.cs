using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Linq;

namespace Frostgate.RiftHunters.Core.Utils
{
    /// <summary>
    /// Исчтоник камеры. Способ получения и поиска камеры можно задавать в инскпеторе компонента.
    /// </summary>
    [AddComponentMenu(BattleComponentMenus.Shared.Utils.Menu + "/" + nameof(CameraProvider))]
    public sealed class CameraProvider : MonoBehaviour
    {
        private enum Type
        {
            Main,
            Current,
            Method,
            Inject,
            Serialize,
            Tag,
            Name
        }

        [Header("Settings")]
        [SerializeField] private Type _providingType;
        [SerializeField, ShowIf(nameof(_providingType), Type.Serialize)] private Camera _serializeCamera;
        [SerializeField, ShowIf(nameof(_providingType), Type.Tag)] private string _cameraObjectTag;
        [SerializeField, ShowIf(nameof(_providingType), Type.Name)] private string _cameraObjectName;

        private Camera _camera;
        [ShowInInspector, CanBeNull] public Camera Camera => _camera != null ? _camera : _camera = FindCamera();

        /// <summary>
        /// For Inject-Type only.
        /// </summary>
        private void MonoConstructor([InjectOptional] Camera camera)
        {
            if (_providingType == Type.Inject)
                _camera = camera;
        }

        /// <summary>
        /// For Method-Type only.
        /// </summary>
        public void SetCamera(Camera camera)
        {
            if (_providingType == Type.Method)
                _camera = camera;
        }

        private Camera FindCamera() =>
            _providingType switch
            {
                Type.Main => Camera.main,
                Type.Current => Camera.current,
                Type.Method => _camera,
                Type.Inject => _camera,
                Type.Serialize => _serializeCamera,
                Type.Name => Camera.allCameras.SingleOrDefault(c => c != null && c.name == _cameraObjectName),
                Type.Tag => Camera.allCameras.SingleOrDefault(c => c != null && c.CompareTag(_cameraObjectTag)),
                _ => null
            };
    }
}