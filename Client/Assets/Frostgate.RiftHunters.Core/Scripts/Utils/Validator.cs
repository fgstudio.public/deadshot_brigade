using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Utils
{
    public static class Validator
    {
        public static bool HasDuplicateElements<TElement>([NotNull] IEnumerable<TElement> elements) =>
            HasDuplicateElements(elements.ToArray());

        public static bool HasDuplicateElements<TElement>([NotNull] TElement[] elements) =>
            elements.Length != elements.Distinct().Count();

        public static bool HasNullElements<TElement>([NotNull] IEnumerable<TElement> elements) =>
            elements.Any(e => e == null);
    }
}
