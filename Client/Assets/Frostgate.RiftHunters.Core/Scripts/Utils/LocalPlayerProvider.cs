using System;
using Mirror;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Utils
{
    public static class LocalPlayerProvider
    {
        public static NetworkIdentity LocalPlayer => NetworkClient.localPlayer;
        public static bool IsInitialized => LocalPlayer != null;

        private static List<Action> _listeners;
        private static Task _task;


        public static void AddInitializationListener(Action listener)
        {
            if (!IsInitialized)
            {
                AddListener(listener);
                _task ??= NotifyListenersAsync();
            }
        }

        public static void RemoveInitializationListener(Action listener)
        {
            if (_listeners != null)
                _listeners.Remove(listener);
        }

        private static void AddListener(Action action)
        {
            _listeners ??= new List<Action>();
            _listeners.Add(action);
        }

        private static async Task NotifyListenersAsync()
        {
            await WaitInitializationTask();
            NotifyListeners();
            ClearListeners();
            ClearTask();
        }

        private static async Task WaitInitializationTask()
        {
            while (!IsInitialized)
                await Task.Yield();
        }

        private static void NotifyListeners()
        {
            foreach (Action listener in _listeners)
                listener?.Invoke();
        }

        private static void ClearListeners() =>
            _listeners.Clear();

        private static void ClearTask() =>
            _task = null;
    }
}
