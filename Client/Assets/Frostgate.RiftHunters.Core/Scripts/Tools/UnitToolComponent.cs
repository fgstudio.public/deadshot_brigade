using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Tools
{
    [RequireComponent(typeof(ImpactAreaDrawer))]
    [AddComponentMenu(BattleComponentMenus.Shared.Tools.Menu + "/" + nameof(UnitToolComponent))]
    public sealed class UnitToolComponent : MonoBehaviour
    {
        [InfoBox("Компонент, который самостоятельно подставляет в " +
                 nameof(ImpactAreaDrawer) + " актуальный конфиг абилити.\n" +
                 "Конфиг можно изменит вручную на любой другой.\n" +
                 "Работает только в SceneView и только в редакторе. " +
                 "На устройстве весь объект выключается.")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private ImpactAreaDrawer _impactAreaDrawer;

        [Required]
        [SerializeField] private UnitNetworkState _unitNetworkState;

#if !UNITY_EDITOR
        private void OnEnable() => gameObject.SetActive(false);
#else
        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _unitNetworkState ??= GetComponentInParent<UnitNetworkState>();
            _impactAreaDrawer ??= GetComponent<ImpactAreaDrawer>();
        }

        private void Start()
        {
            UpdateImpactConfig();
            _unitNetworkState.UnitConfigChanged += OnUnitConfigChanged;
        }

        private void OnDestroy() =>
            _unitNetworkState.UnitConfigChanged -= OnUnitConfigChanged;

        private void OnUnitConfigChanged(UnitConfig arg1, UnitConfig arg2) =>
            UpdateImpactConfig();

        private void UpdateImpactConfig() =>
            _impactAreaDrawer.ImpactConfig = _unitNetworkState.PropertiesProvider?.Impacts.Ability.Config;
#endif
    }
}