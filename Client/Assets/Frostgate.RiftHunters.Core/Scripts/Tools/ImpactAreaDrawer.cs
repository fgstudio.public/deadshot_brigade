using System;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using static Frostgate.RiftHunters.Core.Battle.Impact.Effect.EffectTargetDetectionData;

namespace Frostgate.RiftHunters.Core.Tools
{
    [AddComponentMenu(BattleComponentMenus.Shared.Tools.Menu +
                      "/" + nameof(ImpactAreaDrawer))]
    public sealed class ImpactAreaDrawer : MonoBehaviour
    {
        private static readonly Color defaultColor = new(0, 1, 0, 0.2f);

        [InfoBox("Отрисовывает области действия " + nameof(EffectTargetDetectionData) +
                 " импакта в " + nameof(ImpactConfig) +
                 " относительно позиции данного объекта.")]

        [AssetsOnly, AssetSelector]
        public ImpactConfig ImpactConfig;

        [Space(-10), Title("")]
        [InfoBox("Цвета используются в порядке следования для нескольких областей " +
                 "внутри " + nameof(EffectData))]

        [SerializeField] private Color[] _colors = new[]
        {
            new Color(0, 1, 1, 0.2f),
            new Color(0, 0, 1, 0.2f),
            new Color(0, 1, 0, 0.2f),
            new Color(1, 0, 1, 0.2f),
            new Color(1, 0, 0, 0.2f),
        };

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (ImpactConfig == null)
                return;

            for (int i = 0; i < ImpactConfig.Effects.Count; i++)
            {
                EffectData effectData = ImpactConfig.Effects[i];
                if (effectData == null) continue;

                EffectTargetData targetData = effectData.TargetData;
                EffectTargetDetectionData detectionData = targetData.DetectionData;

                Color color = _colors.Length > 1 ? _colors[i % _colors.Length] : defaultColor;

                if (targetData.Type == EffectTargetData.TargetType.NoAim)
                {
                    Transform t = transform;
                    float detectionRotation = t.eulerAngles.y;

                    EffectAffectAreaData affectAreaData = effectData.AffectAreaData;
                    Vector3 offset = affectAreaData.Offset.RotateY(detectionRotation);
                    Vector3 detectionPosition = t.position + offset;

                    Draw(affectAreaData, detectionPosition, detectionRotation, color);
                }
                else if (detectionData?.Type == DetectionType.AutoAimInPoint)
                {
                    Transform t = transform;
                    float detectionRotation = t.eulerAngles.y;

                    Vector3 offset = detectionData.Offset.RotateY(detectionRotation);
                    Vector3 detectionPosition = t.position + offset;

                    EffectAffectAreaData affectAreaData = effectData.AffectAreaData;
                    Draw(affectAreaData, detectionPosition, detectionRotation, color);
                }
                else
                {
                    Draw(detectionData, color);
                }
            }
        }

        private void Draw(EffectTargetDetectionData data, Color color)
        {
            Transform t = transform;
            float selfRot = t.eulerAngles.y;

            float radius = data.Radius;
            Vector3 offset = data.Offset.RotateY(selfRot);
            Vector3 position = t.position + offset;

            UnityEditor.Handles.color = color;

            switch (data.Type)
            {
                case DetectionType.AutoAimInRadius:
                    UnityEditor.Handles.DrawSolidDisc(position, Vector3.up, radius);
                    break;

                case DetectionType.AutoAimInSector:
                    float angleWidth = data.AngleWidth;
                    Vector3 direction = Vector3.forward
                        .RotateY(selfRot)
                        .RotateY(-angleWidth / 2f)
                        .RotateY(data.Rotation);

                    UnityEditor.Handles.DrawSolidArc(position, Vector3.up, direction, angleWidth, radius);
                    break;

                case DetectionType.AutoAimInPoint:
                    UnityEditor.Handles.DrawSolidDisc(position, Vector3.up, 0.2f);
                    break;

                case DetectionType.AutoAimInRect:
                    var rect = new Rect(position.CutY(), data.Size);
                    UnityEditor.Handles.DrawSolidRectangleWithOutline(rect, color, Color.clear);
                    break;

                default: throw new ArgumentOutOfRangeException();
            }
        }

        private void Draw(EffectAffectAreaData data, Vector3 detectionPosition,
            float detectionRotation, Color color)
        {
            float radius = data.Radius;
            Vector3 offset = data.Offset.RotateY(detectionRotation);
            Vector3 position = detectionPosition + offset;

            UnityEditor.Handles.color = color;

            switch (data.Type)
            {
                case EffectAffectAreaData.AreaType.Point:
                    UnityEditor.Handles.DrawSolidDisc(position, Vector3.up, 0.2f);
                    break;

                case EffectAffectAreaData.AreaType.Sector:
                    float angleWidth = data.AngleWidth;
                    Vector3 direction = Vector3.forward
                        .RotateY(detectionRotation)
                        .RotateY(-angleWidth / 2f)
                        .RotateY(data.Rotation);

                    UnityEditor.Handles.DrawSolidArc(position, Vector3.up, direction, angleWidth, radius);
                    break;

                case EffectAffectAreaData.AreaType.Radius:
                    UnityEditor.Handles.DrawSolidDisc(position, Vector3.up, radius);
                    break;

                case EffectAffectAreaData.AreaType.Rect:
                    var rect = new Rect(position.CutY(), data.Size);
                    UnityEditor.Handles.DrawSolidRectangleWithOutline(rect, color, Color.clear);
                    break;

                default: throw new ArgumentOutOfRangeException();
            }
        }
#endif
    }
}