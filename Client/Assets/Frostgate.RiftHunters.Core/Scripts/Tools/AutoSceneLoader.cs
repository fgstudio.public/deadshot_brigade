﻿using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Frostgate.RiftHunters.Core.Tools
{
    [ExecuteInEditMode]
    [AddComponentMenu(BattleComponentMenus.Shared.Tools.Menu + "/" + nameof(AutoSceneLoader))]
    public sealed class AutoSceneLoader : MonoBehaviour
    {
        [SerializeField] private string _sceneName;
        [SerializeField] private bool _deleteInRuntime = true;

        private void Start()
        {
            if (!Application.isPlaying)
                LoadScene(_sceneName);

            else if (_deleteInRuntime)
                Destroy(gameObject);
        }

        private void LoadScene(string sceneName)
        {
#if UNITY_EDITOR
            string additiveSceneGuid = UnityEditor.AssetDatabase
                .FindAssets($"t: {nameof(Scene)} {sceneName}").Single();

            string additiveScenePath = UnityEditor.AssetDatabase
                .GUIDToAssetPath(additiveSceneGuid);

            var mode = UnityEditor.SceneManagement.OpenSceneMode.Additive;
            UnityEditor.SceneManagement.EditorSceneManager.OpenScene(additiveScenePath, mode);
#else
            UnityEngine.Debug.LogWarning($"[{GetType().Name}] Runtime trying to load scene");
#endif
        }
    }
}