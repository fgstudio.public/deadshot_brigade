namespace Frostgate.RiftHunters.Core
{
    public interface IIdentifiable
    {
        string Id { get; }
    }
}
