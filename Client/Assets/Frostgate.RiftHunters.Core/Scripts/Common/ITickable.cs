using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core
{
    public interface IReadOnlyTickable
    {
        event UnityAction Ticked;
    }

    public interface ITickable : IReadOnlyTickable
    {
        void Tick();
    }

    public interface ITickable<TPayload> : IReadOnlyTickable
    {
        void Tick(TPayload payload);
    }
}
