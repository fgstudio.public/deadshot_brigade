namespace Frostgate.RiftHunters.Core
{
    public interface IResettable
    {
        void Reset();
    }
}
