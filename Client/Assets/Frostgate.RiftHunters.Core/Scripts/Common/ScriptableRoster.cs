﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public abstract class ScriptableRoster<T> : ScriptableConfig where T : ScriptableObject
    {
        [SerializeField, AssetsOnly, AssetSelector]
        private T[] _objects = Array.Empty<T>();

        public IReadOnlyList<T> Objects => _objects;
    }
}