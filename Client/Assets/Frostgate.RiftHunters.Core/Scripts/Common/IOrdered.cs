﻿namespace Frostgate.RiftHunters.Core
{
    public interface IOrdered
    {
        int Order { get; }
    }
}