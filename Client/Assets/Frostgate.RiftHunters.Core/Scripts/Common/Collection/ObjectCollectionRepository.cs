using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface IReadOnlyObjectCollectionRepository<TValue>
    {
        bool Contains<TKey>();
        IReadOnlyObjectCollection<TKey, TValue> Get<TKey>();
    }

    // TODO: добавить возомжность выполнять операции Add и Remove для всех Values
    public class ObjectCollectionRepository<TValue> : IReadOnlyObjectCollectionRepository<TValue>
    {
        private readonly Dictionary<Type, IEnumerable<TValue>> _collections = new();

        public void Add<TKey>([NotNull] IObjectCollection<TKey, TValue> collection) =>
            _collections[typeof(TKey)] = collection;

        public void Remove<TKey>() =>
            _collections.Remove(typeof(TKey));

        public bool Contains<TKey>() =>
            _collections.ContainsKey(typeof(TKey));

        public IObjectCollection<TKey, TValue> Get<TKey>() =>
            _collections[typeof(TKey)] as IObjectCollection<TKey, TValue>;

        IReadOnlyObjectCollection<TKey, TValue> IReadOnlyObjectCollectionRepository<TValue>.Get<TKey>() =>
            Get<TKey>();
    }
}
