using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface IReadOnlyObjectCollection<TKey, TValue> : IEnumerable<TValue>
    {
        event Action<TValue> Added;
        event Action<TValue> Removed;

        IEnumerable<TKey> Keys { get; }
        IEnumerable<TValue> Values { get; }
        [NotNull] TValue this[[NotNull] TKey key] { get; }

        bool Contains([NotNull] TKey key);
        bool TryGet([NotNull] TKey key, out TValue value);
        [NotNull] TValue Get([NotNull] TKey key);
    }

    public interface IObjectCollection<TKey, TValue> : IReadOnlyObjectCollection<TKey, TValue>, IDisposable
    {
        [NotNull] new TValue this[[NotNull] TKey key] { get; set; }

        void Add([NotNull] TKey key, [NotNull] TValue value);
        void Remove([NotNull] TKey key);
    }
}
