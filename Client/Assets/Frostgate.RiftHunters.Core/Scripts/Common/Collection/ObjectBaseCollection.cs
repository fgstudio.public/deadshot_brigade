using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public abstract class ObjectBaseCollection<TKey, TValue> : IObjectCollection<TKey, TValue>
    {
        public event Action<TValue> Added;
        public event Action<TValue> Removed;

        [NotNull] private readonly ILogger _logger;
        [NotNull] private readonly Dictionary<TKey, TValue> _values;

        public IEnumerable<TKey> Keys => _values.Keys;
        public IEnumerable<TValue> Values => _values.Values;

        public TValue this[TKey identity]
        {
            get => _values[identity];
            set => _values[identity] = value;
        }

        protected ObjectBaseCollection([NotNull] ILogger logger)
        {
            _logger = logger;
            _values = new Dictionary<TKey, TValue>();
        }

        public void Dispose() => _values.Clear();

        [NotNull] protected abstract string ExtractName([NotNull] TValue value);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public IEnumerator<TValue> GetEnumerator() => _values.Values.GetEnumerator();

        public bool Contains(TKey key) => _values.ContainsKey(key);

        public TValue Get(TKey key) => _values[key];
        public bool TryGet(TKey key, out TValue value) =>
            _values.TryGetValue(key, out value);

        public void Add(TKey key, TValue value)
        {
            _values[key] = value;

            string name = ExtractName(value);
            _logger.Log($"Added {name}");

            Added?.Invoke(value);
        }

        public void Remove(TKey key)
        {
            if (_values.TryGetValue(key, out TValue value))
                Remove(key, value);
        }

        private void Remove([NotNull] TKey key, [NotNull] TValue value)
        {
            _values.Remove(key);

            string name = ExtractName(value);
            _logger.Log($"Removed {name}");

            Removed?.Invoke(value);
        }
    }
}
