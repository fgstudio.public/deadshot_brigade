using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface IGameObject
    {
        [CanBeNull] GameObject GameObject { get; }
    }
}
