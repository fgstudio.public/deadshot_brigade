﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public sealed class StaticPositionProvider : IPositionProvider
    {
        public Vector3 Position { get; }

        public StaticPositionProvider(Vector3 position)
        {
            Position = position;
        }
    }
}