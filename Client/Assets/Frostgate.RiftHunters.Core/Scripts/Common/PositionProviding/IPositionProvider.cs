﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public interface IPositionProvider
    {
        Vector3 Position { get; }
    }
}