﻿using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class DynamicPositionProvider : IPositionProvider
    {
        [CanBeNull] private readonly Transform _targetTransform;

        private Vector3 _lastPositionCache;

        public Vector3 Position => _targetTransform != null
            ? _lastPositionCache = _targetTransform.position
            : _lastPositionCache;

        public DynamicPositionProvider([NotNull] Transform targetTransform)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(targetTransform, nameof(targetTransform));
            _targetTransform = targetTransform;
            _lastPositionCache = targetTransform.position;
        }
    }
}