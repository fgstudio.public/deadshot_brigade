using Mirror;

namespace Frostgate.RiftHunters.Core
{
    public interface INetworkIdentifiable
    {
        NetworkIdentity Identity { get; }
    }
}
