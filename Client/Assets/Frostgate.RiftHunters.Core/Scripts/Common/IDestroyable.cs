using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core
{
    public interface IDestroyable
    {
        UnityEvent Destroyed { get; }
        void Destroy();
    }
}
