using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class ListCache<T> : IReadOnlyList<T>
    {
        public int Count => _length;
        public T this[int index] => _values[index];

        [NotNull] private T[] _values = Array.Empty<T>();
        private int _length;

        public void Cache(IReadOnlyList<T> list)
        {
            _length = list.Count;

            if (_values.Length < list.Count)
                _values = list.ToArray();
            else
            {
                Array.Clear(_values, 0, _values.Length);
                for (int i = 0; i < list.Count; i++)
                    _values[i] = list[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public IEnumerator<T> GetEnumerator() => GetEnumerable().GetEnumerator();

        private IEnumerable<T> GetEnumerable()
        {
            for (int i = 0; i < _length; i++)
                yield return _values[i];
        }
    }
}
