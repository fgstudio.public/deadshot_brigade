﻿using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core
{
    public interface IReadOnlyBipolarDictionary<TKey, TUniqueValue>
    {
        TKey this[TUniqueValue key] { get; }
        TUniqueValue this[TKey key] { get; }

        int Count { get; }
        IEnumerable<TKey> Keys { get; }
        IEnumerable<TUniqueValue> Values { get; }

        bool ContainsKey(TKey key);
        bool ContainsValue(TUniqueValue key);

        bool TryGetKey(TUniqueValue value, out TKey key);
        bool TryGetValue(TKey key, out TUniqueValue value);
    }

    public sealed class BipolarDictionary<TKey, TUniqueValue> : IReadOnlyBipolarDictionary<TKey, TUniqueValue>
    {
        private readonly Dictionary<TKey, TUniqueValue> _byKeys = new();
        private readonly Dictionary<TUniqueValue, TKey> _byValues = new();

        public TKey this[TUniqueValue key]
        {
            get => _byValues[key];
            set
            {
                if (_byValues.TryGetValue(key, out TKey oldValue))
                    _byKeys.Remove(oldValue);

                _byValues[key] = value;
                _byKeys[value] = key;
            }
        }

        public TUniqueValue this[TKey key]
        {
            get => _byKeys[key];
            set
            {
                if (_byKeys.TryGetValue(key, out TUniqueValue oldValue))
                    _byValues.Remove(oldValue);

                _byValues[value] = key;
                _byKeys[key] = value;
            }
        }

        public int Count => _byKeys.Count;

        public IEnumerable<TKey> Keys => _byKeys.Keys;
        public IEnumerable<TUniqueValue> Values => _byKeys.Values;

        public bool ContainsKey(TKey key) => _byKeys.ContainsKey(key);
        public bool ContainsValue(TUniqueValue key) => _byValues.ContainsKey(key);

        public bool TryGetKey(TUniqueValue value, out TKey key) => _byValues.TryGetValue(value, out key);
        public bool TryGetValue(TKey key, out TUniqueValue value) => _byKeys.TryGetValue(key, out value);
    }
}