using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core
{
    public interface IUnityEventUpdateable
    {
        UnityEvent Updated { get; }
    }
}
