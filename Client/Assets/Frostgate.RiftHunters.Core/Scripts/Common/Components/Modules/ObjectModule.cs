using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Components
{
    public abstract class ObjectModule<TObject> : MonoBehaviour
        where TObject : MonoBehaviour
    {
        [SerializeField, Required] private ObjectModuleComponent<TObject>[] _components;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _components ??= GetComponents<ObjectModuleComponent<TObject>>();

        public void Init([NotNull] TObject obj)
        {
            foreach (ObjectModuleComponent<TObject> component in _components)
                component.Init(obj);
        }

        public void Deinit()
        {
            foreach (ObjectModuleComponent<TObject> component in _components)
                component.Deinit();
        }
    }
}