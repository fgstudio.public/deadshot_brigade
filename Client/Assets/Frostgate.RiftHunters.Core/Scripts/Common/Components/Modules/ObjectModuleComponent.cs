using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Components
{
    public abstract class ObjectModuleComponent<TObject> : MonoBehaviour
        where TObject : MonoBehaviour
    {
        public abstract void Init([NotNull] TObject obj);
        public abstract void Deinit();
    }
}