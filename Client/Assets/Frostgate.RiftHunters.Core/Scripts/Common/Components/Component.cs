using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core
{
    public abstract class Component : MonoBehaviour, IEnabling
    {
        private const string ComponentEventsGroup = "Component Events";

        [field: FoldoutGroup(ComponentEventsGroup), SerializeField] public UnityEvent Enabled { get; private set; }
        [field: FoldoutGroup(ComponentEventsGroup), SerializeField] public UnityEvent Disabled { get; private set; }

        [CanBeNull] private CancellationTokenSource _waitCts;

        bool IEnabling.IsEnabled => enabled;

        private void OnEnable()
        {
            OnEnabled();
            StopWaiting();
            Enabled?.Invoke();
        }

        private void OnDisable()
        {
            OnDisabled();
            StopWaiting();
            Disabled?.Invoke();
        }

        [ContextMenu(nameof(Enable))] public void Enable() => SetEnabled(true);
        [ContextMenu(nameof(Disable))] public void Disable() => SetEnabled(false);

        public UniTask EnableWithDelay(TimeSpan delay) => SetEnabledWithDelay(true, delay);
        public UniTask DisableWithDelay(TimeSpan delay) => SetEnabledWithDelay(false, delay);
        public UniTask SetEnabledWithDelay(bool enabled, TimeSpan delay)
        {
            StopWaiting();
            _waitCts = new CancellationTokenSource();

            return UniTask.Delay(delay, cancellationToken: _waitCts.Token)
                .ContinueWith(() =>
                {
                    if (this != null && gameObject != null)
                        SetEnabled(enabled);
                });
        }

        public void SetEnabled(bool enabled)
        {
            StopWaiting();

            if (this == null)
                return;

            this.enabled = enabled;

            if (!gameObject.activeInHierarchy)
                InvokeActualStateManually();
        }

        protected virtual void OnEnabled() { }
        protected virtual void OnDisabled() { }

        private void InvokeActualStateManually()
        {
            if (enabled) OnEnable();
            else OnDisable();
        }

        private void StopWaiting()
        {
            if (_waitCts != null)
            {
                _waitCts.Cancel();
                _waitCts.Dispose();
                _waitCts = null;
            }
        }
    }
}
