using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(BlinkComponent))]
    public sealed class BlinkComponent : Component
    {
        [Header("References"), Required, ChildGameObjectsOnly]
        [SerializeField] private Renderer _renderer;

        [Header("Settings")]
        [MinValue(0), SuffixLabel("times/sec", true)]
        [SerializeField] private float _frequency = 7;

        private Coroutine _routine;
        private bool _rendererEnabledCache;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _renderer ??= GetComponentInChildren<Renderer>();

        protected override void OnEnabled()
        {
#if UNITY_EDITOR
            if (gameObject.IsPrefab()) return;
#endif
            StartBlinking();
        }

        protected override void OnDisabled()
        {
#if UNITY_EDITOR
            if (gameObject.IsPrefab()) return;
#endif
            StopBlinking();
        }

        private void StartBlinking()
        {
            StopBlinking();
            _rendererEnabledCache = _renderer.enabled;
            _routine = StartCoroutine(BlinkRoutine(_frequency));
        }

        private void StopBlinking()
        {
            if (_routine != null)
            {
                StopCoroutine(_routine);
                _routine = null;

                _renderer.enabled = _rendererEnabledCache;
            }
        }

        private IEnumerator BlinkRoutine(float frequency)
        {
            float cooldown = frequency > 0 ? 1f / frequency : 0;
            YieldInstruction waitInstruction = cooldown > 0
                ? new WaitForSeconds(cooldown) : null;

            while (gameObject.activeInHierarchy)
            {
                _renderer.enabled = !_renderer.enabled;
                yield return waitInstruction;
            }
        }
    }
}