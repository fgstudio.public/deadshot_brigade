using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Components
{
    /// <summary>
    /// Компонент автоуничтожения объекта.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(AutoDestroyComponent))]
    public sealed class AutoDestroyComponent : Component
    {
        [field: SerializeField, Required, ChildGameObjectsOnly] public TimerComponent TimerComponent { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public DestroyComponent DestroyComponent { get; private set; }

        private void Awake() => Init();
        private void OnValidate() => Init();

        private void Init()
        {
            InitReferences();
            InitComponents();
        }

        private void InitReferences()
        {
            TimerComponent ??= this.GetComponentInHierarchy<TimerComponent>();
            DestroyComponent ??= this.GetComponentInHierarchy<DestroyComponent>();
        }

        private void InitComponents()
        {
            TimerComponent.Disable();
            DestroyComponent.Disable();
        }

        protected override void OnEnabled()
        {
            InitComponents();
            TimerComponent.Elapsed.AddListener(DestroyComponent.Enable);
            TimerComponent.Enable();
        }

        protected override void OnDisabled()
        {
            TimerComponent.Elapsed.RemoveListener(DestroyComponent.Enable);
            InitComponents();
        }
    }
}