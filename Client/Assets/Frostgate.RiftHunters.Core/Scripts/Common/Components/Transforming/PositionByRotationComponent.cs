using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(PositionByRotationComponent))]
    public sealed class PositionByRotationComponent : Component
    {
        [Serializable] private sealed class StateConfig
        {
            [field: SerializeField] public Vector3 Rotation { get; private set; }
            [field: SerializeField] public Vector3 Position { get; private set; }
        }

        private struct FoundRotationDTO
        {
            public readonly int Index;
            public readonly StateConfig State;
            public readonly float SqrMagnitude;
            public FoundRotationDTO(int index, StateConfig state, float sqrMagnitude) =>
                (Index, State, SqrMagnitude) = (index, state, sqrMagnitude);
        }

        [SerializeField] private Space _positionSpace;
        [SerializeField] private Space _rotationSpace;
        [SerializeField] private StateConfig[] _states;

        private Transform _transformCache;

        protected override void OnEnabled() =>
            _transformCache ??= transform;

        private void Update()
        {
            Vector3 rotation = GetActualRotation();
            FoundRotationDTO closestDTO = FindClosestRotation(rotation);
            FoundRotationDTO lessCloseDTO = FindClosestRotation(rotation, closestDTO.Index);

            float ratio = closestDTO.SqrMagnitude / (closestDTO.SqrMagnitude + lessCloseDTO.SqrMagnitude);
            Vector3 position = Vector3.Lerp(closestDTO.State.Position, lessCloseDTO.State.Position, ratio);

            SetActualPosition(position);
        }

        private Vector3 GetActualRotation() =>
            _rotationSpace switch
            {
                Space.Self => _transformCache.localEulerAngles,
                Space.World => _transformCache.eulerAngles,
                _ => throw new ArgumentOutOfRangeException()
            };

        private void SetActualPosition(Vector3 actualPosition)
        {
            switch (_positionSpace)
            {
                case Space.Self:
                    _transformCache.localPosition = actualPosition;
                    break;

                case Space.World:
                    _transformCache.position = actualPosition;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private FoundRotationDTO FindClosestRotation(Vector3 targetRotation, int exceptIndex = -1)
        {
            var dto = new FoundRotationDTO(0, _states[0], float.MaxValue);

            for (int i = 0; i < _states.Length; i++)
            {
                if (i == exceptIndex) continue;

                Vector3 rotationDiff = _states[i].Rotation - targetRotation;
                float sqrMagnitude = Vector3.SqrMagnitude(rotationDiff);

                if (sqrMagnitude < dto.SqrMagnitude)
                    dto = new FoundRotationDTO(i, _states[i], sqrMagnitude);
            }

            return dto;
        }
    }
}