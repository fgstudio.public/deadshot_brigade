using UnityEngine;

namespace Frostgate.RiftHunters.Core.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(OverrideRotationComponent))]
    public sealed class OverrideRotationComponent : Component
    {
        [SerializeField] private Space _space;
        [SerializeField] private Vector3 _rotation;

        private Transform _transformCache;

        protected override void OnEnabled() =>
            _transformCache ??= transform;

        private void Update()
        {
            switch (_space)
            {
                case Space.Self:
                    transform.localEulerAngles = _rotation;
                    break;
                case Space.World:
                    transform.eulerAngles = _rotation;
                    break;
            }
        }
    }
}