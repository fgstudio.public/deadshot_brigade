using Frostgate.RiftHunters.Core.Utils;
using Frostgate.RiftHunters.Core.Components.Filters;
using Frostgate.RiftHunters.Core.Components.Observable;

namespace Frostgate.RiftHunters.Core.Components
{
    public partial class AutoRotationWithCamera
    {
        private const string NoObservablesError = "No observables";
        private const string DuplicateObservablesError = "Duplicate observables";

        private void OnValidate()
        {
            if (_filters == null || _filters.Length == 0)
                _filters = GetComponents<Vector3Filter>();

            if (_observables == null || _observables.Length == 0)
                _observables = GetComponents<MonoObservable>();

            _rotatingObject ??= transform;
            _cameraProvider ??= GetComponent<CameraProvider>();
        }

        private bool HasObservables() => _observables?.Length > 0;
        private bool AreObservablesUnique() => _observables == null || !Validator.HasDuplicateElements(_observables);
    }
}
