using UnityEngine;

namespace Frostgate.RiftHunters.Core.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(MoveByVelocityComponent))]
    public sealed class MoveByVelocityComponent : Component
    {
        public Vector2 Velocity;

        private void Update()
        {
            if (Velocity == Vector2.zero)
                return;

            Vector3 velocity3 = new Vector3(Velocity.x, 0, Velocity.y);
            transform.Translate(Time.deltaTime * velocity3, Space.Self);
        }
    }
}