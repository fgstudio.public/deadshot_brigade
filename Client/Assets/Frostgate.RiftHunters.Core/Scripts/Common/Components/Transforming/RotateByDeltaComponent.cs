using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(RotateByDeltaComponent))]
    public sealed class RotateByDeltaComponent : Component
    {
        private const int MaxDegrees = 360;

        [FoldoutGroup("Settings"), SerializeField, Range(-MaxDegrees, MaxDegrees)]
        private float _degreesPerInch;

        public Vector2 DeltaRotation;

        private void Update()
        {
            if (DeltaRotation == Vector2.zero)
                return;

            float angle = DeltaRotation.x * _degreesPerInch;
            transform.Rotate(Vector3.up, angle);

            DeltaRotation = Vector2.zero;
        }
    }
}