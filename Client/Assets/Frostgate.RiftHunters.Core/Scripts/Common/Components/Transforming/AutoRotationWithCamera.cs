using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Utils;
using Frostgate.RiftHunters.Core.Components.Filters;
using Frostgate.RiftHunters.Core.Components.Observable;

namespace Frostgate.RiftHunters.Core.Components
{
    /// <summary>
    /// Компонент для поворота объекта при изменении данных камеры.
    /// Обновление поворота будет происходить в зависимости от добавленных Observable.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(AutoRotationWithCamera))]
    public sealed partial class AutoRotationWithCamera : Component
    {
        [Header("References")]
        [SerializeField, Required] private Transform _rotatingObject;
        [SerializeField, Required] private CameraProvider _cameraProvider;
        [ValidateInput(nameof(HasObservables), NoObservablesError)]
        [ValidateInput(nameof(AreObservablesUnique), DuplicateObservablesError)]
        [SerializeField, Required] private MonoObservable[] _observables;

        [Header("Settings")]
        [SerializeField, Required] private Vector3Filter[] _filters = Array.Empty<Vector3Filter>();

        [CanBeNull] private Transform _cameraTransform;
        private bool _isClientWithCamera;
        private Vector3 _rotationVector;

        private void OnEnable()
        {
            UpdateObservableCamera();

            //TODO: Сейчас этот компонент работает на серваке а камеры на серваке уже нет. Как только переедет чисто на клиент проверку можно выпилить.
            _isClientWithCamera = _cameraTransform != null;

            if (_isClientWithCamera)
                foreach (MonoObservable observable in _observables)
                    observable.Changed.AddListener(UpdateRotationVector);
        }

        private void OnDisable()
        {
            if (_isClientWithCamera)
                foreach (MonoObservable observable in _observables)
                    observable.Changed.RemoveListener(UpdateRotationVector);
        }

        private void Update()
        {
            bool needFindingCamera = _isClientWithCamera && _cameraTransform!.gameObject.activeInHierarchy == false;
            if (needFindingCamera) UpdateObservableCamera();
        }

        // на случай, если объект будет находиться внутри другого вращаюшегося объекта.
        private void LateUpdate()
        {
            if (_isClientWithCamera)
                UpdateObjectRotation();
        }

        private void UpdateObservableCamera()
        {
            Camera camera = _cameraProvider.Camera;
            Transform cameraTransform = camera?.transform;

            if (cameraTransform != null)
            {
                _cameraTransform = cameraTransform;

                foreach (MonoObservable observable in _observables)
                    observable.SetObservable(cameraTransform);

                UpdateRotationVector();
                UpdateObjectRotation();
            }
        }

        private void UpdateObjectRotation() =>
            _rotatingObject.eulerAngles = _rotationVector;

        private void UpdateRotationVector() =>
            _rotationVector = CalcNewRotation();

        private Vector3 CalcNewRotation()
        {
            Vector3 newRotation = _cameraTransform != null
                ? _cameraTransform.eulerAngles : Vector3.zero;

            foreach (Vector3Filter filter in _filters)
                newRotation = filter.Filter(_rotatingObject, newRotation);

            return newRotation;
        }
    }
}