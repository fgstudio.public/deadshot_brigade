using System;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Components
{
    /// <summary>
    /// Компонент удаления объекта
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(DestroyComponent))]
    public sealed partial class DestroyComponent : Component
    {
        [Flags]
        public enum DestroyPolicy
        {
            None = 0x00,
            DestroyObject = 0x01,
            SendEvent = 0x02
        }

        [Required] public GameObject DestroyingObject;
        [DisableIf(nameof(IsPropertyBlocked))] public DestroyPolicy Policy = DestroyPolicy.DestroyObject;

        [field: SerializeField, ShowIf(nameof(ShowInspectorEvent)), FoldoutGroup("Events")]
        public UnityEvent Destroyed { get; private set; }

        protected override void OnEnabled()
        {
            if (Policy.HasFlag(DestroyPolicy.SendEvent))
                Destroyed?.Invoke();

            if (Policy.HasFlag(DestroyPolicy.DestroyObject))
                Destroy(DestroyingObject);
        }
    }
}