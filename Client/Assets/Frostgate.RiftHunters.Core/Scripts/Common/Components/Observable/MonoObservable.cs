using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Components.Observable
{
    /// <summary>
    /// Уведомляет подписчиков при изменении данных Observable-объекта.
    /// Изменение отслеживается сравнением текущих данных с данными в кэше.
    /// </summary>
    public abstract class MonoObservable : Component
    {
        protected interface ICache
        {
            void Update([NotNull] Transform observable);
            bool IsObsolete([NotNull] Transform observable);
        }

        [field: Header("References")]
        [field: SerializeField, Required] public Transform Observable { get; private set; }
        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Changed { get; private set; }

        protected abstract ICache Cache { get; }

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();

        [ContextMenu(nameof(InitReferences))]
        private void InitReferences()
        {
            if (Observable == null)
                SetObservable(transform);
        }

        private void Update()
        {
            // TODO: null-check — это хот-фикс
            if (Observable != null && Cache.IsObsolete(Observable))
            {
                Cache.Update(Observable);
                Changed?.Invoke();
            }
        }

        public void SetObservable([NotNull] Transform observable)
        {
            Observable = observable;
            Cache.Update(Observable);
        }
    }
}
