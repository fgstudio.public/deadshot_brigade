using UnityEngine;

namespace Frostgate.RiftHunters.Core.Components.Observable
{
    /// <summary>
    /// Уведомляет подписчиков при изменении поворота Observable-объекта.
    /// </summary>
    [AddComponentMenu(BattleComponentMenus.Components.Observable.Menu + "/" + nameof(RotationObservable))]
    public sealed class RotationObservable : MonoObservable
    {
        private sealed class CacheRotation : ICache
        {
            private Quaternion _rotation;

            public void Update(Transform observable) =>
                _rotation = observable.localRotation;

            public bool IsObsolete(Transform observable) =>
                _rotation != observable.localRotation;
        }

        private readonly CacheRotation _cacheRotation = new();
        protected override ICache Cache => _cacheRotation;
    }
}