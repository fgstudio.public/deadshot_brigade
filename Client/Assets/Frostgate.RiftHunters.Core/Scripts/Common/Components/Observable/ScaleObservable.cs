using UnityEngine;

namespace Frostgate.RiftHunters.Core.Components.Observable
{
    /// <summary>
    /// Уведомляет подписчиков при изменении масштаба Observable-объекта.
    /// </summary>
    [AddComponentMenu(BattleComponentMenus.Components.Observable.Menu + "/" + nameof(ScaleObservable))]
    public sealed class ScaleObservable : MonoObservable
    {
        private sealed class CacheScale : ICache
        {
            private Vector3 _scale;

            public void Update(Transform observable) =>
                _scale = observable.localScale;

            public bool IsObsolete(Transform observable) =>
                _scale != observable.localScale;
        }

        private readonly CacheScale _cacheScale = new();
        protected override ICache Cache => _cacheScale;
    }
}