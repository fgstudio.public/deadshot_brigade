using UnityEngine;

namespace Frostgate.RiftHunters.Core.Components.Observable
{
    /// <summary>
    /// Уведомляет подписчиков при изменении позиции Observable-объекта.
    /// </summary>
    [AddComponentMenu(BattleComponentMenus.Components.Observable.Menu + "/" + nameof(PositionObservable))]
    public sealed class PositionObservable : MonoObservable
    {
        private sealed class CachePosition : ICache
        {
            private Vector3 _position;

            public void Update(Transform transform) =>
                _position = transform.localPosition;

            public bool IsObsolete(Transform transform) =>
                _position != transform.localPosition;
        }

        private readonly CachePosition _cachePosition = new();
        protected override ICache Cache => _cachePosition;
    }
}