using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Components
{
    public partial class DestroyComponent
    #if UNITY_EDITOR
        : Utils.Inspector.IPropertyBlockable
    #endif
    {
        private bool ShowInspectorEvent => Policy.HasFlag(DestroyPolicy.SendEvent);

#if UNITY_EDITOR
        private readonly HashSet<string> _blockedPropertyNames = new();

        void Utils.Inspector.IPropertyBlockable.BlockProperty(string filedName) => _blockedPropertyNames.Add(filedName);
        void Utils.Inspector.IPropertyBlockable.UnblockProperty(string fieldName) => _blockedPropertyNames.Remove(fieldName);

        private bool IsPropertyBlocked(Sirenix.OdinInspector.Editor.InspectorProperty property) =>
            _blockedPropertyNames.Contains(property.Name);
#else
        private bool IsPropertyBlocked() => false;
#endif
    }
}
