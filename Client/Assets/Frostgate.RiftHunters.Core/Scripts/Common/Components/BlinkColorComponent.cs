﻿using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;

namespace Frostgate.RiftHunters.Core.Components
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(BlinkColorComponent))]
    public sealed class BlinkColorComponent : Component
    {
        [SerializeField, Required] private Renderer _renderer;
        [SerializeField] private Color _colorFrom;
        [SerializeField] private Color _colorTo;
        [SerializeField, MinValue(0), SuffixLabel("sec", true)] private float _duration;

        private TweenerCore<Color, Color, ColorOptions> _tween;

        protected override void OnEnabled()
        {
            _renderer.material.color = _colorFrom;
            _tween = _renderer.material.DOColor(_colorTo, _duration)
                .SetLoops(-1, LoopType.Yoyo);
        }

        protected override void OnDisabled()
        {
            _tween.Kill();
            _renderer.material.color = _colorFrom;
        }
    }
}