using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Components.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(ClearColorComponent))]
    public sealed class ClearColorComponent : Component
    {
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private Graphic _graphic;

        private Color _originalColor;

        private void Awake() => InitGraphics();
        private void OnValidate() => InitGraphics();
        private void InitGraphics() => _graphic ??= GetComponent<Graphic>();

        protected override void OnEnabled()
        {
            _originalColor = _graphic.color;
            _graphic.color = Color.clear;
        }

        protected override void OnDisabled()
        {
            _graphic.color = _originalColor;
        }
    }
}