﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Components.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(SpinnerComponent))]
    public sealed class SpinnerComponent : Component
    {
        [SerializeField] private Vector3 _rotationAnglePerSecond;

        private void FixedUpdate() => RotationTick(Time.fixedDeltaTime);

        private void RotationTick(float tickTime) => transform.Rotate(_rotationAnglePerSecond.x * tickTime,
            _rotationAnglePerSecond.y * tickTime, _rotationAnglePerSecond.z * tickTime);
    }
}