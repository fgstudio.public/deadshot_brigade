﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Components
{
    /// <summary>
    /// Компонент, реализующущий сбрасываемый таймер. Включается при активации, отключается при деактивации.
    /// </summary>
    /// <remarks>Может иметь аффект от TimeScale.</remarks>
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(ResetableTimerComponent))]
    public sealed class ResetableTimerComponent : Component
    {
        private const int MinSeconds = 0;
        private const int MaxSeconds = 3600;

        [field: SerializeField, FoldoutGroup("Events")]
        public UnityEvent Elapsed { get; private set; }

        [Range(MinSeconds, MaxSeconds)] public float Seconds;

        private float _elapsedSeconds;
        private Coroutine _timerRoutine;

        public void ResetTimer() => StartTimer();

        public void ResetElapsedTime() => _elapsedSeconds = 0;

        protected override void OnEnabled() => StartTimer();

        protected override void OnDisabled() => StopTimer();

        private IEnumerator TimerRoutine()
        {
            while (_elapsedSeconds < Seconds)
            {
                yield return null;
                _elapsedSeconds += Time.deltaTime;
            }

            Elapsed.Invoke();
        }

        private void StartTimer()
        {
            StopTimer();

            ResetElapsedTime();
            _timerRoutine = StartCoroutine(TimerRoutine());
        }

        private void StopTimer()
        {
            if (_timerRoutine != null)
                StopCoroutine(_timerRoutine);
        }
    }
}