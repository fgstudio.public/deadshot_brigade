using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Components
{
    /// <summary>
    /// Отключает gameObject, если задан NetId, являющийся NetId локального игрока
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(ComponentMenus.RiftHunters.Menu + "/" + nameof(NonLocalPlayerObjectComponent))]
    public sealed class NonLocalPlayerObjectComponent : NetworkPlayerObjectComponent
    {
        protected override bool IsObjectActive()
        {
            if (NetworkClient.active) return NetworkClient.localPlayer?.netId != NetId;
            if (NetworkServer.active) return NetworkServer.localConnection?.identity.netId != NetId;
            return true;
        }
    }
}
