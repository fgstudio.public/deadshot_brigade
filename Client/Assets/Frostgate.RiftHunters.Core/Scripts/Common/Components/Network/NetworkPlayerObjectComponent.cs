using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Components
{
    public abstract class NetworkPlayerObjectComponent : Component
    {
        [SerializeField, OnValueChanged(nameof(UpdateActivity))] private uint _netId;

        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent NetIdChanged { get; private set; }

        public uint NetId
        {
            get => _netId;
            set => SetNetId(value);
        }

        protected abstract bool IsObjectActive();
        protected override void OnEnabled() => UpdateActivity();

        private void SetNetId(uint netId)
        {
            if (_netId != netId)
            {
                _netId = netId;
                OnNetIdChanged();
            }
        }

        private void OnNetIdChanged()
        {
            UpdateActivity();
            NetIdChanged?.Invoke();
        }

        private void UpdateActivity() =>
            gameObject.SetActive(IsObjectActive());
    }
}
