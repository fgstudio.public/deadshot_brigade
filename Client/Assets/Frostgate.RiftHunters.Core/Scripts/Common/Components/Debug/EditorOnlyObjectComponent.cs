﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Debug
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(EditorOnlyObjectComponent))]
    public sealed class EditorOnlyObjectComponent : MonoBehaviour
    {
        private void OnEnable()
        {
            if (!Application.isEditor)
                gameObject.SetActive(false);
        }
    }
}