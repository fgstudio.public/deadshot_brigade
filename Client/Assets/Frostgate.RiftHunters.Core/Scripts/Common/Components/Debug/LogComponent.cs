using UnityEngine;

namespace Frostgate.RiftHunters.Core.Debug
{
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(LogComponent))]
    public sealed class LogComponent : Component
    {
        [SerializeField] private Color _defaultColor = Color.gray;

        public void Log(string message) =>
            Log(message, _defaultColor);

        public void Log(string message, Color color)
        {
            if (!enabled)
                return;

            UnityEngine.Debug.Log(ColorMessage(message, color));
        }

        private string ColorMessage(string message, Color color)
        {
            const string colorTag = "color";
            const string closeAttr = "</" + colorTag + ">";

            string stringRGB = ColorUtility.ToHtmlStringRGB(color);
            string openAttr = $"<{colorTag}=#{stringRGB}>";

            return openAttr + message + closeAttr;
        }
    }
}