using System;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.Components
{
    /// <summary>
    /// Компонент, реализующий таймер. Включается при активации компонента и отключается при деактивации.
    /// <see cref="Timer"/>
    /// </summary>
    [AddComponentMenu(BattleComponentMenus.Components.Menu + "/" + nameof(TimerComponent))]
    public sealed class TimerComponent : Component, ITimer
    {
        private const int MinSeconds = 0;
        private const int MaxSeconds = 3600;

        [Range(MinSeconds, MaxSeconds)] public float Seconds;
        public bool IgnoreTimeScale;

        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Started { get; private set; } = new();
        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Paused { get; private set; } = new();
        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Stopped { get; private set; } = new();
        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Ticked { get; private set; } = new();
        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Elapsed { get; private set; } = new();

        private Timer _timerInstance;

        public TimeSpan Interval => TimeSpan.FromSeconds(Seconds);
        public TimeSpan Passed => _timerInstance?.Passed ?? TimeSpan.Zero;
        public TimeSpan Left => _timerInstance?.Left ?? Interval;
        public bool IsPaused => _timerInstance?.IsPaused ?? false;
        public bool IsElapsed => _timerInstance?.IsElapsed ?? false;
        public UniTask Task => _timerInstance?.Task ?? UniTask.CompletedTask;

        bool IReadOnlyTimer.IgnoreTimeScale => IgnoreTimeScale;
        event UnityAction IReadOnlyTickable.Ticked
        {
            add => Ticked.AddListener(value);
            remove => Ticked.RemoveListener(value);
        }
        event UnityAction IReadOnlyTimer.Started
        {
            add => Started.AddListener(value);
            remove => Started.RemoveListener(value);
        }
        event UnityAction IReadOnlyTimer.Paused
        {
            add => Paused.AddListener(value);
            remove => Paused.RemoveListener(value);
        }
        event UnityAction IReadOnlyTimer.Stopped
        {
            add => Stopped.AddListener(value);
            remove => Stopped.RemoveListener(value);
        }
        event UnityAction IReadOnlyTimer.Elapsed
        {
            add => Elapsed.AddListener(value);
            remove => Elapsed.RemoveListener(value);
        }

        void IDisposable.Dispose() => Destroy(gameObject);
        private void OnDestroy()
        {
            if (_timerInstance != null)
                DestroyTimer(_timerInstance);
        }

        protected override void OnEnabled()
        {
            _timerInstance = GetTimerInstance();
            _timerInstance.Start();
        }

        protected override void OnDisabled() =>
            _timerInstance?.Stop();

        void ITimer.Start() => Enable();
        void ITimer.Stop() => Disable();
        [ContextMenu(nameof(Pause))] public void Pause() => _timerInstance.Pause();
        [ContextMenu(nameof(Continue))] public void Continue() => _timerInstance.Start();

        private Timer GetTimerInstance()
        {
            if (_timerInstance == null)
                return CreateTimer();

            if (!IsTimerActual(_timerInstance))
            {
                DestroyTimer(_timerInstance);
                return CreateTimer();
            }

            return _timerInstance;
        }

        private bool IsTimerActual([NotNull] Timer timer) =>
            Mathf.Approximately((float)timer.Interval.TotalSeconds, Seconds) && timer.IgnoreTimeScale == IgnoreTimeScale;

        private Timer CreateTimer()
        {
            var timer = new Timer(TimeSpan.FromSeconds(Seconds), IgnoreTimeScale);
            Subscribe(timer);

            return timer;
        }

        private void DestroyTimer([NotNull] Timer timer)
        {
            Unsubscribe(timer);
            timer.Dispose();
        }

        private void Subscribe([NotNull] Timer timer)
        {
            timer.Started += Started.Invoke;
            timer.Paused += Paused.Invoke;
            timer.Stopped += Stopped.Invoke;
            timer.Ticked += Ticked.Invoke;
            timer.Elapsed += ElapseTimer;
        }

        private void Unsubscribe([NotNull] Timer timer)
        {
            timer.Started -= Started.Invoke;
            timer.Paused -= Paused.Invoke;
            timer.Stopped -= Stopped.Invoke;
            timer.Ticked -= Ticked.Invoke;
            timer.Elapsed -= ElapseTimer;
        }

        private void ElapseTimer()
        {
            Elapsed.Invoke();
            Disable();
        }
    }
}