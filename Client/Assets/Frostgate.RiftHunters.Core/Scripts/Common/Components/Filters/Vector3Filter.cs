using UnityEngine;

namespace Frostgate.RiftHunters.Core.Components.Filters
{
    /// <summary>
    /// Базовый компонент фильтра при установке Vector3-значения.
    /// </summary>
    public abstract class Vector3Filter : Component
    {
        public Vector3 Filter(Transform transform, Vector3 vector3) =>
            !enabled ? vector3 : InnerFilter(transform, vector3);

        protected abstract Vector3 InnerFilter(Transform transform, Vector3 vector3);
    }
}
