using UnityEngine;

namespace Frostgate.RiftHunters.Core.Components.Filters
{
    /// <summary>
    /// Компонент для игнорирования выбранных осей в векторе.
    /// </summary>
    [AddComponentMenu(BattleComponentMenus.Components.Filters.Menu + "/" + nameof(IgnoreEulerAnglesFilter))]
    public sealed class IgnoreEulerAnglesFilter : Vector3Filter
    {
        [SerializeField] private bool _x;
        [SerializeField] private bool _y;
        [SerializeField] private bool _z;

        private bool HasIgnored => _x || _y || _z;

        protected override Vector3 InnerFilter(Transform transform, Vector3 newRotation)
        {
            if (!HasIgnored) return newRotation;

            Vector3 rotation = transform.eulerAngles;

            if (_x) newRotation.x = rotation.x;
            if (_y) newRotation.y = rotation.y;
            if (_z) newRotation.z = rotation.z;

            return newRotation;
        }
    }
}