﻿using Zenject;

namespace Frostgate.RiftHunters.Core
{
    public abstract class FeatureInstaller : MonoInstaller
    {
        public sealed override void InstallBindings()
        {
#if UNITY_EDITOR
            InstallServerPart();
            InstallClientPart();
#elif UNITY_SERVER
            InstallServerPart();
#else
            InstallClientPart();
#endif
        }

        protected virtual void InstallServerPart()
        {
        }

        protected virtual void InstallClientPart()
        {
        }
    }
}
