﻿using System;

namespace Frostgate.RiftHunters.Core
{
    public static class StaticObject<T> where T : class
    {
        public static T Instance
        {
            get
            {
                if (!HasValue())
                    ThrowHasNoValue();

                return _instance;
            }

            set => _instance = value;
        }

        private static T _instance;

        public static bool HasValue() => _instance != null;

        private static void ThrowHasNoValue() =>
            throw new InvalidOperationException($"Object of type {typeof(T)} not registered");
    }
}
