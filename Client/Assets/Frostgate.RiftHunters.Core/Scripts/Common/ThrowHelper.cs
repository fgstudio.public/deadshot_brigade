﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public static class ThrowHelper
    {
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public static void ThrowIfArgumentNotFulfillCondition<TArg>([CanBeNull] TArg arg,
            [NotNull] Func<TArg, bool> condition) => ThrowIfArgumentNotFulfillCondition(arg, condition, nameof(arg));

        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public static void ThrowIfArgumentNotFulfillCondition<TArg>([CanBeNull] TArg arg,
            [NotNull] Func<TArg, bool> condition, string argName)
        {
            ThrowIfArgumentNullOrDefault(condition, nameof(condition));
            if (!condition.Invoke(arg))
                throw new ArgumentException($"Argument '{argName}' does not fulfill the condition.");
        }

        /// <exception cref="ArgumentNullException"></exception>
        public static void ThrowIfArgumentNullOrDefault<TArg>([CanBeNull] TArg arg) =>
            ThrowIfArgumentNullOrDefault(arg, nameof(arg));

        /// <exception cref="ArgumentNullException"></exception>
        public static void ThrowIfArgumentNullOrDefault<TArg>([CanBeNull] TArg arg, string argName)
        {
            if (EqualityComparer<TArg>.Default.Equals(arg, default))
                throw new ArgumentNullException(argName);
        }
    }
}