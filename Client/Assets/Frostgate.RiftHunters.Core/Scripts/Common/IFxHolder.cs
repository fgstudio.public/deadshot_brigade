using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public interface IFxHolder
    {
        Transform FxContainer { get; }
        Transform WorldFxContainer { get; }
    }
}
