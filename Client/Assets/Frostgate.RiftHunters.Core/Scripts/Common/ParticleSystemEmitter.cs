using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core
{
    public readonly struct ParticleSystemDataCache
    {
        public readonly float Size;
        public readonly LayerMask CollidesWith;

        public ParticleSystemDataCache(ParticleSystem particleSystem)
        {
            Size = particleSystem.main.startSizeMultiplier;
            CollidesWith = particleSystem.collision.collidesWith;
        }
    }

    [AddComponentMenu(ComponentMenus.RiftHunters.Menu + "/" + nameof(ParticleSystemEmitter))]
    public sealed class ParticleSystemEmitter : MonoBehaviour
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private ParticleSystem[] _particleSystems;

        [Header("On Validate Options")]
        [SerializeField] private bool _autoAttachSystems;

        private readonly Dictionary<ParticleSystem, ParticleSystemDataCache> _dataCache = new ();

        private void Start()
        {
            foreach (ParticleSystem ps in _particleSystems)
                _dataCache[ps] = new ParticleSystemDataCache(ps);
        }

        private void OnValidate()
        {
            if (_autoAttachSystems)
                _particleSystems = GetComponentsInChildren<ParticleSystem>();
        }

        public void Emit(int count)
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                _particleSystems[i].Emit(count);
        }

        public void Play()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                _particleSystems[i].Play();
        }

        public void Stop()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
                _particleSystems[i].Stop();
        }

        public void MultiplyAlpha(float alphaMult)
        {
            for (int i = 0; i < _particleSystems.Length; i++)
            {
                ParticleSystem.MainModule main = _particleSystems[i].main;
                ParticleSystem.MinMaxGradient startColor = main.startColor;
                startColor.color = startColor.color.SetAlpha(startColor.color.a * alphaMult);
                main.startColor = startColor;
            }
        }

        public void SetSizeMultiplier(float multiplier)
        {
            for (int i = 0; i < _particleSystems.Length; i++)
            {
                ParticleSystem ps = _particleSystems[i];
                ParticleSystem.MainModule main = ps.main;
                main.startSizeMultiplier = _dataCache[ps].Size * multiplier;
            }
        }

        public void SetStartSpeed(float speed)
        {
            for (int i = 0; i < _particleSystems.Length; i++)
            {
                ParticleSystem ps = _particleSystems[i];
                ParticleSystem.MainModule main = ps.main;
                main.startSpeed = new ParticleSystem.MinMaxCurve(speed, speed);
            }
        }

        public void SetCollidesWithMask(LayerMask collidesWith)
        {
            for (int i = 0; i < _particleSystems.Length; i++)
            {
                ParticleSystem ps = _particleSystems[i];
                ParticleSystem.CollisionModule collision = ps.collision;
                collision.collidesWith = collidesWith;
            }
        }

        public void ResetCollidesWithMask()
        {
            for (int i = 0; i < _particleSystems.Length; i++)
            {
                ParticleSystem ps = _particleSystems[i];
                ParticleSystem.CollisionModule collision = ps.collision;
                collision.collidesWith = _dataCache[ps].CollidesWith;
            }
        }
    }
}