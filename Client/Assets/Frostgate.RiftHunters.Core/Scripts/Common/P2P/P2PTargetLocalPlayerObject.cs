using Mirror;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Utils;

namespace Frostgate.RiftHunters.Core
{
    /// <summary>
    /// Скрипт, который отключает объекты для игроков, чьи localPlayer не являются целевыми или серверными.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Components.P2P.Menu + "/" + nameof(P2PTargetLocalPlayerObject))]
    public sealed class P2PTargetLocalPlayerObject : MonoBehaviour
    {
        [SerializeField, Required] private P2PObject _p2pObject;

        private static NetworkIdentity LocalPlayer => LocalPlayerProvider.LocalPlayer;

        private void Awake()
        {
            InitReferences();
            Subscribe();
        }
        private void OnDestroy() => Unsubscribe();
        private void OnEnable() => UpdateActivity();
        private void OnValidate() => InitReferences();

        private void Subscribe()
        {
            _p2pObject.Updated.AddListener(UpdateActivity);
            LocalPlayerProvider.AddInitializationListener(UpdateActivity);
        }
        private void Unsubscribe()
        {
            _p2pObject.Updated.RemoveListener(UpdateActivity);
            LocalPlayerProvider.RemoveInitializationListener(UpdateActivity);
        }

        private void InitReferences() => _p2pObject ??= this.GetComponentInHierarchy<P2PObject>();
        private void UpdateActivity() => gameObject.SetActive(CanBeActive());
        private bool CanBeActive() => LocalPlayer == null || LocalPlayer.isServerOnly || _p2pObject.TargetId == LocalPlayer.netId;
    }
}