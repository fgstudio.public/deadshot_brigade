namespace Frostgate.RiftHunters.Core
{
    public interface IP2P
    {
        IP2PObject P2P { get; }
    }
}
