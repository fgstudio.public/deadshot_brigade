using Mirror;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface IP2PObject : INetworkIdentifiable, IUnityEventUpdateable
    {
        uint HostId { get; }
        uint TargetId { get; }
    }

    /// <summary>
    /// Поведение, которое позволяет задавать владельца объекта и его получателя.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(NetworkIdentity))]
    [AddComponentMenu(BattleComponentMenus.Components.P2P.Menu + "/" + nameof(P2PObject))]
    public sealed class P2PObject : NetworkBehaviour, IP2PObject, IResettable
    {
        [Header("References")]
        [SerializeField, Required, ReadOnly] private NetworkIdentity _identity;

        [Header("Data")]
        // Можно было бы задавать NetworkIdentity, но они время от времени синкаются как null.
        // Объектов с netId == 0 нет. При 0 можно считать, что значение не задано.
        [SerializeField, ReadOnly, SyncVar(hook = nameof(SyncHook))] private uint _hostIdId;
        [SerializeField, ReadOnly, SyncVar(hook = nameof(SyncHook))] private uint _targetId;

        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Updated { get; private set; }

        public uint HostId => _hostIdId;
        public uint TargetId => _targetId;
        public NetworkIdentity Identity => _identity;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() => _identity ??= GetComponent<NetworkIdentity>();

        public void SetData(uint hostId, uint targetId)
        {
            _hostIdId = hostId;
            _targetId = targetId;
            Updated?.Invoke();
        }

        public void Reset()
        {
            _hostIdId = default;
            _targetId = default;
            Updated?.Invoke();
        }

        private void SyncHook(uint oldValue, uint newValue)
        {
            if (oldValue != newValue)
                Updated?.Invoke();
        }
    }
}