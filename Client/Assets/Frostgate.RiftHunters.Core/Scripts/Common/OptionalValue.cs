﻿using System.Collections.Generic;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core
{
    [Serializable]
    public struct OptionalValue<T>
    {
        public bool HasValue => EqualityComparer<T>.Default.Equals(Value, default);
        [field: SerializeField] public T Value { get; private set; }
    }
}