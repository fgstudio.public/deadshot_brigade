﻿using System;
using System.Collections;

namespace Frostgate.RiftHunters.Core
{
    public abstract class AsyncOperation : IEnumerator
    {
        public virtual event Action<AsyncOperation> Completed
        {
            add
            {
                if (IsCompleted)
                    value.Invoke(this);
                else
                    _completed += value;
            }

            remove => _completed -= value;
        }

        public virtual bool IsCompleted
        {
            get => _isCompleted;
            protected set
            {
                if (value && !_isCompleted)
                {
                    _isCompleted = true;
                    InvokeCompletedEvent();
                }
            }
        }

        public virtual object Current => null;

        private Action<AsyncOperation> _completed = delegate { };

        private bool _isCompleted;

        public virtual bool MoveNext() => !IsCompleted;

        public abstract void Cancel();
        
        public virtual void Reset()
        {
        }

        protected void InvokeCompletedEvent() => _completed.Invoke(this);
    }
}
