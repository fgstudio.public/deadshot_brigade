﻿namespace Frostgate.RiftHunters.Core
{
    public class CompletedOperationWithResult<TResult, TError> : AsyncOperationWithResult<TResult, TError>
    {
        public sealed override bool IsCompleted => true;

        public sealed override TResult Result { get; protected set; }

        public sealed override TError Error { get; protected set; }

        public CompletedOperationWithResult(TResult result, TError error = default)
        {
            Result = result;
            Error = error;
        }

        public override void Cancel()
        {
        }
    }
}
