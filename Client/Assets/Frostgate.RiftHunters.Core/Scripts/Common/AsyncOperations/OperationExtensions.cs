﻿using System.Collections;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public static class OperationExtensions
    {
        public static UniTask ToUniTask([NotNull] this AsyncOperation operation) =>
            ((IEnumerator)operation).ToUniTask();

        public static async UniTask<TResult> ToUniTask<TResult, TError>(
            [NotNull] this AsyncOperationWithResult<TResult, TError> operation)
        {
            await ((AsyncOperation)operation).ToUniTask();

            return operation.Result;
        }
    }
}
