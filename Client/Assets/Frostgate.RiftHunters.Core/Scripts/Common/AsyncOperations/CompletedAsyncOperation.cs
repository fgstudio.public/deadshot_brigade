﻿namespace Frostgate.RiftHunters.Core
{
    public class CompletedAsyncOperation : AsyncOperation
    {
        public sealed override bool IsCompleted => true;

        public override void Cancel()
        {
        }
    }
}