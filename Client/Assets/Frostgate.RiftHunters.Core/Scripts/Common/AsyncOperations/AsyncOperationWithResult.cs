﻿namespace Frostgate.RiftHunters.Core
{
    public abstract class AsyncOperationWithResult<TResult, TError> : AsyncOperationWithError<TError>
    {
        public abstract TResult Result { get; protected set; }

        protected void SetCompleted(TResult result, TError error = default)
        {
            Result = result;
            Error = error;
            IsCompleted = true;
        }
    }
}
