﻿namespace Frostgate.RiftHunters.Core
{
    public abstract class AsyncOperationWithError<TError> : AsyncOperation
    {
        public abstract TError Error { get; protected set; }

        protected void SetCompleted(TError error = default)
        {
            Error = error;
            IsCompleted = true;
        }
    }
}
