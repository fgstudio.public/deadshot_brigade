﻿namespace Frostgate.RiftHunters.Core
{
    public class CompletedOperationWithError<TError> : AsyncOperationWithError<TError>
    {
        public sealed override bool IsCompleted => true;

        public sealed override TError Error { get; protected set; }

        public CompletedOperationWithError(TError error = default)
        {
            Error = error;
        }

        public override void Cancel()
        {
        }
    }
}