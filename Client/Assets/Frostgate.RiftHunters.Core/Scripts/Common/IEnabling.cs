namespace Frostgate.RiftHunters.Core
{
    public interface IEnabling
    {
        public bool IsEnabled { get; }

        public void Enable();
        public void Disable();
    }
}
