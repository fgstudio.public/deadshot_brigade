﻿using Sirenix.OdinInspector;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core
{
    [Serializable]
    public struct FadeSettings
    {
        public const float MaxAlpha = 1f;
        public const float MinAlpha = 0f;
        public const float MinDuration = 0f;
        public const float MaxDuration = float.MaxValue;

        [field: SerializeField, Range(0, 1)] public float FadedTargetAlpha { get; private set; }
        [field: SerializeField, MinValue(0)] public float FadeDuration { get; private set; }

        [field: SerializeField, Range(0, 1), Title(""), Space(-20)] public float UnfadedTargetAlpha { get; private set; }
        [field: SerializeField, MinValue(0)] public float UnfadeDuration { get; private set; }

        public FadeSettings(float fadedTargetAlpha = MinAlpha, float fadeDuration = MinDuration,
            float unfadedTargetAlpha = MaxAlpha, float unfadeDuration = MinDuration)
        {
            FadedTargetAlpha = Mathf.Clamp(fadedTargetAlpha, MinAlpha, MaxAlpha);
            FadeDuration = Mathf.Clamp(fadeDuration, MinDuration, MaxDuration);
            UnfadedTargetAlpha = Mathf.Clamp(unfadedTargetAlpha, MinAlpha, MaxAlpha);
            UnfadeDuration = Mathf.Clamp(unfadeDuration, MinDuration, MaxDuration);
        }
    }
}