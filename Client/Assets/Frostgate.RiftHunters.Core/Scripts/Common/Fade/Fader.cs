﻿using JetBrains.Annotations;
using DG.Tweening;
using System;

namespace Frostgate.RiftHunters.Core
{
    public sealed class Fader<T>
    {
        public delegate Tweener DoFade(T fadeable, float targetAlpha, float duration);

        [NotNull] private readonly T _fadeable;
        [NotNull] private readonly DoFade _doFade;

        private readonly FadeSettings _settings;

        [CanBeNull] private Tweener _tweener;

        public Fader([NotNull] T fadeable, [NotNull] DoFade doFade, FadeSettings settings)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(fadeable, nameof(fadeable));
            ThrowHelper.ThrowIfArgumentNullOrDefault(doFade, nameof(doFade));

            _fadeable = fadeable;
            _doFade = doFade;
            _settings = settings;
        }

        public void Fade([CanBeNull] Action completeCallback = null)
        {
            _tweener?.Kill(true);
            _tweener = _doFade.Invoke(_fadeable, _settings.FadedTargetAlpha, _settings.FadeDuration);
            if (completeCallback != null) _tweener.OnComplete(completeCallback.Invoke);
        }

        public void Unfade([CanBeNull] Action completeCallback = null)
        {
            _tweener?.Kill(true);
            _tweener = _doFade.Invoke(_fadeable, _settings.UnfadedTargetAlpha, _settings.UnfadeDuration);
            if (completeCallback != null) _tweener.OnComplete(completeCallback.Invoke);
        }
    }
}