﻿using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public static class FaderExtensions
    {
        public static void SetFaded<T>([NotNull] this Fader<T> fader, bool isFaded,
            [CanBeNull] Action completeCallback = null)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(fader, nameof(fader));

            if (isFaded)
                fader.Fade(completeCallback);
            else
                fader.Unfade(completeCallback);
        }
    }
}