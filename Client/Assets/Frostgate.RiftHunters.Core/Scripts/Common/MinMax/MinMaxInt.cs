using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core
{
    [Serializable, InlineProperty(LabelWidth = 30)]
    public struct MinMaxInt: IMinMax<int>
    {
        [field: SerializeField, HorizontalGroup] public int Min { get; private set; }
        [field: SerializeField, HorizontalGroup] public int Max { get; private set; }

        public int Middle => (Min + Max) / 2;
        public int Magnitude => Mathf.Abs(Max - Min);

        public static MinMaxInt Infinity => new MinMaxInt(int.MinValue, int.MaxValue);

        public MinMaxInt(int value) : this(value, value) { }
        public MinMaxInt(int min, int max)
        {
            Min = min;
            Max = max;
        }

        public readonly int Clamp(int value) => Mathf.Clamp(value, Min, Max);
        public readonly bool Contains(int value) => Min <= value && value <= Max;
        public readonly bool Contains(float value) => Min <= value && value <= Max;
    }
}
