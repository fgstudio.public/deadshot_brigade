namespace Frostgate.RiftHunters.Core
{
    public interface IMinMax<T>
    {
        T Min { get; }
        T Max { get; }
        T Middle { get; }
        T Magnitude { get; }

        T Clamp(T value);
        bool Contains(T value);
    }
}
