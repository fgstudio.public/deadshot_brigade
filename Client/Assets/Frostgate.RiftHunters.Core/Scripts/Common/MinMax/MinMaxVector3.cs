using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    [Serializable]
    public struct MinMaxVector3 : IMinMax<Vector3>
    {
        private const string XGroup = "X";
        private const string YGroup = "Y";
        private const string ZGroup = "Z";
        private const float LabelWidth = 20;

        [field: SerializeField, LabelText("X"), HorizontalGroup(XGroup, LabelWidth = LabelWidth)]
        public bool UseRangeForX { get; private set; }
        [SerializeField, HideLabel, ShowIf(nameof(UseRangeForX)), HorizontalGroup(XGroup)]
        private MinMaxFloat x;

        [field: SerializeField, LabelText("Y"), HorizontalGroup(YGroup, LabelWidth = LabelWidth)]
        public bool UseRangeForY { get; private set; }
        [SerializeField, HideLabel, ShowIf(nameof(UseRangeForY)), HorizontalGroup(YGroup)]
        private MinMaxFloat y;

        [field: SerializeField, LabelText("Z"), HorizontalGroup(ZGroup, LabelWidth = LabelWidth)]
        public bool UseRangeForZ { get; private set; }
        [SerializeField, HideLabel, ShowIf(nameof(UseRangeForZ)), HorizontalGroup(ZGroup)]
        private MinMaxFloat z;

        public Vector3 Min => new Vector3(x.Min, y.Min, z.Min);
        public Vector3 Max => new Vector3(x.Max, y.Max, z.Max);

        public MinMaxFloat X => UseRangeForX ? x : MinMaxFloat.Infinity;
        public MinMaxFloat Y => UseRangeForY ? y : MinMaxFloat.Infinity;
        public MinMaxFloat Z => UseRangeForZ ? z : MinMaxFloat.Infinity;

        public Vector3 Middle => new Vector3
        (
            UseRangeForX ? x.Middle : 0,
            UseRangeForY ? y.Middle : 0,
            UseRangeForZ ? z.Middle : 0
        );
        public Vector3 Magnitude => new Vector3
        (
            x.Magnitude,
            y.Magnitude,
            z.Magnitude
        );


        public MinMaxVector3(Vector3 min, Vector3 max, bool useRangeForX, bool useRangeForY, bool useRangeForZ)
        : this(useRangeForX, min.x, max.x, useRangeForY, min.y, max.y, useRangeForZ, min.z, max.z)
        { }

        public MinMaxVector3(
            bool useRangeForX, float minX, float maxX,
            bool useRangeForY, float minY, float maxY,
            bool useRangeForZ, float minZ, float maxZ)
        {
            this.UseRangeForX = useRangeForX;
            x = useRangeForX ? new MinMaxFloat(minX, maxX) : new MinMaxFloat(minX);

            this.UseRangeForY = useRangeForY;
            y = useRangeForY ? new MinMaxFloat(minY, maxY) : new MinMaxFloat(minY);

            this.UseRangeForZ = useRangeForZ;
            z = useRangeForZ ? new MinMaxFloat(minZ, maxZ) : new MinMaxFloat(minZ);
        }


        public readonly Vector3 Clamp(Vector3 value)
        {
            if (UseRangeForX)
                value.x = x.Clamp(value.x);

            if (UseRangeForY)
                value.y = y.Clamp(value.y);

            if (UseRangeForZ)
                value.z = z.Clamp(value.z);

            return value;
        }

        public readonly bool Contains(Vector3 value)
        {
            if (UseRangeForX && !x.Contains(value.x))
                return false;

            if (UseRangeForY && !y.Contains(value.y))
                return false;

            if (UseRangeForZ && !z.Contains(value.z))
                return false;

            return true;
        }
    }
}
