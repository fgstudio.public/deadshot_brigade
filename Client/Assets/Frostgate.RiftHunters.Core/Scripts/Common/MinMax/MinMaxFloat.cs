using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    [Serializable, InlineProperty(LabelWidth = 30)]
    public struct MinMaxFloat : IMinMax<float>
    {
        public static MinMaxFloat Infinity => new MinMaxFloat(Mathf.NegativeInfinity, Mathf.Infinity);

        [field: SerializeField, HorizontalGroup] public float Min { get; private set; }
        [field: SerializeField, HorizontalGroup] public float Max { get; private set; }

        public float Middle => (Min + Max) / 2f;
        public float Magnitude => Mathf.Abs(Max - Min);

        public MinMaxFloat(float value) : this(value, value) { }
        public MinMaxFloat(float min, float max)
        {
            Min = min;
            Max = max;
        }

        public readonly float Clamp(float value) => Mathf.Clamp(value, Min, Max);
        public readonly bool Contains(float value) => Min <= value && value <= Max;
    }
}
