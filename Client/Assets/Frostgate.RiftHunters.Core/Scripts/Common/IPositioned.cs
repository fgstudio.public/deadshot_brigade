using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public interface IPositioned
    {
        Vector3 Position { get; }
    }
}
