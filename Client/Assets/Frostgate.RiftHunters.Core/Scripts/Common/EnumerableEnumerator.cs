using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core
{
    /// <summary>
    /// Итератор с возможностью применять Linq для своих эелментов
    /// </summary>
    public sealed class EnumerableEnumerator<T> : IEnumerable<T>, IEnumerator<T>
    {
        private const int DefaultIndex = -1;

        private readonly IReadOnlyList<T> _list;

        private int _currentIndex = DefaultIndex;
        private bool _isDisposed;

        public T Current => _currentIndex.IsContained(0, _list.Count - 1) ? _list[_currentIndex] : default;
        object IEnumerator.Current => Current;

        public EnumerableEnumerator(IEnumerable<T> enumerable)
            : this(enumerable.ToArray()) {}

        public EnumerableEnumerator(IReadOnlyList<T> list) =>
            _list = list;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
                _currentIndex = int.MaxValue;
            }
        }

        public IEnumerator<T> GetEnumerator() => _list.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public bool MoveNext()
        {
            int nextIndex = _currentIndex + 1;
            bool isNextIndexInRange = nextIndex.IsContained(0, _list.Count - 1);

            _currentIndex = isNextIndexInRange ? nextIndex : _list.Count;
            return isNextIndexInRange;
        }

        public void Reset() =>
            _currentIndex = DefaultIndex;
    }
}
