using System;
using UnityEngine.Events;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core
{
    public sealed class StubTimer : ITimer
    {
        public event UnityAction Ticked;
        public event UnityAction Started;
        public event UnityAction Paused;
        public event UnityAction Stopped;
        public event UnityAction Elapsed;

        public TimeSpan Interval => TimeSpan.Zero;
        public TimeSpan Passed => TimeSpan.Zero;
        public TimeSpan Left => Interval - Passed;
        public UniTask Task => UniTask.CompletedTask;
        public bool IsPaused => false;
        public bool IsElapsed => false;
        public bool IgnoreTimeScale => false;

        public void Dispose() { }
        public void Start() { }
        public void Pause() { }
        public void Stop() { }
    }
}
