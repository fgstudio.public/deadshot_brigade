﻿using System;

namespace Frostgate.RiftHunters.Core
{
    public interface IReadOnlyTimedContainer<out TData>
    {
        TData Data { get; }
        bool IsExpired { get; }
        TimeSpan TimeLeft { get; }
    }

    public sealed class TimedContainer<TData> : IReadOnlyTimedContainer<TData>
    {
        public TData Data { get; private set; }
        public bool IsExpired => DateTime.Now >= _expiresAfter;
        public TimeSpan TimeLeft => _expiresAfter - DateTime.Now;

        private DateTime _expiresAfter;

        public TimedContainer(TData data, DateTime expiresAfter)
        {
            Data = data;
            _expiresAfter = expiresAfter;
        }

        public TimedContainer(TData data, TimeSpan expiresAfter) : this(data, DateTime.Now + expiresAfter)
        {
        }

        public void Prolong(TimeSpan time) => _expiresAfter += time;
    }
}