﻿namespace Frostgate.RiftHunters.Core
{
    public interface IObservable
    {
    }

    public interface IObservable<out TEvent> : IObservable
    {
        void AddObserver<T>(T observer) where T : IObserver<TEvent>;
        void RemoveObserver<T>(T observer) where T : IObserver<TEvent>;
    }
}
