﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    /// <summary>
    /// Хранилище наблюдателей для разных типов событий с функционалом шины сообщений.
    /// </summary>
    /// <remarks>
    /// !Осторожно! Возможна упаковка конкретной реализации IObserver
    /// </remarks>
    public sealed class ObserverStorage
    {
        private IDictionary<Type, List<ObserverWrapper>> _observersStorage =
            new Dictionary<Type, List<ObserverWrapper>>();

        public void AddObserver<TEvent>([NotNull] IObserver<TEvent> observer)
        {
            if (!TryGetWrappers<TEvent>(out List<ObserverWrapper> wrappers))
            {
                wrappers = new List<ObserverWrapper>();
                _observersStorage[typeof(TEvent)] = wrappers;
            }

            wrappers.Add(new ObserverWrapper(observer));
        }

        public void RemoveObserver<TEvent>([NotNull] IObserver<TEvent> observer)
        {
            if (!TryGetWrappers<TEvent>(out List<ObserverWrapper> wrappers))
                return;

            ObserverWrapper wrapper = wrappers.Find(w => w.Observer == observer);
            if (wrapper != null)
                wrappers.Remove(wrapper);
        }

        public void NotifyObservers<TEvent>(TEvent @event)
        {
            if (!TryGetWrappers<TEvent>(out List<ObserverWrapper> wrappers))
                return;

            foreach (ObserverWrapper wr in wrappers)
                wr.TryHandleEvent(@event);
        }

        private bool TryGetWrappers<TEvent>(out List<ObserverWrapper> wrappers) =>
            _observersStorage.TryGetValue(typeof(TEvent), out wrappers);
    }
}
