﻿namespace Frostgate.RiftHunters.Core
{
    public interface IObserver
    {
    }

    public interface IObserver<in TEvent> : IObserver
    {
        void HandleEvent(TEvent @event);
    }
}
