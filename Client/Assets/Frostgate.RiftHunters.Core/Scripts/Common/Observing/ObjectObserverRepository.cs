using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Observing
{
    public sealed class ObjectObserverRepository : IDisposable
    {
        private readonly Dictionary<Type, IDisposable> _observers = new();

        public void Dispose()
        {
            foreach (IDisposable observer in _observers.Values)
                observer.Dispose();

            _observers.Clear();
        }

        public void Add<TObservable>([NotNull] ObjectObserver<TObservable> observer) where TObservable : class =>
            _observers[typeof(TObservable)] = observer;

        public bool Contains<TObservable>() where TObservable : class =>
            _observers.ContainsKey(typeof(TObservable));

        public ObjectObserver<TObservable> Get<TObservable>() where TObservable : class =>
            _observers[typeof(TObservable)] as ObjectObserver<TObservable>;

        public void Remove<TObservable>() where TObservable : class =>
            _observers.Remove(typeof(TObservable));
    }
}
