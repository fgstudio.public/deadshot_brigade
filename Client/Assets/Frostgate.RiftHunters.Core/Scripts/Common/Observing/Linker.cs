﻿namespace Frostgate.RiftHunters.Core
{
    public static class Linker<TEvent>
    {
        public static void Link<TObservable, TObserver>(TObservable observable, TObserver observer)
            where TObservable : IObservable<TEvent> where TObserver : IObserver<TEvent> =>
            observable.AddObserver(observer);

        public static void Unlink<TObservable, TObserver>(TObservable observable, TObserver observer)
            where TObservable : IObservable<TEvent> where TObserver : IObserver<TEvent> =>
            observable.RemoveObserver(observer);
    }
}
