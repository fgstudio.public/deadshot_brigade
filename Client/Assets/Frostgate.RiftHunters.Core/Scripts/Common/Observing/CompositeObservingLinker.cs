﻿using System;

namespace Frostgate.RiftHunters.Core
{
    public sealed class CompositeObservingLinker : IObservingLinker
    {
        private readonly IObservingLinker[] _linkers;

        public CompositeObservingLinker(params IObservingLinker[] linkers)
        {
            _linkers = linkers;
        }

        public void Link() => EachLinker(e => e.Link());

        public void Unlink() => EachLinker(e => e.Unlink());

        private void EachLinker(Action<IObservingLinker> each)
        {
            foreach (IObservingLinker linker in _linkers)
                each.Invoke(linker);
        }
    }
}
