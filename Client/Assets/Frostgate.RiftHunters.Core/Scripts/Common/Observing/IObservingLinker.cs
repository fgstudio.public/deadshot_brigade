﻿namespace Frostgate.RiftHunters.Core
{
    public interface IObservingLinker
    {
        void Link();
        void Unlink();
    }
}