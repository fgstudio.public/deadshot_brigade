﻿namespace Frostgate.RiftHunters.Core
{
    public sealed class ObservingLinker<TEvent> : IObservingLinker
    {
        private readonly IObservable<TEvent> _observable;
        private readonly IObserver<TEvent> _observer;

        public ObservingLinker(IObservable<TEvent> observable, IObserver<TEvent> observer)
        {
            _observable = observable;
            _observer = observer;
        }

        public void Link() => Linker<TEvent>.Link(_observable, _observer);
        public void Unlink() => Linker<TEvent>.Unlink(_observable, _observer);
    }
}
