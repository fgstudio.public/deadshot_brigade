﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    internal sealed class ObserverWrapper
    {
        public readonly IObserver Observer;

        public ObserverWrapper([NotNull] IObserver observer)
        {
            Observer = observer;
        }

        public bool TryHandleEvent<TEvent>(TEvent @event)
        {
            var observer = Observer as IObserver<TEvent>;
            observer?.HandleEvent(@event);

            return observer != null;
        }
    }
}
