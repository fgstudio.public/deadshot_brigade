using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Observing
{
    /// <summary>
    /// Класс, который позволяет наблюдать за заданным объектом и реагировать на изменения его данных.
    /// </summary>
    public abstract class ObjectObserver<TObservable> : IDisposable
        where TObservable : class
    {
        protected TObservable Observable { get; private set; }

        private bool _isDisposed;

        public void Dispose()
        {
            if (_isDisposed) return;

            RemoveObservable();
            OnDisposed();

            _isDisposed = true;
        }

        public void SetObservable([NotNull] TObservable observable)
        {
            RemoveObservable();

            Observable = observable;

            OnSet(observable);
            Subscribe(observable);
        }

        public void RemoveObservable()
        {
            if (Observable != null)
            {
                Unsubscribe(Observable);
                OnRemove(Observable);
                Observable = null;
            }
        }

        protected virtual void OnDisposed() { }
        protected virtual void OnSet(TObservable observable) { }
        protected virtual void OnRemove(TObservable observable) { }

        protected abstract void Subscribe(TObservable observable);
        protected abstract void Unsubscribe(TObservable observable);
    }
}
