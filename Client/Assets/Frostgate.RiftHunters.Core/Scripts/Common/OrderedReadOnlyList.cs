﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class OrderedReadOnlyList<T> : IReadOnlyList<T> where T : IOrdered
    {
        public int Count => _items.Length;
        public T this[int index] => _items[index];

        [NotNull] private readonly T[] _items;
        
        public OrderedReadOnlyList(T[] items, bool descending = false) : this((IEnumerable<T>)items, descending)
        {
        }

        public OrderedReadOnlyList(IEnumerable<T> items, bool descending = false)
        {
            IEnumerable<T> e = descending ? items.OrderByDescending(i => i.Order) : items.OrderBy(i => i.Order);
            _items = e.ToArray();
        }

        public IEnumerator<T> GetEnumerator() => ((IEnumerable<T>)_items).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}