﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public static class Area
    {
        public static Vector3 CircleBorderY(Vector3 center, float radius, int segmentNumber, int totalSegmentsNumber)
        {
            float angleBetweenSegments = 360f / totalSegmentsNumber;

            return CreateVector(center, angleBetweenSegments * segmentNumber, radius);
        }

        public static Vector3 CreateVector(Vector3 center, float angle, float magnitude)
        {
            Quaternion rotation = Quaternion.Euler(angle * Vector3.up);
            Vector3 offset = magnitude * (rotation * Vector3.forward);

            return center + offset;
        }
    }
}