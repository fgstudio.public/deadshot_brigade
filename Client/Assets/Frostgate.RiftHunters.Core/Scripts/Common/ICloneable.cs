namespace Frostgate.RiftHunters.Core
{
    public interface ICloneable<out T>
    {
        T Clone();
    }
}
