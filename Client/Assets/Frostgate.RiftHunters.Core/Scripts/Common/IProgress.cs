using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core
{
    public interface IProgress
    {
        event UnityAction Completed;
        event UnityAction Restored;
        event UnityAction Changed;
        float Ratio { get; }
        bool IsIncreased { get; }
    }
}