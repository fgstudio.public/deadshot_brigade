using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Reanimation
{
    public abstract class UILayout : MonoBehaviour
    {
        [Header("Dependencies")]
        [SerializeField, Required, ChildGameObjectsOnly] protected UISmoothActivator SmoothActivator;

        public bool IsActive => SmoothActivator.IsActive;

        public abstract void Activate();
        public abstract void Deactivate();
    }
}
