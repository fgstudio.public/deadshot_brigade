using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI.Reanimation;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e23a099eb137443b8a16bd7b023dc6d8")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIReanimationMediator))]
    public sealed class UIReanimationMediator : MonoBehaviour, ISmoothActivation
    {
        [Header("Dependencies")]
        [SerializeField, Required, ChildGameObjectsOnly] private UISmoothActivator _smoothActivator;

        [Header("Layouts")]
        [SerializeField, Required, ChildGameObjectsOnly] private UIWaitingLayout _waitingLayout;
        [SerializeField, Required, ChildGameObjectsOnly] private UIReanimationLayout _reanimationLayout;

        public UIWaitingLayout WaitingLayout => _waitingLayout;
        public ISmoothActivator SmoothActivator => _smoothActivator;
        public UIReanimationLayout ReanimationLayout => _reanimationLayout;

        private bool _isAutoRespawn;
        private IReadOnlyTimer _timer;

        private void OnValidate()
        {
            _smoothActivator ??= GetComponentInChildren<UISmoothActivator>();
            _waitingLayout ??= GetComponentInChildren<UIWaitingLayout>();
            _reanimationLayout ??= GetComponentInChildren<UIReanimationLayout>();
        }

        [ContextMenu(nameof(EnableReanimationMode))]
        public void EnableReanimationMode()
        {
            _waitingLayout.Deactivate();
            _reanimationLayout.Activate();
        }

        [ContextMenu(nameof(EnableWaitingMode))]
        public void EnableWaitingMode()
        {
            _reanimationLayout.Deactivate();
            _waitingLayout.Activate();

            if (_isAutoRespawn && _timer != null)
                _waitingLayout.ShowRespawnEnabled(_timer);
            else
                _waitingLayout.ShowRespawnDisabled();
        }

        public void SetReanimationTimer([CanBeNull] IReadOnlyTimer timer)
        {
            _timer = timer;

            if (_waitingLayout.IsActive && _isAutoRespawn && _timer != null)
                _waitingLayout.ShowRespawnEnabled(timer);
        }

        [ContextMenu(nameof(Hide))]
        public void Hide()
        {
            _waitingLayout.Deactivate();
            _reanimationLayout.Deactivate();
        }

        public void SetAutoRespawn(bool autoRespawn)
        {
            _isAutoRespawn = autoRespawn;
        }
    }
}