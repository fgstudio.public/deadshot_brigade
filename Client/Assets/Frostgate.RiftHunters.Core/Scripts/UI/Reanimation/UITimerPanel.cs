using TMPro;
using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.UI.Reanimation
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e23a099eb137443b8a16bd7b023dc6d8")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UITimerPanel))]
    public sealed class UITimerPanel : MonoBehaviour
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _minutes;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _seconds;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text[] _texts;

        [Header("Settings")]
        [SerializeField, Required] private string _timeFormat = "00";

        private IReadOnlyTimer _timer;

        private void OnValidate() =>
            _texts ??= GetComponentsInChildren<TMP_Text>();

        public void SetTimer([CanBeNull] IReadOnlyTimer timer)
        {
            if (_timer != null)
            {
                _timer.Ticked -= UpdateState;
                _timer = null;
            }

            if (timer != null)
            {
                timer.Ticked += UpdateState;
                _timer = timer;
            }

            SetVisible(timer != null);
        }

        public void SetColor(Color color)
        {
            foreach (TMP_Text text in _texts)
                text.color = color;
        }

        private void UpdateState()
        {
            TimeSpan left = _timer.Left;
            _minutes.text = left.Minutes.ToString(_timeFormat);
            _seconds.text = left.Seconds.ToString(_timeFormat);
        }

        private void SetVisible(bool isVisible)
        {
            foreach (var text in _texts)
            {
                text.enabled = isVisible;
            }
        }
    }
}