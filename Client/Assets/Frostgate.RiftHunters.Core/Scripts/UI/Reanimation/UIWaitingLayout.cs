using System;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Reanimation
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIWaitingLayout))]
    public sealed class UIWaitingLayout : UILayout
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _infoEnabledText;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _infoDisabledText;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _timerText;

        [Space(10)]
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _respawnEnabledElements;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _respawnDisabledElements;

        [SerializeField, FoldoutGroup("Message settings"), Required] private string _timeFormat = "00";
        [SerializeField, FoldoutGroup("Message settings")] private string _waitingRespawnMessage;
        [SerializeField, FoldoutGroup("Message settings")] private string _respawnDisableMessage;

        [Header("Backgrounds")]
        [SerializeField, Required] private UISmoothActivator _reanimationBackground;
        [SerializeField, Required] private UISmoothActivator _waitingBackground;

        private IReadOnlyTimer _respawnTimer;

        public override void Activate()
        {
            SmoothActivator.Activate();

            _waitingBackground.Activate();
            _reanimationBackground.Deactivate();

            _infoEnabledText.text = _waitingRespawnMessage;
            _infoDisabledText.text = _respawnDisableMessage;
        }

        public override void Deactivate()
        {
            SmoothActivator.Deactivate();
            if (_respawnTimer != null)
                _respawnTimer.Ticked -= OnTicked;
        }

        public void ShowRespawnDisabled()
        {
            _respawnDisabledElements.SetActive(true);
            _respawnEnabledElements.SetActive(false);
            _respawnTimer = null;
        }

        public void ShowRespawnEnabled(IReadOnlyTimer timer)
        {
            _respawnTimer = timer;

            _respawnDisabledElements.SetActive(false);
            _respawnEnabledElements.SetActive(true);

            if (_respawnTimer != null)
            {
                _respawnTimer.Ticked -= OnTicked;
                _respawnTimer.Stopped -= OnStopped;
                _respawnTimer = null;
            }

            if (timer != null)
            {
                timer.Ticked += OnTicked;
                timer.Stopped += OnStopped;
                _respawnTimer = timer;
            }
        }

        private void OnTicked()
        {
            TimeSpan left = _respawnTimer.Left;
            _timerText.text = left.Minutes.ToString(_timeFormat) + ":" + left.Seconds.ToString(_timeFormat);
        }

        private void OnStopped()
        {
            if (_respawnTimer == null)
                return;

            _respawnTimer.Ticked -= OnTicked;
            _respawnTimer.Stopped -= OnStopped;
            _respawnTimer = null;
        }
    }
}