using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.UI.Reanimation
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIReanimationLayout))]
    public sealed class UIReanimationLayout : UILayout
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Slider _progressSlider;

        [Header("Backgrounds")]
        [SerializeField, Required] private UISmoothActivator _reanimationBackground;
        [SerializeField, Required] private UISmoothActivator _waitingBackground;

        private IProgress _progress;

        public override void Activate() => SmoothActivator.Activate();

        public override void Deactivate() => SmoothActivator.Deactivate();

        public void SetProgress([NotNull] IProgress progress)
        {
            RemoveProgress();

            _progress = progress;
            progress.Changed += OnProgressChanged;
        }

        private void RemoveProgress()
        {
            if (_progress == null) return;

            _progress.Changed -= OnProgressChanged;
            _progress = null;
        }

        private void OnProgressChanged()
        {
            _progressSlider.value = _progress.Ratio;

            ChangeColorBackground(_progress.IsIncreased);
        }

        private void ChangeColorBackground(bool isReanimating)
        {
            if (!_reanimationBackground.IsActive && isReanimating)
            {
                _reanimationBackground.Activate();
                _waitingBackground.Deactivate();
            }
            else if (_reanimationBackground.IsActive && !isReanimating)
            {
                _reanimationBackground.Deactivate();
                _waitingBackground.Activate();
            }
        }
    }
}