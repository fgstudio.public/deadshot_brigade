using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public class UniFPSCounter : MonoBehaviour
    {
        private int _screenLongSide;
        private Rect _boxRect;
        private readonly GUIStyle _style = new();

        // for fps calculation.
        private int _frameCount;
        private float _elapsedTime;
        private double _frameRate;

        /// <summary>
        /// Initialization
        /// </summary>
        private void Awake() => UpdateUISize();

        /// <summary>
        /// Monitor changes in resolution and calcurate FPS
        /// </summary>
        private void Update()
        {
            // FPS calculation
            _frameCount++;
            _elapsedTime += Time.deltaTime;
            if (_elapsedTime > 0.5f)
            {
                _frameRate = Math.Round(_frameCount / _elapsedTime, 1, MidpointRounding.AwayFromZero);
                _frameCount = 0;
                _elapsedTime = 0;

                // Update the UI size if the resolution has changed
                if (_screenLongSide != Mathf.Max(Screen.width, Screen.height))
                {
                    UpdateUISize();
                }
            }
        }

        /// <summary>
        /// Resize the UI according to the screen resolution
        /// </summary>
        private void UpdateUISize()
        {
            _screenLongSide = Mathf.Max(Screen.width, Screen.height);
            var rectLongSide = _screenLongSide / 10;
            _boxRect = new Rect(190, 1, rectLongSide, rectLongSide / 3f);
            _style.fontSize = (int) (_screenLongSide / 36.8);
            _style.normal.textColor = Color.white;
        }

        /// <summary>
        /// Display FPS
        /// </summary>
        private void OnGUI()
        {
            GUI.Box(_boxRect, "");
            GUI.Label(_boxRect, " " + _frameRate + "fps", _style);
        }
    }
}
