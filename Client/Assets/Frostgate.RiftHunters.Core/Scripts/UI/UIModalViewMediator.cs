using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIModalViewMediator : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _title;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _greyButtonText;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _greenButtonText;
        [SerializeField, Required, ChildGameObjectsOnly] private Button _greyButton;
        [SerializeField, Required, ChildGameObjectsOnly] private Button _greenButton;
        [SerializeField, Required, ChildGameObjectsOnly] private UISmoothActivator _smoothActivator;

        private Action _onGreenButtonClickAction;
        private Action _onGreyButtonClickAction;

        public ISmoothActivator SmoothActivator => _smoothActivator;
        private void OnValidate()
        {
            _smoothActivator ??= GetComponentInChildren<UISmoothActivator>();
            _greyButton ??= GetComponentInChildren<Button>();
        }

        private void OnEnable() => Subscribe();

        private void OnDisable() => Unsubscribe();

        public void SetTitle(string title) => _title.text = title;

        public void SetupGreyButton(string buttonText, Action onButtonClickAction)
        {
            _greyButtonText.text = buttonText;
            _onGreyButtonClickAction = onButtonClickAction;
        }

        public void SetupGreenButton(string buttonText, Action onButtonClickAction)
        {
            _greenButtonText.text = buttonText;
            _onGreenButtonClickAction = onButtonClickAction;
        }

        private void Subscribe()
        {
            _greyButton.onClick.AddListener(OnGreyButtonClicked);
            _greenButton.onClick.AddListener(OnGreenButtonClicked);
        }

        private void Unsubscribe()
        {
            _greyButton.onClick.RemoveListener(OnGreyButtonClicked);
            _greenButton.onClick.RemoveListener(OnGreenButtonClicked);
        }

        private void OnGreyButtonClicked() => _onGreyButtonClickAction?.Invoke();

        private void OnGreenButtonClicked() =>  _onGreenButtonClickAction?.Invoke();
    }
}
