using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.UI;

namespace Frostgate.RiftHunters.Core.Battle.UI
{
    public interface IUIBossPhasesMediator : ISmoothActivation
    {
        public void Init(UnitNetworkState unitNetworkState);
        void Deinit();
    }
}