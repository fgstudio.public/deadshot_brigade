using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.UI;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.UI
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIBossPhasesMediator))]
    [DisallowMultipleComponent]
    public sealed class UIBossPhasesMediator : MonoBehaviour, IUIBossPhasesMediator
    {
        [SerializeField, Required] private UISmoothActivator _smoothActivator;
        [SerializeField, Required] private UISmoothSlider _progressSlider;
        [SerializeField, Required] private TMP_Text _phaseCounterText;
        [SerializeField, Required] private RisingAnimationComponent _risingAnimation;

        private UnitNetworkState _bossState;

        public ISmoothActivator SmoothActivator => _smoothActivator;

        public void Init(UnitNetworkState unitNetworkState)
        {
            _bossState = unitNetworkState;

            Subscribe();

            SetPhaseCounter(_bossState.UnitConfig.Phase);
            SetPhaseProgress(_bossState.Health / _bossState.MaxHealth);

            SmoothActivator.SetActive(true);
            _risingAnimation.Show();
        }

        public void Deinit()
        {
            OnBossDeath();
            _bossState = null;
        }

        private void Subscribe()
        {
            _bossState.HealthChanged += OnBossHealthChanged;
            _bossState.UnitConfigChanged += OnPhaseChange;
        }

        private void Unsubscribe()
        {
            _bossState.HealthChanged -= OnBossHealthChanged;
            _bossState.UnitConfigChanged -= OnPhaseChange;
        }

        private void OnPhaseChange(UnitConfig _, UnitConfig newConfig) =>
            SetPhaseCounter(newConfig.Phase);

        private void OnBossHealthChanged(float diff)
        {
            float value = _bossState.Health / _bossState.MaxHealth;

            SetPhaseProgress(value);

            if (_bossState.IsDead)
                OnBossDeath();
        }

        private void OnBossDeath()
        {
            _smoothActivator.SetActive(false);
            _risingAnimation.Hide();
            Unsubscribe();
        }

        private void SetPhaseCounter(int phaseCount) => _phaseCounterText.text = phaseCount.ToString();

        private void SetPhaseProgress(float progressValue) => _progressSlider.SetValue(progressValue);

        private int GetPhasesAmount(UnitNetworkState unitNetworkState)
        {
            int phasesAmount = 1;
            UnitPhaseConfig unitPhaseConfig = unitNetworkState.UnitConfig.UnitPhaseConfig;

            //вторая часть условия что бы избежать зацикливания если конфиги будут указывать друг на друга
            while (unitPhaseConfig && phasesAmount < 99)
            {
                phasesAmount++;
                unitPhaseConfig = unitPhaseConfig.UnitConfig.UnitPhaseConfig;
            }

            return phasesAmount;
        }
    }
}