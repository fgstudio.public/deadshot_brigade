using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Button))]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu(ComponentMenus.RiftHunters.UI.Menu + "/" + nameof(ButtonScaler))]
    public sealed class ButtonScaler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Button _button;
        [SerializeField, Required, ChildGameObjectsOnly] private RectTransform _rectTransform;

        [Header("Animation Settings")]
        [SerializeField] private Ease _ease = Ease.Linear;
        [SerializeField] private float _duration = 0.15f;
        [SerializeField] private Vector3 _targetScale = 0.95f * Vector3.one;

        private Vector3 _initialScale;
        private Tweener _tweener;

        private void OnValidate() => InitReferences();
        private void Awake() => InitReferences();
        private void OnDestroy() => StopTween();
        private void Start() => InitValues();

        private void InitReferences()
        {
            _button ??= GetComponent<Button>();
            _rectTransform ??= GetComponent<RectTransform>();
        }

        private void InitValues() =>
            _initialScale = _rectTransform.localScale;

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData) => HandlePointerEvent(_targetScale);
        void IPointerUpHandler.OnPointerUp(PointerEventData eventData) => HandlePointerEvent(_initialScale);

        private void HandlePointerEvent(Vector3 targetScale)
        {
            if (_button.interactable)
                PlayTween(targetScale, _duration);
        }

        private void PlayTween(Vector3 targetScale, float duration)
        {
            StopTween();
            _tweener = _rectTransform.DOScale(targetScale, duration).SetEase(_ease);
        }

        private void StopTween() => _tweener?.Kill();
    }
}
