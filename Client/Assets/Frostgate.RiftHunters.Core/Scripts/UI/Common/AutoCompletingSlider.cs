using System;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Slider))]
    [AddComponentMenu(ComponentMenus.RiftHunters.UI.Menu + "/" + nameof(AutoCompletingSlider))]
    public sealed class AutoCompletingSlider : MonoBehaviour, IProgress
    {
        [Header("References")]
        [SerializeField, Required, ReadOnly] private Slider _slider;

        [Header("Settings")]
        public float Speed;

        [field: SerializeField, FoldoutGroup("Event")] public UnityEvent Changed;
        [field: SerializeField, FoldoutGroup("Event")] public UnityEvent Completed;
        [field: SerializeField, FoldoutGroup("Event")] public UnityEvent Restored;

        public float Ratio => _slider.value;
        public bool IsIncreased { get; private set; }

        private Coroutine _routine;

        event UnityAction IProgress.Completed
        {
            add => Completed.AddListener(value);
            remove => Completed.RemoveListener(value);
        }
        event UnityAction IProgress.Restored
        {
            add => Restored.AddListener(value);
            remove => Restored.RemoveListener(value);
        }
        event UnityAction IProgress.Changed
        {
            add => Changed.AddListener(value);
            remove => Changed.RemoveListener(value);
        }

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() => _slider ??= GetComponent<Slider>();

        [ContextMenu(nameof(Reset))]
        public void Reset()
        {
            StopProgress();
            SetCurrentProgress(_slider.minValue);
        }

        [ContextMenu(nameof(StartIncreasingProgress))]
        public void StartIncreasingProgress()
        {
            StopProgress();
            _routine = StartCoroutine(ProgressRoutine(1, Completed));
        }

        [ContextMenu(nameof(StartDecreasingProgress))]
        public void StartDecreasingProgress()
        {
            StopProgress();
            _routine = StartCoroutine(ProgressRoutine(0, Restored));
        }

        [ContextMenu(nameof(StopProgress))]
        public void StopProgress()
        {
            if (_routine != null)
                StopCoroutine(_routine);

            _routine = null;
        }

        private IEnumerator ProgressRoutine(float targetValue, UnityEvent callBack = null)
        {
            IsIncreased = _slider.value < targetValue;
            while (Math.Abs(_slider.value - targetValue) > Speed * Time.deltaTime)
            {
                yield return null;
                if (IsIncreased)
                    SetCurrentProgress(_slider.value + Speed * Time.deltaTime);
                else
                    SetCurrentProgress(_slider.value - Speed * Time.deltaTime);
            }
            callBack?.Invoke();
        }

        private void SetCurrentProgress(float currentProgress)
        {
            _slider.value = Mathf.Clamp(currentProgress, _slider.minValue, _slider.maxValue);
            Changed?.Invoke();
        }
    }
}