using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI.Animations;

namespace Frostgate.RiftHunters.Core.UI
{
    /// <summary>
    /// Компонент, позволяющий запускать Fade и Unfade анимации.
    /// </summary>
    [AddComponentMenu(ComponentMenus.RiftHunters.UI.Menu + "/" + nameof(AnimatedFader))]
    public sealed class AnimatedFader : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private FadeAnimation _fadeAnimation;
        [SerializeField, Required, ChildGameObjectsOnly] private FadeAnimation _unfadeAnimation;

        private void Awake() => InitComponents();
        private void OnValidate() => InitComponents();
        private void InitComponents()
        {
            _fadeAnimation.Enable();
            _unfadeAnimation.Enable();
        }

        [ContextMenu(nameof(Fade))]
        public void Fade()
        {
            _fadeAnimation.Play();
            _unfadeAnimation.Stop();
        }

        [ContextMenu(nameof(FadeInstantly))]
        public void FadeInstantly()
        {
            _fadeAnimation.PlayDiscrete();
            _unfadeAnimation.Stop();
        }

        [ContextMenu(nameof(Unfade))]
        public void Unfade()
        {
            _fadeAnimation.Stop();
            _unfadeAnimation.Play();
        }

        [ContextMenu(nameof(UnfadeInstantly))]
        public void UnfadeInstantly()
        {
            _fadeAnimation.Stop();
            _unfadeAnimation.PlayDiscrete();
        }

        [ContextMenu(nameof(Stop))]
        public void Stop()
        {
            _fadeAnimation.Stop();
            _unfadeAnimation.Stop();
        }
    }
}
