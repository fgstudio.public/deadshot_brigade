using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{

    [CreateAssetMenu(fileName = "RelationsColorSettings", menuName = "Frostgate/Rift Hunters/Settings/RelationsColorSettings")]
    public class RelationsColorSettings : ScriptableObject
    {
        [Serializable]
        public class Relation
        {
            public Color Main;
            public Color Dark;
        }

        public Relation PlayerColors;
        public Relation TeammateColors;
        public Relation EnemyColors;
    }
}
