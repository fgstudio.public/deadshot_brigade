using System.Collections;
using DG.Tweening;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public interface IBulletsView
    {
        void Init(UnitNetworkState unitNetworkState);
    }

    public sealed class BulletsView : MonoBehaviour, IBulletsView
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _bulletsCounterText;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _reloadSpiner;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _infiniteIcon;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _reloadTimer;
        [SerializeField, Required, ChildGameObjectsOnly] private CanvasGroup _canvasGroup;

        [SerializeField, Required] private Color _highlightColor;
        [SerializeField, Required] private float _highlightScale;
        [SerializeField, Required] private float _highlightDuration;

        private UnitNetworkState _unitNetworkState;
        private int _maxBulletsValue;
        private float _weaponReloadingDuration = 1f;
        private Sequence _sequence;
        private bool _isReloading;
        private bool _isInfiniteAmmo;

        public void Init(UnitNetworkState unitNetworkState)
        {
            _unitNetworkState = unitNetworkState;
            RangeWeaponConfig rangeWeapon = unitNetworkState.PropertiesProvider?.RangeWeapon;

            if (rangeWeapon != null)
            {
                _maxBulletsValue = rangeWeapon.PatronsCount;
                _weaponReloadingDuration = rangeWeapon.ReloadSeconds;

                Subscribe(unitNetworkState);
                OnBulletsCountChanged();
            }
            else
            {
                _bulletsCounterText.gameObject.SetActive(false);
            }
        }

        private void OnDestroy() => Unsubscribe(_unitNetworkState);

        private void Subscribe(UnitNetworkState unitNetworkState)
        {
            if (unitNetworkState != null)
            {
                unitNetworkState.PatronsCountChanged += OnBulletsCountChanged;
                unitNetworkState.BehaviourStateChanged += OnBehaviourStateChanged;

                unitNetworkState.UnitNetwork.UnitNetworkEffects.Params.Changed += OnParamChanged;
            }
        }

        private void Unsubscribe(UnitNetworkState unitNetworkState)
        {
            if (unitNetworkState != null)
            {
                unitNetworkState.PatronsCountChanged -= OnBulletsCountChanged;
                unitNetworkState.BehaviourStateChanged -= OnBehaviourStateChanged;

                unitNetworkState.UnitNetwork.UnitNetworkEffects.Params.Changed -= OnParamChanged;
            }
        }

        private void OnBulletsCountChanged()
        {
            int patronsCount = _unitNetworkState.PatronsCount < 0 ? 0 : _unitNetworkState.PatronsCount;
            _bulletsCounterText.text = $"{patronsCount}/{_maxBulletsValue}";
        }

        private void OnBehaviourStateChanged()
        {
            if (_unitNetworkState.BehaviourState == BehaviourState.Reload)
            {
                StartCoroutine(AnimateReloading());
            }
            else if (_isReloading)
            {
                StopAllCoroutines();
                _sequence.Kill();
                SetReloading(false);
            }

            SetVisible(_unitNetworkState.BehaviourState != BehaviourState.Dead);
        }

        private void OnParamChanged()
        {
            var infiniteAmmo = _unitNetworkState.UnitNetwork.UnitNetworkEffects.Params.InfiniteAmmo;

            if (infiniteAmmo == _isInfiniteAmmo)
                return;

            if (_isReloading)
            {
                StopAllCoroutines();
                _sequence.Kill();
                SetReloading(false);
            }

            _isInfiniteAmmo = infiniteAmmo;

            _infiniteIcon.gameObject.SetActive(infiniteAmmo);
            _bulletsCounterText.gameObject.SetActive(!infiniteAmmo);
        }

        private IEnumerator AnimateReloading()
        {
            SetReloading(true);
            HighlightSpiner();

            float time = _weaponReloadingDuration;
            while (time > 0)
            {
                if(!_isReloading)
                    yield break;

                _reloadTimer.text = $"{time:F2}";

                time -= Time.deltaTime;
                yield return null;
            }

            SetReloading(false);
            HighlightBulletsCounter();
        }

        private void SetReloading(bool active)
        {
            _isReloading = active;
            _reloadSpiner.gameObject.SetActive(active);
            _reloadTimer.gameObject.SetActive(active);
            _bulletsCounterText.gameObject.SetActive(!active);
        }

        private void HighlightSpiner()
        {
            _sequence = DOTween.Sequence();
            _sequence.Append(_reloadSpiner.transform.DOScale(_highlightScale, _highlightDuration).SetLoops(2, LoopType.Yoyo))
                .Insert(0f, _reloadSpiner.DOColor(_highlightColor, _highlightDuration).SetLoops(2, LoopType.Yoyo));
            _sequence.Play();
        }

        private void HighlightBulletsCounter()
        {
            _sequence = DOTween.Sequence();
            _sequence.Append(_bulletsCounterText.transform.DOScale(_highlightScale, _highlightDuration).SetLoops(2, LoopType.Yoyo))
                .Insert(0f, _bulletsCounterText.DOColor(_highlightColor, _highlightDuration).SetLoops(2, LoopType.Yoyo));
            _sequence.Play();
        }

        private void SetVisible(bool visible) => _canvasGroup.alpha = visible ? 1 : 0;
    }
}