using System;
using System.Threading;
using UnityEngine;
using Sirenix.OdinInspector;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.UI.Animations
{
    public sealed class UnityAnimation : AnimationComponent
    {
        [SerializeField, Required, ChildGameObjectsOnly] private Animation _animation;

        private CancellationTokenSource _tokenSource;

        protected override void OnPlaying()
        {
            _animation.Play();
            StartWaiting();
        }

        protected override void OnPlayingInstantly() { }

        protected override void OnStopping()
        {
            _animation.Stop();
            StopWaiting();
        }

        private async void StartWaiting()
        {
            _tokenSource = new CancellationTokenSource();
            TimeSpan duration = TimeSpan.FromSeconds(_animation.clip.averageDuration);

            await UniTask.Delay(duration, DelayType.DeltaTime, PlayerLoopTiming.Update, _tokenSource.Token)
                .ContinueWith(Finished.Invoke);
        }

        private void StopWaiting()
        {
            if (_tokenSource != null)
            {
                _tokenSource.Cancel();
                _tokenSource.Dispose();
                _tokenSource = null;
            }
        }
    }
}
