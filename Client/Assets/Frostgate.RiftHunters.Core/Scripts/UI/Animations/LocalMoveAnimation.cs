using DG.Tweening;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Animations
{
    /// <summary>
    /// Компонент анимации смещения позиции для RectTransform.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Animations.Menu + "/" + nameof(LocalMoveAnimation))]
    public sealed class LocalMoveAnimation : AnimationComponent
    {
        private sealed class Cache
        {
            private Vector3? _position;
            public void CacheData(LocalMoveAnimation component) => _position = component._rectTransform.localPosition;
            public void RestoreData(LocalMoveAnimation component)
            {
                if (!_position.HasValue) return;
                component._rectTransform.localPosition = _position.Value;
                _position = null;
            }
        }

        private const float MinValue = 0;

        [Header("References")]
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private RectTransform _rectTransform;

        [Header("Settings")]
        [Min(MinValue)] public float SecondsDuration;
        [Min(MinValue)] public float SecondsDelay;
        public Vector3 Offset;

        private readonly Cache _cache = new Cache();
        private Tweener _tweener;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();

        [ContextMenu(nameof(InitReferences))]
        private void InitReferences() =>
            _rectTransform ??= GetComponent<RectTransform>();

        protected override void OnPlaying()
        {
            _cache.CacheData(this);
            _tweener = _rectTransform
                .DOLocalMove(CalcTargetPosition(), SecondsDuration)
                .SetDelay(SecondsDelay)
                .OnComplete(Finished.Invoke);
        }

        protected override void OnPlayingInstantly() =>
            _rectTransform.localPosition = CalcTargetPosition();

        protected override void OnStopping()
        {
            _tweener?.Kill();
            _cache.RestoreData(this);
        }

        private Vector3 CalcTargetPosition() =>
            _rectTransform.localPosition + Offset;
    }
}