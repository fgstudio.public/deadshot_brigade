using DG.Tweening;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Animations
{
    /// <summary>
    /// Компонент анимаци альфа-канала CanvasGroup.
    /// </summary>
    [RequireComponent(typeof(CanvasGroup))]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Animations.Menu + "/" + nameof(FadeAnimation))]
    public sealed class FadeAnimation : AnimationComponent
    {
        private sealed class Cache
        {
            private float _alpha;
            public void CacheData(FadeAnimation component) => _alpha = component._canvasGroup.alpha;
            public void RestoreData(FadeAnimation component) => component._canvasGroup.alpha = _alpha;
        }

        private const float MinValue = 0;
        private const float MaxValue = 1;

        [Header("References")]
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private CanvasGroup _canvasGroup;

        [Header("Settings")]
        [Min(MinValue)] public float SecondsDuration;
        [Min(MinValue)] public float SecondsDelay;
        [Range(MinValue, MaxValue)] public float Alpha;

        private readonly Cache _cache = new Cache();
        private Tweener _tweener;

        private void Awake()
        {
            InitReferences();
            _cache.CacheData(this);
        }
        private void OnValidate() => InitReferences();

        [ContextMenu(nameof(InitReferences))]
        private void InitReferences() =>
            _canvasGroup ??= GetComponent<CanvasGroup>();

        protected override void OnPlaying()
        {
            if (Mathf.Approximately(_canvasGroup.alpha, Alpha))
                return;

            _tweener = _canvasGroup
                .DOFade(Alpha, SecondsDuration)
                .SetDelay(SecondsDelay)
                .OnComplete(Finished.Invoke);
        }

        protected override void OnPlayingInstantly() =>
            _canvasGroup.alpha = Alpha;

        protected override void OnStopping()
        {
            if (_tweener != null)
            {
                _tweener.Kill();
                _tweener = null;

                _cache.RestoreData(this);
            }
        }
    }
}