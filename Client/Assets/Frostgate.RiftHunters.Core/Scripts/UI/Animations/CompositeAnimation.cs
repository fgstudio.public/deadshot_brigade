using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Animations
{
    /// <summary>
    /// Компонент, объединяющий несколько анимации
    /// и предоставляющий точку входа для коллекитивного запуска или остановки.
    /// </summary>
    [AddComponentMenu(BattleComponentMenus.Client.UI.Animations.Menu + "/" + nameof(CompositeAnimation))]
    public sealed partial class CompositeAnimation : AnimationComponent
    {
        [ValidateInput(nameof(AreAllAnimationsNotNull), NullError)]
        [ValidateInput(nameof(AreAllAnimationsUnique), DuplicateError)]
        [SerializeField, Required] private List<AnimationComponent> _animations;

        private IEnumerable<AnimationComponent> EnabledAnimations => _animations.Where(a => a.enabled);

        private int _counter;

        protected override void OnPlaying()
        {
            foreach (AnimationComponent animation in EnabledAnimations)
            {
                _counter++;

                animation.Play();

                animation.Stopped.AddListener(OnCompleted);
                animation.Finished.AddListener(OnCompleted);
            }
        }


        protected override void OnPlayingInstantly()
        {
            foreach (AnimationComponent animation in EnabledAnimations)
                animation.PlayDiscrete();
        }

        protected override void OnStopping()
        {
            _counter = default;

            foreach (AnimationComponent animation in _animations)
            {
                animation.Stopped.RemoveListener(OnCompleted);
                animation.Finished.RemoveListener(OnCompleted);

                animation.Stop();
            }
        }

        private void OnCompleted()
        {
            _counter--;

            if (_counter == 0)
                Finished?.Invoke();
        }
    }
}