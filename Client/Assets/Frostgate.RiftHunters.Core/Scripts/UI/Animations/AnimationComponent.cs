using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Animations
{
    /// <summary>
    /// Базовый компонент для реализации простой анимации.
    /// </summary>
    public abstract class AnimationComponent : Component
    {
        private const string EventsFoldoutName = "Animation Events";

        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Played { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Stopped { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Finished { get; private set; }

        [ContextMenu(nameof(Play))]
        public void Play()
        {
            if (this == null || !enabled) return;

            Stop();
            OnPlaying();
            Played.Invoke();
        }

        [ContextMenu(nameof(PlayDiscrete))]
        public void PlayDiscrete()
        {
            if (this == null || !enabled) return;

            Stop();
            OnPlayingInstantly();

            Played.Invoke();
            Finished.Invoke();
        }

        [ContextMenu(nameof(Stop))]
        public void Stop()
        {
            OnStopping();
            Stopped.Invoke();
        }

        protected abstract void OnPlaying();
        protected abstract void OnPlayingInstantly();
        protected abstract void OnStopping();

        protected override void OnDisabled() => Stop();
    }
}
