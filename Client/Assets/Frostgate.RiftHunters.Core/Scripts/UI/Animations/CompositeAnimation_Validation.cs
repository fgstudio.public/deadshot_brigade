using Frostgate.RiftHunters.Core.Utils;

namespace Frostgate.RiftHunters.Core.UI.Animations
{
    public partial class CompositeAnimation
    {
        private const string NullError = "Has null objects";
        private const string DuplicateError = "Has duplicate objects";
        private void OnValidate() =>
            RemoveNullAnimations();

        private void RemoveNullAnimations()
        {
            if (_animations == null) return;

            for (int i = _animations.Count - 1; i >= 0; i--)
                if (_animations[i] == null)
                    _animations.RemoveAt(i);
        }

        private bool AreAllAnimationsNotNull() =>
            _animations == null || _animations.Count == 0 || !Validator.HasNullElements(_animations);

        private bool AreAllAnimationsUnique() =>
            _animations == null || _animations.Count == 0 || !Validator.HasNullElements(_animations);
    }
}
