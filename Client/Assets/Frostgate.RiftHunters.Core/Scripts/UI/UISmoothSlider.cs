using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.Battle.UI
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UISmoothSlider))]
    [DisallowMultipleComponent]
    public sealed class UISmoothSlider : MonoBehaviour
    {
        [SerializeField, Required] private Slider _slider;
        [SerializeField] private float _transiteDuration;

        public void SetMaxValue(float value) => _slider.maxValue = value;

        public void SetValue(float current) => _slider.DOValue(current, _transiteDuration);
        public void SetValueForTime(float current, float time) => _slider.DOValue(current, time);

        private void OnValidate() => _slider ??= GetComponent<Slider>();
    }
}