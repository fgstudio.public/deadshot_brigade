using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Hud.SpecialProgressBars
{
    public interface ISpecialProgressBar
    {
        UnityEvent Shown { get; }
        UnityEvent Hidden { get; }
        bool IsShowing { get; }

        void Show();
        void Hide(bool instant = false);
    }

    [DisallowMultipleComponent]
    [RequireComponent(typeof(ProgressBar))]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(SpecialProgressBar))]
    public sealed class SpecialProgressBar : MonoBehaviour, ISpecialProgressBar
    {
        private const string EventsFoldoutGroup = "Events";

        [Header("References")]
        [Required, ChildGameObjectsOnly] public ProgressBar ProgressBar;
        [Required, ChildGameObjectsOnly] public UISmoothActivator SmoothActivator;

        [field: SerializeField, FoldoutGroup(EventsFoldoutGroup)] public UnityEvent Shown { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutGroup)] public UnityEvent Hidden { get; private set; }

        [Title("Info")]
        [ShowInInspector, ReadOnly] public bool IsShowing => gameObject.activeSelf;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() => ProgressBar ??= GetComponent<ProgressBar>();

        [Button]
        public void Show()
        {
            if (IsShowing) return;

            gameObject.SetActive(true);
            SmoothActivator.SetActive(true);
            Shown?.Invoke();
        }

        [Button]
        public void Hide(bool instant = false)
        {
            if (!IsShowing) return;

            if (instant)
                gameObject.SetActive(false);
            else
                SmoothActivator.SetActive(false, null, () => gameObject.SetActive(false));

            Hidden?.Invoke();
        }

        public void SetProgress(float value, float maxValue) =>
            ProgressBar.SetProgress(value, maxValue);

        public void StartProgress(float time) =>
            ProgressBar.StartProgress(time);
    }
}