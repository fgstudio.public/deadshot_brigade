using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Core.UI.Hud.SpecialProgressBars;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public interface ISpecialProgressBarsPanel
    {
        void SetObservableUnit([NotNull] UnitNetworkEffects unitUnitNetworkEffects, [NotNull] UnitNetworkState unitUnitNetworkState);
        void RemoveObservableUnit();
        void ShowProgressFor([NotNull] ITrap trap);
        void HideProgressFor([NotNull] ITrap trap);
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(SpecialProgressBarsPanel))]
    public sealed class SpecialProgressBarsPanel : MonoBehaviour, ISpecialProgressBarsPanel
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private SpecialProgressBar _stunProgressBar;
        [SerializeField, Required, ChildGameObjectsOnly] private TrapProgressBar _trapProgressBar;

        [Header("Settings")]
        [SerializeField, Min(0), SuffixLabel("sec", true)] private float _crusherStunDurationHardcode = 3f;

        private UnitNetworkEffects _unitUnitNetworkEffects;
        private UnitNetworkState _unitUnitNetworkState;

        private void OnValidate() =>
            InitReferences();

        private void Awake()
        {
            InitReferences();
            InitProgressBars();
        }

        private void OnDestroy()
        {
            if (_unitUnitNetworkState != null)
                UnsubscribeUnitState(_unitUnitNetworkState);
        }

        private void InitReferences()
        {
            _stunProgressBar ??= GetComponentInChildren<SpecialProgressBar>(true);
            _trapProgressBar ??= GetComponentInChildren<TrapProgressBar>(true);
        }

        private void InitProgressBars()
        {
            _stunProgressBar.gameObject.SetActive(false);
            _trapProgressBar.gameObject.SetActive(false);
        }

        // не учитывается ситуация, когда игрок находится в нескольких клетках
        public void ShowProgressFor(ITrap trap)
        {
            _trapProgressBar.SetTrap(trap);
            _trapProgressBar.Show();
        }

        // не учитывается ситуация, когда игрок находится в нескольких клетках
        public void HideProgressFor(ITrap trap)
        {
            _trapProgressBar.RemoveTrap();
            _trapProgressBar.Hide();
        }

        public void SetObservableUnit(UnitNetworkEffects unitUnitNetworkEffects, UnitNetworkState unitUnitNetworkState)
        {
            _unitUnitNetworkEffects = unitUnitNetworkEffects;
            _unitUnitNetworkState = unitUnitNetworkState;

            SubscribeUnitState(unitUnitNetworkState);
        }

        public void RemoveObservableUnit()
        {
            if (_unitUnitNetworkState != null)
                UnsubscribeUnitState(_unitUnitNetworkState);

            _unitUnitNetworkEffects = null;
            _unitUnitNetworkState = null;
        }

        private void SubscribeUnitState([NotNull] UnitNetworkState state) =>
            state.BehaviourStateChanged += OnUnitBehaviourStateChange;

        private void UnsubscribeUnitState([NotNull] UnitNetworkState state) =>
            state.BehaviourStateChanged -= OnUnitBehaviourStateChange;

        private void OnUnitBehaviourStateChange()
        {
            if (_unitUnitNetworkState.BehaviourState == BehaviourState.Dead)
            {
                InstantRemoveStunProgressBar();
                return;
            }

            if(_unitUnitNetworkState.BehaviourState != BehaviourState.Stun)
                return;

            foreach (var effect in _unitUnitNetworkEffects.States)
            {
                if (effect.Config.Type == UnitEffectTypes.Stun)
                {
                    AddStunProgressBar(effect.Config.Duration);
                    StartCoroutine(DisableProgressBarWithDelay(_stunProgressBar, effect.Config.Duration));
                    return;
                }
            }

            AddStunProgressBar(_crusherStunDurationHardcode);
            StartCoroutine(DisableProgressBarWithDelay(_stunProgressBar, _crusherStunDurationHardcode));
        }

        private void OnAddUnitEffect(UnitEffectState effectState)
        {
            if(effectState.Config.Type == UnitEffectTypes.Stun)
                AddStunProgressBar(effectState.Config.Duration);
        }

        private void OnRemoveUnitEffect(UnitEffectState effectState)
        {
            if(effectState.Config.Type == UnitEffectTypes.Stun)
                RemoveStunProgressBar();
        }

        private void AddStunProgressBar(float time)
        {
            _stunProgressBar.Show();
            _stunProgressBar.StartProgress(time);
        }

        private void RemoveStunProgressBar() => _stunProgressBar.Hide();
        private void InstantRemoveStunProgressBar() => _stunProgressBar.Hide(instant: true);

        private IEnumerator DisableProgressBarWithDelay(SpecialProgressBar progressBar, float time)
        {
            yield return new WaitForSeconds(time);
            progressBar.Hide();
        }
    }
}