using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;

namespace Frostgate.RiftHunters.Core.UI.Hud.SpecialProgressBars
{
    public interface ITrapProgressBar : ISpecialProgressBar
    {
        void SetTrap([NotNull] ITrap trap);
        void RemoveTrap();
    }

    [DisallowMultipleComponent]
    [RequireComponent(typeof(SpecialProgressBar))]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(TrapProgressBar))]
    public sealed class TrapProgressBar : MonoBehaviour, ITrapProgressBar
    {
        [Required, ChildGameObjectsOnly] public SpecialProgressBar ProgressBar;

        public UnityEvent Shown => ProgressBar.Shown;
        public UnityEvent Hidden => ProgressBar.Hidden;

        public bool IsShowing => ProgressBar.IsShowing;

        [CanBeNull] private ITrap _trap;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void OnDestroy() => RemoveTrap();
        private void InitReferences() => ProgressBar ??= GetComponent<SpecialProgressBar>();

        public void Show() => ProgressBar.Show();
        public void Hide(bool instant = false) => ProgressBar.Hide(instant);

        public void SetTrap(ITrap trap)
        {
            RemoveTrap();

            _trap = trap;
            Subscribe(trap);
            UpdateProgress(trap);
        }

        public void RemoveTrap()
        {
            if (_trap != null)
            {
                Unsubscribe(_trap);
                ResetProgress();
                _trap = null;
            }
        }

        private void Subscribe([NotNull] ITrap trap)
        {
            trap.Destroyed.AddListener(RemoveTrap);
            trap.State.HealthChanged += OnTrapHealthChanged;
        }

        private void Unsubscribe([NotNull] ITrap trap)
        {
            trap.Destroyed.RemoveListener(RemoveTrap);
            trap.State.HealthChanged -= OnTrapHealthChanged;
        }

        private void OnTrapHealthChanged(float diff) =>
            UpdateProgress(_trap!);

        private void ResetProgress() =>
            ProgressBar.SetProgress(default, default);

        private void UpdateProgress([NotNull] ITrap trap) =>
            ProgressBar.SetProgress(trap.State.Health, trap.State.MaxHealth);
    }
}