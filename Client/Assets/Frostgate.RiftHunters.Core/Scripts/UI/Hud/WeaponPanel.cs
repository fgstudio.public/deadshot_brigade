using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using UnityEngine.AddressableAssets;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public interface IWeaponPanel
    {
        void Init(UnitNetworkState unitNetworkState);
    }

    public class WeaponPanel : MonoBehaviour, IWeaponPanel
    {
        [SerializeField] private Image[] _gunImages;
        [SerializeField] private Slider _gunImageFiller;

        private UnitNetworkState _unitNetworkState;
        private float _weaponReloadingDuration = 1f;
        private Tweener _reloadingTween;

        public void Init(UnitNetworkState unitNetworkState)
        {
            Unsubscribe(_unitNetworkState);

            _unitNetworkState = unitNetworkState;
            RangeWeaponConfig rangeWeapon = _unitNetworkState.PropertiesProvider?.RangeWeapon;

            if (rangeWeapon != null)
            {
                _weaponReloadingDuration = rangeWeapon.ReloadSeconds;

                foreach (Image image in _gunImages)
                    image.sprite = rangeWeapon.Icon;

                Subscribe(unitNetworkState);
            }
        }

        private void OnDestroy()
        {
            // TODO-SG: Временная затычка
            Addressables.Release(_gunImages[0].sprite);
            Unsubscribe(_unitNetworkState);
        }

        private void Subscribe(UnitNetworkState unitNetworkState)
        {
            if (unitNetworkState != null)
                unitNetworkState.BehaviourStateChanged += OnBehaviourStateChanged;
        }

        private void Unsubscribe(UnitNetworkState unitNetworkState)
        {
            if (unitNetworkState != null)
                unitNetworkState.BehaviourStateChanged -= OnBehaviourStateChanged;
        }

        private void OnBehaviourStateChanged()
        {
            _reloadingTween?.Kill();

            if (_unitNetworkState.BehaviourState != BehaviourState.Reload)
                return;

            _gunImageFiller.value = 0;
            _reloadingTween = DOTween.To(() => _gunImageFiller.value, x => _gunImageFiller.value = x, 1f, _weaponReloadingDuration);
        }
    }
}