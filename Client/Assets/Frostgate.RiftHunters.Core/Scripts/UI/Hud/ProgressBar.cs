using TMPro;
using Sirenix.OdinInspector;
using UnityEngine;
using System.Collections;
using Frostgate.RiftHunters.Core.Battle.UI;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(ProgressBar))]
    public sealed class ProgressBar : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _progressText;
        [SerializeField, Required, ChildGameObjectsOnly] private UISmoothSlider _progressSlider;

        public void SetProgress(float value, float maxValue)
        {
            _progressSlider.SetValue(CalcProgress(value, maxValue));
            _progressText.text = $"{(int)value}/{(int)maxValue}";
            // лучше это контроллировать, конечно, на уровне вёрстки
            // и использовать в коде ссылки на два текстовых компонента.
        }

        public void StartProgress(float time)
        {
            _progressSlider.SetValueForTime(time, 0f);
            _progressSlider.SetValueForTime(0, time);
            StartCoroutine(StartTimer(time));
        }

        private IEnumerator StartTimer(float time)
        {
            while (time > 0)
            {
                _progressText.text = $"{(int)time} sec";
                yield return new WaitForSeconds(1f);
                time -= 1f;
            }
        }

        private float CalcProgress(float value, float maxValue) =>
            Mathf.Clamp01(value > 0 ? value / maxValue : 0);
    }
}