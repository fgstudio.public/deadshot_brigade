﻿using System;
using Mirror;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    [AddComponentMenu(ComponentMenus.RiftHunters.UI.Menu + "/" + nameof(NetworkRttDisplay))]
    [DisallowMultipleComponent]
    public sealed class NetworkRttDisplay : MonoBehaviour
    {
        [SerializeField, Required] private string _rttLabelFormat = "RTT: {0} ms";
        [SerializeField] private RttColor[] _rttColors = Array.Empty<RttColor>();
        [SerializeField] private LayoutSettings _layoutSettings = LayoutSettings.CreateDefault();

        private GUIStyle _style;

        [Button]
        private void Reset()
        {
            _layoutSettings = new();
            _rttColors = Array.Empty<RttColor>();
        }

        private void OnGUI()
        {
            if (!NetworkClient.active)
                return;

            if (_style == null)
            {
                _style = GUI.skin.GetStyle("Label");
                _style.alignment = TextAnchor.MiddleCenter;
            }

            DisplayRtt(Mathf.Round((float)NetworkTime.rtt * 1000));
        }

        private void DisplayRtt(float rtt)
        {
            GUI.color = GetRttColor(rtt);
            GUI.Label(CalcRect(), String.Format(_rttLabelFormat, rtt), _style);

            GUI.color = Color.white;
        }

        private Color GetRttColor(float rtt)
        {
            return _rttColors.TryGetNearest(rtt, rttC => rttC.Rtt, out RttColor rttColor)
                ? rttColor.Color
                : Color.white;
        }

        private Rect CalcRect()
        {
            Vector2 CalcPosition()
            {
                return _layoutSettings.BindAnchor switch
                {
                    LayoutSettings.Anchor.TopLeft => new Vector2(_layoutSettings.PaddingX, _layoutSettings.PaddingY),

                    LayoutSettings.Anchor.TopRight => new Vector2(
                        Screen.width - _layoutSettings.Width - _layoutSettings.PaddingX, _layoutSettings.PaddingY),

                    LayoutSettings.Anchor.BottomRight => new Vector2(
                        Screen.width - _layoutSettings.Width - _layoutSettings.PaddingX,
                        Screen.height - _layoutSettings.Height - _layoutSettings.PaddingY),

                    LayoutSettings.Anchor.BottomLeft => new Vector2(_layoutSettings.PaddingX,
                        Screen.height - _layoutSettings.Height - _layoutSettings.PaddingY),

                    _ => Vector2.zero
                };
            }

            Vector2 size = new Vector2(_layoutSettings.Width, _layoutSettings.Height);

            return new Rect(CalcPosition(), size);
        }

        #region Private structs

        [Serializable]
        private struct RttColor
        {
            [SerializeField, MinValue(0)] private float _rtt;
            [SerializeField] private Color _color;

            public float Rtt => _rtt;
            public Color Color => _color;
        }

        [Serializable]
        private struct LayoutSettings
        {
            [SerializeField, MinValue(0)] private int _width;
            [SerializeField, MinValue(0)] private int _height;

            [SerializeField] private Anchor _bindAnchor;

            [SerializeField] private int _paddingX;
            [SerializeField] private int _paddingY;

            public int Width => _width;
            public int Height => _height;

            public Anchor BindAnchor => _bindAnchor;

            public int PaddingX => _paddingX;
            public int PaddingY => _paddingY;

            public static LayoutSettings CreateDefault()
            {
                return new()
                {
                    _width = 90,
                    _height = 20,
                    _bindAnchor = Anchor.TopLeft
                };
            }

            public enum Anchor : byte
            {
                TopLeft,
                TopRight,
                BottomRight,
                BottomLeft
            }
        }

        #endregion
    }
}