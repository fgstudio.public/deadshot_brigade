using System.Diagnostics.CodeAnalysis;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI.Hud.Pointing;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public interface IAllyPanel
    {
        void SetTarget([NotNull] Transform target, Sprite roleIcon, bool pulsationIsOn = false, bool hasMessage = false);
        void RemoveTarget([NotNull] Transform target);
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/HUD-2a3d0156e3f142039d4bf1ee9d00a7b9")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Targeting.Menu + "/" + nameof(AllyPanel))]
    public sealed class AllyPanel : MonoBehaviour, IAllyPanel
    {
        [SerializeField, Required, ChildGameObjectsOnly] private MultiplePointerPanel _pointerPanel;
        [SerializeField] private Color _pointerColor;

        private void OnValidate() =>
            _pointerPanel ??= GetComponentInChildren<MultiplePointerPanel>();

        public void SetTarget(Transform target, Sprite roleIcon, bool pulsationIsOn = false, bool hasMessage = false) =>
            _pointerPanel.SetTarget(target, roleIcon, _pointerColor, pulsationIsOn, hasMessage);
        public void RemoveTarget(Transform target) => _pointerPanel.RemoveTarget(target);
    }
}