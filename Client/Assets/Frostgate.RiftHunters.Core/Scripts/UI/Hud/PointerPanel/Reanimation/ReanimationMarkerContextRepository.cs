using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Reanimation;

namespace Frostgate.RiftHunters.Core.UI.Hud.Pointing.Reanimation
{
    public sealed class ReanimationMarkerContextRepository : IDisposable
    {
        private readonly Dictionary<IReanimationMechanic, ReanimationTargetMarkerContext> _dictionary = new();

        public void Dispose()
        {
            foreach (ReanimationTargetMarkerContext context in _dictionary.Values)
                context.Dispose();

            _dictionary.Clear();
        }

        public bool HasContext([NotNull] IReanimationMechanic target) =>
            _dictionary.ContainsKey(target);

        public void CreateContext([NotNull] IReanimationMechanic target, [NotNull] PointerMarker marker, IReadOnlyTimer timer)
        {
            var context = new ReanimationTargetMarkerContext(target, marker, timer);
            context.Initialize();

            _dictionary[target] = context;
        }

        public void RemoveContext([NotNull] IReanimationMechanic target)
        {
            _dictionary[target].Dispose();
            _dictionary.Remove(target);
        }
    }
}