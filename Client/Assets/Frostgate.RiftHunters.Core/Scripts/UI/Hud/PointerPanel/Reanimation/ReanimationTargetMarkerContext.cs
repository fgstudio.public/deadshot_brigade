using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Reanimation;

namespace Frostgate.RiftHunters.Core.UI.Hud.Pointing.Reanimation
{
    public sealed class ReanimationTargetMarkerContext : IDisposable
    {
        private readonly PointerMarker _marker;
        private readonly IReanimationMechanic _target;
        private readonly ReanimatorMarkerWrapper _wrapper;

        public ReanimationTargetMarkerContext([NotNull] IReanimationMechanic target, [NotNull] PointerMarker marker, IReadOnlyTimer timer)
        {
            _target = target;
            _marker = marker;
            _wrapper = marker.GetComponent<ReanimatorMarkerWrapper>();
            _wrapper.SetTimer(timer);
        }

        public void Dispose() =>
            Unsubscribe();

        public void Initialize()
        {
            if (_target.IsActive) _wrapper.SetActiveState();
            else _wrapper.SetInactiveState();

            if (_target.IsEnabled) Subscribe();
        }

        private void Subscribe()
        {
            _target.Enabled.AddListener(Subscribe);
            _target.Disabled.AddListener(Unsubscribe);
            _target.Activated.AddListener(_wrapper.SetActiveState);
            _target.Deactivated.AddListener(_wrapper.SetInactiveState);
            _target.AreaOfInterest.VisitorRegistered.AddListener(OnVisitorRegistered);
            _target.AreaOfInterest.VisitorUnregistered.AddListener(OnVisitorUnregistered);
        }

        private void Unsubscribe()
        {
            _target.Enabled.RemoveListener(Subscribe);
            _target.Disabled.RemoveListener(Unsubscribe);
            _target.Activated.RemoveListener(_wrapper.SetActiveState);
            _target.Deactivated.RemoveListener(_wrapper.SetInactiveState);
            _target.AreaOfInterest.VisitorRegistered.RemoveListener(OnVisitorRegistered);
            _target.AreaOfInterest.VisitorUnregistered.RemoveListener(OnVisitorUnregistered);
        }

        private void OnVisitorRegistered(UnitNetwork unit)
        {
            if (IsLocalPlayer(unit))
                _marker.SmoothActivator.Deactivate();
        }

        private void OnVisitorUnregistered(UnitNetwork unit)
        {
            if (IsLocalPlayer(unit))
                _marker.SmoothActivator.Activate();
        }

        private static bool IsLocalPlayer(UnitNetwork unit) =>
            unit.Identity.isLocalPlayer;
    }
}