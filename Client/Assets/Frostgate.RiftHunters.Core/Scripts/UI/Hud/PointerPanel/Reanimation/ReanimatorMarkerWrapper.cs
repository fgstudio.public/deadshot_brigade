using System;
using Frostgate.RiftHunters.Core.UI.Reanimation;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.UI.Hud.Pointing.Reanimation
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/HUD-7b221ef73f7b411e85421bb940f84901")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Targeting.Menu + "/" + nameof(ReanimatorMarkerWrapper))]
    public sealed class ReanimatorMarkerWrapper : MonoBehaviour
    {
        [Serializable] private sealed class StateConfig
        {
            [field: SerializeField] public Color Color { get; private set; }
            [field: SerializeField] public Sprite Icon { get; private set; }
            [field: SerializeField] public bool AnimationIsOn { get; private set; }
        }

        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private PointerMarker _marker;
        [SerializeField, Required, ChildGameObjectsOnly] private UITimerPanel _timerPanel;

        [Header("Settings")]
        [SerializeField] private StateConfig _activeState;
        [SerializeField] private StateConfig _inactiveState;

        private void OnValidate()
        {
            _marker ??= GetComponentInChildren<PointerMarker>();
        }

        public void SetActiveState() => SetState(_activeState);
        public void SetInactiveState() => SetState(_inactiveState);
        public void SetTimer(IReadOnlyTimer timer) => _timerPanel.SetTimer(timer);

        private void SetState([NotNull] StateConfig config)
        {
            _marker.SetIcon(config.Icon);
            _marker.SetColor(config.Color);
            _timerPanel.SetColor(config.Color);

            if(config.AnimationIsOn)
                _marker.AnimatePulsation();
            else
                _marker.SetDefauilState();
        }
    }
}