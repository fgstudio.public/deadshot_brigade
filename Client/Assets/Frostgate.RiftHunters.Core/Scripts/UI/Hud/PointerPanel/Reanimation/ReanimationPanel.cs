using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI.Hud.Pointing;
using Frostgate.RiftHunters.Core.UI.Hud.Pointing.Reanimation;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Reanimation;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public interface IReanimationPanel
    {
        void SetTarget([NotNull] IReanimationMechanic target, IReadOnlyTimer timer = null);
        void RemoveTarget([NotNull] IReanimationMechanic target);
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/HUD-7b221ef73f7b411e85421bb940f84901")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Targeting.Menu + "/" + nameof(ReanimationPanel))]
    public sealed class ReanimationPanel : MonoBehaviour, IReanimationPanel
    {
        [SerializeField, Required, ChildGameObjectsOnly, ValidateInput(nameof(ValidatePanel))]
        private MultiplePointerPanel _pointerPanel;

        private readonly ReanimationMarkerContextRepository _contextRepository = new();

        private void OnValidate() =>
            _pointerPanel ??= GetComponentInChildren<MultiplePointerPanel>();

        private void OnDestroy() =>
            _contextRepository.Dispose();

        private bool ValidatePanel() =>
            _pointerPanel != null &&
            _pointerPanel.MarkerPool.Prefab.TryGetComponent(out ReanimatorMarkerWrapper _);

        public void SetTarget(IReanimationMechanic target, IReadOnlyTimer timer)
        {
            _pointerPanel.SetTarget(target.PointerPoint);
            PointerMarker marker = _pointerPanel.Markers[target.PointerPoint];

            if (!_contextRepository.HasContext(target))
                _contextRepository.CreateContext(target, marker, timer);
        }

        public void RemoveTarget(IReanimationMechanic target)
        {
            if (_contextRepository.HasContext(target))
                _contextRepository.RemoveContext(target);

            _pointerPanel.RemoveTarget(target.PointerPoint);
        }
    }
}