using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.UI.Hud.Pointing
{
    public interface ITotemPanel
    {
        void SetTarget([NotNull] Transform target);
        void RemoveTarget([NotNull] Transform target);
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Targeting.Menu + "/" + nameof(TotemPanel))]
    public sealed class TotemPanel : MonoBehaviour, ITotemPanel
    {
        [SerializeField, Required, ChildGameObjectsOnly] private MultiplePointerPanel _pointerPanel;

        private void OnValidate() =>
            _pointerPanel ??= GetComponentInChildren<MultiplePointerPanel>();

        public void SetTarget(Transform target) => _pointerPanel.SetTarget(target);
        public void RemoveTarget(Transform target) => _pointerPanel.RemoveTarget(target);

    }
}