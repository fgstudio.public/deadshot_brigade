using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Hud.Pointing
{
    public interface IPointerPanelRoster
    {
        IMultiplePointerPanel MultiplePointerPanel { get; }
        ITargetPanel TargetPanel { get; }
        IAllyPanel AllyPanel { get; }
        IReanimationPanel ReanimationPanel { get; }
        ITotemPanel TotemPanel { get; }
    }

    // TODO: Почему под каждую фичу нужно городить один и тот же код и новую панель?
    // Почему core функционал маркеров UI должен знать про цели, союзников, реанцимацию и т.д. и реализовывать для них интерфейсы?
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/HUD-2a3d0156e3f142039d4bf1ee9d00a7b9")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Targeting.Menu + "/" + nameof(PointerPanelRoster))]
    public sealed class PointerPanelRoster : MonoBehaviour, IPointerPanelRoster
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private MultiplePointerPanel _multiplePointerPanel;

        [SerializeField, Required, ChildGameObjectsOnly] private TargetPanel _targetPanel;
        [SerializeField, Required, ChildGameObjectsOnly] private AllyPanel _allyPanel;
        [SerializeField, Required, ChildGameObjectsOnly] private ReanimationPanel _reanimationPanel;
        [SerializeField, Required, ChildGameObjectsOnly] private TotemPanel _totemPanel;

        public IMultiplePointerPanel MultiplePointerPanel => _multiplePointerPanel;
        public ITargetPanel TargetPanel => _targetPanel;
        public IAllyPanel AllyPanel => _allyPanel;
        public IReanimationPanel ReanimationPanel => _reanimationPanel;
        public ITotemPanel TotemPanel => _totemPanel;

        private void OnValidate()
        {
            _targetPanel ??= GetComponentInChildren<TargetPanel>();
            _allyPanel ??= GetComponentInChildren<AllyPanel>();
            _reanimationPanel ??= GetComponentInChildren<ReanimationPanel>();
            _totemPanel ??= GetComponentInChildren<TotemPanel>();
        }
    }
}