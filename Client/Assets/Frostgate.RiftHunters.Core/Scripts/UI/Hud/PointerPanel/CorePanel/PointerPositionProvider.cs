using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.UI.Hud.Pointing
{
    public sealed class PointerPositionProvider
    {
        [CanBeNull] private Transform _target;
        private Vector3 _positionCache;
        private bool _isStatic;

        public bool HasTarget { get; private set; }
        public Vector3 Position => _isStatic
            ? _positionCache
            : _target != null ? _target.position : default;

        public void SetTarget([NotNull] Transform target)
        {
            HasTarget = true;

            _target = target;
            _positionCache = target.position;
            _isStatic = _target.gameObject.isStatic;
        }

        public void RemoveTarget()
        {
            HasTarget = false;

            _target = null;
            _isStatic = default;
            _positionCache = default;
        }
    }
}