using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Utils;

namespace Frostgate.RiftHunters.Core.UI.Hud.Pointing
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/HUD-2a3d0156e3f142039d4bf1ee9d00a7b9")]
    public abstract class PointerPanel : MonoBehaviour
    {
        protected const string ReferencesFoldoutName = "References";
        protected const string SettingsFoldoutName = "Settings";
        protected const string EventsFoldoutName = "Events";

        [FoldoutGroup(ReferencesFoldoutName), Required, ChildGameObjectsOnly] public CameraProvider CameraProvider;
        [FoldoutGroup(ReferencesFoldoutName), Required, ChildGameObjectsOnly] public PointerMarkerPool MarkerPool;

        [FoldoutGroup(SettingsFoldoutName)] public bool HideMarkerInViewport;
        [FoldoutGroup(SettingsFoldoutName), MinValue(0), MaxValue(1)] public Vector2 CenterPivot = Vector2.one / 2f;
        [FoldoutGroup(SettingsFoldoutName), MinValue(0)] public float NormalScaleMult = 1f;
        [FoldoutGroup(SettingsFoldoutName), MinValue(0)] public float LargerScaleMult = 1.4f;

        [FoldoutGroup(EventsFoldoutName)] public UnityEvent<PointerMarker> MarkerCreated;

        private readonly PointerPanelCache _cache = new();

        private void OnValidate()
        {
            CameraProvider ??= GetComponentInChildren<CameraProvider>();
            MarkerPool ??= GetComponentInChildren<PointerMarkerPool>();
        }

        private void OnEnable() => _cache.Update(this);

        private void Update()
        {
#if UNITY_EDITOR
            _cache.Update(this);
#endif
            OnUpdating();
        }

        protected abstract void OnUpdating();

        protected void UpdateMarker([NotNull] PointerMarker marker, [NotNull] PointerPositionProvider positionProvider)
        {
            Camera camera = CameraProvider.Camera;
            if (camera == null)
                return;

            Vector3 screenPoint = camera.WorldToScreenPoint(positionProvider.Position);
            bool isVisible = _cache.ScreenRect.Contains(screenPoint);

            var markerData = new PointerMarkerData(_cache, screenPoint, isVisible);

            marker.SetData(markerData);
            UpdateMarkerScale(marker, isVisible);
            UpdateMarkerVisibility(marker, isVisible);
        }

        [NotNull]
        protected PointerMarker CreateMarker()
        {
            PointerMarker targetMarker = MarkerPool.Get();
            targetMarker.SmoothActivator.Activate();
            MarkerCreated?.Invoke(targetMarker);

            return targetMarker;
        }

        protected void RemoveMarker([NotNull] PointerMarker marker) =>
            marker.SmoothActivator.Deactivate(
                onComplete: () => MarkerPool.Release(marker));

        private void UpdateMarkerVisibility(PointerMarker marker, bool isVisible)
        {
            if (HideMarkerInViewport)
                if (isVisible && marker.SmoothActivator.IsActive) marker.SmoothActivator.Deactivate();
                else if (!isVisible && !marker.SmoothActivator.IsActive) marker.SmoothActivator.Activate();
        }

        private void UpdateMarkerScale(PointerMarker marker, bool isVisible)
        {
            float scaleMult = isVisible ? NormalScaleMult : LargerScaleMult;
            marker.SetScaleMult(scaleMult);
        }
    }
}
