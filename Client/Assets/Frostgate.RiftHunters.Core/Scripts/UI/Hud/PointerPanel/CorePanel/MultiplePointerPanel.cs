using UnityEngine;
using UnityEngine.Pool;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.UI.Hud.Pointing
{
    public interface IMultiplePointerPanel
    {
        void SetTarget([NotNull] Transform target, Sprite icon = null, Color? color = null, bool pulsationIsOn = false, bool hasMessage = false);
        void RemoveTarget([NotNull] Transform target);
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/HUD-2a3d0156e3f142039d4bf1ee9d00a7b9")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Targeting.Menu + "/" + nameof(MultiplePointerPanel))]
    public sealed class MultiplePointerPanel : PointerPanel, IMultiplePointerPanel
    {
        [FoldoutGroup(SettingsFoldoutName), MinValue(0)] public int DefaultProviderPoolCapacity = 3;
        [FoldoutGroup(SettingsFoldoutName), MinValue(0)] public int MaxProviderPoolSize = 5;

        private readonly Dictionary<Transform, PointerPositionProvider> _positionProviders = new();
        private readonly Dictionary<Transform, PointerMarker> _markers = new();

        private ObjectPool<PointerPositionProvider> _positionProviderPoolCache;
        private IObjectPool<PointerPositionProvider> PositionProviderPool => _positionProviderPoolCache ??= CreateProviderPool();

        public IReadOnlyDictionary<Transform, PointerMarker> Markers => _markers;

        private void Awake() =>
            _positionProviderPoolCache = CreateProviderPool();

        private ObjectPool<PointerPositionProvider> CreateProviderPool() =>
            new(
                createFunc: () => new PointerPositionProvider(),
                actionOnRelease: p => p.RemoveTarget(),
                defaultCapacity: DefaultProviderPoolCapacity,
                maxSize: MaxProviderPoolSize
            );

        protected override void OnUpdating()
        {
            foreach (var kv in _positionProviders)
                if (kv.Value.HasTarget)
                    UpdateMarkerForTarget(kv.Key);
        }

        private void UpdateMarkerForTarget(Transform target)
        {
            PointerPositionProvider positionProvider = _positionProviders[target];
            PointerMarker marker = _markers[target];

            if (positionProvider.HasTarget)
                UpdateMarker(marker, positionProvider);
        }

        public void SetTarget([NotNull] Transform target, Sprite icon = null, Color? color = null, bool pulsationIsOn = false, bool hasMessage = false)
        {
            if (!_positionProviders.ContainsKey(target))
            {
                PointerPositionProvider positionProvider = PositionProviderPool.Get();
                _positionProviders[target] = positionProvider;
                positionProvider.SetTarget(target);
            }

            if (!_markers.ContainsKey(target))
                _markers[target] = CreateMarker();

            if (!_markers.TryGetValue(target, out PointerMarker marker))
            {
                marker = CreateMarker();
                _markers[target] = marker;
            }

            if(icon != null)
                marker.SetIcon(icon);

            if (color != null)
                marker.SetColor(color.Value);

            if(hasMessage)
                marker.AnimateMessage();

            if(pulsationIsOn)
                marker.AnimatePulsation();
            else
                marker.SetDefauilState();
        }

        public void RemoveTarget([NotNull] Transform target)
        {
            if (_positionProviders.TryGetValue(target, out PointerPositionProvider positionProvider))
            {
                _positionProviders.Remove(target);
                PositionProviderPool.Release(positionProvider);
            }

            if (_markers.TryGetValue(target, out PointerMarker marker))
            {
                RemoveMarker(marker);
                _markers.Remove(target);
            }
        }
    }
}