using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI.Hud.Pointing;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/HUD-2a3d0156e3f142039d4bf1ee9d00a7b9")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Targeting.Menu + "/" + nameof(SinglePointerPanel))]
    public sealed class SinglePointerPanel : PointerPanel
    {
        private readonly PointerPositionProvider _positionProvider = new();

        private PointerMarker _marker;
        [CanBeNull] public PointerMarker Marker => _marker;

        protected override void OnUpdating()
        {
            if (_positionProvider.HasTarget)
                UpdateMarker(_marker, _positionProvider);
        }

        public void SetTarget([NotNull] Transform target)
        {
            _positionProvider.SetTarget(target);

            if (_marker) RemoveMarker(_marker);
            _marker = CreateMarker();
        }

        public void RemoveTarget()
        {
            _positionProvider.RemoveTarget();

            if (_marker) RemoveMarker(_marker);
            _marker = null;
        }
    }
}