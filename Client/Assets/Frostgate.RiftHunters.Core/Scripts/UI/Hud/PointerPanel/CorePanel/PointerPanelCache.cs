using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.UI.Hud.Pointing
{
    public interface IReadOnlyTargetPanelCache
    {
        Rect ScreenRect { get; }
        Vector2 PivotPoint { get; }
    }

    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/HUD-2a3d0156e3f142039d4bf1ee9d00a7b9"/>
    /// </summary>
    public sealed class PointerPanelCache : IReadOnlyTargetPanelCache
    {
        public Rect ScreenRect { get; private set; }
        public Vector2 PivotPoint { get; private set; }

        public void Update([NotNull] PointerPanel panel)
        {
            Camera camera = panel.CameraProvider.Camera;

            var size = camera != null
                ? new Vector2(camera.scaledPixelWidth, camera.scaledPixelHeight)
                : Vector2.zero;

            ScreenRect = new Rect(Vector2.zero, size);
            PivotPoint = Vector2.Scale(panel.CenterPivot, ScreenRect.size);
        }
    }
}
