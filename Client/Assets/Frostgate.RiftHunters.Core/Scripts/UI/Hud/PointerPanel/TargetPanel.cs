using UnityEngine;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI.Hud.Pointing;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public interface ITargetPanel
    {
        void SetTarget([NotNull] Transform target, [CanBeNull] Sprite icon, Color backgroundColor, bool animationIsOn);
        void RemoveTarget([NotNull] Transform target);
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/HUD-2a3d0156e3f142039d4bf1ee9d00a7b9")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Targeting.Menu + "/" + nameof(TargetPanel))]
    public sealed class TargetPanel : MonoBehaviour, ITargetPanel
    {
        [SerializeField, Required, ChildGameObjectsOnly] private MultiplePointerPanel _pointerPanel;

        private Sprite _markerIcon;

        private void OnValidate() =>
            _pointerPanel ??= GetComponentInChildren<MultiplePointerPanel>();

        private void Start() =>
            _pointerPanel.MarkerCreated.AddListener(OnMarkerCreated);

        private void OnDestroy() =>
            _pointerPanel.MarkerCreated.RemoveListener(OnMarkerCreated);

        private void OnMarkerCreated(PointerMarker marker) =>
            marker.SetIcon(_markerIcon);

        public void SetTarget(Transform target, Sprite icon, Color backgroundColor, bool animationIsOn) =>
            _pointerPanel.SetTarget(target, icon, backgroundColor, animationIsOn);

        public void RemoveTarget(Transform target) =>
            _pointerPanel.RemoveTarget(target);
    }
}