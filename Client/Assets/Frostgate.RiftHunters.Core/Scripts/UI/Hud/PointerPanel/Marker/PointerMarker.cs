using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.UI.Hud.Pointing
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/HUD-2a3d0156e3f142039d4bf1ee9d00a7b9")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Targeting.Menu + "/" + nameof(PointerMarker))]
    public sealed class PointerMarker : MonoBehaviour, IPoolObject, ISmoothActivation
    {
        public static int Pulsation { get; private set; } = Animator.StringToHash(nameof(Pulsation));
        public static int Message { get; private set; } = Animator.StringToHash(nameof(Message));
        public static int DefaultState { get; private set; } = Animator.StringToHash(nameof(DefaultState));

        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private UISmoothActivator _smoothActivator;
        [SerializeField, Required, ChildGameObjectsOnly] private RectTransform _rectTransform;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _icon;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _background;
        [SerializeField, Required, ChildGameObjectsOnly] private Animator _animator;
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _transform;

        [SerializeField, ChildGameObjectsOnly] private List<Image> _vfxLayout;

        [Header("Info")]
        [ShowInInspector, ReadOnly] private Color _defaultColor;

        private Vector3? _scaleCache;

        public ISmoothActivator SmoothActivator => _smoothActivator;

        private void OnValidate()
        {
            _rectTransform ??= GetComponentInChildren<RectTransform>();
            _defaultColor = _background.color;
        }

        private void Start() =>
            CacheScaleIfNotCached();

        public void SetIcon([CanBeNull] Sprite icon) =>
            _icon.sprite = icon;

        public void SetColor(Color color) =>
            _background.color = color;

        public void AnimatePulsation() => _animator.SetTrigger(Pulsation);

        public void AnimateMessage() => _animator.SetTrigger(Message);
        public void SetDefauilState() => _animator.SetTrigger(DefaultState);

        public void SetData(PointerMarkerData data)
        {
            SetPosition(data.Position);
            SetRotation(data.Rotation);
        }

        public void SetPosition(Vector3 position) =>
            _rectTransform.position = position;

        public void SetRotation(Quaternion rotation) =>
            _rectTransform.rotation = rotation;

        public void SetScaleMult(float scaleMult) =>
            _transform.localScale = scaleMult * GetCachedScale();

        void IPoolObject.OnGet() { }

        void IPoolObject.OnRelease()
        {
            SetColor(_defaultColor);
            _vfxLayout.ForEach(t => t.SetAlpha(0));
            _background.SetAlpha(1);
            ResetScaleFromCache();
        }

        private void CacheScaleIfNotCached() =>
            _scaleCache ??= _transform.localScale;

        private Vector3 GetCachedScale()
        {
            CacheScaleIfNotCached();
            return _scaleCache!.Value;
        }

        private void ResetScaleFromCache()
        {
            if (_scaleCache.HasValue)
                _transform.localScale = _scaleCache.Value;
        }
    }
}