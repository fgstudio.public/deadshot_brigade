using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Hud.Pointing
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/HUD-2a3d0156e3f142039d4bf1ee9d00a7b9"/>
    /// </summary>
    public readonly struct PointerMarkerData
    {
        public readonly Vector2 Position;
        public readonly Quaternion Rotation;

        public PointerMarkerData(IReadOnlyTargetPanelCache targetPanelCache, Vector3 targetScreenPoint, bool isTargetInViewport) : this()
        {
            Position = CalcPosition(targetPanelCache.ScreenRect, targetScreenPoint, isTargetInViewport);
            Rotation = CalcRotation(targetPanelCache.PivotPoint, Position, isTargetInViewport);
        }

        private Vector2 CalcPosition(Rect screenRect, Vector3 targetScreenPoint, bool isTargetInViewport)
        {
            Vector2 markerPosition = targetScreenPoint;

            if (targetScreenPoint.z < 0)
                markerPosition *= -1;

            if (!isTargetInViewport)
            {
                markerPosition.x = Mathf.Clamp(markerPosition.x, screenRect.xMin, screenRect.xMax);
                markerPosition.y = Mathf.Clamp(markerPosition.y, screenRect.yMin, screenRect.yMax);
            }

            return markerPosition;
        }

        private Quaternion CalcRotation(Vector2 pivotPoint, Vector2 markerPosition, bool isTargetInViewport)
        {
            float angle = isTargetInViewport ? 0 : Vector2.SignedAngle(Vector2.up, pivotPoint - markerPosition);
            return Quaternion.Euler(Vector3.forward * angle);
        }
    }
}
