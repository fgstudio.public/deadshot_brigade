using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.UI.Hud.Pointing
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/HUD-2a3d0156e3f142039d4bf1ee9d00a7b9")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Targeting.Menu + "/" + nameof(PointerMarkerPool))]
    public sealed class PointerMarkerPool : MonoPoolComponent<PointerMarker>
    {
        [Required, AssetsOnly] public PointerMarker Prefab;

        protected override GameObject GetPrefab() => Prefab.gameObject;
        protected override PointerMarker CreateObject(GameObject prefab) => Instantiate(prefab).GetComponent<PointerMarker>();
        protected override void DestroyObject(PointerMarker poolObject) => Destroy(poolObject.gameObject);
    }
}