using System;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public interface IHeroAvatar
    {
        void Init(UnitNetworkState state);
    }

    // TODO: Разделить на две сущности: игровую и статичную
    public class HeroAvatar : MonoBehaviour, IHeroAvatar
    {
        [SerializeField, ChildGameObjectsOnly] private Image _heroImage;
        [SerializeField, ChildGameObjectsOnly] private Bar _healthBar;

        private UnitNetworkState _state;

        public void Init(UnitNetworkState state)
        {
            _state = state;
            _healthBar.Set(_state.Health, _state.MaxHealth);
            _heroImage.sprite = _state.UnitConfig.Sprite;

            state.HealthChanged += OnUnitHealthChanged;
            state.HealthState.MaxHealthChanged += OnUnitMaxHealthChanged;
        }

        public void Init(UnitConfig unitConfig) =>
            _heroImage.sprite = unitConfig.Sprite;

        private void OnUnitHealthChanged(float value) => _healthBar.SetCurrent(_state.Health);

        private void OnUnitMaxHealthChanged(float oldValue, float newValue) => _healthBar.SetMax(newValue);
    }
}