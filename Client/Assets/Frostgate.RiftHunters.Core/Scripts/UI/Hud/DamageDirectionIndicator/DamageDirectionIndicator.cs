using System;
using System.Collections.Generic;
using Zenject;
using DG.Tweening;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using Frostgate.RiftHunters.Core.Battle.Client;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public sealed class DamageDirectionIndicator : MonoBehaviour
    {
        private static readonly Color defaultColor = new(1f, 1f, 1f, 0f);

        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Image _left;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _forward;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _right;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _back;

        [Header("Settings")]
        [SerializeField] private float _appearTime = 0.1f;
        [SerializeField] private float _disappearTime = 0.5f;

        [Tooltip("Angle 0 point to forward, angle 180 and -180 point to back. (0,-180) - left side, (0, 180) - right side")]
        [SerializeField, Range(0, 180)] private float _forwardAngle = 45;
        [SerializeField, Range(0, 180)] private float _backAngle = 135;

        private readonly Dictionary<Image, Sequence> _animations = new();
        [CanBeNull] private BattleCamera _battleCamera;

        [Inject]
        private void MonoConstructor([InjectOptional] BattleCamera battleCamera)
        {
            _battleCamera = battleCamera;
        }

        private void OnEnable() => Clear();

        public void Indicate([NotNull] Transform casterTransform, [NotNull] Transform receiverTransform)
        {
            float angle = GetAngle(casterTransform, receiverTransform);

            if(Math.Abs(angle) < _forwardAngle)
                PlayIndicateAnimation(_forward);

            else if(Math.Abs(angle) > _backAngle)
                PlayIndicateAnimation(_back);

            else if(angle < 0)
                PlayIndicateAnimation(_left);

            else
                PlayIndicateAnimation(_right);
        }

        private void Clear()
        {
            _left.color = defaultColor;
            _forward.color = defaultColor;
            _right.color = defaultColor;
            _back.color = defaultColor;
        }

        private float GetAngle([NotNull] Transform casterTransform, [NotNull] Transform receiverTransform)
        {
            Vector3 casterPosition = casterTransform.position
                .SetY(receiverTransform.position.y);

            Vector3 forward = _battleCamera != null
                ? _battleCamera.ForwardXZ.AsVector3WithY(0) : receiverTransform.forward;
            Vector3 casterDir = casterPosition - receiverTransform.position;

            return -Vector3.SignedAngle(casterDir, forward, Vector3.up);
        }

        private void PlayIndicateAnimation(Image image)
        {
            StopAnimation(image);

            _animations[image] = DOTween.Sequence()
                .Append(image.DOFade(1, _appearTime))
                .Append(image.DOFade(0, _disappearTime))
                .Play();
        }

        private void StopAnimation(Image image)
        {
            if (_animations.TryGetValue(image, out Sequence sequence))
                sequence.Kill();
        }
    }
}