using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public sealed class UIActivityPopUp : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private CanvasGroup _canvasGroup;
        private Sequence _sequence;

        [Header("Settings")]
        [SerializeField, Range(0, 5f)] private float emergenceTime = 1f;
        [SerializeField, Range(0, 5f)] private float liveTime = 1.5f;
        [SerializeField, Range(0, 5f)] private float hideTime = 1f;

        public void PlayMovingTo(Vector3 endPosition)
        {
            _sequence?.Kill();

            _sequence = DOTween.Sequence();

            _sequence.Append(transform.DOMove(endPosition, hideTime))
                .Insert(hideTime / 2f, transform.DOScale(0f, hideTime))
                .Play();
        }

        public void PlayHiding()
        {
            ResetState();
            _sequence?.Kill();

            _sequence = DOTween.Sequence();

            _sequence.Append(_canvasGroup.DOFade(1f, emergenceTime))
                .Insert(emergenceTime + liveTime, _canvasGroup.DOFade(0f, hideTime))
                .Play();
        }

        private void ResetState()
        {
            _canvasGroup.alpha = 0;
            transform.localPosition = Vector3.zero;
            transform.localScale = Vector3.one;
        }
    }
}
