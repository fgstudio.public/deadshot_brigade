using Frostgate.RiftHunters.Core.Battle.Client.Activity;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using DG.Tweening;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using UnityEngine;
using TMPro;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public sealed class UIActivityDescriptionPopUp : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _title;
        [SerializeField, Required, ChildGameObjectsOnly] private CanvasGroup _canvasGroup;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _glow;
        [SerializeField, Required] private UIActivityPopUpGoal _popUpGoalPrefab;

        [Header("Settings")]
        [SerializeField, Range(0, 5f)] private float emergenceTime = 1f;
        [SerializeField, Range(0, 5f)] private float liveTime = 3f;
        [SerializeField, Range(0, 5f)] private float hideTime = 1f;

        private List<UIActivityPopUpGoal> _missionSubObjectives;
        private Sequence _sequence;

        public void Show(ActivityDescription description, Vector3 endPosition)
        {
            SetMissionDescription(description);
            Play(endPosition);
        }
        
        private void OnEnable() => ResetState();

        private void ResetState()
        {
            _canvasGroup.alpha = 0;
            transform.localPosition = Vector3.zero;
            transform.localScale = Vector3.one;
            _glow.color = Color.white;
        }

        private void Play(Vector3 endPosition)
        {
            ResetState();
            _sequence?.Kill();

            _sequence = DOTween.Sequence();

            _sequence.Append(_canvasGroup.DOFade(1f, emergenceTime))
                .Insert(emergenceTime, _glow.DOFade(0, liveTime))
                .Insert(emergenceTime + liveTime, transform.DOMove(endPosition, hideTime))
                .Insert(emergenceTime + liveTime + (hideTime / 3f), _canvasGroup.DOFade(1f, hideTime))
                .Insert(emergenceTime + liveTime + (hideTime / 3f), transform.DOScale(0f, hideTime))
                .InsertCallback(emergenceTime + liveTime + hideTime, ClearSubObjectives)
                .Play();
        }

        private void SetMissionDescription(ActivityDescription description)
        {
            _missionSubObjectives ??= new List<UIActivityPopUpGoal>();
            
            _title.text = description.Title;
            foreach (ActivityGoal goal in description.Goals)
                AddSubObjective(goal.Title, goal.EnableMarker, goal.MarkerBgColor, goal.MarkerSprite);
        }
        
        private void AddSubObjective(string text, bool markerOn, Color markerBackgroundColor, Sprite markerIcon)
        {
            var subObjective = Instantiate(_popUpGoalPrefab, transform);

            subObjective.gameObject.SetActive(true);
            _missionSubObjectives.Add(subObjective);

            if (markerOn)
                subObjective.Set(text, markerBackgroundColor, markerIcon);
            else
                subObjective.Set(text);
        }

        private void ClearSubObjectives()
        {
            foreach (var subObjective in _missionSubObjectives)
                Destroy(subObjective.gameObject);

            _missionSubObjectives.Clear();
        }
    }
}
