﻿using Frostgate.RiftHunters.Core.Battle.Client.Activity;
using Sirenix.OdinInspector;
using UnityEngine;
using System;
using DG.Tweening;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    [RequireComponent(typeof(RectTransform))]
    public class UIActivityDescriptionPanel : MonoBehaviour, IUIBarPanel<IUIActivityPanelItem>
    {
        [SerializeField, Required] private UIActivityDescriptionPopUp _activityDescriptionPopUp;
        [SerializeField, Required] private UIActivityPopUp _activityGoalPopUp;
        [SerializeField, Required] private UIActivityPopUpGoal _missionSubObjective;
        [SerializeField, Required] private Transform _noticeStartPoint;
        [SerializeField, Required] private Transform _noticeEndPoint;

        [SerializeField, FoldoutGroup("Popup settings")] private float _startMovingTime = 1.5f;
        [SerializeField, FoldoutGroup("Popup settings")] private float _moveTime = 1f;
        [SerializeField, FoldoutGroup("Popup settings")] private float _startScale = 0.6f;
        [SerializeField, FoldoutGroup("Popup settings")] private float _endScale = 0f;

        public GameObject GameObject => NativeBarPanel.GameObject;
        public RectTransform Transform => NativeBarPanel.Transform;

        private UIBarPanel<IUIActivityPanelItem> NativeBarPanel => _nativeBarPanelCache ??= CreateNativeBarPanel();

        private UIBarPanel<IUIActivityPanelItem> _nativeBarPanelCache;

        public void PlayGoalPopUp(ActivityGoal goal)
        {
            _missionSubObjective.Set(goal.Title, goal.MarkerBgColor, goal.MarkerSprite);
            _activityGoalPopUp.PlayHiding();

            var popUpTransform = _missionSubObjective.transform;
            popUpTransform.position = _noticeStartPoint.position;
            popUpTransform.localScale = new Vector3(_startScale, _startScale, _startScale);
            var sequence = DOTween.Sequence();
            sequence.Append(popUpTransform.DOScale(1.1f, 0.5f))
                .Insert(0.5f, popUpTransform.DOScale(1f, 0.5f))
                .Insert(_startMovingTime, popUpTransform.DOMove(_noticeEndPoint.position, _moveTime))
                .Insert(_startMovingTime, popUpTransform.DOScale(_endScale, _moveTime))
                .Play();
        }

        public void Add(IUIActivityPanelItem item)
        {
            _activityDescriptionPopUp.Show(item.BuildDescription(), _noticeEndPoint.position);
            NativeBarPanel.Add(item);
        }

        public void Remove(IUIActivityPanelItem item)
        {
            NativeBarPanel.Remove(item);
        }

        public void Remove(IUIActivityPanelItem item, Action removedCallback)
        {
            NativeBarPanel.Remove(item, removedCallback);
        }

        public bool Contains(IUIActivityPanelItem item) => NativeBarPanel.Contains(item);

        private void OnSkipChangingActivityGoal() =>
            _activityGoalPopUp.PlayMovingTo(_noticeEndPoint.position);

        private UIBarPanel<IUIActivityPanelItem> CreateNativeBarPanel() =>
            new(new UIPanel<IUIActivityPanelItem>((RectTransform)transform));

        [SerializeField, Required] private ActivityGoal _testActivityGoal;

        [ContextMenu("Test")]
        private void Test() => PlayGoalPopUp(_testActivityGoal);
    }
}