﻿using Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using Frostgate.RiftHunters.Core.Battle.UI;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Hud.Activity.SapperActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(UISapperActivityAreaProgressBar))]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(UIBarPanelItem))]
    [RequireComponent(typeof(SpeedIndicator))]
    public sealed class UISapperActivityAreaProgressBar : MonoBehaviour, IUIBarPanelItem, ICapturedReceiver,
        ICaptureProgressReceiver, ICaptureSpeedReceiver
    {
        [SerializeField, Required] private UIBarPanelItem _panelItem;
        [SerializeField, Required] private SpeedIndicator _speedIndicator;

        [SerializeField, ChildGameObjectsOnly, Required]
        private TMP_Text _areaIndexText;

        [SerializeField, ChildGameObjectsOnly, Required]
        private UISmoothSlider _slider;

        public string AreaIndex
        {
            set => _areaIndexText.text = value;
        }

        GameObject IGameObject.GameObject => _panelItem.gameObject;
        RectTransform IUIObject.Transform => _panelItem.Transform;
        UISmoothActivator IUIBarPanelItem.SmoothActivator => _panelItem.SmoothActivator;
        RisingAnimationComponent IUIBarPanelItem.RisingAnimation => _panelItem.RisingAnimation;

        bool ICapturedReceiver.IsCaptured
        {
            set { }
        }

        float ICaptureProgressReceiver.MaxProgress
        {
            set => _slider.SetMaxValue(value);
        }

        float ICaptureProgressReceiver.CurrentProgress
        {
            set => _slider.SetValue(value);
        }

        int ICaptureSpeedReceiver.CaptureSpeed
        {
            set => _speedIndicator.Speed = value;
        }

        private void OnValidate() => DiscoverReferences();

        [Button]
        private void Reset()
        {
            _panelItem = null;
            _speedIndicator = null;
            _areaIndexText = null;
            _slider = null;
        }

        private void Awake() => DiscoverReferences();

        private void Start() => _speedIndicator.Show();

        private void DiscoverReferences()
        {
            _panelItem ??= GetComponent<UIBarPanelItem>();
            _speedIndicator ??= GetComponent<SpeedIndicator>();
            _slider ??= GetComponentInChildren<UISmoothSlider>();
        }
    }
}