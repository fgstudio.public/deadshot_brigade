using TMPro;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    /// <summary>
    /// Счётчик лут-боксов на Hud.
    /// </summary>
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(LootBoxCounter))]
    public sealed class LootBoxCounter : MonoBehaviour
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Image _icon;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _count;

        [field: Header("Events")]
        [field: SerializeField] public UnityEvent CounterChanged { get; private set; }

        [ContextMenu(nameof(ResetCount))]
        public void ResetCount() => SetCount(default);

        public void SetCount(int count)
        {
            string newText = count.ToString();

            if (newText != _count.text)
            {
                _count.text = newText;
                CounterChanged?.Invoke();
            }
        }
    }
}