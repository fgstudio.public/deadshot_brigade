using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Looting;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public interface ILootBoxesPanel
    {
        void UpdateCounts([NotNull] IReadOnlyLootBoxStateCounter stateCounter);
        void SetCount(LootBoxType type, int count);
    }

    /// <summary>
    /// Панель с счётчиками лутбоксов для Hud.
    /// </summary>
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(LootBoxesPanel))]
    public sealed class LootBoxesPanel : MonoBehaviour, ILootBoxesPanel
    {
        [SerializeField, Required, ChildGameObjectsOnly] private LootBoxCounter _greenCounter;
        [SerializeField, Required, ChildGameObjectsOnly] private LootBoxCounter _blueCounter;
        [SerializeField, Required, ChildGameObjectsOnly] private LootBoxCounter _purpleCounter;

        public void UpdateCounts(IReadOnlyLootBoxStateCounter stateCounter)
        {
            UpdateCount(LootBoxType.Green, stateCounter);
            UpdateCount(LootBoxType.Blue, stateCounter);
            UpdateCount(LootBoxType.Purple, stateCounter);
        }

        public void SetCount(LootBoxType type, int count) =>
            GetCounter(type).SetCount(count);

        private void UpdateCount(LootBoxType type, IReadOnlyLootBoxStateCounter stateCounter) =>
            SetCount(type, stateCounter.GetCount(type));

        private LootBoxCounter GetCounter(LootBoxType type) =>
            type switch
            {
                LootBoxType.Green => _greenCounter,
                LootBoxType.Blue => _blueCounter,
                LootBoxType.Purple => _purpleCounter,
                _ => throw new ArgumentOutOfRangeException()
            };
    }
}