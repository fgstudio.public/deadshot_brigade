using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI.Hud.Pointing;
using Frostgate.RiftHunters.Core.UI.Hud.AutoRespawn;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Frostgate.RiftHunters.Core.UI.Hud.KillStreak;
using Frostgate.RiftHunters.Core.UI.Input;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public interface IHudMediator
    {
        ITeamPanel TeamPanel { get; }
        IPointerPanelRoster PointerPanels { get; }
        IUIActivityPanel ActivityPanel { get; }
        UIActivityDescriptionPanel ActivityDescription { get; }
        ISpecialProgressBarsPanel SpecialProgressBars { get; }
        DamageDirectionIndicator DamageDirectionIndicator { get; }
        AutoRespawnLayout AutoRespawnLayout { get; }
        UIMissionTimer MissionTimer { get; }
        UIMissionEndTimer MissionEndTimer { get; }
        UiKillStreak KillStreak { get; }
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + nameof(HudMediator))]
    public sealed class HudMediator : MonoBehaviour, IHudMediator, ISmoothActivation
    {
        [Header("Dependencies")]
        [SerializeField, Required, ChildGameObjectsOnly] private UISmoothActivator _smoothActivator;

        [Header("Elements")]
        [SerializeField, Required, ChildGameObjectsOnly] private TeamPanel _teamPanel;
        [SerializeField, Required, ChildGameObjectsOnly] private PointerPanelRoster _pointerPanels;
        [SerializeField, Required, ChildGameObjectsOnly] private UIActivityPanel _activityPanel;
        [SerializeField, Required, ChildGameObjectsOnly] private UIActivityDescriptionPanel _activityDescription;
        [SerializeField, Required, ChildGameObjectsOnly] private SpecialProgressBarsPanel _specialProgressBars;
        [SerializeField, Required, ChildGameObjectsOnly] private DamageDirectionIndicator _damageDirectionIndicator;
        [SerializeField, Required, ChildGameObjectsOnly] private AutoRespawnLayout _autoRespawnLayout;
        [SerializeField, Required, ChildGameObjectsOnly] private UIMissionTimer _missionTimer;
        [SerializeField, Required, ChildGameObjectsOnly] private UIMissionEndTimer _missionEndTimer;
        [SerializeField, Required, ChildGameObjectsOnly] private UiKillStreak _killStreak;

        public ITeamPanel TeamPanel => _teamPanel;
        public ISmoothActivator SmoothActivator => _smoothActivator;
        public IPointerPanelRoster PointerPanels => _pointerPanels;
        public IUIActivityPanel ActivityPanel => _activityPanel;
        public UIActivityDescriptionPanel ActivityDescription => _activityDescription;
        public ISpecialProgressBarsPanel SpecialProgressBars => _specialProgressBars;
        public DamageDirectionIndicator DamageDirectionIndicator => _damageDirectionIndicator;
        public AutoRespawnLayout AutoRespawnLayout => _autoRespawnLayout;
        public UIMissionTimer MissionTimer => _missionTimer;
        public UIMissionEndTimer MissionEndTimer => _missionEndTimer;
        public UiKillStreak KillStreak => _killStreak;

        private void Awake() => _killStreak.gameObject.SetActive(false);

        private void OnValidate()
        {
            _smoothActivator ??= GetComponentInChildren<UISmoothActivator>();
            _teamPanel ??= GetComponentInChildren<TeamPanel>();
            _pointerPanels ??= GetComponentInChildren<PointerPanelRoster>();
            _activityDescription ??= GetComponentInChildren<UIActivityDescriptionPanel>();
            _specialProgressBars ??= GetComponentInChildren<SpecialProgressBarsPanel>();
            _damageDirectionIndicator ??= GetComponentInChildren<DamageDirectionIndicator>();
            _autoRespawnLayout ??= GetComponentInChildren<AutoRespawnLayout>();
            _missionTimer ??= GetComponentInChildren<UIMissionTimer>();
            _missionEndTimer ??= GetComponentInChildren<UIMissionEndTimer>();
            _killStreak ??= GetComponentInChildren<UiKillStreak>();
        }
    }
}