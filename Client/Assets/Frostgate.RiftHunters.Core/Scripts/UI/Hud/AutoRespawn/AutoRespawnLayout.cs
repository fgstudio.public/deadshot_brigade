﻿using TMPro;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Hud.AutoRespawn
{
    [DisallowMultipleComponent]
    public class AutoRespawnLayout : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _autoRespawnDisabledLabel;

        public void Show()
        {
            _autoRespawnDisabledLabel.enabled = true;
        }

        public void Hide()
        {
            _autoRespawnDisabledLabel.enabled = false;
        }
    }
}