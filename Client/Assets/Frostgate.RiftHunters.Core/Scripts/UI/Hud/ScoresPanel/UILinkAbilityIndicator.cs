using System;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

namespace Frostgate.RiftHunters.Core.UI.Hud.Score
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UILinkAbilityIndicator))]
    public sealed class UILinkAbilityIndicator : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private SpeedIndicator _speedIndicator;

        [CanBeNull] private LinkSpawnerView _linkSpawnerView;
        private bool _isLinkActive;

        public void Init([NotNull] LinkSpawnerView linkSpawnerView)
        {
            _isLinkActive = false;
            _linkSpawnerView = linkSpawnerView;

            Subscribe(_linkSpawnerView);

            if (_linkSpawnerView.IsActive)
                OnLinkStarted(default, default);
        }

        private void OnDestroy()
        {
            _isLinkActive = false;

            if (_linkSpawnerView != null)
                Unsubscribe(_linkSpawnerView);

            _linkSpawnerView = null;
        }

        private void Update()
        {
            if (_isLinkActive && _linkSpawnerView != null)
                _speedIndicator.Speed = _linkSpawnerView.LinkRank;
        }

        private void Subscribe([NotNull] LinkSpawnerView linkSpawnerView)
        {
            linkSpawnerView.Started.AddListener(OnLinkStarted);
            linkSpawnerView.Finished.AddListener(OnLinkFinished);
        }

        private void Unsubscribe([NotNull] LinkSpawnerView linkSpawnerView)
        {
            linkSpawnerView.Started.RemoveListener(OnLinkStarted);
            linkSpawnerView.Finished.RemoveListener(OnLinkFinished);
        }

        private void OnLinkStarted(float radius, TimeSpan duration)
        {
            _isLinkActive = true;
            _speedIndicator.gameObject.SetActive(true);
        }

        private void OnLinkFinished()
        {
            _isLinkActive = false;
            _speedIndicator.gameObject.SetActive(false);
        }
    }
}