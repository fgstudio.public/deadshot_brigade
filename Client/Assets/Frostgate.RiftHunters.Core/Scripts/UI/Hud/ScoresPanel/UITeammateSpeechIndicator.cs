﻿using Frostgate.RiftHunters.Core.Voice;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Frostgate.RiftHunters.Core.Scripts.UI.Hud.Score
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UITeammateSpeechIndicator))]
    public sealed class UITeammateSpeechIndicator : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private Image _indicationActiveImage;

        [SerializeField, Required, ChildGameObjectsOnly]
        private Image _indicationInactiveImage;

        [NotNull] private IParticipantsVoiceController _participantsController;
        private string _userName = string.Empty;
        private bool _isMuted;

        private void OnValidate() => DiscoverDependencies();

        private void Awake() => DiscoverDependencies();

        [Inject]
        public void MonoConstructor([NotNull] IParticipantsVoiceController participantsController)
        {
            _participantsController = participantsController;
        }

        public void Initialize(string userName)
        {
            _userName = userName;

            SetIndicationEnabled(false);

            Subscribe();
        }

        public void Deinitialize()
        {
            _userName = string.Empty;

            SetIndicationEnabled(false);

            Unsubscribe();
        }

        private void Subscribe()
        {
            _participantsController.ParticipantSpeechDetected += OnParticipantSpeechDetected;
            _participantsController.MuteParticipantChanged += OnParticipantMuteChanged;
        }

        private void Unsubscribe()
        {
            _participantsController.ParticipantSpeechDetected -= OnParticipantSpeechDetected;
            _participantsController.MuteParticipantChanged -= OnParticipantMuteChanged;
        }

        private void DiscoverDependencies() =>
            _indicationActiveImage ??= GetComponentInChildren<Image>();

        private void OnParticipantSpeechDetected(string userName, bool value)
        {
            if (_isMuted)
                return;

            if (_userName == userName)
                SetIndicationEnabled(value);
        }

        private void OnParticipantMuteChanged(string userName, bool isMuted)
        {
            if (_userName != userName)
                return;

            _isMuted = isMuted;
            SetActive(!isMuted);
        }

        private void SetIndicationEnabled(bool isEnabled)
        {
            _indicationActiveImage.enabled = isEnabled;
            _indicationInactiveImage.enabled = !isEnabled;
        }

        private void SetActive(bool isActive) =>
            gameObject.SetActive(isActive);
    }
}