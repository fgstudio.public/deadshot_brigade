using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI.Hud.Score;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    // TODO: коода будет обеспечена уникальность userName, тогда можно отказаться от identity
    public interface ITeamPanel
    {
        void UpdateTeammatePanelAnimated(uint netId, string userName, int score, bool isLocalPlayer);
        void UpdateTeammatePanelDiscrete([NotNull] UnitNetwork unit);
        void RemoveTeammatePanel([NotNull] NetworkIdentity identity);
        void UpdateTeammatePanelEmotion([NotNull] UnitNetwork unit, [CanBeNull] UnitEmotionConfig emotionConfig);
    }

    [HelpURL("https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#f6918c33089f4e14b33e639b0ad8b2c0")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(TeamPanel))]
    public sealed class TeamPanel : MonoBehaviour, ITeamPanel
    {
        [SerializeField, Required, ChildGameObjectsOnly] private ScorePanelItemPool _localItemPool;
        [SerializeField, Required, ChildGameObjectsOnly] private ScorePanelItemPool _remoteItemPool;
        [SerializeField, Required, ChildGameObjectsOnly] private AnimatedListContainer _animatedListContainer;

        private readonly ScorePanelItemSortedCollection _sortedCollection = new();

        public void UpdateTeammatePanelAnimated(uint netId, string userName, int score, bool isLocalPlayer)
        {
            IPool<TeammatePanel> itemPool = GetPool(isLocalPlayer);
            UpdateTeammatePanel(netId, userName, score);
        }

        public void UpdateTeammatePanelDiscrete(UnitNetwork unit)
        {
            IPool<TeammatePanel> itemPool = GetPool(unit.Identity.isLocalPlayer);
            UpdateTeammatePanel(unit, itemPool, true);
        }

        public void RemoveTeammatePanel(NetworkIdentity identity)
        {
            uint netId = identity.netId;
            bool isLocalPlayer = identity.isLocalPlayer;

            if (_sortedCollection.TryGetItem(netId, out TeammatePanel teammatePanel))
            {
                _sortedCollection.RemoveItem(netId);
                GetPool(isLocalPlayer).Release(teammatePanel);
                _animatedListContainer.UpdateContent();
            }
        }

        public void UpdateTeammatePanelEmotion(UnitNetwork unit, UnitEmotionConfig emotionConfig)
        {
            if (_sortedCollection.TryGetItem(unit.Identity.netId, out TeammatePanel item))
            {
                if (emotionConfig != null)
                    item.SetEmotion(emotionConfig);
                else
                    item.ClearEmotion();
            }
        }

        private IPool<TeammatePanel> GetPool(bool isLocalPlayer) =>
            isLocalPlayer ? _localItemPool : _remoteItemPool;

        private void UpdateTeammatePanel(UnitNetwork unit, IPool<TeammatePanel> itemPool, bool discrete)
        {
            if (!_sortedCollection.TryGetItem(unit.Identity.netId, out TeammatePanel item))
            {
                item = itemPool.Get();
                item.Init(unit);
                item.ClearEmotion();
            }

            if (discrete)
                item.UpdateDataDiscrete(unit.UnitNetworkState.PlayerName, unit.UnitNetworkState.Exp);
            else
                item.UpdateDataAnimated(unit.UnitNetworkState.PlayerName, unit.UnitNetworkState.Exp);

            _sortedCollection.UpdateItem(unit.Identity.netId, item);
            _animatedListContainer.UpdateContent();
        }

        private void UpdateTeammatePanel(uint netId, string userName, int score)
        {
            if (!_sortedCollection.TryGetItem(netId, out TeammatePanel item))
                return;

            item.UpdateDataAnimated(userName, score);

            _sortedCollection.UpdateItem(netId, item);
            _animatedListContainer.UpdateContent();
        }
    }
}