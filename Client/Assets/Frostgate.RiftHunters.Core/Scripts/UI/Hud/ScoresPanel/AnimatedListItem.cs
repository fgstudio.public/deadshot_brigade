﻿using DG.Tweening;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public class AnimatedListItem : MonoBehaviour
    {
        [SerializeField] private float _animationDuration = 1.0f;

        private Transform _transform;

        public int PositionIndex { get; set; } = -1;

        private void Awake() => _transform = transform;

        //private void OnEnable() => _lastPosition = _transform.position;

        public void UpdatePosition(Vector3 targetPosition)
        {
            if (!_transform)
                return;

            _transform.DOLocalMove(targetPosition, _animationDuration);
        }
    }
}