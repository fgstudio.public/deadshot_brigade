using System;
using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.UI.Hud.Score
{
    public sealed class ScorePanelItemSortedCollection
    {
        private readonly Dictionary<uint, TeammatePanel> _items = new();
        private TeammatePanel[] _sortedItems = Array.Empty<TeammatePanel>();

        public IEnumerable<TeammatePanel> Items => _sortedItems;

        public TeammatePanel this[uint netId]
        {
            set => UpdateItem(netId, value);
            get => _items[netId];
        }

        public void UpdateItem(uint netId, [NotNull] TeammatePanel item)
        {
            _items[netId] = item;

            if (!IsSortedFor(item))
                UpdateSorting();
        }

        public void RemoveItem(uint netId) =>
            _items.Remove(netId);

        public bool TryGetItem(uint netId, out TeammatePanel item) =>
            _items.TryGetValue(netId, out item);

        private bool IsSortedFor(TeammatePanel item)
        {
            int index = Array.IndexOf(_sortedItems, item);

            if (index == -1)
                return false;

            if (index > 0)
            {
                TeammatePanel leftItem = _sortedItems[index - 1];
                if (leftItem.CurrentScore < item.CurrentScore)
                    return false;
            }

            if (index < _sortedItems.Length - 1)
            {
                TeammatePanel rightItem = _sortedItems[index + 1];
                if (rightItem.CurrentScore > item.CurrentScore)
                    return false;
            }

            return true;
        }

        private void UpdateSorting()
        {
            _sortedItems = _items.Values
                .OrderByDescending(x => x.CurrentScore)
                .ToArray();

            for (int i = 0; i < _sortedItems.Length; i++)
                _sortedItems[i].transform.SetSiblingIndex(i);
        }
    }
}
