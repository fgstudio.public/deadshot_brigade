using TMPro;
using UnityEngine;
using DG.Tweening;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Reanimation;
using Frostgate.RiftHunters.Core.Scripts.UI.Hud.Score;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Pool;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI.Hud.Score
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#f6918c33089f4e14b33e639b0ad8b2c0")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(TeammatePanel))]
    public sealed class TeammatePanel : MonoBehaviour, IPoolObject
    {
        [Header("References")] [SerializeField, Required, ChildGameObjectsOnly]
        private Image _userClassIcon;

        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _userName;

        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _statusLayout;

        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _emotion;

        [SerializeField, Required, ChildGameObjectsOnly]
        private Image _emotionIcon;

        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _emotionText;

        [SerializeField, Required, ChildGameObjectsOnly]
        private Image _emotionGlowIcon;

        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _emotionGlowText;

        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _score;

        [SerializeField, Required, ChildGameObjectsOnly]
        private Bar _healthBar;

        [SerializeField, Required, ChildGameObjectsOnly]
        private EnergyBar _energyBar;

        [SerializeField, Required, ChildGameObjectsOnly]
        private UIUnitEffectsView _unitEffectsView;

        [SerializeField, Required, ChildGameObjectsOnly]
        private UILinkAbilityIndicator _linkAbilityIndicator;

        [SerializeField, Required, ChildGameObjectsOnly]
        private UITeammateSpeechIndicator _speechIndicator;

        [SerializeField, Required, ChildGameObjectsOnly]
        private Animator _animator;

        [Header("Settings")] [SerializeField, Range(0, 500)]
        private float _scorePerSecond;

        private Tweener _scoreTween;
        private int _displayedScore;
        private UnitNetworkState _state;
        private ReanimationMechanicComponent _reanimationMechanic;

        private static readonly int _resetTrigger = Animator.StringToHash("Reset");
        private static readonly int _defaultTrigger = Animator.StringToHash("Default");
        private static readonly int _knockdownTrigger = Animator.StringToHash("Knockdown");
        private static readonly int _reanimationTrigger = Animator.StringToHash("Reanimation");

        public int CurrentScore { get; private set; }

        public void Init(UnitNetwork unit)
        {
            SetUnitState(unit.UnitNetworkState);
            SetUnitEffectsPanel(unit);
            SetLinkAbilityIndicator(unit);
            SetReanimationMechanic(unit.ReanimationMechanic);
        }

        private void OnEnable() => SetDefaultState();

        private void OnDestroy()
        {
            RemoveReanimationMechanic();
            RemoveUnitState();
        }

        public void SetEmotion(UnitEmotionConfig config)
        {
            _emotionText.text = config.Name;
            _emotionIcon.sprite = config.Sprite;
            _emotionGlowText.text = config.Name;
            _emotionGlowIcon.sprite = config.GlowSprite;

            ClearEmotion();
            _emotion.SetActive(true);
            _statusLayout.gameObject.SetActive(false);
        }

        public void ClearEmotion()
        {
            _emotion.gameObject.SetActive(false);
            _statusLayout.gameObject.SetActive(true);
        }

        public void RemoveUnitState()
        {
            if (_state == null) return;

            _state.HealthChanged -= OnUnitHealthChanged;
            _state.HealthState.MaxHealthChanged -= OnUnitMaxHealthChanged;
            _state.EnergyChanged -= OnUnitEnergyChanged;

            _state = null;
            _speechIndicator.Deinitialize();
        }

        public void UpdateDataAnimated(string userName, int score)
        {
            UpdateUserName(userName);
            UpdateScoreAnimated(score);
        }

        public void UpdateDataDiscrete(string userName, int score)
        {
            UpdateUserName(userName);
            UpdateScoreDiscrete(score);
        }

        void IPoolObject.OnGet()
        {
        }

        void IPoolObject.OnRelease()
        {
            _unitEffectsView.OnPoolRelease();
            OnDestroy();
        }

        private void SetUnitState(UnitNetworkState unitNetworkState)
        {
            RemoveUnitState();

            _state = unitNetworkState;
            _animator.SetTrigger(_resetTrigger);
            _userClassIcon.sprite = _state.UnitConfig.GoldRoleIcon;

            _healthBar.Set(unitNetworkState.Health, unitNetworkState.MaxHealth);
            _healthBar.Color.UpdateColor(_state.isLocalPlayer, _state.UnitType);

            _energyBar.Set(unitNetworkState.Energy, unitNetworkState.MaxEnergy);

            _state.HealthChanged += OnUnitHealthChanged;
            _state.HealthState.MaxHealthChanged += OnUnitMaxHealthChanged;
            _state.EnergyChanged += OnUnitEnergyChanged;
            
            _speechIndicator.Initialize(unitNetworkState.PlayerName);
        }

        private void SetUnitEffectsPanel(UnitNetwork unit) =>
            _unitEffectsView.Init(unit.UnitNetworkEffects, unit.UnitNetworkState);

        private void SetLinkAbilityIndicator(UnitNetwork unit) =>
            _linkAbilityIndicator.Init(unit.LinkSpawnerView);

        private void SetReanimationMechanic(ReanimationMechanicComponent reanimationMechanic)
        {
            _reanimationMechanic = reanimationMechanic;

            _reanimationMechanic.Activated.AddListener(SetReanimationState);
            _reanimationMechanic.Completed.AddListener(SetDefaultState);
            _reanimationMechanic.Deactivated.AddListener(SetKnockdownState);
        }

        private void RemoveReanimationMechanic()
        {
            if (_reanimationMechanic == null) return;

            _reanimationMechanic.Activated.RemoveListener(SetReanimationState);
            _reanimationMechanic.Completed.RemoveListener(SetDefaultState);
            _reanimationMechanic.Deactivated.RemoveListener(SetKnockdownState);

            _reanimationMechanic = null;
        }

        private void SetReanimationState() => _animator.SetTrigger(_reanimationTrigger);
        private void SetDefaultState() => _animator.SetTrigger(_defaultTrigger);
        private void SetKnockdownState() => _animator.SetTrigger(_knockdownTrigger);

        private void OnUnitHealthChanged(float value)
        {
            int trigger = _state.Health > 0 ? _defaultTrigger : _knockdownTrigger;
            _animator.SetTrigger(trigger);

            _healthBar.SetCurrent(_state.Health);
        }

        private void OnUnitMaxHealthChanged(float oldValue, float newValue) =>
            _healthBar.SetMax(newValue);

        private void OnUnitEnergyChanged(float diff) =>
            _energyBar.SetCurrent(_state.Energy);

        private void UpdateUserName(string userName) =>
            _userName.text = userName;

        private void UpdateScoreAnimated(int score)
        {
            CurrentScore = score;

            _scoreTween?.Kill();
            float duration = CalcDuration(_displayedScore, score);
            _scoreTween = DOTween.To(GetCurrentScore, SetScore, score, duration);
        }

        private void UpdateScoreDiscrete(int score)
        {
            CurrentScore = score;
            _displayedScore = score;
            _score.text = score.ToString();
        }

        private void SetScore(int score)
        {
            _displayedScore = score;
            _score.text = _displayedScore.ToString();
        }

        private int GetCurrentScore() => _displayedScore;

        private float CalcDuration(int startScore, int endScore) =>
            _scorePerSecond > 0
                ? Mathf.Abs(endScore - startScore) / _scorePerSecond
                : 0;
    }
}