using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Pool;
using JetBrains.Annotations;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI.Hud.Score
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(ScorePanelItemPool))]
    public sealed class ScorePanelItemPool : MonoPoolComponent<TeammatePanel>
    {
        [SerializeField, Required, AssetsOnly] private TeammatePanel _prefab;

        [NotNull]private IPrefabFactory _prefabFactory;
        
        [Inject]
        public void MonoConstructor([NotNull] IPrefabFactory prefabFactory)
        {
            _prefabFactory = prefabFactory;
        }
        
        protected override GameObject GetPrefab() => _prefab.gameObject;

        protected override TeammatePanel CreateObject(GameObject prefab) =>
            _prefabFactory.Instantiate<TeammatePanel>(prefab);

        protected override void DestroyObject(TeammatePanel poolObject) =>
            Destroy(poolObject.gameObject);
    }
}