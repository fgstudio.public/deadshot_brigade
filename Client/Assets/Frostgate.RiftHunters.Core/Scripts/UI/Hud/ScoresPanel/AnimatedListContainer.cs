using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public sealed class AnimatedListContainer : MonoBehaviour
    {
        [SerializeField] private float _elementHeight = 25.0f;
        [SerializeField] private float _spaceBetweenElements = 5.0f;

        private Transform _transform;
        private AnimatedListItem[] _itemsList;

        private void OnEnable() => UpdateContent();

        public void UpdateContent()
        {
            if (_transform == null)
                _transform = transform;

            if (_itemsList == null || _itemsList.Length != _transform.childCount)
                _itemsList = GetComponentsInChildren<AnimatedListItem>();

            foreach (var item in _itemsList)
            {
                var transformSiblingIndex = item.transform.GetSiblingIndex();

                item.PositionIndex = transformSiblingIndex;
                item.UpdatePosition(CalculateTargetPosition(transformSiblingIndex));
            }
        }

        private Vector3 CalculateTargetPosition(int positionIndex)
        {
            var offsetY = (_elementHeight + _spaceBetweenElements) * positionIndex;
            var position = _transform.localPosition;
            position.y -= offsetY;
            return position;
        }
    }
}
