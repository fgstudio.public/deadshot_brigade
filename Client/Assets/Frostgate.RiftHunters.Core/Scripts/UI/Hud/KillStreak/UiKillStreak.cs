﻿using System;
using System.Threading;
using TMPro;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Client.KillStreak;
using Frostgate.RiftHunters.Core.Battle.UI;

namespace Frostgate.RiftHunters.Core.UI.Hud.KillStreak
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0?pvs=4")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UiKillStreak))]
    public sealed class UiKillStreak : MonoBehaviour
    {
        [SerializeField, BoxGroup("References"), Required] private GameObject _rootObject;
        [SerializeField, BoxGroup("References"), Required] private CanvasGroup _canvasGroup;
        [SerializeField, BoxGroup("References"), Required] private TMP_Text _countText;
        [SerializeField, BoxGroup("References"), Required] private GameObject _endStreakMessage;

        [BoxGroup("Animation settings"), MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _animationDuration = 1f;

        [BoxGroup("Animation settings"), MinValue(0)]
        [SerializeField] private float _bonusScale = 1.3f;

        [CanBeNull] private CancellationTokenSource _cts;
        private Vector3? _scaleCache;

        private void Start() =>
            CacheScale();

        private void OnDestroy()
        {
            StopFadeTween();
            StopScaleTween();

            CancelDelay();
        }

        public void ShowStreak()
        {
            StopFadeTween();
            StopScaleTween();
            ResetScale();
            CancelDelay();

            SetFinishKillStreak(false);
            SetVisible(true);
        }

        public void ShowResult()
        {
            SetFinishKillStreak(true);
        }

        public void SetKillCount(int count)
        {
            _countText.text = "x" + count.ToString();
        }

        private void CacheScale()
        {
            _scaleCache = _rootObject.transform.localScale;
        }

        private void ResetScale()
        {
            if (_scaleCache.HasValue)
                _rootObject.transform.localScale = _scaleCache.Value;
        }

        private void SetFinishKillStreak(bool isFinish)
        {
            _endStreakMessage.gameObject.SetActive(isFinish);

            if (isFinish)
            {
                PlayScaleTween();
                Delay(_animationDuration, () => SetVisible(false));
            }
        }

        private void SetVisible(bool visible)
        {
            if (visible)
            {
                _rootObject.gameObject.SetActive(true);
                PlayFadeTween(1);
            }
            else
            {
                PlayFadeTween(0);
                Delay(_animationDuration, () => _rootObject.gameObject.SetActive(false));
            }
        }

        private void PlayScaleTween()
        {
            Transform rootTransform = _rootObject.transform;
            rootTransform.DOScale(rootTransform.localScale * _bonusScale, _animationDuration);
        }

        private void StopScaleTween()
        {
            DOTween.Kill(_rootObject.transform);
        }

        private void PlayFadeTween(float targetAlpha)
        {
            _canvasGroup.DOFade(targetAlpha, _animationDuration);
        }

        private void StopFadeTween()
        {
            DOTween.Kill(_canvasGroup);
        }

        private void Delay(float delaySec, Action callback)
        {
            _cts = new CancellationTokenSource();

            UniTask.Delay(TimeSpan.FromSeconds(delaySec), cancellationToken: _cts.Token)
                .ContinueWith(callback).SuppressCancellationThrow();
        }

        private void CancelDelay()
        {
            if (_cts != null)
            {
                _cts.Cancel();
                _cts.Dispose();
                _cts = null;
            }
        }
    }
}