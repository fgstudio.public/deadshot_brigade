using System;
using TMPro;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public sealed class UIMissionTimer : MonoBehaviour
    {
        private readonly int _activation = Animator.StringToHash("Activation");
        private readonly int _bump = Animator.StringToHash("Bump");

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private TMP_Text _timerText;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Animator _animator;

        [SerializeField, MinValue(0f), SuffixLabel("sec", true), FoldoutGroup("Setting")]
        private float _updateInterval = 1f;

        private int? _currentAnimatorState;
        private TimeSpan _lastInterval;

        public bool IsActive => _currentAnimatorState.HasValue &&
                                _currentAnimatorState.Value == _activation;

        private void OnEnable() => UpdateAnimatorState();

        public void UpdateTimer(TimeSpan interval, bool forced = false)
        {
            TimeSpan dt = interval - _lastInterval;
            if (forced || dt.TotalSeconds > _updateInterval)
            {
                _lastInterval = interval;
                _timerText.text = interval.ToString("mm':'ss");
            }
        }

        public void SetActiveState() => SetAnimatorState(_activation);

        public void StartBumpAnimation() => SetAnimatorState(_bump);

        private void SetAnimatorState(int state)
        {
            if (!_currentAnimatorState.HasValue || state != _currentAnimatorState.Value)
                _currentAnimatorState = state;

            UpdateAnimatorState();
        }

        private void UpdateAnimatorState()
        {
            if (_currentAnimatorState.HasValue && enabled)
                _animator.SetTrigger(_currentAnimatorState.Value);
        }
    }
}