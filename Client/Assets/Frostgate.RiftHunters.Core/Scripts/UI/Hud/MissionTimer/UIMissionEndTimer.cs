using System.Collections;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public sealed class UIMissionEndTimer : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _timerValueText;
        [SerializeField, Required, ChildGameObjectsOnly] private UISmoothActivator _smoothActivator;

        public void SetActive(bool isActive)
        {
            if(isActive)
                _smoothActivator.Activate();
            else
                _smoothActivator.Deactivate();
        }

        public void RunTimer(float seconds)
        {
            SetActive(true);
            StartCoroutine(StartTimer(seconds));
        }

        private IEnumerator StartTimer(float seconds)
        {
            while (seconds > 0)
            {
                UpdateTimer(seconds);

                yield return new WaitForSeconds(1);
                seconds -= 1;
            }

            UpdateTimer(0);
            SetActive(false);
        }

        private void UpdateTimer(float seconds) => _timerValueText.text = $"{seconds:F0}";
    }
}
