using DG.Tweening;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    /// <summary>
    /// Кломпонент анимации для прогресс-бара уровня энергии.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(EnergyProgress))]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(AnimatedEnergyProgress))]
    public sealed class AnimatedEnergyProgress : Component, IEnergyProgress
    {
        private const float MinProgressSpeed = 0f;

        [Header("References")]
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private EnergyProgress _progress;

        [Header("Settings")]
        [SerializeField, Min(MinProgressSpeed)] private float _progressPerSecond;
        [SerializeField, Min(MinProgressSpeed)] private float redutionSpeedMod;

        public UnityEvent Updated => _progress.Updated;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() => _progress ??= GetComponent<EnergyProgress>();
        protected override void OnDisabled() => StopAnimation();

        [ContextMenu(nameof(ResetProgress))]
        public void ResetProgress() => SetProgress(default);

        public void InitProgress(float ratio) =>
            _progress.InitProgress(ratio);

        public void SetProgress(float ratio)
        {
            StopAnimation();

            if (enabled)
            {
                float duration = CalcDuration(_progress.ProgressSlider.value, ratio);
                _progress.ProgressSlider.DOValue(ratio, duration);
                _progress.SetBoostedDiapason(ratio);
            }
            else
            {
                _progress.SetProgress(ratio);
            }
        }

        [ContextMenu(nameof(StopAnimation))]
        private void StopAnimation() => _progress.ProgressSlider.DOKill();

        private float CalcDuration(float currentValue, float targetValue)
        {
            float speedMod = targetValue - currentValue < 0 ? redutionSpeedMod : 1;

            float duration = _progressPerSecond > MinProgressSpeed
                ? Mathf.Abs(targetValue - currentValue) / _progressPerSecond
                : MinProgressSpeed;

            return duration / speedMod;
        }
    }
}