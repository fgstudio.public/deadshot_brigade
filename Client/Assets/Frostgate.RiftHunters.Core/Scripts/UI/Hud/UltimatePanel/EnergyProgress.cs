using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public interface IEnergyProgress : IUnityEventUpdateable
    {
        void ResetProgress();

        /// <summary>
        /// Устновка начального значения прогресс-бара
        /// </summary>
        /// <param name="ratio">Значение в интервале [0..1]</param>
        void InitProgress(float ratio);

        /// <summary>
        /// Устновка значения прогресс-бара
        /// </summary>
        /// <param name="ratio">Значение в интервале [0..1]</param>
        void SetProgress(float ratio);
    }

    /// <summary>
    /// Компонент прогресс-бара для уровня энергии.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(EnergyProgress))]
    public sealed class EnergyProgress : MonoBehaviour, IEnergyProgress
    {
        [Required] public Slider ProgressSlider;
        [Required] public RectMask2D BoostedPartMask;
        [Required] public RectTransform BoostedPartTransform;
        [field: FoldoutGroup("Events")] public UnityEvent Updated { get; private set; }

        [ContextMenu(nameof(ResetProgress))]
        public void ResetProgress() => SetProgress(default);

        public void InitProgress(float ratio) =>
            SetProgress(ratio);

        public void SetProgress(float ratio)
        {
            ProgressSlider.value = ratio;
            Updated?.Invoke();
        }

        public void SetBoostedDiapason(float targetValue)
        {
            var currentValue = ProgressSlider.value;
            if(targetValue - currentValue <= 0.01f)
            {
                BoostedPartMask.gameObject.SetActive(false);
                return;
            }
            BoostedPartMask.gameObject.SetActive(true);

            var padding = BoostedPartMask.padding;
            padding.w = BoostedPartTransform.rect.height * (1 - targetValue);
            BoostedPartMask.padding = padding;
        }

        private void SetStartBoostPart(float value)
        {
            var padding = BoostedPartMask.padding;
            padding.y = value * BoostedPartTransform.rect.height;
            BoostedPartMask.padding = padding;
        }

        private void OnEnable() => ProgressSlider.onValueChanged.AddListener(SetStartBoostPart);

        private void OnDisable() => ProgressSlider.onValueChanged.RemoveListener(SetStartBoostPart);
    }
}