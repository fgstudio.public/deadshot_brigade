using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.UI.Hud.TotemPanel
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(TotemsPanelItemPool))]
    public sealed class TotemsPanelItemPool : MonoPoolComponent<TotemsPanelItem>
    {
        [Required, AssetsOnly] public TotemsPanelItem Prefab;

        protected override GameObject GetPrefab() => Prefab.gameObject;
        protected override TotemsPanelItem CreateObject(GameObject prefab) => Instantiate(prefab).GetComponent<TotemsPanelItem>();
        protected override void DestroyObject(TotemsPanelItem poolObject) => Destroy(poolObject.gameObject);
    }
}