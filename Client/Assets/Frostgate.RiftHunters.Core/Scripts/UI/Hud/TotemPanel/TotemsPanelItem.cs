using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.UI;

namespace Frostgate.RiftHunters.Core.UI.Hud.TotemPanel
{
    public interface ITotemsPanelItem : IPoolObject
    {
        ITotem Totem { get; }
        void SetTotem([NotNull] ITotem totem);
        void RemoveTotem();
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(TotemsPanelItem))]
    public sealed class TotemsPanelItem : MonoBehaviour, ITotemsPanelItem
    {
        [Required, ChildGameObjectsOnly] public UISmoothSlider Slider;
        [Required, ChildGameObjectsOnly] public RisingAnimationComponent RisingAnimation;
        [Required, ChildGameObjectsOnly] public UISmoothActivator SmoothActivator;
        [ShowInInspector, ReadOnly, CanBeNull] public ITotem Totem { get; private set; }

        private void OnValidate() => Slider ??= GetComponentInChildren<UISmoothSlider>();
        private void OnDestroy() => RemoveTotem();

        void IPoolObject.OnGet() { }
        void IPoolObject.OnRelease() => RemoveTotem();

        public void SetTotem(ITotem totem)
        {
            RemoveTotem();

            Totem = totem;
            SubscribeTotem(totem);
            UpdateSlider(totem);
        }

        public void RemoveTotem()
        {
            if (Totem != null)
            {
                UnsubscribeTotem(Totem);
                ResetSlider();
                Totem = null;
            }
        }

        private void SubscribeTotem([NotNull] ITotem totem)
        {
            totem.Destroyed.AddListener(RemoveTotem);
            totem.State.HealthChanged += OnHealthChanged;
        }

        private void UnsubscribeTotem([NotNull] ITotem totem)
        {
            totem.Destroyed.RemoveListener(RemoveTotem);
            totem.State.HealthChanged -= OnHealthChanged;
        }

        private void OnHealthChanged(float diff) => UpdateSlider(Totem!);

        private void UpdateSlider([NotNull] ITotem totem)
        {
            float hp = totem.State.Health;
            float maxHp = totem.State.MaxHealth;

            float value = maxHp > 0 ? hp / maxHp : 0;
            Slider.SetValue(value);
        }

        private void ResetSlider() =>
            Slider.SetValue(default);
    }
}