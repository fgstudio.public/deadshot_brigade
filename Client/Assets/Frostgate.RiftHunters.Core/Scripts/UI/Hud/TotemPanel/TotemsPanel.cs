using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.UI.Hud.TotemPanel
{
    public interface ITotemsPanel
    {
        void TrackTotemHp([NotNull] ITotem totem);
        void UntrackTotemHp([NotNull] ITotem totem);
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(TotemsPanel))]
    public sealed class TotemsPanel : MonoBehaviour, ITotemsPanel
    {
        [Required, ChildGameObjectsOnly] public TotemsPanelItemPool Pool;

        private readonly Dictionary<ITotem, TotemsPanelItem> _items = new();

        private void OnValidate() =>
            Pool ??= GetComponentInChildren<TotemsPanelItemPool>();

        public void TrackTotemHp(ITotem totem)
        {
            if (!_items.ContainsKey(totem))
            {
                TotemsPanelItem item = Pool.Get();
                _items[totem] = item;
                item.SetTotem(totem);
                item.SmoothActivator.SetActive(true);
                item.RisingAnimation.Show();
            }
        }

        public void UntrackTotemHp(ITotem totem)
        {
            if (_items.TryGetValue(totem, out TotemsPanelItem item))
            {
                item.RemoveTotem();
                item.SmoothActivator.SetActive(false);
                item.RisingAnimation.Hide(() => Pool.Release(item));
                _items.Remove(totem);
            }
        }
    }
}