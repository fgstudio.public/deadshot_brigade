﻿using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.UI
{
    public interface IUIBotSpawningMediator
    {
        void StartWaves(int spawnerId, bool isNextWaveAfterDelay);
        void SpawnWave(int spawnerId, int waveNumber, bool arePlannedMoreWaves, [NotNull] IBotsInWave botsInWave);
        void KillBotsOfWave(int spawnerId, int waveNumber);
        void KillBotsOfAllWaves(int spawnerId);
        void WaitWave(int spawnerId, int waveNumber, float delaySeconds);
        void EndAwaitOfWave(int spawnerId, int waveNumber, bool willBeMoreWaves);
    }
}