using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.UI
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIBotSpawningMediator))]
    [DisallowMultipleComponent]
    public sealed class UIBotSpawningMediator : MonoBehaviour, IUIBotSpawningMediator
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private UIBotWave _byDeathBotWave;

        [SerializeField, Required, AssetsOnly] private UIBotWave _byTimerBotWaveSample;

        private UIByDeathBotWaveBehavior _byDeathBehavior;
        private UIByTimerBotWaveBehavior _byTimerBehavior;
        [CanBeNull] private IUIBotWaveBehavior _behavior;

        private void Awake()
        {
            _byDeathBehavior = new UIByDeathBotWaveBehavior(_byDeathBotWave, transform);
            _byTimerBehavior = new UIByTimerBotWaveBehavior(_byTimerBotWaveSample, transform);
        }

        public void StartWaves(int spawnerId, bool isNextWaveAfterDelay)
        {
            gameObject.SetActive(true);
            if (isNextWaveAfterDelay)
                _behavior?.StartSpawn(spawnerId);
        }

        public void SpawnWave(int spawnerId, int waveNumber, bool arePlannedMoreWaves, IBotsInWave botsInWave) =>
            _behavior?.SpawnWave(spawnerId, waveNumber, arePlannedMoreWaves, botsInWave);

        public void KillBotsOfWave(int spawnerId, int waveNumber) =>
            _behavior?.KillBotsOfWave(spawnerId, waveNumber);

        public void KillBotsOfAllWaves(int spawnerId) =>
            _behavior?.KillBotsOfAllWaves(spawnerId);

        public void WaitWave(int spawnerId, int waveNumber, float delaySeconds) =>
            _behavior?.WaitWave(spawnerId, waveNumber, delaySeconds);

        public void EndAwaitOfWave(int spawnerId, int waveNumber, bool willBeMoreWaves) =>
            _behavior?.EndAwaitOfWave(spawnerId, waveNumber, willBeMoreWaves);
    }
}