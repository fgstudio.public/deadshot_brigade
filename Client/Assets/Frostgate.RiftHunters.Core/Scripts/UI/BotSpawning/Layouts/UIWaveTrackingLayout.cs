﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.UI.BotSpawning
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIWaveTrackingLayout))]
    [DisallowMultipleComponent]
    public sealed class UIWaveTrackingLayout : Component
    {
        [SerializeField, ChildGameObjectsOnly, Required]
        private WaveCounter _waveCounter;

        [SerializeField, ChildGameObjectsOnly, Required]
        private WaveProgress _waveProgress;

        [SerializeField, ChildGameObjectsOnly, Required]
        private RisingAnimationComponent _risingAnimation;

        public void SetWaveNumber(int waveNumber) => _waveCounter.Count = waveNumber;

        public void SetWaveProgress(int deadBotsCount, int totalBotsCount)
        {
            _waveProgress.Current = deadBotsCount;
            _waveProgress.Total = totalBotsCount;
        }

        protected override void OnEnabled() => _risingAnimation.Show();
    }
}