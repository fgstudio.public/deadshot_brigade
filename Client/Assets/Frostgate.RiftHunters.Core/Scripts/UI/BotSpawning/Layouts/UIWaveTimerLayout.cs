﻿using Sirenix.OdinInspector;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core.Battle.UI.BotSpawning
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIWaveTimerLayout))]
    [DisallowMultipleComponent]
    public sealed class UIWaveTimerLayout : Component
    {
        [SerializeField, ChildGameObjectsOnly, Required]
        private WaveTimer _timer;
        [SerializeField, ChildGameObjectsOnly, Required]
        private RisingAnimationComponent _animationComponent;

        public void EnableTimer(float seconds)
        {
            _timer.Seconds = seconds;
            _timer.Enable();
            _animationComponent.Show();
        }

        public void DisableTimer(Action onDisable)
        {
            _timer.Disable();
            if(_animationComponent.gameObject.activeInHierarchy)
                _animationComponent.Hide(onDisable);
        }
    }
}