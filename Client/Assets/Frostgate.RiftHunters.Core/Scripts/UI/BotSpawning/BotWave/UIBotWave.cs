﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Battle.UI.BotSpawning;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.UI
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIBotWave))]
    [DisallowMultipleComponent]
    public sealed class UIBotWave : Component
    {
        public ISmoothActivator SmoothActivator => _smoothActivator;

        [SerializeField, ChildGameObjectsOnly, Required]
        private UIWaveTrackingLayout _trackingLayout;

        [SerializeField, ChildGameObjectsOnly, Required]
        private UIWaveTimerLayout _timerLayout;

        [SerializeField, Required] private UISmoothActivator _smoothActivator;

        [CanBeNull] private Coroutine _waveObservingRoutine;
        [CanBeNull] private IBotsInWave _botsInWave;

        private void Awake()
        {
            InitRefs();
            SetTimerLayoutEnabled(false);
        }

        private void InitRefs()
        {
            _trackingLayout ??= GetComponentInChildren<UIWaveTrackingLayout>();
            _timerLayout ??= GetComponentInChildren<UIWaveTimerLayout>();
            _smoothActivator ??= GetComponent<UISmoothActivator>();
        }

        public void StartWaveObserving(int waveNumber, IBotsInWave botsInWave)
        {
            _trackingLayout.SetWaveNumber(waveNumber);
            _botsInWave = botsInWave;
            _waveObservingRoutine = StartCoroutine(WaveObservingRoutine());
        }

        public void StopWaveObserving()
        {
            if (_waveObservingRoutine != null)
            {
                StopCoroutine(_waveObservingRoutine);
                SetWaveProgress(_botsInWave!.TotalBotsCount, _botsInWave.TotalBotsCount);

                _botsInWave = null;
                _waveObservingRoutine = null;
            }
        }

        public void EnableTimer(float delaySeconds)
        {
            SetTimerLayoutEnabled(true);
            _timerLayout.EnableTimer(delaySeconds);
        }

        public void DisableTimer() => _timerLayout.DisableTimer(() =>  SetTimerLayoutEnabled(false));

        private void SetTimerLayoutEnabled(bool enabled) => _timerLayout.gameObject.SetActive(enabled);

        private IEnumerator WaveObservingRoutine()
        {
            while (true)
            {
                /// Почему то после вызова <see cref="StopWaveObserving"/> один раз вызывается данный метод
                /// и при этом вызове _botsInWave ожидаемо null
                if (_botsInWave != null)
                    SetWaveProgress(_botsInWave.DeadBotsCount, _botsInWave.TotalBotsCount);
                yield return null;
            }
        }

        private void SetWaveProgress(int deadBotsCount, int totalBotsCount) =>
            _trackingLayout.SetWaveProgress(totalBotsCount - deadBotsCount, totalBotsCount);
    }
}