using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.UI
{
    public interface IUIBotWaveBehavior
    {
        void StartSpawn(int spawnerId);
        void SpawnWave(int spawnerId, int waveIndex, bool arePlannedMoreWaves, [NotNull] IBotsInWave botsInWave);
        void KillBotsOfWave(int spawnerId, int waveIndex);
        void KillBotsOfAllWaves(int spawnerId);
        void WaitWave(int spawnerId, int waveIndex, float delaySeconds);
        void EndAwaitOfWave(int spawnerId, int waveIndex, bool willBeMoreWaves);
    }
}