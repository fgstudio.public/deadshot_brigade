using UnityEngine;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.UI
{
    public sealed class UIByDeathBotWaveBehavior : IUIBotWaveBehavior
    {
        private readonly UIBotWave _sample;
        private readonly Transform _wavesParent;
        private readonly Queue<Wave> _pool = new();
        private readonly Dictionary<int, Wave> _activeWaves = new();
        private readonly HashSet<int> _allowedSpawners = new();

        public UIByDeathBotWaveBehavior(UIBotWave sample, Transform wavesParent)
        {
            _sample = sample;
            _wavesParent = wavesParent;
        }

        public void StartSpawn(int spawnerId) =>
            _allowedSpawners.Add(spawnerId);

        public void SpawnWave(int spawnerId, int waveIndex, bool arePlannedMoreWaves, IBotsInWave botsInWave)
        {
            if (!_allowedSpawners.Contains(spawnerId)) return;

            Wave wave = NewWave(spawnerId, waveIndex);

            wave.ArePlannedMoreWaves = arePlannedMoreWaves;
            wave.Instance.SmoothActivator.TryActivate();
            wave.Instance.DisableTimer();
            wave.Instance.StartWaveObserving(waveIndex, botsInWave);
        }

        public void KillBotsOfWave(int spawnerId, int waveIndex)
        {
            if (_activeWaves.TryGetValue(spawnerId, out Wave wave))
                wave.Instance.StopWaveObserving();
        }

        public void KillBotsOfAllWaves(int spawnerId)
        {
            KillBotsOfWave(spawnerId, default);
            EndAwaitOfWave(spawnerId, default, false);
        }

        public void WaitWave(int spawnerId, int waveIndex, float delaySeconds)
        {
            if (_activeWaves.TryGetValue(spawnerId, out Wave wave) && wave.ArePlannedMoreWaves)
                wave.Instance.EnableTimer(delaySeconds);
        }

        public void EndAwaitOfWave(int spawnerId, int waveIndex, bool willBeMoreWaves)
        {
            if (!willBeMoreWaves && _activeWaves.TryGetValue(spawnerId, out Wave wave))
            {
                _activeWaves.Remove(wave.SpawnerKey);
                _allowedSpawners.Remove(wave.SpawnerKey);

                wave.Instance.SmoothActivator.Deactivate(
                    onComplete: () => _pool.Enqueue(wave));
            }
        }

        private Wave NewWave(int spawnerId, int waveIndex)
        {
            if (_activeWaves.TryGetValue(spawnerId, out Wave spawnerWave))
                return spawnerWave;

            if (_pool.TryDequeue(out Wave wave))
                wave.NewWave(spawnerId, waveIndex);
            else
                wave = new Wave(Object.Instantiate(_sample, _wavesParent), spawnerId, waveIndex);

            _activeWaves[spawnerId] = wave;

            return wave;
        }


        private class Wave
        {
            public int SpawnerKey { get; private set; }
            public int WaveIndex;
            public UIBotWave Instance;
            public bool ArePlannedMoreWaves;

            public Wave(UIBotWave instance, int spawnerKey, int waveIndex)
            {
                SpawnerKey = spawnerKey;
                WaveIndex = waveIndex;
                Instance = instance;
            }

            public void NewWave(int spawnerKey, int waveIndex)
            {
                SpawnerKey = spawnerKey;
                WaveIndex = waveIndex;
            }
        }
    }
}