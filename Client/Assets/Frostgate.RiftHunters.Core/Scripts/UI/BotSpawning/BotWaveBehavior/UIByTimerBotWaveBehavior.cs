using System;
using System.Collections.Generic;
using UnityEngine;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Core.Battle.UI
{
    public sealed class UIByTimerBotWaveBehavior : IUIBotWaveBehavior
    {
        private const int InactiveAwait = 2;

        private readonly UIBotWave _botWaveSample;
        private readonly Transform _wavesParent;
        private readonly Dictionary<int, Wave> _activeWaves = new();
        private readonly Queue<Wave> _pool = new();
        private readonly HashSet<int> _allowedSpawners = new();

        public UIByTimerBotWaveBehavior(UIBotWave sample, Transform wavesParent)
        {
            _botWaveSample = sample;
            _wavesParent = wavesParent;
        }

        public void StartSpawn(int spawnerId) =>
            _allowedSpawners.Add(spawnerId);

        public void SpawnWave(int spawnerId, int waveIndex, bool arePlannedMoreWaves, IBotsInWave botsInWave)
        {
            if (!arePlannedMoreWaves) return;
            if (!_allowedSpawners.Contains(spawnerId)) return;

            Wave wave = null;
            if (_pool.TryDequeue(out wave))
            {
                _activeWaves[spawnerId] = wave;
                wave.NewWave(spawnerId, waveIndex);
            }
            else
            {
                wave = new Wave(Object.Instantiate(_botWaveSample, _wavesParent), spawnerId, waveIndex);
                _activeWaves[spawnerId] = wave;
            }

            wave.Instance.SmoothActivator.Activate();
            wave.Instance.StartWaveObserving(waveIndex, botsInWave);
        }

        public void KillBotsOfAllWaves(int spawnerId) =>
            KillBotsOfWaveForced(spawnerId, default);

        public void KillBotsOfWave(int spawnerId, int waveIndex) =>
            KillBotsOfWave(spawnerId, waveIndex, false);

        private void KillBotsOfWaveForced(int spawnerId, int waveIndex) =>
            KillBotsOfWave(spawnerId, waveIndex, true);

        private void KillBotsOfWave(int spawnerId, int waveIndex, bool forced)
        {
            if (!_activeWaves.TryGetValue(spawnerId, out Wave wave))
                return;

            wave.IsKilled = true;
            wave.Instance.StopWaveObserving();

            if (forced) Deactivate(wave);
            else TryDeactivate(wave);
        }

        public void WaitWave(int spawnerId, int waveIndex, float delaySeconds)
        {
            if (_activeWaves.TryGetValue(spawnerId, out Wave wave))
                wave.Instance.EnableTimer(delaySeconds);
        }

        public void EndAwaitOfWave(int spawnerId, int waveIndex, bool willBeMoreWaves)
        {
            if (!_activeWaves.TryGetValue(spawnerId, out Wave wave))
                return;

            wave.IsEndedAwaiting = true;
            TryDeactivate(wave);
        }

        private void TryDeactivate(Wave wave)
        {
            if (wave.IsEndedAwaiting && wave.IsKilled)
                Deactivate(wave);
        }

        private void Deactivate(Wave wave)
        {
            _activeWaves.Remove(wave.SpawnerKey);
            _allowedSpawners.Remove(wave.SpawnerKey);

            UniTask.Delay(TimeSpan.FromSeconds(InactiveAwait))
                .ContinueWith(
                    () => wave.Instance.SmoothActivator.Deactivate(
                        onComplete: () => _pool.Enqueue(wave)))
                .AsAsyncUnitUniTask();
        }

        private class Wave
        {
            public int SpawnerKey { get; private set; }
            public UIBotWave Instance;
            public int WaveIndex;
            public bool IsKilled;
            public bool IsEndedAwaiting;

            public Wave(UIBotWave instance, int spawnerKey, int waveIndex)
            {
                SpawnerKey = spawnerKey;
                WaveIndex = waveIndex;
                Instance = instance;
            }

            public void NewWave(int spawnerKey, int waveIndex)
            {
                IsKilled = false;
                IsEndedAwaiting = false;
                SpawnerKey = spawnerKey;
                WaveIndex = waveIndex;
            }
        }
    }
}