﻿using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.UI.BotSpawning
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(WaveProgressText))]
    [DisallowMultipleComponent]
    public sealed class WaveProgressText : WaveProgress
    {
        [SerializeField, Required] private TMP_Text _currentText;
        [SerializeField, Required] private TMP_Text _totalText;

        protected override void OnSetTotal(int total) => _totalText.text = total.ToString();
        protected override void OnSetCurrent(int current) => _currentText.text = current.ToString();
    }
}