﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.Battle.UI.BotSpawning
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(WaveProgressBar))]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Slider))]
    public sealed class WaveProgressBar : WaveProgress
    {
        [SerializeField, Required] private UISmoothSlider _slider;

        private void Awake() => InitRefs();

        protected override void OnSetTotal(int total) => _slider.SetMaxValue(total);

        protected override void OnSetCurrent(int current) => _slider.SetValue(current);

        private void InitRefs()
        {
            _slider ??= GetComponent<UISmoothSlider>();
        }
    }
}