﻿using System.Globalization;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.UI.BotSpawning
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(FloatValueText))]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(TMP_Text))]
    public sealed class FloatValueText : Component
    {
        private const string Settings = nameof(Settings);
        private const string Info = nameof(Info);
        private const float MinSymbolSize = 0.1f;
        private const float SymbolSizeByFontSizeCoeff = 0.6f;

        [FoldoutGroup(Settings)] public bool UseCustomFormat;

        [FoldoutGroup(Settings), ShowIf(nameof(UseCustomFormat))]
        public string FloatFormat;

        [FoldoutGroup(Settings), ShowIf(nameof(UseCustomFormat))]
        public string DecimalSeparator;

        public float Value
        {
            get => StringToFloat(_text.text);
            set => SetValue(value);
        }

        [SerializeField, Required] private TMP_Text _text;
        [InfoBox("Устанавливает символам фиксированный размер. " +
            "Если задано слишком маленькое или отрицательное значение, размер равен 'размер шрифта' * 0.6")]
        [SerializeField] private float _monoSymbolsSize = 14f;

        [FoldoutGroup(Info), ShowInInspector, ReadOnly]
        private float _value;

        private NumberFormatInfo _nfi;

        private void Awake()
        {
            InitRefs();
            CreateFormatInfo();
        }

        private void SetValue(float value)
        {
            string text = FloatToString(value);
            float monoSymbolSize = _monoSymbolsSize;
            if (monoSymbolSize < MinSymbolSize)
                monoSymbolSize = _text.fontSize * SymbolSizeByFontSizeCoeff;

            _text.text = $"<mspace={ monoSymbolSize }>{ text }</mspace>";
            _value = value;
        }

        private void InitRefs() => _text ??= GetComponent<TMP_Text>();

        private string FloatToString(float value)
        {
            if (!UseCustomFormat)
                return value.ToString(CultureInfo.InvariantCulture);

            ApplyDecSeparator();
            return value.ToString(FloatFormat, _nfi);
        }

        private void CreateFormatInfo()
        {
            _nfi = new NumberFormatInfo();
        }

        private void ApplyDecSeparator()
        {
            if (_nfi.NumberDecimalSeparator != DecimalSeparator)
                _nfi.NumberDecimalSeparator = DecimalSeparator;
        }

        private float StringToFloat(string value) => UseCustomFormat ? float.Parse(value, _nfi) : float.Parse(value);
    }
}