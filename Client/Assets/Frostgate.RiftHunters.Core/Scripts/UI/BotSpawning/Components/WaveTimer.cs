﻿using System;
using Frostgate.RiftHunters.Core.Components;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.UI.BotSpawning
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(WaveTimer))]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(TimerComponent))]
    public sealed class WaveTimer : Component
    {
        private const string Events = nameof(Events);

        [field: FoldoutGroup(Events), SerializeField]
        public UnityEvent<float> Ticked { get; private set; }

        public float Seconds
        {
            get => _timer.Seconds;
            set => _timer.Seconds = value;
        }

        [SerializeField, Required] private TimerComponent _timer;

        private void Awake() => InitRefs();

        private void OnEnable()
        {
            Subscribe();
            EnableTimer();
        }

        private void OnDisable()
        {
            DisableTimer();
            Unsubscribe();
        }

        private void InitRefs()
        {
            _timer ??= GetComponent<TimerComponent>();
        }

        private void Subscribe()
        {
            _timer.Enabled.AddListener(OnTimerEnabled);
            _timer.Disabled.AddListener(OnTimerDisabled);
            _timer.Ticked.AddListener(OnTicked);
        }

        private void Unsubscribe()
        {
            _timer.Enabled.RemoveListener(OnTimerEnabled);
            _timer.Disabled.RemoveListener(OnTimerDisabled);
            _timer.Ticked.RemoveListener(OnTicked);
        }

        private void EnableTimer() => _timer.Enable();
        private void DisableTimer() => _timer.Disable();

        private void OnTicked()
        {
            float leftTime = GetLeftTime();
            Ticked.Invoke(leftTime);
        }

        private void OnTimerEnabled() => Enable();
        private void OnTimerDisabled() => Disable();

        private float GetLeftTime()
        {
            TimeSpan leftTime = _timer.Left;
            return Mathf.Clamp((float)leftTime.TotalSeconds, 0, _timer.Seconds);
        }
    }
}