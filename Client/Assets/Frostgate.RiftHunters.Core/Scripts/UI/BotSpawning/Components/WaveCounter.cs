﻿using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.UI.BotSpawning
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(WaveCounter))]
    [DisallowMultipleComponent]
    public sealed class WaveCounter : Component
    {
        private const string Info = nameof(Info);

        public int Count
        {
            get => _count;
            set => SetCount(value);
        }

        [SerializeField, Required] private TMP_Text _text;

        [FoldoutGroup(Info), ShowInInspector, ReadOnly]
        private int _count;

        private void SetCount(int count)
        {
            _text.text = count.ToString();
            _count = count;
        }
    }
}