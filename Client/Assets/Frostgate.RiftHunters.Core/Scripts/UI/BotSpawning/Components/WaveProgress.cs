﻿using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.UI.BotSpawning
{
    public abstract class WaveProgress : Component
    {
        private const string Info = nameof(Info);

        public int Total
        {
            get => _total;
            set
            {
                OnSetTotal(value);
                _total = value;
            }
        }

        public int Current
        {
            get => _current;
            set
            {
                OnSetCurrent(value);
                _current = value;
            }
        }

        [FoldoutGroup(Info), ShowInInspector, ReadOnly]
        private int _total;

        [FoldoutGroup(Info), ShowInInspector, ReadOnly]
        private int _current;

        protected abstract void OnSetTotal(int total);
        protected abstract void OnSetCurrent(int current);
    }
}
