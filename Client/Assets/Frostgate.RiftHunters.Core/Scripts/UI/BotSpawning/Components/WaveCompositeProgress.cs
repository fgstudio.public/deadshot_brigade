﻿using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.UI.BotSpawning
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(WaveCompositeProgress))]
    [DisallowMultipleComponent]
    public sealed class WaveCompositeProgress : WaveProgress
    {
        [SerializeField] private WaveProgress[] _waveProgressCollection;

        private void Awake() => InitRefs();

        protected override void OnSetTotal(int total) => IterateProgressCollection(e => e.Total = total);

        protected override void OnSetCurrent(int current) => IterateProgressCollection(e => e.Current = current);

        private void InitRefs()
        {
            _waveProgressCollection ??= GetComponentsInChildren<WaveProgress>();
        }

        private void IterateProgressCollection(Action<WaveProgress> each)
        {
            foreach (WaveProgress p in _waveProgressCollection)
                each.Invoke(p);
        }
    }
}