﻿using System;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(LayoutElement))]
[RequireComponent(typeof(RectMask2D))]
public class RisingAnimationComponent : MonoBehaviour
{
    [SerializeField, ChildGameObjectsOnly, Required] private RectTransform _visualRectTransform;
    [SerializeField, ChildGameObjectsOnly, Required] private RectTransform _rectTransform;
    [SerializeField, ChildGameObjectsOnly, Required] private LayoutElement _layoutElement;
    [SerializeField] private float animationDuration = 1.0f;

    private Coroutine _animationCoroutine;

    private void OnValidate()
    {
        _rectTransform ??= transform as RectTransform;
        _layoutElement ??= GetComponent<LayoutElement>();
    }

    public void Hide(Action onComplete = null) => StartAnimation(false, onComplete);

    public void Show(Action onComplete = null) => StartAnimation(true, onComplete);

    private void StartAnimation(bool show, Action onComplete)
    {
        if (this == null)
            return;

        if (_animationCoroutine != null)
            StopCoroutine(_animationCoroutine);

        if (gameObject.activeInHierarchy)
            _animationCoroutine = StartCoroutine(
                DoAnimation(show, animationDuration, onComplete));
    }

    private IEnumerator DoAnimation(bool show, float duration, Action onComplete)
    {
        float startHeight;
        float endHeight;
        float startWidth;
        float endWidth;

        if (show)
        {
            startHeight = 0f;
            endHeight = _visualRectTransform.rect.height;
            startWidth = 0f;
            endWidth = _visualRectTransform.rect.width;
        }
        else
        {
            startHeight = _rectTransform.rect.height;
            endHeight = 0f;
            startWidth = _rectTransform.rect.width;
            endWidth = 0f;
        }

        float time = 0.0f;
        while (time < duration)
        {
            _layoutElement.preferredHeight = Mathf.Lerp(startHeight, endHeight, time / duration);
            _layoutElement.preferredWidth = Mathf.Lerp(startWidth, endWidth, time / duration);
            yield return null;
            time += Time.deltaTime;
        }

        _layoutElement.preferredHeight = endHeight;
        _layoutElement.preferredWidth = endWidth;

        onComplete?.Invoke();
    }
}