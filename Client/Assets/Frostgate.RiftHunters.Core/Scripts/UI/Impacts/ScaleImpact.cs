using DG.Tweening;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Impacts
{
    /// <summary>
    /// Реакция с изменением масштаба элемента.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Impacts.Menu + "/" + nameof(ScaleImpact))]
    public sealed class ScaleImpact : MonoBehaviour
    {
        private const float MinScale = 0;
        private const float MaxScale = 5;

        private const float MinDuration = 0;
        private const float MaxDuration = 5;

        [Header("References")]
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private RectTransform _rectTransform;

        [Header("Settings")]
        [Range(MinScale, MaxScale)] public float TargetScale = 1;
        [Range(MinDuration, MaxDuration)] public float Duration;
        public bool IgnoreTimeScale = true;

        private Vector3 _initialScale;
        private Tweener _tween;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() => _rectTransform ??= GetComponent<RectTransform>();

        [ContextMenu(nameof(Impact))]
        public void Impact()
        {
            const int loopsCount = 2;

            Stop();
            CacheScale();

            _tween = _rectTransform
                .DOScale(_initialScale * TargetScale, Duration / loopsCount)
                .SetLoops(loopsCount, LoopType.Yoyo)
                .SetUpdate(IgnoreTimeScale);
        }

        [ContextMenu(nameof(Stop))]
        public void Stop()
        {
            if (_tween == null)
                return;

            _tween?.Kill();
            ResetScaleFromCache();
        }

        private void CacheScale() => _initialScale = _rectTransform.localScale;
        private void ResetScaleFromCache() => _rectTransform.localScale = _initialScale;
    }
}