using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIScreenHeader : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private Button _backButton;

        public Button BackButton => _backButton;
    }
}
