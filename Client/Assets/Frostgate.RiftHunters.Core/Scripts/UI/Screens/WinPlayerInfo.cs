namespace Frostgate.RiftHunters.Core.Network.Shared
{
    public class WinPlayerInfo
    {
        public bool IsLocalPlayer;
        public string HeroId;
        public string UserName;
        public int PlayerScore;
    }
}