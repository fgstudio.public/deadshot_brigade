using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    public sealed class UICardsColors : MonoBehaviour
    {
        [Serializable]
        public class RarityColor
        {
            public EquipmentRarity Rarity;
            public Color Color;
        }

        [Serializable]
        public class HeroRarityColor
        {
            public HeroRarity Rarity;
            public Color Color;
        }

        [SerializeField] private RarityColor[] _rarityColorsSettings;
        [SerializeField] private HeroRarityColor[] _heroRarityColorsSettings;

        private Dictionary<EquipmentRarity, Color> _rarityColors;
        private Dictionary<HeroRarity, Color> _heroRarityColors;

        private void Awake()
        {
            _rarityColors = new Dictionary<EquipmentRarity, Color>();
            _heroRarityColors = new Dictionary<HeroRarity, Color>();

            foreach (var rarityColors in _rarityColorsSettings)
                _rarityColors.Add(rarityColors.Rarity, rarityColors.Color);

            foreach (var heroRarityColor in _heroRarityColorsSettings)
                _heroRarityColors.Add(heroRarityColor.Rarity, heroRarityColor.Color);
        }

        public Color GetColorByRarity(EquipmentRarity rarity) => _rarityColors[rarity];

        public Color GetColorByRarity(HeroRarity rarity) => _heroRarityColors[rarity];
    }
}
