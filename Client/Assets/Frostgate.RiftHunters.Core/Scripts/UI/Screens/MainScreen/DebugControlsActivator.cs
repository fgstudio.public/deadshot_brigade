using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu(ComponentMenus.RiftHunters.UI.Menu + "/" + nameof(DebugControlsActivator))]
    public sealed class DebugControlsActivator : Component
    {
        [Header("References")] [Required, ChildGameObjectsOnly]
        public GameObject[] _controls = Array.Empty<GameObject>();

        protected override void OnEnabled() => SetActive(true);
        protected override void OnDisabled() => SetActive(false);

        private void SetActive(bool isActive) => _controls.ForEach(e => e.SetActive(isActive));
    }
}