using System.Linq;
using Frostgate.RiftHunters.Infrastructure;
using Frostgate.RiftHunters.Meta;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UICampaignButton : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private Image _regionImage;
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _regionName;
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _phaseGearscore;
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _progressBarValue;
        [SerializeField, Required, ChildGameObjectsOnly]
        private Transform _progressBarFill;

        private CampaignService _campaignService;
        private CampaignRoster _campaignRoster;

        [Inject]
        private void MonoConstructor(Preferences preferences, CampaignService campaignService)
        {
            _campaignRoster = preferences.CampaignRoster;
            _campaignService = campaignService;
        }

        private void OnEnable()
        {
            var currentPhase = _campaignService.GetCurrentPhase();
            var currentRegion = _campaignRoster.GetRegion(currentPhase);

            _regionImage.sprite = currentRegion.Sprite;
            _regionName.text = currentRegion.Name.ToUpper();
            _phaseGearscore.text = currentPhase.RecommendedGearScore.ToString();

            var score = _campaignService.GetScore(currentPhase);
            SetProgressBar(score, currentPhase.MaxScore);
        }

        private void SetProgressBar(int currentValue, int maxValue)
        {
            _progressBarValue.text = $"{currentValue}/{maxValue}";
            _progressBarFill.localScale = new Vector3((float)currentValue / maxValue, 1f, 1f);
        }
    }
}
