using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Services.PlayerData;
using Frostgate.RiftHunters.Core.UI.Screens;
using Frostgate.RiftHunters.Core.UI.Toasters;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Player;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [UIScreenPrefab("UIMainScreen")]
    public sealed class UIMainScreen : UIScreen
    {
        private readonly string _comingSoonMessage = "Coming soon";

        [SerializeField, FoldoutGroup("Events")]
        private UnityEvent _startServerRequested = new();

        [SerializeField, FoldoutGroup("Events")]
        private UnityEvent _startSingleplayerRequested = new();

        [SerializeField, FoldoutGroup("Events")]
        private UnityEvent _startMultiplayerRequested = new();

        [SerializeField, FoldoutGroup("Events")]
        private UnityEvent _reconnectRequested = new();

        #region References

        [SerializeField, Required, FoldoutGroup("References")]
        private Button _startServerButton;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Button _startSingleplayerButton;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Button _startMultiplayerButton;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Button _reconnectButton;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private TMP_InputField _serverAddrInputField;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private TMP_InputField _userNameInputField;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private TMP_Dropdown _missionSelectionDropdown;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIModalViewMediator _modalViewMediator;

        [SerializeField, Required, FoldoutGroup("References")]
        private DebugControlsActivator _debugControls;

        [SerializeField, Required, FoldoutGroup("References")]
        private Button _heroInfoButton;

        [SerializeField, Required, FoldoutGroup("References")]
        private Button _inventoryButton;

        [SerializeField, Required, FoldoutGroup("References")]
        private Button _campaignButton;

        [SerializeField, Required, FoldoutGroup("References")]
        private UISmoothActivator _smoothActivator;

        [SerializeField, Required, FoldoutGroup("References")]
        private Button[] _commingSoonFeaturesButtons;

        [SerializeField, Required, FoldoutGroup("References")]
        private TMP_Text _unitName;

        [SerializeField, Required, FoldoutGroup("References")]
        private Image _unitClassIcon;

        [SerializeField, Required, FoldoutGroup("References")]
        private TMP_Text _gearScoreValue;

        #endregion

        public event Action StartServerRequested = delegate { };
        public event Action StartSingleplayerRequested = delegate { };
        public event Action StartMultiplayerRequested = delegate { };
        public event Action ReconnectRequested = delegate { };


        public bool IsReconnectAllowed
        {
            get => _isReconnectAllowed;
            set
            {
                if (_isReconnectAllowed == value)
                    return;

                _isReconnectAllowed = value;
                OnIsReconnectAllowedValueChanged(value);
            }
        }

        private IPlayerIdStorage _playerIdStorage;
        private IPreferences _preferences;
        private IUIService _uiService;
        private PlayerDataService _playerDataService;
        private GearScoreProvider _gearScoreProvider;
        private IToastService _toastService;

        private IReadOnlyList<IMissionConfig> _missionConfigs;
        private IReadOnlyList<Hero> _heroes;
        private CharacterSelectionView _characterSelectionView;

        private int _selectedUnitConfigIndex;

        private IMissionConfig _selectedMission;
        private bool _isReconnectAllowed;

        [Inject]
        private void MonoConstructor(
            [NotNull] IPlayerIdStorage playerIdStorage,
            [NotNull] PlayerDataService playerDataService,
            [NotNull] GearScoreProvider gearScoreService,
            [NotNull] IPreferences preferences, [NotNull] IToastService toastService)
        {
            _playerIdStorage = playerIdStorage;
            _playerDataService = playerDataService;
            _gearScoreProvider = gearScoreService;
            _preferences = preferences;
            _toastService = toastService;
        }

        private void OnDestroy() => Unsubscribe();

        public async UniTask InitializeAsync([NotNull] IEnumerable<Hero> heroes,
            [NotNull] IReadOnlyList<IMissionConfig> missionConfigs, IUIService uiService)
        {
            _uiService = uiService;
            _heroes = heroes.ToList();
            _debugControls.SetEnabled(_preferences.IsDebugModeEnabled);
            OnIsReconnectAllowedValueChanged(false);

            InitMissionsSelectionDropdown(missionConfigs);

            InitUserNameInputField(_playerDataService.GetName());

            _characterSelectionView ??=
                FindObjectOfType<CharacterSelectionView>(); // TODO: Провести отпимизацию и избавиться от FindObjOfType
            ShowCurrentHero();

            Subscribe();
        }

        public void ShowErrorModalView(Action onStartClientButtonClicked)
        {
            _modalViewMediator.SmoothActivator.Activate();
            _modalViewMediator.SetTitle("Connection error");
            _modalViewMediator.SetupGreyButton("Exit", () => _modalViewMediator.SmoothActivator.Deactivate());
            _modalViewMediator.SetupGreenButton("Reconnect",
                () => _modalViewMediator.SmoothActivator.Deactivate(null, onStartClientButtonClicked));
        }

        public ScreenData GetScreenData()
        {
            var newPlayerName = _userNameInputField.text;
            _playerDataService.SetNameAsync(newPlayerName);

            PlayerDto player = new(_playerIdStorage.PlayerId, newPlayerName);
            string serverAddr = _serverAddrInputField.text;

            return new ScreenData(player, _selectedMission, serverAddr);
        }


        private void InitMissionsSelectionDropdown(IReadOnlyList<IMissionConfig> missionConfigs)
        {
            _missionConfigs = missionConfigs;

            _missionSelectionDropdown.ClearOptions();
            _missionSelectionDropdown.options =
                _missionConfigs.Select(c => new TMP_Dropdown.OptionData(c.MissionName)).ToList();

            SelectMission(0);
        }

        private void InitUserNameInputField(string playerName)
        {
            _userNameInputField.text = playerName;
            if (_userNameInputField.text.Length > _userNameInputField.characterLimit)
                _userNameInputField.text = _userNameInputField.text.Substring(0, _userNameInputField.characterLimit);
        }

        private void Subscribe()
        {
            _startServerButton.onClick.AddListener(OnStartServerButtonClicked);
            _startSingleplayerButton.onClick.AddListener(OnStartSingleplayerButtonClicked);
            _startMultiplayerButton.onClick.AddListener(OnStartMultiplayerButtonClicked);
            _reconnectButton.onClick.AddListener(OnReconnectButtonClicked);
            _heroInfoButton.onClick.AddListener(OnHeroInfoButtonClicked);
            _inventoryButton.onClick.AddListener(OnInventoryButtonClicked);
            _campaignButton.onClick.AddListener(OnCampaignButtonClicked);

            _missionSelectionDropdown.onValueChanged.AddListener(OnMissionsDropdownValueChanged);

            foreach (var button in _commingSoonFeaturesButtons)
                button.onClick.AddListener(ShowComingSoonToaster);
        }

        private void Unsubscribe()
        {
            _startServerButton.onClick.RemoveListener(OnStartServerButtonClicked);
            _startSingleplayerButton.onClick.RemoveListener(OnStartSingleplayerButtonClicked);
            _startMultiplayerButton.onClick.RemoveListener(OnStartMultiplayerButtonClicked);
            _reconnectButton.onClick.RemoveListener(OnReconnectButtonClicked);
            _heroInfoButton.onClick.RemoveListener(OnHeroInfoButtonClicked);
            _inventoryButton.onClick.RemoveListener(OnInventoryButtonClicked);
            _campaignButton.onClick.RemoveListener(OnCampaignButtonClicked);

            _missionSelectionDropdown.onValueChanged.RemoveListener(OnMissionsDropdownValueChanged);

            foreach (var button in _commingSoonFeaturesButtons)
                button.onClick.RemoveListener(ShowComingSoonToaster);
        }

        private void OnStartServerButtonClicked()
        {
            _startServerRequested.Invoke();
            StartServerRequested.Invoke();
        }

        private void OnStartSingleplayerButtonClicked()
        {
            _startSingleplayerRequested.Invoke();
            StartSingleplayerRequested.Invoke();
        }

        private void OnStartMultiplayerButtonClicked()
        {
            _startMultiplayerRequested.Invoke();
            StartMultiplayerRequested.Invoke();
        }

        private void OnReconnectButtonClicked()
        {
            _reconnectRequested.Invoke();
            ReconnectRequested.Invoke();
        }

        private void OnHeroInfoButtonClicked() => OpenHeroInfoScreen();

        private void OnInventoryButtonClicked() => OpenInventoryScreen();

        private void OnCampaignButtonClicked() =>  OpenCampaignScreen();

        private async void OpenHeroInfoScreen()
        {
            _uiService.Hide();

            UIHeroInfoScreen heroInfoScreen = await _uiService.ShowAsync<UIHeroInfoScreen>();
            await heroInfoScreen.InitializeAsync(_heroes, _playerDataService, _characterSelectionView, OnHeroInfoClose);
        }

        private async void OpenInventoryScreen()
        {
            _uiService.Hide();

            UIInventoryScreen inventoryScreen = await _uiService.ShowAsync<UIInventoryScreen>();
            await inventoryScreen.InitializeAsync(OnHeroInfoClose);
        }

        private async void OpenCampaignScreen()
        {
            _uiService.Hide();

            UICampaignScreen inventoryScreen = await _uiService.ShowAsync<UICampaignScreen>();
            await inventoryScreen.InitializeAsync(OnHeroInfoClose);
        }

        private void OnHeroInfoClose()
        {
            _uiService.ShowPrevious();
            ShowCurrentHero();
        }

        private void OnMissionsDropdownValueChanged(int index) => SelectMission(index);

        private void OnIsReconnectAllowedValueChanged(bool isAllowed)
        {
            _reconnectButton.gameObject.SetActive(isAllowed);
            _startMultiplayerButton.gameObject.SetActive(!isAllowed);
        }


        private void SelectMission(int index)
        {
            IMissionConfig newSelectedMission = _missionConfigs[index];
            if (_selectedMission == newSelectedMission)
                return;

            _selectedMission = _missionConfigs[index];
        }

        private void ShowCurrentHero()
        {
            var hero = _playerDataService.GetSelectedHero();
            _characterSelectionView.ShowHero(hero);

            _unitName.text = hero.Config.Name;
            _unitClassIcon.sprite = hero.Config.RoleIcon;
            _gearScoreValue.text = _gearScoreProvider.CalcTotalHeroGearScore(hero.Id).ToString();
        }

        private void ShowComingSoonToaster() =>
            _toastService.Show(_comingSoonMessage);

        public readonly struct ScreenData
        {
            public PlayerDto Player { get; }
            public IMissionConfig MissionConfig { get; }
            public string ServerAddress { get; }

            public ScreenData(PlayerDto player, IMissionConfig missionConfig, string serverAddress)
            {
                Player = player;
                MissionConfig = missionConfig;
                ServerAddress = serverAddress;
            }
        }
    }
}