using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIDevelopmentMenu : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private RectTransform _rect;
        [SerializeField, Required, ChildGameObjectsOnly]
        private Button _button;
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _titleText;

        [SerializeField] private float _minHeight = 60f;
        [SerializeField] private float _maxHeight = 420f;

        private bool _isDeployed;

        private string _showMenuText = "Show Development Menu";
        private string _hideMenuText = "Hide Development Menu";

        private void Awake()
        {
            Hide();
            _button.onClick.AddListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            if (_isDeployed)
                Hide();
            else
                Show();
        }

        private void Show()
        {
            _titleText.text = _hideMenuText;
            _isDeployed = true;
            _rect.sizeDelta = new Vector2(_rect.rect.width, _maxHeight);
        }

        private void Hide()
        {
            _titleText.text = _showMenuText;
            _isDeployed = false;
            _rect.sizeDelta = new Vector2(_rect.rect.width, _minHeight);
        }
    }
}
