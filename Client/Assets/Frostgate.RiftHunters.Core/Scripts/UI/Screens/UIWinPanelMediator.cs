using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Frostgate.RiftHunters.Core.Network.Shared;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI
{
    public interface IUIWinPanelMediator
    {
        void SetScoresTable(IEnumerable<WinPlayerInfo> winPlayerInfos,
            TimeSpan sessionDuration, int starsCount, UnityAction onExitClicked);
    }

    public class UIWinPanelMediator : MonoBehaviour, IUIWinPanelMediator
    {
        [Header("Dependencies")]
        [SerializeField, Required] private UISmoothActivator _smoothActivator;

        [Header("References")]
        [SerializeField, Required] private UIWinPlayerInfoPanel _winPlayerInfoPanelPrefab;
        [SerializeField, Required] private Transform _playerInfoContainer;
        [SerializeField, Required] private Button _exitButton;
        [SerializeField, Required] private TMP_Text _timerText;
        [SerializeField, Required] private GameObject[] _stars;

        [Header("Settings")]
        [SerializeField, Range(0f, 5f), SuffixLabel("sec", true)] private float _starsActivationTime = 2f;

        public ISmoothActivator SmoothActivator => _smoothActivator;

        private IPrefabFactory _prefabFactory;

        [Inject]
        private void MonoConstructor(IPrefabFactory prefabFactory) =>
            _prefabFactory = prefabFactory;

        private void OnDestroy() =>
            _exitButton.onClick.RemoveAllListeners();

        public void SetScoresTable(IEnumerable<WinPlayerInfo> winPlayerInfos,
            TimeSpan sessionDuration, int starsCount, UnityAction onExitClicked)
        {
            _exitButton.onClick.RemoveAllListeners();
            _exitButton.onClick.AddListener(onExitClicked);

            IOrderedEnumerable<WinPlayerInfo> orderedInfos =
                winPlayerInfos.OrderByDescending((p) => p.PlayerScore);

            foreach (WinPlayerInfo playerInfo in orderedInfos)
            {
                UIWinPlayerInfoPanel infoPanel = _prefabFactory
                    .Instantiate<UIWinPlayerInfoPanel>(_winPlayerInfoPanelPrefab, _playerInfoContainer);
                infoPanel.Init(playerInfo);
            }

            _timerText.text = sessionDuration.ToString("mm':'ss");
            StartCoroutine(StarDisplayRoutine(starsCount));
        }

        private IEnumerator StarDisplayRoutine(int starsCount)
        {
            float timeBetweenStarsActivation = _starsActivationTime / _stars.Length;
            var waitRoutine = new WaitForSeconds(timeBetweenStarsActivation);

            _stars.ForEach(s => s.SetActive(false));

            for (int i = 0; i < Mathf.Min(starsCount, _stars.Length); i++)
            {
                yield return waitRoutine;
                _stars[i].SetActive(true);
            }
        }
    }
}