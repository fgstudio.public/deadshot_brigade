using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    public static class UIRankTextHelper
    {
        private static readonly string[] _romanNumerals = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X" };

        public static string ConvertRankToString(int rank) => _romanNumerals[rank];
    }
}
