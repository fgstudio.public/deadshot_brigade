using DG.Tweening;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class PulsationEffect : MonoBehaviour
    {
        [SerializeField, Required] private Transform _transform;
        [SerializeField] private Vector3 _scale;
        [SerializeField] private float _time;
        private Vector3 _originScale;
        private Tweener _tweener;
        private void OnValidate() => _transform ??= GetComponent<Transform>();

        private void OnEnable()
        {
            _originScale = _transform.localScale;
            Play();
        }

        private void OnDisable() => Stop();

        private void Play()
        {
            Stop();
            _tweener = _transform.DOScale(_scale, _time).SetLoops(-1, LoopType.Yoyo);
        }

        private void Stop()
        {
            _tweener?.Kill();
            _transform.localScale = _originScale;
        }
    }
}
