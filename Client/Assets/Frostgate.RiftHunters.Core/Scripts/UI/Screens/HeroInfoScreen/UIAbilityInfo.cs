using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Battle.Impact.Client;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    public sealed class UIAbilityInfo : MonoBehaviour, ITooltipInfo
    {
        [SerializeField, Required, ChildGameObjectsOnly] private Image _icon;

        private string _abilityName;
        private string _abilityDescription;

        public void SetInfo([NotNull] IConfigCollection<ImpactUIConfig> impactUIConfigs, ImpactConfig abilityConfig)
        {
            string abilityConfigId = abilityConfig?.Id ?? string.Empty;
            impactUIConfigs!.TryGet(abilityConfigId, out ImpactUIConfig uiAbilityConfig);

            _abilityName = uiAbilityConfig?.Name ?? "";
            _abilityDescription = uiAbilityConfig?.Description ?? "";

            _icon.sprite = uiAbilityConfig?.Icon;
        }

        public TooltipData GetInfo() =>
            new (_abilityName, _abilityDescription);
    }
}
