using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    public sealed class UIHeroCard : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _level;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _gearscore;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _heroImage;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _classIcon;
        [SerializeField, Required, ChildGameObjectsOnly] private Button _button;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _selectionFrame;

        public void SetActive(bool active) =>
            _selectionFrame.SetActive(active);

        public void Init([NotNull] HeroCardData data, int index, UnityAction<int> onHeroCardClick)
        {
            _heroImage.sprite = data.Image;
            _classIcon.sprite = data.ClassIcon;
            _level.text = data.Level.ToString();
            _gearscore.text = data.Gearscore.ToString();
            _button.onClick.AddListener(() => onHeroCardClick.Invoke(index));
        }

        public void SetLockedCardCallback(UnityAction onHeroCardClick) =>
            _button.onClick.AddListener(onHeroCardClick);
    }
}