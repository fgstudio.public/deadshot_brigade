using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Economy;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    public sealed class UIHeroCharacteristicsPanel : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _healthValue;
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _staminaValue;
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _staminaRecoverValue;
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _meleeDamageValue;
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _speedValue;

        private HeroUpgradeService _heroUpgradeService;
        private UnitPropertiesProviderFactory _unitPropertiesProviderFactory;

        [Inject]
        private void MonoConstructor(HeroUpgradeService heroUpgradeService, UnitPropertiesProviderFactory unitPropertiesProviderFactory)
        {
            _heroUpgradeService = heroUpgradeService;
            _unitPropertiesProviderFactory = unitPropertiesProviderFactory;
        }

        public void SetValues(UnitConfig unitConfig)
        {
            int heroLevel = _heroUpgradeService.GetHeroLevel(unitConfig.Id);

            var unitProperties = _unitPropertiesProviderFactory.Create(unitConfig, heroLevel);


            _healthValue.text = unitProperties.Health.ToString();
            _staminaValue.text = unitProperties.Stamina.ToString();
            _staminaRecoverValue.text = unitProperties.StaminaAutoRecovery?.Speed.ToString();
            _meleeDamageValue.text = unitProperties.MeleeWeapon?.GetDamage().ToString();
            _speedValue.text = unitProperties.Speed.ToString();
        }
    }
}
