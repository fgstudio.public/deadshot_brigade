using System;
using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    public sealed class UIEquipmentPanel : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIClassIcons _classIcons;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UICardsColors _cardsColors;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIEquipmentSlot _firstWeaponSlot;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIEquipmentSlot _secondWeaponSlot;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIEquipmentSlot _headSlot;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIEquipmentSlot _bodySlot;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIEquipmentSlot _heandsSlot;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIEquipmentSlot _footSlot;

        private EquipmentService _equipmentService;

        [Inject]
        private void MonoConstructor(EquipmentService equipmentService) =>
            _equipmentService = equipmentService;

        public void UpdateEquipmentCards(string heroId)
        {
            ClearSlots();

            var equipments = _equipmentService.FindEquippedItems(heroId).ToArray();

            bool firstWeaponSlotFree = true;
            foreach (var equipment in equipments)
            {
                ItemCardData itemData = new ItemCardData()
                {
                    Rank = equipment.Rank,
                    IsNew = false,
                    RarityColor = _cardsColors.GetColorByRarity(equipment.Rarity),
                    ClassIcon = _classIcons.GetClassIcon(equipment.Class),
                    ItemIcon = equipment.Icon,
                    Gearscore = equipment.GearScore
                };


                switch (equipment.Type)
                {
                    case EquipmentType.Head:
                        _headSlot.SetEquipmentCard(itemData);
                        break;
                    case EquipmentType.Body:
                        _bodySlot.SetEquipmentCard(itemData);
                        break;
                    case EquipmentType.Arms:
                        _heandsSlot.SetEquipmentCard(itemData);
                        break;
                    case EquipmentType.Legs:
                        _footSlot.SetEquipmentCard(itemData);
                        break;
                    case EquipmentType.RangeWeapon:
                        if (firstWeaponSlotFree)
                        {
                            _firstWeaponSlot.SetEquipmentCard(itemData);
                            firstWeaponSlotFree = false;
                        }
                        else
                        {
                            _secondWeaponSlot.SetEquipmentCard(itemData);
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }


        private void ClearSlots()
        {
            _firstWeaponSlot.RemoveEquipmentCard();
            _secondWeaponSlot.RemoveEquipmentCard();
            _headSlot.RemoveEquipmentCard();
            _bodySlot.RemoveEquipmentCard();
            _heandsSlot.RemoveEquipmentCard();
            _footSlot.RemoveEquipmentCard();
        }
    }
}
