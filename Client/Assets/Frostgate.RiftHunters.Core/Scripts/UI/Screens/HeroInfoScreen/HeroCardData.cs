using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    public sealed class HeroCardData
    {
        public Sprite Image;
        public Sprite ClassIcon;
        public int Level;
        public int Gearscore;
        public Color RarityColor;

        public HeroCardData()
        {
        }
    }
}
