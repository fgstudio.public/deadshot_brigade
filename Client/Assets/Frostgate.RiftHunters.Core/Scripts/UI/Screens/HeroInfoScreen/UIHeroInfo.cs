using TMPro;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Impact.Client;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    public class UIHeroInfo : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _name;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _class;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _currentLevel;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _maxLevel;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _gearscore;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _rank;
        [SerializeField, Required] private UIHeroCharacteristicsPanel _characteristicsPanel;
        [SerializeField, Required] private UIEquipmentPanel _equipmentPanel;
        [SerializeField, Required, ChildGameObjectsOnly] private UIAbilityInfo _ultimate;
        [SerializeField, Required, ChildGameObjectsOnly] private UIAbilityInfo _ability;
        [SerializeField, Required, ChildGameObjectsOnly] private UIAbilityInfo _extraAbility;

        [CanBeNull] private IConfigCollection<ImpactUIConfig> _impactUIConfigs;
        private UIAbilityInfo _activeAbilityInfoTooltip;
        private HeroUpgradeService _heroUpgradeService;
        private GearScoreProvider _gearScoreProvider;

        [Inject]
        private void MonoConstructor(IConfigCollection<ImpactUIConfig> impactUIConfigs,
            [NotNull] HeroUpgradeService heroUpgradeService,
            [NotNull] GearScoreProvider gearScoreProvider)
        {
            _impactUIConfigs = impactUIConfigs;
            _heroUpgradeService = heroUpgradeService;
            _gearScoreProvider = gearScoreProvider;
        }

        public void SetData([NotNull] Hero hero)
        {
            SetHeroInfo(hero.Config, hero.Id);
            SetAbilitiesInfo(hero.Config);
        }

        private void SetHeroInfo([NotNull] UnitConfig unitConfig, string heroId)
        {
            _name.text = unitConfig.Name;
            _class.text = unitConfig.Class.ToString();
            _gearscore.text = _gearScoreProvider.CalcTotalHeroGearScore(heroId).ToString();
            _currentLevel.text = _heroUpgradeService.GetHeroLevel(heroId).ToString();
            _rank.text = UIRankTextHelper.ConvertRankToString(_heroUpgradeService.GetHeroRank(heroId));
            _characteristicsPanel.SetValues(unitConfig);
            _equipmentPanel.UpdateEquipmentCards(heroId);
        }

        private void SetAbilitiesInfo([NotNull] UnitConfig unitConfig)
        {
            _ultimate.SetInfo(_impactUIConfigs, unitConfig.Impacts.Ultimate.Config);
            _ability.SetInfo(_impactUIConfigs, unitConfig.Impacts.Ability.Config);
            _extraAbility.SetInfo(_impactUIConfigs, unitConfig.Impacts.ExtraAbility.Config);
        }
    }
}