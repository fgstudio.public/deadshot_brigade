using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    public sealed class UIEquipmentSlot : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private Button _button;
        [SerializeField, Required, ChildGameObjectsOnly]
        private UIItemCardEquipment _itemCard;

        public void SetEquipmentCard(ItemCardData itemData)
        {
            _itemCard.gameObject.SetActive(true);
            _itemCard.Init(itemData, null);
        }

        public void RemoveEquipmentCard() =>
            _itemCard.gameObject.SetActive(false);
    }
}
