using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    public sealed class UIHeroListPanel : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _container;
        [SerializeField, Required] private UIHeroCard _heroInfoPrefab;
        [SerializeField, Required] private UICardsColors _cardsColors;
        [SerializeField, Required] private UIClassIcons _classIcons;

        private Dictionary<Hero, UIHeroCard> _heroListItems;
        private UIHeroCard _lastActiveItem;

        private HeroUpgradeService _heroUpgradeService;
        private GearScoreProvider _gearScoreProvider;

        [Inject]
        private void MonoConstructor(HeroUpgradeService heroUpgradeService, GearScoreProvider gearScoreProvider)
        {
            _heroUpgradeService = heroUpgradeService;
            _gearScoreProvider = gearScoreProvider;
        }

        public void Initialize(IEnumerable<Hero> heroes, UnityAction<int> onHeroListItemClick)
        {
            var heroesArray = heroes.ToArray();
            _heroListItems = new Dictionary<Hero, UIHeroCard>();

            for (int i = 0; i < heroesArray.Length; i++)
            {
                var listItem = Instantiate(_heroInfoPrefab, _container);
                var heroConfig = heroesArray[i].Config;
                HeroCardData cardData = new HeroCardData()
                {
                    Image = heroConfig.Sprite,
                    ClassIcon = _classIcons.GetClassIcon(heroConfig.Class),
                    Level = _heroUpgradeService.GetHeroLevel(heroConfig.Id),
                    Gearscore = _gearScoreProvider.CalcTotalHeroGearScore(heroConfig.Id),
                    RarityColor = _cardsColors.GetColorByRarity(heroesArray[i].Rarity)
                };
                listItem.Init(cardData, i, onHeroListItemClick);
                listItem.SetActive(false);

                _heroListItems.Add(heroesArray[i], listItem);
            }
        }

        public void SetActive(Hero availableHero)
        {
            _heroListItems.TryGetValue(availableHero, out var item);

            if (item != null && item != _lastActiveItem)
            {
                item.SetActive(true);
                if(_lastActiveItem)
                    _lastActiveItem.SetActive(false);

                _lastActiveItem = item;
            }
        }
    }
}