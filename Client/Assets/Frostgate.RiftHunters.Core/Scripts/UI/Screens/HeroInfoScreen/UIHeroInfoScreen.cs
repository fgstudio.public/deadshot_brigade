using System.Linq;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Systems.Input.UI;
using Frostgate.RiftHunters.Core.UI.Toasters;
using Frostgate.RiftHunters.Meta.Player;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    [DisallowMultipleComponent]
    [UIScreenPrefab("UIHeroInfoScreen")]
    public sealed class UIHeroInfoScreen : UIScreen
    {
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIScreenHeader _screenHeader;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Button _confirmButton;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIHeroInfo _heroInfo;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIHeroListPanel _heroListPanel;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private DragArea _dragArea;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIHeroCard[] _fakeHeroCards;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Button[] _notAvailableButtons;

        private int _selectedUnitConfigIndex;
        private Hero[] _heroes;
        private CharacterSelectionView _characterSelectionView;
        private UnityAction _onClose;
        private PlayerDataService _playerDataService;

        private IToastService _toastService;
        private void OnDisable() => Unsubscribe();

        [Inject]
        private void MonoConstructor([NotNull] IToastService toastService) =>
            _toastService = toastService;

        public async UniTask InitializeAsync([NotNull] IEnumerable<Hero> heroes, PlayerDataService playerDataService,
            CharacterSelectionView characterSelectionView, UnityAction onClose)
        {
            _playerDataService = playerDataService;
            _characterSelectionView = characterSelectionView;
            _onClose = onClose;

            InitCharacterSelectionView(heroes);
            Subscribe();
        }

        private void InitCharacterSelectionView(IEnumerable<Hero> heroes)
        {
            _heroes = heroes.ToArray();
            _heroListPanel.Initialize(heroes, OnHeroListItemClicked);

            SelectUnit(_playerDataService.GetSelectedHero());
            InitFakeHeroCards();
            InitNotAvailableButtons();
        }

        private void Subscribe()
        {
            _screenHeader.BackButton.onClick.AddListener(OnBackButtonClick);
            _confirmButton.onClick.AddListener(OnConfirmButtonClick);
            _dragArea.PointerDragEvent.AddListener(OnPointerDrag);
        }

        private void Unsubscribe()
        {
            _screenHeader.BackButton.onClick.RemoveListener(OnBackButtonClick);
            _confirmButton.onClick.RemoveListener(OnConfirmButtonClick);
            _dragArea.PointerDragEvent.RemoveListener(OnPointerDrag);
        }

        private void OnPointerDrag(Vector2 vector) => _characterSelectionView.RotateHero(vector.x);

        private void OnBackButtonClick() => _onClose?.Invoke();

        private void OnConfirmButtonClick() => _onClose?.Invoke();

        private void OnHeroListItemClicked(int index) => SelectUnit(index);

        private void SelectUnit(int index)
        {
            _selectedUnitConfigIndex = index;
            _playerDataService.SetSelectedHeroAsync(_heroes[index].Id);

            ShowCurrentHero();
        }

        private void SelectUnit(Hero hero)
        {
            for (var i = 0; i < _heroes.Length; i++)
                if (_heroes[i] == hero)
                {
                    SelectUnit(i);
                    return;
                }

            SelectUnit(0);
        }

        private void ShowCurrentHero()
        {
            _characterSelectionView.ShowHero(_heroes[_selectedUnitConfigIndex]);
            _heroInfo.SetData(_heroes[_selectedUnitConfigIndex]);
            _heroListPanel.SetActive(_heroes[_selectedUnitConfigIndex]);
        }

        private void ShowNotAvailableToast() => _toastService.Show("Not available");
        private void ShowNotAvailableHeroToast() => _toastService.Show("Cannot select this Hero");

        private void InitFakeHeroCards()
        {
            foreach (var card in _fakeHeroCards)
            {
                card.SetLockedCardCallback(ShowNotAvailableHeroToast);
                card.transform.SetAsLastSibling();
            }
        }

        private void InitNotAvailableButtons()
        {
            foreach (var button in _notAvailableButtons)
                button.onClick.AddListener(ShowNotAvailableToast);
        }
    }
}