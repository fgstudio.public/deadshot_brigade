using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.UI.Hud;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Zenject;


namespace Frostgate.RiftHunters.Core.UI
{
    public class UIWinPlayerInfoPanel : MonoBehaviour
    {
        [SerializeField, Required] private HeroAvatar _avatar;
        [SerializeField, Required] private Image _avatarBackground;
        [SerializeField, Required] private TMP_Text _playerName;
        [SerializeField, Required] private TMP_Text _playerScore;
        [SerializeField, Required] private RelationsColorSettings _colorSettings;

        private IConfigCollection<Hero> _heroCollection;

        [Inject]
        private void MonoConstructor(IConfigCollection<Hero> heroCollection) =>
            _heroCollection = heroCollection;

        public void Init(WinPlayerInfo winPlayerInfo)
        {
            if (_heroCollection.TryGet(winPlayerInfo.HeroId, out Hero hero))
                _avatar.Init(hero.Config);

            _playerName.text = winPlayerInfo.UserName;
            _playerScore.text = winPlayerInfo.PlayerScore.ToString();

            var color = winPlayerInfo.IsLocalPlayer ? _colorSettings.PlayerColors.Main : _colorSettings.TeammateColors.Main;
            _playerName.color = color;
            _playerScore.color = color;
            _avatarBackground.color = color;
        }
    }
}