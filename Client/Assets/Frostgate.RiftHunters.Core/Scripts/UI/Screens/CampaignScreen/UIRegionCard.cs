using System;
using Frostgate.RiftHunters.Meta;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIRegionCard : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private Image _image;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _name;
        [SerializeField, Required, ChildGameObjectsOnly] private Button _button;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _selectionFrame;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _lock;
        [SerializeField, Required] private Material _grayScale;

        [SerializeField] private Color _greyColor;

        private void OnDestroy() => _button.onClick.RemoveAllListeners();

        public void Init(RegionConfig region)
        {
            _image.sprite = region.Sprite;
            _name.text = region.Name.ToUpper();

            _lock.SetActive(false);
            SetSelected(false);
        }

        public void SetSelected(bool isSelected) => _selectionFrame.SetActive(isSelected);

        public void SetLockedState(UnityAction onClickAction)
        {
            _lock.SetActive(true);
            _image.material = _grayScale;
            _image.color = _greyColor;
            _name.color = _greyColor;
            _button.onClick.AddListener(onClickAction);
        }

        public void SetUnlockedState(UnityAction onClickAction)
        {
            _lock.SetActive(false);
            _image.material = null;
            _image.color = Color.white;
            _name.color = Color.white;
            _button.onClick.AddListener(onClickAction);
        }
    }
}
