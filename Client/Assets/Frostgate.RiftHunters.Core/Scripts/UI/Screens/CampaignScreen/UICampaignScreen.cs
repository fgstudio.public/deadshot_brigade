using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.UI.Toasters;
using Frostgate.RiftHunters.Infrastructure;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Meta.Player;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [UIScreenPrefab("UICampaignScreen")]
    public sealed class UICampaignScreen : UIScreen
    {
        private readonly string _regionNotAvailable = "Region not available";
        private readonly string _phaseNotAvailable = "Phase not available";
        private readonly string _phaseNotAvailableByHeroRank = "Phase not available by Hero rank";

        [SerializeField, Required, ChildGameObjectsOnly] private Button _closeButton;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _titleDescription;
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _phaseCardsContainer;
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _regionsCardsContainer;
        [SerializeField, Required] private UIPhaseCard _phaseCardPrefab;
        [SerializeField, Required] private UIRegionCard _regionCardPrefab;

        private Dictionary<UIRegionCard, RegionConfig> _regionCards;
        private Dictionary<UIPhaseCard, RegionPhaseConfig> _phaseCards;
        private UIRegionCard _selectedRegionCard;
        private UIPhaseCard _selectedPhaseCard;

        private UnityAction _onClose;
        private IToastService _toastService;
        private CampaignService _campaignService;
        private CampaignRoster _campaignRoster;
        private HeroUpgradeService _heroesUpgradeService;
        private PlayerDataService _playerDataService;

        public void Awake()
        {
            _regionCards = new Dictionary<UIRegionCard, RegionConfig>();
            _phaseCards = new Dictionary<UIPhaseCard, RegionPhaseConfig>();
        }

        [Inject]
        private void MonoConstructor([NotNull] IToastService toastService, Preferences preferences, CampaignService campaignService,
            HeroUpgradeService heroesUpgradeService, PlayerDataService playerDataService)
        {
            _toastService = toastService;
            _campaignRoster = preferences.CampaignRoster;
            _campaignService = campaignService;
            _heroesUpgradeService = heroesUpgradeService;
            _playerDataService = playerDataService;
        }

        public async UniTask InitializeAsync(UnityAction onClose)
        {
            _onClose = onClose;
            _closeButton.onClick.AddListener(onClose);

            SetTitleDescription();
            ShowRegions();
        }

        private void SetTitleDescription()
        {
            var currentPhase = _campaignService.GetCurrentPhase();
            var currentRegion = _campaignRoster.GetRegion(currentPhase);

            _titleDescription.text = $"{currentRegion.Name} / {currentPhase.Name}";
        }

        public void ShowRegions()
        {
            var regions = _campaignRoster.Regions;
            var currentPhase = _campaignService.GetCurrentPhase();
            var currentRegion = _campaignRoster.GetRegion(currentPhase);

            for (int i = 0; i < regions.Length; i++)
            {
                var region = regions[i];
                var regionCard = Instantiate(_regionCardPrefab, _regionsCardsContainer);
                regionCard.Init(region);
                _regionCards.Add(regionCard, region);

                var isUnlocked = _campaignService.IsRegionUnlocked(region);
                if (isUnlocked)
                    regionCard.SetUnlockedState(() => OnRegionCardClick(regionCard));
                else
                    regionCard.SetLockedState(OnLockedRegionCardClick);

                if (region == currentRegion)
                {
                    ShowRegionPhases(region);
                    OnRegionCardClick(regionCard);
                }
            }
        }

        public void ShowRegionPhases(RegionConfig region)
        {
            RemoveAllPhasesCards();

            var currentPhase = _campaignService.GetCurrentPhase();

            for (int i = 0; i < region.Phases.Length; i++)
            {
                var phase = region.Phases[i];
                var phaseCard = Instantiate(_phaseCardPrefab, _phaseCardsContainer);

                var score = _campaignService.GetScore(phase);

                phaseCard.Init(phase, score, i);
                _phaseCards.Add(phaseCard, phase);

                var isUnlocked = _campaignService.IsPhaseUnlocked(phase);

                if (isUnlocked)
                {
                    phaseCard.SetUnlockedState(() => OnPhaseCardClick(phaseCard));

                    var isCompleted = score >= phase.MaxScore;
                    if (isCompleted)
                        phaseCard.SetCompleted(phase);

                    var isSelected = phase == currentPhase;
                    if (isSelected)
                        phaseCard.SetSelected(true);

                }
                else
                {
                    phaseCard.SetLockedState(OnLockedPhaseCardClick);
                }
            }
        }

        private void RemoveAllPhasesCards()
        {
            if (_phaseCards.Count > 0)
            {
                foreach (var card in _phaseCards)
                    Destroy(card.Key.gameObject);
                _phaseCards.Clear();
            }
        }

        private void OnLockedRegionCardClick() => _toastService.Show(_regionNotAvailable);

        private void OnRegionCardClick(UIRegionCard card)
        {
            if(_selectedRegionCard == card)
                return;

            if (_selectedRegionCard != null)
                _selectedRegionCard.SetSelected(false);

            _selectedRegionCard = card;
            card.SetSelected(true);
            ShowRegionPhases(_regionCards[card]);
        }

        private void OnLockedPhaseCardClick() => _toastService.Show(_phaseNotAvailable);
        private async void OnPhaseCardClick(UIPhaseCard card)
        {
            if(_selectedPhaseCard == card)
                return;

            if (_campaignService.IsPhaseUnlocked(card.Phase))
            {
                var hero = _playerDataService.GetSelectedHero();
                var rank = _heroesUpgradeService.GetHeroRank(hero.Id);

                if (await _campaignService.TrySetCurrentPhase(card.Phase, rank))
                {
                    var currentRegion = _campaignRoster.GetRegion(card.Phase);
                    ShowRegionPhases(currentRegion);
                }
                else
                {
                    _toastService.Show(_phaseNotAvailableByHeroRank);
                }
            }
            else
            {
                _toastService.Show(_phaseNotAvailable);
            }

            _onClose?.Invoke();
        }
    }
}
