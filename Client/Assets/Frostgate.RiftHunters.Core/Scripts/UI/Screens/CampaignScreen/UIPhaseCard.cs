using Frostgate.RiftHunters.Meta;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIPhaseCard : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private Image _image;
        [SerializeField, Required, ChildGameObjectsOnly]
        private Transform _progressBarFill;
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _progressBarValue;
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _phaseNumber;
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _gearScoreValue;
        [SerializeField, Required, ChildGameObjectsOnly]
        private Button _button;
        [SerializeField, Required, ChildGameObjectsOnly]
        private Image _bossImage;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _bossShadow;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _selectionFrame;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _lastOpenedPhaseHat;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _complitedPhaseHat;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _lock;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _bossLabel;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _bossDefeatedLabel;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _complitedLabel;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _progressBarLayout;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _hat;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _star;

        public RegionPhaseConfig Phase { get; private set; }

        [SerializeField, Required] private Material _grayScale;

        private void OnDestroy() => _button.onClick.RemoveAllListeners();

        public void Init(RegionPhaseConfig phase, int currentProgress, int phaseIndex)
        {
            Phase = phase;

            _image.sprite = phase.Sprite;
            _gearScoreValue.text = phase.RecommendedGearScore.ToString();

            if (phase.MaxScore <= 0)
            {
                _bossImage.gameObject.SetActive(true);
                _bossShadow.SetActive(true);
                _bossLabel.SetActive(true);
                _phaseNumber.gameObject.SetActive(false);
                _progressBarLayout.SetActive(false);
            }
            else
            {
                _phaseNumber.text = $"Phase {phaseIndex + 1}";
                SetProgressBar(currentProgress, phase.MaxScore);
            }

            SetSelected(false);
        }

        public void SetSelected(bool isSelected) => _selectionFrame.SetActive(isSelected);

        public void SetCompleted(RegionPhaseConfig regionPhase)
        {
            _lastOpenedPhaseHat.SetActive(false);
            _complitedPhaseHat.SetActive(true);

            if (regionPhase.MaxScore <= 0)
            {
                _bossDefeatedLabel.SetActive(true);
            }
            else
            {
                _complitedLabel.SetActive(true);
                _star.SetActive(true);
                SetProgressBar(regionPhase.MaxScore, regionPhase.MaxScore);
            }
        }

        public void SetLockedState(UnityAction onClickAction)
        {
            _hat.SetActive(false);
            _lock.SetActive(true);
            _image.material = _grayScale;
            _bossImage.material = _grayScale;
            _button.onClick.AddListener(onClickAction);
        }

        public void SetUnlockedState(UnityAction onClickAction)
        {
            _hat.SetActive(true);
            _lock.SetActive(false);
            _image.material = null;
            _bossImage.material = null;
            _button.onClick.AddListener(onClickAction);
        }

        private void SetProgressBar(int currentValue, int maxValue)
        {
            _progressBarValue.text = $"{currentValue}/{maxValue}";
            _progressBarFill.localScale = new Vector3((float)currentValue / maxValue, 1f, 1f);
        }
    }
}
