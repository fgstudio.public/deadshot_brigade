using System;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Client;

namespace Frostgate.RiftHunters.Core.UI
{
    public class CharacterSelectionView : MonoBehaviour
    {
        [Serializable]
        private class UnitSelectionPreview
        {
            public UnitConfig Config;
            public UnitPreview Preview;
        }

        [SerializeField, Required] private Transform _playerHeroSlot;
        [SerializeField, Required] private UnitSelectionPreview[] _unitPreviewRoaster;

        [SerializeField] private float _rotationSensitivity;

        private UnitPreview _currentHeroPreview;

        public void ShowHero(Hero config)
        {
            if(_currentHeroPreview)
                Destroy(_currentHeroPreview.gameObject);

            _currentHeroPreview = Instantiate(GetUnitPreview(config), _playerHeroSlot);
            _currentHeroPreview.transform.position = _playerHeroSlot.position;


        }

        public void RotateHero(float offsetAngle) =>
            _currentHeroPreview.transform.Rotate(Vector3.up, -offsetAngle * _rotationSensitivity);

        private UnitPreview GetUnitPreview(Hero config)
        {
            foreach (var preview in _unitPreviewRoaster)
            {
                if (preview.Config.Id == config.Config.Id)
                    return preview.Preview;
            }

            throw new Exception($"PreviewRoaster don't have prefab for {config.Id} config");
        }
    }
}