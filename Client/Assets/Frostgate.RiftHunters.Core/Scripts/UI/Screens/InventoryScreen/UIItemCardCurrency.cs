using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIItemCardCurrency : MonoBehaviour, IUIItemCard
    {
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _newMarker;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _rarityBackground;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _itemIcon;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _quantity;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _selectionFrame;
        [SerializeField, Required, ChildGameObjectsOnly] private Button _button;

        private UnityAction _onCardClickAction;

        public GameObject GameObject => gameObject;

        public void Init(ItemCardData data, UnityAction onCardClickAction)
        {
            _newMarker.SetActive(data.IsNew);
            //_rarityBackground.color = rarityColor;
            _itemIcon.sprite = data.ItemIcon;
            _quantity.text = data.Quantity.ToString();
            _button.onClick.AddListener(OnCardClick);
            _onCardClickAction = onCardClickAction;
        }

        public void SetSelectedState(bool isSelected) => _selectionFrame.SetActive(isSelected);

        private void OnCardClick() => _onCardClickAction?.Invoke();
    }
}
