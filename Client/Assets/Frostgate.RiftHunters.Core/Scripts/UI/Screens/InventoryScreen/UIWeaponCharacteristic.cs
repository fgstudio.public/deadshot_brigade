using Frostgate.RiftHunters.Core.Battle;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIWeaponCharacteristic : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private UICharacteristicInfo _weaponDamage;
        [SerializeField, Required, ChildGameObjectsOnly] private UICharacteristicInfo _weaponReload;
        [SerializeField, Required, ChildGameObjectsOnly] private UICharacteristicInfo _weaponAmmo;

        public void Init(RangeWeaponConfig weapon)
        {
            _weaponDamage.SetStars(weapon.DamageRank);
            _weaponReload.SetStars(weapon.ReloadRank);
            _weaponAmmo.SetValue(weapon.PatronsCount);
        }
    }
}
