using System;
using System.Linq;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIEquipmentCharacteristic : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _healthModificator;

        public void Init(Equipment equipment)
        {
            if (equipment.Modifications.Count > 0)
            {
                foreach (var modifier in equipment.Modifications)
                {
                    if (modifier.Property == UnitProperty.Health)
                    {
                        string prefix = modifier.Operation == UnitPropertyOp.Add && modifier.Value > 0 ? "+" : string.Empty;
                        _healthModificator.text = $"{prefix}{modifier.Value} {modifier.Property}";
                    }
                }
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
    }
}
