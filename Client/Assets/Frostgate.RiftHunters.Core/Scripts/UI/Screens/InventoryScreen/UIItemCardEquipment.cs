using Frostgate.RiftHunters.Core.UI.Screens;
using Frostgate.RiftHunters.Meta.Economy;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIItemCardEquipment : MonoBehaviour, IUIItemCard
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _rank;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _newMarker;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _rarityBackground;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _classIcon;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _itemIcon;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _gearscoreValue;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _selectionFrame;
        [SerializeField, Required, ChildGameObjectsOnly] private Button _button;

        private UnityAction _onCardClickAction;

        public GameObject GameObject => gameObject;

        public void Init(ItemCardData data, UnityAction onCardClickAction)
        {
            _rank.text = UIRankTextHelper.ConvertRankToString(data.Rank);
            _classIcon.sprite = data.ClassIcon;
            _rarityBackground.color = data.RarityColor;
            _itemIcon.sprite = data.ItemIcon;
            _gearscoreValue.text = data.Gearscore.ToString();
            _button.onClick.AddListener(OnCardClick);
            _onCardClickAction = onCardClickAction;
            _newMarker.SetActive(data.IsNew);
        }

        public void SetSelectedState(bool isSelected) => _selectionFrame.SetActive(isSelected);

        private void OnCardClick() => _onCardClickAction?.Invoke();
    }
}
