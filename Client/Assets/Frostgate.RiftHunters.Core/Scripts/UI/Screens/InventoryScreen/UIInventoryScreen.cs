using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.UI.Screens;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [UIScreenPrefab("UIInventoryScreen")]
    public sealed class UIInventoryScreen : UIScreen
    {
        [SerializeField, Required] private UICardsColors _cardColors;
        [SerializeField, Required] private UIClassIcons _classIcons;
        [SerializeField, Required] private UIItemCardEquipment _itemCardEquipmentPrefab;
        [SerializeField, Required] private UIItemCardCurrency _itemCardCurrencyPrefab;

        [SerializeField, Required, ChildGameObjectsOnly]
        private Button _closeButton;
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _filtersParamertsText;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _typesToggleGroup;
        [SerializeField, Required, ChildGameObjectsOnly]
        private Transform _itemCardsContainer;
        [SerializeField, Required, ChildGameObjectsOnly]
        private UIInventoryItemInfoPanel _inventoryItemInfoPanel;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _cardsView;
        [SerializeField, Required, ChildGameObjectsOnly]
        private Image _emptyImage;

        #region TogglesReferences
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("Tabs references")]
        private Toggle _weaponToggle;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("Tabs references")]
        private Toggle _headToggle;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("Tabs references")]
        private Toggle _bodyToggle;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("Tabs references")]
        private Toggle _handsToggle;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("Tabs references")]
        private Toggle _footToggle;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("Filters references")]
        private Toggle _tankToggle;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("Filters references")]
        private Toggle _supportToggle;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("Filters references")]
        private Toggle _damageDealerToggle;
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("Filters references")]
        private Toggle _currencyToggle;
        #endregion

        [SerializeField, Required, FoldoutGroup("Tabs images")] private Sprite _tankIcon;
        [SerializeField, Required, FoldoutGroup("Tabs images")] private Sprite _supportIcon;
        [SerializeField, Required, FoldoutGroup("Tabs images")] private Sprite _damageDealerIcon;
        [SerializeField, Required, FoldoutGroup("Tabs images")] private Sprite _othersIcon;

        private InventoryService _inventoryService;
        private WalletService _walletService;

        private Dictionary<IUIItemCard, IConfig> _cards;
        private IUIItemCard _lastSelectedCard;

        private HeroClass _currentClass;
        private EquipmentType _currentTypeFilter;
        private Sprite _currentClassIcon;

        private string CurrencyFilterText = "Others";
        private const EquipmentRarity CurrencyRarity = EquipmentRarity.Uncommon;
        [Inject]
        private void MonoConstructor([NotNull] InventoryService inventoryService, [NotNull] WalletService walletService)
        {
            _inventoryService = inventoryService;
            _walletService = walletService;
        }

        private void Awake()
        {
            _cards = new Dictionary<IUIItemCard, IConfig>();

            _currentClassIcon = _tankIcon;
            _currentTypeFilter = EquipmentType.RangeWeapon;
            _currentClass = HeroClass.Tank;
        }

        private void OnDestroy() => Unsubscribe();

        public async UniTask InitializeAsync(UnityAction onClose)
        {
            _closeButton.onClick.AddListener(onClose);

            Subscribe();
            ShowWeaponType();
        }

        private void ShowWeaponType() => ShowEquipment(_currentClass, EquipmentType.RangeWeapon, _currentClassIcon);
        private void ShowHeadType() => ShowEquipment(_currentClass, EquipmentType.Head, _currentClassIcon);
        private void ShowBodyType() => ShowEquipment(_currentClass, EquipmentType.Body, _currentClassIcon);
        private void ShowHandsType() => ShowEquipment(_currentClass, EquipmentType.Arms, _currentClassIcon);
        private void ShowFootType() => ShowEquipment(_currentClass, EquipmentType.Legs, _currentClassIcon);
        private void ShowTankClass() => ShowEquipment(HeroClass.Tank, _currentTypeFilter, _tankIcon);
        private void ShowSupportClass() => ShowEquipment(HeroClass.Support, _currentTypeFilter, _supportIcon);
        private void ShowDamageDealerClass() => ShowEquipment(HeroClass.DamageDealer, _currentTypeFilter, _damageDealerIcon);

        private async void ShowEquipment(HeroClass equipmentClass, EquipmentType type, Sprite classIcon)
        {
            _filtersParamertsText.text = $"{equipmentClass} / {type}";
            _currentClass = equipmentClass;
            _currentClassIcon = classIcon;
            _currentTypeFilter = type;

            _typesToggleGroup.SetActive(true);
            _cardsView.SetActive(true);
            _inventoryItemInfoPanel.gameObject.SetActive(true);
            _emptyImage.gameObject.SetActive(false);
            RemoveAllCards();

            var equipments = (await GetEquipment())
                .Where(x => x.Class == equipmentClass && x.Type == type).ToArray();

            if (equipments.Length == 0)
                ShowEmpty(classIcon);
            else
                AddEquipmentCards(equipments);
        }

        private async void ShowCurrency()
        {
            _filtersParamertsText.text = CurrencyFilterText;
            _typesToggleGroup.SetActive(false);
            _cardsView.SetActive(true);
            _inventoryItemInfoPanel.gameObject.SetActive(true);
            _emptyImage.gameObject.SetActive(false);
            RemoveAllCards();

            var currency = (await GetCurrency()).ToArray();

            if (currency.Length == 0)
                ShowEmpty(_othersIcon);
            else
                AddCurrencyCards(currency);
        }

        private async Task<IEnumerable<Equipment>> GetEquipment() =>
            await _inventoryService!.GetAllEquipment();

        private async Task<IEnumerable<CurrencyQuantity>> GetCurrency() =>
            await _walletService!.GetTotalBalanceAsync();

        private void AddEquipmentCards(IEnumerable<Equipment> equipments)
        {
            foreach (var equipment in equipments)
            {
                var itemCard = Instantiate(_itemCardEquipmentPrefab, _itemCardsContainer);

                ItemCardData itemData = new ItemCardData()
                {
                    Rank = equipment.Rank,
                    IsNew = false,
                    RarityColor = _cardColors.GetColorByRarity(equipment.Rarity),
                    ClassIcon = _classIcons.GetClassIcon(equipment.Class),
                    ItemIcon = equipment.Icon,
                    Gearscore = equipment.GearScore
                };

                itemCard.Init(itemData, () => OnCardClick(itemCard));
                _cards.Add(itemCard, equipment);
            }

            var firstCard = _cards.First();
            ShowCardInfo(firstCard.Key);
        }

        private void AddCurrencyCards(IEnumerable<CurrencyQuantity> currencyQuantities)
        {
            foreach (var currencyQuantity in currencyQuantities)
            {
                var itemCard = Instantiate(_itemCardCurrencyPrefab, _itemCardsContainer);

                ItemCardData itemData = new ItemCardData()
                {
                    IsNew = false,
                    ItemIcon = currencyQuantity.Currency.Icon,
                    Quantity = currencyQuantity.Quantity
                };

                itemCard.Init(itemData, () => OnCardClick(itemCard));
                _cards.Add(itemCard, currencyQuantity.Currency);
            }

            var firstCard = _cards.First();
            ShowCardInfo(firstCard.Key);
        }

        private void RemoveAllCards()
        {
            foreach (var card in _cards)
                Destroy(card.Key.GameObject);
            _cards.Clear();
        }

        private void OnCardClick(IUIItemCard card)
        {
            if(card == _lastSelectedCard)
                return;

            ShowCardInfo(card);
        }

        private void ShowCardInfo(IUIItemCard card)
        {
            if (_cards[card] is Equipment item)
                _inventoryItemInfoPanel.ShowInfo(item, _cardColors.GetColorByRarity(item.Rarity), _classIcons.GetClassIcon(item.Class));

            if (_cards[card] is Currency currency)
                _inventoryItemInfoPanel.ShowInfo(currency, _cardColors.GetColorByRarity(CurrencyRarity));

            if(_lastSelectedCard != null)
                _lastSelectedCard.SetSelectedState(false);
            _lastSelectedCard = card;
            card.SetSelectedState(true);
        }

        private void ShowEmpty(Sprite icon)
        {
            _emptyImage.sprite = icon;
            _cardsView.SetActive(false);
            _inventoryItemInfoPanel.gameObject.SetActive(false);
            _emptyImage.gameObject.SetActive(true);
        }

        private void Subscribe()
        {
            _weaponToggle.onValueChanged.AddListener((isOn) => { if(isOn) ShowWeaponType();});
            _headToggle.onValueChanged.AddListener((isOn) => { if(isOn) ShowHeadType();});
            _bodyToggle.onValueChanged.AddListener((isOn) => { if(isOn) ShowBodyType();});
            _handsToggle.onValueChanged.AddListener((isOn) => { if(isOn) ShowHandsType();});
            _footToggle.onValueChanged.AddListener((isOn) => { if(isOn) ShowFootType();});
            _tankToggle.onValueChanged.AddListener((isOn) => { if(isOn) ShowTankClass();});
            _supportToggle.onValueChanged.AddListener((isOn) => { if(isOn) ShowSupportClass();});
            _damageDealerToggle.onValueChanged.AddListener((isOn) => { if(isOn) ShowDamageDealerClass();});
            _currencyToggle.onValueChanged.AddListener((isOn) => { if(isOn) ShowCurrency();});
        }

        private void Unsubscribe()
        {
            _weaponToggle.onValueChanged.RemoveAllListeners();
            _headToggle.onValueChanged.RemoveAllListeners();
            _bodyToggle.onValueChanged.RemoveAllListeners();
            _handsToggle.onValueChanged.RemoveAllListeners();
            _footToggle.onValueChanged.RemoveAllListeners();
            _tankToggle.onValueChanged.RemoveAllListeners();
            _supportToggle.onValueChanged.RemoveAllListeners();
            _damageDealerToggle.onValueChanged.RemoveAllListeners();
            _currencyToggle.onValueChanged.RemoveAllListeners();
        }
    }
}