using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.UI
{
    public interface IUIItemCard
    {
        void Init(ItemCardData data, UnityAction onCardClickAction);

        void SetSelectedState(bool isSelected);
        GameObject GameObject { get; }
    }
}
