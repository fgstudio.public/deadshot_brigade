using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIToggleClickHandler : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private Toggle _toggle;
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _activeIcon;

        private void Awake() =>
            _toggle.onValueChanged.AddListener(OnClick);

        private void OnDestroy() =>
            _toggle.onValueChanged.RemoveListener(OnClick);

        private void OnClick(bool isOn) =>
            _activeIcon.SetActive(isOn);
    }
}
