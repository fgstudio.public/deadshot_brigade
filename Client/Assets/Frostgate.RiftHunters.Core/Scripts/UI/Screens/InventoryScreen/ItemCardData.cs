using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class ItemCardData
    {
        public int Rank;
        public bool IsNew;
        public Color RarityColor;
        public Sprite ClassIcon;
        public Sprite ItemIcon;
        public int Quantity;
        public int Gearscore;
    }
}
