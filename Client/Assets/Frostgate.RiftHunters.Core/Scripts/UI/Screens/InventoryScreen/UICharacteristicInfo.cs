using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UICharacteristicInfo : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _characteristicName;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject[] _stars;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _starsBackground;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _dashedLine;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _value;

        public void SetName(string characteristicName) =>
            _characteristicName.text = characteristicName;

        public void SetStars(int value)
        {
            _dashedLine.SetActive(false);
            _starsBackground.SetActive(true);

            for (int i = 0; i < _stars.Length; i++)
                _stars[i].SetActive(i < value);
        }

        public void SetValue(int value)
        {
            _dashedLine.SetActive(true);
            _starsBackground.SetActive(false);

            _value.text = value.ToString();
        }
    }
}
