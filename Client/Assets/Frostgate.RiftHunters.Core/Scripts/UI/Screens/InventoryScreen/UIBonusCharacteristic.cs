using System.Linq;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIBonusCharacteristic : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text[] _bonusModificators;

        private const int MaxVisibleModifiers = 3;

        public void Init(Equipment equipment)
        {
            if (equipment.Modifications.Count > 0)
            {
                var mods = equipment.Modifications.ToArray();

                for (int i = 0; i < MaxVisibleModifiers; i++)
                {
                    if (equipment.Modifications.Count < i + 1 || mods[i].Property == UnitProperty.Health)
                    {
                        _bonusModificators[i].gameObject.SetActive(false);
                        continue;
                    }

                    _bonusModificators[i].gameObject.SetActive(true);

                    string prefix = mods[i].Operation == UnitPropertyOp.Add && mods[i].Value > 0 ? "+" : string.Empty;

                    if (mods[i].Measure == UnitPropertyMeasure.Units)
                        _bonusModificators[i].text = $"{prefix}{mods[i].Value} {mods[i].Property}";
                    else
                        _bonusModificators[i].text = $"{prefix}{mods[i].Value * 100}% {mods[i].Property}";
                }
            }
            gameObject.SetActive(_bonusModificators[0].gameObject.activeSelf);
        }
    }
}
