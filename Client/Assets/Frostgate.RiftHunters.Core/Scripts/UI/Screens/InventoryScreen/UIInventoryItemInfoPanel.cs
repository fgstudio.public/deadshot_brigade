using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIInventoryItemInfoPanel : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _name;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _classIcon;

        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _type;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _slotType;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _rank;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _rankLabel;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _rarityBackground;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _rarityColoredLine;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _itemIcon;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _gearscore;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _gearscoreIcon;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _description;

        [SerializeField, Required, ChildGameObjectsOnly] private UIWeaponCharacteristic _weaponCharacteristic;
        [SerializeField, Required, ChildGameObjectsOnly] private UIEquipmentCharacteristic _equipmentCharacteristic;
        [SerializeField, Required, ChildGameObjectsOnly] private UIBonusCharacteristic _bonusCharacteristic;

        public void ShowInfo(Equipment item, Color rarityColor, Sprite classIcon)
        {
            _name.text = item.Name;
            _name.color = rarityColor;
            _classIcon.sprite = classIcon;
            _classIcon.gameObject.SetActive(true);
            _type.text = item.Type.ToString();
            _slotType.gameObject.SetActive(item.Type == EquipmentType.RangeWeapon);
            _rarityBackground.color = rarityColor;
            _rarityColoredLine.color = rarityColor;
            _rank.text = "I";
            _rank.gameObject.SetActive(true);
            _rankLabel.gameObject.SetActive(true);
            _itemIcon.sprite = item.Icon;
            _gearscore.text = item.GearScore.ToString();
            _gearscore.gameObject.SetActive(true);
            _gearscoreIcon.SetActive(true);

            _description.text = item.Description;

            if (item is RangeWeaponConfig weapon)
            {
                _weaponCharacteristic.gameObject.SetActive(true);
                _weaponCharacteristic.Init(weapon);
            }
            else
            {
                _weaponCharacteristic.gameObject.SetActive(false);
            }

            if (item.Type != EquipmentType.RangeWeapon)
            {
                _equipmentCharacteristic.gameObject.SetActive(true);
                _equipmentCharacteristic.Init(item);
            }
            else
            {
                _equipmentCharacteristic.gameObject.SetActive(false);
            }

            _bonusCharacteristic.Init(item);
        }

        public void ShowInfo(Currency item, Color rarityColor)
        {
            _name.text = item.Name;
            _classIcon.gameObject.SetActive(false);
            _name.color = rarityColor;
            _type.text = "Currency type";
            _slotType.gameObject.SetActive(false);
            _rarityBackground.color = rarityColor;
            _rarityColoredLine.color = rarityColor;
            _itemIcon.sprite = item.Icon;
            _gearscore.gameObject.SetActive(false);
            _gearscoreIcon.SetActive(false);
            _rank.gameObject.SetActive(false);
            _rankLabel.gameObject.SetActive(false);

            _description.text = item.Description;
            _weaponCharacteristic.gameObject.SetActive(false);
            _equipmentCharacteristic.gameObject.SetActive(false);
            _bonusCharacteristic.gameObject.SetActive(false);
        }
    }
}
