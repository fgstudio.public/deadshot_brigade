using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Meta;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    public sealed class UIClassIcons : MonoBehaviour
    {
        [Serializable]
        public class ClassIcon
        {
            public HeroClass Class;
            public Sprite Icon;
        }

        [SerializeField] private ClassIcon[] _classIconsSettings;
        private Dictionary<HeroClass, Sprite> _classIcons;

        private void Awake()
        {
            _classIcons = new Dictionary<HeroClass, Sprite>();

            foreach (var classIcons in _classIconsSettings)
                _classIcons.Add(classIcons.Class, classIcons.Icon);
        }

        public Sprite GetClassIcon(HeroClass heroClass) => _classIcons[heroClass];
    }
}
