﻿using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Screens
{
    [UIScreenPrefab("UILoadingScreen")]
    public sealed class UILoadingScreen : UIScreen
    {
        public string Text
        {
            get => _text.text;
            set => _text.text = value;
        }

        [SerializeField, Required] private TMP_Text _text;
    }
}