using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    public class VersionText : MonoBehaviour
    {
        private const int VersionLength = 7;

        [SerializeField] private TMP_Text Text;

        private void Start() => UpdateVersionText();

        private void UpdateVersionText()
        {
            string version = Application.version;
            Text.text = version.Length >= VersionLength ? version.Substring(0, VersionLength) : version;
        }
    }
}