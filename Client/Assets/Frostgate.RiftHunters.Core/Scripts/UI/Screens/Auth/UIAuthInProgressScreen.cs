﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Screens.Auth
{
    [UIScreenPrefab(nameof(UIAuthInProgressScreen))]
    public class UIAuthInProgressScreen : UIScreen
    {
        [SerializeField] private RectTransform _loadingIndicator;
        [SerializeField] private UISmoothActivator _smoothActivator;

        private void Awake() => Subscribe();

        private void Start() => _smoothActivator.SetActive(true);

        private void Update() => _loadingIndicator.Rotate(Vector3.up * Time.deltaTime);

        private void OnDestroy() => Unsubscribe();

        private void Subscribe() { }

        private void Unsubscribe() { }
    }
}