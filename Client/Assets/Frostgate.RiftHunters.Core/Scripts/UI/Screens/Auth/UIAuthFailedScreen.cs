﻿using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI.Screens.Auth
{
    [UIScreenPrefab(nameof(UIAuthFailedScreen))]
    public class UIAuthFailedScreen : UIScreen
    {
        [SerializeField] private Button _okButton;
        [SerializeField] private UISmoothActivator _smoothActivator;

        private readonly UniTaskCompletionSource _tcs = new();
        private void Awake() => Subscribe();

        private void Start() => _smoothActivator.SetActive(true);

        private void OnDestroy() => Unsubscribe();

        public UniTask WaitForConfirm() => _tcs.Task;

        private void Subscribe() => _okButton.onClick.AddListener(OnClickOkButton);
        private void Unsubscribe() => _okButton.onClick.RemoveListener(OnClickOkButton);

        private void OnClickOkButton() => _tcs.TrySetResult();
    }
}