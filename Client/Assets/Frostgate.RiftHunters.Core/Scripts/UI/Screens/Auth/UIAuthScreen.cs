﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Services.Auth;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI.Screens.Auth
{
    [UIScreenPrefab(nameof(UIAuthScreen))]
    public class UIAuthScreen : UIScreen
    {
        [SerializeField] private Button _googlePlayAuthButton;
        [SerializeField] private Button _anonymousAuthButton;
        [SerializeField] private UISmoothActivator _smoothActivator;

        private readonly UniTaskCompletionSource<AuthType> _tcs = new();

        private void Awake() => Subscribe();

        private void Start() => _smoothActivator.SetActive(true);

        private void OnDestroy() => Unsubscribe();

        public UniTask<AuthType> GetAuthTypeAsync() => _tcs.Task;

        private void Subscribe()
        {
            _googlePlayAuthButton.onClick.AddListener(OnClickGooglePlayAuthButton);
            _anonymousAuthButton.onClick.AddListener(OnClickAnonymousAuthButton);
        }

        private void Unsubscribe()
        {
            _googlePlayAuthButton.onClick.RemoveListener(OnClickGooglePlayAuthButton);
            _anonymousAuthButton.onClick.RemoveListener(OnClickAnonymousAuthButton);
        }

        private void OnClickAnonymousAuthButton() => _tcs.TrySetResult(AuthType.Anonymous);

        private void OnClickGooglePlayAuthButton() => _tcs.TrySetResult(AuthType.GooglePlay);
    }
}