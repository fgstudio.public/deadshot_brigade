﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public readonly struct UIContainer<TUIObject> where TUIObject : IUIObject
    {
        [NotNull] private readonly RectTransform _transform;
        [NotNull] private readonly ICollection<TUIObject> _items;

        public UIContainer([NotNull] RectTransform transform)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(transform, nameof(transform));
            _transform = transform;
            _items = new HashSet<TUIObject>();
        }

        public void Add(TUIObject item)
        {
            if (Contains(item))
                throw new InvalidOperationException("UIPanel already contains this item.");

            SetItemParent(item, _transform);
            _items.Add(item);
        }

        public void Remove(TUIObject item)
        {
            if (!Contains(item))
                throw new InvalidOperationException("UIPanel does not contains this item.");

            SetItemParent(item, null);
            _items.Remove(item);
        }

        public bool Contains([NotNull] TUIObject item)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(item, nameof(item));
            return _items.Contains(item);
        }

        private void SetItemParent(TUIObject item, Transform parent) => item.Transform.SetParent(parent);
    }
}