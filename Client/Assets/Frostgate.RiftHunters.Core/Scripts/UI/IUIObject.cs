﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public interface IUIObject : IGameObject
    {
        RectTransform Transform { get; }
    }
}