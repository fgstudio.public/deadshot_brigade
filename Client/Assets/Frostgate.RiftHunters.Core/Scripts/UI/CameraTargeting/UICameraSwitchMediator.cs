using TMPro;
using Zenject;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Client;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.UI
{
    public interface IUICameraSwitchMediator
    {
        void SetNonPlayerUnits([NotNull] IEnumerable<UnitNetwork> units);
        void SetPlayerUnit([NotNull] UnitNetwork unit);
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UICameraSwitchMediator))]
    public sealed class UICameraSwitchMediator : MonoBehaviour, IUICameraSwitchMediator
    {
        [Header("Dependencies")]
        [SerializeField, Required, ChildGameObjectsOnly] private UISmoothActivator _smoothActivator;

        [Header("Object References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Button _switchTargetButton;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _unitNameLabel;

        [CanBeNull] private BattleCamera _battleCamera;

        private IEnumerator<UnitNetwork> _nonPlayerUnits;
        private UnitNetwork _playerUnit;

        public ISmoothActivator SmoothActivator => _smoothActivator;

        private void OnValidate()
        {
            _smoothActivator ??= GetComponentInChildren<UISmoothActivator>();
            _switchTargetButton ??= GetComponentInChildren<Button>();
            _unitNameLabel ??= GetComponentInChildren<TMP_Text>();
        }

        [Inject]
        public void MonoConstructor([InjectOptional, CanBeNull]BattleCamera battleCamera) => _battleCamera ??= battleCamera;

        private void Start() => _switchTargetButton.onClick.AddListener(SwitchTarget);
        private void OnDestroy() => _switchTargetButton.onClick.RemoveListener(SwitchTarget);

        public void EnableFreeSwitchingMode()
        {
            SmoothActivator.SetActive(true);
            SwitchToUnit(_playerUnit);
        }

        public void EnableOnlyPlayerMode()
        {
            SwitchToUnit(_playerUnit);
            SmoothActivator.SetActive(false);
        }

        public void SetNonPlayerUnits(IEnumerable<UnitNetwork> units) => _nonPlayerUnits = units.GetEnumerator();
        public void SetPlayerUnit(UnitNetwork unit) => _playerUnit = unit;

        private void SwitchTarget()
        {
            if (_nonPlayerUnits == null)
                SwitchToUnit(_playerUnit);

            else if (!_nonPlayerUnits.MoveNext())
            {
                _nonPlayerUnits.Reset();
                SwitchToUnit(_playerUnit);
            }

            else if (_nonPlayerUnits.Current != null && !_nonPlayerUnits.Current.UnitNetworkState.IsDead)
                SwitchToUnit(_nonPlayerUnits.Current);

            else
                SwitchTarget();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "Unity.NoNullPropagation")]
        private void SwitchToUnit(UnitNetwork unit)
        {
            string userName = unit?.UnitNetworkState?.PlayerName ?? string.Empty;
            Transform unitTransform = unit?.View?.transform;

            _unitNameLabel.text = userName;

            if (unitTransform != null)
                _battleCamera?.StartFollowing(unitTransform);
        }
    }
}