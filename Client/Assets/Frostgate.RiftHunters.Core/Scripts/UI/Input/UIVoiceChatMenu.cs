using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Voice;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIVoiceChatMenu : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private UIPlayerMicrophoneSwitchView playerMicrophoneSwitchPrefab;
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _container;
        [SerializeField, Required, ChildGameObjectsOnly] private Button _closeButton;

        private IParticipantsVoiceController _participantsController;
        private ObjectCollectionRepository<UnitNetwork> _unitCollections;

        private List<UIPlayerMicrophoneSwitchView> _playerMicrophoneSwitchViews;

        [Inject]
        public void MonoConstructor(IParticipantsVoiceController participantsController,
            ObjectCollectionRepository<UnitNetwork> unitCollections)
        {
            _participantsController = participantsController;
            _unitCollections = unitCollections;
        }

        private void Awake()
        {
            _closeButton.onClick.AddListener(() => gameObject.SetActive(false));
            _playerMicrophoneSwitchViews = new List<UIPlayerMicrophoneSwitchView>();
        }

        private void OnEnable() => ShowPlayersList();

        private void ShowPlayersList()
        {
            foreach (var view in _playerMicrophoneSwitchViews)
                Destroy(view.gameObject);
            _playerMicrophoneSwitchViews.Clear();

            var remotePlayers = _unitCollections.Get<uint>()
                .Where(unit => unit.UnitNetworkState.UnitType == BattleUnitType.Player && !unit.UnitNetworkState.isLocalPlayer)
                .Select(player => player.UnitNetworkState);

            foreach (var player in remotePlayers)
            {
                var playerMicroSwitchView = Instantiate(playerMicrophoneSwitchPrefab, _container);
                playerMicroSwitchView.gameObject.SetActive(true);
                _playerMicrophoneSwitchViews.Add(playerMicroSwitchView);

                playerMicroSwitchView.Init(player.PlayerName, _participantsController.ParticipantIsMuted(player.PlayerName),
                    _participantsController.MuteParticipant);
            }
        }
    }
}
