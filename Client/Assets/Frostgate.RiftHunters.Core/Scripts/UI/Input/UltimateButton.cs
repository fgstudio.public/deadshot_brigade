using Frostgate.RiftHunters.Core.UI.Hud;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.UI.Input
{
    public interface IUltimateButton : IInteractable, IEnabling
    {
        void SetActive(bool isActive);
        void SetIcon([CanBeNull] Sprite icon);
        IEnergyProgress EnergyProgress { get; }
    }

    /// <summary>
    /// Кнопка активации способности с обводкой в активном состоянии.
    /// </summary>
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#0e7f55eb0a9f4e6aa21e201826124dc8")]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UltimateButton))]
    public sealed class UltimateButton : MonoBehaviour, IUltimateButton
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Button _button;
        [SerializeField, Required, ChildGameObjectsOnly] private Animator _animator;
        [SerializeField, Required, ChildGameObjectsOnly] private Image[] _iconComponents;
        [SerializeField, Required, ChildGameObjectsOnly] private AnimatedEnergyProgress _energyProgress;

        private readonly int _active = Animator.StringToHash("Active");

        public bool IsEnabled => gameObject.activeSelf;
        public UnityEvent Clicked => _button.onClick;
        public IEnergyProgress EnergyProgress => _energyProgress;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _button ??= GetComponentInChildren<Button>();
            _animator ??= GetComponentInChildren<Animator>();
            _iconComponents ??= GetComponentsInChildren<Image>();
            _energyProgress ??= GetComponentInChildren<AnimatedEnergyProgress>();
        }

        public void Enable() => gameObject.SetActive(true);
        public void Disable() => gameObject.SetActive(false);

        public void SetIcon(Sprite icon)
        {
            for (int i = 0; i < _iconComponents.Length; i++)
                _iconComponents[i].sprite = icon;
        }

        public void SetActive(bool isActive)
        {
            _button.interactable = isActive;
            _animator.SetBool(_active, isActive);
        }

        void IInteractable.SetInteractable(bool interactable) =>
            SetActive(interactable);
    }
}