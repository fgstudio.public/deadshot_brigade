using TMPro;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Input.Reload
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIButtonCooldownTimer))]
    public sealed class UIButtonCooldownTimer : UICooldownComponent
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private TMP_Text _timerText;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _timerText ??= GetComponentInChildren<TMP_Text>(true);

        protected override void OnDataUpdated() =>
            _timerText.text = $"{Remains.TotalSeconds:F2}";

        protected override void OnDataCleared() =>
            _timerText.text = string.Empty;
    }
}