using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.UI.Input
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(AbilityButtonDetonation))]
    public sealed class AbilityButtonDetonation : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private Image _icon;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _detonatorVxf;

        private readonly List<UIEnemyEffectsView> _unitEffects = new();

        public void AddDetonationUnit([NotNull] UIEnemyEffectsView unitEffects)
        {
            if(_unitEffects.Contains(unitEffects))
                return;

            unitEffects.SetDetonationIcon(_icon.sprite);
            _unitEffects.Add(unitEffects);
            _detonatorVxf.SetActive(_unitEffects.Count > 0);
        }

        public void RemoveDetonationUnit([NotNull] UIEnemyEffectsView unitEffects)
        {
            if(!_unitEffects.Contains(unitEffects))
                return;

            unitEffects.HideDetonationIcon();
            _unitEffects.Remove(unitEffects);
            _detonatorVxf.SetActive(_unitEffects.Count > 0);
        }
    }
}