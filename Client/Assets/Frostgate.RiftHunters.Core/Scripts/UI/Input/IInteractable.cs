namespace Frostgate.RiftHunters.Core.UI.Input
{
    public interface IInteractable
    {
        void SetInteractable(bool interactable);
    }
}
