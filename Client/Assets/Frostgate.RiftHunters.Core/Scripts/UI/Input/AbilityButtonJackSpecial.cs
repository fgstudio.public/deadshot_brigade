using Zenject;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;
using Frostgate.RiftHunters.Core.Systems.Input;
using Frostgate.RiftHunters.Core.Systems.Input.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.UI.Input
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(AbilityButtonJackSpecial))]
    public sealed class AbilityButtonJackSpecial : OnScreenControl, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [SerializeField, Required] private RectTransform _cancelButtonRoot;
        [SerializeField, Required, Tooltip("Зона, выходя за которую появляется кнопка отмены способности")]
        private RectTransform _cancelView;

        [SerializeField] private OnScreenControlDragAreaAdapter _dragAdapter;
        [SerializeField, Required, ChildGameObjectsOnly] private CooldownableButton _cooldownableButton;


        [InputControl(layout = "Button"), SerializeField] private string _buttonControlPath;

        protected override string controlPathInternal
        {
            get => _buttonControlPath;
            set => _buttonControlPath = value;
        }

        private bool PressedButtonMode => _dragAdapter.enabled;

        private CoreInput _coreInput;
        private ImpactModel _impactModel;

        [Inject]
        private void MonoConstructor(CoreInput coreInput) =>
            _coreInput = coreInput;

        public void EnableDrag(bool enabled) =>
            _dragAdapter.enabled = enabled;

        void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
        {
            if (_cooldownableButton.IsInteractable)
                SendValueToControl(1f);
        }

        void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
        {
            if (PressedButtonMode &&
                CheckContainsPointInView(_cancelButtonRoot, eventData.position) &&
                _cancelButtonRoot.gameObject.activeSelf)
                Reset();

            SendValueToControl(0f);

            _cancelButtonRoot.gameObject.SetActive(false);
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            if (!PressedButtonMode)
                return;
            if (!_cooldownableButton.IsInteractable)
                return;
            if (_cancelButtonRoot.gameObject.activeSelf)
                return;

            if (!CheckContainsPointInView(_cancelView, eventData.position, true))
                _cancelButtonRoot.gameObject.SetActive(true);
        }

        private void Reset()
        {
            // Корректно вызвать инпут получилось через SendValueToControl, а корректно отменить инпут через InputAction.
            // В итоге получилась такая химера. Пробуем отменить InputAction если он подходит под текущий control.
            if (TryReset(_coreInput.Player.Ability))
                UnityEngine.Debug.Log("Reset Input");
        }

        private bool TryReset(InputAction inputAction)
        {
            if (inputAction.activeControl != control)
                return false;

            inputAction.Reset();
            return true;
        }

        private bool CheckContainsPointInView(RectTransform rect, Vector2 point, bool force = false)
        {
            if (!force && !rect.gameObject.activeSelf)
                return false;

            var local = rect.InverseTransformPoint(point);
            var contains = rect.rect.Contains(local);
            return contains;
        }
    }
}