using Frostgate.RiftHunters.Core.Systems.Input;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI
{
    public class UIPlayerEmotionButton : MonoBehaviour
    {
        public int Index => _index;

        [SerializeField] private int _index;
        [SerializeField] private float _sector = 30;
        [SerializeField] private float _sectorHalfSize = 30;
        [SerializeField] private Image _backImage;
        [SerializeField] private Sprite _selectedSprite;
        [SerializeField] private Sprite _defaultSprite;

        private PlayerInput _input;

        [Inject]
        private void MonoConstruct(PlayerInput input) => _input = input;

        public void SetSelected(bool selected) => _backImage.sprite = selected ? _selectedSprite : _defaultSprite;
        public bool CheckSector(float angle)
        {
            var delta = Mathf.DeltaAngle(_sector, angle);
            return Mathf.Abs(delta) <= _sectorHalfSize;
        }
    }
}