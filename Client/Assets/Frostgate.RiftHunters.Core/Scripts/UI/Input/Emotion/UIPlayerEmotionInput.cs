using Frostgate.RiftHunters.Core.Systems.Input;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI
{
    public class UIPlayerEmotionInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [SerializeField] private GameObject _emotionsBlock;
        [SerializeField] private UIPlayerEmotionButton[] _buttons;

        private PlayerInput _input;
        private Vector2 _startPos;
        private float _currentAngle;
        private UIPlayerEmotionButton _selectedBtn;

        [Inject]
        private void MonoConstruct(PlayerInput input) => _input = input;

        private void OnEnable()
        {
            HideEmotions();
            _input.Emotion.AddListener(OnEmotion);
        }

        private void OnDisable()
        {
            HideEmotions();
            _input.Emotion.RemoveListener(OnEmotion);
        }

        private void ShowEmotions()
        {
            _emotionsBlock.SetActive(true);
        }

        private void OnEmotion(int index) => HideEmotions();

        private void HideEmotions()
        {
            _emotionsBlock.SetActive(false);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            _selectedBtn = null;
            _startPos = eventData.position;

            foreach (var button in _buttons)
                button.SetSelected(false);

            ShowEmotions();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            HideEmotions();

            if (_selectedBtn != null)
                _input.InvokeEmotion(_selectedBtn.Index);
        }

        public void OnDrag(PointerEventData eventData)
        {
            var v = eventData.position - _startPos;

            var maxDistToStart = Screen.width * 0.1f;
            var dist = v.magnitude;
            if (dist > maxDistToStart)
            {
                v = v.normalized * maxDistToStart;
                _startPos = eventData.position - v;
            }

            _currentAngle = Vector2.SignedAngle(v, Vector2.up);

            var selectedBtn = GetSelectedButton();
            _selectedBtn = selectedBtn;

            foreach (var button in _buttons)
            {
                var needSelect = _selectedBtn == button;
                button.SetSelected(needSelect);
            }
        }

        private UIPlayerEmotionButton GetSelectedButton()
        {
            UIPlayerEmotionButton selectedButton = null;

            foreach (var button in _buttons)
            {
                var needSelect = button.CheckSector(_currentAngle);

                if (needSelect)
                {
                    selectedButton = button;
                    break;
                }
            }

            return selectedButton;
        }
    }
}