using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public class EmotionUnitView : MonoBehaviour
    {
        [SerializeField, Required] private GameObject _messageLayout;
        [SerializeField, Required] private TMP_Text[] _text;
        [SerializeField, Required] private Image _defaultIcon;
        [SerializeField, Required] private Image _glowingIcon;

        private void Awake()
        {
            ClearEmotion();
        }

        public void ShowEmotion(UnitEmotionConfig config)
        {
            _messageLayout.SetActive(false);

            for (int i = 0; i < _text.Length; i++)
                _text[i].text = config.Name;

            _defaultIcon.sprite = config.Sprite;
            _glowingIcon.sprite = config.GlowSprite;

            _messageLayout.SetActive(true);
        }

        public void ClearEmotion() => _messageLayout.SetActive(false);
    }
}