using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Input.Reload
{
    public abstract class UICooldownComponent : MonoBehaviour
    {
        public TimeSpan Elapsed { get; private set; }
        public TimeSpan Cooldown { get; private set; }

        protected TimeSpan Remains => Cooldown - Elapsed;
        protected float Progress => Cooldown.Ticks > 0
            ? (float)Elapsed.Ticks / Cooldown.Ticks : 0;

        public void ClearData()
        {
            Elapsed = default;
            Cooldown = default;

            OnDataCleared();
        }

        public void UpdateData(TimeSpan elapsed, TimeSpan cooldown)
        {
            Elapsed = elapsed;
            Cooldown = cooldown;

            OnDataUpdated();
        }

        protected abstract void OnDataUpdated();
        protected abstract void OnDataCleared();
    }
}
