using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Input.Reload
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIButtonCooldownProgressBar))]
    public sealed class UIButtonCooldownProgressBar : UICooldownComponent
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private Image _progressImage;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _progressImage ??= GetComponentInChildren<Image>(true);

        protected override void OnDataUpdated() =>
            _progressImage.fillAmount = Progress;

        protected override void OnDataCleared() =>
            _progressImage.fillAmount = default;
    }
}