using System;
using Frostgate.RiftHunters.Core.UI.Input;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIPlayerMicrophoneSwitchView : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _playerNameText;
        [SerializeField, Required, ChildGameObjectsOnly] private Button _button;
        [SerializeField, Required, ChildGameObjectsOnly] private VoiceChatButton _voiceChatButton;

        private string _playerName;
        private bool _isMuted;
        private Action<string, bool> _onClick;

        public void Init(string playerName, bool isMuted, Action<string, bool> onClick)
        {
            _playerName = playerName;
            _onClick = onClick;
            _isMuted = isMuted;

            _playerNameText.text = _playerName;
            _voiceChatButton.SetActive(!_isMuted);

            Subscribe();
        }

        private void OnDestroy() => Unsubscribe();

        private void Subscribe() =>
            _button.onClick.AddListener(OnButtonClick);

        private void Unsubscribe() =>
            _button.onClick.RemoveListener(OnButtonClick);

        private void OnButtonClick()
        {
            _isMuted = !_isMuted;
            _voiceChatButton.SetActive(!_isMuted);
            _onClick?.Invoke(_playerName, _isMuted);
        }
    }
}
