using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI.Input;

namespace Frostgate.RiftHunters.Core.UI
{
    public interface IUIPlayerInputMediator
    {
        IUltimateButton UltimateButton { get; }
        IAbilityButton AbilityButton { get; }
        IAbilityButton ExtraAbilityButton { get; }
        IReloadButton ReloadButton { get; }
        IDodgeButton DodgeButton { get; }
        IVoiceChatButton MuteParticipantsButton { get; }
        IVoiceChatButton MuteSelfButton { get; }
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIPlayerInputMediator))]
    public sealed class UIPlayerInputMediator : MonoBehaviour, IUIPlayerInputMediator, ISmoothActivation
    {
        [Header("Dependencies")]
        [SerializeField, Required, ChildGameObjectsOnly] private UISmoothActivator _smoothActivator;

        [Header("Elements")]
        [Required, ChildGameObjectsOnly] public UltimateButton UltimateButton;
        [Required, ChildGameObjectsOnly] public AbilityButton AbilityButton;
        [Required, ChildGameObjectsOnly] public AbilityButton ExtraAbilityButton;
        [Required, ChildGameObjectsOnly] public ReloadButton ReloadButton;
        [Required, ChildGameObjectsOnly] public DodgeButton DodgeButton;
        [Required, ChildGameObjectsOnly] public VoiceChatButton MuteParticipantsButton;
        [Required, ChildGameObjectsOnly] public VoiceChatButton MuteSelfButton;

        IUltimateButton IUIPlayerInputMediator.UltimateButton => UltimateButton;
        IAbilityButton IUIPlayerInputMediator.AbilityButton => AbilityButton;
        IAbilityButton IUIPlayerInputMediator.ExtraAbilityButton => ExtraAbilityButton;
        IReloadButton IUIPlayerInputMediator.ReloadButton => ReloadButton;
        IDodgeButton IUIPlayerInputMediator.DodgeButton => DodgeButton;
        IVoiceChatButton IUIPlayerInputMediator.MuteParticipantsButton => MuteParticipantsButton;
        IVoiceChatButton IUIPlayerInputMediator.MuteSelfButton => MuteSelfButton;
        public ISmoothActivator SmoothActivator => _smoothActivator;

        private void OnValidate()
        {
            _smoothActivator ??= GetComponentInChildren<UISmoothActivator>(true);
            UltimateButton ??= GetComponentInChildren<UltimateButton>(true);
            AbilityButton ??= GetComponentInChildren<AbilityButton>(true);
            ExtraAbilityButton ??= GetComponentInChildren<AbilityButton>(true);
            ReloadButton ??= GetComponentInChildren<ReloadButton>(true);
            DodgeButton ??= GetComponentInChildren<DodgeButton>(true);
        }
    }
}