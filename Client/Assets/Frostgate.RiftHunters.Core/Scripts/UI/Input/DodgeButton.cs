using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Input
{
    public interface IDodgeButton : IInteractable
    {
        void SetCurrentStamina(float stamina, float maxStamina);
        void SetSegments(int value);
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(DodgeButton))]
    public sealed class DodgeButton : MonoBehaviour, IDodgeButton
    {
        [Serializable]
        public class SpriteSet
        {
            public Sprite Main;
            public Sprite Change;
        }

        [SerializeField, Required, ChildGameObjectsOnly] private Button _button;
        [SerializeField, Required, ChildGameObjectsOnly] private Bar _progressBar;
        [SerializeField, Required, ChildGameObjectsOnly] private Image[] _progressBarImage;
        [SerializeField, Required, ChildGameObjectsOnly] private Image _progressBarChangeImage;
        [SerializeField, Required] private SpriteSet[] _progressBarSprites;

        public UnityEvent Clicked => _button.onClick;

        private void OnValidate()
        {
            _button ??= GetComponentInChildren<Button>(true);
            _progressBar ??= GetComponentInChildren<Bar>(true);
        }

        public void SetInteractable(bool interactable) =>
            _button.interactable = interactable;

        public void SetCurrentStamina(float stamina, float maxStamina) =>
            _progressBar.Set(stamina, maxStamina);

        public void SetSegments(int segmentsValue)
        {
            var progressBarSprite  = segmentsValue > _progressBarSprites.Length ? _progressBarSprites[0] : _progressBarSprites[segmentsValue - 1];
            for (int i = 0; i < _progressBarImage.Length; i++)
                _progressBarImage[i].sprite = progressBarSprite.Main;

            _progressBarChangeImage.sprite = progressBarSprite.Change;
        }
    }
}