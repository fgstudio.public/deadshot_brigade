using System;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI.Input
{
    public interface IReloadButton : IInteractable
    {
        void StartReload(TimeSpan cooldown);
        void StopReload();
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(ReloadButton))]
    public sealed class ReloadButton : MonoBehaviour, IReloadButton
    {
        [Required, ChildGameObjectsOnly] public CooldownableButton Button;

        public void SetInteractable(bool interactable) => Button.SetInteractable(interactable);
        public void StartReload(TimeSpan cooldown) => Button.StartCooldown(cooldown);
        public void StopReload() => Button.StopCooldown();
    }
}