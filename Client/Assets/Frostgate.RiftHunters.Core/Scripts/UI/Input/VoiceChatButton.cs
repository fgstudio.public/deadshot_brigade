﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Input
{
    public interface IVoiceChatButton
    {
        void SetActive(bool isActive);
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(VoiceChatButton))]
    public sealed class VoiceChatButton : MonoBehaviour, IVoiceChatButton
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject ActiveLayout;

        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject InactiveLayout;

        public void SetActive(bool isActive)
        {
            ActiveLayout.SetActive(isActive);
            InactiveLayout.SetActive(!isActive);
        }
    }
}