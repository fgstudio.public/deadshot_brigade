using System;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.UI.Input
{
    public interface IAbilityButton : IEnabling
    {
        bool IsBlocked { get; }

        [CanBeNull] AbilityButtonDetonation Detonation { get; }
        [CanBeNull] AbilityButtonJackSpecial JackSpecial { get; }

        void SetIcon([NotNull] Sprite icon);
        void StartCooldown(TimeSpan cooldown);
        void StopCooldown();
        void BlockButton();
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(AbilityButton))]
    public sealed class AbilityButton : MonoBehaviour, IAbilityButton
    {
        [Required, ChildGameObjectsOnly] public CooldownableButton CooldownableButton;
        [Required, ChildGameObjectsOnly] public Image[] IconComponents;
        [ChildGameObjectsOnly] public AbilityButtonDetonation Detonation;
        [ChildGameObjectsOnly] public AbilityButtonJackSpecial JackSpecial;

        public bool IsBlocked { get; private set; }
        public bool IsEnabled => gameObject.activeSelf;
        AbilityButtonDetonation IAbilityButton.Detonation => Detonation;
        AbilityButtonJackSpecial IAbilityButton.JackSpecial => JackSpecial;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            IconComponents ??= GetComponentsInChildren<Image>(true);
            CooldownableButton ??= GetComponentInChildren<CooldownableButton>(true);
            Detonation ??= GetComponentInChildren<AbilityButtonDetonation>(true);
            JackSpecial ??= GetComponentInChildren<AbilityButtonJackSpecial>(true);
        }

        public void Enable() => gameObject.SetActive(true);
        public void Disable() => gameObject.SetActive(false);

        public void SetIcon(Sprite icon) =>
            IconComponents.ForEach(c => c.sprite = icon);

        public void StartCooldown(TimeSpan cooldown)
        {
            IsBlocked = false;
            CooldownableButton.StartCooldown(cooldown);
        }

        public void StopCooldown()
        {
            IsBlocked = false;
            CooldownableButton.StopCooldown();
        }

        public void BlockButton()
        {
            IsBlocked = true;
            CooldownableButton.StartInfiniteCooldown();
        }
    }
}