using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.UI.Input.Reload;

namespace Frostgate.RiftHunters.Core.UI.Input
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(CooldownableButton))]
    public sealed class CooldownableButton : MonoBehaviour, IInteractable
    {
        public bool IsInteractable => _button.interactable;

        [SerializeField, Required, ChildGameObjectsOnly] private Button _button;
        [SerializeField, Required, ChildGameObjectsOnly] private UICooldownComponent[] _cooldownComponents;

        [SerializeField, Required, ChildGameObjectsOnly] private GameObject[] _buttonLayoutObjects;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject[] _cooldownLayoutObjects;

        [CanBeNull] private CancellationTokenSource _cts;

        private ILogger<CooldownableButton> _logger = LoggerFactory.CreateLogger<CooldownableButton>();
        private TimeSpan _elapsed;

        private bool IsUsable =>
            this != null && gameObject != null && gameObject.activeSelf;

        private void OnValidate()
        {
            _button ??= GetComponentInChildren<Button>(true);
            _cooldownComponents ??= GetComponentsInChildren<UICooldownComponent>(true);
        }

        private void OnDestroy() =>
            CancelToken();

        public void SetInteractable(bool interactable) =>
            _button.interactable = interactable;

        public void StartInfiniteCooldown()
        {
            StopCooldown();
            ClearComponents();
            EnableCooldownLayout();
        }

        public async void StartCooldown(TimeSpan cooldown)
        {
            var elapsed = _elapsed;

            StopCooldown();
            EnableCooldownLayout();

            _elapsed = elapsed;

            _cts = new CancellationTokenSource();
            CancellationToken token = _cts.Token;

            try
            {
                await CooldownRoutine(cooldown, token)
                    .ContinueWith(EnableButtonLayout)
                    .SuppressCancellationThrow();
            }
            catch (Exception e)
            {
                _logger.LogException(e);
            }
        }

        public void StopCooldown()
        {
            _elapsed = TimeSpan.Zero;

            CancelToken();

            if (IsUsable)
                ClearComponents();

            EnableButtonLayout();
        }

        private async UniTask CooldownRoutine(TimeSpan cooldown, CancellationToken token)
        {
            cooldown += _elapsed;

            while (_elapsed < cooldown && IsUsable)
            {
                token.ThrowIfCancellationRequested();

                UpdateComponents(_elapsed, cooldown);
                await Task.Yield();
                _elapsed += TimeSpan.FromSeconds(Time.deltaTime);
            }

            _elapsed = TimeSpan.Zero;

            token.ThrowIfCancellationRequested();
            UpdateComponents(cooldown, cooldown);
        }

        private void ClearComponents()
        {
            foreach (UICooldownComponent component in _cooldownComponents)
                component.ClearData();
        }

        private void UpdateComponents(TimeSpan elapsed, TimeSpan cooldown)
        {
            foreach (UICooldownComponent component in _cooldownComponents)
                component.UpdateData(elapsed, cooldown);
        }

        private void EnableButtonLayout()
        {
            _button.interactable = true;
            _buttonLayoutObjects.ForEach(o => o.SetActive(true));
            _cooldownLayoutObjects.ForEach(o => o.SetActive(false));
        }

        private void EnableCooldownLayout()
        {
            _button.interactable = false;
            _buttonLayoutObjects.ForEach(o => o.SetActive(false));
            _cooldownLayoutObjects.ForEach(o => o.SetActive(true));
        }

        private void CancelToken()
        {
            if (_cts != null)
            {
                _cts.Cancel();
                _cts.Dispose();
                _cts = null;
            }
        }
    }
}