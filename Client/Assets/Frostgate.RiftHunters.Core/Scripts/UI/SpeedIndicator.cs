﻿using TMPro;
using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(CanvasGroup))]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(SpeedIndicator))]
    public sealed class SpeedIndicator : MonoBehaviour
    {
        [SerializeField, Required, FoldoutGroup("References")]
        private CanvasGroup _canvasGroup;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private TMP_Text _speedText;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Image[] _indicationImages = Array.Empty<Image>();

        [SerializeField, FoldoutGroup("Settings")]
        private FadeSettings _fadeSettings;

        [ShowInInspector, FoldoutGroup("Info")]
        public int Speed
        {
            get => _speed;
            set
            {
                if (value != _speed)
                    SetSpeed(value);
            }
        }

        private Fader<CanvasGroup> _fader;
        private int _speed;

        private void OnValidate() => DiscoverReferences();

        [Button]
        private void Reset()
        {
            _canvasGroup = null;
            _speedText = null;
            _indicationImages = Array.Empty<Image>();
            _fadeSettings = new();
        }

        private void Awake()
        {
            DiscoverReferences();

            _fader = new Fader<CanvasGroup>(_canvasGroup, (cg, alpha, duration) => cg.DOFade(alpha, duration),
                _fadeSettings);
        }

        private void Start() => SetSpeed(0);

        [Button]
        public void Show() => _fader.Unfade();

        [Button]
        public void Hide() => _fader.Fade();

        private void DiscoverReferences()
        {
            _canvasGroup ??= GetComponent<CanvasGroup>();
        }

        private void SetSpeed(int speed)
        {
            _speed = speed;
            UpdateIndicationObjects();
            UpdateSpeedText();
        }

        private void UpdateIndicationObjects()
        {
            for (var i = _indicationImages.Length - 1; i >= 0; i--)
            {
                bool isIndicatorEnabled = _indicationImages.Length - i - 1 < _speed;
                _indicationImages[i].enabled = isIndicatorEnabled;
            }
        }

        private void UpdateSpeedText()
        {
            _speedText.text = _speed.ToString();
        }
    }
}