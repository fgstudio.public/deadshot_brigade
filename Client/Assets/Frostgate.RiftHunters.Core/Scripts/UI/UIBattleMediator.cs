using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI.Hud;
using Frostgate.RiftHunters.Core.Battle.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.UI.Hud.TotemPanel;
using Frostgate.RiftHunters.Core.UI.Input;

namespace Frostgate.RiftHunters.Core.UI
{
    public readonly struct ReanimationModeDTO
    {
        [NotNull] public readonly UnitNetwork PlayerUnit;
        [NotNull, ItemNotNull] public readonly IEnumerable<UnitNetwork> NonPlayerUnits;
        [NotNull] public readonly IProgress Progress;

        public ReanimationModeDTO([NotNull] UnitNetwork playerUnit,
            [NotNull, ItemNotNull] IEnumerable<UnitNetwork> nonPlayerUnits, [NotNull] IProgress progress)
        {
            PlayerUnit = playerUnit;
            NonPlayerUnits = nonPlayerUnits;
            Progress = progress;
        }
    }

    public interface IUIBattleMediator
    {
        UnitNetwork PlayerUnit { get; }
        IHudMediator Hud { get; }
        IFloatingTextsMediator FloatingTextsMediator { get; }
        IUIPlayerInputMediator InputMediator { get; }
        IUIWinPanelMediator WinPanelMediator { get; }
        IUIBotSpawningMediator BotSpawningMediator { get; }
        IUIBossPhasesMediator BossPhasesMediator { get; }
        ITotemsPanel TotemsPanel { get; }
        IUIBarPanel<IUIBarPanelItem> BarPanel { get; }
        UIReanimationMediator ReanimationMediator { get; }
        UIVoiceChatMenu VoiceChatMenu { get; }

        void EnableGameplayMode(UnitNetwork playerUnit);
        void EnableFinishedGameplayMode();
        void EnableReanimationMode(ReanimationModeDTO dto);
        void DisableReanimationMode();
        void ActivateReanimationProcess();
        void StopReanimationProcess();
        void EnableAutoRespawn();
        void DisableAutoRespawn();
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIBattleMediator))]
    public sealed class UIBattleMediator : MonoBehaviour, IUIBattleMediator
    {
        [SerializeField, ChildGameObjectsOnly] private UIBattleMenuMediator _battleMenu;
        [SerializeField, Required, ChildGameObjectsOnly] private HudMediator _hud;
        [SerializeField, Required, ChildGameObjectsOnly] private UIFloatingTextsMediator _floatingTextsMediator;
        [SerializeField, Required, ChildGameObjectsOnly] private UIPlayerInputMediator _inputMediator;
        [SerializeField, Required, ChildGameObjectsOnly] private UIReanimationMediator _reanimationMediator;
        [SerializeField, Required, ChildGameObjectsOnly] private UICameraSwitchMediator _cameraSwitchMediator;
        [SerializeField, Required, ChildGameObjectsOnly] private UIWinPanelMediator _winPanelMediator;
        [SerializeField, Required, ChildGameObjectsOnly] private UIBotSpawningMediator _botSpawningMediator;
        [SerializeField, Required, ChildGameObjectsOnly] private UIBossPhasesMediator _bossPhasesMediator;
        [SerializeField, Required, ChildGameObjectsOnly] private TotemsPanel _totemsPanel;
        [SerializeField, Required, ChildGameObjectsOnly] private MonoUIBarPanel _barPanel;
        [SerializeField, Required, ChildGameObjectsOnly] private UISmoothActivator _progressBarsPanel;
        [SerializeField, Required, ChildGameObjectsOnly] private UIVoiceChatMenu _voiceChatMenu;

        public UnitNetwork PlayerUnit { get; private set; }
        public IHudMediator Hud => _hud;
        public IFloatingTextsMediator FloatingTextsMediator => _floatingTextsMediator;
        public IUIPlayerInputMediator InputMediator => _inputMediator;
        public IUICameraSwitchMediator CameraSwitchMediator => _cameraSwitchMediator;
        public IUIWinPanelMediator WinPanelMediator => _winPanelMediator;
        public IUIBotSpawningMediator BotSpawningMediator => _botSpawningMediator;
        public IUIBossPhasesMediator BossPhasesMediator => _bossPhasesMediator;
        public ITotemsPanel TotemsPanel => _totemsPanel;
        public IUIBarPanel<IUIBarPanelItem> BarPanel => _barPanel;
        public UIReanimationMediator ReanimationMediator => _reanimationMediator;
        public UIVoiceChatMenu VoiceChatMenu => _voiceChatMenu;

        private void OnValidate()
        {
            _hud ??= GetComponentInChildren<HudMediator>();
            _battleMenu ??= GetComponentInChildren<UIBattleMenuMediator>();
            _inputMediator ??= GetComponentInChildren<UIPlayerInputMediator>();
            _reanimationMediator ??= GetComponentInChildren<UIReanimationMediator>();
            _cameraSwitchMediator ??= GetComponentInChildren<UICameraSwitchMediator>();
            _botSpawningMediator ??= GetComponentInChildren<UIBotSpawningMediator>();
            _bossPhasesMediator ??= GetComponentInChildren<UIBossPhasesMediator>();
            _totemsPanel ??= GetComponentInChildren<TotemsPanel>();
            _voiceChatMenu ??= GetComponentInChildren<UIVoiceChatMenu>();

            DisableReferences();
        }

        private void Awake() => DisableReferences();
        private void DisableReferences()
        {
            if (_battleMenu != null)
                _battleMenu.gameObject.SetActive(false);

            _hud.gameObject.SetActive(false);
            _inputMediator.gameObject.SetActive(false);
            _reanimationMediator.gameObject.SetActive(false);
            _cameraSwitchMediator.gameObject.SetActive(false);
            _winPanelMediator.gameObject.SetActive(false);
            _botSpawningMediator.gameObject.SetActive(false);
            _bossPhasesMediator.gameObject.SetActive(false);
            _barPanel.gameObject.SetActive(false);
            _progressBarsPanel.SetActive(false);
            _voiceChatMenu.gameObject.SetActive(false);
        }

        public void EnableGameplayMode(UnitNetwork playerUnit)
        {
            if (_battleMenu != null)
                _battleMenu.SmoothActivator.SetActive(true);

            PlayerUnit = playerUnit;
            _hud.SmoothActivator.SetActive(true);
            _inputMediator.SmoothActivator.SetActive(true);
            _reanimationMediator.SmoothActivator.SetActive(false);
            _cameraSwitchMediator.SmoothActivator.SetActive(false);
            _winPanelMediator.SmoothActivator.SetActive(false);
            _botSpawningMediator.gameObject.SetActive(false);
            _bossPhasesMediator.gameObject.SetActive(false);
            _barPanel.gameObject.SetActive(true);
            _progressBarsPanel.SetActive(true);
            _voiceChatMenu.gameObject.SetActive(false);
        }

        public void EnableFinishedGameplayMode()
        {
            if (_battleMenu != null)
                _battleMenu.SmoothActivator.SetActive(false);

            PlayerUnit = null;
            _hud.SmoothActivator.SetActive(false);
            _inputMediator.SmoothActivator.SetActive(false);
            _reanimationMediator.SmoothActivator.SetActive(false);
            _cameraSwitchMediator.SmoothActivator.SetActive(false);
            _winPanelMediator.SmoothActivator.SetActive(true);
            _botSpawningMediator.gameObject.SetActive(false);
            _bossPhasesMediator.gameObject.SetActive(false);
            _barPanel.gameObject.SetActive(false);
            _progressBarsPanel.SetActive(false);
            _voiceChatMenu.gameObject.SetActive(false);
        }

        public void EnableReanimationMode(ReanimationModeDTO dto)
        {
            _cameraSwitchMediator.SetPlayerUnit(dto.PlayerUnit);
            _cameraSwitchMediator.SetNonPlayerUnits(dto.NonPlayerUnits);

            _reanimationMediator.ReanimationLayout.SetProgress(dto.Progress);
            SetReanimationMode(true);
            _reanimationMediator.EnableWaitingMode();
        }

        public void DisableReanimationMode()
        {
            SetReanimationMode(false);
        }

        public void EnableAutoRespawn()
        {
            Hud.AutoRespawnLayout.Hide();
            _reanimationMediator.SetAutoRespawn(true);
        }

        public void DisableAutoRespawn()
        {
            Hud.AutoRespawnLayout.Show();
            _reanimationMediator.SetAutoRespawn(false);
        }

        public void ActivateReanimationProcess()
        {
            _reanimationMediator.EnableReanimationMode();
            _cameraSwitchMediator.EnableOnlyPlayerMode();
        }

        public void StopReanimationProcess()
        {
            _cameraSwitchMediator.EnableFreeSwitchingMode();
        }

        private void SetReanimationMode(bool enabled)
        {
            _progressBarsPanel.SetActive(!enabled);
            _hud.SmoothActivator.SetActive(!enabled);
            _inputMediator.SmoothActivator.SetActive(!enabled);
            _reanimationMediator.SmoothActivator.SetActive(enabled);

            if (enabled) _cameraSwitchMediator.EnableFreeSwitchingMode();
            else _cameraSwitchMediator.EnableOnlyPlayerMode();
        }
    }
}