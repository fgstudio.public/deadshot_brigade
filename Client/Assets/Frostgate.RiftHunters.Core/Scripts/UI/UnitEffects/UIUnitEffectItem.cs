using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.UI
{
    public class UIUnitEffectItem : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private Image _effectIcon;

        [SerializeField, Required, ChildGameObjectsOnly]
        private UISmoothActivator _smoothActivator;

        [Title("Settings")] [SerializeField] private AnimationCurve _animationCurve;
        [SerializeField] private float _hideDuration;

        private UnitEffectState _state;
        private Sequence _sequence;
        private static readonly Color _resetColor = new Color(1, 1, 1, 0);

        public void Init(UnitEffectState effectState)
        {
            if (effectState == _state)
                return;

            _state = effectState;
            
            if (_sequence != null)
                _sequence.Kill();
            
            _sequence = DOTween.Sequence();

            if (_state == null)
            {
                _sequence.Append(_effectIcon.DOFade(0, 0.5f));
            }
            else
            {
                _effectIcon.sprite = _state.Config.Sprite;
                _sequence.Append(_effectIcon.DOFade(1, 0.5f));

                if (!float.IsInfinity(_state.Config.Duration))
                {
                    

                    float startTime = _state.Config.Duration - _hideDuration < 0
                        ? 0
                        : _state.Config.Duration - _hideDuration;

                    float duration = _hideDuration < _state.Config.Duration
                        ? _hideDuration
                        : _state.Config.Duration;

                    _sequence = DOTween.Sequence();
                    _sequence.Insert(startTime, _effectIcon.DOFade(0, duration).SetEase(_animationCurve));
                }
            }

            _sequence.Play();
        }

        public void Reset()
        {
            if (_sequence != null)
                _sequence.Kill();
            
            _state = null;
            _effectIcon.color = _resetColor;
        }

        [Button]
        private void PlayHideAnimation()
        {
            _effectIcon.color = Color.white;
            _effectIcon.DOFade(0, _hideDuration).SetEase(_animationCurve);
        }
    }
}