﻿using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public class UIItem : MonoBehaviour
    {
        [SerializeField] private Image _image;
        
        public void Show(Sprite sprite)
        {
            _image.sprite = sprite;
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}