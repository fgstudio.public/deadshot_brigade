using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIEffectMessage : MonoBehaviour
    {
        [SerializeField] private TMP_Text Text;
        [SerializeField] private Material RedGlowMaterial;
        [SerializeField] private Material BlueGlowMaterial;

        [Header("Settings")]
        [SerializeField] private float _fadeTime = 0.3f;
        [SerializeField] private float _showDuration = 2.0f;
        [SerializeField] private Vector3 _endLocalPosition;

        private Sequence _sequence;

        public void ShowStrengtheningMessage() => Show("STRENGTHENING", true);

        public void Show(string messageText, bool isPositiveEffect)
        {
            Text.transform.localPosition = Vector3.zero;
            Text.text = messageText;
            Text.fontMaterial = isPositiveEffect ? BlueGlowMaterial : RedGlowMaterial;

            StartAnimation();
        }

        private void StartAnimation()
        {
            if (_sequence != null)
                _sequence.Kill();

            _sequence = DOTween.Sequence();
            _sequence.Append(Text.DOFade(1, _fadeTime))
                .Append(Text.gameObject.transform.DOLocalMove(_endLocalPosition, _showDuration))
                .Insert(_showDuration, Text.DOFade(0, _fadeTime))
                .Play();

            Text.alpha = 1;
        }

        [ContextMenu("Show buff")]
        private void ShowTestBuff() => Show("BUFF", true);

        [ContextMenu("Show debuff")]
        private void ShowTestDebuff() => Show("DEBUFF", false);
    }
}
