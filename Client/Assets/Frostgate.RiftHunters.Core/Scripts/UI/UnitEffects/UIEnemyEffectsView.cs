﻿using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Core.UI.Input;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public class UIEnemyEffectsView : MonoBehaviour
    {
        [SerializeField, Required] private UIItem _detonationAbilityItem;
        [SerializeField, Required] private UIItem[] _effectItems;

        [CanBeNull] private UnitNetworkEffects _unitNetworkEffects;
        [CanBeNull] private UnitNetworkState _unitNetworkState;
        private readonly List<UnitEffectConfig> _replaceableUnitEffects = new();

        private IUIBattleMediator _battleMediator;
        private bool _isDirty;

        public void Init(UnitNetworkEffects unitNetworkEffects, UnitNetworkState unitNetworkState,
            UnitNetwork playerUnit = null, IUIBattleMediator battleMediator = null)
        {
            _battleMediator = battleMediator;
            _replaceableUnitEffects.Clear();

            if (playerUnit != null)
            {
                foreach (var impact in playerUnit.UnitNetworkState.PropertiesProvider!.Impacts.AllImpacts)
                foreach (var effect in impact.Config!.Effects)
                foreach (var data in effect.AffectData)
                foreach (var replacement in data.UnitEffectReplacements)
                    _replaceableUnitEffects.Add(replacement.ReplaceableEffect);
            }

            _unitNetworkEffects = unitNetworkEffects;
            _unitNetworkState = unitNetworkState;
            Subscribe(_unitNetworkEffects, _unitNetworkState);

            _isDirty = true;
        }



        public void SetDetonationIcon(Sprite sprite) => _detonationAbilityItem.Show(sprite);

        public void HideDetonationIcon() => _detonationAbilityItem.Hide();

        public void OnPoolRelease()
        {
            if (_unitNetworkEffects != null && _unitNetworkState != null)
                Unsubscribe(_unitNetworkEffects, _unitNetworkState);

            foreach (var item in _effectItems)
                item.Hide();
        }

        private void Update()
        {
            if (!_isDirty)
                return;

            _isDirty = false;

            RecalculateReplacementIcon();
            RecalculateEffectItems();
        }

        private void RecalculateReplacementIcon()
        {
            if (_replaceableUnitEffects.Count <= 0)
                return;

            AbilityButtonDetonation buttonDetonation =
                _battleMediator?.InputMediator.AbilityButton.Detonation;

            foreach (var state in _unitNetworkEffects!.States)
                if (_replaceableUnitEffects.Contains(state.Config))
                {
                    buttonDetonation?.AddDetonationUnit(this);
                    return;
                }

            buttonDetonation?.RemoveDetonationUnit(this);
        }

        private void RecalculateEffectItems()
        {
            var index = 0;

            if (_unitNetworkState!.BehaviourState != BehaviourState.Dead)
                foreach (var state in _unitNetworkEffects.States)
                {
                    if (state.Config.Sprite == null)
                        continue;

                    _effectItems[index].Show(state.Config.Sprite);
                    index++;

                    if (index >= _effectItems.Length)
                        return;
                }

            while (index < _effectItems.Length)
            {
                _effectItems[index].Hide();
                index++;
            }
        }

        private void AddEffectView(UnitEffectState effectState) => _isDirty = true;

        private void RemoveEffectView(UnitEffectState effectState) => _isDirty = true;

        private void OnUnitBehaviourStateChange() => _isDirty = true;

        private void OnDestroy()
        {
            if (_unitNetworkEffects != null && _unitNetworkState != null)
                Unsubscribe(_unitNetworkEffects, _unitNetworkState);
        }

        private void Subscribe([NotNull] UnitNetworkEffects unitNetworkEffects,
            [NotNull] UnitNetworkState unitNetworkState)
        {
            unitNetworkEffects.OnAdd += AddEffectView;
            unitNetworkEffects.OnRemove += RemoveEffectView;
            unitNetworkState.BehaviourStateChanged += OnUnitBehaviourStateChange;
        }

        private void Unsubscribe([NotNull] UnitNetworkEffects unitNetworkEffects,
            [NotNull] UnitNetworkState unitNetworkState)
        {
            unitNetworkEffects.OnAdd -= AddEffectView;
            unitNetworkEffects.OnRemove -= RemoveEffectView;
            unitNetworkState.BehaviourStateChanged -= OnUnitBehaviourStateChange;
        }
    }
}