using UnityEngine;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.UI
{
    public class UIUnitEffectsView : MonoBehaviour
    {
        [SerializeField, Required] private UIUnitEffectItem[] _effectItems;

        [CanBeNull] private UnitNetworkEffects _unitNetworkEffects;
        [CanBeNull] private UnitNetworkState _unitNetworkState;

        private bool _isDirty;

        public void Init(UnitNetworkEffects unitNetworkEffects, UnitNetworkState unitNetworkState)
        {
            _unitNetworkEffects = unitNetworkEffects;
            _unitNetworkState = unitNetworkState;
            Subscribe(_unitNetworkEffects, _unitNetworkState);

            _isDirty = true;
        }

        public void OnPoolRelease()
        {
            if (_unitNetworkEffects != null && _unitNetworkState != null)
                Unsubscribe(_unitNetworkEffects, _unitNetworkState);

            foreach (var item in _effectItems)
                item.Reset();
        }

        private void Update()
        {
            if (!_isDirty)
                return;

            _isDirty = false;

            RecalculateEffectItems();
        }

        private void RecalculateEffectItems()
        {
            var index = 0;

            foreach (var state in _unitNetworkEffects.States)
            {
                if (state.Config.Sprite == null)
                    continue;

                _effectItems[index].Init(state);
                index++;

                if (index >= _effectItems.Length)
                    return;
            }

            while (index < _effectItems.Length)
            {
                _effectItems[index].Init(null);
                index++;
            }
        }

        private void AddEffectView(UnitEffectState effectState) => _isDirty = true;

        private void RemoveEffectView(UnitEffectState effectState) => _isDirty = true;

        private void OnDestroy()
        {
            if (_unitNetworkEffects != null && _unitNetworkState != null)
                Unsubscribe(_unitNetworkEffects, _unitNetworkState);
        }

        private void Subscribe([NotNull] UnitNetworkEffects unitNetworkEffects,
            [NotNull] UnitNetworkState unitNetworkState)
        {
            unitNetworkEffects.OnAdd += AddEffectView;
            unitNetworkEffects.OnRemove += RemoveEffectView;
        }

        private void Unsubscribe([NotNull] UnitNetworkEffects unitNetworkEffects,
            [NotNull] UnitNetworkState unitNetworkState)
        {
            unitNetworkEffects.OnAdd -= AddEffectView;
            unitNetworkEffects.OnRemove -= RemoveEffectView;
        }
    }
}