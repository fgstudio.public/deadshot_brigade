﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI.Hud;
using Frostgate.RiftHunters.Core.UI.Hud.Pointing;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIBattleInstaller))]
    public sealed class UIBattleInstaller : MonoInstaller
    {
        [SerializeField, Required] private UIBattleMediator _battleMediator;

        public override void InstallBindings()
        {
            Container.Bind<IUIBattleMediator>().FromInstance(_battleMediator).AsSingle();

            IHudMediator hud = _battleMediator.Hud;
            Container.Bind<IHudMediator>().FromInstance(hud).AsSingle();
            Container.Bind<ITeamPanel>().FromInstance(hud.TeamPanel).AsSingle();

            IPointerPanelRoster pointerPanels = hud.PointerPanels;
            Container.Bind<IPointerPanelRoster>().FromInstance(pointerPanels).AsSingle();
            Container.Bind<ITargetPanel>().FromInstance(pointerPanels.TargetPanel).AsSingle();
            Container.Bind<IAllyPanel>().FromInstance(pointerPanels.AllyPanel).AsSingle();
        }
    }
}