using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public interface ITooltipService
    {
        void Show(string title, string description, Vector2 clickPosition);
    }
}
