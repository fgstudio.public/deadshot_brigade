﻿using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public class UITooltipPanel : MonoBehaviour
    {
        public RectTransform Rect => _rect;

        [SerializeField] private TMP_Text _title;
        [SerializeField] private TMP_Text _description;
        [SerializeField] private RectTransform _rect;

        public void Init(string title, string descriptions)
        {
            _title.text = title;
            _description.text = descriptions;
        }
    }
}