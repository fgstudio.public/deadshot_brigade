using System.Collections;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public class UITooltipService : MonoBehaviour, ITooltipService
    {
        [SerializeField] private UITooltipWindow _windowPrefab;
        [SerializeField] private UITooltipPanel _panelPrefab;

        private UITooltipWindow _windowInstance;
        private UITooltipPanel _panel;

        private Canvas _canvas;

        private void Awake()
        {
            _windowInstance = Instantiate(_windowPrefab, transform);
            _panel = Instantiate(_panelPrefab, _windowInstance.transform);

            _canvas = GetComponent<Canvas>();

            _windowInstance.gameObject.SetActive(false);
        }

        public void Show(string title, string description, Vector2 clickPosition)
        {
            _windowInstance.Clicked += OnClicked;
            _windowInstance.gameObject.SetActive(true);

            _panel.Init(title, description);

            //Костыль, при первом включении sizeDelta = 0, так как размер устанавливает ContentSizeFitter
            //поэтмоу запускаем корутину, чтобы подождать фрейм
            if (_panel.Rect.sizeDelta.x < 1)
            {
                StartCoroutine(LateInitialize(title, description, clickPosition));
                _panel.transform.position = new Vector3(5000, 0, 0);
                return;
            }

            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
                    _windowPrefab.Rect,
                    clickPosition,
                    _canvas.worldCamera,
                    out Vector2 localPoint))
            {
                var screen = _windowInstance.Rect;
                var panelRect = _panel.Rect;

                var scaleX = _canvas.transform.localScale.x;
                var scaleY = _canvas.transform.localScale.y;

                panelRect.pivot = Vector2.zero;

                if (localPoint.x + panelRect.sizeDelta.x * scaleX > screen.rect.size.x * scaleX)
                {
                    panelRect.pivot = new Vector2(1, panelRect.pivot.y);
                }

                if (localPoint.y + panelRect.sizeDelta.y * scaleY > screen.rect.size.y * scaleY)
                {
                    panelRect.pivot = new Vector2(panelRect.pivot.x, 1);
                }

                panelRect.transform.position = localPoint;
            }
        }

        private IEnumerator LateInitialize(string title, string description, Vector2 clickPosition)
        {
            yield return null;
            Show(title, description, clickPosition);
        }

        private void OnClicked(Vector2 position)
        {
            _windowInstance.Clicked -= OnClicked;
            _windowInstance.gameObject.SetActive(false);
        }
    }
}