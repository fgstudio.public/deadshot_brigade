using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI
{
    public class TooltipData
    {
        public string Title;
        public string Description;

        public TooltipData(string title, string description)
        {
            Title = title;
            Description = description;
        }
    }

    [DisallowMultipleComponent]
    [RequireComponent(typeof(ITooltipInfo))]
    public sealed class UITooltipTrigger : MonoBehaviour, IPointerClickHandler
    {
        private ITooltipInfo _tooltipInfo;
        private ITooltipService _tooltipService;
        private Transform _transform;

        [Inject]
        private void MonoConstructor(ITooltipService tooltipService)
        {
            _tooltipService = tooltipService;
        }

        private void Awake()
        {
            _tooltipInfo = GetComponent<ITooltipInfo>();
            _transform = GetComponent<Transform>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if(_tooltipInfo == null)
                return;

            var tooltipData = _tooltipInfo.GetInfo();
            var position = _transform.position;
            _tooltipService.Show(tooltipData.Title, tooltipData.Description, new Vector2(position.x, position.y));
        }
    }
}
