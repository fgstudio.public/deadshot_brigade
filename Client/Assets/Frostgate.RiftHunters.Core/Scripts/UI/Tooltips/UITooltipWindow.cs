﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public class UITooltipWindow : MonoBehaviour
    {
        public RectTransform Rect => _rect;

        public event Action<Vector2> Clicked;

        [SerializeField] private RectTransform _rect;
        [SerializeField] private Button _button;

        public void Awake()
        {
            _button.onClick.AddListener(OnButtonClick);
        }

        public void OnDestroy()
        {
            _button.onClick.RemoveListener(OnButtonClick);
        }

        public void OnButtonClick()
        {
            Clicked?.Invoke(Vector2.zero);
        }
    }
}