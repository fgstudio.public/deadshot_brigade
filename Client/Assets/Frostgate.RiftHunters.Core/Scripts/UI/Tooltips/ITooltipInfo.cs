using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public interface ITooltipInfo
    {
        TooltipData GetInfo();
    }
}
