﻿using System;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.UI
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, Inherited = false)]
    [BaseTypeRequired(typeof(IUIScreen))]
    public sealed class UIScreenPrefabAttribute : Attribute
    {
        public string PrefabAddress { get; }

        private readonly string _targetClassName;

        public UIScreenPrefabAttribute(string prefabAddress, [CallerMemberName] string targetClassName = "")
        {
            ValidateAddress(prefabAddress);

            PrefabAddress = prefabAddress;
            _targetClassName = targetClassName;
        }

        private void ValidateAddress(string address)
        {
            if (string.IsNullOrWhiteSpace(address))
                throw new ArgumentException($"Invalid prefab address for {_targetClassName} screen type.");
        }
    }
}
