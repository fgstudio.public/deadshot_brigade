﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public abstract class UIScreen : MonoBehaviour, IUIScreen
    {
        GameObject IGameObject.GameObject => this != null? gameObject : null;
    }
}
