﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.UI
{
    public interface IUIService
    {
        UniTask<T> ShowAsync<T>(bool withDestroy = false) where T : IUIScreen;
        void Hide(bool withDestroy = false);
        void ShowPrevious();
    }
}