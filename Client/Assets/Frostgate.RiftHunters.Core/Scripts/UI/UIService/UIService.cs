﻿using System;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class UIService : IUIService
    {
        [NotNull] private readonly RectTransform _screenContainer;
        [NotNull] private readonly IPrefabFactory _prefabFactory;
        [NotNull] private readonly IScreenPrefabLoader _prefabLoader;

        private IUIScreen _activeScreen;
        private IUIScreen _previousScreen;

        public UIService([NotNull] RectTransform screenContainer, [NotNull] IPrefabFactory prefabFactory,
            [NotNull] IScreenPrefabLoader prefabLoader)
        {
            _screenContainer = screenContainer;
            _prefabFactory = prefabFactory;
            _prefabLoader = prefabLoader;
        }

        public async UniTask<T> ShowAsync<T>(bool withDestroy = false) where T : IUIScreen
        {
            if (_activeScreen is T targetScreen)
            {
                targetScreen.GameObject?.SetActive(true);
                return targetScreen;
            }

            Hide(withDestroy);
            if (!withDestroy)
            {
                Hide(false);
                if(_previousScreen != null)
                    Object.Destroy(_previousScreen.GameObject);
                _previousScreen = _activeScreen;
            }

            T screen = await InstantiateScreenAsync<T>();
            _activeScreen = screen;

            return screen;
        }

        public void Hide(bool withDestroy)
        {
            if (_activeScreen == null)
                return;

            if (withDestroy)
            {
                Object.Destroy(_activeScreen.GameObject);
                _activeScreen = null;
            }
            else
            {
                _activeScreen.GameObject?.SetActive(false);
            }
        }

        public void ShowPrevious()
        {
            if(_previousScreen == null)
                return;

            _previousScreen.GameObject?.SetActive(true);
            Object.Destroy(_activeScreen.GameObject);
            _activeScreen = _previousScreen;
            _previousScreen = null;
        }

        private async UniTask<T> InstantiateScreenAsync<T>() where T : IUIScreen
        {
            string prefabAddr = GetPrefabAttr<T>().PrefabAddress;
            GameObject screenPrefab = await _prefabLoader.LoadPrefabAsync(prefabAddr);

            return _prefabFactory.Instantiate<T>(screenPrefab, _screenContainer);
        }

        private UIScreenPrefabAttribute GetPrefabAttr<T>() where T : IUIScreen
        {
            Type screenType = typeof(T);
            object[] attributes = screenType.GetCustomAttributes(typeof(UIScreenPrefabAttribute), false);
            if (attributes.Length == 0)
                throw new InvalidOperationException(
                    $"Screen {screenType.Name} has no {nameof(UIScreenPrefabAttribute)}");

            return (UIScreenPrefabAttribute)attributes[0];
        }
    }
}