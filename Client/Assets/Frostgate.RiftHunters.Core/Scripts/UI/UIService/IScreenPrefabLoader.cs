﻿using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public interface IScreenPrefabLoader
    {
        UniTask<GameObject> LoadPrefabAsync(string address);
    }
}