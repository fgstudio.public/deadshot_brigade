using System;
using DG.Tweening;
using JetBrains.Annotations;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI
{
    public interface ISmoothActivation
    {
        ISmoothActivator SmoothActivator { get; }
    }

    public interface ISmoothActivator
    {
        bool IsActive { get; }

        bool TryActivate();
        void SetActive(bool isActive, Action onStart = null, Action onComplete = null);
        void Activate(Action onStart = null, Action onComplete = null);
        void Deactivate(Action onStart = null, Action onComplete = null);
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(ComponentMenus.RiftHunters.Menu + "/" + nameof(UISmoothActivator))]
    public sealed class UISmoothActivator : MonoBehaviour, ISmoothActivator
    {
        private const float ActivatedAlpha = 1;
        private const float DeactivatedAlpha = 0;

        [Header("Transition")]
        [SerializeField, Required, ChildGameObjectsOnly, CanBeNull] private CanvasGroup _canvasGroup;
        [SerializeField, Range(0, 1)] private float _transitionDuration;
        [SerializeField] private bool _startFromCurrent = true;

        private float CurrentAlpha => _canvasGroup != null ? _canvasGroup.alpha : default;
        public bool IsActive { get; private set; }

        private void OnValidate() =>
            _canvasGroup ??= GetComponentInChildren<CanvasGroup>();

        private void Start() =>
            IsActive = gameObject.activeSelf;

        public bool TryActivate()
        {
            if (!IsActive) Activate();
            return !IsActive;
        }

        public void SetActive(bool isActive, Action onStart = null, Action onComplete = null)
        {
            if (isActive) Activate(onStart, onComplete);
            else Deactivate(onStart, onComplete);
        }

        public void Activate(Action onStart = null, Action onComplete = null) =>
            PlayTransition(
                startAlpha: _startFromCurrent ? CurrentAlpha : DeactivatedAlpha,
                targetAlpha: ActivatedAlpha,
                duration: _transitionDuration,
                onStart: () =>
                {
                    IsActive = true;
                    gameObject.SetActive(true);
                    onStart?.Invoke();
                },
                onComplete);

        public void Deactivate(Action onStart = null, Action onComplete = null) =>
            PlayTransition(
                startAlpha: _startFromCurrent ? CurrentAlpha : ActivatedAlpha,
                targetAlpha: DeactivatedAlpha,
                duration: _transitionDuration,
                onStart: () =>
                {
                    IsActive = false;
                    onStart?.Invoke();
                },
                onComplete: () =>
                {
                    onComplete?.Invoke();
                    gameObject.SetActive(false);
                });

        private void PlayTransition(float startAlpha, float targetAlpha, float duration, Action onStart = null, Action onComplete = null)
        {
            if (_canvasGroup == null)
                return;

            onStart?.Invoke();

            _canvasGroup.DOKill();
            _canvasGroup.alpha = startAlpha;

            bool isCompleted = Mathf.Abs(startAlpha - targetAlpha) < Mathf.Epsilon;

            if (isCompleted)
                onComplete?.Invoke();

            else
                _canvasGroup.DOFade(targetAlpha, duration)
                    .OnComplete(() => onComplete?.Invoke());
        }
    }
}
