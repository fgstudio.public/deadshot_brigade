using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(BarColorRepository))]
    public sealed class BarColorRepository : MonoBehaviour
    {
        [field: SerializeField] public Color SelfColor { get; private set; }
        [field: SerializeField] public Color EnemyColor { get; private set; }
        [field: SerializeField] public Color FriendColor { get; private set; }
        [field: SerializeField] public Color InvulnerabilityColor { get; private set; }
        [field: SerializeField] public Color SelfColorDark { get; private set; }
        [field: SerializeField] public Color EnemyColorDark { get; private set; }
        [field: SerializeField] public Color FriendColorDark { get; private set; }
    }
}