using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Frostgate.RiftHunters.Core.Components;


namespace Frostgate.RiftHunters.Core.UI
{

    [RequireComponent(typeof(Bar))]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(SegmentedBar))]
    public sealed class SegmentedBar : MonoBehaviour, IUnityEventUpdateable
    {
        public UnityEvent Updated => _bar.Updated;

        public AnimatedFader Fader => _bar.Fader;
        public BarColor Color => _bar.Color;
        public float SegmentRatio => _bar.Max > 0 ? _sizeSegment / _bar.Max : 0;
        public NetworkPlayerObjectComponent NetworkComponent => _networkComponent;


        [SerializeField, Required] private Bar _bar;
        [SerializeField, Required] private RectTransform _separatorSample;
        [SerializeField, Required, ChildGameObjectsOnly] private RectTransform _separatorParent;
        [SerializeField, Required] private NetworkPlayerObjectComponent _networkComponent;

        private float _sizeSegment;
        private readonly List<RectTransform> _separators = new();


        private void OnValidate()
        {
            _bar ??= GetComponent<Bar>();
            _networkComponent ??= GetComponent<NetworkPlayerObjectComponent>();
        }


        public void SetCurrent(float current) => Set(current, _bar.Max, _sizeSegment);
        public void SetMax(float max) => Set(_bar.Current, max, _sizeSegment);
        public void Set(float current, float max, float sizeSegment)
        {
            _bar.Set(current, max);
            SetSizeSegment(sizeSegment);
        }
        public void SetSizeSegment(float size)
        {
            _sizeSegment = size;

            int needSeparators = Mathf.CeilToInt(_bar.Max / _sizeSegment) - 1;
            int deltaCount = needSeparators - _separators.Count;

            if (deltaCount == 0 || _sizeSegment < float.Epsilon)
                return;

            if (deltaCount < 0)
            {
                int lastIndex = _separators.Count - 1;
                int deleteFrom = lastIndex + deltaCount;

                for (int i = lastIndex; i >= deleteFrom; i--)
                {
                    Destroy(_separators[i].gameObject);
                    _separators.RemoveAt(i);
                }
            }
            else
            {
                for (int i = 0; i < deltaCount; i++)
                    _separators.Add(Instantiate(_separatorSample, _separatorParent));
            }

            UpdateSeparatorsPos();
        }

        private void UpdateSeparatorsPos()
        {
            float xPosition = 0;
            foreach (var separator in _separators)
            {
                xPosition += SegmentRatio;

                separator.anchorMin = separator.anchorMin.SetX(xPosition);
                separator.anchorMax = separator.anchorMax.SetX(xPosition);
                separator.anchoredPosition = Vector2.zero;
            }
        }
    }
}