﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI
{
    [RequireComponent(typeof(RectTransform))]
    [DisallowMultipleComponent]
    public class MonoUIBarPanel<TBar> : MonoBehaviour, IUIBarPanel<TBar> where TBar : IUIBarPanelItem
    {
        public GameObject GameObject => Transform.gameObject;

        [field: SerializeField, Required, ChildGameObjectsOnly]
        public RectTransform Transform { get; private set; }

        private UIBarPanel<TBar> _panel;
        private ILogger _logger;

        private void OnValidate() => InitReferences();

        [Inject]
        private void MonoConstruct(ILogger<MonoUIBarPanel> logger)
        {
            _logger = logger;
        }

        private void Awake()
        {
            InitReferences();
            CreateBarPanel();
        }

        public void Add(TBar item)
        {
            if (_panel == null)
                CreateBarPanel();
            _panel.Add(item);
        }

        public void Remove(TBar item)
        {
            if (_panel != null)
                _panel.Remove(item);
        }

        public void Remove(TBar item, Action removedCallback)
        {
            if (_panel != null)
                _panel.Remove(item, removedCallback);
        }

        public bool Contains(TBar item)
        {
            if (_panel != null)
                return _panel.Contains(item);

            _logger.LogError("_panel is null!");
            return false;
        }

        private void InitReferences()
        {
            Transform ??= GetComponent<RectTransform>();
        }

        private void CreateBarPanel()
        {
            _panel = new UIBarPanel<TBar>(new UIPanel<TBar>(GetComponent<RectTransform>()));
        }
    }

    [RequireComponent(typeof(RectTransform))]
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(MonoUIBarPanel))]
    public class MonoUIBarPanel : MonoUIBarPanel<IUIBarPanelItem>
    {
    }
}