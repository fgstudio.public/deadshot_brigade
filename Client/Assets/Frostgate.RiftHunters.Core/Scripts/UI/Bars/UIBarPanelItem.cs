﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    [RequireComponent(typeof(RectTransform))]
    public class UIBarPanelItem : MonoBehaviour, IUIBarPanelItem
    {
        GameObject IGameObject.GameObject => this != null? gameObject : null;

        [field: SerializeField, Required]
        public RectTransform Transform { get; private set; }


        [field: SerializeField, Required]
        public UISmoothActivator SmoothActivator { get; private set; }

        [field: SerializeField, Required]
        public RisingAnimationComponent RisingAnimation { get; private set; }

        private void OnValidate() => InitReferences();
        private void Awake() => InitReferences();

        private void InitReferences()
        {
            Transform ??= GetComponent<RectTransform>();
            RisingAnimation ??= GetComponent<RisingAnimationComponent>();
            SmoothActivator ??= GetComponent<UISmoothActivator>();
        }
    }
}