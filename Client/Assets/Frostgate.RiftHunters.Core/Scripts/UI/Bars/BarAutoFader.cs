using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Components;

namespace Frostgate.RiftHunters.Core.UI
{
    /// <summary>
    /// Компонент для автоматического исчезновения и появления HealhBar.
    /// <see cref="https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#5925f4ce2b9c413f87ee3928901671e7"/>
    /// <seealso cref="Bar"/>
    /// </summary>
    [RequireComponent(typeof(Bar))]
    [RequireComponent(typeof(TimerComponent))]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(BarAutoFader))]
    public sealed class BarAutoFader : Component
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly, ReadOnly] private Bar _bar;
        [SerializeField, Required, ChildGameObjectsOnly, ReadOnly] private TimerComponent _timer;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _timer ??= GetComponent<TimerComponent>();
            _bar ??= GetComponent<Bar>();
        }

        protected override void OnEnabled()
        {
            Subscribe();
            _timer.Enable();
        }

        protected override void OnDisabled()
        {
            Unsubscribe();
            _timer.Disable();
        }

        private void Subscribe()
        {
            _timer.Elapsed.AddListener(OnTimerElapsed);
            _bar.Updated.AddListener(OnHealthBarUpdated);
        }

        private void Unsubscribe()
        {
            _timer.Elapsed.RemoveListener(OnTimerElapsed);
            _bar.Updated.RemoveListener(OnHealthBarUpdated);
        }

        private void OnTimerElapsed() =>
            _bar.Fader.Fade();

        private void OnHealthBarUpdated()
        {
            _timer.Disable();
            _timer.Enable();
            _bar.Fader.Unfade();
        }
    }
}