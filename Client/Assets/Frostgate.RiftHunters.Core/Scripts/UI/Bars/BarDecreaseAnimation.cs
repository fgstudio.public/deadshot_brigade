using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public class BarDecreaseAnimation : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private Image _damagedFill;
        [SerializeField, Required, ChildGameObjectsOnly] private Slider _slider;
        [SerializeField, Required, ChildGameObjectsOnly] private Bar _bar;

        [SerializeField] private float _animationDuration = 1.0f;
        [SerializeField] private float _animationDelay = 0.5f;

        private float _delayTime;

        public void Start() => _delayTime = 0;

        private void Update()
        {
            if (_delayTime < _animationDelay)
            {
                _delayTime += Time.deltaTime;
            }
            else if (!Mathf.Approximately(_damagedFill.fillAmount, _slider.value))
            {
                _delayTime = 0;
                _damagedFill.DOFillAmount(_slider.value, _animationDuration);
            }
        }
    }
}
