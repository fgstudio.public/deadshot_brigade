﻿namespace Frostgate.RiftHunters.Core.UI
{
    public interface IUIBarPanelItem : IUIPanelItem
    {
        UISmoothActivator SmoothActivator { get; }
        RisingAnimationComponent RisingAnimation { get; }
    }
}