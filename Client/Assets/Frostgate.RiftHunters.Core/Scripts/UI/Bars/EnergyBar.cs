using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public sealed class EnergyBar : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private Bar _bar;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject _glow;
        [SerializeField, FoldoutGroup("Settings")] private float _animationScale;
        [SerializeField, FoldoutGroup("Settings")] private float _animationDuration;

        private Tweener _tweener;

        public void SetCurrent(float current)
        {
            if (_bar == null || _glow == null)
                return;

            _bar.SetCurrent(current);

            if (Mathf.Approximately(current, _bar.Max))
            {
                if (!_glow.activeSelf)
                    ActivateGlow();
            }
            else if (_glow.activeSelf)
            {
                DeactivateGlow();
            }
        }

        public void Set(float current, float max) => _bar.Set(current, max);

        private void ActivateGlow()
        {
            _glow.SetActive(true);
            _tweener = transform.DOScale(_animationScale, _animationDuration).SetLoops(2, LoopType.Yoyo);
        }

        private void DeactivateGlow()
        {
            _glow.SetActive(false);
            _tweener?.Kill();
        }
    }
}
