﻿using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.UI;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIHealthBarPanelItem))]
    public class UIHealthBarPanelItem : UIBarPanelItem
    {
        [field: SerializeField, Required, ChildGameObjectsOnly]
        public UISmoothSlider Slider { get; private set; }

        [CanBeNull, ShowInInspector, ReadOnly]
        public IHealth Health
        {
            get => _health;
            set
            {
                if (value != null)
                    SetHealth(value);
                else
                    RemoveHealth();
            }
        }

        private IHealth _health;

        private void OnDestroy() => RemoveHealth();

        private void SetHealth(IHealth value)
        {
            if (value == _health)
                return;

            RemoveHealth();

            _health = value;
            SubscribeHealth();
        }

        private void RemoveHealth()
        {
            if (_health != null)
                UnsubscribeHealth();

            _health = null;
        }

        private void SubscribeHealth() => _health.HealthChanged += OnHealthChanged;

        private void UnsubscribeHealth() => _health.HealthChanged -= OnHealthChanged;

        private void OnHealthChanged(float _) => UpdateSlider(Health);

        private void UpdateSlider(IHealth health)
        {
            float hp = health.Health;
            float maxHp = health.MaxHealth;

            float value = maxHp > 0 ? hp / maxHp : 0;
            Slider.SetValue(value);
        }
    }
}