using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.UI
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(BarColor))]
    public sealed class BarColor : MonoBehaviour
    {
        [Header("References")]
        [SerializeField, Required] private Image _fillImage;
        [SerializeField, Required] private Image _backgroundImage;
        [SerializeField, Required] private BarColorRepository _colorRepository;

        private Color _defaultColor = Color.white;
        private Color _defaultBackgroundColor = Color.white;
        public void UpdateColor(bool isLocalPlayer, BattleUnitType battleUnitType)
        {
            if (isLocalPlayer)
                SetColor(_colorRepository.SelfColor, _colorRepository.SelfColorDark);

            else if (battleUnitType == BattleUnitType.Player)
                SetColor(_colorRepository.FriendColor, _colorRepository.FriendColorDark);

            else
                SetColor(_colorRepository.EnemyColor, _colorRepository.EnemyColorDark);
        }

        public void UpdateBarState(bool isInvincible)
        {
            if (isInvincible)
            {
                _backgroundImage.color = _colorRepository.InvulnerabilityColor;
                _fillImage.color = _colorRepository.InvulnerabilityColor;
            }
            else
            {
                _backgroundImage.color = _defaultBackgroundColor;
                _fillImage.color = _defaultColor;
            }
        }

        private void SetColor(Color color, Color darkColor)
        {
            _backgroundImage.color = darkColor;
            _fillImage.color = color;
            _defaultColor = color;
            _defaultBackgroundColor = darkColor;
        }
    }
}