﻿using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.UI
{
    public interface IUIBarPanel<in TBarPanelItem> : IUIPanel<TBarPanelItem> where TBarPanelItem : IUIBarPanelItem
    {
        void Remove([NotNull] TBarPanelItem item, [NotNull] Action removedCallback);
    }
}