﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public class UIBarPanel<TBar> : IUIBarPanel<TBar> where TBar : IUIBarPanelItem
    {
        public GameObject GameObject => _panel.GameObject;
        public RectTransform Transform => _panel.Transform;

        [NotNull] private readonly IUIPanel<TBar> _panel;

        public UIBarPanel([NotNull] IUIPanel<TBar> panel)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(panel, nameof(panel));
            _panel = panel;
        }

        public void Add(TBar item)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(item, nameof(item));

            _panel.Add(item);
            item.SmoothActivator.SetActive(true);
            item.RisingAnimation.Show();
        }

        public void Remove(TBar item) => Remove(item, delegate { });

        public void Remove(TBar item, Action removedCallback)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(item, nameof(item));
            ThrowHelper.ThrowIfArgumentNullOrDefault(removedCallback, nameof(removedCallback));
            if (item is MonoBehaviour mono && mono == null) return;

            item.RisingAnimation.Hide();
            item.SmoothActivator.SetActive(false, onComplete: () =>
            {
                _panel.Remove(item);
                removedCallback.Invoke();
            });
        }

        public bool Contains(TBar item)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(item, nameof(item));
            return _panel.Contains(item);
        }
    }
}