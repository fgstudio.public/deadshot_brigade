using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.UI
{
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(Bar))]
    public sealed class Bar : MonoBehaviour, IUnityEventUpdateable
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Slider _slider;
        [SerializeField, Required, ChildGameObjectsOnly] private AnimatedFader _fader;
        [SerializeField, Required, ChildGameObjectsOnly] private BarColor _color;

        [Header("Settings")]
        [SerializeField, Range(0, 1)] private float _minSliderValue = 0.08f;
        [SerializeField, Range(0, 1)] private float _maxSliderValue = 0.92f;

        [field: FoldoutGroup("Events")]
        [field: SerializeField] public UnityEvent Updated { get; private set; }

        public AnimatedFader Fader => _fader;
        public BarColor Color => _color;
        public float Max { get; private set; }
        public float Current { get; private set; }

        private bool IsVisible => Ratio > 0;
        private float Ratio => Max > 0 ? Current / Max : 0;
        private float Diapason => _maxSliderValue - _minSliderValue;

        public void SetCurrent(float current) => Set(current, Max);
        public void SetMax(float max) => Set(Current, max);
        public void Set(float current, float max)
        {
            Current = current;
            Max = max;
            UpdateBar();
        }

        public void SetInvincibleState(bool isInvincible) => _color.UpdateBarState(isInvincible);

        private void UpdateBar()
        {
            if (IsVisible) _fader.Unfade();
            else _fader.Fade();

            var value = Ratio * Diapason + _minSliderValue;
            _slider.value = value;
            Updated.Invoke();
        }
    }
}