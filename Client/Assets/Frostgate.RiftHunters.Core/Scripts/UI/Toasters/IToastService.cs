namespace Frostgate.RiftHunters.Core.UI.Toasters
{
    public interface IToastService
    {
        void Show(string message);
    }
}
