using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Toasters
{
    public sealed class UIToastMessage : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private TMP_Text _message;
        [SerializeField, Required, ChildGameObjectsOnly]
        private UISmoothActivator _smoothActivator;

        [SerializeField, Header("Settings")]
        private float _lifeTime = 1f;
        [SerializeField, Header("Settings")]
        private Vector3 _movement = Vector3.zero;

        private Sequence _sequence;

        public void Show(string message)
        {
            _message.text = message;
            StopAllCoroutines();
            _smoothActivator.Activate(null, StartAnimation);
        }

        public void Hide() => _smoothActivator.Deactivate(null, DestroySelf);

        private void StartAnimation()
        {
            if(_sequence != null)
                _sequence.Kill();

            _sequence = DOTween.Sequence();
            _sequence.Append(transform.DOLocalMove(_movement, _lifeTime))
                .InsertCallback(_lifeTime, Hide);
        }

        private void DestroySelf() => Destroy(gameObject);
    }
}
