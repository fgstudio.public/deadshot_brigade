using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Toasters
{
    public sealed class UIToastService : MonoBehaviour, IToastService
    {
        [SerializeField, Required] private UIToastMessage toastMessagePrefab;
        [SerializeField, Required] private Transform spawnPoint;

        public void Show(string message)
        {
            var toaster = Instantiate(toastMessagePrefab, spawnPoint);
            toaster.Show(message);
        }
    }
}
