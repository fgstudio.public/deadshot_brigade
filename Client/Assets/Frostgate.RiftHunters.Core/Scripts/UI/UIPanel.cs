﻿using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    public class UIPanel<TPanelItem> : IUIPanel<TPanelItem> where TPanelItem : IUIPanelItem
    {
        public GameObject GameObject => Transform.gameObject;
        public RectTransform Transform { get; }

        private readonly UIContainer<TPanelItem> _itemContainer;

        public UIPanel([NotNull] RectTransform container)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(container, nameof(container));
            Transform = container;
            _itemContainer = new UIContainer<TPanelItem>(Transform);
        }

        public void Add(TPanelItem item)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(item, nameof(item));
            _itemContainer.Add(item);
            ResetScaleFactor(item);
        }

        public void Remove(TPanelItem item)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(item, nameof(item));
            _itemContainer.Remove(item);
        }

        public bool Contains(TPanelItem item)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(item, nameof(item));
            return _itemContainer.Contains(item);
        }

        private void ResetScaleFactor(TPanelItem item) => item.Transform.localScale = Vector3.one;
    }
}