﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.UI
{
    public interface IUIPanel<in TPanelItem> : IUIObject where TPanelItem : IUIPanelItem
    {
        void Add([NotNull] TPanelItem item);
        void Remove([NotNull] TPanelItem item);
        bool Contains([NotNull] TPanelItem item);
    }
}