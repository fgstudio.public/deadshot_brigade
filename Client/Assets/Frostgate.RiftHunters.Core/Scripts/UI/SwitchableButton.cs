using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI.Input;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Button))]
    [AddComponentMenu(ComponentMenus.RiftHunters.UI.Menu + "/" + nameof(SwitchableButton))]
    public sealed class SwitchableButton : Component, IInteractable
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly, ReadOnly] private Button _button;
        [Required, ChildGameObjectsOnly] public GameObject ActiveLayout;
        [Required, ChildGameObjectsOnly] public GameObject InactiveLayout;

        [field: SerializeField, FoldoutGroup("Button Events")]
        public UnityEvent<bool> Switched { get; private set; }

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() => _button ??= GetComponent<Button>();

        protected override void OnEnabled() => UpdateState(true);
        protected override void OnDisabled() => UpdateState(false);

        void IInteractable.SetInteractable(bool interactable) => enabled = interactable;
        private void UpdateState(bool enabled)
        {
            _button.interactable = enabled;
            ActiveLayout.SetActive(enabled);
            InactiveLayout.SetActive(!enabled);

            Switched?.Invoke(enabled);
        }
    }
}
