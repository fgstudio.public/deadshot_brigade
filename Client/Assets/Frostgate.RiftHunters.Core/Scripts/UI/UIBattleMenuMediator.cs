using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Input;
using Zenject;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.UI.Menu + "/" + nameof(UIBattleMenuMediator))]
    public sealed class UIBattleMenuMediator : MonoBehaviour, ISmoothActivation
    {
        [Header("Dependencies")]
        [SerializeField, Required, ChildGameObjectsOnly] private UISmoothActivator _smoothActivator;
        [SerializeField, Required, ChildGameObjectsOnly] private UIModalViewMediator modalView;

        [Header("Elements")]
        [SerializeField, Required, ChildGameObjectsOnly] private Button _exitButton;

        private IGameStateMachine _stateMachine;
        private PlayerInput _playerInput;

        [Inject]
        private void MonoConstructor(IGameStateMachine stateMachine, PlayerInput playerInput)
        {
            _stateMachine = stateMachine;
            _playerInput = playerInput;
        }

        public ISmoothActivator SmoothActivator => _smoothActivator;

        private void OnEnable() => _exitButton.onClick.AddListener(OpenModal);

        private void OpenModal()
        {
            PlayerInputDeactivate();

            modalView.SmoothActivator.Activate();
            modalView.SetTitle("To drop out?");
            modalView.SetupGreyButton("Leave", () => modalView.SmoothActivator.Deactivate(null, ExitToMainMenu));
            modalView.SetupGreenButton("Return", () => modalView.SmoothActivator.Deactivate(null, PlayerInputActivate));
        }

        private void ExitToMainMenu()
        {
            PlayerInputActivate();
            _stateMachine.EnterAsync<GameLoadMainSceneState, ClientConnectionError>(ClientConnectionError.None);
        }

        private void PlayerInputDeactivate() => _playerInput.gameObject.SetActive(false);
        private void PlayerInputActivate() => _playerInput.gameObject.SetActive(true);
    }
}