using Frostgate.RiftHunters.Core.Utils;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI.Hud
{
    public interface IFloatingTextsMediator
    {
        Transform Container { get; }
        Vector3 GetScreenPosition(Vector3 targetPosition);
        Vector3 GetScreenPositionWithRandomOffset(Vector3 targetPosition);
    }

    public sealed class UIFloatingTextsMediator : MonoBehaviour, IFloatingTextsMediator
    {
        [SerializeField, Required, ChildGameObjectsOnly] private CameraProvider _cameraProvider;
        [SerializeField, Required] private Transform _transform;
        [SerializeField, Required] private RectTransform _area;
        [SerializeField] private Vector3 _maxRandomOffset;

        public Transform Container => _transform;
        private Camera Camera => _cameraProvider.Camera;

        public Vector3 GetScreenPosition(Vector3 targetPosition)
        {
            Vector3 screenPosition = Camera.WorldToScreenPoint(targetPosition);
            return _area.InverseTransformPoint(screenPosition);
        }

        public Vector3 GetScreenPositionWithRandomOffset(Vector3 targetPosition)
        {
            Vector3 screenPosition = Camera.WorldToScreenPoint(targetPosition);
            Vector3 position = _area.InverseTransformPoint(screenPosition);

            Vector3 randomOffset = new Vector3(
                Random.Range(-_maxRandomOffset.x, _maxRandomOffset.x),
                Random.Range(-_maxRandomOffset.y, _maxRandomOffset.y),
                Random.Range(-_maxRandomOffset.z, _maxRandomOffset.z)
            );

            position += randomOffset;

            return position;
        }
    }
}