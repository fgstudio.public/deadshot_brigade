﻿using System;
using DG.Tweening;
using Frostgate.RiftHunters.Core.UI;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu(ComponentMenus.RiftHunters.Menu + "/" + nameof(UISmoothSpriteActivator))]
    public sealed class UISmoothSpriteActivator : MonoBehaviour, ISmoothActivator
    {
        private const float ActivatedAlpha = 1;
        private const float DeactivatedAlpha = 0;

        [Header("Transition")]
        [SerializeField, Required, ChildGameObjectsOnly, CanBeNull] private SpriteRenderer _spriteRenderer;
        [SerializeField, Range(0, 1)] private float _transitionDuration;
        [SerializeField] private bool _startFromCurrent = true;

        private float CurrentAlpha => _spriteRenderer != null ? _spriteRenderer.color.a : default;
        public bool IsActive { get; private set; }

        private void OnValidate() =>
            _spriteRenderer ??= GetComponentInChildren<SpriteRenderer>();

        private void Start() =>
            IsActive = gameObject.activeSelf;

        public bool TryActivate()
        {
            if (!IsActive) Activate();
            return !IsActive;
        }

        public void SetActive(bool isActive, Action onStart = null, Action onComplete = null)
        {
            if (isActive) Activate(onStart, onComplete);
            else Deactivate(onStart, onComplete);
        }

        public void Activate(Action onStart = null, Action onComplete = null) =>
            PlayTransition(
                startAlpha: _startFromCurrent ? CurrentAlpha : DeactivatedAlpha,
                targetAlpha: ActivatedAlpha,
                duration: _transitionDuration,
                onStart: () =>
                {
                    IsActive = true;
                    gameObject.SetActive(true);
                    onStart?.Invoke();
                },
                onComplete);

        public void Deactivate(Action onStart = null, Action onComplete = null) =>
            PlayTransition(
                startAlpha: _startFromCurrent ? CurrentAlpha : ActivatedAlpha,
                targetAlpha: DeactivatedAlpha,
                duration: _transitionDuration,
                onStart: () =>
                {
                    IsActive = false;
                    onStart?.Invoke();
                },
                onComplete: () =>
                {
                    onComplete?.Invoke();
                    gameObject.SetActive(false);
                });

        private void PlayTransition(float startAlpha, float targetAlpha, float duration, Action onStart = null, Action onComplete = null)
        {
            if (_spriteRenderer == null)
                return;

            onStart?.Invoke();

            _spriteRenderer.DOKill();
            _spriteRenderer.color.SetAlpha(startAlpha);

            bool isCompleted = Mathf.Abs(startAlpha - targetAlpha) < Mathf.Epsilon;

            if (isCompleted)
                onComplete?.Invoke();

            else
                _spriteRenderer.DOFade(targetAlpha, duration)
                    .OnComplete(() => onComplete?.Invoke());
        }
    }
}