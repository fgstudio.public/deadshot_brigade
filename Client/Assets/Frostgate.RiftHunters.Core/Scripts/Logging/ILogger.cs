﻿using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface ILogger
    {
        void Log([NotNull] string message, LogLevel level = LogLevel.Info);
        void LogException([NotNull] Exception exception, LogLevel level = LogLevel.Error);
    }

    public interface ILogger<TSource> : ILogger
    {
    }
}
