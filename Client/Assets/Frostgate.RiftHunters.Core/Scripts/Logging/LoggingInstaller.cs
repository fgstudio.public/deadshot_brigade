﻿using JetBrains.Annotations;
using Zenject;

namespace Frostgate.RiftHunters.Core
{
    [UsedImplicitly]
    public sealed class LoggingInstaller : Installer
    {
        public override void InstallBindings()
        {
            Container.Bind<ILogger>().FromMethod(LoggerFactory.CreateLogger).AsSingle();
            Container.Bind(typeof(ILogger<>)).To(typeof(OpenGenericLoggerFactoryDecorator<>)).AsTransient();
        }
    }
}