﻿namespace Frostgate.RiftHunters.Core.Logging
{
    public static class InternalLoggerFactory
    {
        public static ILogger CreateLogger() => new UnityDebugLogger();
        public static ILogger<TSource> CreateLogger<TSource>() => new GenericLoggerDecorator<TSource>(CreateLogger());
        public static ILogger CreateLogger(string sourceName) => new NamedLoggerDecorator(CreateLogger(), sourceName);
    }
}