using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Logging
{
    public sealed class NamedLoggerDecorator : ILogger
    {
        private const string LogFormat = "{0} | {1} "; // | TSourceName | Message |

        private readonly ILogger _logger;
        private readonly string _sourceName;

        public NamedLoggerDecorator([NotNull] ILogger logger, [NotNull] string sourceName)
        {
            _logger = logger;
            _sourceName = sourceName;
        }

        public void Log(string message, LogLevel level = LogLevel.Info)
        {
            string formattedMessage = FormatMessage(message);
            _logger.Log(formattedMessage, level);
        }

        public void LogException(Exception exception, LogLevel level = LogLevel.Error) => Log(exception.Message, level);

        private string FormatMessage(string message) => String.Format(LogFormat, _sourceName, message);
    }
}
