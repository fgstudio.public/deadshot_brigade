﻿using System;

namespace Frostgate.RiftHunters.Core.Logging
{
    public static class LoggingHelper
    {
        public static Action<string> GetUnityLogger(LogLevel level)
        {
            return level switch
            {
                LogLevel.Info => UnityEngine.Debug.Log,
                LogLevel.Warning => UnityEngine.Debug.LogWarning,
                LogLevel.Error => UnityEngine.Debug.LogError,
                LogLevel.Critical => UnityEngine.Debug.LogError,
                _ => throw new NotImplementedException($"LogLevel '{level}' logging not implemented.")
            };
        }
    }
}