﻿using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Logging
{
    [UsedImplicitly]
    public class UnityDebugLogger : ILogger
    {
        private const string LogFormat = "| {0} | {1} |"; // | Level | Message |

        public virtual void Log(string message, LogLevel level)
        {
            string formattedMessage = FormatMessage(message, level);
            LoggingHelper.GetUnityLogger(level).Invoke(formattedMessage);
        }

        public void LogException(Exception exception, LogLevel level) => Log(exception.Message, level);

        protected virtual string FormatMessage(string message, LogLevel level) =>
            String.Format(LogFormat, level, message);
    }
}
