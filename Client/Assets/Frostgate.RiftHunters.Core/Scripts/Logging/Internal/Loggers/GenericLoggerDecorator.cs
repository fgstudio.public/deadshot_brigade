﻿using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Logging
{
    [UsedImplicitly]
    public sealed class GenericLoggerDecorator<TSource> : ILogger<TSource>
    {
        private readonly ILogger _logger;

        public GenericLoggerDecorator([NotNull] ILogger logger) =>
            _logger = new NamedLoggerDecorator(logger, typeof(TSource).Name);
        
        public void LogException(Exception exception, LogLevel level = LogLevel.Error) => Log(exception.Message, level);

        public void Log(string message, LogLevel level = LogLevel.Info) => _logger.Log(message, level);
    }
}
