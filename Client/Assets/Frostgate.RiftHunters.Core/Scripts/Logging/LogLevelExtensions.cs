﻿using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public static class LogLevelExtensions
    {
        public static LogLevel ToCoreLogLevel(this LogType type)
        {
            return type switch
            {
                LogType.Assert => LogLevel.Info,
                LogType.Log => LogLevel.Info,
                LogType.Warning => LogLevel.Warning,
                LogType.Error => LogLevel.Error,
                LogType.Exception => LogLevel.Error,
                _ => throw new ArgumentOutOfRangeException(
                    $"{nameof(LogType)} '{type}' mapping to '{nameof(LogLevel)}' not implemented.")
            };
        }
    }
}