﻿using System;

namespace Frostgate.RiftHunters.Core
{
    public sealed class OpenGenericLoggerFactoryDecorator<TSource> : ILogger<TSource>
    {
        private readonly ILogger<TSource> _logger = LoggerFactory.CreateLogger<TSource>();

        public void Log(string message, LogLevel level = LogLevel.Info) => _logger.Log(message, level);

        public void LogException(Exception exception, LogLevel level = LogLevel.Error) =>
            _logger.LogException(exception, level);
    }
}