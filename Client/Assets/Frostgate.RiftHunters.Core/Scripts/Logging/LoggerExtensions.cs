﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public static class LoggerExtensions
    {
        public static void LogInfo([NotNull] this ILogger logger, string message) => 
            logger.Log(message, LogLevel.Info);

        public static void LogWarning([NotNull] this ILogger logger, string message) =>
            logger.Log(message, LogLevel.Warning);

        public static void LogError([NotNull] this ILogger logger, string message) =>
            logger.Log(message, LogLevel.Error);

        public static void LogCritical([NotNull] this ILogger logger, string message) =>
            logger.Log(message, LogLevel.Critical);
    }
}