﻿namespace Frostgate.RiftHunters.Core
{
    public enum LogLevel : byte
    {
        Info,
        Warning,
        Error,
        Critical
    }
}
