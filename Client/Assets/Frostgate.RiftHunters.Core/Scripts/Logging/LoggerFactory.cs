﻿using Frostgate.RiftHunters.Core.Logging;

namespace Frostgate.RiftHunters.Core
{
    public static class LoggerFactory
    {
        public static ILogger CreateLogger() => InternalLoggerFactory.CreateLogger();
        public static ILogger<TSource> CreateLogger<TSource>() => InternalLoggerFactory.CreateLogger<TSource>();
        public static ILogger CreateLogger(string sourceName) => InternalLoggerFactory.CreateLogger(sourceName);
    }
}