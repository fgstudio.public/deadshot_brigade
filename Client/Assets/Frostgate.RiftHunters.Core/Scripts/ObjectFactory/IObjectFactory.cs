﻿using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core
{
    public interface IObjectFactory
    {
        T Instantiate<T>();
        T Instantiate<T>(IEnumerable<object> ctorArgs);
    }
}
