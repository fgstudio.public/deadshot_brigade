﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public interface IPrefabFactory
    {
        GameObject Instantiate(Object prefab);
        GameObject Instantiate(Object prefab, Transform parent);
        GameObject Instantiate(Object prefab, Vector3 position, Quaternion rotation, Transform parent = null);

        T Instantiate<T>(Object prefab);
        T Instantiate<T>(Object prefab, Transform parent);

        T Instantiate<T>(Object prefab, Vector3 position, Quaternion rotation, Transform parent = null);
    }
}
