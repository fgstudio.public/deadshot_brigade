﻿using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public interface IComponentFactory
    {
        T Instantiate<T>(GameObject target) where T : Component;
        T Instantiate<T>(GameObject target, IEnumerable<object> ctorArgs) where T : Component;
    }
}
