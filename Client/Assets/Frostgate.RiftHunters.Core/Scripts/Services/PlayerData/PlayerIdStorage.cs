﻿namespace Frostgate.RiftHunters.Core.Services.PlayerData
{
    public sealed class PlayerIdStorage : IPlayerIdStorageInternal
    {
        private string _playerId;

        public string PlayerId => _playerId;

        public void SetPlayerId(string playerId)
        {
            _playerId = playerId;
        }
    }
}