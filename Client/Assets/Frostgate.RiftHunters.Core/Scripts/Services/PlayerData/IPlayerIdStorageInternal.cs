﻿namespace Frostgate.RiftHunters.Core.Services.PlayerData
{
    public interface IPlayerIdStorageInternal : IPlayerIdStorage
    {
        void SetPlayerId(string playerId);
    }
}