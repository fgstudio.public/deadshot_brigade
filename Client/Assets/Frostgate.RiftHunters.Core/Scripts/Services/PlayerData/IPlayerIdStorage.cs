﻿namespace Frostgate.RiftHunters.Core.Services.PlayerData
{
    public interface IPlayerIdStorage
    {
        string PlayerId { get; }
    }
}