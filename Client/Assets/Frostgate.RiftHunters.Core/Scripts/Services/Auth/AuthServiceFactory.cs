﻿using System;
using Frostgate.RiftHunters.Infrastructure.Services.Auth;
using JetBrains.Annotations;
using Zenject;

namespace Frostgate.RiftHunters.Core.Services.Auth
{
    public class AuthServiceFactory : IAuthServiceFactory
    {
        [NotNull] private readonly DiContainer _diContainer;

        public AuthServiceFactory([NotNull] DiContainer diContainer)
        {
            _diContainer = diContainer;
        }

        public IAuthService Create(AuthType authType) =>
            authType switch
            {
                AuthType.Anonymous => _diContainer.Instantiate<AnonymousAuthService>(),
                AuthType.GooglePlay => _diContainer.Instantiate<GooglePlayAuthService>(),
                _ => throw new ArgumentOutOfRangeException(nameof(authType), authType, null)
            };
    }
}