﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.Services.Auth
{
    public interface IAuthService
    {
        UniTask<AuthResult> SignInAsync();

        void ClearSessionToken();
    }
}