﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Services.Auth;
using Frostgate.RiftHunters.Core.Services.PlayerData;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Scripts.Services.Auth
{
    public class AuthServiceFacade : IAuthServiceFacade
    {
        [NotNull] private readonly IAuthServiceProvider _authServiceProvider;
        [NotNull] private readonly IPlayerIdStorageInternal _playerIdStorageInternal;
        [NotNull] private readonly ILogger<AuthServiceFacade> _logger;

        public AuthServiceFacade(
            [NotNull] IAuthServiceProvider authServiceProvider,
            [NotNull] IPlayerIdStorageInternal playerIdStorageInternal,
            [NotNull] ILogger<AuthServiceFacade> logger)
        {
            _authServiceProvider = authServiceProvider;
            _playerIdStorageInternal = playerIdStorageInternal;
            _logger = logger;
        }

        public async UniTask<AuthResult> SignInAsync(AuthType authType)
        {
            _logger.Log($"Sign in: {authType}");

            var authService = _authServiceProvider.Provide(authType);
            var authResult = await authService.SignInAsync();

            if (authResult.IsSuccess)
            {
                _playerIdStorageInternal.SetPlayerId(authResult.PlayerId);

                _logger.Log($"Sign in completed.");
                _logger.Log($"Player id - {authResult.PlayerId}.");
            }
            else
            {
                _logger.Log($"Sign in failed. Error message: {authResult.ErrorMessage}");
            }

            return authResult;
        }
    }
}