﻿namespace Frostgate.RiftHunters.Core.Services.Auth
{
    public interface IAuthServiceProvider
    {
        IAuthService Provide(AuthType authType);
    }
}