﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.Services.Auth
{
    public interface IAuthServiceFacade
    {
        UniTask<AuthResult> SignInAsync(AuthType authType);
    }
}