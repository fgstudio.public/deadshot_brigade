﻿namespace Frostgate.RiftHunters.Core.Services.Auth
{
    public interface IAuthServiceFactory
    {
        IAuthService Create(AuthType authType);
    }
}