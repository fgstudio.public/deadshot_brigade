﻿namespace Frostgate.RiftHunters.Core.Services.Auth
{
    public readonly struct AuthResult
    {
        public bool IsSuccess { get; }
        public string PlayerId { get; }
        public string ErrorMessage { get; }

        private AuthResult (bool isSuccess, string playerId, string errorMessage)
        {
            IsSuccess = isSuccess;
            PlayerId = playerId;
            ErrorMessage = errorMessage;
        }

        public static AuthResult FromResult(string playerId) => new (true, playerId, default);
        public static AuthResult FromError(string error) => new (false, default, error);
    }
}