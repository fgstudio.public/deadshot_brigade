﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Services.Auth
{
    public class AuthServiceProvider : IAuthServiceProvider
    {
        [NotNull] private readonly IAuthServiceFactory _authServiceFactory;

        private readonly IReadOnlyDictionary<AuthType, IAuthService> _servicesMap;

        public AuthServiceProvider([NotNull] IAuthServiceFactory authServiceFactory)
        {
            _authServiceFactory = authServiceFactory;

            _servicesMap = new Dictionary<AuthType, IAuthService>
            {
                { AuthType.Anonymous, CreateService(AuthType.Anonymous) },
                { AuthType.GooglePlay, CreateService(AuthType.GooglePlay) },
            };
        }

        public IAuthService Provide(AuthType authType) => _servicesMap[authType];

        private IAuthService CreateService(AuthType authType) =>
            _authServiceFactory.Create(authType);
    }
}