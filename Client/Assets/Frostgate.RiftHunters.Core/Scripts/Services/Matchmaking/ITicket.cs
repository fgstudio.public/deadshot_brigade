﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface ITicket
    {
        [NotNull] string Id { get; }
        [NotNull] ITicketOptions Options { get; }
    }
}