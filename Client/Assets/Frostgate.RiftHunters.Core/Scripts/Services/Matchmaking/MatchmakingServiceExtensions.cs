﻿using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public static class MatchmakingServiceExtensions
    {
        public static async UniTask<ITicketData> GetTickedDataWithResultingStatusAsync(
            [NotNull] this IMatchmakingService service, [NotNull] ITicket ticket,
            int pollIntervalMilliseconds = 1000)
        {
            ITicketData data = null;
            while (data == null || !HasResultingStatus(data))
            {
                data = await service.GetTicketDataAsync(ticket);
                await UniTask.Delay(pollIntervalMilliseconds, DelayType.Realtime);
            }

            return data;
        }

        private static bool HasResultingStatus(ITicketData data)
        {
            TicketStatus status = data.Status;
            return status is TicketStatus.Failed or TicketStatus.Timeout or TicketStatus.Found;
        }
    }
}