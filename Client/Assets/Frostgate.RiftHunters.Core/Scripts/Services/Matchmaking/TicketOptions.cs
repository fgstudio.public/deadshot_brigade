﻿using Frostgate.RiftHunters.Core.Battle;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface ITicketOptions
    {
        [NotNull] IMissionConfig SelectedMission { get; }
        [NotNull] UnitConfig SelectedUnit { get; }
    }

    public sealed class TicketOptions : ITicketOptions
    {
        public IMissionConfig SelectedMission { get; }
        public UnitConfig SelectedUnit { get; }

        public TicketOptions([NotNull] IMissionConfig selectedMission, [NotNull] UnitConfig selectedUnit)
        {
            SelectedMission = selectedMission;
            SelectedUnit = selectedUnit;
        }
    }
}