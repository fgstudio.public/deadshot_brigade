﻿using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface IMatchmakingService
    {
        /// <summary>
        /// Создание тикета и регистрация игрока в очереди на подбор игроков.
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        UniTask<ITicket> CreateTicketAsync([NotNull] ITicketOptions options);

        /// <summary>
        /// Опросить сервер о статусе и данных, присвоенных тикету.
        /// В случае имплементации от Unity.Matchmaking не опрашивать чаще, чем 1 раз секунду.
        /// </summary>
        /// <param name="ticket"></param>
        /// <returns></returns>
        UniTask<ITicketData> GetTicketDataAsync([NotNull] ITicket ticket);

        /// <summary>
        /// Отменить регистрацию игрока в очереди на подбор игроков.
        /// </summary>
        /// <param name="ticket"></param>
        /// <returns></returns>
        UniTask DeleteTicketAsync([NotNull] ITicket ticket);
    }
}