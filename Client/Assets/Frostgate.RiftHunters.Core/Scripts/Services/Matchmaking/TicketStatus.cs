﻿namespace Frostgate.RiftHunters.Core
{
    public enum TicketStatus : byte
    {
        Failed = 0,
        Timeout = 1,
        InProgress = 2,
        Found = 3,
    }
}