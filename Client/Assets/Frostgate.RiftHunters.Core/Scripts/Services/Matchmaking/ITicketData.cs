﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface ITicketData
    {
        [NotNull] ITicket Ticket { get; }

        TicketStatus Status { get; }
        [NotNull] MatchAssignment Assignment { get; }
    }
}