﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class MatchAssignment
    {
        [NotNull] public string MatchId { get; }
        [NotNull] public string ServerIp { get; }
        [CanBeNull] public int? ServerPort { get; }

        public MatchAssignment([NotNull] string matchId, [NotNull] string serverIp, int? serverPort = null)
        {
            MatchId = matchId;
            ServerIp = serverIp;
            ServerPort = serverPort;
        }
    }
}