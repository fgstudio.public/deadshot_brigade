﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core
{
    public interface ISaveService
    {
        UniTask InitializeAsync();
        UniTask SaveData<T>(T data);
        UniTask<T> GetData<T>();
    }
}