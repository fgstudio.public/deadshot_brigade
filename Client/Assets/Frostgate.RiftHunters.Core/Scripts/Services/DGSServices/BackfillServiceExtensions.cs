﻿using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public static class BackfillServiceExtensions
    {
        public static async UniTask StartBackfillingAsync([NotNull] this IServerBackfillService service,
            int backfillMilliseconds)
        {
            await service.StartBackfillingAsync();
            await UniTask.Delay(backfillMilliseconds, DelayType.Realtime).SuppressCancellationThrow();
            await service.StopBackfillingAsync();
        }
    }
}