﻿namespace Frostgate.RiftHunters.Core
{
    public interface IServerConfigurationProvider
    {
        int Port { get; }
        int QueryPort { get; }
    }
}