﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core
{
    /// <summary>
    /// Сервис для обновления состояния игрового сервера на матчмейкере.
    /// </summary>
    public interface IServerBackfillService
    {
        /// <summary>
        /// Максимальное количество игроков разрешённое для подключений.
        /// </summary>
        public int MaxPlayersNumber { set; }

        /// <summary>
        /// Запустить цикл отправки состояний игрового сервера.
        /// </summary>
        /// <returns></returns>
        UniTask StartBackfillingAsync();
        
        /// <summary>
        /// Остановить цикл отправки состояний игрового сервера.
        /// </summary>
        /// <returns></returns>
        UniTask StopBackfillingAsync();

        void AddPlayerToMatch(string playerId);
        void RemovePlayerFromMatch(string playerId);
    }
}