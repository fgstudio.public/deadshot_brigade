﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    /// <summary>
    /// Сервис для отправки аналитики(SQP).
    /// </summary>
    public interface IServerDataQueryService
    {
        [CanBeNull] IMissionConfig CurrentMission { set; }
        int CurrentPlayersNumber { set; }
        
        UniTask StartQueryingAsync();
        void StopQuerying();
    }
}