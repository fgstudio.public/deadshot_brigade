﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core
{
    /// <summary>
    /// Сервис для получения данных аллокации игрового сервера, запущенного матчмейкером.
    /// </summary>
    public interface IServerAllocationService
    {
        UniTask<IAllocationData> AwaitAllocationAsync();
    }
}