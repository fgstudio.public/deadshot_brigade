﻿using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface IAllocationData
    {
        [NotNull] IMissionConfig Mission { get; }
        [NotNull] IReadOnlyList<MatchPlayer> Players { get; }
    }
}