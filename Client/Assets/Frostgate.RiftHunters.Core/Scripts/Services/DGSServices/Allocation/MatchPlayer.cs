﻿using Frostgate.RiftHunters.Core.Battle;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class MatchPlayer
    {
        [NotNull] public string Id { get; }
        [NotNull] public UnitConfig SelectedUnit { get; }

        public MatchPlayer([NotNull] string id, [NotNull] UnitConfig selectedUnit)
        {
            Id = id;
            SelectedUnit = selectedUnit;
        }
    }
}