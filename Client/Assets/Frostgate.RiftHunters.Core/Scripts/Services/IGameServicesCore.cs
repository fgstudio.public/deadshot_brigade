﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core
{
    public interface IGameServicesCore
    {
        UniTask InitializeAsync();
    }
}