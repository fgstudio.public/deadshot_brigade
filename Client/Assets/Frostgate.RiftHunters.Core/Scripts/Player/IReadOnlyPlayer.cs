﻿namespace Frostgate.RiftHunters.Core
{
    public interface IReadOnlyPlayer
    {
        string Id { get; }
        string Name { get; }
    }
}
