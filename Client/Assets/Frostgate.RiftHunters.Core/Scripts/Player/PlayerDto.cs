﻿namespace Frostgate.RiftHunters.Core
{
    public readonly struct PlayerDto : IReadOnlyPlayer
    {
        public readonly string Id;
        public readonly string Name;

        string IReadOnlyPlayer.Id => Id;
        string IReadOnlyPlayer.Name => Name;

        public PlayerDto(string id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}