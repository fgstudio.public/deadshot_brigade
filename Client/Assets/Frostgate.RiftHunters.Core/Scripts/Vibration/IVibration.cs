﻿namespace Frostgate.RiftHunters.Core
{
    public interface IVibration
    {
        void Stop();
    }
}