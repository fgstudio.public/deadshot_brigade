﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface IVibrator
    {
        [NotNull]
        IVibration Vibrate(int durationMs);

        [NotNull]
        IVibration Vibrate(int durationMs, byte intensity);

        [NotNull]
        IVibration Vibrate([NotNull] IVibrationPattern pattern);

        void Cancel();
    }
}