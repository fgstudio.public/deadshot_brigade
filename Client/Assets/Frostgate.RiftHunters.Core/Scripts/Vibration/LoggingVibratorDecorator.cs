﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class LoggingVibratorDecorator : IVibrator
    {
        [NotNull] private readonly IVibrator _vibrator;
        [NotNull] private readonly ILogger<IVibrator> _logger;
        private readonly string _vibratorImplementationName;

        public LoggingVibratorDecorator([NotNull] IVibrator vibrator, [NotNull] ILogger<IVibrator> logger)
        {
            _vibrator = vibrator;
            _logger = logger;

            _vibratorImplementationName = _vibrator.GetType().Name;
        }

        public IVibration Vibrate(int durationMs)
        {
            LogVibrated($"With duration '{durationMs}' ms.");
            return _vibrator.Vibrate(durationMs);
        }

        public IVibration Vibrate(int durationMs, byte intensity)
        {
            LogVibrated($"With duration '{durationMs}' ms and intensity '{intensity}'.");
            return _vibrator.Vibrate(durationMs, intensity);
        }

        public IVibration Vibrate(IVibrationPattern pattern)
        {
            LogVibrated("With custom pattern");
            return _vibrator.Vibrate(pattern);
        }

        public void Cancel()
        {
            _logger.LogInfo("Vibration canceled.");
            _vibrator.Cancel();
        }

        private void LogVibrated(string message) =>
            _logger.LogInfo($"'{_vibratorImplementationName}' vibrated. {message}.");
    }
}