﻿using System;

namespace Frostgate.RiftHunters.Core
{
    public static class VibrationConstants
    {
        public const int DefaultDurationMs = 400;

        public const byte DefaultIntensity = Byte.MaxValue;
        public const byte MinIntensity = 1;
    }
}