﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class VibrationPattern : IVibrationPattern
    {
        [NotNull] public List<VibrationInterval> Intervals { get; }

        IReadOnlyList<VibrationInterval> IVibrationPattern.Intervals => Intervals;

        public VibrationPattern([NotNull] List<VibrationInterval> intervals)
        {
            Intervals = intervals;
        }

        public VibrationPattern([NotNull] int[] durationMs, [NotNull] byte[] intensity)
        {
            if (durationMs.Length != intensity.Length)
                throw new ArgumentOutOfRangeException(
                    $"'{nameof(durationMs)}' and '{intensity}' must be the same length.");

            Intervals = CreateIntervals(durationMs, intensity);
        }

        public VibrationPattern() : this(new List<VibrationInterval>())
        {
        }

        private List<VibrationInterval> CreateIntervals(int[] durationMs, byte[] intensity)
        {
            var intervals = new List<VibrationInterval>(durationMs.Length);
            for (var i = 0; i < durationMs.Length; i++)
                intervals[i] = new VibrationInterval(durationMs[i], intensity[i]);

            return intervals;
        }
    }
}