﻿namespace Frostgate.RiftHunters.Core
{
    public interface IVibratorIntensityDivider
    {
        byte Divide(byte intensity);
    }
}