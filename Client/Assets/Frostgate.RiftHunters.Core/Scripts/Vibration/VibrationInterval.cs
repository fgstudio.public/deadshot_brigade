﻿using System;
using System.Diagnostics;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    [DebuggerDisplay(nameof(VibrationInterval) + " " + "Duration = {DurationMs}, Intensity = {Intensity}")]
    [Serializable]
    public struct VibrationInterval
    {
        [field: SerializeField, Min(0)] public int DurationMs { get; set; }

        /// <summary>
        /// от 1 до 255
        /// </summary>
        [field: SerializeField, Range(1, 255)]
        public byte Intensity { get; set; }

        public VibrationInterval(int durationMs = VibrationConstants.DefaultDurationMs,
            byte intensity = VibrationConstants.DefaultIntensity)
        {
            DurationMs = durationMs;
            Intensity = intensity;
        }
    }
}