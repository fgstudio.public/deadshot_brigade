﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface IVibrationPattern
    {
        [NotNull] IReadOnlyList<VibrationInterval> Intervals { get; }
    }
}