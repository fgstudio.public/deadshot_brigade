﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class IntensityDividingVibratorDecorator : IVibrator
    {
        [NotNull] private readonly IVibrator _vibrator;
        [NotNull] private readonly IVibratorIntensityDivider _intensityDivider;

        public IntensityDividingVibratorDecorator([NotNull] IVibrator vibrator,
            [NotNull] IVibratorIntensityDivider intensityDivider)
        {
            _vibrator = vibrator;
            _intensityDivider = intensityDivider;
        }

        public IVibration Vibrate(int durationMs) => _vibrator.Vibrate(durationMs);

        public IVibration Vibrate(int durationMs, byte intensity) =>
            _vibrator.Vibrate(durationMs, _intensityDivider.Divide(intensity));

        public IVibration Vibrate(IVibrationPattern pattern)
        {
            var dividedPattern = new IntensityDividingVibrationPatternDecorator(pattern, _intensityDivider);
            return _vibrator.Vibrate(dividedPattern);
        }

        public void Cancel() => _vibrator.Cancel();

        private sealed class IntensityDividingVibrationPatternDecorator : IVibrationPattern
        {
            IReadOnlyList<VibrationInterval> IVibrationPattern.Intervals => Array.Empty<VibrationInterval>();

            private readonly List<VibrationInterval> _intervals;

            public IntensityDividingVibrationPatternDecorator([NotNull] IVibrationPattern pattern,
                [NotNull] IVibratorIntensityDivider intensityDivider)
            {
                _intervals = DivideIntervalsIntensity(pattern.Intervals, intensityDivider);
            }

            private List<VibrationInterval> DivideIntervalsIntensity(IReadOnlyList<VibrationInterval> patternIntervals,
                IVibratorIntensityDivider intensityDivider)
            {
                var intervals = new List<VibrationInterval>(patternIntervals.Count);
                for (var i = 0; i < patternIntervals.Count; i++)
                {
                    int durationMs = patternIntervals[i].DurationMs;
                    byte intensity = intensityDivider.Divide(patternIntervals[i].Intensity);

                    intervals[i] = new VibrationInterval(durationMs, intensity);
                }

                return intervals;
            }
        }
    }
}