using System;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Battle.Client.Objects.StreakBonus
{
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [CreateAssetMenu(fileName = nameof(StreakBonusViewSettings),
        menuName = StreakBonusAssetMenu.Client + "/" + nameof(StreakBonusViewSettings))]
    public sealed class StreakBonusViewSettings : ScriptableObject
    {
        [Title("Drop Settings")]
        [MinMaxSlider(0, 5), SuffixLabel("units")]
        [SerializeField] private Vector2 _dropHeight;
        [SerializeField] private Ease _dropEase = Ease.Linear;

        [Title("Collect Settings")]
        [MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _collectDuration;

        [Title("Blink Settings")]
        [MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _blinkTimeBeforeExpired;

        public MinMaxFloat DropHeight => new(_dropHeight.x, _dropHeight.y);
        public Ease DropEase => _dropEase;

        public TimeSpan CollectDuration => TimeSpan.FromSeconds(_collectDuration);

        public TimeSpan BlinkTimeBeforeExpired => TimeSpan.FromSeconds(_blinkTimeBeforeExpired);
    }
}
