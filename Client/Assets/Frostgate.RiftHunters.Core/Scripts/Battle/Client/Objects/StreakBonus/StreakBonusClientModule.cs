using System;
using Cysharp.Threading.Tasks;
using Mirror;
using DG.Tweening;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Components;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;
using Zenject;

namespace Frostgate.RiftHunters.Core.Battle.Client.Objects.StreakBonus
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [AddComponentMenu(StreakBonusComponentMenu.Client + "/" + nameof(StreakBonusClientModule))]
    public sealed class StreakBonusClientModule : MonoBehaviour
    {
        [Inject] private readonly ILogger<StreakBonusClientModule> _logger;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private StreakBonusAnimator _animator;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private BlinkComponent _blinkComponent;

        [CanBeNull] private IReadOnlyStreakBonusState _state;
        [CanBeNull] private StreakBonusViewSettings _viewSettings;
        [CanBeNull] private StreakBonusMechanicSettings _mechanicSettings;

        public void Init(
            [NotNull] IReadOnlyStreakBonusState state,
            [NotNull] StreakBonusViewSettings viewSettings,
            [NotNull] StreakBonusMechanicSettings mechanicSettings)
        {
            _state = state;
            _viewSettings = viewSettings;
            _mechanicSettings = mechanicSettings;

            _animator.Init(viewSettings);

            TimeSpan delay = _mechanicSettings!.LifeTime - _viewSettings!.BlinkTimeBeforeExpired;
            _blinkComponent.Disable();
            _blinkComponent.EnableWithDelay(delay)
                .ContinueWith(() => _logger.Log($"Start Blinking [netId:{_state.CoreObject.Identity.netId}]"))
                .SuppressCancellationThrow();

            state.StatusChanged.AddListener(OnStatusChanged);
            OnStatusChanged(state);
        }

        private void OnDestroy()
        {
            if (_state != null)
                _state.StatusChanged.RemoveListener(OnStatusChanged);
        }

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _animator ??= GetComponentInChildren<StreakBonusAnimator>();
            _blinkComponent ??= GetComponentInChildren<BlinkComponent>();
        }

        private void OnStatusChanged(IReadOnlyStreakBonusState state)
        {
            if (state.Status == StreakBonusStatus.Active) Drop();
            else if (state.Status == StreakBonusStatus.Collected) Collect();
        }

        private void Drop()
        {
            if (!TryGetTransform(_state!.SourceId, out Transform source))
                source = transform;

            Vector3 sourcePos = source.position;
            TimeSpan dropDuration = _mechanicSettings!.DropDuration;
            _animator.PlayDrop(sourcePos, dropDuration);

            uint netId = _state.CoreObject.Identity.netId;
            _logger.Log($"Drop [netId:{netId}] from [{sourcePos}] for [{dropDuration.TotalSeconds}] sec");
        }

        private void Collect()
        {
            if (!TryGetTransform(_state!.CollectorId, out Transform vfxParent))
                vfxParent = transform;

            transform.SetParent(null);

            _blinkComponent.Disable();

            _animator.PlayCollect(vfxParent.transform)
                .OnComplete(() => Destroy(gameObject));

            uint netId = _state.CoreObject.Identity.netId;
            _logger.Log($"Collect [netId:{netId}]");
        }

        private bool TryGetTransform(uint netId, out Transform transform)
        {
            bool success = NetworkClient.spawned.TryGetValue(netId, out NetworkIdentity identity);
            transform = success ? identity.transform : null;
            return success;
        }
    }
}
