using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Battle.Client.Objects.StreakBonus
{
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [CreateAssetMenu(fileName = nameof(ClientModulePrefabRepository),
        menuName = StreakBonusAssetMenu.Client + "/" + nameof(ClientModulePrefabRepository))]
    public sealed class ClientModulePrefabRepository
        : ScriptableConfigRepository<StreakBonusConfig, StreakBonusClientModule>
    { }
}
