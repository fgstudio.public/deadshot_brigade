using System;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;
using Random = UnityEngine.Random;

namespace Frostgate.RiftHunters.Core.Battle.Client.Objects.StreakBonus
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [AddComponentMenu(StreakBonusComponentMenu.Client + "/" + nameof(StreakBonusAnimator))]
    public sealed class StreakBonusAnimator : MonoBehaviour
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private Transform _model;

        [InfoBox("TODO: Эффекты одноразовые и хранятся внутри объекта. Для пула это не подойдёт", InfoMessageType.Warning)]
        [ChildGameObjectsOnly, CanBeNull, BoxGroup("VFX"), LabelText("Idle")]
        [SerializeField] private GameObject _idleVfx;

        [ChildGameObjectsOnly, CanBeNull, BoxGroup("VFX"), LabelText("Collect")]
        [SerializeField] private GameObject _collectVfx;

        [ChildGameObjectsOnly, CanBeNull, BoxGroup("VFX"), LabelText("Collector")]
        [SerializeField] private GameObject _collectorVfx;

        [CanBeNull] private StreakBonusViewSettings _settings;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _model ??= GetComponentInChildren<Transform>();
            if (_idleVfx != null) _idleVfx.SetActive(false);
            if (_collectVfx != null) _collectVfx.SetActive(false);
            if (_collectorVfx != null) _collectorVfx.SetActive(false);
        }

        public void Init([NotNull] StreakBonusViewSettings settings) =>
            _settings = settings;

        public Tween PlayDrop(Vector3 fromWorldPos, TimeSpan duration)
        {
            if (_settings == null)
                throw new NullReferenceException(nameof(_settings));

            const int jumpCount = 1;

            Vector3 toLocalPos = _model.localPosition;
            Vector3 fromLocalPos = _model.InverseTransformPoint(fromWorldPos);

            float seconds = (float)duration.TotalSeconds;
            float height = Random.Range(_settings.DropHeight.Min, _settings.DropHeight.Max);

            _model.localPosition = fromLocalPos;

            return _model.DOLocalJump(toLocalPos, height, jumpCount, seconds)
                .SetEase(_settings.DropEase)
                .OnComplete(() =>
                {
                    if (_idleVfx != null)
                        _idleVfx.SetActive(true);
                });
        }

        public Tween PlayCollect([CanBeNull] Transform collector)
        {
            if (_settings == null)
                throw new NullReferenceException(nameof(_settings));

            if (_collectorVfx != null)
            {
                _collectorVfx.transform.SetParent(collector, false);
                _collectorVfx.SetActive(true);
            }

            if (_collectVfx != null)
            {
                _collectVfx.transform.SetParent(null);
                _collectVfx.SetActive(true);
            }

            if (_idleVfx != null)
            {
                _idleVfx.SetActive(false);
            }

            float duration = (float)_settings.CollectDuration.TotalSeconds;
            return transform.DOScale(Vector3.zero, duration).SetEase(Ease.InOutBack);
        }
    }
}