using Frostgate.RiftHunters.Core.Battle.Client.Weapon;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    [CreateAssetMenu(fileName = nameof(UnitViewPrefabRoster),
        menuName = BattleAssetMenus.Client.Menu + "/" + nameof(UnitViewPrefabRoster))]
    public class UnitViewPrefabRoster : ScriptableObject
    {
        [field: SerializeField] public WeaponSight WeaponSight { get; private set; }
        [field: SerializeField] public float RotateLerpSpeed { get; private set; } = 5;
        [field: SerializeField] public float PredictYRotation { get; private set; } = 5;

        public AbilityPredictedZoneView AbilityPredictedZoneView => _abilityPredictedZoneView;

        [SerializeField] private AbilityPredictedZoneView _abilityPredictedZoneView;
    }
}