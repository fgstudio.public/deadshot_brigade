﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    public interface IClientActivityHandlerFactory
    {
        IClientActivityHandler Create([NotNull] ActivityTaskSceneData taskSceneData);
    }
}