﻿using DG.Tweening;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.UI;
using Sirenix.OdinInspector;
using UnityEngine;
using TMPro;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(UIActivityGoal))]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    public sealed class UIActivityGoal : MonoBehaviour, IUIActivityGoal
    {
        [Title("References")] [SerializeField, ChildGameObjectsOnly, Required]
        private TMP_Text _titleText;
        [SerializeField, ChildGameObjectsOnly, Required]
        private GameObject _glowingPoint;
        [SerializeField, ChildGameObjectsOnly, Required]
        private Transform _visualRoot;
        [SerializeField, ChildGameObjectsOnly, Required]
        private CanvasGroup _popupCanvasGroup;
        [SerializeField, ChildGameObjectsOnly, Required]
        private TMP_Text _popupTitleText;

        [SerializeField, ChildGameObjectsOnly, Required]
        private UIActivityGoalMarker _marker;

        [SerializeField, ChildGameObjectsOnly, Required]
        private UISmoothActivator _smoothActivator;

        [Title("Info")] [ShowInInspector, ReadOnly]
        private ActivityGoal? _goal;

        [Title("Popup settings")]
        [SerializeField] private float _popupScale = 1.4f;
        [SerializeField] private Vector3 _homePosition;
        [SerializeField] private Vector3 _startRatioPosition;
        [SerializeField, Range(0f, 3f)] private float _popupMoveTime;
        [SerializeField, Range(0f, 3f)] private float _popupAppearanceTime;
        [SerializeField] private Color _glowColor;
        [SerializeField, Required] private Material _defaultMaterial;
        [SerializeField, Required] private Material _glowMaterial;

        private Sequence _sequence;

        public RectTransform Transform => (RectTransform)transform;
        public ISmoothActivator SmoothActivator => _smoothActivator;

        [Button]
        private void Reset()
        {
            _titleText = null;
            _marker = null;
        }

        private void Awake() =>
            _marker.gameObject.SetActive(false);

        private void OnEnable() =>
            _popupCanvasGroup.alpha = 0;

        public void SetData(ActivityGoal goal)
        {
            _titleText.text = goal.Title;
            UpdateMarker(goal);
        }

        public void ChangeData(ActivityGoal goal)
        {
            PlayPopUp(goal.Title);
            UpdateMarker(goal);
        }

        private void PlayPopUp(string text)
        {
            _popupTitleText.transform.localScale = new Vector3(_popupScale, _popupScale, _popupScale);
            _popupCanvasGroup.alpha = 1f;

            float delayTime = 0.5f;

            _popupTitleText.text = text;

            //TODO: заменить на закешированную камеру, полученную более корректным способом
            var cam = Camera.allCameras[0];
            var screenCenter = cam.ViewportToScreenPoint(_startRatioPosition);
            _popupCanvasGroup.transform.position = screenCenter;

            if(_sequence != null)
                _sequence.Kill();

            _sequence = DOTween.Sequence();
            _sequence.Append(_popupCanvasGroup.transform.DOScale(_popupScale, _popupAppearanceTime - delayTime));
            float insertingTime = _popupAppearanceTime - delayTime;
            _sequence.Insert(insertingTime, _popupCanvasGroup.transform.DOScale(Vector3.one, delayTime));
            insertingTime = _popupAppearanceTime + delayTime;
            _sequence.Insert(insertingTime, _popupCanvasGroup.transform.DOLocalMove(_homePosition, _popupMoveTime));
            insertingTime += _popupMoveTime;
            _sequence.Insert(insertingTime, _popupTitleText.transform.DOScale(Vector3.one, delayTime));
            _sequence.Insert(insertingTime, _popupCanvasGroup.DOFade(0f, delayTime));
            insertingTime += delayTime;
            _sequence.InsertCallback(insertingTime, () => _titleText.text = text);
            _sequence.InsertCallback(insertingTime, GlowOn);
            _sequence.Insert(insertingTime, _visualRoot.DOScale(_popupScale, delayTime));
            insertingTime += delayTime;
            _sequence.InsertCallback(insertingTime, GlowOff);
            _sequence.Insert(insertingTime, _visualRoot.DOScale(Vector3.one, delayTime));
            _sequence.Play();
        }


        [Button]
        private void TestPopUpSameText() => PlayPopUp(_titleText.text);

        [Button]
        private void TestPopUpNewText() => PlayPopUp("New description");

        private void UpdateMarker(ActivityGoal goal)
        {
            if (!goal.EnableMarker)
            {
                _marker.gameObject.SetActive(false);
            }
            else
            {
                _marker.gameObject.SetActive(true);
                _marker.MarkerImage.sprite = goal.MarkerSprite;
                _marker.MarkerBgImage.color = goal.MarkerBgColor;
            }
        }

        private void GlowOff()
        {
            _titleText.fontMaterial = _defaultMaterial;
            _titleText.color = Color.white;
            _glowingPoint.SetActive(false);
        }

        private void GlowOn()
        {
            _titleText.fontMaterial = _glowMaterial;
            _titleText.color = _glowColor;
            _glowingPoint.SetActive(true);
        }
    }
}