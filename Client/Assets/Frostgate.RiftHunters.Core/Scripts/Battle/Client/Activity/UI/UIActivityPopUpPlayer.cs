﻿using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(UIActivityPopUpPlayer))]
    public sealed class UIActivityPopUpPlayer : MonoBehaviour, IUIActivityPopUpPlayer
    {
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Transform _popUpStartPosition;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Transform _popUpTargetPosition;

        [SerializeField, FoldoutGroup("Pool Settings")]
        private PrefabPoolConfig<UIActivityPopUp, ReferencePoolConfig> _popUpPoolConfig;

        [SerializeField, FoldoutGroup("Pool Settings")]
        private PrefabPoolConfig<UIActivityPopUpGoal, ReferencePoolConfig> _popUpGoalPoolConfig;

        [SerializeField, Range(0, 5f), FoldoutGroup("Play Settings")]
        private float _emergenceTime = 1f;

        [SerializeField, Range(0, 5f), FoldoutGroup("Play Settings")]
        private float _liveTime = 2f;

        [SerializeField, Range(0, 5f), FoldoutGroup("Play Settings")]
        private float _hideTime = 0.8f;


        [CanBeNull] private IPool<UIActivityPopUp> _popUpPoolCache;
        [CanBeNull] private IPool<UIActivityPopUpGoal> _popUpGoalPoolCache;

        [NotNull] private IPool<UIActivityPopUp> PopUpPool => _popUpPoolCache ??= CreatePool(_popUpPoolConfig);
        [NotNull] private IPool<UIActivityPopUpGoal> PopUpGoalPool => _popUpGoalPoolCache ??= CreatePool(_popUpGoalPoolConfig);

        public void Play(ActivityDescription description, Action completedCallback = null)
        {
            UIActivityPopUp popUp = CreatePopUp(description);

            popUp.transform.position = _popUpStartPosition.position;
            DOTween.Sequence()
                .Append(popUp.CanvasGroup.DOFade(1f, _emergenceTime))
                .Insert(_emergenceTime, popUp.GlowImage.DOFade(0f, _liveTime))
                .Insert(_emergenceTime + _liveTime, popUp.transform.DOMove(_popUpTargetPosition.position, _hideTime))
                .Insert(_emergenceTime + _liveTime + _hideTime / 3f, popUp.CanvasGroup.DOFade(0f, _hideTime))
                .Insert(_emergenceTime + _liveTime + _hideTime / 3, popUp.transform.DOScale(0f, _hideTime))
                .InsertCallback(_emergenceTime + _liveTime + _hideTime, () =>
                {
                    ReleasePopUp(popUp);
                    completedCallback?.Invoke();
                }).Play();
        }

        [NotNull]
        private UIActivityPopUp CreatePopUp(ActivityDescription description)
        {
            UIActivityPopUp popUp = PopUpPool.Get();

            popUp.SetTitle(description.Title);
            popUp.SetGoals(CreateGoals(description.Goals));

            popUp.GlowImage.color = popUp.GlowImage.color.SetAlpha(1f);

            Transform popUpTransform = popUp.transform;
            // position вмеcто localPosition, т.к. DOMove работает с position
            popUpTransform.position = _popUpStartPosition.position;
            popUpTransform.localScale = Vector3.one;

            return popUp;
        }

        [NotNull, ItemNotNull]
        private IReadOnlyCollection<UIActivityPopUpGoal> CreateGoals(IReadOnlyList<ActivityGoal> goals)
        {
            UIActivityPopUpGoal[] uiGoals = new UIActivityPopUpGoal[goals.Count];

            for (int i = 0; i < goals.Count; i++)
                uiGoals[i] = CreateGoal(goals[i]);

            return uiGoals;
        }

        [NotNull]
        private UIActivityPopUpGoal CreateGoal(ActivityGoal goal)
        {
            UIActivityPopUpGoal uiGoal = PopUpGoalPool.Get();

            if (goal.EnableMarker)
                uiGoal.Set(goal.Title, goal.MarkerBgColor, goal.MarkerSprite);
            else
                uiGoal.Set(goal.Title);

            return uiGoal;
        }

        private void ReleasePopUp(UIActivityPopUp popUp)
        {
            popUp.SetTitle(string.Empty);

            foreach (UIActivityPopUpGoal goal in popUp.GetGoals())
            {
                goal.Set(string.Empty);
                PopUpGoalPool.Release(goal);
            }

            PopUpPool.Release(popUp);
        }

        [NotNull]
        private IPool<T> CreatePool<T>(PrefabPoolConfig<T, ReferencePoolConfig> config)
            where T : MonoBehaviour, IPoolObject
        {
            var instanceProvider = new PrefabInstanceProvider<T>(config.PrefabSource);
            return new MonoPool<T>(instanceProvider, config.PoolConfig);
        }
    }
}