﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(UIActivityProgressivePanelItem))]
    [DisallowMultipleComponent]
    public sealed class UIActivityProgressivePanelItem : UIActivityPanelItem
    {
        [field: SerializeField, ChildGameObjectsOnly, Required]
        [field: FoldoutGroup("References")]
        public UIActivityProgress Progress { get; private set; }

        [Button]
        protected override void Reset()
        {
            Progress = null;
            base.Reset();
        }

        protected override void DiscoverDependencies()
        {
            base.DiscoverDependencies();
            Progress ??= GetComponentInChildren<UIActivityProgress>();
        }
    }
}