﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.UI;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(UIActivityProgressiveGoal))]
    [RequireComponent(typeof(UIActivityGoal))]
    public sealed class UIActivityProgressiveGoal : MonoBehaviour, IUIActivityGoal
    {
        [SerializeField, Required, FoldoutGroup("References")]
        private UIActivityGoal _goal;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private UIActivityProgress _progress;

        public RectTransform Transform => _goal.Transform;
        public ISmoothActivator SmoothActivator => _goal.SmoothActivator;
        public UIActivityProgress Progress => _progress;

        public void SetData(ActivityGoal goal) => _goal.SetData(goal);
    }
}