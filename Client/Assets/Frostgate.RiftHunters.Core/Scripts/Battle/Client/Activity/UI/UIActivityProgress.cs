﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.UI;
using Sirenix.OdinInspector;
using UnityEngine;
using System;
using TMPro;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(UIActivityProgress))]
    [DisallowMultipleComponent]
    public sealed class UIActivityProgress : MonoBehaviour
    {
        public IProgress Progress
        {
            get => _progress;
            set
            {
                if (value == null)
                    RemoveProgress();
                else
                    SetProgress(value);
            }
        }

        [field: SerializeField, ChildGameObjectsOnly, Required, FoldoutGroup("References")]
        private UISmoothSlider _slider;

        [field: SerializeField, ChildGameObjectsOnly, Required, FoldoutGroup("References")]
        private TMP_Text _maxValueText;

        [field: SerializeField, ChildGameObjectsOnly, Required, FoldoutGroup("References")]
        private TMP_Text _valueText;

        private IProgress _progress;

        private void OnValidate() => DiscoverDependencies();

        [Button]
        private void Reset()
        {
            _slider = null;
            _maxValueText = null;
            _valueText = null;
        }

        private void Awake() => DiscoverDependencies();

        private void SetProgress(IProgress progress)
        {
            if (_progress == progress)
                return;

            RemoveProgress();
            _progress = progress;

            _progress.CurrentValueChanged += OnCurrentValueChanged;
            OnCurrentValueChanged(_progress.CurrentValue, _progress.CurrentValue);
        }

        private void RemoveProgress()
        {
            if (_progress == null)
                return;

            OnCurrentValueChanged(_progress.CurrentValue, _progress.CurrentValue);
            _progress.CurrentValueChanged -= OnCurrentValueChanged;
            _progress = null;
        }

        private void OnCurrentValueChanged(int _, int newVal)
        {
            UpdateSlider(newVal);
            UpdateCounters(newVal);
        }

        private void UpdateSlider(int currentValue)
        {
            _slider.SetMaxValue(_progress.MaxValue);
            _slider.SetValue(currentValue);
        }

        private void UpdateCounters(int currentValue)
        {
            _maxValueText.text = _progress.MaxValue.ToString();
            _valueText.text = currentValue.ToString();
        }

        private void DiscoverDependencies()
        {
            _slider ??= GetComponentInChildren<UISmoothSlider>();
        }

        public interface IProgress
        {
            event Action<int, int> CurrentValueChanged;

            int CurrentValue { get; }
            int MaxValue { get; }
        }
    }
}