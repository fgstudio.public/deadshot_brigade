﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(UIActivityGoalMarker))]
    [DisallowMultipleComponent]
    public sealed class UIActivityGoalMarker : MonoBehaviour
    {
        [field: SerializeField, ChildGameObjectsOnly, Required]
        public Image MarkerImage { get; private set; }

        [field: SerializeField, ChildGameObjectsOnly, Required]
        public Image MarkerBgImage { get; private set; }

        [Button]
        private void Reset()
        {
            MarkerBgImage = null;
            MarkerImage = null;
        }
    }
}