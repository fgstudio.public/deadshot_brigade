using Frostgate.RiftHunters.Core.Systems.Pool;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    public sealed class UIActivityPopUpGoal : MonoBehaviour, IPoolObject
    {
        [SerializeField, ChildGameObjectsOnly, Required] public TMP_Text _text;
        [SerializeField, ChildGameObjectsOnly, Required] public Image _markerBackground;
        [SerializeField, ChildGameObjectsOnly, Required] public Image _markerIcon;

        public void Set(string text)
        {
            _markerBackground.gameObject.SetActive(false);
            _markerIcon.gameObject.SetActive(false);

            _text.text = text;
        }

        public void Set(string text, Color markerBackgroundColor, Sprite markerIcon)
        {
            _markerBackground.gameObject.SetActive(true);
            _markerIcon.gameObject.SetActive(true);

            _text.text = text;
            _markerBackground.color = markerBackgroundColor;
            _markerIcon.sprite = markerIcon;
        }

        public void OnGet()
        {
        }

        public void OnRelease()
        {
        }
    }
}
