﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;
using System.Linq;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.UI;
using Zenject;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(UIActivityPanelItem))]
    [RequireComponent(typeof(UISmoothActivator))]
    [RequireComponent(typeof(RisingAnimationComponent))]
    [DisallowMultipleComponent]
    public class UIActivityPanelItem : MonoBehaviour, IUIActivityPanelItem
    {
        #region IUIBarPanelItem impl

        GameObject IGameObject.GameObject => this != null? gameObject : null;
        public RectTransform Transform => gameObject.transform as RectTransform;

        [field: SerializeField, Required, FoldoutGroup("References")]
        public UISmoothActivator SmoothActivator { get; private set; }

        [field: SerializeField, Required, FoldoutGroup("References")]
        public RisingAnimationComponent RisingAnimation { get; private set; }

        #endregion


        [SerializeField, AssetSelector, Required, FoldoutGroup("References")]
        private UIActivityGoal _goalPrefab;

        [SerializeField, ChildGameObjectsOnly, Required, FoldoutGroup("References")]
        private Transform _goalContainer;


        [SerializeField, ChildGameObjectsOnly, Required, FoldoutGroup("References")]
        private TMP_Text _titleText;

        [SerializeField, ChildGameObjectsOnly, Required, FoldoutGroup("References")]
        private Toggle _toggle;

        [SerializeField, ChildGameObjectsOnly, Required, FoldoutGroup("References")]
        private GameObject _activeBackground;

        [SerializeField, ChildGameObjectsOnly, Required, FoldoutGroup("References")]
        private GameObject _completedBackground;

        [SerializeField, ChildGameObjectsOnly, Required, FoldoutGroup("References")]
        private UISmoothActivator _completedTextActivator;

        [SerializeField, FoldoutGroup("Settings")]
        private Color _activeTextColor;

        [SerializeField, FoldoutGroup("Settings")]
        private Color _completedTextColor;

        [SerializeField, MinValue(0f), FoldoutGroup("Settings")]
        private float _completedTextTime;

        [ShowInInspector, ReadOnly]
        private ActivityDescription? _description;

        private readonly Dictionary<ActivityGoal, UIActivityGoal> _uiGoals = new();


        protected void OnValidate() => DiscoverDependencies();
        protected void Awake() => DiscoverDependencies();
        protected void Start() => SetCompleted(false);

        protected virtual void DiscoverDependencies()
        {
            SmoothActivator ??= GetComponent<UISmoothActivator>();
            RisingAnimation ??= GetComponent<RisingAnimationComponent>();
        }

        [Button]
        protected virtual void Reset()
        {
            SmoothActivator = null;
            RisingAnimation = null;
            _goalPrefab = null;
            _goalContainer = null;
            _titleText = null;
            _toggle = null;
            _activeBackground = null;
            _completedBackground = null;
            _activeTextColor = Color.clear;
            _completedTextColor = Color.clear;
        }

        public void SetCompleted() =>
            SetCompleted(true);

        public void SetTitle(string title) =>
            _titleText.text = title;

        public void AddGoals(IEnumerable<ActivityGoal> goals) =>
            goals.ForEach(AddGoal);

        public void AddGoal(ActivityGoal goal)
        {
            if (_uiGoals.ContainsKey(goal))
                RemoveGoal(goal);

            _uiGoals[goal] = CreateGoal(goal);
        }

        public void ChangeGoal(ActivityGoal oldGoal, ActivityGoal newGoal)
        {
            if(oldGoal.Title == newGoal.Title)
                return;

            var uiGoal = _uiGoals[oldGoal];
            uiGoal.ChangeData(newGoal);

            _uiGoals.Remove(oldGoal);
            _uiGoals.Add(newGoal, uiGoal);
        }

        public void ChangeGoals(ActivityGoal[] goals)
        {
            if (goals.Length != _uiGoals.Count)
                return;

            for (int i = 0; i < goals.Length; i++)
                ChangeGoal(_uiGoals.ElementAt(i).Key, goals[i]);
        }

        public void RemoveGoals(IEnumerable<ActivityGoal> goals) =>
            goals.ForEach(RemoveGoal);

        public void RemoveGoal(ActivityGoal goal)
        {
            if (_uiGoals.TryGetValue(goal, out UIActivityGoal uiGoal))
            {
                DestroyGoal(uiGoal);
                _uiGoals.Remove(goal);
            }
        }

        public void AddGoal(IUIActivityGoal goal)
        {
            RectTransform goalTransform = goal.Transform;

            goalTransform.SetParent(_goalContainer);
            goalTransform.localScale = Vector3.one;
            goalTransform.localPosition = Vector3.one;
            goalTransform.localRotation = Quaternion.identity;

            goal.SmoothActivator.Activate();
        }

        public void RemoveGoal(IUIActivityGoal goal) =>
            goal.SmoothActivator.Deactivate(onComplete: () => goal.Transform.SetParent(null));

        public ActivityDescription BuildDescription() =>
            new(_titleText.text, _uiGoals.Keys.ToArray());

        private void SetCompleted(bool isCompleted)
        {
            _toggle.isOn = isCompleted;
            _activeBackground.SetActive(!isCompleted);
            _completedBackground.SetActive(isCompleted);
            _titleText.color = isCompleted ? _completedTextColor : _activeTextColor;

            if(isCompleted)
                StartCoroutine(ShowCompleteText());
        }

        private UIActivityGoal CreateGoal(ActivityGoal goal)
        {
            UIActivityGoal uiGoal = Instantiate(_goalPrefab, _goalContainer);
            uiGoal.SetData(goal);
            uiGoal.SmoothActivator.Activate();

            return uiGoal;
        }

        private void DestroyGoal(UIActivityGoal uiGoal) =>
            uiGoal.SmoothActivator.Deactivate(
                onComplete: () => Destroy(uiGoal.gameObject));

        private IEnumerator ShowCompleteText()
        {
            _completedTextActivator.Activate();
            yield return new WaitForSeconds(_completedTextTime);
            _completedTextActivator.Deactivate();
        }
    }
}