﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.UI;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(UIActivityPanel))]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(UIActivityPopUpPlayer))]
    [RequireComponent(typeof(RectTransform))]
    public sealed class UIActivityPanel : MonoBehaviour, IUIActivityPanel
    {
        // TODO: Возможно, не лучшее решение размещать плеер в панели
        [SerializeField, Required] private UIActivityPopUpPlayer _popUpPlayer;

        [SerializeField, Required, ChildGameObjectsOnly]
        private RectTransform _itemsContainer;

        public IUIActivityPopUpPlayer PopUpPlayer => _popUpPlayer;

        private IUIBarPanel<IUIActivityPanelItem> BarPanel => GetBarPanel();

        // TODO: Перенести логику анимаций в IUIPanel.
        private IUIBarPanel<IUIActivityPanelItem> _barPanelCache;

        private void OnValidate() => DiscoverDependencies();

        private void Awake() => DiscoverDependencies();

        public void Add(IUIActivityPanelItem item) => BarPanel.Add(item);

        public void Remove(IUIActivityPanelItem item, Action removedCallback = null) =>
            BarPanel.Remove(item, removedCallback ?? delegate { });

        private void DiscoverDependencies()
        {
            _popUpPlayer ??= GetComponent<UIActivityPopUpPlayer>();
        }

        private IUIBarPanel<IUIActivityPanelItem> GetBarPanel()
        {
            return _barPanelCache ??=
                new UIBarPanel<IUIActivityPanelItem>(new UIPanel<IUIActivityPanelItem>(_itemsContainer));
        }
    }
}