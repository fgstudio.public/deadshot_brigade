﻿using Frostgate.RiftHunters.Core.UI;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    public interface IUIActivityGoal : ISmoothActivation
    {
        RectTransform Transform { get; }
        
        void SetData(ActivityGoal goal);
    }
}