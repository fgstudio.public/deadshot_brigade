﻿using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    public interface IUIActivityPanel
    {
        [NotNull] IUIActivityPopUpPlayer PopUpPlayer { get; }

        void Add([NotNull] IUIActivityPanelItem item);
        void Remove([NotNull] IUIActivityPanelItem item, [CanBeNull] Action removedCallback = null);
    }
}