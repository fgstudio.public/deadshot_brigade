﻿using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Systems.Pool;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(UIActivityPopUp))]
    [DisallowMultipleComponent]
    public sealed class UIActivityPopUp : MonoBehaviour, IPoolObject
    {
        [SerializeField, Required, FoldoutGroup("References")]
        private CanvasGroup _canvasGroup;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Image _glowImage;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private TMP_Text _titleText;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Transform _goalContainer;
        
        public Image GlowImage => _glowImage;
        public CanvasGroup CanvasGroup => _canvasGroup;

        private IReadOnlyCollection<UIActivityPopUpGoal> _goals =
            Array.Empty<UIActivityPopUpGoal>();

        public void SetTitle(string title)
        {
            _titleText.text = title;
        }

        public void SetGoals([NotNull] IReadOnlyCollection<UIActivityPopUpGoal> goals)
        {
            _goals = goals;
            foreach (var goal in goals)
            {
                Transform goalTransform = goal.transform;
                goalTransform.SetParent(_goalContainer);
                goalTransform.position = Vector3.zero;
                goalTransform.localScale = Vector3.one;
                goalTransform.rotation = Quaternion.identity;
            }
        }

        public IEnumerable<UIActivityPopUpGoal> GetGoals() => _goals;

        void IPoolObject.OnGet()
        {
        }

        void IPoolObject.OnRelease()
        {
        }
    }
}