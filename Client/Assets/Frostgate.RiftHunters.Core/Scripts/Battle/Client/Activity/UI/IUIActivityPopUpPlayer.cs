﻿using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.UI
{
    public interface IUIActivityPopUpPlayer
    {
        void Play(ActivityDescription description, [CanBeNull] Action completedCallback = null);
    }
}