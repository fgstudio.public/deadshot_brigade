﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using JetBrains.Annotations;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    public interface ICaptureSpeedReceiver
    {
        int CaptureSpeed { set; }
    }

    public interface ICaptureProgressReceiver
    {
        float MaxProgress { set; }
        float CurrentProgress { set; }
    }

    public interface ICapturedReceiver
    {
        bool IsCaptured { set; }
    }

    public sealed class AreaEventBinding<TReceiver> : IDisposable
        where TReceiver : class, ICaptureProgressReceiver, ICaptureSpeedReceiver, ICapturedReceiver
    {
        [NotNull] public CapturableArea Area { get; }
        [NotNull] public TReceiver Receiver { get; }

        public AreaEventBinding([NotNull] CapturableArea area, [NotNull] TReceiver receiver,
            [NotNull] Action<CapturableArea, TReceiver> initialize)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(area, nameof(area));
            ThrowHelper.ThrowIfArgumentNullOrDefault(receiver, nameof(receiver));

            Area = area;
            Receiver = receiver;

            initialize.Invoke(Area, Receiver);

            Subscribe();
        }

        public void Dispose() => Unsubscribe();

        private void Subscribe()
        {
            Area.InvaderCaught.AddListener(OnInvaderCaught);
            Area.InvaderLost.AddListener(OnInvaderLost);
            Area.CurrentCapturePointsChanged.AddListener(OnCapturePointsChanged);
        }

        private void Unsubscribe()
        {
            Area.InvaderCaught.RemoveListener(OnInvaderCaught);
            Area.InvaderLost.RemoveListener(OnInvaderLost);
            Area.CurrentCapturePointsChanged.RemoveListener(OnCapturePointsChanged);
        }

        private void OnInvaderCaught()
        {
            Receiver.IsCaptured = Area.InvadersCount > 0;
            Receiver.CaptureSpeed = Area.InvadersCount;
        }

        private void OnInvaderLost()
        {
            Receiver.IsCaptured = Area.InvadersCount > 0;
            Receiver.CaptureSpeed = Area.InvadersCount;
        }

        private void OnCapturePointsChanged(float _, float newPoints)
        {
            Receiver.MaxProgress = Area.RequiredCapturePoints;
            Receiver.CurrentProgress = Area.CurrentCapturePoints;
        }
    }
}