﻿using System;
using System.Runtime.CompilerServices;
using Frostgate.RiftHunters.Core.Battle.Client.Targeting;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.MoveToPointActivity;
using Frostgate.RiftHunters.Core.UI.Hud;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.MoveToPointActivity
{
    public class MoveToPointActivityClientHandler : IClientActivityHandler
    {
        private readonly IReadOnlyMoveToPointTaskState _taskState;
        private readonly TargetMarkerComponent _targetMarker;
        private readonly ITargetPanel _targetPanel;

        private bool _isDisposed;

        public MoveToPointActivityClientHandler(
            [NotNull] MoveToPointTaskSceneData taskSceneData,
            [NotNull] ITargetPanel targetPanel)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData, nameof(taskSceneData));

            _taskState = taskSceneData.TaskState;
            _targetMarker = taskSceneData.TargetMarker;
            _targetPanel = targetPanel;

            Subscribe();

            switch (_taskState.Status)
            {
                case ActivityStatus.Inactive: OnActivityDeactivated(); break;
                case ActivityStatus.Active: OnActivityActivated(); break;
                case ActivityStatus.Completed: OnActivityCompleted(); break;
                default: throw new ArgumentOutOfRangeException();
            }
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            Unsubscribe();

            _isDisposed = true;
        }

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{nameof(MoveToPointActivityClientHandler)} already disposed.");
        }

        private void Subscribe()
        {
            _taskState.Activated.AddListener(OnActivityActivated);
            _taskState.Completed.AddListener(OnActivityCompleted);
            _taskState.Deactivated.AddListener(OnActivityDeactivated);
        }

        private void Unsubscribe()
        {
            _taskState.Activated.RemoveListener(OnActivityActivated);
            _taskState.Completed.RemoveListener(OnActivityCompleted);
            _taskState.Deactivated.RemoveListener(OnActivityDeactivated);
        }

        private void OnActivityActivated() => EnableMarker();
        private void OnActivityCompleted() => DisableMarker();
        private void OnActivityDeactivated() => DisableMarker();

        private void EnableMarker()
        {
            _targetMarker.Enable();
            _targetPanel.SetTarget(_targetMarker.MarkerPoint, _targetMarker.Icon, _targetMarker.BackgroundColor, _targetMarker.AnimationIsOn);
        }

        private void DisableMarker()
        {
            _targetMarker.Disable();
            _targetPanel.RemoveTarget(_targetMarker.MarkerPoint);
        }
    }
}