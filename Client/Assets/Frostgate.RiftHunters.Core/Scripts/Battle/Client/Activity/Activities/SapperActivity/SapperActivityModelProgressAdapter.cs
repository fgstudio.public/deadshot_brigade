﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    public sealed class SapperActivityModelProgressAdapter : UIActivityProgress.IProgress
    {
        public event Action<int, int> CurrentValueChanged = delegate { };
        public int MaxValue => _taskState.TotalAreasCount;
        public int CurrentValue => _taskState.CompletedAreasCount;

        [NotNull] private readonly IReadOnlySapperTaskState _taskState;

        private bool _isDisposed;

        public SapperActivityModelProgressAdapter([NotNull] IReadOnlySapperTaskState taskState)
        {
            _taskState = taskState;
            Subscribe();
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            Unsubscribe();
            _isDisposed = true;
        }

        private void Subscribe() => _taskState.CompletedAreasCountChanged.AddListener(OnCompletedAreasCountChanged);

        private void Unsubscribe() => _taskState.CompletedAreasCountChanged.RemoveListener(OnCompletedAreasCountChanged);

        private void OnCompletedAreasCountChanged(int oldCount, int newCount) =>
            CurrentValueChanged.Invoke(oldCount, newCount);

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{nameof(SapperActivityModelProgressAdapter)} already disposed.");
        }
    }
}