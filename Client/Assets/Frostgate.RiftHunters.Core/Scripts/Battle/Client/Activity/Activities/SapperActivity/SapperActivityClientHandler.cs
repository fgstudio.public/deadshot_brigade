﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using System.Collections.Generic;
using JetBrains.Annotations;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    public sealed class SapperActivityClientHandler : IClientActivityHandler
    {
        [NotNull] private readonly IReadOnlySapperTaskState _taskState;
        [NotNull] private readonly IReadOnlyList<CapturableArea> _areas;
        [NotNull] private readonly IAreaViewSpawner _viewSpawner;

        private bool _isDisposed;

        public SapperActivityClientHandler(
            [NotNull] IReadOnlySapperTaskState taskState, [NotNull] IReadOnlyList<CapturableArea> areas,
            [NotNull] IAreaViewSpawner viewSpawner)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskState, nameof(taskState));
            ThrowHelper.ThrowIfArgumentNullOrDefault(areas, nameof(areas));
            ThrowHelper.ThrowIfArgumentNullOrDefault(viewSpawner, nameof(viewSpawner));

            _taskState = taskState;
            _areas = areas;
            _viewSpawner = viewSpawner;

            _areas.ForEach(ConfigureArea);

            Subscribe();
            if (_taskState.Status == ActivityStatus.Active)
                OnActivityActivated();
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            Unsubscribe();

            _isDisposed = true;
        }

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{nameof(SapperActivityClientHandler)} already disposed.");
        }

        private void ConfigureArea(CapturableArea area)
        {
            ICapturableAreaConfig config = _taskState.Config!.GetAreaConfigurationById(area.Id);
            area.Init(config);
        }

        private void Subscribe()
        {
            _taskState.Activated.AddListener(OnActivityActivated);
            _taskState.Completed.AddListener(OnActivityCompleted);
            _taskState.Deactivated.AddListener(OnActivityDeactivated);
        }

        private void Unsubscribe()
        {
            _taskState.Activated.RemoveListener(OnActivityActivated);
            _taskState.Completed.RemoveListener(OnActivityCompleted);
            _taskState.Deactivated.RemoveListener(OnActivityDeactivated);
        }

        private void OnActivityActivated()
        {
            _areas.ForEach(a =>
            {
                if (!a.IsCaptured)
                {
                    a.Captured.AddListener(OnAreaCaptured);
                    EnableAndSpawnView(a);
                }
            });
        }

        private void OnActivityCompleted() => _areas.ForEach(DisableAndUnspawnView);

        private void OnActivityDeactivated() => _areas.ForEach(DisableAndUnspawnView);

        private void EnableAndSpawnView(CapturableArea area)
        {
            area.Enable();
            _viewSpawner.SpawnViewFor(area);
        }

        private void DisableAndUnspawnView(CapturableArea area)
        {
            _viewSpawner.UnspawnViewFor(area);
            area.Disable();
        }

        private void OnAreaCaptured(CapturableArea area)
        {
            area.Captured.RemoveListener(OnAreaCaptured);
            DisableAndUnspawnView(area);
        }
    }
}