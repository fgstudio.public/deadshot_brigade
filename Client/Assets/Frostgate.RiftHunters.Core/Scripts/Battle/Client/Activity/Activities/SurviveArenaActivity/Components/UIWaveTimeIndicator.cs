﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.UI;
using Sirenix.OdinInspector;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(UIWaveTimeIndicator))]
    [DisallowMultipleComponent]
    public sealed class UIWaveTimeIndicator : UIBarPanelItem, IPoolObject, IWaveTimeIndicator
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private WaveTimeIndicator _indicator;

        private void OnValidate() => DiscoverDependencies();
        private void Awake() => DiscoverDependencies();

        private void DiscoverDependencies()
        {
            _indicator ??= GetComponentInChildren<WaveTimeIndicator>();
        }

        public void Indicate(TimeSpan time) => _indicator.Indicate(time);

        [Button, HideInEditorMode]
        public void Indicate(float seconds) => _indicator.Indicate(seconds);

        void IPoolObject.OnGet() => ((IPoolObject)_indicator).OnGet();

        void IPoolObject.OnRelease() => ((IPoolObject)_indicator).OnRelease();
    }
}