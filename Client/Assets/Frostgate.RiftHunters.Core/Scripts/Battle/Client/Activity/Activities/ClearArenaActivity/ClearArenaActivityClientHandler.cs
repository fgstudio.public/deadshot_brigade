﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.ClearArenaActivity;
using JetBrains.Annotations;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    public class ClearArenaActivityClientHandler : IClientActivityHandler
    {
        private bool _isDisposed;

        public ClearArenaActivityClientHandler([NotNull] ClearArenaTaskSceneData taskSceneData)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData, nameof(taskSceneData));
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            _isDisposed = true;
        }

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{nameof(ClearArenaActivityClientHandler)} already disposed.");
        }
    }
}