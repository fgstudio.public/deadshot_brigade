﻿using System;
using System.Linq;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    public sealed class SurviveArenaActivityClientUIHandler : ClientActivityUIHandler<UIActivityPanelItem, IUIActivityGoal>
    {
        [NotNull] private readonly IReadOnlySurviveArenaActivityTaskState _taskState;
        [NotNull] private readonly IReadOnlyBotSpawnState _spawnState;
        [NotNull] private readonly IReadOnlyList<WaveGate> _gates;
        [NotNull] private readonly IWaveGateIndication _gateIndication;

        public SurviveArenaActivityClientUIHandler(
            [NotNull] IReadOnlySurviveArenaActivityTaskState taskState,
            [NotNull] IUIActivityPanel activityPanel,
            [NotNull] SurviveArenaActivityViewConfig viewConfig,
            [NotNull] IReadOnlyBotSpawnState spawnState,
            [NotNull] IReadOnlyList<WaveGate> gates,
            [NotNull] IWaveGateIndication gateIndication)
            : base(taskState, activityPanel, viewConfig)
        {
            _taskState = taskState;
            _spawnState = spawnState;
            _gates = gates;
            _gateIndication = gateIndication;

            spawnState.DataUpdated.AddListener(OnDataUpdated);
            taskState.Config.Spawners.Select(s => s.Id)
                .ForEach(OnDataUpdated);
        }

        protected override void OnActivated()
        {
            base.OnActivated();
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();
        }

        protected override void OnCompleted()
        {
            base.OnCompleted();
        }
        

        protected override void Dispose(bool disposed)
        {
            _spawnState.DataUpdated.RemoveListener(OnDataUpdated);
            _gateIndication.Dispose();
        }

        private void OnDataUpdated(string spawnerId)
        {
            if (!_taskState.IsSpawnerIdTarget(spawnerId))
                return;

            int spawnerIdx = _taskState.Config.Spawners.IndexOf(s => s.Id == spawnerId);
            BotSpawnerConfig config = _taskState.Config.Spawners[spawnerIdx];

            WaveGate gate = _gates[spawnerIdx];
            
            if (_spawnState.Data.TryGetValue(spawnerId, out BotSpawnerData data) &&
                data.Status == BotSpawnerStatus.Awaiting)
            {
                TimeSpan startTime = TimeSpan.FromSeconds(data.WaitingStartTime);
                TimeSpan delay = config.StartupDelay - startTime; // ???
                
                // TODO: выставить данные времени в указатель и HUD-плашку
                _gateIndication.StartIndication(gate, config.StartupDelay);
            }
            else
            {
                // TODO: убрать указатели и HUD-плашку
                _gateIndication.StopIndication(gate);
            }
        }
    }
}