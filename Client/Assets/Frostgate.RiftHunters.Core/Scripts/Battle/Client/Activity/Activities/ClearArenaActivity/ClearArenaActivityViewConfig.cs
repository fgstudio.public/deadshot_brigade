﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.ClearArenaActivity;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    [CreateAssetMenu(fileName = nameof(ClearArenaActivityViewConfig),
        menuName = ActivityAssetMenu.Menu + "/" + nameof(ClearArenaActivityViewConfig))]
    public sealed class ClearArenaActivityViewConfig : ActivityViewConfig<UIActivityPanelItem, UIActivityGoal>
    {
    }
}