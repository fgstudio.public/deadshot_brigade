﻿using System;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    public interface IWaveTimeIndicator
    {
        void Indicate(TimeSpan time);
    }
}