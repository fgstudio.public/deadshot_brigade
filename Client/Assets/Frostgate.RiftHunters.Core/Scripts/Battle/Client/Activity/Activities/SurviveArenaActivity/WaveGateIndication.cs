﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.UI.Hud.Pointing;
using Frostgate.RiftHunters.Core.UI;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using System;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    public sealed class WaveGateIndication : IWaveGateIndication
    {
        [NotNull] private readonly IMultiplePointerPanel _pointerPanel;
        [NotNull] private readonly IUIBarPanel<IUIBarPanelItem> _barPanel;
        [NotNull] private readonly SurviveArenaActivityViewConfig _viewConfig;

        [NotNull] private readonly IPool<WaveTimeIndicator> _indicatorPool;
        [NotNull] private readonly IPool<UIWaveTimeIndicator> _uiIndicatorPool;
        [NotNull] private readonly NativePoolConfig _indicatorPoolConfig;

        [NotNull] private readonly IDictionary<WaveGate, UIWaveTimeIndicator> _gateUIIndicators =
            new Dictionary<WaveGate, UIWaveTimeIndicator>();

        private bool _isDisposed;

        public WaveGateIndication([NotNull] IMultiplePointerPanel pointerPanel,
            [NotNull] IUIBarPanel<IUIBarPanelItem> barPanel,
            [NotNull] SurviveArenaActivityViewConfig viewConfig)
        {
            _pointerPanel = pointerPanel;
            _barPanel = barPanel;
            _viewConfig = viewConfig;

            _indicatorPoolConfig =
                new NativePoolConfig(
                    _viewConfig.IndicatorPoolInitialCount,
                    _viewConfig.IndicatorPoolMaxCount,
                    nameof(WaveGateIndication));

            _indicatorPool = new AutoReleasingMonoPool<WaveTimeIndicator>(
                () => Object.Instantiate(_viewConfig.TimeIndicatorPrefab),
                i => Object.Destroy(i.gameObject), _indicatorPoolConfig);

            _uiIndicatorPool = new MonoPool<UIWaveTimeIndicator>(
                () => Object.Instantiate(_viewConfig.UITimeIndicatorPrefab),
                i => Object.Destroy(i.gameObject), _indicatorPoolConfig);
        }

        public void StartIndication(WaveGate gate, TimeSpan time)
        {
            _pointerPanel.SetTarget(gate.PointerTransform, _viewConfig.PointerIcon, _viewConfig.PointerBackgroundColor);

            WaveTimeIndicator indicator = _indicatorPool.Get();
            SetIndicatorParent(indicator, gate.IndicatorTransform);
            indicator.Indicate(time);

            UIWaveTimeIndicator uiIndicator = _uiIndicatorPool.Get();
            _barPanel.Add(uiIndicator);
            _gateUIIndicators[gate] = uiIndicator;
            uiIndicator.Indicate(time);
        }

        public void StopIndication(WaveGate gate)
        {
            _pointerPanel.RemoveTarget(gate.PointerTransform);
            if (_gateUIIndicators.TryGetValue(gate, out UIWaveTimeIndicator uiIndicator))
            {
                _barPanel.Remove(uiIndicator, () => _uiIndicatorPool.Release(uiIndicator));
                _gateUIIndicators.Remove(gate);
            }
        }

        private void SetIndicatorParent(WaveTimeIndicator indicator, Transform parent)
        {
            Transform t = indicator.transform;

            t.SetParent(parent);
            t.localScale = Vector3.one;
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.identity;
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            _indicatorPool.Dispose();
            _uiIndicatorPool.Dispose();
            _indicatorPoolConfig.Dispose();
            _isDisposed = true;
        }

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{nameof(WaveGateIndication)} already disposed.");
        }
    }
}