﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    public interface IAreaMarkerPanel
    {
        void Add(string id, Transform transform);
        void Remove(Transform transform);
    }
}