﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    public interface IAreaViewSpawner
    {
        void SpawnViewFor(CapturableArea area);
        void UnspawnViewFor(CapturableArea area);
    }
}