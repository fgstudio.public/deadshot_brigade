﻿using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.MoveToPointActivity;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.MoveToPointActivity
{
    [CreateAssetMenu(menuName = ActivityAssetMenu.Menu + "/" + nameof(MoveToPointActivityViewConfig),
        fileName = nameof(MoveToPointActivityViewConfig))]
    public class MoveToPointActivityViewConfig : ActivityViewConfig<UIActivityPanelItem, UIActivityGoal>
    {
    }
}