﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using System;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    public abstract class AreaViewSpawnerBase<TView> : IAreaViewSpawner
        where TView : MonoBehaviour, ICapturedReceiver, ICaptureProgressReceiver, ICaptureSpeedReceiver
    {
        [NotNull] private readonly TView _viewPrefab;
        [CanBeNull] private readonly ILogger _logger;

        [NotNull] private readonly IDictionary<CapturableArea, AreaEventBinding<TView>> _views =
            new Dictionary<CapturableArea, AreaEventBinding<TView>>();

        public AreaViewSpawnerBase([NotNull] TView viewPrefab, [CanBeNull] ILogger logger)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(viewPrefab, nameof(viewPrefab));

            _viewPrefab = viewPrefab;
            _logger = logger;
        }


        public void SpawnViewFor(CapturableArea area)
        {
            if (_views.ContainsKey(area))
            {
                _logger?.LogWarning(
                    $"Can't spawn view of type '{typeof(TView)}' for area with id '{area.Id}' because view already spawned.");
            }

            TView view = Object.Instantiate(_viewPrefab, area.transform);
            _views.Add(area, new AreaEventBinding<TView>(area, view, InitViewOnBinding));

            OnViewSpawned(area, view);
        }

        public void UnspawnViewFor(CapturableArea area)
        {
            if (!_views.ContainsKey(area))
                return;

            AreaEventBinding<TView> binding = _views[area];
            binding.Dispose();
            _views.Remove(area);

            OnViewWillBeDestroyed(binding.Area, binding.Receiver, () => Object.Destroy(binding.Receiver.gameObject));
        }

        protected abstract void OnViewSpawned([NotNull] CapturableArea area, [NotNull] TView view);

        protected abstract void OnViewWillBeDestroyed([NotNull] CapturableArea area, [NotNull] TView view,
            [NotNull] Action readyToDestroy);

        protected abstract void InitViewOnBinding([NotNull] CapturableArea area, [NotNull] TView view);
    }
}