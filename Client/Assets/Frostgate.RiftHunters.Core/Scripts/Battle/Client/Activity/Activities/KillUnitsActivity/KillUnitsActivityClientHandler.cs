﻿using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.KillUnitsActivity;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    public sealed class KillUnitsActivityClientHandler : IClientActivityHandler
    {
        private bool _isDisposed;

        public KillUnitsActivityClientHandler([NotNull] KillUnitsTaskSceneData taskSceneData)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData, nameof(taskSceneData));
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            _isDisposed = true;
        }

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{nameof(ClearArenaActivityClientHandler)} already disposed.");
        }
    }
}