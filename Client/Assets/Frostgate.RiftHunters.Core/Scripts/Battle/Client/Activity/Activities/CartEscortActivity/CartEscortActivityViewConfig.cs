﻿using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.CartEscortActivity
{
    [CreateAssetMenu(fileName = nameof(CartEscortActivityViewConfig),
        menuName = ActivityAssetMenu.Menu + "/" + nameof(CartEscortActivityViewConfig))]
    public sealed class CartEscortActivityViewConfig : ActivityViewConfig<UIActivityPanelItem, UIActivityGoal>
    {
    }
}