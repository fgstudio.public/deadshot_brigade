﻿using Frostgate.RiftHunters.Core.UI.Hud;
using Frostgate.RiftHunters.Core.UI.Hud.Pointing;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    public sealed class AreaMarkerPanel : IAreaMarkerPanel
    {
        [NotNull] private readonly ITargetPanel _pointerPanel;
        [NotNull] private readonly IAreaDataProvider<Sprite> _spriteProvider;
        private readonly Color _color;

        public AreaMarkerPanel([NotNull] ITargetPanel pointerPanel,
            IAreaDataProvider<Sprite> spriteProvider, Color color)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(pointerPanel, nameof(pointerPanel));
            ThrowHelper.ThrowIfArgumentNullOrDefault(spriteProvider, nameof(spriteProvider));

            _pointerPanel = pointerPanel;
            _spriteProvider = spriteProvider;
            _color = color;
        }

        public void Add(string id, Transform transform) =>
            _pointerPanel.SetTarget(transform, _spriteProvider.Get(id), _color, false);

        public void Remove(Transform transform) => _pointerPanel.RemoveTarget(transform);
    }
}