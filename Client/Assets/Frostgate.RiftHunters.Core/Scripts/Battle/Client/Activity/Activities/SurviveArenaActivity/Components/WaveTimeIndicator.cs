﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity;
using Frostgate.RiftHunters.Core.UI.Animations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Components;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using UnityEngine;
using System;
using TMPro;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(WaveTimeIndicator))]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(TimerComponent))]
    [RequireComponent(typeof(FadeAnimation))]
    public sealed class WaveTimeIndicator : MonoBehaviour, IPoolObject, IAutoReleased, IWaveTimeIndicator
    {
        [SerializeField, Required, FoldoutGroup("References")]
        private TimerComponent _timer;

        [SerializeField, Required, FoldoutGroup("References")]
        private FadeAnimation _fade;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private TMP_Text _timeText;

        [SerializeField, FoldoutGroup("Settings"), PropertyTooltip("dd\\.hh\\:mm\\:ss")]
        private string _timeSpanFormat = "mm\\:ss";


        event UnityAction<IAutoReleased> IAutoReleased.ReadyForRelease
        {
            add => _readyForRelease += value;
            remove => _readyForRelease -= value;
        }

        private UnityAction<IAutoReleased> _readyForRelease = delegate { };

        private void OnValidate()
        {
            DiscoverDependencies();
            InitDependencies();
        }

        private void Awake()
        {
            DiscoverDependencies();
            InitDependencies();

            _timer.Elapsed.AddListener(OnTimerElapsed);
            _timer.Ticked.AddListener(OnTimerTicked);
            _fade.Finished.AddListener(InvokeReadyForReleaseEvent);
        }

        private void OnDestroy()
        {
            _timer.Elapsed.RemoveListener(OnTimerElapsed);
            _timer.Ticked.RemoveListener(OnTimerTicked);
            _fade.Finished.RemoveListener(InvokeReadyForReleaseEvent);
        }

        public void Indicate(TimeSpan time)
        {
            _timer.Seconds = (float)time.TotalSeconds;
            SetTimeText(time);
            _timer.Enable();
        }

        [Button(Name = nameof(Indicate)), HideInEditorMode]
        public void Indicate(float seconds) => Indicate(TimeSpan.FromSeconds(seconds));

        private void DiscoverDependencies()
        {
            _timer ??= GetComponent<TimerComponent>();
            _fade ??= GetComponent<FadeAnimation>();
        }

        private void InitDependencies()
        {
            _timer.Disable();
            _fade.Stop();
        }

        private void OnTimerElapsed()
        {
            SetTimeText(TimeSpan.Zero);
            _fade.Play();
        }
        
        private void OnTimerTicked() => SetTimeText(_timer.Left);

        private void SetTimeText(TimeSpan time)
        {
            _timeText.text = time.ToString(_timeSpanFormat);
        }

        private void InvokeReadyForReleaseEvent() => _readyForRelease.Invoke(this);

        void IPoolObject.OnGet()
        {
        }

        void IPoolObject.OnRelease()
        {
            _timer.Disable();
            _fade.Stop();
        }
    }
}