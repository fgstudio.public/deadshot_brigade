﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using Sirenix.OdinInspector;
using UnityEngine;
using DG.Tweening;
using Frostgate.RiftHunters.Core.UI;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(SapperActivity) + nameof(CapturingIndicator))]
    [DisallowMultipleComponent]
    public sealed class CapturingIndicator : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private SpriteRenderer _areaSprite;

        [SerializeField, FoldoutGroup("Settings")]
        private FadeSettings _areaSpriteFadeSettings;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private SpriteRenderer _capturedAreaSprite;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private SpeedIndicator _captureSpeedIndicator;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Bar _captureProgressBar;

        [SerializeField, FoldoutGroup("Settings")]
        private FadeSettings _capturedAreaSpriteFadeSettings;


        [ShowInInspector, FoldoutGroup("Info")]
        public IndicatorState State
        {
            get => _state;
            set
            {
                _state = value;
                HandleState();
            }
        }

        [ShowInInspector, FoldoutGroup("Info")]
        public float Radius
        {
            get => _radius;
            set => SetRadius(value);
        }

        [ShowInInspector, FoldoutGroup("Info")]
        public float CaptureMaxProgress
        {
            get => _captureProgressBar.Max;
            set => _captureProgressBar.SetMax(value);
        }

        [ShowInInspector, FoldoutGroup("Info")]
        public float CaptureProgress
        {
            get => _captureProgressBar.Current;
            set => _captureProgressBar.SetCurrent(value);
        }

        [ShowInInspector, FoldoutGroup("Info")]
        public int CaptureSpeed
        {
            get => _captureSpeedIndicator.Speed;
            set => _captureSpeedIndicator.Speed = value;
        }


        private IndicatorState _state = IndicatorState.Uncaptured;

        private Fader<SpriteRenderer> _areaSpriteFader;

        private Fader<SpriteRenderer> _capturedAreaSpriteFader;

        private bool _isHidden;

        private float _radius;


        private void OnValidate() => DiscoverReferences();

        [Button]
        private void Reset()
        {
            _areaSprite = null;
            _areaSpriteFadeSettings = new();
            _capturedAreaSprite = null;
            _capturedAreaSpriteFadeSettings = new();
            _captureSpeedIndicator = null;
            _captureProgressBar = null;
        }

        private void Awake()
        {
            _areaSpriteFader = new Fader<SpriteRenderer>(_areaSprite,
                (r, alpha, duration) => r.DOFade(alpha, duration), _areaSpriteFadeSettings);
            _capturedAreaSpriteFader = new Fader<SpriteRenderer>(_capturedAreaSprite,
                (r, alpha, duration) => r.DOFade(alpha, duration), _capturedAreaSpriteFadeSettings);

            DiscoverReferences();
        }

        private void Start() => Show();

        [Button]
        public void Show()
        {
            _isHidden = false;
            _areaSpriteFader.SetFaded(false);
            HandleState();
        }

        [Button]
        public void Hide()
        {
            _isHidden = true;
            _captureSpeedIndicator.Hide();
            _areaSpriteFader.SetFaded(true);
            _capturedAreaSpriteFader.SetFaded(true);
        }

        private void SetRadius(float value)
        {
            _radius = value;
            Vector2 spriteSize = Vector2.one * (value * 2);

            _areaSprite.size = spriteSize;
            _capturedAreaSprite.size = spriteSize;
        }

        private void DiscoverReferences()
        {
            SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
            _areaSprite ??= renderers.Length >= 1 ? renderers[0] : null;
            _capturedAreaSprite ??= renderers.Length >= 2 ? renderers[1] : null;
            _captureSpeedIndicator ??= GetComponentInChildren<SpeedIndicator>();
            _captureProgressBar ??= GetComponentInChildren<Bar>();
        }

        private void HandleState()
        {
            bool isInvisible = _state == IndicatorState.Uncaptured || _isHidden;

            _areaSpriteFader.SetFaded(!isInvisible);
            _capturedAreaSpriteFader.SetFaded(isInvisible);

            Action speedIndicatorAction = isInvisible ? _captureSpeedIndicator.Hide : _captureSpeedIndicator.Show;
            speedIndicatorAction.Invoke();
        }

        public enum IndicatorState : byte
        {
            Uncaptured,
            Captured
        }
    }
}