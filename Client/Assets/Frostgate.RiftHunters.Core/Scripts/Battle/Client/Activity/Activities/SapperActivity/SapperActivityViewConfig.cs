﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using Frostgate.RiftHunters.Core.UI.Hud.Activity.SapperActivity;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using System;
using System.Linq;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    [CreateAssetMenu(fileName = nameof(SapperActivityViewConfig),
        menuName = ActivityAssetMenu.Menu + "/" + nameof(SapperActivityViewConfig))]
    public sealed class SapperActivityViewConfig : ActivityViewConfig<UIActivityPanelItem, UIActivityGoal>,
        IAreaDataProvider<Sprite>, IAreaDataProvider<string>
    {
        [SerializeField, Required, AssetSelector]
        private CapturableAreaView _areaViewPrefab;

        [SerializeField, Required, AssetSelector]
        private UISapperActivityAreaProgressBar _areaProgressBarPrefab;

        [SerializeField, Required, ValidateInput(nameof(HasNoIdDuplicates))]
        private IdRelatedData[] _idRelatedData;


        [SerializeField] private Color _markerColor;

        [SerializeField, Required] private UIActivityProgressiveGoal _uiCaptureMinesGoalPrefab;
        [SerializeField] private ActivityGoal _captureMinesGoal;

        private IReadOnlyDictionary<string, IdRelatedData> _idRelatedDataMap;
        public CapturableAreaView AreaViewPrefab => _areaViewPrefab;
        public UISapperActivityAreaProgressBar AreaProgressBarPrefab => _areaProgressBarPrefab;
        public Color MarkerColor => _markerColor;
        public UIActivityProgressiveGoal UICaptureMinesGoalPrefab => _uiCaptureMinesGoalPrefab;
        public ActivityGoal CaptureMinesGoal => _captureMinesGoal;

        [Button]
        private void Reset()
        {
            _areaViewPrefab = null;
        }

        private bool HasNoIdDuplicates(IdRelatedData[] spritePairs, ref string errMsg, ref InfoMessageType? msgType)
        {
            var set = new HashSet<string>();
            foreach (IdRelatedData pair in spritePairs)
            {
                string id = pair.Id;
                if (!set.Contains(id))
                {
                    set.Add(id);
                }
                else
                {
                    msgType = InfoMessageType.Error;
                    errMsg = $"Id '{id}' duplicate found.";

                    return false;
                }
            }

            return true;
        }

        [Serializable]
        private struct IdRelatedData
        {
            public string Id => _id;
            public Sprite Sprite => _sprite;
            public string BarIndex => _barIndex;

            [SerializeField, Required] private string _id;

            [SerializeField, Required, AssetSelector]
            private Sprite _sprite;

            [SerializeField, Required] private string _barIndex;
        }

        Sprite IAreaDataProvider<Sprite>.Get(string areaId)
        {
            _idRelatedDataMap ??= _idRelatedData.ToDictionary(p => p.Id);

            return _idRelatedDataMap[areaId].Sprite;
        }

        string IAreaDataProvider<string>.Get(string areaId)
        {
            _idRelatedDataMap ??= _idRelatedData.ToDictionary(p => p.Id);

            return _idRelatedDataMap[areaId].BarIndex;
        }
    }
}