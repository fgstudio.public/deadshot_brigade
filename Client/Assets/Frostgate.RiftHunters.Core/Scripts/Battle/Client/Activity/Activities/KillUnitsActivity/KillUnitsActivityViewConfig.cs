﻿using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.KillUnitsActivity;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    [CreateAssetMenu(fileName = nameof(KillUnitsActivityViewConfig),
        menuName = ActivityAssetMenu.Menu + "/" + nameof(KillUnitsActivityViewConfig))]
    public sealed class KillUnitsActivityViewConfig : ActivityViewConfig<UIActivityPanelItem, UIActivityGoal>
    {
        [field: Title(""), PropertySpace(-20)]
        [field: SerializeField, Required] public UnitEffectConfig Effect { get; private set; }

        [field: FoldoutGroup(nameof(DescriptionByEffect)), HideLabel]
        [field: SerializeField] public ActivityDescription DescriptionByEffect { get; private set; }
    }
}