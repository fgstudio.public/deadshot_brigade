﻿using System;
using System.Linq;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    public sealed class SurviveArenaActivityClientHandler : IClientActivityHandler
    {
        [NotNull] private readonly SurviveArenaTaskSceneData _taskSceneData;
        [NotNull] private readonly IReadOnlyBotSpawnState _spawnState;

        private bool _isDisposed;

        public SurviveArenaActivityClientHandler(
            [NotNull] SurviveArenaTaskSceneData taskSceneData,
            [NotNull] IReadOnlyBotSpawnState spawnState)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData, nameof(taskSceneData));
            _taskSceneData = taskSceneData;
            _spawnState = spawnState;

            spawnState.DataUpdated.AddListener(OnDataUpdated);
            taskSceneData.TaskState.Config.Spawners.Select(s => s.Id)
                .ForEach(OnDataUpdated);
        }

        public void Dispose()
        {
            if (_isDisposed)
                throw new ObjectDisposedException(
                    $"{nameof(SurviveArenaActivityClientHandler)} already disposed.");

            _isDisposed = true;
            _spawnState.DataUpdated.RemoveListener(OnDataUpdated);
        }

        private void OnDataUpdated(string spawnerId)
        {
            IReadOnlySurviveArenaActivityTaskState taskState = _taskSceneData.TaskState;
            if (!taskState.IsSpawnerIdTarget(spawnerId))
                return;

            int index = taskState.Config.Spawners.IndexOf(s => s.Id == spawnerId);
            bool isActive = _spawnState.Data.TryGetValue(spawnerId, out BotSpawnerData data) &&
                               data.Status == BotSpawnerStatus.Awaiting;

            _taskSceneData.Gates[index].FloorArea.SetActive(isActive);
        }
    }
}