﻿using System;
using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI.Hud;
using Frostgate.RiftHunters.Core.Battle.Client.Targeting;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.KillUnitsActivity;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    public sealed class KillUnitsActivityClientUIHandler : ClientActivityUIHandler<UIActivityPanelItem, IUIActivityGoal>
    {
        [NotNull] private readonly UIActivityDescriptionPanel _barPanel;
        [NotNull] private readonly IReadOnlyKillUnitsTaskState _taskState;
        [NotNull] private readonly KillUnitsActivityViewConfig _viewConfig;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;
        [NotNull] private readonly ITargetPanel _targetPanel;
        [NotNull] private readonly TargetMarkerComponent _targetMarkerPrefab;

        private readonly ILogger _logger = LoggerFactory.CreateLogger<KillUnitsActivityClientUIHandler>();
        private readonly Dictionary<UnitNetwork, TargetMarkerComponent> _markers = new();
        private readonly HashSet<UnitNetwork> _subscribedUnits = new();

        private bool _isDisposed;

        public KillUnitsActivityClientUIHandler(
            [NotNull] KillUnitsTaskSceneData taskSceneData,
            [NotNull] UIActivityDescriptionPanel barPanel,
            [NotNull] IUIActivityPanel activityPanel,
            [NotNull] KillUnitsActivityViewConfig viewConfig,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection,
            [NotNull] ITargetPanel targetPanel)
            : base(taskSceneData.TaskState, activityPanel, viewConfig)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(unitCollection, nameof(unitCollection));

            _taskState = taskSceneData.TaskState;
            _targetMarkerPrefab = taskSceneData.TargetMarkerPrefab;
            _barPanel = barPanel;
            _viewConfig = viewConfig;
            _unitCollection = unitCollection;
            _targetPanel = targetPanel;

            _unitCollection.Added += OnUnitAdded;
            _unitCollection.Removed += OnUnitRemoved;
            _logger.Log("Subscribed unit collection");
        }

        protected override void Dispose(bool disposed)
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{nameof(ClearArenaActivityClientHandler)} already disposed.");

            _unitCollection.Added -= OnUnitAdded;
            _unitCollection.Removed -= OnUnitRemoved;
            _logger.Log("Unsubscribed unit collection");

            _subscribedUnits.ToArray().ForEach(UnsubscribeUnits);
            _markers.Keys.ToArray().ForEach(DetachTargetMarker);

            _isDisposed = true;
        }

        protected override void OnActivated()
        {
            _unitCollection.ForEach(OnUnitAdded);
        }

        protected override void OnCompleted()
        {
            _unitCollection.ForEach(OnUnitRemoved);
            _markers.Keys.ToArray().ForEach(DetachTargetMarker);
        }

        protected override void OnDeactivated()
        {
            _unitCollection.ForEach(OnUnitRemoved);
            _markers.Keys.ToArray().ForEach(DetachTargetMarker);
        }

        private void OnUnitAdded(UnitNetwork unit)
        {
            if (KillUnitsActivityHelper.IsUnitTarget(unit.UnitNetworkState, _taskState))
            {
                if (!IsUnitSubscribed(unit))
                    SubscribeUnit(unit);

                if (!IsMarkerAttached(unit))
                    AttachTargetMarker(unit);
            }
        }

        private void OnUnitRemoved(UnitNetwork unit)
        {
            if (KillUnitsActivityHelper.IsUnitTarget(unit.UnitNetworkState, _taskState))
            {
                if (IsUnitSubscribed(unit))
                    UnsubscribeUnits(unit);

                if (IsMarkerAttached(unit))
                    DetachTargetMarker(unit);
            }
        }

        private bool IsUnitSubscribed(UnitNetwork unit) =>
            _subscribedUnits.Contains(unit);

        private void SubscribeUnit(UnitNetwork unit)
        {
            _subscribedUnits.Add(unit);
            unit.UnitNetworkEffects.OnAdd += OnEffectAdded;
            unit.UnitNetworkEffects.OnRemove += OnEffectRemoved;
            _logger.Log($"Subscribed target unit {unit.Identity.netId}");
        }

        private void UnsubscribeUnits(UnitNetwork unit)
        {
            _subscribedUnits.Remove(unit);
            unit.UnitNetworkEffects.OnAdd -= OnEffectAdded;
            unit.UnitNetworkEffects.OnRemove -= OnEffectRemoved;
            _logger.Log($"Unsubscribed target unit {unit.Identity.netId}");
        }

        private bool IsMarkerAttached(UnitNetwork unit) =>
            _markers.ContainsKey(unit);

        private void AttachTargetMarker(UnitNetwork unit)
        {
            var marker = UnityEngine.Object.Instantiate(_targetMarkerPrefab, unit.transform);
            _markers[unit] = marker;
            _targetPanel.SetTarget(marker.MarkerPoint, marker.Icon, marker.BackgroundColor, marker.AnimationIsOn);
            marker.Enable();

            _logger.Log($"Attached marker to unit {unit.Identity.netId}");
        }

        private void DetachTargetMarker(UnitNetwork unit)
        {
            TargetMarkerComponent marker = _markers[unit];

            marker.Disable();
            _markers.Remove(unit);
            _targetPanel.RemoveTarget(marker.MarkerPoint);

            _logger.Log($"Detached marker from unit {unit.Identity.netId}");
        }

        private void OnEffectAdded(UnitEffectState effectState)
        {
            if (PanelItem?.gameObject != null && effectState.Config == _viewConfig.Effect)
            {
                PanelItem.SetTitle(_viewConfig.DescriptionByEffect.Title);
                PanelItem.ChangeGoals(_viewConfig.DescriptionByEffect.Goals);

                _logger.Log("Goal has been changed by adding effect");
            }
        }

        private void OnEffectRemoved(UnitEffectState effectState)
        {
            if (PanelItem?.gameObject != null && effectState.Config == _viewConfig.Effect)
            {
                PanelItem.SetTitle(_viewConfig.InPanelDescription.Title);
                PanelItem.ChangeGoals(_viewConfig.InPanelDescription.Goals);

                _logger.Log("Goal has been changed by removing effect");
            }
        }
    }
}