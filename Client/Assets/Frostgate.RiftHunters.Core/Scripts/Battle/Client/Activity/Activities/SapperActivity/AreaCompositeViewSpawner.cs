﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    public sealed class AreaCompositeViewSpawner : IAreaViewSpawner
    {
        private readonly IAreaViewSpawner[] _spawners;

        public AreaCompositeViewSpawner(params IAreaViewSpawner[] spawners)
        {
            _spawners = spawners;
        }

        public void SpawnViewFor(CapturableArea area) => _spawners.ForEach(m => m.SpawnViewFor(area));
        public void UnspawnViewFor(CapturableArea area) => _spawners.ForEach(m => m.UnspawnViewFor(area));
    }
}