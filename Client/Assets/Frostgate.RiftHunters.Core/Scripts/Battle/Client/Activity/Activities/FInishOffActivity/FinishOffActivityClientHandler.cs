﻿using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.FinishOffActivity;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    public sealed class FinishOffActivityClientHandler : IClientActivityHandler
    {
        private bool _isDisposed;

        public FinishOffActivityClientHandler([NotNull] FinishOffTaskSceneData taskSceneData)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData, nameof(taskSceneData));
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            _isDisposed = true;
        }

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{nameof(FinishOffActivityClientHandler)} already disposed.");
        }
    }
}