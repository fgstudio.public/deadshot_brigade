﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using Frostgate.RiftHunters.Core.UI.Hud.Activity.SapperActivity;
using Frostgate.RiftHunters.Core.UI;
using JetBrains.Annotations;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    public sealed class AreaProgressBarSpawner : AreaViewSpawnerBase<UISapperActivityAreaProgressBar>
    {
        [NotNull] private readonly IAreaDataProvider<string> _areaIndexProvider;
        [NotNull] private readonly IUIBarPanel<IUIBarPanelItem> _barPanel;

        public AreaProgressBarSpawner([NotNull] UISapperActivityAreaProgressBar viewPrefab,
            [NotNull] IAreaDataProvider<string> areaIndexProvider, [NotNull] IUIBarPanel<IUIBarPanelItem> barPanel,
            [CanBeNull] ILogger logger) : base(viewPrefab,
            logger)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(areaIndexProvider, nameof(areaIndexProvider));
            ThrowHelper.ThrowIfArgumentNullOrDefault(barPanel, nameof(barPanel));

            _areaIndexProvider = areaIndexProvider;
            _barPanel = barPanel;
        }

        protected override void OnViewSpawned(CapturableArea area, UISapperActivityAreaProgressBar view) =>
            _barPanel.Add(view);

        protected override void OnViewWillBeDestroyed(CapturableArea area, UISapperActivityAreaProgressBar view,
            Action readyToDestroy) => _barPanel.Remove(view, readyToDestroy.Invoke);

        protected override void InitViewOnBinding(CapturableArea area, UISapperActivityAreaProgressBar view)
        {
            view.AreaIndex = _areaIndexProvider.Get(area.Id);

            ICaptureProgressReceiver progressReceiver = view;
            progressReceiver.MaxProgress = area.RequiredCapturePoints;
            progressReceiver.CurrentProgress = area.CurrentCapturePoints;

            ICaptureSpeedReceiver speedReceiver = view;
            speedReceiver.CaptureSpeed = area.InvadersCount;
        }
    }
}