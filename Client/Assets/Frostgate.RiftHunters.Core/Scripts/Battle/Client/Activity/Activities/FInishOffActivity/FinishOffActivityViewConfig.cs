﻿using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.FinishOffActivity;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    [CreateAssetMenu(fileName = nameof(FinishOffActivityViewConfig),
        menuName = ActivityAssetMenu.Menu + "/" + nameof(FinishOffActivityViewConfig))]
    public sealed class FinishOffActivityViewConfig : ActivityViewConfig<UIActivityPanelItem, UIActivityGoal>
    {
    }
}