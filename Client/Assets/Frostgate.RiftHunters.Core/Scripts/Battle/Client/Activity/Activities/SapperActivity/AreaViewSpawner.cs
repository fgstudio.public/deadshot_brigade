﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    public sealed class AreaViewSpawner : AreaViewSpawnerBase<CapturableAreaView>
    {
        [NotNull] private readonly IAreaMarkerPanel _markerPanel;

        public AreaViewSpawner([NotNull] CapturableAreaView viewPrefab, [NotNull] IAreaMarkerPanel markerPanel,
            [NotNull] ILogger<AreaViewSpawner> logger) : base(viewPrefab, logger)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(markerPanel, nameof(markerPanel));

            _markerPanel = markerPanel;
        }

        protected override void OnViewSpawned(CapturableArea area, CapturableAreaView view) =>
            _markerPanel.Add(area.Id, view.MarkerTransform);

        protected override void OnViewWillBeDestroyed(CapturableArea _, CapturableAreaView view,
            Action readyToDestroy)
        {
            _markerPanel.Remove(view.MarkerTransform);
            view.Hide(readyToDestroy.Invoke);
        }

        protected override void InitViewOnBinding(CapturableArea area, CapturableAreaView view)
        {
            view.Radius = area.Radius;
            view.CaptureMaxProgress = area.RequiredCapturePoints;
            view.CaptureProgress = area.CurrentCapturePoints;
            view.IndicatorState = area.IsInvaderCaught
                ? CapturingIndicator.IndicatorState.Captured
                : CapturingIndicator.IndicatorState.Uncaptured;
            view.CaptureSpeed = area.InvadersCount;
        }
    }
}