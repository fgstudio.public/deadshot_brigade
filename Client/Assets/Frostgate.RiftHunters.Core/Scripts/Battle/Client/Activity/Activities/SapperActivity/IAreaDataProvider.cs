﻿namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    public interface IAreaDataProvider<out TData>
    {
        public TData Get(string areaId);
    }
}