﻿using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    public sealed class SapperActivityUIHandler : ClientActivityUIHandler<UIActivityPanelItem, IUIActivityGoal>
    {
        [NotNull] private readonly IReadOnlySapperTaskState _taskState;
        [NotNull] private readonly SapperActivityViewConfig _viewConfig;
        
        private SapperActivityModelProgressAdapter _progressAdapter;
        private UIActivityProgressiveGoal _goal;

        public SapperActivityUIHandler([NotNull] IReadOnlySapperTaskState taskState,
            [NotNull] IUIActivityPanel activityPanel,
            [NotNull] SapperActivityViewConfig viewConfig) : base(taskState, activityPanel, viewConfig)
        {
            _taskState = taskState;
            _viewConfig = viewConfig;
        }

        protected override void OnBeforeAdded(UIActivityPanelItem panelItem)
        {
            base.OnBeforeAdded(panelItem);

            CreateUIGoalIfNull();
            panelItem.AddGoal(_goal);
        }

        protected override void OnBeforeDestroyed(UIActivityPanelItem panelItem)
        {
            base.OnBeforeDestroyed(panelItem);
            
            panelItem.RemoveGoal(_goal);
            DisposeUIGoalIfNotNull();
        }

        private void CreateUIGoalIfNull()
        {
            _progressAdapter = new SapperActivityModelProgressAdapter(_taskState);
            _goal = Object.Instantiate(_viewConfig.UICaptureMinesGoalPrefab);
            _goal.SetData(_viewConfig.CaptureMinesGoal);

            _goal.Progress.Progress = _progressAdapter;
        }

        private void DisposeUIGoalIfNotNull()
        {
            if (_goal == null)
                return;

            _goal.Progress.Progress = null;
            
            _progressAdapter.Dispose();
            _progressAdapter = null;
        }
    }
}