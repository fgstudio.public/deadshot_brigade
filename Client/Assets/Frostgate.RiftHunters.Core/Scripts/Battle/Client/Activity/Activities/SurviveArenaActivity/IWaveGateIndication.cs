﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity;
using JetBrains.Annotations;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    public interface IWaveGateIndication : IDisposable
    {
        void StartIndication([NotNull] WaveGate gate, TimeSpan time);
        void StopIndication([NotNull] WaveGate gate);
    }
}