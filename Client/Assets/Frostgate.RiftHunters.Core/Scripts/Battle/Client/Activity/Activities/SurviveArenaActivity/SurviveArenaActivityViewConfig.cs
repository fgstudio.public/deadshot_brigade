﻿using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities
{
    [CreateAssetMenu(fileName = nameof(SurviveArenaActivityViewConfig),
        menuName = ActivityAssetMenu.Menu + "/" + nameof(SurviveArenaActivityViewConfig))]
    public sealed class SurviveArenaActivityViewConfig : ActivityViewConfig<UIActivityPanelItem, UIActivityGoal>
    {
        [SerializeField, Required, AssetSelector, FoldoutGroup("References")]
        private Sprite _pointerIcon;

        [SerializeField, Required, AssetSelector, FoldoutGroup("References")]
        private WaveTimeIndicator _timeIndicatorPrefab;

        [SerializeField, Required, AssetSelector, FoldoutGroup("References")]
        private UIWaveTimeIndicator _uiTimeIndicatorPrefab;

        [SerializeField, FoldoutGroup("Settings")]
        private Color _pointerBackgroundColor;

        [SerializeField, MinValue(0), FoldoutGroup("Settings")]
        private int _indicatorPoolMaxCount;

        [SerializeField, MinValue(0), FoldoutGroup("Settings")]
        private int _indicatorPoolInitialCount;

        [NotNull] public Sprite PointerIcon => _pointerIcon;
        [NotNull] public WaveTimeIndicator TimeIndicatorPrefab => _timeIndicatorPrefab;
        [NotNull] public UIWaveTimeIndicator UITimeIndicatorPrefab => _uiTimeIndicatorPrefab;

        public Color PointerBackgroundColor => _pointerBackgroundColor;

        public int IndicatorPoolMaxCount => _indicatorPoolMaxCount;
        public int IndicatorPoolInitialCount => _indicatorPoolInitialCount;
    }
}