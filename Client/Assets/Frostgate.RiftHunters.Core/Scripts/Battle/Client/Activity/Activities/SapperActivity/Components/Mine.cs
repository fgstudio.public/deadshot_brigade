﻿using System;
using DG.Tweening;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(SapperActivity) + nameof(Mine))]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(MeshRenderer))]
    public sealed class Mine : MonoBehaviour
    {
        [SerializeField, Required, FoldoutGroup("References")]
        private MeshRenderer _mesh;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private GameObject _blinkVfx;

        [SerializeField, FoldoutGroup("Settings")]
        private FadeSettings _fadeSettings;

        private Fader<MeshRenderer> _meshFader;

        private void OnValidate() => DiscoverReferences();

        [Button]
        private void Reset()
        {
            _mesh = null;
            _blinkVfx = null;
            _fadeSettings = new();
        }

        private void Awake()
        {
            _meshFader = new Fader<MeshRenderer>(_mesh, (m, alpha, duration) => m.material.DOFade(alpha, duration),
                _fadeSettings);
            DiscoverReferences();
        }

        [Button]
        public void Show([CanBeNull] Action completeCallback = null) =>
            _meshFader.SetFaded(false, () =>
            {
                _blinkVfx.SetActive(true);
                completeCallback?.Invoke();
            });

        [Button]
        public void Hide([CanBeNull] Action completeCallback = null)
        {
            _meshFader.SetFaded(true, () => completeCallback?.Invoke());
            _blinkVfx.SetActive(false);
        }


        private void DiscoverReferences()
        {
            _mesh ??= GetComponent<MeshRenderer>();
            _blinkVfx = transform.GetChild(0).gameObject;
        }
    }
}