﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using JetBrains.Annotations;
using System;
using Frostgate.RiftHunters.Core.Battle.Client.Targeting;
using Frostgate.RiftHunters.Core.UI.Hud;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.CartEscortActivity
{
    public sealed class CartEscortActivityClientHandler : IClientActivityHandler
    {
        [NotNull] private readonly IReadOnlyCartEscortTaskState _taskState;
        [NotNull] private readonly CartEscortCapturableArea _capturableArea;
        [NotNull] private readonly RailCartView _cartView;
        [NotNull] private readonly TargetMarkerComponent _targetMarker;
        [NotNull] private readonly ITargetPanel _targetPanel;

        private bool _isDisposed;

        public CartEscortActivityClientHandler(
            [NotNull] CartEscortTaskSceneData taskSceneData,
            [NotNull] ITargetPanel targetPanel)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData, nameof(taskSceneData));
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData.CapturableArea,
                nameof(taskSceneData.CapturableArea));
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData.TaskState, nameof(taskSceneData.TaskState));
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData.Cart, nameof(taskSceneData.Cart));

            _taskState = taskSceneData.TaskState;
            _capturableArea = taskSceneData.CapturableArea;
            _cartView = taskSceneData.Cart.View;
            _targetMarker = taskSceneData.TargetMarker;
            _targetPanel = targetPanel;

            InitActivityObjects();

            Subscribe();
            if (_taskState.Status == ActivityStatus.Active)
                OnActivityActivated();
        }

        public void Dispose()
        {
            ThrowIfDisposed();
            _isDisposed = true;
            Unsubscribe();
        }

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{nameof(CartEscortActivityClientHandler)} already disposed.");
        }

        private void Subscribe()
        {
            _taskState.Activated.AddListener(OnActivityActivated);
            _taskState.Deactivated.AddListener(OnActivityDeactivated);
            _taskState.Completed.AddListener(OnActivityCompleted);
        }

        private void Unsubscribe()
        {
            _taskState.Activated.RemoveListener(OnActivityActivated);
            _taskState.Deactivated.RemoveListener(OnActivityDeactivated);
            _taskState.Completed.RemoveListener(OnActivityCompleted);
        }

        private void OnActivityActivated()
        {
            _capturableArea.View.Show();
            _targetMarker.Enable();
            _targetPanel.SetTarget(_targetMarker.MarkerPoint, _targetMarker.Icon, _targetMarker.BackgroundColor,
                _targetMarker.AnimationIsOn);

            _capturableArea.Capturing.Catcher.Caught.AddListener(OnAreaInvaderCaught);
            _capturableArea.Capturing.Catcher.Lost.AddListener(OnAreaInvaderLost);

            UpdateMovementIndicator();
        }

        private void OnActivityDeactivated()
        {
            _capturableArea.Capturing.Catcher.Caught.RemoveListener(OnAreaInvaderCaught);
            _capturableArea.Capturing.Catcher.Lost.RemoveListener(OnAreaInvaderLost);

            _targetMarker.Disable();
            _targetPanel.RemoveTarget(_targetMarker.MarkerPoint);

            _capturableArea.View.Hide();
        }

        private void OnActivityCompleted()
        {
            Unsubscribe();

            _capturableArea.Capturing.Catcher.Caught.RemoveListener(OnAreaInvaderCaught);
            _capturableArea.Capturing.Catcher.Lost.RemoveListener(OnAreaInvaderLost);

            _targetMarker.Disable();
            _targetPanel.RemoveTarget(_targetMarker.MarkerPoint);

            _capturableArea.View.Hide();

            _cartView.MovementIndicator.DisableIndication();
        }

        private void OnAreaInvaderCaught(Collider collider)
        {
            UpdateMovementIndicator();
            UpdateCapturableViewState();
        }

        private void OnAreaInvaderLost(Collider collider)
        {
            UpdateMovementIndicator();
            UpdateCapturableViewState();
        }

        private void UpdateMovementIndicator()
        {
            _cartView.MovementIndicator.InvadersCount = _capturableArea.Capturing.Catcher.Objects.Count;
        }

        private void UpdateCapturableViewState()
        {
            int objCount = _capturableArea.Capturing.Catcher.Objects.Count;
            CartCapturableAreaView view = _capturableArea.View;

            view.State = objCount == 0
                ? CartCapturableAreaView.ViewState.Inactive
                : CartCapturableAreaView.ViewState.Active;
        }

        private void InitActivityObjects()
        {
            _cartView.MovementIndicator.DisableIndication();
            _cartView.SetActive(true);

            _capturableArea.View.Hide();

            _capturableArea.View.Radius = _taskState.Config.CartCapturableAreaRadius;
        }
    }
}