﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(SapperActivity) + nameof(CapturableAreaView))]
    [DisallowMultipleComponent]
    public sealed class CapturableAreaView : MonoBehaviour, ICapturedReceiver, ICaptureSpeedReceiver,
        ICaptureProgressReceiver
    {
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Transform _markerTransform;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private CapturingIndicator _capturingIndicator;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Mine _mine;


        [ShowInInspector, FoldoutGroup("Info")]
        public CapturingIndicator.IndicatorState IndicatorState
        {
            get => _capturingIndicator.State;
            set => _capturingIndicator.State = value;
        }

        [ShowInInspector, FoldoutGroup("Info")]
        public float CaptureMaxProgress
        {
            get => _capturingIndicator.CaptureMaxProgress;
            set => _capturingIndicator.CaptureMaxProgress = value;
        }

        [ShowInInspector, FoldoutGroup("Info")]
        public float CaptureProgress
        {
            get => _capturingIndicator.CaptureProgress;
            set => _capturingIndicator.CaptureProgress = value;
        }

        [ShowInInspector, FoldoutGroup("Info")]
        public int CaptureSpeed
        {
            get => _capturingIndicator.CaptureSpeed;
            set => _capturingIndicator.CaptureSpeed = value;
        }

        [ShowInInspector, FoldoutGroup("Info")]
        public float Radius
        {
            get => _capturingIndicator.Radius;
            set => _capturingIndicator.Radius = value;
        }

        public Transform MarkerTransform => _markerTransform;


        bool ICapturedReceiver.IsCaptured
        {
            set => IndicatorState = value
                ? CapturingIndicator.IndicatorState.Captured
                : CapturingIndicator.IndicatorState.Uncaptured;
        }

        float ICaptureProgressReceiver.MaxProgress
        {
            set => CaptureMaxProgress = value;
        }

        float ICaptureProgressReceiver.CurrentProgress
        {
            set => CaptureProgress = value;
        }

        private void OnValidate() => DiscoverReferences();

        [Button]
        private void Reset()
        {
            _capturingIndicator = null;
            _mine = null;
        }

        [Button]
        public void Show([CanBeNull] Action completeCallback = null)
        {
            _capturingIndicator.Show();
            _mine.Show(completeCallback);
        }

        [Button]
        public void Hide([CanBeNull] Action completeCallback = null)
        {
            _capturingIndicator.Hide();
            _mine.Hide(completeCallback);
        }

        private void DiscoverReferences()
        {
            _capturingIndicator ??= GetComponentInChildren<CapturingIndicator>();
            _mine ??= GetComponentInChildren<Mine>();
        }
    }
}