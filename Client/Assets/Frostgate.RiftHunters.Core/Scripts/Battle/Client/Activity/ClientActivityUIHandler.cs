﻿using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using System;
using System.Threading;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    // TODO: Надо переделать инициализацию активностей на клиенте для избавления от delay-костылей
    public class ClientActivityUIHandler<TPanelItem, TPanelGoal> : IClientActivityHandler
        where TPanelItem : IUIActivityPanelItem
        where TPanelGoal : IUIActivityGoal
    {
        [NotNull] private readonly IReadOnlyActivityTaskState _taskState;
        [NotNull] private readonly IUIActivityPanel _activityPanel;
        [NotNull] private readonly IActivityViewConfig<TPanelItem, TPanelGoal> _viewConfig;

        [NotNull] private readonly CancellationTokenSource _cts;

        private bool _isDisposed;

        protected TPanelItem PanelItem { get; private set; }

        public ClientActivityUIHandler([NotNull] IReadOnlyActivityTaskState taskState, [NotNull] IUIActivityPanel activityPanel,
            [NotNull] IActivityViewConfig<TPanelItem, TPanelGoal> viewConfig)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskState, nameof(taskState));
            ThrowHelper.ThrowIfArgumentNullOrDefault(activityPanel, nameof(activityPanel));
            ThrowHelper.ThrowIfArgumentNullOrDefault(viewConfig, nameof(viewConfig));

            _taskState = taskState;
            _activityPanel = activityPanel;
            _viewConfig = viewConfig;

            _cts = new CancellationTokenSource();

            // TODO: Delay-костыль
            if (taskState.Status != ActivityStatus.Completed)
                UniTask.Delay(500, cancellationToken: _cts.Token)
                    .ContinueWith(() =>
                    {
                        Subscribe(_taskState);
                        if (_taskState.Status == ActivityStatus.Active)
                            OnActivityActivated();
                    }).SuppressCancellationThrow();
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            _cts.Cancel();
            _cts.Dispose();

            Unsubscribe(_taskState);

            Dispose(true);
            _isDisposed = true;
        }

        protected virtual void OnBeforeAdded(TPanelItem panelItem)
        {
        }

        protected virtual void OnBeforeDestroyed(TPanelItem panelItem)
        {
        }

        protected virtual void Dispose(bool dispose)
        {
        }

        protected virtual void OnActivated()
        {
        }

        protected virtual void OnDeactivated()
        {
        }

        protected virtual void OnCompleted()
        {
        }

        private void Subscribe(IReadOnlyActivityTaskState taskState)
        {
            taskState.Activated.AddListener(OnActivityActivated);
            taskState.Deactivated.AddListener(OnActivityDeactivated);
            taskState.Completed.AddListener(OnActivityCompleted);
        }

        private void Unsubscribe(IReadOnlyActivityTaskState taskState)
        {
            taskState.Activated.RemoveListener(OnActivityActivated);
            taskState.Deactivated.RemoveListener(OnActivityDeactivated);
            taskState.Completed.RemoveListener(OnActivityCompleted);
        }

        private async void OnActivityActivated()
        {
            // TODO: Костыль на паузу для игнорирования плашек завершённых заданий
            CancellationToken token = _cts.Token;
            await UniTask.Delay(TimeSpan.FromSeconds(0.1f), cancellationToken: token)
                .SuppressCancellationThrow();

            if (token.IsCancellationRequested) return;
            if (_taskState.Status != ActivityStatus.Active) return;

            InstantiatePanelItemIfNull();
            SetActivityDescription();

            OnBeforeAdded(PanelItem);
            _activityPanel.PopUpPlayer.Play(_viewConfig.InPopUpDescription);
            _activityPanel.Add(PanelItem);

            OnActivated();
        }

        private void OnActivityDeactivated()
        {
            CancellationToken token = _cts.Token;
            if (token.IsCancellationRequested) return;
            if (_taskState.Status != ActivityStatus.Inactive) return;

            OnBeforeDestroyed(PanelItem);

            _activityPanel.Remove(PanelItem);
            DestroyPanelItem();

            OnDeactivated();
        }

        private async void OnActivityCompleted()
        {
            CancellationToken token = _cts.Token;
            if (_taskState.Status != ActivityStatus.Completed) return;
            if (token.IsCancellationRequested || PanelItem?.GameObject == null) return;

            Unsubscribe(_taskState);

            PanelItem.SetCompleted();
            OnCompleted();

            //TODO-SG: Костыль на паузу беред фейдом плашки
            await UniTask.Delay(TimeSpan.FromSeconds(3), cancellationToken: token)
                .SuppressCancellationThrow();

            if (token.IsCancellationRequested || PanelItem?.GameObject == null)
                return;

            OnBeforeDestroyed(PanelItem);
            _activityPanel.Remove(PanelItem, DestroyPanelItem);
        }

        private void InstantiatePanelItemIfNull()
        {
            PanelItem = Object.Instantiate(_viewConfig.PanelItemPrefab.GameObject).GetComponent<TPanelItem>();
        }

        private void SetActivityDescription()
        {
            PanelItem.SetTitle(_viewConfig.InPanelDescription.Title);
            PanelItem.AddGoals(_viewConfig.InPanelDescription.Goals);
        }

        private void DestroyPanelItem() => Object.Destroy(PanelItem.GameObject);

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{GetType().Name} already disposed.");
        }
    }
}