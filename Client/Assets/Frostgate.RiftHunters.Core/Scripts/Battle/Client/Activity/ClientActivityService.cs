﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    public sealed class ClientActivityService : IDisposable
    {
        private readonly IClientActivityHandlerFactory _handlerFactory;
        private readonly List<ClientActivity> _activities;
        private readonly CancellationTokenSource _cts;
        private bool _isDisposed;

        public ClientActivityService(
            [NotNull] IEnumerable<ActivityTaskSceneData> taskData,
            [NotNull] IClientActivityHandlerFactory handlerFactory)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskData, nameof(taskData));
            ThrowHelper.ThrowIfArgumentNullOrDefault(handlerFactory, nameof(handlerFactory));

            _handlerFactory = handlerFactory;
            _cts = new CancellationTokenSource();

            ActivityTaskSceneData[] taskSceneDatas = taskData.ToArray();
            _activities = new List<ClientActivity>(taskSceneDatas.Length);

            foreach (ActivityTaskSceneData taskSceneData in taskSceneDatas)
                if (taskSceneData.State.IsInitialized)
                    AddClientActivity(taskSceneData);
                else
                    UniTask.WaitUntil(() => taskSceneData.State.IsInitialized, cancellationToken: _cts.Token)
                        .ContinueWith(() => AddClientActivity(taskSceneData))
                        .SuppressCancellationThrow();
        }

        public void Dispose()
        {
            if (_isDisposed)
                throw new ObjectDisposedException(
                    $"{nameof(ClientActivityService)} already disposed.");

            _activities.ForEach(e => e.Dispose());
            _cts.Cancel();
            _cts.Dispose();

            _isDisposed = true;
        }

        private void AddClientActivity(ActivityTaskSceneData taskSceneData)
        {
            IClientActivityHandler handler = _handlerFactory.Create(taskSceneData);
            ClientActivity clientActivity = new(handler);
            _activities.Add(clientActivity);
        }
    }
}