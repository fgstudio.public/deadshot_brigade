﻿using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.ClearArenaActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.CartEscortActivity;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.MoveToPointActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.MoveToPointActivity;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.Activities.SapperActivity;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.FinishOffActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.KillUnitsActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    public sealed class ClientActivityHandlingFactory : IClientActivityHandlerFactory
    {
        [NotNull] private readonly IUIBattleMediator _battleMediator;
        [NotNull] private readonly IReadOnlyBotSpawnState _spawnState;
        [NotNull] private readonly IReadOnlyActivityRelatedViewConfigRoster _roster;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;

        public ClientActivityHandlingFactory(
            [NotNull] IReadOnlyActivityRelatedViewConfigRoster roster,
            [NotNull] IReadOnlyBotSpawnState spawnState,
            [NotNull] IUIBattleMediator battleMediator,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(roster, nameof(roster));
            ThrowHelper.ThrowIfArgumentNullOrDefault(spawnState, nameof(spawnState));
            ThrowHelper.ThrowIfArgumentNullOrDefault(battleMediator, nameof(battleMediator));

            _roster = roster;
            _spawnState = spawnState;
            _battleMediator = battleMediator;
            _unitCollection = unitCollection;
        }

        public IClientActivityHandler Create(ActivityTaskSceneData taskSceneData)
        {
            return taskSceneData switch
            {
                ClearArenaTaskSceneData clearArenaData => CreateClearArenaActivityHandler(clearArenaData),
                FinishOffTaskSceneData finishOffData => CreateFinishOffActivityHandler(finishOffData),
                KillUnitsTaskSceneData killUnitsData => CreateKillUnitsActivityHandler(killUnitsData),
                SurviveArenaTaskSceneData surviveData => CreateSurviveArenaActivityHandler(surviveData),
                CartEscortTaskSceneData escortData => CreateCartEscortActivityHandler(escortData),
                SapperTaskSceneData sapperData => CreateSapperActivityHandler(sapperData),
                MoveToPointTaskSceneData moveToPointData => CreateMoveToPointActivityHandler(moveToPointData),

                _ => throw new NotImplementedException(
                    $"Client handler for '{taskSceneData.GetType().Name}' not implemented.")
            };
        }

        private IClientActivityHandler CreateClearArenaActivityHandler(ClearArenaTaskSceneData taskSceneData)
        {
            var viewConfig = FindActivityViewConfig<ClearArenaActivityViewConfig>(taskSceneData.TaskState);

            var uiHandler =
                new ClientActivityUIHandler<IUIActivityPanelItem, IUIActivityGoal>(taskSceneData.TaskState,
                    _battleMediator.Hud.ActivityPanel, viewConfig);
            var handler = new ClearArenaActivityClientHandler(taskSceneData);

            return new CompositeClientActivityHandler(uiHandler, handler);
        }

        private IClientActivityHandler CreateFinishOffActivityHandler(FinishOffTaskSceneData taskSceneData)
        {
            var viewConfig = FindActivityViewConfig<FinishOffActivityViewConfig>(taskSceneData.TaskState);

            var uiHandler =
                new ClientActivityUIHandler<IUIActivityPanelItem, IUIActivityGoal>(taskSceneData.TaskState,
                    _battleMediator.Hud.ActivityPanel, viewConfig);
            var handler = new FinishOffActivityClientHandler(taskSceneData);

            return new CompositeClientActivityHandler(uiHandler, handler);
        }

        private IClientActivityHandler CreateKillUnitsActivityHandler(KillUnitsTaskSceneData taskSceneData)
        {
            var viewConfig = FindActivityViewConfig<KillUnitsActivityViewConfig>(taskSceneData.TaskState);

            var uiHandler =
                new KillUnitsActivityClientUIHandler(taskSceneData,
                    _battleMediator.Hud.ActivityDescription, _battleMediator.Hud.ActivityPanel, viewConfig, _unitCollection,
                    _battleMediator.Hud.PointerPanels.TargetPanel);
            var handler = new KillUnitsActivityClientHandler(taskSceneData);

            return new CompositeClientActivityHandler(uiHandler, handler);
        }

        private IClientActivityHandler CreateSurviveArenaActivityHandler(SurviveArenaTaskSceneData taskSceneData)
        {
            var viewConfig = FindActivityViewConfig<SurviveArenaActivityViewConfig>(taskSceneData.TaskState);


            var gateIndication =
                new WaveGateIndication(_battleMediator.Hud.PointerPanels.MultiplePointerPanel, _battleMediator.BarPanel, viewConfig);

            var uiHandler =
                new SurviveArenaActivityClientUIHandler(taskSceneData.TaskState, _battleMediator.Hud.ActivityPanel,
                    viewConfig, _spawnState, taskSceneData.Gates, gateIndication);

            var handler = new SurviveArenaActivityClientHandler(taskSceneData, _spawnState);

            return new CompositeClientActivityHandler(uiHandler, handler);
        }

        private IClientActivityHandler CreateCartEscortActivityHandler(CartEscortTaskSceneData taskSceneData)
        {
            var viewConfig = FindActivityViewConfig<CartEscortActivityViewConfig>(taskSceneData.TaskState);

            var uiHandler =
                new ClientActivityUIHandler<IUIActivityPanelItem, IUIActivityGoal>(taskSceneData.TaskState,
                    _battleMediator.Hud.ActivityPanel, viewConfig);
            var handler = new CartEscortActivityClientHandler(taskSceneData, _battleMediator.Hud.PointerPanels.TargetPanel);

            return new CompositeClientActivityHandler(uiHandler, handler);
        }

        private IClientActivityHandler CreateSapperActivityHandler(SapperTaskSceneData references)
        {
            var viewConfig = FindActivityViewConfig<SapperActivityViewConfig>(references.TaskState);

            var uiHandler = new SapperActivityUIHandler(references.TaskState, _battleMediator.Hud.ActivityPanel,
                viewConfig);

            var areaPanel = new AreaMarkerPanel(_battleMediator.Hud.PointerPanels.TargetPanel,
                viewConfig, viewConfig.MarkerColor);

            var areaViewSpawner = new AreaViewSpawner(viewConfig.AreaViewPrefab, areaPanel,
                LoggerFactory.CreateLogger<AreaViewSpawner>());
            var areaProgressBarSpawner = new AreaProgressBarSpawner(viewConfig.AreaProgressBarPrefab, viewConfig,
                _battleMediator.BarPanel, LoggerFactory.CreateLogger<AreaProgressBarSpawner>());

            var compositeViewSpawner = new AreaCompositeViewSpawner(areaViewSpawner, areaProgressBarSpawner);
            var handler =
                new SapperActivityClientHandler(references.TaskState, references.AreaRoster, compositeViewSpawner);

            return new CompositeClientActivityHandler(uiHandler, handler);
        }

        private IClientActivityHandler CreateMoveToPointActivityHandler(MoveToPointTaskSceneData taskSceneData)
        {
            var viewConfig = FindActivityViewConfig<MoveToPointActivityViewConfig>(taskSceneData.TaskState);

            var uiHandler =
                new ClientActivityUIHandler<IUIActivityPanelItem, IUIActivityGoal>(taskSceneData.TaskState,
                    _battleMediator.Hud.ActivityPanel, viewConfig);
            var handler = new MoveToPointActivityClientHandler(taskSceneData, _battleMediator.Hud.PointerPanels.TargetPanel);

            return new CompositeClientActivityHandler(uiHandler, handler);
        }

        private TConfig FindActivityViewConfig<TConfig>(IReadOnlyActivityTaskState<ActivityTaskConfig> taskState)
            where TConfig : ActivityViewConfig
        {
            if (!_roster.TryGetViewConfig(taskState.Config.Id, out ActivityViewConfig viewConfig))
                throw new ArgumentOutOfRangeException($"ViewConfig for activity id '{taskState.Config.Id}' not found.");

            return (TConfig)viewConfig;
        }
    }
}