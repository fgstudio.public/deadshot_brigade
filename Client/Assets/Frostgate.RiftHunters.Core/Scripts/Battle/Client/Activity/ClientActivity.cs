﻿using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    public sealed class ClientActivity : IDisposable
    {
        [NotNull] private readonly IClientActivityHandler _handler;

        private bool _isDisposed;

        public ClientActivity([NotNull] IClientActivityHandler handler)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(handler, nameof(handler));

            _handler = handler;
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            _handler.Dispose();
            _isDisposed = true;
        }

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{nameof(ClientActivity)} already disposed.");
        }
    }
}