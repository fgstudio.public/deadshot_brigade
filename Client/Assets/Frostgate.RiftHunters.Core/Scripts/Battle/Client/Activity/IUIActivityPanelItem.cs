﻿using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Frostgate.RiftHunters.Core.UI;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    public interface IUIActivityPanelItem : IUIBarPanelItem
    {
        void SetCompleted();
        ActivityDescription BuildDescription();
        void SetTitle(string title);
        void AddGoals(IEnumerable<ActivityGoal> goals);
        void AddGoal(ActivityGoal goal);
        void RemoveGoals(IEnumerable<ActivityGoal> goals);
        void RemoveGoal(ActivityGoal goal);

        void AddGoal([NotNull] IUIActivityGoal goal);
        void RemoveGoal([NotNull] IUIActivityGoal goal);
    }
}