﻿namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    public sealed class CompositeClientActivityHandler : IClientActivityHandler
    {
        private readonly IClientActivityHandler[] _handlers;

        public CompositeClientActivityHandler(params IClientActivityHandler[] handlers)
        {
            _handlers = handlers;
        }

        public void Dispose() => _handlers.ForEach(h => h.Dispose());
    }
}