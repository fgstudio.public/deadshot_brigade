﻿using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    public interface IReadOnlyActivityRelatedViewConfigRoster
    {
        IReadOnlyList<ActivityRelatedViewConfig> Configs { get; }
        bool TryGetViewConfig(string activityId, out ActivityViewConfig viewConfig);
    }
}