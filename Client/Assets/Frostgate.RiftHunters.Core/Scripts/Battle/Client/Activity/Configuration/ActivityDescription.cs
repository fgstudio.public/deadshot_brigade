﻿using UnityEngine;
using Sirenix.OdinInspector;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    [Serializable]
    public struct ActivityDescription
    {
        [SerializeField, Required] private string _title;
        [SerializeField, Required] private ActivityGoal[] _goals;

        public string Title => _title;
        public ActivityGoal[] Goals => _goals ??= Array.Empty<ActivityGoal>();

        public ActivityDescription(string title, IEnumerable<ActivityGoal> goals)
        {
            _title = title;
            _goals = goals.ToArray();
        }
    }
}