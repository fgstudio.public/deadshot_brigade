﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    [CreateAssetMenu(fileName = nameof(ActivityRelatedViewConfigRoster),
        menuName = ActivityAssetMenu.Menu + "/" + nameof(ActivityRelatedViewConfigRoster))]
    public sealed class ActivityRelatedViewConfigRoster : ScriptableConfig, IReadOnlyActivityRelatedViewConfigRoster
    {
        // TODO: Валидация на повторы и наличие всех необходимых элементов
        [field: SerializeField] public ActivityRelatedViewConfig[] Configs { get; private set; } = Array.Empty<ActivityRelatedViewConfig>();

        IReadOnlyList<ActivityRelatedViewConfig> IReadOnlyActivityRelatedViewConfigRoster.Configs => Configs;


        public bool TryGetViewConfig(string activityId, out ActivityViewConfig viewConfig)
        {
            viewConfig = null;
            foreach (ActivityRelatedViewConfig r in Configs)
            {
                if (r.ActivityConfig.Id == activityId)
                {
                    viewConfig = r.ViewConfig;
                    break;
                }
            }

            return viewConfig != null;
        }

        [Button]
        private void Reset()
        {
            Configs = Array.Empty<ActivityRelatedViewConfig>();
        }
    }
}