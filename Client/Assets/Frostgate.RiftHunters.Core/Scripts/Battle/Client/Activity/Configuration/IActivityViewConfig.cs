﻿using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    public interface IActivityViewConfig
    {
        ActivityDescription InPanelDescription { get; }
        ActivityDescription InPopUpDescription { get; }
    }

    public interface IActivityViewConfig<out TPanelItem, out TPanelGoal> : IActivityViewConfig
        where TPanelItem : IUIActivityPanelItem
        where TPanelGoal : IUIActivityGoal
    {
        TPanelItem PanelItemPrefab { get; }
        TPanelGoal PanelItemGoalPrefab { get; }
    }
}