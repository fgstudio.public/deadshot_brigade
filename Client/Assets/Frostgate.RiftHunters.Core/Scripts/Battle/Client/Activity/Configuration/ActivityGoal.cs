﻿using Sirenix.OdinInspector;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    [Serializable]
    public struct ActivityGoal
    {
        [SerializeField, Required] private string _title;

        [SerializeField, Required, LabelText("Enable Marker"), Title(""), Space(-20)]
        private bool _isMarkerEnabled;

        [SerializeField, ShowIf(nameof(_isMarkerEnabled)), LabelText("Background Color")]
        private Color _markerBgColor;

        [SerializeField, AssetSelector, ShowIf(nameof(_isMarkerEnabled))]
        private Sprite _markerSprite;

        public string Title => _title;
        public bool EnableMarker => _isMarkerEnabled;
        public Color MarkerBgColor => _markerBgColor;
        public Sprite MarkerSprite => _markerSprite;
    }
}