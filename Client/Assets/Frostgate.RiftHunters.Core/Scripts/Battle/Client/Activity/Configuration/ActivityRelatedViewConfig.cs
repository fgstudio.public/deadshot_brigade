﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Sirenix.OdinInspector;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    [Serializable]
    public sealed class ActivityRelatedViewConfig
    {
        [field: SerializeField, Required, AssetSelector]
        public ActivityTaskConfig ActivityConfig { get; private set; }

        [field: SerializeField, Required, AssetSelector]
        public ActivityViewConfig ViewConfig { get; private set; }
    }
}