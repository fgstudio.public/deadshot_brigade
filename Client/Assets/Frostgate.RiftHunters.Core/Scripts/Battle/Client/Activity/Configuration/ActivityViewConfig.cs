﻿using Frostgate.RiftHunters.Core.Battle.Client.Activity.UI;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Activity
{
    public abstract class ActivityViewConfig : ScriptableConfig, IActivityViewConfig
    {
        [SerializeField, TitleGroup("UI Description"), HorizontalGroup("UI Description/Control")]
        private bool _overridePopUp;

        [SerializeField, TabGroup("UI Description/Data", "Panel"), HideLabel]
        private ActivityDescription _inPanelDescription;

        [SerializeField, TabGroup("UI Description/Data", "Pop-up")] 
        [EnableIf(nameof(_overridePopUp), true), HideLabel]
        private ActivityDescription _inPopUpDescription;

        public ActivityDescription InPanelDescription => _inPanelDescription;
        public ActivityDescription InPopUpDescription => _overridePopUp ? _inPopUpDescription : _inPanelDescription;

        [Button(Name = "Sync"), HorizontalGroup("UI Description/Control")]
        private void SyncDescriptions()
        {
            _inPopUpDescription = new ActivityDescription(_inPanelDescription.Title, _inPanelDescription.Goals);
        }
    }

    public abstract class ActivityViewConfig<TPanelItem, TPanelGoal> : ActivityViewConfig, IActivityViewConfig<TPanelItem, TPanelGoal>
        where TPanelItem : MonoBehaviour, IUIActivityPanelItem
        where TPanelGoal : MonoBehaviour, IUIActivityGoal
    {
        [SerializeField, AssetSelector, Required, TabGroup("UI Description/Data", "Panel")]
        [Title("Prefabs")]
        private TPanelItem _panelItemPrefab;

        [SerializeField, AssetSelector, Required, TabGroup("UI Description/Data", "Panel")]
        private TPanelGoal _panelItemGoalPrefab;

        public TPanelItem PanelItemPrefab => _panelItemPrefab;

        public TPanelGoal PanelItemGoalPrefab => _panelItemGoalPrefab;
    }
}