using System;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact;

namespace Frostgate.RiftHunters.Core.Battle
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(ThrowableItem))]
    [AddComponentMenu(BattleComponentMenus.Client.Impacts.Menu + "/" +
                      nameof(ThrowableItemAsImpactObject))]
    public sealed class ThrowableItemAsImpactObject : MonoBehaviour, IImpactViewObject
    {
        [SerializeField, Required, ChildGameObjectsOnly] private ThrowableItem _throwableItem;

        public void Execute(EventData.EventType eventType,
            Vector3 sourcePosition, Vector3 targetPosition, TimeSpan duration) =>
            _throwableItem.ThrowWithDuration(sourcePosition, targetPosition, duration);

        private void OnValidate() => InitReferences();
        private void Awake() => InitReferences();

        private void InitReferences() =>
            _throwableItem ??= GetComponent<ThrowableItem>();
    }
}