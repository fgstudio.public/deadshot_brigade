using System;
using System.Collections;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle
{
    public enum ThrowType
    {
        Linear,
        Parabola
    }

    public sealed class ThrowableItem : MonoBehaviour
    {
        private const string EventsFoldoutName = "Events";

        public ThrowType Type;
        public float Speed;
        [Min(0), SuffixLabel("sec", true)] public float Delay;

        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Started { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Finished { get; private set; }

        [CanBeNull] private Coroutine _routine;

        public void ThrowWithDuration(Vector3 startPoint, Vector3 endPoint, TimeSpan duration)
        {
            var dto = new ThrowableDTO(startPoint, endPoint, duration - TimeSpan.FromSeconds(Delay));
            StartRoutine(dto);
        }

        public void ThrowWithSpeed(Vector3 startPoint, Vector3 endPoint, float speed)
        {
            var dto = new ThrowableDTO(startPoint, endPoint, speed);
            StartRoutine(dto);
        }

        public void Throw(Vector3 startPoint, Vector3 endPoint)
        {
            var dto = new ThrowableDTO(startPoint, endPoint, Speed);
            StartRoutine(dto);
        }

        private void StartRoutine(ThrowableDTO dto)
        {
            StopRoutine();

            if (Delay > 0)
                UniTask.Delay(TimeSpan.FromSeconds(Delay))
                    .ContinueWith(Start).AsAsyncUnitUniTask();
            else
                Start();

            void Start()
            {
                Started?.Invoke();
                _routine = StartCoroutine(
                    ThrowingRoutine(dto, () => Finished?.Invoke()));
            }
        }

        private void StopRoutine()
        {
            if (_routine != null)
            {
                StopCoroutine(_routine);
                _routine = null;
            }
        }

        private IEnumerator ThrowingRoutine(ThrowableDTO dto, Action callback = null)
        {
            float d = 0f;
            Transform t = transform;

            t.position = dto.StartPoint;

            while (d < dto.Distance)
            {
                d += dto.Speed * Time.deltaTime;
                t.position = CalcNewPosition(dto, d / dto.Distance);
                yield return null;
            }

            t.position = dto.EndPoint;
            callback?.Invoke();
        }

        private Vector3 CalcNewPosition(ThrowableDTO dto, float ratio) =>
            Type switch
            {
                ThrowType.Linear =>
                    Vector3.Slerp(dto.StartPoint, dto.EndPoint, ratio),

                ThrowType.Parabola =>
                    Vector3.Slerp(dto.StartPoint, dto.EndPoint, ratio)
                        .SetY(Mathf.Sin(Mathf.PI * ratio) * dto.Distance / 2f),

                _ => throw new ArgumentOutOfRangeException(nameof(ThrowType))
            };
    }
}
