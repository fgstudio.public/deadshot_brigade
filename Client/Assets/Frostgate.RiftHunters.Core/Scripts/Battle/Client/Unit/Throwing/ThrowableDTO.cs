using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle
{
    public readonly struct ThrowableDTO
    {
        public readonly Vector3 StartPoint;
        public readonly Vector3 EndPoint;
        public readonly TimeSpan Duration;
        public readonly float Distance;
        public readonly float Speed;

        public ThrowableDTO(Vector3 startPoint, Vector3 endPoint, TimeSpan duration)
        {
            Duration = duration;
            StartPoint = startPoint;
            EndPoint = endPoint;
            Distance = Vector3.Distance(startPoint, endPoint);
            Speed = (float)(Distance / duration.TotalSeconds);
        }

        public ThrowableDTO(Vector3 startPoint, Vector3 endPoint, float speed)
        {
            Speed = speed;
            StartPoint = startPoint;
            EndPoint = endPoint;
            Distance = Vector3.Distance(startPoint, endPoint);
            Duration = TimeSpan.FromSeconds(Distance / speed);
        }
    }
}
