﻿using UnityEngine;
using Frostgate.RiftHunters.Core.Components;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Server.Menu + "/" + nameof(UnitClientModule))]
    public sealed class UnitClientModule : ObjectModule<UnitNetwork>
    { }
}