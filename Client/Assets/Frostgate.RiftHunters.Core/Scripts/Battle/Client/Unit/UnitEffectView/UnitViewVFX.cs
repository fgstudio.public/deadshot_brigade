using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class UnitViewVFX : MonoBehaviour, IDisposable
    {
        protected UnitView UnitView;
        protected UnitEffectState UnitEffectState;

        public virtual void Init(UnitEffectState unitEffectState, UnitView unitView)
        {
            UnitView = unitView;
            UnitEffectState = unitEffectState;
        }

        public virtual void Dispose() => Destroy(gameObject);
    }
}