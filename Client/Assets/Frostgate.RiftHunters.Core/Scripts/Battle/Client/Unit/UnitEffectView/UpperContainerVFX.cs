using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class UpperContainerVFX : UnitViewVFX
    {
        public override void Init(UnitEffectState unitEffectState, UnitView unitView)
        {
            transform.SetParent(unitView.UpperVfxContainer);
            transform.localPosition = Vector3.zero;
        }
    }
}