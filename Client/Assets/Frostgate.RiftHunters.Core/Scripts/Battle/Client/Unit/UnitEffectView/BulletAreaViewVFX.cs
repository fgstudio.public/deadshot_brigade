using UnityEngine;
using System.Collections;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class BulletAreaViewVFX : UnitViewVFX
    {
        [SerializeField] private Transform _areaView;
        [SerializeField] private Transform _ballView;
        [SerializeField] private AnimationCurve _ballAnimation;

        public override void Init(UnitEffectState state, UnitView unitView)
        {
            base.Init(state, unitView);
            _areaView.parent = null;
            _areaView.position = UnitEffectState.Position + Vector3.forward.RotateY(UnitEffectState.RotationY) * UnitEffectState.Config.BulletAreaData.ForwardOffset;
            _areaView.eulerAngles = new Vector3(0, state.RotationY, 0);

            StartCoroutine(DoBallAnimation(state.Config.BulletAreaData.ForwardOffset));
        }

        public override void Dispose()
        {
            Destroy(_areaView.gameObject);
            Destroy(_ballView.gameObject);
            base.Dispose();
        }

        private IEnumerator DoBallAnimation(float maxDistance)
        {
            _ballView.parent = null;
            var time = 0f;
            var endTime = _ballAnimation[_ballAnimation.length - 1].time;

            while (time < endTime)
            {
                var yPosition = _ballAnimation.Evaluate(time);
                var distance = maxDistance * time / endTime;
                _ballView.position = UnitEffectState.Position + new Vector3(0, yPosition, distance).RotateY(UnitEffectState.RotationY);

                time += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            _ballView.gameObject.SetActive(false);
            _areaView.gameObject.SetActive(true);
        }
    }
}