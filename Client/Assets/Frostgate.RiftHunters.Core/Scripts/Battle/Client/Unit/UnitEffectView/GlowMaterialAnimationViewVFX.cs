using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class GlowMaterialAnimationViewVFX : UnitViewVFX
    {
        [SerializeField] private Material _material;
        [SerializeField] private float _targetGradient = 1;

        private const float MaterialAnimationSpeed = 3;

        private readonly int _gradId = Shader.PropertyToID("_Grad");
        private readonly List<Material> _materialInstances = new();
        private float _gradient;

        public override void Init(UnitEffectState unitEffectState, UnitView unitView)
        {
            base.Init(unitEffectState, unitView);

            foreach (var rnd in UnitView.Renderers)
            {
                rnd.materials = rnd.materials.Append(_material).ToArray();
                _materialInstances.Add(rnd.materials.Last());
            }

            StopAllCoroutines();

            if (gameObject.activeInHierarchy)
                StartCoroutine(Show());
        }

        public override void Dispose()
        {
            StopAllCoroutines();

            if (gameObject.activeInHierarchy)
            {
                StartCoroutine(Hide(() =>
                {
                    foreach (var rnd in UnitView.Renderers)
                        rnd.materials = rnd.materials.Where(x => !_materialInstances.Contains(x)).ToArray();

                    _materialInstances.Clear();

                    base.Dispose();
                }));
            }
            else
            {
                base.Dispose();
            }
        }

        private IEnumerator Show()
        {
            while (_gradient < _targetGradient)
            {
                _gradient += Time.deltaTime * MaterialAnimationSpeed;
                SetFloat(_gradId, _gradient);
                yield return null;
            }
        }

        private IEnumerator Hide(Action onFinish)
        {
            while (_gradient > 0)
            {
                _gradient -= Time.deltaTime * MaterialAnimationSpeed;
                SetFloat(_gradId, _gradient);
                yield return null;
            }

            onFinish?.Invoke();
        }

        private void SetFloat(int paramId, float value)
        {
            foreach (var material in _materialInstances)
                material.SetFloat(paramId, value);
        }
    }
}