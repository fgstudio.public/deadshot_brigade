using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class FireViewVFX : UnitViewVFX
    {
        [SerializeField] private ParticleSystem _particleSystemTemplate;
        [SerializeField] private Material _burntBodyMaterial;
        [SerializeField] private Color _beginBurntColor;
        [SerializeField] private Color _endBurntColor;

        private readonly List<ParticleSystem> _particleSystems = new();
        private readonly List<Material> _materialInstances = new();
        private readonly int _colorID = Shader.PropertyToID("_BaseColor");

        private void OnDestroy() =>
            RemoveBurntMaterial();

        public override void Init(UnitEffectState state, UnitView unitView)
        {
            base.Init(state, unitView);

            foreach (var renderer in unitView.Renderers)
            {
                var smr = renderer as SkinnedMeshRenderer;
                if (smr == null)
                    continue;

                var bonesCount = Random.Range(5, 8);
                for (var i = 0; i < bonesCount; i++)
                {
                    var bone = smr.bones.GetRandom();
                    AddParticleSystem(bone);
                }
            }

            const float minDamageMultiplierForBurntMaterial = 0.3f;
            var needBurnt = unitView.UnitConfig.GetDamageMultiplier(DamageType.Fire) > minDamageMultiplierForBurntMaterial;
            if (gameObject.activeInHierarchy && needBurnt)
                StartCoroutine(AddBurntMaterial());
        }

        private IEnumerator AddBurntMaterial()
        {
            const float delay = 2;
            yield return new WaitForSeconds(delay);

            foreach (var rnd in UnitView.Renderers)
            {
                rnd.materials = rnd.materials.Append(_burntBodyMaterial).ToArray();
                var materialInst = rnd.materials.Last();

                // Перемешаем для разнообразия
                materialInst.mainTextureOffset = new Vector2(Random.value, Random.value);

                _materialInstances.Add(materialInst);
            }

            const float lerpDuration = 1;
            for (var t = 0f; t < lerpDuration; t += Time.deltaTime)
            {
                var lerp = t / lerpDuration;
                var color = Color.Lerp(_beginBurntColor, _endBurntColor, lerp);

                UnityEngine.Debug.Log(color);

                foreach (var materialInstance in _materialInstances)
                    materialInstance.SetColor(_colorID, color);

                yield return null;
            }
        }


        private void RemoveBurntMaterial()
        {
            if (UnitView == null)
                return;

            foreach (var rnd in UnitView.Renderers)
                rnd.materials = rnd.materials.Where(x => !_materialInstances.Contains(x)).ToArray();

            _materialInstances.Clear();
        }

        private void AddParticleSystem(Transform parent)
        {
            if (parent == null)
                return;

            var particles = Instantiate(_particleSystemTemplate, parent);
            particles.gameObject.SetActive(true);
            particles.transform.localPosition = Vector3.zero;
            _particleSystems.Add(particles);
        }

        public override void Dispose()
        {
            if (gameObject.activeInHierarchy)
                StartCoroutine(DisposeFire());
            else
                base.Dispose();
        }

        private IEnumerator DisposeFire()
        {
            const float particlesStopDelay = 4;
            yield return new WaitForSeconds(particlesStopDelay);

            CancelInvoke();

            foreach (var particleSystem in _particleSystems)
                particleSystem.Stop();

            const float destroyDelay = 10;
            yield return new WaitForSeconds(destroyDelay);

            foreach (var particleSystem in _particleSystems)
                Destroy(particleSystem.gameObject, destroyDelay);

            _particleSystems.Clear();
            base.Dispose();
        }
    }
}