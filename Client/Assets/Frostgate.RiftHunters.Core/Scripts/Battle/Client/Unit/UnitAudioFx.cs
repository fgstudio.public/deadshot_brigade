using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Client;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Client.Audio;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Scripts.Battle.Client.Unit
{
    public class UnitAudioFx : UnitNetworkComponent
    {
        [SerializeField] private AudioClip[] _meleeAttack;
        [SerializeField] private AudioClip[] _death;
        [SerializeField] private AudioClip[] _footStep;
        [SerializeField] private AudioClip[] _damage;
        [SerializeField] private AudioClip[] _dodge;

        private const float StepMinInterval = 0.3f;

        private UnitNetwork _unit;
        private AudioService _audioService;
        private float _stepNextTime;

        public override void Init(UnitView unitView, AudioService audioService)
        {
            _unit = unitView.UnitNetwork;
            _audioService = audioService;

            _unit.UnitNetworkState.BehaviourStateChanged += OnBehaviourStateChanged;
            _unit.UnitActionState.OnBeginAction += OnBeginAction;
        }

        private void OnDestroy()
        {
            if (_unit == null)
                return;

            _unit.UnitNetworkState.BehaviourStateChanged -= OnBehaviourStateChanged;
            _unit.UnitActionState.OnBeginAction -= OnBeginAction;
        }

        private void OnBehaviourStateChanged()
        {
            switch (_unit.UnitNetworkState.BehaviourState)
            {
                case BehaviourState.MeleeAttack:
                    if (_meleeAttack.Length > 0)
                        _audioService.SmallWorldFx.Play(_meleeAttack.GetRandom(), transform.position);
                    break;
                case BehaviourState.Dead:
                    if (_death.Length > 0)
                        _audioService.SmallWorldFx.Play(_death.GetRandom(), transform.position);
                    break;
            }
        }

        private void OnBeginAction(UnitActionConfig config)
        {
            switch (config.Type)
            {
                case UnitActionTypes.LowDamage:
                case UnitActionTypes.MiddleDamage:
                case UnitActionTypes.PowerDamage:
                case UnitActionTypes.UltraDamage:
                    if (_damage.Length > 0)
                        _audioService.SmallWorldFx.Play(_damage.GetRandom(), transform.position);
                    break;
                case UnitActionTypes.Dodge:
                    if (_dodge.Length > 0)
                        _audioService.SmallWorldFx.Play(_dodge.GetRandom(), transform.position);
                    break;
            }
        }

        [UsedImplicitly]
        private void FootStepAnimEvent()
        {
            if (_footStep.Length <= 0)
                return;

            // Не допускаем слишком частых шагов на случай ели смешается две анимашки с шагами.
            if (_stepNextTime > Time.time)
                return;

            _stepNextTime = Time.time + StepMinInterval;

            _audioService.SmallWorldFx.Play(_footStep.GetRandom(), transform.position);
        }
    }
}