using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit
{
    public class HumanoidAnimation : UnitAnimation
    {
        // TODO: когда появится смена оружия, надо будет брать это из конфига оружия
        [SerializeField] private float _weaponType;

        private const float MoveToAimSpeed = 10;
        private const float MoveFromAimSpeed = 1;

        private float _currentAiming;

        public override void Update()
        {
            base.Update();

            _animator.SetBool(AnimatorKeys.Reloading, Unit.UnitNetworkState.BehaviourState == BehaviourState.Reload);
            _animator.SetFloat(AnimatorKeys.WeaponType, _weaponType);

            var targetAiming = Unit.ShootingSharedLogic.InProcess ? 1 : 0;
            var aimSpeed = targetAiming > _currentAiming ? MoveToAimSpeed : MoveFromAimSpeed;
            _currentAiming = Mathf.MoveTowards(_currentAiming, targetAiming, Time.deltaTime * aimSpeed);
            _animator.SetFloat(AnimatorKeys.Aiming, _currentAiming);
        }
    }
}