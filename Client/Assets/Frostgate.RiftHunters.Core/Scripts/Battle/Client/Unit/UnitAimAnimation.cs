using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Client.Audio;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Shooting
{
    public class UnitAimAnimation : UnitNetworkComponent
    {
        [SerializeField] private Transform _animatedBone;
        [SerializeField] private float _speed = 5;
        [SerializeField] private float _angleLimit = 60;

        private UnitNetworkInput _input;
        private UnitView _unitView;
        private float _lerp;
        private UnitNetwork _unit;

        public override void Init(UnitView unitView, AudioService audioService)
        {
            _unitView = unitView;
            _unit = unitView.UnitNetwork;
            _input = _unit.UnitNetworkInput;
        }

        private void LateUpdate()
        {
            var targetLerp = _unit.ShootingSharedLogic.InProcess ? 1 : 0;
            _lerp = Mathf.MoveTowards(_lerp, targetLerp, Time.deltaTime * _speed);

            var deltaRotation = Quaternion.FromToRotation(_unitView.WeaponViewDirection, _input.AimDirection);
            deltaRotation = Quaternion.Lerp(Quaternion.identity, deltaRotation, _lerp);

            var eulerAngles = ClampAngle(deltaRotation.eulerAngles, _angleLimit);
            _animatedBone.Rotate(eulerAngles, Space.World);
        }

        private Vector3 ClampAngle(Vector3 eulerAngles, float angleLimit)
        {
            //Крутим только в горизонтальной плоскости, так как кадзуо передёргивает дробовик, и прочие отдачи могут задирать ствол вверх.
            eulerAngles.y = ClampAngle(eulerAngles.y, angleLimit);
            eulerAngles.x = 0;
            eulerAngles.z = 0;

            return eulerAngles;
        }

        private float ClampAngle(float angle, float angleLimit)
        {
            const float offset = 360;
            const float triggerValue = 180;

            if (angle > triggerValue)
                angle -= offset;

            if (angle < -triggerValue)
                angle += offset;

            return Mathf.Clamp(angle, -angleLimit, angleLimit);
        }
    }
}