using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Client.Audio;
using Object = UnityEngine.Object;
using static Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact.EventActionData;
using static Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact.EventObjectData;
using static Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact.EventPositionData;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact
{
    public sealed class EventHandler
    {
        [NotNull] private readonly UnitView _view;
        [NotNull] private readonly DurationCalculator _durationCalculator;
        [NotNull] private readonly ObjectLinkResolver _objectLinkResolver;

        private IEnumerable<ImpactConfig> Impacts =>
            _view.UnitNetwork.UnitNetworkState.PropertiesProvider?
                .Impacts.AllImpacts.Select(i => i.Config);

        public EventHandler([NotNull] UnitView view)
        {
            _view = view;
            _durationCalculator = new DurationCalculator();
            _objectLinkResolver = new ObjectLinkResolver(view.gameObject);
        }

        public void Handle(string impactId,
            [NotNull, ItemNotNull] IEnumerable<EventData> eventData, Vector3 position)
        {
            foreach (EventData data in eventData)
                Handle(impactId, data, position);
        }

        public void Handle(string impactId, [NotNull] EventData eventData, Vector3 position)
        {
            GameObject eventObject = ResolveEventObject(eventData) ??
                                     throw new ArgumentNullException(nameof(eventObject));

            ExecuteEventAction(eventData.ActionData, eventObject);
            PositionEventObject(eventData.PositionData, eventObject, position);

            if (eventData.ActionData.ExecuteInterfaceMethod)
                ExecuteInterfaceMethod(impactId, eventData, eventObject, position);

            // TODO: тут напрашивается инициализация вьюхи импакта из метода UnitView.Init с пробросом всего необходимого
            if (eventData.AudioData.UseAudioFx)
                _view.AudioService.BigWorldFx.Play(eventData.AudioData.Clip, position);
        }

        private GameObject ResolveEventObject([NotNull] EventData eventData)
        {
            switch (eventData.ObjectData.Type)
            {
                case ObjectType.Instantiate:
                    GameObject prefab = eventData.ObjectData.InstantiatingPrefab;

                    ObjectLink containerData = eventData.ObjectData.InstantiationContainer;
                    GameObject containerObject = _objectLinkResolver.Resolve(containerData);
                    Transform container = containerObject != null ? containerObject.transform : null;

                    return Object.Instantiate(prefab, container, false);

                case ObjectType.Link:
                    return _objectLinkResolver.Resolve(eventData.ObjectData.LinkedObject);

                default: throw new ArgumentOutOfRangeException(nameof(ObjectType));
            }
        }

        private void ExecuteEventAction(
            [NotNull] EventActionData actionData, [NotNull] GameObject eventObject)
        {
            switch (actionData.Type)
            {
                case ActionType.None:
                    break;

                case ActionType.ActivateObject:
                    eventObject.SetActive(true);
                    break;

                case ActionType.DeactivateObject:
                    eventObject.SetActive(false);
                    break;

                default: throw new ArgumentOutOfRangeException(nameof(ActionType));
            }
        }

        private void PositionEventObject([NotNull] EventPositionData positionData, [NotNull] GameObject eventObject, Vector3 position)
        {
            Transform eot = eventObject.transform;
            Transform viewTransform = _view.transform;

            eot.position = positionData.Type switch
            {
                PositionType.ObjectDefault => eot.position,
                PositionType.Source => viewTransform.position,
                PositionType.Target => position,
                PositionType.Relative => (viewTransform.position + positionData.RelativePosition)
                    .RotateAroundPivot(viewTransform.position, viewTransform.rotation),
                _ => throw new ArgumentOutOfRangeException(nameof(PositionType))
            };
        }

        private void ExecuteInterfaceMethod(string impactId, [NotNull] EventData eventData,
            [NotNull] GameObject eventObject, Vector3 position)
        {
            IImpactViewObject[] interfaces = FindInterfaces(eventObject);

            if (interfaces.Length > 0)
            {
                Vector3 sourcePosition = _view.transform.position;
                TimeSpan delay = CalDelay(impactId, eventData, sourcePosition, position);

                foreach (IImpactViewObject i in interfaces)
                    i.Execute(eventData.Type, sourcePosition, position, delay);
            }
        }

        [NotNull, ItemNotNull]
        private IImpactViewObject[] FindInterfaces([NotNull] GameObject eventObject)
        {
            IImpactViewObject[] interfaces = eventObject.GetComponents<IImpactViewObject>();

            return interfaces.Length > 0 ? interfaces :
                throw new ArgumentNullException(nameof(eventObject),
                    $"Event object should have at least one {nameof(IImpactViewObject)}");
        }

        private TimeSpan CalDelay(string impactId, [NotNull] EventData eventData,
            Vector3 sourcePosition, Vector3 position)
        {
            string effectId = eventData.EffectId;
            ImpactConfig impactConfig = Impacts.First(i => i.Id == impactId);
            EffectData effectData = impactConfig.Effects.FirstOrDefault(e => e.Id == effectId);

            switch (eventData.Type)
            {
                case EventData.EventType.CastStarted:
                    return impactConfig.CastDuration;

                case EventData.EventType.CastFinished:
                case EventData.EventType.EffectAffectedTarget:
                    return TimeSpan.Zero;

                case EventData.EventType.EffectActivatedInPosition:
                    EffectActivationData activationData = effectData!.ActivationData;
                    return _durationCalculator.CalcEffectDelay(activationData, sourcePosition, position);

                case EventData.EventType.EffectAffectingTarget:
                case EventData.EventType.EffectAffectingInPosition:
                    return effectData!.AffectData
                        .Select(d => d.StatDurationData)
                        .Where(d => d != null)
                        .Select(_durationCalculator.CalcEffectDuration)
                        .Union(TimeSpan.Zero.AsEnumerable())
                        .Max();

                default: throw new ArgumentOutOfRangeException(nameof(EventData.EventType));
            }
        }
    }
}