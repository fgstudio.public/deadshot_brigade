using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact
{
    [Serializable]
    public sealed class EventAudioData
    {
        [SerializeField] private bool _useAudioFx;
        [SerializeField, ShowIf(nameof(UseAudioFx))] private AudioClip _clip;

        public bool UseAudioFx => _useAudioFx;
        public AudioClip Clip => _clip;
    }
}