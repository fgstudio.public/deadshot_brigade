using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact
{
    [Serializable]
    public sealed class ImpactData
    {
        // TODO: в редакторе использовать ссылки на конфиги
        [SerializeField, Required] private string _impactId;
        [SerializeField] private EventData[] _events;

        public string ImpactId => _impactId;
        public IReadOnlyList<EventData> Events => _events;
    }
}
