using System;
using UnityEngine;
using JetBrains.Annotations;

using static Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact.ObjectLink;
using static Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact.SceneObjectPath;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact
{
    public sealed class ObjectLinkResolver
    {
        [NotNull] private readonly GameObject _root;

        public ObjectLinkResolver([NotNull] GameObject root) =>
            _root = root ? root : throw new ArgumentNullException(nameof(root));

        [CanBeNull]
        public GameObject Resolve([NotNull] ObjectLink objectLink) =>
            objectLink.Type switch
            {
                LinkType.SceneLink => ResolveAsLink(objectLink.Object),
                LinkType.ScenePath => ResolveAsPath(objectLink.Path),
                _ => throw new ArgumentOutOfRangeException(nameof(LinkType))
            };

        [CanBeNull]
        private GameObject ResolveAsLink(GameObject linkedObject) =>
            linkedObject;

        [CanBeNull]
        private GameObject ResolveAsPath(SceneObjectPath objectPath)
        {
            string[] objectNames = objectPath.Path.Split(Divider);

            GameObject root;
            Span<string> namesSpan;

            switch (objectPath.Type)
            {
                case PathType.Absolute:
                    root = GameObject.Find(objectNames[0]);
                    namesSpan = objectNames.AsSpan(1);
                    break;

                case PathType.Relative:
                    root = _root;
                    namesSpan = objectNames.AsSpan();
                    break;

                default: throw new ArgumentOutOfRangeException(nameof(PathType));
            }

            return root?.FindRecursively(namesSpan);
        }
    }
}
