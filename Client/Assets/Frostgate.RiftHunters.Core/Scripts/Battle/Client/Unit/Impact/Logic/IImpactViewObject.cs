using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact
{
    public interface IImpactViewObject
    {
        void Execute(EventData.EventType eventType, Vector3 sourcePosition,
            Vector3 targetPosition, TimeSpan duration);
    }
}
