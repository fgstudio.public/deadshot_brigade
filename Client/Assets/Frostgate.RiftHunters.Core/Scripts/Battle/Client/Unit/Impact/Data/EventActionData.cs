using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact
{
    [Serializable]
    public sealed class EventActionData
    {
        public enum ActionType
        {
            None,
            ActivateObject,
            DeactivateObject
        }

        [SerializeField] private ActionType _type;

        [Tooltip("Запустить на указанном объекте метод специального интерфейса " + nameof(IImpactViewObject))]
        [SerializeField] private bool _executeInterfaceMethod;

        public ActionType Type => _type;
        public bool ExecuteInterfaceMethod => _executeInterfaceMethod;
        // TODO: провалидироать, что у объекта есть нужный интерфейс
    }
}
