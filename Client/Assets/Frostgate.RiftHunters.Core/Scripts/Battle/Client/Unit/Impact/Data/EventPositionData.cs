using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact
{
    [Serializable]
    public sealed class EventPositionData
    {
        public enum PositionType
        {
            ObjectDefault, // позиция объекта по умолчанию
            Source, // позиция исполнителя абилити
            Target, // позиция цели абилити (позиция целевого юнита или области применения)
            Relative // конкретная позиция относительно источника
        }

        [Tooltip("Какую позицию задавать:\n" +
                 nameof(PositionType.ObjectDefault) + " — позиция объекта по умолчанию,\n" +
                 nameof(PositionType.Source) + " — позиция исполнителя абилити,\n" +
                 nameof(PositionType.Target) + " — позиция цели абилити (позиция целевого юнита или области применения),\n" +
                 nameof(PositionType.Relative) + " — конкретная заданная относительная позиция.")]
        [SerializeField] private PositionType _type;

        [ShowIf(nameof(IsPositionCustom))]
        [SerializeField] private Vector3 _relativePosition;

        public PositionType Type => _type;
        public Vector3 RelativePosition => IsPositionCustom ? _relativePosition : default;

        private bool IsPositionCustom => _type == PositionType.Relative;
    }
}
