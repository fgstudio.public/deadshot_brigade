using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact
{
    [Serializable]
    public sealed class EventObjectData
    {
        public enum ObjectType
        {
            Link, // ссылка на объект
            Instantiate // нстанциирование объекта
        }

        [Tooltip("Тип объекта:\n" +
                 nameof(ObjectType.Link) + " — заданный по ссылке,\n" +
                 nameof(ObjectType.Instantiate) + " — инстанциированный в процессе.")]
        [SerializeField] private ObjectType _type;

        [Required, ShowIf(nameof(IsInstantiating)), Title(nameof(InstantiatingPrefab))]
        [SerializeField] private GameObject _prefab;

        [ShowIf(nameof(IsInstantiating)), HideLabel, Title(nameof(InstantiationContainer))]
        [SerializeField] private ObjectLink _container;

        [HideIf(nameof(IsInstantiating)), HideLabel, Title(nameof(LinkedObject))]
        [SerializeField] private ObjectLink _object;

        public ObjectType Type => _type;
        public GameObject InstantiatingPrefab => IsInstantiating ? _prefab : null;
        public ObjectLink InstantiationContainer => IsInstantiating ? _container : null;
        public ObjectLink LinkedObject => IsInstantiating ? null : _object;

        private bool IsInstantiating => _type == ObjectType.Instantiate;
    }
}
