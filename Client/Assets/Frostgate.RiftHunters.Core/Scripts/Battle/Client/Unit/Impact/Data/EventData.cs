using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact
{
    [Serializable]
    public sealed class EventData
    {
        public enum EventType
        {
            CastStarted,
            CastFinished,
            EffectActivatedInPosition,
            EffectAffectingInPosition,
            EffectAffectingTarget,
            EffectAffectedTarget,
            Interrupted
        }

        [SerializeField] private EventType _type;
        // TODO: в редакторе предлагать варианты из доступных
        [SerializeField, Required, ShowIf(nameof(IsEffectIdUsed))] private string _effectId;
        [SerializeField, HideLabel, FoldoutGroup(nameof(ObjectData))] private EventObjectData _objectData;
        [SerializeField, HideLabel, FoldoutGroup(nameof(ActionData))] private EventActionData _actionData;
        [SerializeField, HideLabel, FoldoutGroup(nameof(PositionData))] private EventPositionData _positionData;
        [SerializeField, HideLabel, FoldoutGroup(nameof(AudioData))] private EventAudioData _audioData;

        public EventType Type => _type;
        public string EffectId => IsEffectIdUsed ? _effectId : string.Empty;
        public EventObjectData ObjectData => _objectData;
        public EventActionData ActionData => _actionData;
        public EventPositionData PositionData => _positionData;
        public EventAudioData AudioData => _audioData;

        private bool IsEffectIdUsed => _type != EventType.CastStarted &&
                                       _type != EventType.CastFinished &&
                                       _type != EventType.Interrupted;
    }
}
