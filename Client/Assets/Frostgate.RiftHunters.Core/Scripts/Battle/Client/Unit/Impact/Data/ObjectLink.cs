using System;
using UnityEngine;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact
{
    [Serializable]
    public sealed class ObjectLink
    {
        public enum LinkType
        {
            SceneLink, // ссылка на объект на сцене
            ScenePath // путь до объекта на сцене
        }

        [HorizontalGroup(MinWidth = 120), LabelWidth(30)]
        [Tooltip("Как получить объект на сцене:\n" +
                 nameof(LinkType.SceneLink) + " — по ссылке на объект,\n" +
                 nameof(LinkType.ScenePath) + " — по пути до объекта.")]
        [SerializeField] private LinkType _type;

        [CanBeNull, ShowIf(nameof(IsObjectUsed)), HorizontalGroup, LabelWidth(40)]
        [Tooltip("Ссылка на объект")]
        [SerializeField] private GameObject _object;

        [ShowIf(nameof(IsPathUsed)), HideLabel, HorizontalGroup, InlineProperty]
        [Tooltip("Путь до объекта")]
        [SerializeField] private SceneObjectPath _path;

        public LinkType Type => _type;
        public GameObject Object => IsObjectUsed ? _object : null;
        public SceneObjectPath Path => IsPathUsed ? _path : null;

        private bool IsPathUsed => _type == LinkType.ScenePath;
        private bool IsObjectUsed => _type == LinkType.SceneLink;
    }
}
