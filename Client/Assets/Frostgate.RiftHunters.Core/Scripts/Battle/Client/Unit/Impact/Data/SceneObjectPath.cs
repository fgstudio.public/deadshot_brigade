using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact
{
    [Serializable]
    public sealed class SceneObjectPath
    {
        public enum PathType
        {
            Absolute, // относительно корня сцены
            Relative // относительно текущего объекта
        }

        public const char Divider = '/';

        [LabelText("Path"), LabelWidth(30), HorizontalGroup]
        [Tooltip("Тип пути:\n" +
                 nameof(PathType.Absolute) + " — относительно корня сцены,\n" +
                 nameof(PathType.Relative) + " — относительно текущего объекта.")]
        [SerializeField] private PathType _type;

        [HideLabel, HorizontalGroup]
        [SerializeField] private string _path;

        public PathType Type => _type;
        public string Path => _path;
    }
}
