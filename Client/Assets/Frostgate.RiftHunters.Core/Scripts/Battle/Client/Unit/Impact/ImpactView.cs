using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(UnitView))]
    [AddComponentMenu(BattleComponentMenus.Client.Impacts.Menu + "/" + nameof(ImpactView))]
    [TypeInfoBox("Компонент для настройки реакций на события, происходяшие при работе " + nameof(ImpactState))]
    public sealed class ImpactView : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private UnitView _view;
        [SerializeField, ListDrawerSettings(NumberOfItemsPerPage = 1)] private ImpactData[] _impacts;

        [CanBeNull] private EventHandler _eventHandler;

        private void OnValidate() =>
            _view ??= GetComponent<UnitView>();

        private void Awake() =>
            _eventHandler = new EventHandler(_view);

        private void Start()
        {
            if (_view.UnitNetwork == null) _view.Initialized.AddListener(OnViewInitialized);
            else Subscribe(_view.UnitNetwork.ImpactState);
        }

        private void OnDestroy()
        {
            if (_view.UnitNetwork != null)
                Unsubscribe(_view.UnitNetwork.ImpactState);
        }

        private void OnViewInitialized() =>
            Subscribe(_view.UnitNetwork.ImpactState);

        private void Subscribe([NotNull] ImpactState impactState)
        {
            impactState.Interrupted.AddListener(OnInterrupted);
            impactState.CastStarted.AddListener(OnCastStarted);
            impactState.CastFinished.AddListener(OnCastFinished);
            impactState.EffectActivatedInPosition.AddListener(OnEffectActivatedInPosition);
            impactState.EffectAffectingInPosition.AddListener(OnEffectAffectingInPosition);
            impactState.EffectAffectingTarget.AddListener(OnEffectAffectingTarget);
            impactState.EffectAffectedTarget.AddListener(OnEffectAffectedTarget);
        }

        private void Unsubscribe([NotNull] ImpactState impactState)
        {
            impactState.Interrupted.RemoveListener(OnInterrupted);
            impactState.CastStarted.RemoveListener(OnCastStarted);
            impactState.CastFinished.RemoveListener(OnCastFinished);
            impactState.EffectActivatedInPosition.RemoveListener(OnEffectActivatedInPosition);
            impactState.EffectAffectingInPosition.RemoveListener(OnEffectAffectingInPosition);
            impactState.EffectAffectingTarget.RemoveListener(OnEffectAffectingTarget);
            impactState.EffectAffectedTarget.RemoveListener(OnEffectAffectedTarget);
        }

        private void OnInterrupted(string impactId) =>
            HandleEvent(EventData.EventType.Interrupted, impactId);

        private void OnCastStarted(string impactId) =>
            HandleEvent(EventData.EventType.CastStarted, impactId);

        private void OnCastFinished(string impactId) =>
            HandleEvent(EventData.EventType.CastFinished, impactId);

        private void OnEffectActivatedInPosition(string impactId, string effectId, Vector3 position) =>
            HandleEvent(EventData.EventType.EffectActivatedInPosition, impactId, effectId, position);

        private void OnEffectAffectingInPosition(string impactId, string effectId, Vector3 position) =>
            HandleEvent(EventData.EventType.EffectAffectingInPosition, impactId, effectId, position);

        private void OnEffectAffectingTarget(string impactId, string effectId, Vector3 position) =>
            HandleEvent(EventData.EventType.EffectAffectingTarget, impactId, effectId, position);

        private void OnEffectAffectedTarget(string impactId, string effectId, Vector3 position) =>
            HandleEvent(EventData.EventType.EffectAffectedTarget, impactId, effectId, position);

        private void HandleEvent(EventData.EventType type, string impactId)
        {
            IEnumerable<EventData> eventData = FindEventData(type, impactId);
            _eventHandler!.Handle(impactId, eventData, _view.transform.position);
        }

        private void HandleEvent(EventData.EventType type, string impactId, string effectId, Vector3 position)
        {
            IEnumerable<EventData> eventData = FindEventData(type, impactId, effectId);
            _eventHandler!.Handle(impactId, eventData, position);
        }

        private IEnumerable<EventData> FindEventData(EventData.EventType type, string impactId, string effectId) =>
            FindEventData(type, impactId).Where(e => e.EffectId == effectId);

        private IEnumerable<EventData> FindEventData(EventData.EventType type, string impactId) =>
            _impacts
                .Where(i => i.ImpactId == impactId)
                .SelectMany(i => i.Events)
                .Where(e => e.Type == type);
    }
}