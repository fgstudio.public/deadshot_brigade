using Frostgate.RiftHunters.Core.UI;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public sealed class UnitStateView : MonoBehaviour
    {
        [field: SerializeField, Required, ChildGameObjectsOnly] public Transform FxContainer { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public TMP_Text UnitName { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public BarColorRepository NameColors { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public Bar HealthBar { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public EnergyBar EnergyBar { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public BarAutoFader BarAutoFader { get; private set; }
        [field: SerializeField] public BulletsView BulletsView { get; private set; }
        [field: SerializeField] public SegmentedBar StaminaBar { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public UIEnemyEffectsView EnemyEffectsView { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public EmotionUnitView EmotionUnitView { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public UIEffectMessage EffectMessage { get; private set; }
    }
}
