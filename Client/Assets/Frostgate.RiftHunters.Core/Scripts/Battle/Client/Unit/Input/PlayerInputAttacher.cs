using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Input;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit
{
    public class PlayerInputAttacher
    {
        private readonly BattleCamera _battleCamera;
        private readonly PlayerInput _input;
        private readonly Transform _ui;

        public PlayerInputAttacher(BattleCamera battleCamera, [NotNull] PlayerInput input, [NotNull] Transform ui)
        {
            _battleCamera = battleCamera;
            _input = input;
            _ui = ui;
        }

        public void AttachToUnit([NotNull] UnitNetwork unit)
        {
            if (_battleCamera == null)
                return;

            var playerUnitInput = unit.gameObject.AddComponent<PlayerUnitInput>();
            playerUnitInput.Init(_battleCamera, _input, unit, _ui);
        }

        public void DeattachUnit([NotNull] UnitNetwork unit)
        {
            if (unit.gameObject.TryGetComponent(out PlayerUnitInput playerUnitInput))
                Object.Destroy(playerUnitInput);
        }
    }
}