using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Input;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit
{
    public sealed class PlayerUnitInput : MonoBehaviour
    {
        private PlayerInput _input;
        private UnitNetwork _unit;
        private BattleCamera _battleCamera;
        private Transform _ui;

        public void Init([NotNull] BattleCamera battleCamera, [NotNull] PlayerInput input, [NotNull] UnitNetwork unit, [NotNull] Transform ui)
        {
            _battleCamera = battleCamera;
            _input = input;
            _unit = unit;
            _ui = ui;

            Subscribe(_unit);
            Subscribe(_input);

            if (_unit.UnitNetworkInput.hasAuthority)
                _unit.UnitNetworkInput.YRotation = _battleCamera?.YRotation ?? 0f;

            UpdateUiVisibility();
        }

        private void OnDestroy()
        {
            if (_input != null) Unsubscribe(_input);
            if (_unit != null) Unsubscribe(_unit);
        }

        private void Subscribe([NotNull] PlayerInput input)
        {
            input.Move.AddListener(OnMove);
            input.Look.AddListener(OnLook);
            input.Reload.AddListener(OnReload);
            input.Ability.AddListener(OnAbility);
            input.ExtraAbility.AddListener(OnExtraAbility);
            input.Ultimate.AddListener(OnUltimate);
            input.Dodge.AddListener(OnDodge);
            input.Emotion.AddListener(OnEmotion);
        }

        private void Unsubscribe([NotNull] PlayerInput input)
        {
            input.Move.RemoveListener(OnMove);
            input.Look.RemoveListener(OnLook);
            input.Reload.RemoveListener(OnReload);
            input.Ability.RemoveListener(OnAbility);
            input.ExtraAbility.RemoveListener(OnExtraAbility);
            input.Ultimate.RemoveListener(OnUltimate);
            input.Dodge.RemoveListener(OnDodge);
            input.Emotion.RemoveListener(OnEmotion);
        }

        private void Subscribe([NotNull] UnitNetwork unit)
        {
            unit.UnitNetworkState.HealthChanged += OnHealthChanged;
            unit.UnitMove.RotationSet.AddListener(UpdateLook);
        }

        private void Unsubscribe([NotNull] UnitNetwork unit)
        {
            unit.UnitNetworkState.HealthChanged -= OnHealthChanged;
            unit.UnitMove.RotationSet.RemoveListener(UpdateLook);
        }

        private void OnHealthChanged(float diff) => UpdateUiVisibility();
        private void UpdateUiVisibility() => _ui.gameObject.SetActive(!_unit.UnitNetworkState.IsDead);
        private void OnMove(Vector2 move) => _unit.UnitNetworkInput.Move = move;
        private void OnReload() => _unit.UnitNetworkInput.ExecuteReloading();
        private void OnAbility() => _unit.UnitNetworkInput.ExecuteAbility();
        private void OnExtraAbility() => _unit.UnitNetworkInput.ExecuteExtraAbility();
        private void OnUltimate() => _unit.UnitNetworkInput.ExecuteUltimate();
        private void OnDodge() => _unit.UnitNetworkInput.ExecuteDodge();
        private void OnLook(Vector2 look) => SetLook(_battleCamera.YRotation + look.x);

        private void UpdateLook() => SetLook(_unit.UnitNetworkInput.YRotation);
        private void SetLook(float yRotation)
        {
            _battleCamera.YRotation = yRotation;
            _unit.UnitNetworkInput.YRotation = _battleCamera.YRotation;
        }

        private void OnEmotion(int index)
        {
            if (_unit.UnitEmotionRoster.TryGet(index, out var config))
                _unit.UnitNetworkInput.InvokeEmotion(config);
        }
    }
}