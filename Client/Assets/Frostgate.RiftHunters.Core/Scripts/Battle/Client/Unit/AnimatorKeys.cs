using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public static class AnimatorKeys
    {
        public static int Forward { get; private set; } = Animator.StringToHash(nameof(Forward));
        public static int Strafe { get; private set; } = Animator.StringToHash(nameof(Strafe));
        public static int IsAttacking { get; private set; } = Animator.StringToHash(nameof(IsAttacking));
        public static int Reloading { get; private set; } = Animator.StringToHash(nameof(Reloading));
        public static int IsStun { get; private set; } = Animator.StringToHash(nameof(IsStun));
        public static int Dodge { get; private set; } = Animator.StringToHash(nameof(Dodge));
        public static int Death { get; private set; } = Animator.StringToHash(nameof(Death));
        public static int Ability { get; private set; } = Animator.StringToHash(nameof(Ability));
        public static int AbilityUpper { get; private set; } = Animator.StringToHash(nameof(AbilityUpper));
        public static int ExtraAbility { get; private set; } = Animator.StringToHash(nameof(ExtraAbility));
        public static int ExtraAbilityUpper { get; private set; } = Animator.StringToHash(nameof(ExtraAbilityUpper));
        public static int Ultimate { get; private set; } = Animator.StringToHash(nameof(Ultimate));
        public static int UltimateUpper { get; private set; } = Animator.StringToHash(nameof(UltimateUpper));
        public static int Respawn { get; private set; } = Animator.StringToHash(nameof(Respawn));
        public static int AttackState { get; private set; } = Animator.StringToHash(nameof(AttackState));
        public static int SingleAttack { get; private set; } = Animator.StringToHash(nameof(SingleAttack));
        public static int WeaponType { get; private set; } = Animator.StringToHash(nameof(WeaponType));
        public static int Aiming { get; private set; } = Animator.StringToHash(nameof(Aiming));
        public static int CycleOffset { get; private set; } = Animator.StringToHash(nameof(CycleOffset));
    }
}