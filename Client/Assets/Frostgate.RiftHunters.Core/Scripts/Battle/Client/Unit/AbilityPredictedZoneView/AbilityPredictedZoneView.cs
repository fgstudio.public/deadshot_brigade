using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Input;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class AbilityPredictedZoneView : MonoBehaviour
    {
        [Required]
        [SerializeField] private GameObject _view;

        [MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _abilityDuration = 1.5f;

        private PlayerInput _playerInput;
        private GameObject _hiddenObject;
        private Coroutine _showAbilityExecution;
        private UnitNetwork _unit;

        public void Init(UnitNetwork unit, PlayerInput playerInput, GameObject hiddenObject)
        {
            _unit = unit;
            _hiddenObject = hiddenObject;
            _playerInput = playerInput;

            _playerInput.AbilityPressed.AddListener(Recalculate);
            _playerInput.AbilityCanceled.AddListener(Recalculate);
            _unit.ImpactRoster.Ability.CastStarted += OnAbilityExecuting;
            _unit.UnitActionState.OnActionTypeChanged += Recalculate;

            Hide();
        }

        private void OnDestroy()
        {
            if (_playerInput == null)
                return;

            _playerInput.AbilityPressed.RemoveListener(Recalculate);
            _playerInput.AbilityCanceled.RemoveListener(Recalculate);
            _unit.ImpactRoster.Ability.CastStarted -= OnAbilityExecuting;
            _unit.UnitActionState.OnActionTypeChanged -= Recalculate;
        }

        private void OnAbilityExecuting() =>
            _showAbilityExecution = StartCoroutine(ShowAbilityExecution());

        private void Recalculate()
        {
            // Запущена логика показа действия абилки. Событий обрабатывать не нужно.
            if (_showAbilityExecution != null)
                return;

            var canExecute = _unit.ImpactRoster.Ability.CanExecute;
            var inDodge = _unit.UnitActionState.CurrentActionType == UnitActionTypes.Dodge;
            var isPressed = _playerInput.IsAbilityPressed;

            if (!inDodge && isPressed && canExecute)
                Show();
            else
                Hide();
        }

        private void Show()
        {
            _hiddenObject.SetActive(false);
            _view.SetActive(true);
        }

        private void Hide()
        {
            _hiddenObject.SetActive(true);
            _view.SetActive(false);
        }

        private IEnumerator ShowAbilityExecution()
        {
            Show();

            if (_abilityDuration > 0)
            {
                float finishTime = Time.time + _abilityDuration;
                yield return new WaitUntil(() =>
                    _unit.IsDestroyedOrDead || Time.time >= finishTime);
            }

            Hide();
            _showAbilityExecution = null;
        }
    }
}