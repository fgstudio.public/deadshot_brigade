using System.Collections;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Client.Audio;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class UnitAnimation : UnitNetworkComponent
    {
        [SerializeField] protected Animator _animator;
        [SerializeField] private Transform _transform;
        [SerializeField] private string _specialImpactAnimationName;
        [SerializeField] private string _specialImpactUpperAnimationName;

        private Rigidbody Rigidbody => Unit.Rigidbody;

        protected UnitNetwork Unit { get; private set; }
        protected int SpecialAnimationHash { get; private set; }
        protected int UltimateAnimationHash { get; private set; }
        protected int AbilityAnimationHash { get; private set; }
        protected int ExtraAbilityAnimationHash { get; private set; }

        private bool _isDead;
        private int _meleeAttackKey;
        private bool _isAbilityAnimation;
        private bool _isExtraAbilityAnimation;
        private bool _isUltimateAnimation;
        private bool _isSpecialAnimation;
        private readonly List<string> _activeTriggerNames = new();
        private readonly List<int> _activeTriggerIds = new();

        private Vector3 _currentMove;

        public override void Init(UnitView unitView, AudioService audioService)
        {
            _animator.applyRootMotion = false;
            _animator.updateMode = AnimatorUpdateMode.Normal;

            Unit = unitView.UnitNetwork;

            if (Unit.MeleeWeapon != null)
                _meleeAttackKey = Animator.StringToHash(Unit.MeleeWeapon.AnimationKey);

            if (Unit.UnitNetworkState.IsDead)
                TrySetDead();

            if (gameObject.activeInHierarchy)
                StartCoroutine(AnimatorRewind());

            Unit.UnitNetworkState.SingleAttacked += OnSingleAttacked;
            Unit.UnitNetworkState.BehaviourStateChanged += BehaviourStateChanged;
            Unit.UnitActionState.OnBeginAction += OnBeginAction;

            UnitImpactRoster impacts = Unit.UnitNetworkState.PropertiesProvider?.Impacts;

            ImpactConfig ultimate = impacts?.Ultimate.Config;
            UltimateAnimationHash = ultimate && ultimate.CanMove
                ? AnimatorKeys.UltimateUpper
                : AnimatorKeys.Ultimate;

            ImpactConfig ability = impacts?.Ability.Config;
            AbilityAnimationHash = ability && ability.CanMove
                ? AnimatorKeys.AbilityUpper
                : AnimatorKeys.Ability;

            ImpactConfig extraAbility = impacts?.ExtraAbility.Config;
            ExtraAbilityAnimationHash = extraAbility && extraAbility.CanMove
                ? AnimatorKeys.ExtraAbilityUpper
                : AnimatorKeys.ExtraAbility;

            ImpactConfig special = impacts?.Special.Config;
            SpecialAnimationHash = Animator.StringToHash(
                special && special.CanMove
                    ? _specialImpactUpperAnimationName
                    : _specialImpactAnimationName);
        }

        private void OnDestroy()
        {
            if (Unit == null || Unit.UnitNetworkState == null) return;
            Unit.UnitNetworkState.SingleAttacked -= OnSingleAttacked;
            Unit.UnitNetworkState.BehaviourStateChanged -= BehaviourStateChanged;
            Unit.UnitActionState.OnBeginAction -= OnBeginAction;
        }

        private void OnSingleAttacked()
        {
            if (_isDead)
                return;

            SetTrigger(AnimatorKeys.SingleAttack);
        }

        private void OnBeginAction(UnitActionConfig actionConfig)
        {
            if (actionConfig.IsDeath)
                _isDead = true;
            else if (_isDead)
                return;

            SetTrigger(actionConfig.AnimationTrigger);
        }

        private void BehaviourStateChanged()
        {
            if (Unit.UnitNetworkState.BehaviourState != BehaviourState.Dead)
                TryRespawn();

            switch (Unit.UnitNetworkState.BehaviourState)
            {
                case BehaviourState.MeleeAttack:
                    HandleMeleeAttack();
                    break;
                case BehaviourState.Dead:
                    TrySetDead();
                    break;
            }
        }

        private void TrySetDead()
        {
            if (_isDead)
                return;

            _isDead = true;
            SetTrigger(AnimatorKeys.Death);
        }

        private void TryRespawn()
        {
            if (!_isDead)
                return;

            _isDead = false;
            SetTrigger(AnimatorKeys.Respawn);
        }

        private IEnumerator AnimatorRewind()
        {
            // Сразу после инсташиирования перемотка может не срабатывать. Ждем кадр.
            yield return new WaitForEndOfFrame();

            // Перемотаем с рандомом, чтобы рассинхронизировать одновременно заспауненых
            // TODO: но чтобы это заработало надо ещё переделать животный аниматор, занести айдл и движение в блендтри.
            var length = Random.Range(5, 6f);
            _animator.Update(length);
        }

        public virtual void Update()
        {
            _activeTriggerIds.Clear();
            _activeTriggerNames.Clear();

            const float moveLerpSpeed = 10;
            var velocity = Rigidbody.velocity;
            velocity = _transform.InverseTransformVector(velocity);
            var targetMove = velocity / Unit.UnitNetworkState.PropertiesProvider!.Speed;

            _currentMove = Vector3.Lerp(_currentMove, targetMove, Time.deltaTime * moveLerpSpeed);

            _animator.SetFloat(AnimatorKeys.Forward, _currentMove.z);
            _animator.SetFloat(AnimatorKeys.Strafe, _currentMove.x);
            _animator.SetBool(AnimatorKeys.IsAttacking, Unit.ShootingSharedLogic.InProcess);
            _animator.SetBool(AnimatorKeys.IsStun, Unit.UnitNetworkState.BehaviourState == BehaviourState.Stun);

            UpdateAbilityAnimation();
            UpdateExtraAbilityAnimation();
            UpdateUltimateAnimation();
            UpdateSpecialAnimation();
        }

        private void UpdateAbilityAnimation() =>
            UpdateAnimation(Unit.ImpactRoster.Ability, ref _isAbilityAnimation, AbilityAnimationHash);

        private void UpdateExtraAbilityAnimation() =>
            UpdateAnimation(Unit.ImpactRoster.ExtraAbility, ref _isExtraAbilityAnimation, ExtraAbilityAnimationHash);

        private void UpdateUltimateAnimation() =>
            UpdateAnimation(Unit.ImpactRoster.Ultimate, ref _isUltimateAnimation, UltimateAnimationHash);

        private void UpdateSpecialAnimation() =>
            UpdateAnimation(Unit.ImpactRoster.Special, ref _isSpecialAnimation, SpecialAnimationHash);

        private void UpdateAnimation(IClientImpactModel ultimateImpactModel, ref bool animationFlag, int animationHash)
        {
            if (!animationFlag && ultimateImpactModel.IsCasting)
            {
                SetTrigger(animationHash);
                animationFlag = true;
            }

            if (animationFlag && !ultimateImpactModel.IsCasting)
                animationFlag = false;
        }

        private void HandleMeleeAttack()
        {
            float attackState = 0.5f * Random.Range(0, 3);

            SetTrigger(_meleeAttackKey);
            _animator.SetFloat(AnimatorKeys.AttackState, attackState);
        }

        private void SetTrigger(string triggerName)
        {
            ResetAllTriggers();
            _activeTriggerNames.Add(triggerName);
            _animator.SetTrigger(triggerName);
        }

        private void SetTrigger(int triggerId)
        {
            ResetAllTriggers();
            _activeTriggerIds.Add(triggerId);
            _animator.SetTrigger(triggerId);
        }

        private void ResetAllTriggers()
        {
            foreach (var activeTriggerId in _activeTriggerIds)
                _animator.ResetTrigger(activeTriggerId);

            foreach (var activeTriggerName in _activeTriggerNames)
                _animator.ResetTrigger(activeTriggerName);

            _activeTriggerIds.Clear();
            _activeTriggerNames.Clear();
        }
    }
}