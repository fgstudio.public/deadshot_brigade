using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Components;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Client.Audio;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.Menu + "/" + nameof(InvincibleEffect))]
    public sealed class InvincibleEffect : UnitNetworkComponent
    {
        [Header("References")]
        [Required, ChildGameObjectsOnly, HideInPrefabs]
        [SerializeField] private SkinnedMeshRenderer _meshRenderer;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private BlinkComponent _blinkComponent;

        [CanBeNull] private UnitNetwork _unit;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _meshRenderer ??= GetComponentInChildren<SkinnedMeshRenderer>();
            _blinkComponent ??= GetComponentInChildren<BlinkComponent>();
            _blinkComponent.Disable();
        }

        public override void Init(UnitView unitView, AudioService audioService)
        {
            _unit = unitView.UnitNetwork;
            Subscribe(_unit);
        }

        private void OnDestroy()
        {
            if (_unit != null)
                Unsubscribe(_unit);
        }

        [Button] private void StartInvincible() => _blinkComponent.Enable();
        [Button] private void StopInvincible() => _blinkComponent.Disable();

        private void Subscribe([NotNull] UnitNetwork unit) =>
            unit.UnitNetworkState.IsInvincibleChanged += OnInvincibleChanged;

        private void Unsubscribe([NotNull] UnitNetwork unit) =>
            unit.UnitNetworkState.IsInvincibleChanged -= OnInvincibleChanged;

        private void OnInvincibleChanged()
        {
            if (_unit != null && _unit.UnitNetworkState.IsInvincible)
                StartInvincible();
            else
                StopInvincible();
        }
    }
}