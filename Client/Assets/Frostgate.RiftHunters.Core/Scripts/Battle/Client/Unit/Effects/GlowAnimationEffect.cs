using System.Collections;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Core.Client.Audio;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class GlowAnimationEffect : UnitNetworkComponent
    {
        [SerializeField] private UnitEffectTypes _triggerEffectType = UnitEffectTypes.PiercingBullets;
        [SerializeField] private AnimationCurve _powerAnimation;
        [SerializeField] private AnimationCurve _gradAnimation;
        [SerializeField] private Renderer _renderer;
        [SerializeField] private int _targetMaterialIndex;
        [SerializeField] private ParticleSystem _particleSystem;

        private UnitNetwork _unit;
        private Material _material;
        private readonly int _powerId = Shader.PropertyToID("_Power");
        private readonly int _gradId = Shader.PropertyToID("_Grad");
        private Coroutine _materialCoroutine;

        public override void Init(UnitView unitView, AudioService audioService)
        {
            _unit = unitView.UnitNetwork;
            _material = _renderer.materials[_targetMaterialIndex];

            ResetMaterial();

            _unit.UnitNetworkEffects.OnAdd += OnEffectAdded;
        }

        private void OnDestroy()
        {
            if (_unit != null)
                _unit.UnitNetworkEffects.OnAdd -= OnEffectAdded;
        }

        private void OnEffectAdded(UnitEffectState state)
        {
            if (state.Config.Type != _triggerEffectType)
                return;

            _particleSystem.Play();

            if (_materialCoroutine != null)
                StopCoroutine(_materialCoroutine);

            _materialCoroutine = StartCoroutine(PlayMaterialAnimation());
        }

        private IEnumerator PlayMaterialAnimation()
        {
            var gradDuration = _gradAnimation.keys[_gradAnimation.length - 1].time;
            var powerDuration = _powerAnimation.keys[_powerAnimation.length - 1].time;
            var durationMaterialEffect = Mathf.Max(gradDuration, powerDuration);

            var time = 0f;
            while (time < durationMaterialEffect)
            {
                _material.SetFloat(_powerId, _powerAnimation.Evaluate(time));
                _material.SetFloat(_gradId, _gradAnimation.Evaluate(time));

                yield return null;
                time += Time.deltaTime;
            }

            ResetMaterial();
        }

        private void ResetMaterial()
        {
            _material.SetFloat(_powerId, 0);
            _material.SetFloat(_gradId, -1);
        }
    }
}