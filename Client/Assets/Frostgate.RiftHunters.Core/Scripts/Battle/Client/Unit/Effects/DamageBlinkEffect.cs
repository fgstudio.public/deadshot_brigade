using Mirror;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Threading;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Client.Audio;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.Menu + "/" + nameof(DamageBlinkEffect))]
    public sealed class DamageBlinkEffect : UnitNetworkComponent
    {
        [Header("References")]
        [ChildGameObjectsOnly, Required]
        [SerializeField] private SkinnedMeshRenderer _meshRenderer;

        [Header("Settings")]
        [Tooltip("������ ������������� ������� �� �������. �� �������������, �������������� ����� ���������� ����� ��� ������������ �������")]
        [SerializeField, Required] private AnimationCurve _flashPower;
        [Tooltip("���� �������, ����������� ��� ���������")]
        [SerializeField, Required] private Color _flashColor;

        [Header("Shader properties")]
        [SerializeField, Required] private string _flashColorPropertyName = "_Color";
        [SerializeField, Required] private string _flashPowerPropertyName = "_PowerColor";

        [CanBeNull] private UnitNetwork _unit;
        [CanBeNull] private IDamageService _damageService;
        private CancellationTokenSource _tokenSrc;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() => _meshRenderer ??= GetComponentInChildren<SkinnedMeshRenderer>();

        [Inject]
        private void MonoConstructor([NotNull] IDamageService damageService)
        {
            _damageService = damageService;
            _damageService.Damaged.AddListener(OnDamaged);
        }

        private void OnDestroy()
        {
            if (_damageService != null)
                _damageService.Damaged.RemoveListener(OnDamaged);
        }

        public override void Init(UnitView unitView, AudioService audioService)
        {
            _unit = unitView.UnitNetwork;

            if (_meshRenderer != null)
                _meshRenderer.material.SetColor(_flashColorPropertyName, _flashColor);
            else
                UnityEngine.Debug.LogWarning($"{nameof(DamageBlinkEffect)} {_unit.name} {_meshRenderer} is null");
        }

        [Button]
        private void StopFlash() =>
            CancelPrevFlash();

        [Button]
        private void ToShowFlash()
        {
            CancelPrevFlash();

            _tokenSrc = CancellationTokenSource
                .CreateLinkedTokenSource(this.GetCancellationTokenOnDestroy());
            ToShowFlash(_tokenSrc.Token);
        }

        private void OnDamaged(DamageEventData data)
        {
            if (_unit == null) return;
            if (data.ReceiverNetId != _unit.Identity.netId) return;

            if (IsShowFlash(data.CasterNetId))
                ToShowFlash();
        }

        private bool IsShowFlash(uint casterId) =>
            NetworkClient.active && NetworkClient.localPlayer?.netId == casterId ||
            NetworkServer.active && NetworkServer.localConnection?.identity.netId == casterId;

        private async void ToShowFlash(CancellationToken token)
        {
            float startFlashTime = Time.time;
            float flashDuration = _flashPower.keys[^1].time;
            float endFlashTime = startFlashTime + flashDuration;

            while (!token.IsCancellationRequested && Time.time < endFlashTime)
            {
                float localCurveTime = Time.time - startFlashTime;
                SetPowerFromTime(localCurveTime);

                await UniTask.Yield();
            }

            if (!token.IsCancellationRequested)
                SetPowerFromTime(flashDuration);
        }

        private void SetPowerFromTime(float time)
        {
            Material material = _meshRenderer.material;
            float power = _flashPower.Evaluate(time);

            material.SetFloat(_flashPowerPropertyName, power);
        }

        private void CancelPrevFlash()
        {
            if (_tokenSrc != null)
            {
                _tokenSrc.Cancel();
                _tokenSrc.Dispose();
                _tokenSrc = null;
            }
        }
    }
}