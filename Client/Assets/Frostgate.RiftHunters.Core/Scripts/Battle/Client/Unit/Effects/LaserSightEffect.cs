using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Client.Audio;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class LaserSightEffect : UnitNetworkComponent
    {
        [SerializeField] private LineRenderer _laserLine;
        [SerializeField] private Transform _laserPointTransform;
        [SerializeField] private float _laserMaxLength = 50f;

        private bool SightEnabled => !_unit.IsDestroyedOrDead && _unit.ShootingSharedLogic.InProcess;
        private Vector3 SightDir => _unit.UnitNetworkInput.AimDirection;
        private Vector3 SightStart => _unit.ShootPoint;

        private UnitNetwork _unit;
        private readonly Vector3[] _points = new Vector3[2];
        private Vector3 _dir;

        public override void Init(UnitView unitView, AudioService audioService)
        {
            _unit = unitView.UnitNetwork;
            _dir = SightDir;
        }

        private void Update()
        {
            if (!SightEnabled)
            {
                _laserLine.enabled = false;
                _dir = _unit.ForwardAxis;

                PointSetActive(false);
                return;
            }

            const float rotateSpeed = 1f;
            var rotateStep = rotateSpeed * Time.deltaTime;
            _dir = Vector3.RotateTowards(_dir, SightDir.normalized, rotateStep, rotateStep);

            _points[0] = SightStart;

            if (Physics.Raycast(SightStart, _dir, out var rh, _laserMaxLength, PhysicsLayers.UnitBodyPartAndWallMask))
            {
                _points[1] = rh.point;

                PointSetActive(true);

                _laserPointTransform.position = rh.point;
                _laserPointTransform.forward = -_dir;
            }
            else
            {
                PointSetActive(false);
                _points[1] = SightStart + _dir * _laserMaxLength;
            }

            _laserLine.enabled = true;
            _laserLine.SetPositions(_points);
        }

        private void PointSetActive(bool isActive) =>
            _laserPointTransform.gameObject.SetActive(isActive);
    }
}