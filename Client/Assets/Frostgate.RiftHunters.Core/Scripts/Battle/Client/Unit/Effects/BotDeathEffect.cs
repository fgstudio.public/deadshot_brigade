using System.Collections;
using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Client.Audio;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public sealed class BotDeathEffect : UnitNetworkComponent
    {
        [SerializeField, Min(0)] private float _delay = 3;
        [SerializeField, Min(0)] private float _duration = 5;
        [SerializeField, Min(0)] private float _offset = 2;
        [SerializeField] private GameObject _bloodExplosionEffect;
        [SerializeField] private float _bloodExplosionPercentLimit = .5f;

        private UnitNetworkState _state;
        private UnitNetwork _unit;
        private bool _isActivated;
        private float _accumulatedDamage;
        private float _damageClearTime;

        public override void Init(UnitView unitView, AudioService audioService)
        {
            _unit = unitView.UnitNetwork;
            _state = _unit.UnitNetworkState;

            UpdateStatus(true, _state.IsDead);

            _state.HealthChanged += OnHealthChanged;
            _state.BehaviourStateChanged += BehaviourStateChanged;
        }

        private void BehaviourStateChanged() =>
            UpdateStatus(false, _state.BehaviourState == BehaviourState.Dead);

        private void OnHealthChanged(float diff)
        {
            if (diff < 0)
            {
                if (_damageClearTime < Time.time)
                {
                    const float interval = 0.3f;
                    _damageClearTime = Time.time + interval;
                    _accumulatedDamage = 0;
                }

                // Немного накапливаем дамаг для расчётов. Благодаря этому например посчитается несколько дробинок дробовика в сумме.
                _accumulatedDamage += Mathf.Abs(diff);
            }
        }

        private void OnDestroy()
        {
            if (_unit == null || _state == null)
                return;

            _state.HealthChanged -= OnHealthChanged;
            _state.BehaviourStateChanged -= BehaviourStateChanged;
        }

        private void UpdateStatus(bool isDiscrete, bool isDead)
        {
            if (!_isActivated && isDead) Activate(isDiscrete);
            if (_isActivated && !isDead) Deactivate();
        }

        private void Activate(bool isDiscrete)
        {
            _isActivated = true;

            if (isDiscrete)
            {
                gameObject.SetActive(false);
                return;
            }

            var damage = _accumulatedDamage;

            // Если перед смертью нанесён значительный урон, то кровь, кишки, распидарасило.
            var needBloodExplosion = damage / _state.MaxHealth >= _bloodExplosionPercentLimit;
            if (needBloodExplosion)
            {
                gameObject.SetActive(false);
                Instantiate(_bloodExplosionEffect,  _unit.FocusPoint, _unit.Transform.rotation, _unit.Transform.parent);
                return;
            }

            // А если незначительный урон, то готовим тушку к земле.
            StopAllCoroutines();
            StartCoroutine(MoveViewDown());
        }

        private IEnumerator MoveViewDown()
        {
            var startPosition = transform.localPosition;
            var targetPosition = Vector3.down * _offset;

            yield return new WaitForSeconds(_delay);

            for (var t = 0f; t < _duration; t += Time.deltaTime)
            {
                yield return null;
                var lerp = t / _duration;
                transform.localPosition = Vector3.Lerp(startPosition, targetPosition, lerp);
            }

            gameObject.SetActive(false);
        }

        private void Deactivate()
        {
            _isActivated = false;

            StopAllCoroutines();
            transform.localPosition = Vector3.zero;
            gameObject.SetActive(true);
        }
    }
}