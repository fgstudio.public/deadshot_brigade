using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Utils;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public partial class UnitView
    {
        private const string NullError = "Has null objects";

        private void OnValidate()
        {
            RemoveNullElements();
            UpdateNetworkComponents();
        }

        private void RemoveNullElements()
        {
            if (_unitNetworkComponents == null) return;

            var cnt = _unitNetworkComponents.Count;
            while (--cnt >= 0)
                if (_unitNetworkComponents[cnt] == null)
                    _unitNetworkComponents.RemoveAt(cnt);
        }

        private void UpdateNetworkComponents()
        {
#if UNITY_EDITOR
            var components = GetComponents<UnitNetworkComponent>();

            foreach (var component in components)
            {
                if (_unitNetworkComponents.Contains(component))
                    continue;

                _unitNetworkComponents.Add(component);
                UnityEditor.EditorUtility.SetDirty(this);
            }
#endif
        }

        private bool HasNullNetworkComponents() =>
            _unitNetworkComponents == null || _unitNetworkComponents.Count == 0 || !Validator.HasNullElements(_unitNetworkComponents);
    }
}