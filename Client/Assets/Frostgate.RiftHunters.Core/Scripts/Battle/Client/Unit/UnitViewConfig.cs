using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Battle.Client.Unit
{
    public class UnitViewConfig : MonoBehaviour
    {
        [field: SerializeField, Required] public Transform ShootPoint { get; private set; }
        [field: SerializeField, Required] public CapsuleCollider Body { get; private set; }
        [field: SerializeField, Required] public CapsuleCollider AimTarget { get; private set; }
        [field: SerializeField] public float BodyFxRadius { get; private set; }
        [field: SerializeField, Required] public DamageZonesConfig DamageZones { get; private set; }

        public Vector3 FocusPoint => AimTarget.transform.position + AimTarget.transform.rotation * AimTarget.center;

        public void Init(BattleUnitType unitType)
        {
            gameObject.name = "UnitViewConfig";
            Body.gameObject.tag = unitType.ToString();
            AimTarget.gameObject.tag = unitType.ToString();
        }
    }
}