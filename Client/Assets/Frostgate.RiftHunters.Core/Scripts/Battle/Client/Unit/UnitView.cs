using System;
using Zenject;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Battle.Client.FX;
using Frostgate.RiftHunters.Core.Battle.Client.Weapon;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Core.Client.Audio;
using Frostgate.RiftHunters.Core.Systems.Input;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public sealed partial class UnitView : MonoBehaviour, IAimTargetView
    {
        public Transform UpperVfxContainer => _upperVfxContainer;
        public Transform FxContainer => _unitStateView.FxContainer;
        public EmotionUnitView EmotionUnitView => _unitStateView.EmotionUnitView;

        [field: SerializeField, Required, ChildGameObjectsOnly]
        public Transform DamageFxTarget { get; private set; }

        [field: SerializeField, Required, ChildGameObjectsOnly]
        public Transform DamageReflectFxTarget { get; private set; }

        [SerializeField] private WeaponView _weaponView;
        [SerializeField] private UnitStateView _unitStateView;
        [SerializeField] private DamageZoneView _damageZoneView;
        [SerializeField] private UnitViewPrefabRoster _prefabRoster;
        [SerializeField] private bool _allowLocamotion = true;
        [SerializeField] private Animator _animator;
        [SerializeField] private Transform _upperVfxContainer;

        [SerializeField, ValidateInput(nameof(HasNullNetworkComponents), NullError)]
        private List<UnitNetworkComponent> _unitNetworkComponents;

        [field: SerializeField, FoldoutGroup("Events")]
        public UnityEvent Initialized { get; private set; }

        public UnitNetwork UnitNetwork { get; private set; }
        public AudioService AudioService { get; private set; }

        public Transform WorldFxContainer => _transform;
        public Bar HealthBar => _unitStateView.HealthBar;
        public EnergyBar EnergyBar => _unitStateView.EnergyBar;
        public IBulletsView BulletsView => _unitStateView.BulletsView;
        public SegmentedBar StaminaBar => _unitStateView.StaminaBar;
        public Vector3 WeaponViewDirection => _weaponView.transform.forward;
        public UnitConfig UnitConfig => UnitNetwork.UnitNetworkState.UnitConfig;

        private SkinnedMeshRenderer[] _renderers;
        public IEnumerable<Renderer> Renderers => _renderers ??= GetComponentsInChildren<SkinnedMeshRenderer>();
        public DamageZoneView DamageZoneView => _damageZoneView;

        private readonly Dictionary<UnitEffectState, UnitViewVFX> _unitEffectViews = new();
        private Transform _transform;
        private float _currentYRotation;
        private bool _isInitialized;
        private float _prevYRotationInput;
        private float _yRotationPredict;
        private WeaponSight _weaponAreaView;

        private DamageZonePlayerSelector _damageZonePlayerSelector;

        public void Init(UnitNetwork unit, IFxRoster fxRoster, IUIBattleMediator uiBattleMediator,
            IReadOnlyObjectCollection<uint, UnitNetwork> unitNetworks, PlayerInput input,
            AudioService audioService)
        {
            UnitNetwork = unit;
            AudioService = audioService;

            SetUnitName(UnitNetwork);
            InitHealthBar(UnitNetwork);
            InitEnergyBar(UnitNetwork);
            InitStaminaBar(UnitNetwork);
            InitBulletsView(unit.UnitNetworkState);

            foreach (var unitNetworkComponent in _unitNetworkComponents)
                unitNetworkComponent.Init(this, audioService);

            // Надо будет спаунить вьюху соответствующюю конфигу
            if (_weaponView != null)
                _weaponView.Init(UnitNetwork, fxRoster.WorldFx.WallImpact, UnitNetwork.RangeWeapon, _animator, audioService);

            _damageZoneView.Init(UnitNetwork);

            if (UnitNetwork.UnitNetworkState.isLocalPlayer)
                _damageZonePlayerSelector = new DamageZonePlayerSelector(UnitNetwork, unitNetworks);

            if (UnitNetwork.UnitType != BattleUnitType.Player)
                _unitStateView.EnemyEffectsView.Init(UnitNetwork.UnitNetworkEffects, UnitNetwork.UnitNetworkState, uiBattleMediator.PlayerUnit, uiBattleMediator);

            UnitNetwork.UnitNetworkEffects.OnAdd += OnAddUnitEffect;
            UnitNetwork.UnitNetworkEffects.OnRemove += OnRemoveUnitEffect;

            _transform = transform;

            if (unit.UnitNetworkState.isLocalPlayer)
            {
                _weaponAreaView = Instantiate(_prefabRoster.WeaponSight, _transform);
                _weaponAreaView.Init(this);

                if (unit.UnitNetworkState.PropertiesProvider?.Impacts.Ability.Config?.Id == "hero_jack_ability")
                    Instantiate(_prefabRoster.AbilityPredictedZoneView, _transform).Init(unit, input, _weaponAreaView.gameObject);

                UnitNetwork.UnitNetworkInput.OnSetYRotation += OnPlayerSetYRotation;
            }

            UnitNetwork.LinkSpawnerView.Started.AddListener(OnUnitsLinkStarted);

            _isInitialized = true;
            Initialized?.Invoke();

            fxRoster.WorldFx.Respawn.Play(_transform);
        }

        private void Update()
        {
            if (!_isInitialized) return;

            UpdateViewRotation();
            UpdateWeaponAreaRotation();
        }

        private void OnDestroy()
        {
            _damageZonePlayerSelector?.Dispose();

            if (UnitNetwork != null)
            {
                UnitNetwork.UnitNetworkEffects.OnAdd -= OnAddUnitEffect;
                UnitNetwork.UnitNetworkEffects.OnRemove -= OnRemoveUnitEffect;
                UnitNetwork.UnitNetworkInput.OnSetYRotation -= OnPlayerSetYRotation;

                UnitNetwork.LinkSpawnerView.Started.RemoveListener(OnUnitsLinkStarted);
            }
        }

        private void OnPlayerSetYRotation(float yRotation)
        {
            _transform.eulerAngles = new Vector3(0, UnitNetwork.UnitNetworkInput.YRotation, 0);
        }

        private void OnUnitsLinkStarted(float radius, TimeSpan duration) =>
            _unitStateView.EffectMessage.ShowStrengtheningMessage();

        private void UpdateViewRotation()
        {
            if (!UnitNetwork.CanMove)
            {
                _transform.localEulerAngles = Vector3.zero;
                return;
            }

            // Юнитов которые умеют бегать только вперёд поворачиваем по направлению движения
            if (!_allowLocamotion && UnitNetwork.UnitNetworkInput.Move != Vector2.zero)
            {
                var moveAngle = Vector2.SignedAngle(Vector2.up, UnitNetwork.UnitNetworkInput.Move.normalized);
                _transform.eulerAngles = new Vector3(0, UnitNetwork.UnitNetworkInput.YRotation - moveAngle, 0);
                return;
            }

            if (!float.IsPositiveInfinity(UnitNetwork.UnitNetworkState.PropertiesProvider!.TurnSpeed))
            {
                _transform.localEulerAngles = Vector3.zero;
                return;
            }

            _transform.eulerAngles = new Vector3(0, UnitNetwork.UnitNetworkInput.YRotation, 0);
        }

        private void UpdateWeaponAreaRotation()
        {
            if (_weaponAreaView == null)
                return;

            var targetYRotation = GetPredictedYRotationInput();
            _prevYRotationInput = UnitNetwork.UnitNetworkInput.YRotation;

            _weaponAreaView.transform.eulerAngles = new Vector3(0, targetYRotation, 0);
        }

        private float GetPredictedYRotationInput()
        {
            var targetYRotationPredict = 0f;
            if (UnitNetwork.UnitNetworkInput.YRotation > _prevYRotationInput)
                targetYRotationPredict = _prefabRoster.PredictYRotation;
            else if (UnitNetwork.UnitNetworkInput.YRotation < _prevYRotationInput) targetYRotationPredict = -_prefabRoster.PredictYRotation;

            _yRotationPredict = Mathf.Lerp(_yRotationPredict, targetYRotationPredict, Time.deltaTime * _prefabRoster.RotateLerpSpeed);

            return UnitNetwork.UnitNetworkInput.YRotation + _yRotationPredict;
        }

        // Animation callback stub

        public void AnimEventStub()
        {
        }

        public void EnableDeadStateView(bool enabled)
        {
            _unitStateView.HealthBar.gameObject.SetActive(!enabled);
            _unitStateView.EnergyBar.gameObject.SetActive(!enabled);
            _unitStateView.StaminaBar?.gameObject.SetActive(!enabled);
            _damageZoneView.gameObject.SetActive(!enabled);
        }

        private void SetUnitName(UnitNetwork unit)
        {
            bool showName = unit.UnitType == BattleUnitType.Player;

            _unitStateView.UnitName.gameObject.SetActive(showName);

            if (showName)
            {
                _unitStateView.UnitName.text = unit.UnitNetworkState.PlayerName;
                _unitStateView.UnitName.color = unit.isLocalPlayer ? _unitStateView.NameColors.SelfColor : _unitStateView.NameColors.FriendColor;
            }
        }

        private void InitHealthBar(UnitNetwork unit)
        {
            UnitNetworkState unitState = unit.UnitNetworkState;

            if(_unitStateView.BarAutoFader)
                _unitStateView.BarAutoFader.enabled = !unit.UnitNetworkState.isLocalPlayer;

            _unitStateView.HealthBar.Set(unitState.Health, unitState.MaxHealth);
            _unitStateView.HealthBar.Color.UpdateColor(unitState.isLocalPlayer, unitState.UnitType);

            if(!unit.UnitNetworkState.isLocalPlayer)
                _unitStateView.HealthBar.Fader.FadeInstantly();
        }

        private void InitEnergyBar(UnitNetwork unit)
        {
            UnitNetworkState unitState = unit.UnitNetworkState;

            _unitStateView.EnergyBar.Set(unitState.Energy, unitState.MaxEnergy);
        }

        private void SetHealthBarInvincibleState(bool isInvincible)
        {
            _unitStateView.HealthBar.SetInvincibleState(isInvincible);
            _unitStateView.BarAutoFader.enabled = !isInvincible;
            if(isInvincible)
                _unitStateView.HealthBar.Fader.UnfadeInstantly();
        }

        private void InitStaminaBar(UnitNetwork unit)
        {
            if (_unitStateView.StaminaBar == null)
                return;

            UnitNetworkState unitState = unit.UnitNetworkState;

            if (!unitState.UnitConfig.UnitActionRoster.TryGetAction(UnitActionTypes.Dodge, out var dodgeAction))
                return;

            _unitStateView.StaminaBar.Set(unitState.Stamina, unitState.MaxStamina, dodgeAction.StaminaCost);
            _unitStateView.StaminaBar.Color.UpdateColor(unitState.isLocalPlayer, unitState.UnitType);
            _unitStateView.StaminaBar.Fader.FadeInstantly();
            _unitStateView.StaminaBar.NetworkComponent.NetId = unit.NetworkIdentity.netId;
        }

        private void InitBulletsView(UnitNetworkState unitNetworkState)
        {
            if(BulletsView == null)
                return;

            BulletsView.Init(unitNetworkState);
        }

        private void OnRemoveUnitEffect(UnitEffectState state)
        {
            //Снимает подсветку неуязвимости на хп баре даже если было два эффекта неуязвимости, а отменился только один
            if(state.Config.Type == UnitEffectTypes.DamageZone && state.Config.DamageZones.DefaultZone.Multiplier == 0)
                SetHealthBarInvincibleState(false);

            if (!_unitEffectViews.ContainsKey(state))
                return;

            if (_unitEffectViews[state] != null)
                _unitEffectViews[state].Dispose();

            _unitEffectViews.Remove(state);
        }

        private void OnAddUnitEffect(UnitEffectState state)
        {
            if(state.Config.Type == UnitEffectTypes.DamageZone && state.Config.DamageZones.DefaultZone.Multiplier == 0)
                SetHealthBarInvincibleState(true);

            if (_unitEffectViews.ContainsKey(state) || state.Config!.ViewVFX == null)
                return;

            var view = Instantiate(state.Config.ViewVFX, transform);
            view.Init(state, this);

            _unitEffectViews.Add(state, view);
        }
    }
}