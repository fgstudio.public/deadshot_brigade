using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Targeting
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/HUD-2a3d0156e3f142039d4bf1ee9d00a7b9")]
    [AddComponentMenu(BattleComponentMenus.Client.Targeting.Menu + "/" + nameof(TargetMarkerComponent))]
    public sealed class TargetMarkerComponent : Component
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _markerPoint;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject[] _activeObjects;

        [Header("Settings")]
        [SerializeField, Required, AssetsOnly] private Sprite _icon;
        [SerializeField, Required, AssetsOnly] private Color _backgroundColor;
        [SerializeField] private bool _animationIsOn;

        public Sprite Icon => _icon;
        public Color BackgroundColor => _backgroundColor;
        public Transform MarkerPoint => _markerPoint;
        public bool AnimationIsOn => _animationIsOn;

        private void OnValidate() => _markerPoint ??= GetComponentInChildren<Transform>();

        protected override void OnEnabled() => _activeObjects.ForEach(e => e.SetActive(true));
        protected override void OnDisabled() => _activeObjects.ForEach(e => e.SetActive(false));
    }
}