using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Targeting
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/HUD-2a3d0156e3f142039d4bf1ee9d00a7b9")]
    [AddComponentMenu(BattleComponentMenus.Client.Targeting.Menu + "/" + nameof(TargetMarkerAnimator))]
    public sealed class TargetMarkerAnimator : MonoBehaviour
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Animator _animator;

        [Header("Settings")]
        [SerializeField, Required] private string _activeParameterName;

        private void OnValidate() => _animator ??= GetComponentInChildren<Animator>();

        [Button] public void PlayActivating() => _animator.SetBool(_activeParameterName, true);
        [Button] public void PlayDeactivating() => _animator.SetBool(_activeParameterName, false);

    }
}