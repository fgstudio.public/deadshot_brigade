﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Network.Shared.KillStreak;
using Frostgate.RiftHunters.Core.UI.Hud.KillStreak;
using JetBrains.Annotations;
using UnityEditor;

namespace Frostgate.RiftHunters.Core.Battle.Client.KillStreak
{
    public class KillStreakClientHandler : IDisposable
    {
        [NotNull] private readonly SimulationTime _simulationTime;
        [NotNull] private readonly ILogger _logger;

        [NotNull] private readonly IReadOnlyKillStreakState _streakState;

        [NotNull] private readonly UiKillStreak _uiKillStreak;

        private readonly PercentTimer _timer;
        private readonly TimeSpan _timerInterval;

        private int _lastKillStreak;

        public KillStreakClientHandler(
            [NotNull] SimulationTime simulationTime,
            [NotNull] ILogger logger,
            [NotNull] IReadOnlyKillStreakState streakState,
            [NotNull] KillStreakConfig config,
            [NotNull] UiKillStreak uiKillStreak)
        {
            _simulationTime = simulationTime;
            _logger = logger;
            _streakState = streakState;
            _uiKillStreak = uiKillStreak;

            _timerInterval = config.StreakCooldown;
            _timer = new PercentTimer(_timerInterval);

            _timer.Started += OnStarted;
            _timer.Elapsed += OnElapsed;

            _streakState.StreakCountChanged += OnStreakCountChanged;
        }

        public void Dispose()
        {
            _timer.Started -= OnStarted;
            _timer.Elapsed -= OnElapsed;

            _streakState.StreakCountChanged -= OnStreakCountChanged;

            _timer?.Dispose();
        }

        private void OnStarted()
        {
            _uiKillStreak.ShowStreak();
        }

        private void OnElapsed()
        {
            _uiKillStreak.ShowResult();
            _logger.Log($"Total kill streak: {_lastKillStreak}. Streak finished!");
        }

        private void OnStreakCountChanged(KillStreakData streakData)
        {
            _timer.Stop();

            TimeSpan timeFromLastKill = TimeSpan
                .FromSeconds(_simulationTime.Time - streakData.TimeKill);
            TimeSpan passed = _timerInterval - timeFromLastKill;

            _timer.SetPassedTime(passed);
            _timer.Start();

            _lastKillStreak = _streakState.StreakCount;
            _uiKillStreak.SetKillCount(_lastKillStreak);

            _logger.Log($"Kill streak updated! Streak: {_lastKillStreak}");
        }
    }
}