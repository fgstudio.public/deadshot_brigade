﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.Client.KillStreak
{
    public sealed class PercentTimer : ITimer
    {
        public event UnityAction Started;
        public event UnityAction Paused;
        public event UnityAction Stopped;
        public event UnityAction Ticked;
        public event UnityAction Elapsed;

        public double PassedPercent => Passed.TotalSeconds / Interval.TotalSeconds;
        public TimeSpan Interval { get; }
        public TimeSpan Left => Interval - Passed;
        public TimeSpan Passed { get; private set; }
        public bool IsElapsed => Left.Ticks == 0;

        public bool IgnoreTimeScale { get; }
        public UniTask Task { get; private set; }
        public bool IsPaused { get; private set; }

        private CancellationTokenSource _tokenSource;

        public PercentTimer(TimeSpan interval, bool ignoreTimeScale = false)
        {
            Interval = interval;
            IgnoreTimeScale = ignoreTimeScale;
        }

        public void Dispose()
        {
            CancelToken();
        }

        public void Start()
        {
            if (IsPaused)
            {
                IsPaused = false;
            }
            else
            {
                CancelToken();
                Passed = TimeSpan.Zero;

                _tokenSource = new CancellationTokenSource();
                Task = TimerRoutine(_tokenSource.Token);
            }

            Started?.Invoke();
        }

        public void Pause()
        {
            if (!IsPaused)
            {
                IsPaused = true;
                Paused?.Invoke();
            }
        }

        public void Stop()
        {
            CancelToken();
            IsPaused = default;
            Task = UniTask.CompletedTask;

            Stopped?.Invoke();
        }

        public void SetPassedTime(TimeSpan passedTime) =>
            Passed = passedTime < Interval ? passedTime : Interval;

        private async UniTask TimerRoutine(CancellationToken token)
        {
            while (!token.IsCancellationRequested && Left.TotalSeconds > 0)
            {
                await UniTask.Yield();
                if (!IsPaused) Tick();
            }

            if (!token.IsCancellationRequested)
                Elapsed?.Invoke();
        }

        private void CancelToken()
        {
            if (_tokenSource != null)
            {
                _tokenSource?.Cancel();
                _tokenSource?.Dispose();
                _tokenSource = null;
            }
        }

        private void Tick()
        {
            Passed += CalcDeltaTime(IgnoreTimeScale);
            Ticked?.Invoke();
        }

        private TimeSpan CalcDeltaTime(bool ignoreTimeScale)
        {
            float deltaTime = ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime;
            return TimeSpan.FromSeconds(deltaTime);
        }
    }
}