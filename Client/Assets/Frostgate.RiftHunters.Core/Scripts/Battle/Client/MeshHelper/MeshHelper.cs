using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Client
{
    public static class MeshHelper
    {
        public const int PolygonVerticesCount = 4;

        public static readonly List<Vector3> VerticesCache = new(1024);
        public static readonly List<int> TrianglesCache = new(1024);
        public static readonly List<Color> VerticesColorCache = new(1024);
    }
}