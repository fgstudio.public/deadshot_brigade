using UnityEngine;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;

namespace Frostgate.RiftHunters.Core.Battle.Client.ShootingTarget
{
    public class BarrelView : ShootingTargetView
    {
        [Header("References")]
        [SerializeField] private GameObject _renderGameObject;
        [SerializeField] private GameObject _explosionFxGameObject;
        [SerializeField] private GameObject _fireFxGameObject;
        [SerializeField] private Bar _healthBar;

        [Header("Settings")]
        [SerializeField, Min(0)] private float _hpRatioToEnableFireFx;

        private ShootingTargetNetwork _target;

        public override void Init(ShootingTargetNetwork target, SharedDependencies _)
        {
            _target = target;

            _target.OnDeath += OnDeath;
            _target.HealthChanged += OnHealthChanged;
            _target.OnRespawn += OnRespawn;

            if (_target.IsDead)
            {
                _renderGameObject.SetActive(false);
                _fireFxGameObject.SetActive(false);
                _explosionFxGameObject.SetActive(false);
            }
        }

        private void OnDestroy()
        {
            if (_target != null)
            {
                _target.OnDeath -= OnDeath;
                _target.HealthChanged -= OnHealthChanged;
                _target.OnRespawn -= OnRespawn;
            }
        }

        private void OnRespawn(ShootingTargetNetwork targetNetwork)
        {
            _renderGameObject.SetActive(true);
            _fireFxGameObject.SetActive(false);
            _explosionFxGameObject.SetActive(false);
        }

        private void OnHealthChanged(float diff)
        {
            if (_target.IsDead)
                return;

            _healthBar.Set(_target.Health, _target.MaxHealth);
            _fireFxGameObject.SetActive(_target.Health / _target.MaxHealth <= _hpRatioToEnableFireFx);
        }

        private void OnDeath(ShootingTargetNetwork target)
        {
            _healthBar.Set(0, _target.MaxHealth);
            _renderGameObject.SetActive(false);
            _fireFxGameObject.SetActive(false);
            _explosionFxGameObject.SetActive(true);
        }
    }
}