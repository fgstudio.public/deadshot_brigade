using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;

namespace Frostgate.RiftHunters.Core.Battle.Client.ShootingTarget
{
    public class AimTargetView : ShootingTargetView
    {
        [SerializeField] private AimTargetViewPart[] _parts;

        private Vector3 _lastAttackDir;
        private ShootingTargetNetwork _target;

        [CanBeNull] private IDamageService _damageService;

        public override void Init(ShootingTargetNetwork target, SharedDependencies sharedDependencies)
        {
            _target = target;
            _damageService = sharedDependencies.ServiceRoster.DamageService;

            foreach (var part in _parts)
                Physics.IgnoreCollision(part.Collider, _target.AimTarget);

            _target.OnRespawn += OnRespawn;
            _target.HealthChanged += OnHealthChanged;

            _damageService!.Damaged.AddListener(OnDamaged);
        }

        private void OnDestroy()
        {
            if (_target != null)
            {
                _target.HealthChanged -= OnHealthChanged;
                _target.OnRespawn -= OnRespawn;
            }

            if (_damageService != null)
                _damageService.Damaged.RemoveListener(OnDamaged);
        }

        private void OnHealthChanged(float diff)
        {
            foreach (var part in _parts)
                if (!part.Detached && part.HealthPercentLimit >= _target.Health / _target.MaxHealth)
                    part.Detach(_lastAttackDir);
        }

        private void OnRespawn(ShootingTargetNetwork targetNetwork)
        {
            foreach (var part in _parts)
                part.Reset();
        }

        private void OnDamaged(DamageEventData data)
        {
            if (data.ReceiverNetId == _target.netId &&
                NetworkClient.spawned.TryGetValue(data.CasterNetId, out NetworkIdentity casterIdentity))
                _lastAttackDir = transform.position - casterIdentity.transform.position;
        }
    }
}