using UnityEngine;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;

namespace Frostgate.RiftHunters.Core.Battle.Client.ShootingTarget
{
    public abstract class ShootingTargetView : MonoBehaviour, IAimTargetView
    {
        public Transform FxContainer => damageFxPivot;
        public Transform WorldFxContainer => transform;
        public Transform DamageFxTarget => damageFxPivot;
        public Transform DamageReflectFxTarget => damageFxPivot;

        [Header("Base References")]
        [SerializeField] private Transform damageFxPivot;

        //todo: циклическая зависимость, необходимо отрефакторить
        public abstract void Init(ShootingTargetNetwork target, SharedDependencies sharedDependencies);
    }
}