using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.ShootingTarget
{
    public class AimTargetViewPart : MonoBehaviour
    {
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private Collider _collider;
        [SerializeField] private float _healthPercentLimit;

        public bool Detached => !_rigidbody.isKinematic;
        public float HealthPercentLimit => _healthPercentLimit;
        public Collider Collider => _collider;

        private Vector3 _startLocalPosition;
        private Quaternion _startLocalRotation;

        private void Start()
        {
            _startLocalPosition = transform.localPosition;
            _startLocalRotation = transform.localRotation;
        }

        public void Detach(Vector3 forceVector)
        {
            _collider.enabled = true;
            _rigidbody.isKinematic = false;

            const float minSpeed = 3, maxSpeed = 5;
            _rigidbody.velocity = forceVector.normalized * Random.Range(minSpeed, maxSpeed);

            float GetRandomAngularSpeed()
            {
                const float minAngularSpeed = -50, maxAngularSpeed = 50;
                return Random.Range(minAngularSpeed, maxAngularSpeed);
            }

            _rigidbody.angularVelocity = new Vector3(
                GetRandomAngularSpeed(),
                GetRandomAngularSpeed(),
                GetRandomAngularSpeed());
        }

        public void Reset()
        {
            _collider.enabled = false;
            _rigidbody.isKinematic = true;

            transform.localPosition = _startLocalPosition;
            transform.localRotation = _startLocalRotation;
        }
    }
}