﻿using Frostgate.RiftHunters.Core.Battle.Impact;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    public readonly struct ImpactCastStartedEvent
    {
        public string ImpactId { get; }

        public ImpactCastStartedEvent([NotNull] ImpactConfig impact) : this(impact.Id)
        {
        }

        public ImpactCastStartedEvent(string impactId)
        {
            ImpactId = impactId;
        }
    }
}