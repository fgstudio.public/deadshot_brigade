﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    public readonly struct OutcomingDamageEvent
    {
        [NotNull] public IWeapon Weapon { get; }

        public OutcomingDamageEvent([NotNull] IWeapon weapon)
        {
            Weapon = weapon;
        }
    }
}