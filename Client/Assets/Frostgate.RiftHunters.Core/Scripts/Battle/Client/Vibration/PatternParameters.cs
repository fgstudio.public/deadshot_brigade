﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    [Serializable]
    public sealed class PatternParameters : IVibrationPattern
    {
        IReadOnlyList<VibrationInterval> IVibrationPattern.Intervals => _intervals ??= new List<VibrationInterval>();

        [SerializeField] private List<VibrationInterval> _intervals;
    }
}