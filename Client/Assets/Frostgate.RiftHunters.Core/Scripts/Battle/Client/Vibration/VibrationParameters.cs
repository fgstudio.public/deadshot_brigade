﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    [Serializable]
    public struct VibrationParameters
    {
        [field: SerializeField] public VibrationType Type { get; private set; }

        [field: SerializeField, ShowIf(nameof(Type), VibrationType.OneShot)]
        public OneShotParameters OneShotParameters { get; private set; }

        [field: SerializeField, ShowIf(nameof(Type), VibrationType.Pattern)]
        public PatternParameters PatternParameters { get; private set; }

        [field: SerializeField] public VibrationCooldown Cooldown { get; private set; }
    }
}