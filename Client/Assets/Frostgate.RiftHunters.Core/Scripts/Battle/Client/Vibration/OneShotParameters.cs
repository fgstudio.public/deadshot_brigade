﻿using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    [Serializable]
    public sealed class OneShotParameters
    {
        [field: SerializeField, Min(0)] public int DurationMs { get; private set; }
        [field: SerializeField, Range(1, 255)] public byte Intensity { get; private set; }
    }
}