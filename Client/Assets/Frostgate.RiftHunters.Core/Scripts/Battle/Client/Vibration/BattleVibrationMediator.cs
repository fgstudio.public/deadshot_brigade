﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    public sealed class BattleVibrationMediator : IObserver<IncomingDamageEvent>, IObserver<OutcomingDamageEvent>,
        IObserver<ImpactCastStartedEvent>
    {
        [NotNull] private readonly IVibrator _vibrator;
        [NotNull] private readonly IEventVibrationStorage<IncomingDamageEvent> _ideParamsStorage;
        [NotNull] private readonly IEventVibrationStorage<OutcomingDamageEvent> _odeParamsStorage;
        [NotNull] private readonly IEventVibrationStorage<ImpactCastStartedEvent> _icsParamsStorage;
        [NotNull] private readonly IEventVibrationStorage<InputButtonClickedEvent> _ibcParamsStorage;

        private HashSet<Type> _eventsCooldowns = new();

        private bool _isGlobalCooldownActive;

        public BattleVibrationMediator([NotNull] IVibrator vibrator,
            [NotNull] IEventVibrationStorage<IncomingDamageEvent> ideParamsStorage,
            [NotNull] IEventVibrationStorage<OutcomingDamageEvent> odeParamsStorage,
            [NotNull] IEventVibrationStorage<ImpactCastStartedEvent> icsParamsStorage,
            [NotNull] IEventVibrationStorage<InputButtonClickedEvent> ibcParamsStorage)
        {
            _vibrator = vibrator;
            _ideParamsStorage = ideParamsStorage;
            _odeParamsStorage = odeParamsStorage;
            _icsParamsStorage = icsParamsStorage;
            _ibcParamsStorage = ibcParamsStorage;
        }

        public void HandleEvent(IncomingDamageEvent @event) => VibrateOnEvent(@event, _ideParamsStorage);

        public void HandleEvent(OutcomingDamageEvent @event) => VibrateOnEvent(@event, _odeParamsStorage);

        public void HandleEvent(ImpactCastStartedEvent @event) => VibrateOnEvent(@event, _icsParamsStorage);

        public void HandleEvent(InputButtonClickedEvent @event) => VibrateOnEvent(@event, _ibcParamsStorage);

        private void VibrateOnEvent<T>(T @event, IEventVibrationStorage<T> storage)
        {
            if (storage.TryGetVibrationParameters(@event, out VibrationParameters parameters))
                Vibrate<T>(parameters);
        }

        private void Vibrate<TEvent>(VibrationParameters parameters)
        {
            if (IsCooldownEnabled<TEvent>())
                return;

            switch (parameters.Type)
            {
                case VibrationType.OneShot:
                    Vibrate(parameters.OneShotParameters);
                    break;

                case VibrationType.Pattern:
                    Vibrate(parameters.PatternParameters);
                    break;
            }

            if (parameters.Cooldown.DurationMs > 0)
                EnableCooldown<TEvent>(parameters.Cooldown);
        }

        private void Vibrate(OneShotParameters oneShot) => _vibrator.Vibrate(oneShot.DurationMs, oneShot.Intensity);

        private void Vibrate(PatternParameters pattern) => _vibrator.Vibrate(pattern);

        private bool IsCooldownEnabled<TEvent>() =>
            _isGlobalCooldownActive || _eventsCooldowns.Contains(typeof(TEvent));

        private async void EnableCooldown<TEvent>(VibrationCooldown cooldown)
        {
            if (cooldown.IsGlobal)
            {
                _isGlobalCooldownActive = true;
                await UniTask.Delay(cooldown.DurationMs).ContinueWith(() => _isGlobalCooldownActive = false);
            }
            else
            {
                _eventsCooldowns.Add(typeof(TEvent));
                await UniTask.Delay(cooldown.DurationMs).ContinueWith(() => _eventsCooldowns.Remove(typeof(TEvent)));
            }
        }
    }
}