﻿using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    [Serializable]
    public struct VibrationCooldown
    {
        [field: SerializeField, Min(0)] public int DurationMs { get; private set; }
        [field: SerializeField] public bool IsGlobal { get; private set; }
    }
}