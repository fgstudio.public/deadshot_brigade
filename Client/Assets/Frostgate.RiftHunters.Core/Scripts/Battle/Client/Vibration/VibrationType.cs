﻿namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    public enum VibrationType : byte
    {
        OneShot,
        Pattern
    }
}