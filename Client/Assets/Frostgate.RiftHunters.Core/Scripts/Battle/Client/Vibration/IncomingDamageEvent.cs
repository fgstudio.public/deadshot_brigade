﻿using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    public readonly struct IncomingDamageEvent
    {
        public DamageType Type { get; }
        public float DamageValue { get; }

        public IncomingDamageEvent(DamageType type, float damageValue)
        {
            Type = type;
            DamageValue = damageValue;
        }
    }
}