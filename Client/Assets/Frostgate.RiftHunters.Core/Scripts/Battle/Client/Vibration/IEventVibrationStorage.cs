﻿namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    public interface IEventVibrationStorage<in T>
    {
        bool TryGetVibrationParameters(T @event, out VibrationParameters parameters);
    }
}