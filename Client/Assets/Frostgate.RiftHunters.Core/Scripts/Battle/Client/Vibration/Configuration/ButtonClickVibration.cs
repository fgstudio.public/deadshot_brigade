using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    [Serializable]
    public struct ButtonClickVibration
    {
        [field: SerializeField] public VibrationParameters Parameters { get; private set; }
    }
}
