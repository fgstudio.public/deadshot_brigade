﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    [Serializable]
    public struct OutcomingDamageVibration
    {
        [field: SerializeField, ValidateInput(nameof(AreConfigNotNull), "Config cannot be a null.")]
        public WeaponConfig Weapon { get; private set; }

        [field: SerializeField] public VibrationParameters Parameters { get; private set; }

        private bool AreConfigNotNull(WeaponConfig config) => config != null;
    }
}