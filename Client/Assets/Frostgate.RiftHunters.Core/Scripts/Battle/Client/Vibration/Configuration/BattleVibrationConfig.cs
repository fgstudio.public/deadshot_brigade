﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    [CreateAssetMenu(menuName = BattleAssetMenus.Client.Menu + "/" + nameof(BattleVibrationConfig),
        fileName = nameof(BattleVibrationConfig))]
    public sealed class BattleVibrationConfig : ScriptableObject, IEventVibrationStorage<IncomingDamageEvent>,
        IEventVibrationStorage<OutcomingDamageEvent>, IEventVibrationStorage<ImpactCastStartedEvent>,
        IEventVibrationStorage<InputButtonClickedEvent>, IConfig
    {
        [ShowInInspector, ReadOnly] public string Id => name;

        [Header("Configuration")] [SerializeField, LabelText("Реакции на входящий урон")]
        private List<IncomingDamageVibration> _idvConfigs = new();

        [SerializeField, LabelText("Реакции на исходящий урон")]
        private List<OutcomingDamageVibration> _odvConfigs = new();

        [SerializeField, LabelText("Реакции на применение способностей")]
        private List<UltimateAppliedVibration> _icsConfigs = new();

        [SerializeField, LabelText("Реакции на нажатие кнопок")]
        private List<ButtonClickVibration> _ibcConfigs = new();

        bool IEventVibrationStorage<IncomingDamageEvent>.TryGetVibrationParameters(IncomingDamageEvent @event,
            out VibrationParameters parameters)
        {
            foreach (IncomingDamageVibration vibration in _idvConfigs)
            {
                if (vibration.Type != @event.Type || vibration.MinDamageTreshold > @event.DamageValue)
                    continue;

                parameters = vibration.Parameters;
                return true;
            }

            parameters = default;
            return false;
        }

        bool IEventVibrationStorage<OutcomingDamageEvent>.TryGetVibrationParameters(OutcomingDamageEvent @event,
            out VibrationParameters parameters)
        {
            foreach (OutcomingDamageVibration vibration in _odvConfigs)
            {
                if (vibration.Weapon.Id != @event.Weapon.Id)
                    continue;

                parameters = vibration.Parameters;
                return true;
            }

            parameters = default;
            return false;
        }

        bool IEventVibrationStorage<ImpactCastStartedEvent>.TryGetVibrationParameters(ImpactCastStartedEvent @event,
            out VibrationParameters parameters)
        {
            foreach (UltimateAppliedVibration vibration in _icsConfigs)
            {
                if (vibration.Impact.Id != @event.ImpactId)
                    continue;

                parameters = vibration.Parameters;
                return true;
            }

            parameters = default;
            return default;
        }

        bool IEventVibrationStorage<InputButtonClickedEvent>.TryGetVibrationParameters(InputButtonClickedEvent @event,
            out VibrationParameters parameters)
        {
            foreach (ButtonClickVibration vibration in _ibcConfigs)
            {
                parameters = vibration.Parameters;
                return true;
            }

            parameters = default;
            return default;
        }
    }
}