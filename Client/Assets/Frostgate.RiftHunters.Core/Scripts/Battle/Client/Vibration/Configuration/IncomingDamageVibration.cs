﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    [Serializable]
    public struct IncomingDamageVibration
    {
        [field: SerializeField] public DamageType Type { get; private set; }
        [field: SerializeField, Min(0)] public float MinDamageTreshold { get; private set; }

        [field: SerializeField] public VibrationParameters Parameters { get; private set; }
    }
}