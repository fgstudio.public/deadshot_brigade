﻿using System;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Vibration
{
    [Serializable]
    public struct UltimateAppliedVibration
    {
        [field: SerializeField, ValidateInput(nameof(AreConfigNotNull), "Config cannot be a null.")]
        public ImpactConfig Impact { get; private set; }

        [field: SerializeField] public VibrationParameters Parameters { get; private set; }

        private bool AreConfigNotNull(ImpactConfig config) => config != null;
    }
}