using UnityEngine;
using UnityEngine.Events;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public abstract class Fx : MonoBehaviour, IPoolObject, IAutoReleased
    {
        private UnityAction<IAutoReleased> _readyForReleaseAction;
        event UnityAction<IAutoReleased> IAutoReleased.ReadyForRelease
        {
            add => _readyForReleaseAction += value;
            remove => _readyForReleaseAction -= value;
        }

        void IPoolObject.OnGet() => OnGet();
        void IPoolObject.OnRelease() => OnRelease();

        protected virtual void OnGet() { }
        protected virtual void OnRelease() { }

        protected void InvokeReadyForRelease() => _readyForReleaseAction?.Invoke(this);
    }
}
