using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IFxRoster
    {
        IWorldFxRoster WorldFx { get; }
        IUiVfxRoster UiVfx { get; }
    }

    public sealed class FxRoster : IFxRoster
    {
        public IWorldFxRoster WorldFx { get; }
        public IUiVfxRoster UiVfx { get; }

        public FxRoster([NotNull] IWorldFxRoster worldFxRoster, [NotNull] IUiVfxRoster uiVfxRoster)
        {
            WorldFx = worldFxRoster;
            UiVfx = uiVfxRoster;
        }
    }
}
