using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IRespawnFxSystem
    {
        void Play([NotNull] Transform target);
    }

    public sealed class RespawnFxSystem : IRespawnFxSystem
    {
        private readonly IPool<Fx> _pool;

        public RespawnFxSystem([NotNull] IPool<Fx> pool) =>
            _pool = pool;

        public void Play(Transform target)
        {
            Fx fx = _pool.Get();
            Transform fxTransform = fx.transform;

            fxTransform.position = target.position;
            fxTransform.rotation = target.rotation;
        }
    }
}
