﻿using Frostgate.RiftHunters.Core.Battle.Client.FX.Heal;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    [DisallowMultipleComponent]
    public sealed class WorldFxInstaller : FeatureInstaller
    {
        [Header("WorldFx pools configuration"), SerializeField, Required]
        private PrefabPoolConfig<Fx, ReferencePoolConfig> _damageFxConfig;

        [SerializeField, Required] private PrefabPoolConfig<Fx, ReferencePoolConfig> _respawnFxConfig;
        [SerializeField, Required] private PrefabPoolConfig<Fx, ReferencePoolConfig> _wallImpactFxConfig;
        [SerializeField, Required] private PrefabPoolConfig<Fx, ReferencePoolConfig> _shieldImpactFxConfig;
        [SerializeField, Required] private PrefabPoolConfig<Fx, ReferencePoolConfig> _healFxConfig;

        protected override void InstallClientPart()
        {
            Container.Bind<IFxRoster>().To<FxRoster>().AsSingle();
            Container.Bind<IWorldFxRoster>().FromMethod(CreateRoster).AsSingle();
        }

        private IWorldFxRoster CreateRoster() =>
            new WorldFxRoster(CreateDamageSystem(), CreateRespawnSystem(),
                CreateRespawnImpactSystem(), CreateRespawnShieldImpactFxSystem(), CreateHealFxSystem());

        private IHealFxSystem CreateHealFxSystem()
        {
            IPool<Fx> pool = PoolFactory.CreateDefaultAutoReleasingPool(_healFxConfig);

            return new HealFxSystem(pool);        }

        private IDamageFxSystem CreateDamageSystem()
        {
            IPool<Fx> pool = PoolFactory.CreateDefaultAutoReleasingPool(_damageFxConfig);
            var transformer = Container.Instantiate<DamageFxTransformer>();

            return new DamageFxSystem(pool, transformer);
        }

        private IRespawnFxSystem CreateRespawnSystem()
        {
            IPool<Fx> pool = PoolFactory.CreateDefaultAutoReleasingPool(_respawnFxConfig);

            return new RespawnFxSystem(pool);
        }

        private IWallImpactFxSystem CreateRespawnImpactSystem()
        {
            IPool<Fx> pool = PoolFactory.CreateDefaultAutoReleasingPool(_wallImpactFxConfig);

            return new WallImpactFxSystem(pool);
        }

        private IShieldImpactFxSystem CreateRespawnShieldImpactFxSystem()
        {
            IPool<Fx> pool = PoolFactory.CreateDefaultAutoReleasingPool(_shieldImpactFxConfig);
            var transformer = Container.Instantiate<ShieldImpactFxTransformer>();

            return new ShieldImpactFxSystem(pool, transformer);
        }
    }
}
