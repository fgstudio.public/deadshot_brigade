using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IDamageFxTransformer
    {
        void Transform([NotNull] Transform fx, [CanBeNull] Transform parent, float targetRadius = 0);
    }

    public sealed class DamageFxTransformer : IDamageFxTransformer
    {
        private readonly IRandom _random;

        public DamageFxTransformer([NotNull] IRandom random) =>
            _random = random;

        public void Transform(Transform fx, Transform parent, float targetRadius = 0)
        {
            Quaternion randomRotation = Quaternion.Euler(0, _random.Range(0, 360), 0);
            Vector3 randomOffset = randomRotation * (Vector3.forward * targetRadius);

            fx.SetParent(parent);
            fx.localPosition = randomOffset;
            fx.localRotation = randomRotation;
        }
    }
}
