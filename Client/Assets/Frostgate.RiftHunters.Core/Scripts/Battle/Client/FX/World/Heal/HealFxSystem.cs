using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX.Heal
{
    public interface IHealFxSystem
    {
        void Play([NotNull] Transform target);
    }

    public class HealFxSystem: IHealFxSystem
    {
        private readonly IPool<Fx> _pool;

        public HealFxSystem([NotNull] IPool<Fx> pool) =>
            _pool = pool;

        public void Play(Transform target)
        {
            Fx fx = _pool.Get();
            Transform fxTransform = fx.transform;

            fxTransform.position = target.position;
            fxTransform.rotation = target.rotation;
        }
    }
}