using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IShieldImpactFxTransformer
    {
        void Transform([NotNull] Transform fx, [CanBeNull] Transform parent);
    }

    public sealed class ShieldImpactFxTransformer : IShieldImpactFxTransformer
    {
        private static readonly Vector2 positionRange = new(0.8f, 1f);

        private readonly AreaRandom _random;

        public ShieldImpactFxTransformer([NotNull] AreaRandom random) =>
            _random = random;

        public void Transform(Transform fx, Transform parent)
        {
            fx.SetParent(parent, false);
            fx.localPosition = _random.RectZ(Vector3.zero, positionRange);
        }
    }
}
