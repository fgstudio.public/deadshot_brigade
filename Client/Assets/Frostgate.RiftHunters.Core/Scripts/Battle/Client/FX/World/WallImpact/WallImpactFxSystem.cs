using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IWallImpactFxSystem
    {
        void Play(RaycastHit raycastHit);
    }

    public sealed class WallImpactFxSystem : IWallImpactFxSystem
    {
        private readonly IPool<Fx> _pool;

        public WallImpactFxSystem([NotNull] IPool<Fx> pool) =>
            _pool = pool;

        public void Play(RaycastHit raycastHit)
        {
            Fx fx = _pool.Get();
            Transform fxTransform = fx.transform;

            fxTransform.position = raycastHit.point;
            fxTransform.rotation = Quaternion.LookRotation(raycastHit.normal);
        }
    }
}
