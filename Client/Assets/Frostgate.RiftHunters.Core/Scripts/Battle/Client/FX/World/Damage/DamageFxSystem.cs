using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IDamageFxSystem
    {
        void Play([NotNull] Transform target, float damage, float targetRadius = 0);
    }

    public sealed class DamageFxSystem : IDamageFxSystem
    {
        private readonly IPool<Fx> _pool;
        private readonly IDamageFxTransformer _transformer;

        public DamageFxSystem([NotNull] IPool<Fx> pool, [NotNull] IDamageFxTransformer transformer)
        {
            _pool = pool;
            _transformer = transformer;
        }

        public void Play(Transform target, float damage, float targetRadius = 0)
        {
            if (damage <= 0) return;
            Fx fx = _pool.Get();
            _transformer.Transform(fx.transform, target, targetRadius);
        }
    }
}
