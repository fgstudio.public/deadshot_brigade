using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IShieldImpactFxSystem
    {
        void Play([CanBeNull] Transform parent);
    }

    public sealed class ShieldImpactFxSystem : IShieldImpactFxSystem
    {
        private readonly IPool<Fx> _pool;
        private readonly IShieldImpactFxTransformer _transformer;

        public ShieldImpactFxSystem([NotNull] IPool<Fx> pool, IShieldImpactFxTransformer transformer)
        {
            _pool = pool;
            _transformer = transformer;
        }

        public void Play(Transform parent)
        {
            Fx fx = _pool.Get();
            _transformer.Transform(fx.transform, parent);
        }
    }
}
