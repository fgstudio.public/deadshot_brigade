using Frostgate.RiftHunters.Core.Battle.Client.FX.Heal;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IWorldFxRoster
    {
        IDamageFxSystem Damage { get; }
        IRespawnFxSystem Respawn { get; }
        IWallImpactFxSystem WallImpact { get; }
        IShieldImpactFxSystem ShieldImpact { get; }
        IHealFxSystem Heal { get; }
    }

    public sealed class WorldFxRoster : IWorldFxRoster
    {
        public IDamageFxSystem Damage { get; }
        public IRespawnFxSystem Respawn { get; }
        public IWallImpactFxSystem WallImpact { get; }
        public IShieldImpactFxSystem ShieldImpact { get; }
        public IHealFxSystem Heal { get; }

        public WorldFxRoster(
            [NotNull] IDamageFxSystem damageSystem, [NotNull] IRespawnFxSystem respawnSystem,
            [NotNull] IWallImpactFxSystem wallImpactSystem, [NotNull] IShieldImpactFxSystem shieldImpact, [NotNull] IHealFxSystem heal)
        {
            Damage = damageSystem;
            Respawn = respawnSystem;
            WallImpact = wallImpactSystem;
            ShieldImpact = shieldImpact;
            Heal = heal;
        }
    }
}
