using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX.ArenaGateAnimation
{
    public class ShowByCameraTargetAnimation : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private string _showTrigger;
        [SerializeField] private string _hideTrigger;
        [SerializeField] private Transform _showArea;

        private BattleCamera _battleCamera;
        private bool _visible;

        [Inject]
        private void MonoConstructor([InjectOptional] BattleCamera battleCamera) =>
            _battleCamera = battleCamera;

        private void OnDisable()
        {
            CancelInvoke(nameof(Check));
            SetVisibility(false);
        }

        private void OnEnable() =>
            InvokeRepeating(nameof(Check), Random.value, 1);

        // Чекать по триггеру было проблематично, так как надо только нашего игрока учитывать.
        private void Check()
        {
            if (_battleCamera == null || _battleCamera.Target == null)
                return;
            var locPos = _showArea.InverseTransformPoint(_battleCamera.Target.position);

            const float halfSize = 0.5f;
            var needShow = locPos.x < halfSize && locPos.z < halfSize && locPos.y < halfSize &&
                           locPos.x > -halfSize && locPos.z > -halfSize && locPos.y > -halfSize;

            SetVisibility(needShow);
        }

        private void SetVisibility(bool needShow)
        {
            if (_visible == needShow)
                return;

            _visible = needShow;
            _animator.SetTrigger(_visible ? _showTrigger : _hideTrigger);
        }
    }
}