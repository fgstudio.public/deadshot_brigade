using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    [Serializable]
    public class AnimEffectData
    {
        [field: SerializeField, Required] public string Name { get; private set; }
        [field: SerializeField, Required] public ParticleSystem Effect { get; private set; }
    }

    public class AnimEffectsContainer : MonoBehaviour
    {
        [InfoBox("Компонент включает и выключает эффекты через события анимации.\n" +
                 "В событиях необходимо вызвать функцию " + nameof(PlayAnimEffect) + " или " + nameof(StopAnimEffect) + ",\n" +
                 "а также передать строковым параметром Name необходимого эффекта.")]
        [SerializeField] private AnimEffectData[] _effectsData;

        public void PlayAnimEffect(string effectName)
        {
            foreach (var effectData in _effectsData)
                if (effectData.Name == effectName)
                    effectData.Effect.Play();
        }

        public void StopAnimEffect(string effectName)
        {
            foreach (var effectData in _effectsData)
                if (effectData.Name == effectName)
                    effectData.Effect.Stop();
        }
    }
}