using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(ParticleSystem))]
    [AddComponentMenu(BattleComponentMenus.Client.FX.Menu + "/" + nameof(ParticleFx))]
    public class ParticleFx : Fx
    {
        [SerializeField, Required, ChildGameObjectsOnly] private ParticleSystem _particleSystem;

        private void Awake() => Init();
        private void OnValidate() => Init();
        private void OnParticleSystemStopped() => InvokeReadyForRelease();

        private void Init()
        {
            InitReferences();
            InitComponents();
        }

        private void InitReferences() =>
            _particleSystem ??= GetComponent<ParticleSystem>();

        private void InitComponents()
        {
            ParticleSystem.MainModule main = _particleSystem.main;
            main.stopAction = ParticleSystemStopAction.Callback;
        }
    }
}