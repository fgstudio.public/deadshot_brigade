using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public class FxPool : AutoReleasingMonoPool<Fx>
    {
        public FxPool([NotNull] IPrefabInstanceProvider<Fx> instanceProvider, [NotNull] ReferencePoolConfig config)
            : base(instanceProvider, config) { }
    }
}
