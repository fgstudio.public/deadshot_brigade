﻿using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IUiFxDamageSpawner<out T>
    {
        T Spawn([NotNull] Transform parentTransform);
    }
}
