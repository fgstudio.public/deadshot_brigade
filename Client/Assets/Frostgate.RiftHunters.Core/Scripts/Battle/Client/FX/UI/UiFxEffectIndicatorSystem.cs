using UnityEngine;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public sealed class UiFxEffectIndicatorSystem
    {
        private readonly IPool<UiFxEffectIndicator> _pool;

        public UiFxEffectIndicatorSystem(IPool<UiFxEffectIndicator> pool) =>
            _pool = pool;

        public void PlayInTheBackEffect(Vector3 position, Transform container = default) =>
            SpawnVfx(container, position).SetEffectText("IN THE BACK", false).Play();

        public void Play(UnitEffectConfig unitEffectConfig, Vector3 position, Transform container = default) =>
            SpawnVfx(container, position).SetEffectText(unitEffectConfig.Name, unitEffectConfig.IsBuff);

        private UiFxEffectIndicator SpawnVfx(Transform container, Vector3 position)
        {
            UiFxEffectIndicator vfx = _pool.Get();
            SetParent(vfx.transform, container);
            vfx.Transform.localPosition = position;
            return vfx;
        }

        private static void SetParent(Transform transform, Transform parent)
        {
            transform.SetParent(parent);
            transform.localPosition = default;
            transform.localRotation = default;
            transform.localScale = Vector3.one;
        }
    }
}