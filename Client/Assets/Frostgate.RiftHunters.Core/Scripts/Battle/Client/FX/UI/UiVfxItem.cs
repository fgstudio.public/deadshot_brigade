using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    /// <summary>
    /// VFX-элемент, отображающий события юнита.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.FX.Menu + "/" + nameof(UiVfxItem))]
    public sealed class UiVfxItem : AnimationFx
    {
    }
}