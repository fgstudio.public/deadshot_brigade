﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.FX.Menu + "/" + nameof(UiFxDamageComponent))]
    public sealed class UiFxDamageComponent : AnimationFx, IUiFxDamageComponent
    {
        [Serializable]
        public class DamageIcon
        {
            [field: SerializeField] public Sprite Sprite;
            [field: SerializeField] public Color Color;
        }

        [field: SerializeField, FoldoutGroup("Events")]
        public UnityEvent DamageValueChanged { get; private set; }
        public Transform Transform => transform;

        [SerializeField, Required] private Transform _textLayoutTransform;
        [SerializeField, Required] private TMP_Text _valueSign;
        [SerializeField, Required] private TMP_Text _damageValue;
        [SerializeField, Required] private Image _zoneDamagedImage;

        [Header("Setings")]
        [SerializeField] private DamageIcon _criticalIcon;
        [SerializeField] private DamageIcon _replacementIcon;
        [SerializeField, Range(0.05f, 1.0f)] private float _increaseScaleMultiplierStrength = 0.25f;
        [SerializeField, Range(0.05f, 1.0f)] private float _reducingScaleMultiplierStrength = 0.75f;
        [SerializeField, Range(0f, 1.0f)] private float _minScaleMultiplier;

        private Color _currentColor;
        private Sprite _currentSprite;

        public void AddDamage(float value, DamageZone zone, bool isCritical, DamageType damageType, float distanceMultiplier)
        {
            _damageValue.text = $"{GetMulticast(damageType)} {value:F0}";

            SetTextScale(zone.Multiplier, distanceMultiplier);

            if(isCritical)
                SetZoneColoredSprite(_criticalIcon.Sprite, _criticalIcon.Color);
            else if(damageType >= DamageType.Replacement && damageType <= DamageType.ReplacementX4)
                SetZoneColoredSprite(_replacementIcon.Sprite, _replacementIcon.Color);
            else
                SetZoneSprite(zone.Sprite);

            DamageValueChanged.Invoke();
        }

        private string GetMulticast(DamageType damageType)
        {
            switch (damageType)
            {
                case DamageType.ReplacementX2:
                    return "X2";
                case DamageType.ReplacementX3:
                    return "X3";
                case DamageType.ReplacementX4:
                    return "X4";
                default:
                    return string.Empty;
            }
        }

        protected override void OnRelease()
        {
            base.OnRelease();
            ResetComponent();
        }

        private void SetTextScale(float zoneMultiplier, float distanceMultiplier)
        {
            float damageModificator = zoneMultiplier * distanceMultiplier;
            float multiplier = damageModificator < 1f ? _reducingScaleMultiplierStrength : _increaseScaleMultiplierStrength;

            float totalMultiplier = (damageModificator * multiplier) + (1f - multiplier);
            float textScale = totalMultiplier < _minScaleMultiplier ? _minScaleMultiplier : totalMultiplier;
            _textLayoutTransform.localScale = new Vector3(textScale, textScale, textScale);
        }

        private void SetTextColor(Color color)
        {
            if (_currentColor == color)
                return;

            SetTextColor(_valueSign, color);
            SetTextColor(_damageValue, color);

            _currentColor = color;
        }

        private void SetZoneSprite(Sprite sprite)
        {
            if (_currentSprite == sprite)
                return;

            _zoneDamagedImage.enabled = sprite != null;
            _zoneDamagedImage.sprite = sprite;

            _currentSprite = sprite;
        }

        private void SetZoneColoredSprite(Sprite sprite, Color color)
        {
            SetZoneSprite(sprite);
            _zoneDamagedImage.color = color;
        }

        private void ResetComponent() => SetZoneSprite(null);

        private void SetTextColor(TMP_Text text, Color color) => text.color = color;
    }
}