using UnityEngine;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    /// <summary>
    /// Система проигрывания VFX, отображающих события юнита.
    /// </summary>
    public sealed class UiVfxSystem
    {
        private readonly IPool<UiVfxItem> _pool;

        public UiVfxSystem(IPool<UiVfxItem> pool) =>
            _pool = pool;

        public void Play(Transform container = default) =>
            SpawnVfx(container).Play();

        private UiVfxItem SpawnVfx(Transform container)
        {
            UiVfxItem vfx = _pool.Get();
            vfx.transform.SetParent(container, false);

            return vfx;
        }
    }
}
