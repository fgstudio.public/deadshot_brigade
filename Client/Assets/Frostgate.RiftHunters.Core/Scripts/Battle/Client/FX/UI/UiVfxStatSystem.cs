using UnityEngine;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    /// <summary>
    /// Система проигрывания VFX, отображающих изменение характеристик юнита.
    /// </summary>
    public sealed class UiVfxStatSystem
    {
        private readonly IPool<UiVfxStatItem> _pool;

        public UiVfxStatSystem(IPool<UiVfxStatItem> pool) =>
            _pool = pool;

        public void Play(float value, Transform container = default) => Play((int)value, container);
        public void Play(float value, Color color, Transform container = default) => Play((int)value, color, container);

        public void Play(int value, Transform container = default) => SpawnVfx(container).Play(value);
        public void Play(int value, Color color, Transform container = default) => SpawnVfx(container).Play(value, color);

        public void Play(Color color, Transform container = default) => SpawnVfx(container).Play(color);
        public void Play(Transform container = default) => SpawnVfx(container).Play();

        private UiVfxStatItem SpawnVfx(Transform container)
        {
            UiVfxStatItem vfx = _pool.Get();
            SetParent(vfx.transform, container);

            return vfx;
        }

        private static void SetParent(Transform transform, Transform parent)
        {
            transform.SetParent(parent);
            transform.localPosition = default;
            transform.localRotation = default;
            transform.localScale = Vector3.one;
        }
    }
}
