using TMPro;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public sealed class UiFxEffectIndicator : AnimationFx, IUiFxEffectIndicator
    {
        [SerializeField, Required] private TMP_Text Text;
        [SerializeField, Required] private Material RedGlowMaterial;
        [SerializeField, Required] private Material BlueGlowMaterial;

        public Transform Transform => transform;

        public UiFxEffectIndicator SetEffectText(string messageText, bool isPositiveEffect)
        {
            Text.transform.localPosition = Vector3.zero;
            Text.text = messageText;
            Text.fontMaterial = isPositiveEffect ? BlueGlowMaterial : RedGlowMaterial;

            return this;
        }
    }
}