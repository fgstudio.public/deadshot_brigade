﻿using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IUiFxDamageSystem : IUiFxSystem
    {
        void Handle(DamageEventData data, Vector3 position, [NotNull] DamageZone damageZone, [NotNull] Transform container);
    }
}