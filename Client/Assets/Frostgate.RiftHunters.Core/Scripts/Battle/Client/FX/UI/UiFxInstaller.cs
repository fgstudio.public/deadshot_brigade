﻿using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    [DisallowMultipleComponent]
    public sealed class UiFxInstaller : FeatureInstaller
    {
        [Header("UiFx pools configuration")]

        [SerializeField, Required] private PrefabPoolConfig<UiVfxStatItem, ReferencePoolConfig> _expFxConfig;
        [SerializeField, Required] private PrefabPoolConfig<UiVfxStatItem, ReferencePoolConfig> _healFxConfig;
        [SerializeField, Required] private PrefabPoolConfig<UiFxDamageComponent, ReferencePoolConfig> _invulFxConfig;
        [SerializeField, Required] private PrefabPoolConfig<UiFxEffectIndicator, ReferencePoolConfig> _effectIndicatorConfig;
        [SerializeField, Required] private PrefabPoolConfig<UiVfxItem, ReferencePoolConfig> _aggroFxConfig;

        protected override void InstallClientPart() =>
            Container.Bind<IUiVfxRoster>().FromMethod(CreateRoster).AsSingle();

        private IUiVfxRoster CreateRoster() =>
            new UiVfxRoster(CreateExpSystem(), CreateHealSystem(), CreateInvulSystem(),
                CreateEffectIndicatorSystem(), CreateAggroSystem());

        private UiVfxStatSystem CreateExpSystem() => CreateUiVfxStatSystem(_expFxConfig);
        private UiVfxStatSystem CreateHealSystem() => CreateUiVfxStatSystem(_healFxConfig);
        private IUiFxDamageSystem CreateInvulSystem() => CreateUiVfxDamageSystem(_invulFxConfig);
        private UiFxEffectIndicatorSystem CreateEffectIndicatorSystem() => CreateUiVfxEffectIndicatorSystem(_effectIndicatorConfig);
        private UiVfxSystem CreateAggroSystem() => CreateUiVfxSystem(_aggroFxConfig);

        private IUiFxDamageSystem CreateUiVfxDamageSystem(PrefabPoolConfig<UiFxDamageComponent, ReferencePoolConfig> damageFxConfig)
        {
            IPool<UiFxDamageComponent> pool = PoolFactory.CreateDefaultAutoReleasingPool(damageFxConfig);
            var fxSpawner = new UiFxDamageSpawner<UiFxDamageComponent>(pool);

            return new UiFxDamageSystem<UiFxDamageComponent>(fxSpawner);
        }

        private UiVfxStatSystem CreateUiVfxStatSystem(PrefabPoolConfig<UiVfxStatItem, ReferencePoolConfig> config)
        {
            IPool<UiVfxStatItem> pool = PoolFactory.CreateDefaultAutoReleasingPool(config);
            return new UiVfxStatSystem(pool);
        }

        private UiVfxSystem CreateUiVfxSystem(PrefabPoolConfig<UiVfxItem, ReferencePoolConfig> config)
        {
            IPool<UiVfxItem> pool = PoolFactory.CreateDefaultAutoReleasingPool(config);
            return new UiVfxSystem(pool);
        }

        private UiFxEffectIndicatorSystem CreateUiVfxEffectIndicatorSystem(PrefabPoolConfig<UiFxEffectIndicator, ReferencePoolConfig> config)
        {
            IPool<UiFxEffectIndicator> pool = PoolFactory.CreateDefaultAutoReleasingPool(config);
            return new UiFxEffectIndicatorSystem(pool);
        }
    }
}
