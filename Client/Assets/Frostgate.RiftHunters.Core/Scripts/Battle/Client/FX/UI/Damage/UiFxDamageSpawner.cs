﻿using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Systems.Pool;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public sealed class UiFxDamageSpawner<T> : IUiFxDamageSpawner<T> where T : AnimationFx
    {
        private readonly IPool<T> _fxPool;

        private readonly IDictionary<Transform, T> _spawnedFxs = new Dictionary<Transform, T>();

        public UiFxDamageSpawner([NotNull] IPool<T> fxPool)
        {
            _fxPool = fxPool;
        }

        public T Spawn(Transform parentTransform)
        {
            if (_spawnedFxs.TryGetValue(parentTransform, out T fx))
                return fx;

            fx = _fxPool.Get();
            SetParent(fx.transform, parentTransform);
            fx.Played.AddListener(() => OnAnimationPlayed(parentTransform));
            _spawnedFxs[parentTransform] = fx;

            return fx;
        }

        private void SetParent(Transform fxTransform, Transform parentTransform)
        {
            fxTransform.SetParent(parentTransform);
            fxTransform.localPosition = default;
            fxTransform.localRotation = default;
            fxTransform.localScale = Vector3.one;
        }

        private void OnAnimationPlayed(Transform parentTransform) => _spawnedFxs.Remove(parentTransform);
    }
}
