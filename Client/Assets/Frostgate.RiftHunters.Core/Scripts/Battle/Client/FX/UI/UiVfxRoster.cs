using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IUiVfxRoster
    {
        UiVfxStatSystem Exp { get; }
        UiVfxStatSystem Heal { get; }
        IUiFxDamageSystem Invul { get; }
        UiFxEffectIndicatorSystem Indicator { get; }
        UiVfxSystem Aggro { get; }
    }

    /// <summary>
    /// Посредник VFX-систем для отображения изменений характеристик юнита.
    /// </summary>
    public sealed class UiVfxRoster : IUiVfxRoster
    {
        public UiVfxStatSystem Exp { get; }
        public UiVfxStatSystem Heal { get; }
        public IUiFxDamageSystem Invul { get; }
        public UiFxEffectIndicatorSystem Indicator { get; }
        public UiVfxSystem Aggro { get; }

        public UiVfxRoster([NotNull] UiVfxStatSystem exp, [NotNull] UiVfxStatSystem heal,
            [NotNull] IUiFxDamageSystem invul, [NotNull] UiFxEffectIndicatorSystem indicator, [NotNull] UiVfxSystem aggro)
        {
            Exp = exp;
            Heal = heal;
            Invul = invul;
            Indicator = indicator;
            Aggro = aggro;
        }
    }
}