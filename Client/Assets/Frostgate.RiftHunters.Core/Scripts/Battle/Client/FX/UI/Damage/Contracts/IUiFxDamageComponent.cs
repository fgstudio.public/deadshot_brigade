﻿using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IUiFxDamageComponent
    {
        Transform Transform { get; }
        void AddDamage(float value, DamageZone zone, bool isCritical, DamageType damageType, float distanceMultiplier);
    }
}
