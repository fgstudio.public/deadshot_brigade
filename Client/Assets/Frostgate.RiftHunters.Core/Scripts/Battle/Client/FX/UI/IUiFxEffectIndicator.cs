using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public interface IUiFxEffectIndicator
    {
        Transform Transform { get; }

        UiFxEffectIndicator SetEffectText(string messageText, bool isPositiveEffect);
    }
}
