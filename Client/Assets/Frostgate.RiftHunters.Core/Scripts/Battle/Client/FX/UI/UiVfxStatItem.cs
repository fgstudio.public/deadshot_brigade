using TMPro;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    /// <summary>
    /// VFX-элемент, отображающий изменение характеристики юнита.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.FX.Menu + "/" + nameof(UiVfxStatItem))]
    public class UiVfxStatItem : AnimationFx
    {
        [SerializeField, CanBeNull, ChildGameObjectsOnly] private TMP_Text _sign;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _value;

        public void Play(int count, Color color)
        {
            _value.color = color;
            if (_sign != null) _sign.color = color;
            Play(count);
        }

        public void Play(Color color)
        {
            _value.color = color;
            if (_sign != null) _sign.color = color;
            Play();
        }

        public void Play(int count)
        {
            _value.text = count.ToString();
            Play();
        }
    }
}