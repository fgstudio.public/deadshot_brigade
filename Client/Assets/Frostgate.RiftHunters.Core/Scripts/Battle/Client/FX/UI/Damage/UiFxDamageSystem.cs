﻿using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Systems.Pool;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    public sealed class UiFxDamageSystem<T> : IUiFxDamageSystem
        where T : IUiFxDamageComponent, IPoolObject
    {
        private readonly IUiFxDamageSpawner<T> _fxSpawner;

        public UiFxDamageSystem([NotNull] IUiFxDamageSpawner<T> fxSpawner)
        {
            _fxSpawner = fxSpawner;
        }

        public void Handle(DamageEventData data, Vector3 position, DamageZone damageZone, Transform container)
        {
            T fx = _fxSpawner.Spawn(container);
            fx.AddDamage(data.Damage, damageZone, data.IsCritical,
                data.Type, data.DamageByDistanceMultiplier);
            fx.Transform.localPosition = position;
        }
    }
}