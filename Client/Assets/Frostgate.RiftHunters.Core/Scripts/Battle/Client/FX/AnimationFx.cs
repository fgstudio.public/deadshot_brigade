using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI.Animations;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.FX.Menu + "/" + nameof(AnimationFx))]
    public class AnimationFx : Fx
    {
        [SerializeField, Required, ChildGameObjectsOnly] private AnimationComponent _animation;

        public UnityEvent Played => _animation.Played;
        public UnityEvent Stopped => _animation.Stopped;
        public UnityEvent Finished => _animation.Finished;

        private void OnValidate() => Init();
        private void Start() => Subscribe();
        private void OnDestroy() => Unsubscribe();

        private void Init() => _animation = this.GetComponentInHierarchy<AnimationComponent>();
        private void Subscribe() => _animation.Finished.AddListener(InvokeReadyForRelease);
        private void Unsubscribe() => _animation.Finished.RemoveListener(InvokeReadyForRelease);

        [ContextMenu(nameof(Play))] public void Play() => _animation.Play();
        [ContextMenu(nameof(Stop))] public void Stop() => _animation.Stop();

        protected override void OnRelease() => Stop();
    }
}