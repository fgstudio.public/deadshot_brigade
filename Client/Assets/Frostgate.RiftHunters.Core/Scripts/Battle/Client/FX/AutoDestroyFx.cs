using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Components;

namespace Frostgate.RiftHunters.Core.Battle.Client.FX
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(AutoDestroyComponent))]
    [AddComponentMenu(BattleComponentMenus.Client.FX.Menu + "/" + nameof(AutoDestroyFx))]
    public class AutoDestroyFx : Fx
    {
        public const string AutoDestroyComponentName = nameof(_autoDestroyComponent);

        [SerializeField, Required, ChildGameObjectsOnly, ReadOnly]
        private AutoDestroyComponent _autoDestroyComponent;

        private void OnValidate() => Init();
        private void Awake() => Init();
        private void Start() => Subscribe();
        private void OnDestroy() => Unsubscribe();

        private void Init()
        {
            InitReferences();
            InitComponents();
        }

        private void InitReferences() =>
            _autoDestroyComponent ??= GetComponent<AutoDestroyComponent>();

        private void InitComponents()
        {
            _autoDestroyComponent.DestroyComponent.Policy = DestroyComponent.DestroyPolicy.SendEvent;
            _autoDestroyComponent.Enable();
        }

        private void Subscribe() =>
            _autoDestroyComponent.DestroyComponent.Destroyed.AddListener(InvokeReadyForRelease);

        private void Unsubscribe() =>
            _autoDestroyComponent.DestroyComponent.Destroyed.RemoveListener(InvokeReadyForRelease);
    }
}