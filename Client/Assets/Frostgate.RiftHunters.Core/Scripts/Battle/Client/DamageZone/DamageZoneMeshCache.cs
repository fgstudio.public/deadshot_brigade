using UnityEngine;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Client;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public static class DamageZoneMeshCache
    {
        private const float AngleStep = 10;
        private const float SelectedSize = 1.7f;
        private const float OutSize = 1.3f;
        private const float InnerSize = 1.2f;

        private static readonly Dictionary<DamageZonesConfig, DamageZoneMesh> cache = new();

        public static DamageZoneMesh Get(DamageZonesConfig damageZonesConfig)
        {
            if (!cache.ContainsKey(damageZonesConfig))
                CreateDamageZoneMesh(damageZonesConfig);

            return cache[damageZonesConfig];
        }

        private static void CreateDamageZoneMesh(DamageZonesConfig damageZonesConfig)
        {
            var damageZoneMesh = new DamageZoneMesh();

            MeshHelper.VerticesCache.Clear();
            MeshHelper.VerticesColorCache.Clear();
            MeshHelper.TrianglesCache.Clear();

            foreach (var damageZone in damageZonesConfig.AllZones)
            {
                var noSelectedColor = damageZone.Color;
                var selectedColor = new Color(noSelectedColor.r, noSelectedColor.g, noSelectedColor.b, 0);
                AddZone(damageZone, noSelectedColor, out var startZoneVertexIndex, out var endZoneVertexIndex);
                AddBlendShape(damageZoneMesh, startZoneVertexIndex, endZoneVertexIndex, noSelectedColor, selectedColor);
            }

            damageZoneMesh.SharedMesh.SetVertices(MeshHelper.VerticesCache);
            damageZoneMesh.SharedMesh.SetTriangles(MeshHelper.TrianglesCache, 0);
            damageZoneMesh.SharedMesh.SetColors(MeshHelper.VerticesColorCache);

            cache.Add(damageZonesConfig, damageZoneMesh);
        }

        private static void AddBlendShape(DamageZoneMesh damageZoneMesh, int startZoneVertexIndex,
            int endZoneVertexIndex, Color noSelectedColor, Color selectedColor)
        {
            var blendShape = new DamageZoneBlendShape(noSelectedColor, selectedColor);

            for (var i = startZoneVertexIndex; i <= endZoneVertexIndex; i++)
            {
                // Будем отодвигать только внешнюю границу зоны. Это не четные вертексы
                if (i % 2 == 0)
                    continue;

                blendShape.Indexes.Add(i);
                var v = MeshHelper.VerticesCache[i];
                blendShape.NoSelectedPoses.Add(v);
                blendShape.SelectedPoses.Add(v.normalized * SelectedSize);
            }

            damageZoneMesh.BlendShapes.Add(blendShape);
        }

        private static void AddZone(DamageZone damageZone, Color color, out int startZoneVertexIndex, out int endZoneVertexIndex)
        {
            var startAngle = damageZone.OffsetAngle - damageZone.HalfSizeAngle;
            var endAngle = damageZone.OffsetAngle + damageZone.HalfSizeAngle;

            startZoneVertexIndex = MeshHelper.VerticesCache.Count;

            for (var angle = startAngle; angle < endAngle;)
            {
                var nextAngle = angle + AngleStep;
                if (nextAngle > endAngle)
                    nextAngle = endAngle;

                var isFirstPolygon = startZoneVertexIndex == MeshHelper.VerticesCache.Count;

                // Если полигон не первый то можно взять в текущий полигон 2 предыдущие вершины
                var firstPolygonVertexIndex = isFirstPolygon
                    ? MeshHelper.VerticesCache.Count
                    : MeshHelper.VerticesCache.Count - MeshHelper.PolygonVerticesCount / 2;


                if (isFirstPolygon)
                {
                    MeshHelper.VerticesCache.Add(Vector3.forward.RotateY(angle) * InnerSize);
                    MeshHelper.VerticesCache.Add(Vector3.forward.RotateY(angle) * OutSize);
                    MeshHelper.VerticesColorCache.Add(color);
                    MeshHelper.VerticesColorCache.Add(color);
                }

                MeshHelper.VerticesCache.Add(Vector3.forward.RotateY(nextAngle) * InnerSize);
                MeshHelper.VerticesCache.Add(Vector3.forward.RotateY(nextAngle) * OutSize);
                MeshHelper.VerticesColorCache.Add(color);
                MeshHelper.VerticesColorCache.Add(color);

                // Строим полигон из добавленных вершин
                MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex);
                MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 1);
                MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 2);
                MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 1);
                MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 3);
                MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 2);

                angle += AngleStep;
            }

            endZoneVertexIndex = MeshHelper.VerticesCache.Count - 1;
        }
    }
}