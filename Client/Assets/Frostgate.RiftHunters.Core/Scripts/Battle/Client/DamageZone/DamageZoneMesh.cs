using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class DamageZoneMesh
    {
        public readonly Mesh SharedMesh = new();
        public readonly List<DamageZoneBlendShape> BlendShapes = new();
    }

    public class DamageZoneBlendShape
    {
        public readonly Color SelectedColor;
        public readonly Color NoSelectedColor;
        public readonly List<int> Indexes = new();
        public readonly List<Vector3> NoSelectedPoses = new();
        public readonly List<Vector3> SelectedPoses = new();

        public DamageZoneBlendShape(Color noSelectedColor, Color selectedColor)
        {
            SelectedColor = selectedColor;
            NoSelectedColor = noSelectedColor;
        }
    }
}