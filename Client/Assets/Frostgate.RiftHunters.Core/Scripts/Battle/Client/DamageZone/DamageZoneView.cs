using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Client;
using Frostgate.RiftHunters.Core.UI;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class DamageZoneView : MonoBehaviour
    {
        [SerializeField, Required] private MeshFilter _damageZoneMeshFilter;
        [SerializeField, Required] private SpriteRenderer _baseSprite;
        [SerializeField, Required] private SpriteRenderer _heroBaseRingSprite;
        [SerializeField, Required] private BarColorRepository _colorRepository;

        private bool DamageZoneAnimationEnabled => _vertices != null;

        private UnitNetwork _unit;
        private Transform _unitTransform;
        private List<Vector3> _vertices;
        private List<Color> _verticesColor;
        private List<float> _damageZoneLerpValues;
        private DamageZonesConfig _config;
        private DamageZoneMesh _zoneMesh;
        private UnitNetwork _damageSource;
        private int _selectedZoneIndex = -1;
        private bool _zonesViewIsDirty;

        public void Init(UnitNetwork unit)
        {
            _unit = unit;
            _config = _unit.DamageZonesConfig;
            _unitTransform = _unit.Transform;

            var isLocalPlayer = _unit.UnitNetworkState.isLocalPlayer;
            var battleUnitType = _unit.UnitType;

            DamageZoneInit(battleUnitType);

            _heroBaseRingSprite.gameObject.SetActive(battleUnitType == BattleUnitType.Player);

            if (isLocalPlayer)
                SetColor(_colorRepository.SelfColor);
            else if (battleUnitType == BattleUnitType.Player)
                SetColor(_colorRepository.FriendColor);
            else
                SetColor(_colorRepository.EnemyColor);
        }

        private void Update()
        {
            if (!DamageZoneAnimationEnabled)
                return;

            if (_config != _unit.DamageZonesConfig)
            {
                _config = _unit.DamageZonesConfig;
                DamageZoneInit(_unit.UnitType);
            }

            var zoneIndex = -1;
            if (_damageSource != null && !_damageSource.UnitNetworkState.IsDead)
            {
                var attackVector = _unitTransform.position - _damageSource.Transform.position;
                zoneIndex = _config.GetDamageZoneIndex(attackVector, _unitTransform.forward);
            }

            if (_selectedZoneIndex != zoneIndex)
            {
                int valuesCount = _damageZoneLerpValues.Count;

                if (_selectedZoneIndex >= 0 && _selectedZoneIndex < valuesCount)
                    _damageZoneLerpValues[_selectedZoneIndex] = 0;

                if (zoneIndex >= 0 && zoneIndex < valuesCount)
                    _damageZoneLerpValues[zoneIndex] = 0;

                _zonesViewIsDirty = true;
            }

            if (!_zonesViewIsDirty)
                return;

            _selectedZoneIndex = zoneIndex;

            var hasChanges = false;

            for (var i = 0; i < _zoneMesh.BlendShapes.Count; i++)
            {
                if (_damageZoneLerpValues[i] > 1)
                    continue;

                hasChanges = true;

                var blendShape = _zoneMesh.BlendShapes[i];
                var selected = i == _selectedZoneIndex;
                var lerp = _damageZoneLerpValues[i] += Time.unscaledDeltaTime * 5;
                if (!selected)
                    lerp = 1 - lerp;

                LerpDamageZone(blendShape, lerp);
            }

            // Если в этом апдейте ничего лерпить не надо было,
            // то снимаем флаг чтобы повторно не пробегать по циклу и не перезаписывать данные меша.
            if (!hasChanges)
                _zonesViewIsDirty = false;

            if (_zonesViewIsDirty)
            {
                _damageZoneMeshFilter.mesh.SetVertices(_vertices);
                _damageZoneMeshFilter.mesh.SetColors(_verticesColor);
            }
        }

        private void LerpDamageZone(DamageZoneBlendShape blendShape, float lerp)
        {
            var selectedColor = blendShape.SelectedColor;
            var noSelectedColor = blendShape.NoSelectedColor;

            for (var i = 0; i < blendShape.Indexes.Count; i++)
            {
                var vertexIndex = blendShape.Indexes[i];
                var selectedPos = blendShape.SelectedPoses[i];
                var noSelectedPos = blendShape.NoSelectedPoses[i];

                _vertices[vertexIndex] = Vector3.Lerp(noSelectedPos, selectedPos, lerp);
                _verticesColor[vertexIndex] = Color.Lerp(noSelectedColor, selectedColor, lerp);
            }
        }

        private void DamageZoneInit(BattleUnitType battleUnitType)
        {
            if (battleUnitType == BattleUnitType.Player)
            {
                _damageZoneMeshFilter.gameObject.SetActive(false);
                return;
            }

            _zoneMesh = DamageZoneMeshCache.Get(_config);
            _damageZoneMeshFilter.sharedMesh = _zoneMesh.SharedMesh;
            _vertices = new List<Vector3>();
            _verticesColor = new List<Color>();
            _damageZoneLerpValues = new List<float>();

            foreach (var _ in _zoneMesh.BlendShapes)
                _damageZoneLerpValues.Add(1);

            _damageZoneMeshFilter.mesh.GetVertices(_vertices);
            _damageZoneMeshFilter.mesh.GetColors(_verticesColor);
        }

        private void SetColor(Color color)
        {
            _heroBaseRingSprite.color = color;
            _baseSprite.color = color;
        }

        public void SetDamageSource(UnitNetwork unitNetwork) =>
            _damageSource = unitNetwork;

        public void ResetDamageSource() =>
            _damageSource = null;
    }
}