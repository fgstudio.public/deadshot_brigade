﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class DamageZonePlayerSelector : IDisposable
    {
        private readonly UnitNetwork _unitNetwork;
        private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollections;

        private DamageZoneView _currentTarget;

        public DamageZonePlayerSelector(UnitNetwork unitNetwork, IReadOnlyObjectCollection<uint, UnitNetwork> unitNetworks)
        {
            _unitNetwork = unitNetwork;
            _unitCollections = unitNetworks;
            _unitNetwork.UnitNetworkState.TargetChanged += OnTargetChanged;
        }

        public void Dispose()
        {
            if (_unitNetwork == null)
                return;

            _unitNetwork.UnitNetworkState.TargetChanged -= OnTargetChanged;
        }

        private void OnTargetChanged(NetworkIdentity networkIdentity)
        {
            DamageZoneView damageZoneView = networkIdentity != null ?
                GetDamageZoneView(networkIdentity.netId) : null;

            if (_currentTarget == damageZoneView)
                return;

            if (_currentTarget != null)
                _currentTarget.ResetDamageSource();

            if (damageZoneView != null)
                damageZoneView.SetDamageSource(_unitNetwork);

            _currentTarget = damageZoneView;
        }

        private DamageZoneView GetDamageZoneView(uint netId)
        {
            DamageZoneView result = _unitCollections.TryGet(netId, out UnitNetwork unitNetwork)
                ? unitNetwork.View?.DamageZoneView
                : null;
            return result;
        }
    }
}