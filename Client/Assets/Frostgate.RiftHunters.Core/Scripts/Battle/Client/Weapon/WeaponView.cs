using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Client.FX;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Client.Audio;

namespace Frostgate.RiftHunters.Core.Battle.Client.Weapon
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.Menu + "/" + nameof(WeaponView))]
    public sealed class WeaponView : MonoBehaviour
    {
        public event Action OnShoot;

        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private ParticleSystemEmitter _shootTrails;
        [SerializeField, Required, ChildGameObjectsOnly] private ParticleSystemEmitter _sparks;
        [SerializeField] private AudioClip[] _shootClips;

        [Header("Allies Settings")]
        [Range(0, 1)] public float AllyTrailsAlpha = 0.5f;

        [Header("Piercing Settings")]
        [MinValue(0)] public float PiercingTrailsSizeMultiplier = 2;
        public LayerMask PiercingTrailsLayerMask;

        private float _timer;
        [CanBeNull] private UnitNetwork _unit;
        [CanBeNull] private RangeWeaponConfig _rangeWeaponConfig;
        private IWallImpactFxSystem _wallImpactFx;
        private ShootingSharedLogic _shootingSharedLogic;
        private Animator _animator;
        private AudioService _audioService;

        public void Init([NotNull] UnitNetwork unit, [NotNull] IWallImpactFxSystem wallImpactFx,
            [CanBeNull] RangeWeaponConfig rangeWeaponConfig, Animator animator,
            [NotNull] AudioService audioService)
        {
            _unit = unit;
            _animator = animator;
            _wallImpactFx = wallImpactFx;
            _rangeWeaponConfig = rangeWeaponConfig;
            _audioService = audioService;

            //TODO: OnShoot и OnWall хотелось бы видеть в одном метсе
            _unit.ShootingSharedLogic.OnShoot += Shoot;
            if (_unit.UnitRangeAttackCaster != null)
                _unit.UnitRangeAttackCaster.OnWall += OnWall;

            if(unit.UnitType == BattleUnitType.Player && !unit.UnitNetworkState.isLocalPlayer)
                _shootTrails.MultiplyAlpha(AllyTrailsAlpha);
        }

        private void OnDestroy()
        {
            if (_unit != null)
            {
                _unit.ShootingSharedLogic.OnShoot -= Shoot;
                if (_unit.UnitRangeAttackCaster != null)
                    _unit.UnitRangeAttackCaster.OnWall -= OnWall;
            }
        }

        private void Shoot()
        {
            if (_unit == null || _rangeWeaponConfig == null)
                return;

            OnShoot?.Invoke();

            if (_shootClips.Length > 0)
                _audioService.SmallWorldFx.Play(_shootClips.GetRandom(), transform.position);

            if (_unit.PiercingBullets)
            {
                _shootTrails.SetCollidesWithMask(PiercingTrailsLayerMask);
                _shootTrails.SetSizeMultiplier(PiercingTrailsSizeMultiplier);
            }
            else
            {
                _shootTrails.ResetCollidesWithMask();
                _shootTrails.SetSizeMultiplier(1);
            }

            var directions =
                ShootingHelper.TakeBulletDirections(_unit.UnitNetworkInput.AimDirection, _rangeWeaponConfig, _unit.UnitNetworkState.Random);

            _shootTrails.transform.position = _unit.ShootPoint;
            _shootTrails.SetStartSpeed(_rangeWeaponConfig.BulletSpeed);

            foreach (var direction in directions)
            {
                _shootTrails.transform.forward = direction;
                _shootTrails.Emit(1);
                _sparks.Play();
            }

            if (!string.IsNullOrEmpty(_rangeWeaponConfig.AttackAnimation))
                _animator.SetTrigger(_rangeWeaponConfig.AttackAnimation);
        }

        private void OnWall(RaycastHit raycastHit) =>
            _wallImpactFx.Play(raycastHit);
    }
}