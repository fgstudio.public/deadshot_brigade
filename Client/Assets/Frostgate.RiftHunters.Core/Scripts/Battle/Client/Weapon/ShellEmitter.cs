using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Weapon
{
    public class ShellEmitter : MonoBehaviour
    {
        [SerializeField] private ParticleSystem _particleSystem;
        [SerializeField] private WeaponView _weaponView;
        [SerializeField] private float _delay;

        private void Start() =>
            _weaponView.OnShoot += OnShoot;

        private void OnShoot()
        {
            CancelInvoke();
            Invoke(nameof(Emmit), _delay);
        }

        private void OnDestroy() =>
            _weaponView.OnShoot -= OnShoot;

        private void Emmit() =>
            _particleSystem.Emit(1);
    }
}