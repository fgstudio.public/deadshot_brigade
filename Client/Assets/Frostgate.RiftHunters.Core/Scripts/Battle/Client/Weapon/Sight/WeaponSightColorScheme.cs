using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Weapon
{
    [Serializable]
    public class WeaponSightColorScheme
    {
        [field: SerializeField] public Color DefaultInnerColor { get; private set; }
        [field: SerializeField] public Color DefaultOutColor { get; private set; }
        [field: SerializeField] public Color SideInnerColor { get; private set; }
        [field: SerializeField] public Color SideOutColor { get; private set; }
        [field: SerializeField] public Color SightColor { get; private set; } = Color.white;
    }
}