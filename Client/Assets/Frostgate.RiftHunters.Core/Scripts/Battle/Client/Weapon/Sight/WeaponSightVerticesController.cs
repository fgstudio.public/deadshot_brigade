using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Weapon
{
    public partial class WeaponSightVerticesController
    {
        private const float ColorLerpSpeed = 5;

        private readonly Mesh _mesh;
        private readonly Transform _transform;
        private readonly List<FarVertexData> _farVertexData = new();
        private readonly List<VertexData> _vertexData = new();
        private readonly List<Vector3> _vertices;
        private readonly List<Color> _colors;
        private readonly Vector3 _raycastOffset;
        private WeaponSightColorScheme _currentColorScheme;

        public WeaponSightVerticesController(Mesh mesh, List<Vector3> vertices, List<Color> colors, Transform transform
            , float raycastYOffset, WeaponSightColorScheme colorScheme)
        {
            _mesh = mesh;
            _vertices = vertices;
            _colors = colors;
            _transform = transform;
            _raycastOffset = Vector3.up * raycastYOffset;

            SetCurrentColorScheme(colorScheme);
        }

        public void SetCurrentColorScheme(WeaponSightColorScheme colorScheme) =>
            _currentColorScheme = colorScheme;

        public void AddFarVertex(SightVertexColorType colorType, Vector3 closePosition, Vector3 farPosition)
        {
            _farVertexData.Add(new FarVertexData(_vertices.Count, colorType, closePosition, farPosition));
            _vertices.Add(farPosition);
            _colors.Add(_currentColorScheme.DefaultInnerColor);
        }

        public void AddVertex(SightVertexColorType colorType, Vector3 positiob)
        {
            _vertexData.Add(new VertexData(_vertices.Count, colorType));
            _vertices.Add(positiob);
            _colors.Add(_currentColorScheme.DefaultInnerColor);
        }

        public void Update(float deltaTime)
        {
            foreach (var vertexData in _farVertexData)
            {
                var worldClosePos = _transform.TransformPoint(vertexData.ClosePosition);
                var shootPoint = _transform.position;
                var dirToClosePos = worldClosePos - shootPoint;
                var distToClosePos = dirToClosePos.magnitude;

                if (Physics.Raycast(shootPoint + _raycastOffset, dirToClosePos, distToClosePos, PhysicsLayers.WallMask))
                {
                    _vertices[vertexData.Index] = vertexData.ClosePosition;
                    continue;
                }

                var worldFarPos = _transform.TransformPoint(vertexData.FarPosition);
                var dir = worldFarPos - worldClosePos;

                if (Physics.Raycast(worldClosePos + _raycastOffset, dir, out var rh, vertexData.MaxLength, PhysicsLayers.WallMask))
                {
                    var pos = _transform.InverseTransformPoint(rh.point - _raycastOffset);
                    _vertices[vertexData.Index] = pos;
                }
                else
                {
                    _vertices[vertexData.Index] = vertexData.FarPosition;
                }

                vertexData.RecalculateColor(_currentColorScheme, deltaTime * ColorLerpSpeed);
                var color = vertexData.GetCurrentColor(_vertices[vertexData.Index]);
                _colors[vertexData.Index] = color;
            }

            foreach (var vertexData in _vertexData)
            {
                vertexData.RecalculateColor(_currentColorScheme, deltaTime * ColorLerpSpeed);
                var color = vertexData.GetCurrentColor();
                _colors[vertexData.Index] = color;
            }

            _mesh.SetVertices(_vertices);
            _mesh.SetColors(_colors);
        }
    }
}