using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Weapon
{
    public partial class WeaponSightVerticesController
    {
        private class VertexData
        {
            public int Index { get; }

            protected readonly SightVertexColorType SightVertexColorType;
            protected Color CurrentColor;

            public VertexData(int index, SightVertexColorType sightVertexColorType)
            {
                Index = index;
                SightVertexColorType = sightVertexColorType;
            }

            private Color GetTargetColor(WeaponSightColorScheme colorScheme)
            {
                switch (SightVertexColorType)
                {
                    case SightVertexColorType.DefaultInnerColor:
                        return colorScheme.DefaultInnerColor;
                    case SightVertexColorType.DefaultOutColor:
                        return colorScheme.DefaultOutColor;
                    case SightVertexColorType.SideInnerColor:
                        return colorScheme.SideInnerColor;
                    case SightVertexColorType.SideOutColor:
                        return colorScheme.SideOutColor;
                    case SightVertexColorType.SightColor:
                        return colorScheme.SightColor;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            public Color GetCurrentColor() =>
                CurrentColor;

            public virtual void RecalculateColor(WeaponSightColorScheme colorScheme, float lerp) =>
                CurrentColor = Color.Lerp(CurrentColor, GetTargetColor(colorScheme), lerp);
        }

        private class FarVertexData : VertexData
        {
            public Vector3 ClosePosition { get; }
            public Vector3 FarPosition { get; }
            public float MaxLength { get; }

            private Color _currentAlterColor;

            public FarVertexData(int index, SightVertexColorType sightVertexColorType, Vector3 closePosition, Vector3 farPosition) : base(index, sightVertexColorType)
            {
                ClosePosition = closePosition;
                FarPosition = farPosition;
                MaxLength = Vector3.Distance(ClosePosition, FarPosition);
            }

            public override void RecalculateColor(WeaponSightColorScheme colorScheme, float lerp)
            {
                switch (SightVertexColorType)
                {
                    case SightVertexColorType.SideOutColor:
                        _currentAlterColor = Color.Lerp(_currentAlterColor, colorScheme.SideInnerColor, lerp);
                        break;
                    case SightVertexColorType.DefaultOutColor:
                        _currentAlterColor = Color.Lerp(_currentAlterColor, colorScheme.DefaultInnerColor, lerp);
                        break;
                }

                base.RecalculateColor(colorScheme, lerp);
            }

            public Color GetCurrentColor(Vector3 position)
            {
                switch (SightVertexColorType)
                {
                    case SightVertexColorType.DefaultOutColor:
                    case SightVertexColorType.SideOutColor:
                        var lerp = Vector3.Distance(ClosePosition, position) / MaxLength;
                        return Color.Lerp(_currentAlterColor, CurrentColor, lerp);
                }

                return base.GetCurrentColor();
            }
        }
    }
}