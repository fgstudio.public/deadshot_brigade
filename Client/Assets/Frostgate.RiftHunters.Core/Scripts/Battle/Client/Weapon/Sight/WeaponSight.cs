using UnityEngine;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Client;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Battle.Client.Weapon
{
    public class WeaponSight : MonoBehaviour
    {
        [SerializeField] private MeshFilter _meshFilter;
        [SerializeField] private float _meshAngleStep = 3;
        [SerializeField] private float _sightOriginHalfSize = 0.05f;
        [SerializeField] private float _sightYOffset = 0.02f;
        [SerializeField] private float _rootYOffset = 0.15f;
        [SerializeField] private float _forwardOffset;
        [SerializeField] private WeaponSightColorScheme _defaultColorScheme;
        [SerializeField] private WeaponSightColorScheme _attackColorScheme;
        [SerializeField] private WeaponSightColorScheme _disabledColorScheme;

        [CanBeNull] private IWeapon RangeWeapon => _unitView.UnitNetwork.RangeWeapon;

        private Quaternion _prevRotation;
        private UnitView _unitView;
        private WeaponSightVerticesController _verticesController;
        private readonly List<Vector3> _vertices = new(1024);
        private readonly List<Color> _colors = new(1024);

        public void Init(UnitView unitView)
        {
            _unitView = unitView;
            transform.localPosition = new Vector3(0, _rootYOffset, 0);
            transform.localEulerAngles = Vector3.zero;

            _meshFilter.mesh = new Mesh();

            var raycastYOffset = _unitView.UnitNetwork.ShootPoint.y - _rootYOffset;
            _verticesController = new WeaponSightVerticesController(_meshFilter.mesh, _vertices, _colors, transform, raycastYOffset, _defaultColorScheme);

            var projectionStartPoint = ShootingHelper.GetShootingProjectionStartPoint(RangeWeapon, Vector3.zero, Vector3.forward);
            var backLength = projectionStartPoint.magnitude;
            var angleAttack = _unitView.UnitNetwork.RangeWeapon?.AttackAngle ?? default;
            var shortStep = angleAttack % _meshAngleStep;
            var innerSize = backLength + _forwardOffset;
            var outSize = backLength + RangeWeapon?.MaxDistance ?? default;

            _vertices.Clear();
            MeshHelper.TrianglesCache.Clear();

            DrawShootingArea(angleAttack, shortStep, innerSize, outSize, backLength);
            DrawSight(innerSize - backLength, outSize - backLength);

            _meshFilter.mesh.SetVertices(_vertices);
            _meshFilter.mesh.SetColors(_colors);
            _meshFilter.mesh.SetTriangles(MeshHelper.TrianglesCache, 0);
            _meshFilter.mesh.RecalculateNormals();
            _meshFilter.mesh.RecalculateTangents();
        }

        private void Update()
        {
            _verticesController.Update(Time.deltaTime);

            UpdateColorScheme();
        }

        private void UpdateColorScheme()
        {
            if (!_unitView.UnitNetwork.CanRangeAttack)
            {
                _verticesController.SetCurrentColorScheme(_disabledColorScheme);
                return;
            }

            _verticesController.SetCurrentColorScheme(
                _unitView.UnitNetwork.ShootingSharedLogic.InProcess
                    ? _attackColorScheme : _defaultColorScheme);
        }

        private void DrawSight(float innerSize, float outSize)
        {
            _verticesController.AddVertex(SightVertexColorType.SightColor, new Vector3(-_sightOriginHalfSize, _sightYOffset, innerSize));

            var farPosition = new Vector3(0, _sightYOffset, outSize);
            var closePosition = new Vector3(0, _sightYOffset, innerSize);
            _verticesController.AddFarVertex(SightVertexColorType.SightColor, closePosition, farPosition);

            _verticesController.AddVertex(SightVertexColorType.SightColor, new Vector3(_sightOriginHalfSize, _sightYOffset, innerSize));

            MeshHelper.TrianglesCache.Add(_vertices.Count - 3);
            MeshHelper.TrianglesCache.Add(_vertices.Count - 2);
            MeshHelper.TrianglesCache.Add(_vertices.Count - 1);
        }

        private void DrawShootingArea(float angleAttack, float shortStep, float innerSize, float outSize, float backLength)
        {
            for (var angle = -angleAttack; angle < angleAttack;)
            {
                var isFirstSection = _vertices.Count == 0;
                var isShortSection = shortStep > 0 && (isFirstSection || angle + _meshAngleStep > angleAttack);
                var step = isShortSection ? shortStep : _meshAngleStep;

                if (isFirstSection)
                    AddEdge(-angleAttack, innerSize, outSize, backLength, true);

                angle += step;

                if (angle > angleAttack)
                    angle = angleAttack;

                var isLastSection = angle >= angleAttack;
                AddEdge(angle, innerSize, outSize, backLength, isLastSection);

                var firstPolygonVertexIndex = _vertices.Count - MeshHelper.PolygonVerticesCount;

                // Строим полигон из добавленных вершин
                // Для последней секции меняем разбивку чтобы была зеркально первой. Это влияет на раскраску вертексами
                if (isLastSection)
                {
                    MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex);
                    MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 1);
                    MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 3);
                    MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 3);
                    MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 2);
                    MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex);
                }
                else
                {
                    MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex);
                    MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 1);
                    MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 2);
                    MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 1);
                    MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 3);
                    MeshHelper.TrianglesCache.Add(firstPolygonVertexIndex + 2);
                }
            }
        }

        private void AddEdge(float angle, float innerSize, float outSize, float backLength, bool isSideEdge)
        {
            var farPosition = Vector3.forward.RotateY(angle) * outSize - Vector3.forward * backLength;
            var closePosition = Vector3.forward.RotateY(angle) * innerSize - Vector3.forward * backLength;

            var closeColorType = isSideEdge ? SightVertexColorType.SideInnerColor : SightVertexColorType.DefaultInnerColor;
            _verticesController.AddVertex(closeColorType, closePosition);

            var farColorType = isSideEdge ? SightVertexColorType.SideOutColor : SightVertexColorType.DefaultOutColor;
            _verticesController.AddFarVertex(farColorType, closePosition, farPosition);
        }
    }
}