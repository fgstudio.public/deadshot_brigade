namespace Frostgate.RiftHunters.Core.Battle.Client.Weapon
{
    public enum SightVertexColorType
    {
        DefaultInnerColor,
        DefaultOutColor,
        SideInnerColor,
        SideOutColor,
        SightColor
    }
}