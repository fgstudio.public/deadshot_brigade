using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Client.PathPainter
{
    public sealed class PathPainter : MonoBehaviour
    {
        private const string SettingsGroupName = "Settings";

        [FoldoutGroup("Assets"), Required, AssetsOnly]
        [SerializeField] private LineRenderer _lineRendererPrefab;

        [FoldoutGroup(SettingsGroupName)]
        [SerializeField] private bool _hasConditionsDistance;

        [FoldoutGroup(SettingsGroupName), ShowIf(nameof(_hasConditionsDistance))]
        [SerializeField, MinValue(0)] private float _maxDistance;

        [FoldoutGroup(SettingsGroupName), ShowIf(nameof(_hasConditionsDistance))]
        [SerializeField, MinValue(0)] private float _minDistance;

        [FoldoutGroup(SettingsGroupName), SuffixLabel("sec", true)]
        [Range(0.01f, 1f)] public float DrawInterval;

        [FoldoutGroup(SettingsGroupName), SuffixLabel("units")]
        public Vector3 Offset;

        [FoldoutGroup(SettingsGroupName)]
        public float Width = 1f;

        [field: FoldoutGroup("Events")]
        [field: SerializeField] public UnityEvent<LineRenderer> Drawn { get; private set; }

        private readonly Dictionary<Transform, PathPainterData> _datas = new();
        private float _timeToDraw;

        public void SetTarget(Transform local, Transform target)
        {
            if (!_datas.ContainsKey(target) &&
                local != null &&
                target != null)
            {
                _datas[target] = new PathPainterData(Instantiate(_lineRendererPrefab, transform),
                    target, local);
            }
        }

        public void RemoveTarget(Transform target)
        {
            if (_datas.ContainsKey(target))
            {
                _datas[target].Dispose();
                _datas.Remove(target);
            }
        }

        private void FixedUpdate()
        {
            _timeToDraw -= Time.fixedDeltaTime;

            if (_timeToDraw <= 0)
            {
                foreach (var transformTarget in _datas.Keys)
                {
                    var pathData = _datas[transformTarget];
                    if (pathData.Local != null)
                        SetPath(pathData);
                }

                _timeToDraw = DrawInterval;
            }
        }

        private void SetPath(PathPainterData pathData)
        {
            if (pathData.Target == null || pathData.Local == null)
            {
                pathData.SetVisible(false);
                return;
            }

            NavMesh.CalculatePath(pathData.Target.position, pathData.Local.position,
                NavMesh.AllAreas, pathData.Path);

            if (CheckConditionsDistance(pathData.Path.corners))
            {
                DrawPath(pathData.Path, pathData.LineRenderer);
                pathData.SetVisible(true);
            }
            else
            {
                pathData.SetVisible(false);
            }
        }

        private bool CheckConditionsDistance(Vector3[] corners)
        {
            if (!_hasConditionsDistance || corners.Length < 2)
                return true;

            float sqrDistance = Vector3.SqrMagnitude(corners[0] - corners[^1]);
            return sqrDistance >= Mathf.Pow(_minDistance, 2) &&
                   sqrDistance <= Mathf.Pow(_maxDistance, 2);
        }

        private void DrawPath(NavMeshPath path, LineRenderer lineRenderer)
        {
            var corners = path.corners;

            if(corners.Length < 2)
                return;

            lineRenderer.startWidth = Width;
            lineRenderer.endWidth = Width;

            int positionsCount = corners.Length;
            lineRenderer.positionCount = positionsCount;

            for (int i = 0; i < positionsCount; i++)
            {
                Vector3 pointPosition = CalcPosition(corners[i]);
                lineRenderer.SetPosition(i, pointPosition);
            }

            Drawn?.Invoke(lineRenderer);
        }

        private Vector3 CalcPosition(Vector3 corner) =>
            new(x: corner.x + Offset.z,
                y: corner.z + Offset.x,
                z: corner.y - Offset.y);
    }
}