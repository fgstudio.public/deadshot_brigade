﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.AI;

namespace Frostgate.RiftHunters.Core.Battle.Client.PathPainter
{
    public class PathPainterData : IDisposable
    {
        [CanBeNull] public Transform Local { get; }
        public Transform Target { get; }
        public LineRenderer LineRenderer { get; }
        public NavMeshPath Path { get; }

        public PathPainterData(
            [NotNull] LineRenderer lineRenderer,
            [NotNull] Transform target,
            [CanBeNull] Transform local)
        {
            LineRenderer = lineRenderer;
            Local = local;
            Target = target;
            Path = new NavMeshPath();
        }

        public void SetVisible(bool visible)
        {
            LineRenderer.enabled = visible;
        }

        public void Dispose()
        {
            GameObject.Destroy(LineRenderer.gameObject);
        }
    }
}