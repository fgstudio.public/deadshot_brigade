using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.Menu + "/" + nameof(BattleCamera))]
    public sealed class BattleCamera : MonoBehaviour
    {
        [SerializeField] private Vector3 _offset;
        [SerializeField] private float _xRotationOffset;
        [SerializeField] private float _moveLerpSpeed;

        private float _yRotation;
        private Transform _target;
        private Vector3 _followPosition;

        private bool IsTickable => this != null && _target != null;

        public Transform Target => _target;

        public Vector2 ForwardXZ => (RotationY * Vector3.forward).CutY();
        public Quaternion RotationY => Quaternion.AngleAxis(transform.eulerAngles.y, Vector3.up);

        public float YRotation
        {
            get => _yRotation;
            set
            {
                _yRotation = value;
                if (IsTickable) Tick();
            }
        }

        public void StartFollowing(Transform target)
        {
            _target = target;
            _followPosition = _target.position;
        }

        public void StopFollowing()
        {
            _target = null;
            _followPosition = Vector3.zero;
        }

        private void Update()
        {
            if (IsTickable)
                Tick();
        }

        private void FixedUpdate()
        {
            if (IsTickable)
                UpdateFollowPosition(Time.deltaTime);
        }

        private void Tick()
        {
            var rotation = Quaternion.Euler(0, YRotation, 0);

            RotationTick(rotation);
            MoveTick(rotation);
        }

        private void RotationTick(Quaternion rotation) =>
            transform.rotation = Quaternion.Euler(_xRotationOffset, rotation.eulerAngles.y, 0);

        private void MoveTick(Quaternion rotation) =>
            transform.position = _followPosition + rotation * _offset;

        private void UpdateFollowPosition(float deltaTime) =>
            _followPosition = Vector3.Lerp(_followPosition, _target.position, _moveLerpSpeed * deltaTime);
    }
}