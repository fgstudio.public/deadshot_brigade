using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class CameraBlocker : MonoBehaviour
    {
        [SerializeField] private Renderer _renderer;
        [SerializeField] private Material[] _hidenMaterials;

        private const float ShowDelay = 1;

        private Material[] _visibleMaterials;

        private void Awake()
        {
            _visibleMaterials = _renderer.sharedMaterials;
            gameObject.layer = PhysicsLayers.CameraBlocker;
        }

        private void OnTriggerEnter(Collider _)
        {
            CancelInvoke();
            Hide();
        }

        private void OnTriggerExit(Collider _)
        {
            CancelInvoke();
            Invoke(nameof(Show), ShowDelay);
        }

        private void Hide() =>
            _renderer.sharedMaterials = _hidenMaterials;

        private void Show() =>
            _renderer.sharedMaterials = _visibleMaterials;
    }
}