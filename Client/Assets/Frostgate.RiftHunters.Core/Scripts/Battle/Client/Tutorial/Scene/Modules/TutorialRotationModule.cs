using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Input.UI;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c")]
    [AddComponentMenu(TutorialComponentMenus.Menu + "/" + nameof(TutorialRotationModule))]
    public sealed class TutorialRotationModule : TutorialObjectModule
    {
        [Header("Core references")]
        [SerializeField, Required] private TutorialObjectComponent _tutorialComponent;

        [Header("Module references")]
        [SerializeField, Required, HideInPrefabAssets] private DragArea _dragArea;
        [SerializeField, Required, HideInPrefabAssets] private GameObject _inputBlocker;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _tutorialComponent ??= GetComponent<TutorialObjectComponent>();

        private void Start() => Subscribe(_dragArea);
        private void OnDestroy() => Unsubscribe(_dragArea);

        private void Subscribe(DragArea dragArea) =>
            dragArea.PointerDownEvent.AddListener(OnDragAreaDown);
        private void Unsubscribe(DragArea dragArea) =>
            dragArea.PointerDownEvent.RemoveListener(OnDragAreaDown);

        private void OnDragAreaDown()
        {
            Unsubscribe(_dragArea);
            _tutorialComponent.Complete();
        }

        public override void HandleActivate() => _inputBlocker.SetActive(false);
        public override void HandleDeactivate() => _inputBlocker.SetActive(true);
        public override void HandleComplete() => _inputBlocker.SetActive(false);
    }
}
