﻿using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene.Utils
{
    public class TutorialElementActivator : MonoBehaviour
    {
        private IReadOnlyTutorialState _tutorialState;

        [SerializeField, Required] private GameObject _targetActiveObject;

        [SerializeField] private bool _isHideAfterStart;
        [SerializeField] private TutorialId _activationStateId;
        [SerializeField] private TutorialId _deactivationStateId;

        [Inject]
        private void MonoConstruct(IReadOnlyTutorialState tutorialState)
        {
            _tutorialState = tutorialState;
            _tutorialState.DataUpdated += OnTutorialStateChanged;
        }

        private void Start()
        {
            if (_isHideAfterStart)
                _targetActiveObject.SetActive(false);

            OnTutorialStateChanged(_activationStateId, _tutorialState.GetStatus(_activationStateId));
            OnTutorialStateChanged(_deactivationStateId, _tutorialState.GetStatus(_deactivationStateId));
        }

        private void OnTutorialStateChanged(TutorialId id, TutorialStatus status)
        {
            if (id == _activationStateId && status == TutorialStatus.Active)
                _targetActiveObject.SetActive(true);

            if (id == _deactivationStateId && status == TutorialStatus.Active)
                _targetActiveObject.SetActive(false);
        }
    }
}