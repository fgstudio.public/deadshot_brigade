using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c")]
    [AddComponentMenu(TutorialComponentMenus.Menu + "/" + nameof(TutorialPathByLogicModule))]
    public sealed class TutorialPathByLogicModule : TutorialObjectModule
    {
        [Header("Settings")]
        [MinValue(0)]
        [SerializeField] private float _correctionSpeedMult;
        [SerializeField] private Vector3 _offset;

        [Header("Module references")]
        [Required, HideInPrefabAssets]
        [SerializeField] private PathPainter.PathPainter _pathPainter;

        private bool _isDrawing;
        private Vector3 _targetAngles;
        private Vector3 _targetPosition;

        private void LateUpdate()
        {
            if (_isDrawing)
            {
                float t = Time.deltaTime * _correctionSpeedMult;
                transform.position = Vector3.Lerp(transform.position, _targetPosition, t);
                transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, _targetAngles, t);
            }
        }

        public override void HandleActivate()
        {
            _isDrawing = false;
            _pathPainter.Drawn.AddListener(OnDrawn);
        }

        public override void HandleDeactivate()
        {
            _isDrawing = false;
            _pathPainter.Drawn.RemoveListener(OnDrawn);
        }

        public override void HandleComplete()
        {
            _isDrawing = false;
            _pathPainter.Drawn.RemoveListener(OnDrawn);
        }

        private void OnDrawn(LineRenderer lineRenderer)
        {
            if (lineRenderer.positionCount > 1)
            {
                Vector3 startPosition = ExtractPosition(lineRenderer, 1);
                Vector3 endPosition = ExtractPosition(lineRenderer, 0);
                Vector3 direction = endPosition - startPosition;

                float angle = Vector3.Angle(Vector3.forward, direction);
                Vector3 angles = transform.eulerAngles;
                angles.y = angle;

                _targetPosition = startPosition + _offset.RotateY(angle);
                _targetAngles = angles;

                if (_isDrawing == false)
                {
                    _isDrawing = true;
                    transform.position = _targetPosition;
                    transform.eulerAngles = angles;
                }
            }
        }

        private Vector3 ExtractPosition(LineRenderer lineRenderer, int index)
        {
            Vector3 position = lineRenderer.GetPosition(index);
            position.z = position.y;
            position.y = default;

            return position;
        }
    }
}
