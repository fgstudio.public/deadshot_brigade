using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c")]
    [AddComponentMenu(TutorialComponentMenus.Menu + "/" + nameof(TutorialArmoredBossModule))]
    public sealed class TutorialArmoredBossModule : TutorialObjectModule
    {
        [Header("Core references")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private TutorialObjectComponent _tutorialComponent;

        [Header("Module references")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private TutorialInfoModule _infoModule;

        [Required, HideInPrefabAssets]
        [SerializeField] private UnitView _unitView;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _infoModule ??= GetComponent<TutorialInfoModule>();
            _tutorialComponent ??= GetComponent<TutorialObjectComponent>();
        }

        private void OnDestroy()
        {
            UnitNetworkState unitState = _unitView.UnitNetwork?.UnitNetworkState;
            if (unitState != null) UnsubscribeState(_unitView.UnitNetwork.UnitNetworkState);
        }

        public override void HandleActivate()
        {
            UnitNetworkState unitState = _unitView.UnitNetwork?.UnitNetworkState;

            if (unitState != null)
            {
                SubscribeState(unitState);
                if (unitState.IsDead) OnUnitDied(default);
            }
        }

        public override void HandleDeactivate()
        {
            UnitNetworkState unitState = _unitView.UnitNetwork?.UnitNetworkState;
            if (unitState != null) UnsubscribeState(_unitView.UnitNetwork.UnitNetworkState);
        }

        public override void HandleComplete()
        {
            UnitNetworkState unitState = _unitView.UnitNetwork?.UnitNetworkState;
            if (unitState != null) UnsubscribeState(_unitView.UnitNetwork.UnitNetworkState);
        }

        private void SubscribeState(UnitNetworkState unitState) =>
            unitState.Died += OnUnitDied;

        private void UnsubscribeState(UnitNetworkState unitState) =>
            unitState.Died -= OnUnitDied;

        private void OnUnitDied(UnitNetworkState unitNetworkState) =>
            _infoModule.HandleComplete();
    }
}
