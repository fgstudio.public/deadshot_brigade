﻿using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    public class TutorialSpawnerModule : TutorialObjectModule
    {
        [Header("Core references")]
        [SerializeField, Required] private TutorialObjectComponent _tutorialComponent;
        [SerializeField, Required] private BotSpawnState _spawnState;
        [SerializeField, Required] private BotSpawnerConfig _spawnerConfig;

        private void Start()
        {
            _spawnState.DataUpdated.AddListener(OnDataUpdated);
            OnDataUpdated(_spawnerConfig.Id);
        }

        private void OnDataUpdated(string spawnerId)
        {
            if (spawnerId == _spawnerConfig.Id)
            {
                if (_spawnState.Data.TryGetValue(spawnerId, out var spawnData))
                {
                    switch (spawnData.Status)
                    {
                        case BotSpawnerStatus.Spawning:
                            _tutorialComponent.Activate();
                            break;
                        case BotSpawnerStatus.Completed:
                            _tutorialComponent.Complete();
                            break;
                    }
                }
            }
        }

        public override void HandleActivate()
        {
        }

        public override void HandleDeactivate()
        {
        }

        public override void HandleComplete()
        {
        }
    }
}