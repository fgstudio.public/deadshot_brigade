using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Input.UI;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c")]
    [AddComponentMenu(TutorialComponentMenus.Menu + "/" + nameof(TutorialMovementModule))]
    public sealed class TutorialMovementModule : TutorialObjectModule
    {
        private const float MaxAlpha = 1;

        [Header("Core references")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private TutorialObjectComponent _tutorialComponent;

        [Header("Module references")]
        [SerializeField, Required, HideInPrefabAssets] private Joystick _joystick;
        [SerializeField, Required, HideInPrefabAssets] private CanvasGroup _joystickAlpha;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _tutorialComponent ??= GetComponent<TutorialObjectComponent>();

        private void Start() => Subscribe(_joystick);
        private void OnDestroy() => Unsubscribe(_joystick);

        private void Subscribe(Joystick joystick) =>
            joystick.JoystickDownEvent.AddListener(OnJoystickDown);
        private void Unsubscribe(Joystick joystick) =>
            joystick.JoystickDownEvent.RemoveListener(OnJoystickDown);

        private void OnJoystickDown()
        {
            Unsubscribe(_joystick);
            _tutorialComponent.Complete();
        }

        public override void HandleActivate() => _joystickAlpha.alpha = MaxAlpha;
        public override void HandleDeactivate() { }
        public override void HandleComplete() { }
    }
}
