using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Energy;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c")]
    [AddComponentMenu(TutorialComponentMenus.Menu + "/" + nameof(TutorialEnergyCapsuleModule))]
    public sealed class TutorialEnergyCapsuleModule : TutorialObjectModule
    {
        [Header("Core references")]
        [SerializeField, Required] private TutorialObjectComponent _tutorialComponent;

        [Header("Module references")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private TutorialInfoModule _infoModule;

        [Required, HideInPrefabAssets]
        [SerializeField] private EnergyCapsuleSpawnPoint _spawnPoint;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void OnDestroy() => DeinitState();
        private void InitReferences()
        {
            _infoModule ??= GetComponent<TutorialInfoModule>();
            _tutorialComponent ??= GetComponent<TutorialObjectComponent>();
        }

        public override void HandleActivate()
        {
            _infoModule.HandleDeactivate();
            InitState();
        }

        public override void HandleDeactivate()
        {
            DeinitState();
        }

        public override void HandleComplete()
        {
            DeinitState();
        }

        private void InitState()
        {
            EnergyCapsuleSpawnPointState state = _spawnPoint?.State;

            if (state != null)
            {
                state.Spawned.AddListener(OnCapsuleSpawned);
                state.SpawnedCapsules.ForEach(OnCapsuleSpawned);
            }
        }

        private void DeinitState()
        {
            EnergyCapsuleSpawnPointState state = _spawnPoint?.State;

            if (state != null)
                state.Spawned.RemoveListener(OnCapsuleSpawned);
        }

        private void OnCapsuleSpawned(IClientEnergyCapsule capsule)
        {
            if (capsule == null || capsule.GameObject == null || capsule.State.IsCollected)
                return;

            SubscribeCapsule(capsule);
            _infoModule.HandleActivate();
        }

        private void SubscribeCapsule([NotNull] IClientEnergyCapsule capsule)
        {
            capsule.Collected += OnCapsuleCollected;
            capsule.ReadyForRelease += OnCapsuleReleasing;
        }

        private void UnsubscribeCapsule([NotNull] IClientEnergyCapsule capsule)
        {
            capsule.Collected -= OnCapsuleCollected;
            capsule.ReadyForRelease -= OnCapsuleReleasing;
        }

        private void OnCapsuleCollected(IClientEnergyCapsule capsule)
        {
            UnsubscribeCapsule(capsule);
            _tutorialComponent.Complete();
        }

        private void OnCapsuleReleasing(IAutoReleased autoReleased)
        {
            if (autoReleased is IClientEnergyCapsule capsule)
            {
                UnsubscribeCapsule(capsule);
                _tutorialComponent.Complete();
            }
        }
    }
}