using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c")]
    [AddComponentMenu(TutorialComponentMenus.Menu + "/" + nameof(TutorialInfoModule))]
    public sealed class TutorialInfoModule : TutorialObjectModule
    {
        private const float MinAlpha = 0;
        private const float MaxAlpha = 1;

        [Header("Module references")]
        [SerializeField, Required, ChildGameObjectsOnly] private CanvasGroup _infoLayout;
        [SerializeField] private List<GameObject> _activeElements;
        [SerializeField] private bool _isHideAfterComplete;

        [Header("Animation settings")]
        [MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _duration = 1f;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _infoLayout ??= GetComponentInChildren<CanvasGroup>();

        public override void HandleActivate()
        {
            gameObject.SetActive(true);

            _infoLayout.alpha = 1f;
            // _infoLayout.DOKill();
            // _infoLayout.DOFade(MaxAlpha, _duration);

            _activeElements.ForEach(t => t.SetActive(true));
        }

        public override void HandleDeactivate()
        {
            gameObject.SetActive(false);

            // _infoLayout.DOKill();
            _infoLayout.alpha = MinAlpha;

            if (_isHideAfterComplete)
                _activeElements.ForEach(t => t.SetActive(false));
        }

        public override void HandleComplete()
        {
            _infoLayout.DOKill();
            gameObject.SetActive(false);
            // _infoLayout.DOFade(MinAlpha, _duration).OnComplete(
            //     () => gameObject.SetActive(false));

            if (_isHideAfterComplete)
                _activeElements.ForEach(t => t.SetActive(false));
        }
    }
}