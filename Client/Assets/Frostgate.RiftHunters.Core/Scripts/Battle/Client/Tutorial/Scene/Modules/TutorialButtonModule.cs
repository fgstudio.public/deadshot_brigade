﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    public class TutorialButtonModule : TutorialObjectModule
    {
        [Header("Core references")]
        [SerializeField, Required] private TutorialObjectComponent _tutorialComponent;
        [SerializeField, Required] private Button _reloadButton;

        private void Start() => Subscribe(_reloadButton);
        private void OnDestroy() => UnSubscribe(_reloadButton);
        private void OnValidate() => _tutorialComponent ??= GetComponent<TutorialObjectComponent>();

        private void Subscribe(Button button)
        {
            button.onClick.AddListener(OnReloadClicked);
        }

        private void UnSubscribe(Button button)
        {
            button.onClick.RemoveListener(OnReloadClicked);
        }

        private void OnReloadClicked()
        {
            _tutorialComponent.Complete();
        }

        public override void HandleActivate()
        {
        }

        public override void HandleDeactivate()
        {
        }

        public override void HandleComplete()
        {
        }
    }
}