﻿using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene.Utils
{
    public class TutorialSlowTimeCorrectorAnimation : MonoBehaviour
    {
        [SerializeField] private Animation _animation;

        private void OnValidate()
        {
            _animation ??= GetComponent<Animation>();
        }

        private void OnEnable()
        {
            TimeScaleHelper.TimeScaleChanged += OnTimeScaleChanged;
        }

        private void OnDisable()
        {
            TimeScaleHelper.TimeScaleChanged -= OnTimeScaleChanged;
        }

        private void OnTimeScaleChanged()
        {
            _animation[_animation.clip.name].speed = TimeScaleHelper.MultiplierToNormalTimeScale;
        }

        private void Start()
        {
            OnTimeScaleChanged();
        }
    }
}