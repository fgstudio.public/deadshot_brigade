using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c")]
    [AddComponentMenu(TutorialComponentMenus.Menu + "/" + nameof(TutorialPathByTextureModule))]
    public sealed class TutorialPathByTextureModule : TutorialObjectModule
    {
        [Header("Settings")]
        [SerializeField, Required] private Texture _pathTexture;
        [SerializeField, MinValue(0)] private float _pathWidth;

        [Header("Module references")]
        [Required, HideInPrefabAssets, CanBeNull]
        [SerializeField] private PathPainter.PathPainter _pathPainter;

        private readonly PathPainterWidthCache _painterWidthCache = new();
        private readonly LineRendererTextureCache _rendererTextureCache = new();

        private void OnDestroy()
        {
            if (_pathPainter != null)
                _painterWidthCache.RestoreCache(_pathPainter);

            _rendererTextureCache.RestoreCache();
        }

        public override void HandleActivate()
        {
            if (_pathPainter != null)
            {
                _painterWidthCache.Cache(_pathPainter);
                _pathPainter.Width = _pathWidth;
                _pathPainter.Drawn.AddListener(OnDrawn);
            }
        }

        public override void HandleDeactivate()
        {
            _rendererTextureCache.RestoreCache();

            if (_pathPainter != null)
            {
                _painterWidthCache.RestoreCache(_pathPainter);
                _pathPainter.Drawn.RemoveListener(OnDrawn);
            }
        }

        public override void HandleComplete()
        {
            _rendererTextureCache.RestoreCache();

            if (_pathPainter != null)
            {
                _painterWidthCache.RestoreCache(_pathPainter);
                _pathPainter.Drawn.RemoveListener(OnDrawn);
            }
        }

        private void OnDrawn(LineRenderer lineRenderer)
        {
            if (!_rendererTextureCache.IsCached(lineRenderer))
            {
                _rendererTextureCache.Cache(lineRenderer);
                lineRenderer.material.mainTexture = _pathTexture;
            }
        }
    }
}
