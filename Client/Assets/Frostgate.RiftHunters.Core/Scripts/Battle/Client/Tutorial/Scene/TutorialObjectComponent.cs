using System;
using System.Linq;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    public delegate void ComponentEvent(TutorialId id);

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c")]
    [AddComponentMenu(TutorialComponentMenus.Menu + "/" + nameof(TutorialObjectComponent))]
    public sealed partial class TutorialObjectComponent : MonoBehaviour
    {
        [Title("References")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private TutorialObjectModule _module;

        [Title("Settings")]
        [SerializeField] private TutorialId _id;

        [Title("Info")]
        [ShowInInspector, ReadOnly]
        private TutorialStatus Status =>
            _state?.GetStatus(_id) ?? TutorialStatus.Inactive;

        public event ComponentEvent Activated;
        public event ComponentEvent Completed;

        [CanBeNull] private IReadOnlyTutorialState _state;

        public TutorialId Id => _id;
        private bool IsRegistered => Components.Contains(this);

        private void OnValidate() =>
            InitReferences();

        [Inject]
        private void MonoConstructor([NotNull] IReadOnlyTutorialState state)
        {
            _state = state;

            if (IsRegistered)
                InitState(_state);
        }

        private void Awake()
        {
            InitReferences();
            Register(this);

            if (_state != null)
                InitState(_state);
        }

        private void OnDestroy()
        {
            Unregister(this);

            if (_state != null)
                DeinitState(_state);
        }

        [Button(ButtonSizes.Medium), HorizontalGroup("Btn")]
        public void Activate() => Activated?.Invoke(_id);

        [Button(ButtonSizes.Medium), HorizontalGroup("Btn")]
        public void Complete() => Completed?.Invoke(_id);

        private void InitReferences() =>
            _module ??= GetComponent<TutorialObjectModule>();

        private void InitState([NotNull] IReadOnlyTutorialState state)
        {
            state.DataUpdated += OnDataUpdated;
            OnDataUpdated(_id, state.GetStatus(_id));
        }

        private void DeinitState([NotNull] IReadOnlyTutorialState state)
        {
            state.DataUpdated -= OnDataUpdated;
        }

        private void OnDataUpdated(TutorialId id, TutorialStatus status)
        {
            if (id != _id) return;

            switch (status)
            {
                case TutorialStatus.Inactive: _module.HandleDeactivate(); break;
                case TutorialStatus.Active: _module.HandleActivate(); break;
                case TutorialStatus.Completed: _module.HandleComplete(); break;
                default: throw new ArgumentOutOfRangeException(nameof(status), status, null);
            }
        }
    }
}
