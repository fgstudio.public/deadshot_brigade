using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    public sealed class PathPainterWidthCache
    {
        private float? _cache;

        public void Cache([NotNull] PathPainter.PathPainter pathPainter) =>
            _cache = pathPainter.Width;

        public void RestoreCache([NotNull] PathPainter.PathPainter pathPainter)
        {
            if (_cache.HasValue)
                pathPainter.Width = _cache.Value;

            _cache = null;
        }
    }
}
