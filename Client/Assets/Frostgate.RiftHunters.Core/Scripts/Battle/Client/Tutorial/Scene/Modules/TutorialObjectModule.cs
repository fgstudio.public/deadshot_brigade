using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    [HelpURL("https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c")]
    public abstract class TutorialObjectModule : MonoBehaviour
    {
        public abstract void HandleActivate();
        public abstract void HandleDeactivate();
        public abstract void HandleComplete();
    }
}
