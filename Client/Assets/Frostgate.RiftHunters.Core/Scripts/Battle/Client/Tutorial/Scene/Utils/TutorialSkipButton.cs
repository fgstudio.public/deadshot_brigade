﻿using System;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services;
using Frostgate.RiftHunters.Core.Systems.Input;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene.Utils
{
    public class TutorialSkipButton : MonoBehaviour
    {
        [SerializeField] private GameObject _root;
        [SerializeField] private Button _button;
        [SerializeField] private MissionConfig _missionConfig;

        private TutorialProgressService _tutorialProgressService;
        private IGameStateMachine _stateMachine;
        private PlayerInput _playerInput;

        [Inject]
        private void MonoConstruct(IPreferences preferences, IGameStateMachine stateMachine,
            PlayerInput playerInput)
        {
            if (!preferences.IsDebugModeEnabled)
                return;

            _stateMachine = stateMachine;
            _playerInput = playerInput;
            _tutorialProgressService = new TutorialProgressService();

            _root.SetActive(true);
            _button.onClick.AddListener(OnSkipClicked);
        }

        private void OnSkipClicked()
        {
            TimeScaleHelper.SetSlowTimeScale(false);
            _tutorialProgressService.SetReadyTutorialMission(_missionConfig.Id);
            _playerInput.gameObject.SetActive(true);
            _stateMachine.EnterAsync<GameLoadMainSceneState, ClientConnectionError>(ClientConnectionError.None);
        }

        private void OnDestroy()
        {
            _button.onClick.RemoveAllListeners();
        }
    }
}