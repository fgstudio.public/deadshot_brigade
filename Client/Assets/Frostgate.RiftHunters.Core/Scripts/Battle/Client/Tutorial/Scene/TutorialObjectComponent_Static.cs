using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    public delegate void ComponentStaticEvent(TutorialObjectComponent component);

    public partial class TutorialObjectComponent
    {
        public static event ComponentStaticEvent Registered;
        public static event ComponentStaticEvent Unregistered;

        private static readonly HashSet<TutorialObjectComponent> components = new();

        public static IEnumerable<TutorialObjectComponent> Components => components;

        private static void Register(TutorialObjectComponent component)
        {
            components.Add(component);
            Registered?.Invoke(component);
        }

        private static void Unregister(TutorialObjectComponent component)
        {
            components.Remove(component);
            Unregistered?.Invoke(component);
        }
    }
}
