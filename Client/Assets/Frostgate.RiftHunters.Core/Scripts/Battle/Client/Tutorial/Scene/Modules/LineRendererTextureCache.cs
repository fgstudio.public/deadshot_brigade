using UnityEngine;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    public sealed class LineRendererTextureCache
    {
        private readonly Dictionary<LineRenderer, Texture> _cache = new();

        public bool IsCached([NotNull] LineRenderer lineRenderer) =>
            _cache.ContainsKey(lineRenderer);

        public void Cache([NotNull] LineRenderer lineRenderer) =>
            _cache[lineRenderer] = lineRenderer.material.mainTexture;

        public void RestoreCache()
        {
            foreach (KeyValuePair<LineRenderer, Texture> kv in _cache)
                if (kv.Key != null)
                    kv.Key.material.mainTexture = kv.Value;

            _cache.Clear();
        }
    }
}
