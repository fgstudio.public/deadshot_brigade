using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c")]
    [AddComponentMenu(TutorialComponentMenus.Menu + "/" + nameof(TutorialBarrelModule))]
    public sealed class TutorialBarrelModule : TutorialObjectModule
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private TutorialInfoModule _infoModule;

        [Required, HideInPrefabAssets]
        [SerializeField] private BarrelNetwork _barrel;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _infoModule ??= GetComponent<TutorialInfoModule>();

        private void Start()
        {
            Subscribe(_barrel);
            OnBarrelHealthChanged(default);
        }

        private void OnDestroy() =>
            Unsubscribe(_barrel);

        private void Subscribe(BarrelNetwork barrel) =>
            barrel.HealthChanged += OnBarrelHealthChanged;

        private void Unsubscribe(BarrelNetwork barrel) =>
            barrel.HealthChanged -= OnBarrelHealthChanged;

        private void OnBarrelHealthChanged(float diff)
        {
            if (_barrel.IsDead)
                _infoModule.HandleComplete();
        }

        public override void HandleActivate() { }
        public override void HandleDeactivate() { }
        public override void HandleComplete() { }
    }
}