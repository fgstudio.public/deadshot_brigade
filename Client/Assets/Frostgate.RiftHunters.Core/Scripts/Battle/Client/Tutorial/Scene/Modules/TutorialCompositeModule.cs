using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c")]
    [AddComponentMenu(TutorialComponentMenus.Menu + "/" + nameof(TutorialCompositeModule))]
    public sealed class TutorialCompositeModule : TutorialObjectModule
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private TutorialObjectModule[] _modules;

        private void Awake() => InitModules();
        private void OnValidate() => InitModules();
        private void InitModules() =>
            _modules ??= GetComponents<TutorialObjectModule>()
                .Where(c => c != this).ToArray();

        public override void HandleActivate()
        {
            foreach (TutorialObjectModule module in _modules)
                module.HandleActivate();
        }

        public override void HandleDeactivate()
        {
            foreach (TutorialObjectModule module in _modules)
                module.HandleDeactivate();
        }

        public override void HandleComplete()
        {
            foreach (TutorialObjectModule module in _modules)
                module.HandleComplete();
        }
    }
}
