namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c"/>
    /// </summary>
    public enum TutorialId
    {
        Movement = 0,
        Rotation = 1,
        TaskPanel = 2,
        AimTarget = 3,
        Ability = 4,
        ExtraAbility = 40,
        Ultimate = 5,
        Dodge = 6,
        Reload = 7,
        Path = 8,
        EnergyCapsule = 9,
        Energy = 10,
        Barrel = 11,
        ArmoredBoss = 12,
        Wave1 = 13,
        Wave2 = 14,
        Wave3 = 15,
        Wave4 = 16,
        None = 100,
    }
}