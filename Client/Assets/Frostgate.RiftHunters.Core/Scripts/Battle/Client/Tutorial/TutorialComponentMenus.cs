namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c"/>
    /// </summary>
    public static class TutorialComponentMenus
    {
        public const string Menu = BattleComponentMenus.Client.Menu + "/Tutorial";
    }
}