using System;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Definitions
{
    public enum TutorialConditionType : byte
    {
        Handling = 0,
        Delay = 1,
        OnArenaStart = 2,
        OnArenaEnd = 3,
        OnTutorialStart = 4,
        OnTutorialEnd = 5,
        OnActivityStart = 6,
        OnActivityEnd = 7,
        OnLocalPlayerImpact = 8,
        OnLocalPlayerEnergyFilled = 9,
        OnLocalPlayerEnergyReset = 10
    }

    [Flags]
    public enum TutorialImpactType : byte
    {
        None = 0,
        Death = 1 << 0,
        Ability = 1 << 1,
        ExtraAbility = 1 << 2,
        Ultimate = 1 << 3,
        Special = 1 << 4,
        Any = byte.MaxValue,
    }

    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c"/>
    /// </summary>
    [Serializable]
    public sealed class TutorialCondition
    {
        [HorizontalGroup, HideLabel, LabelText("Type & Delay"), LabelWidth(80)]
        [SerializeField] private TutorialConditionType _type;

        [HorizontalGroup(Width = 130), AssetSelector, HideLabel, ShowIf(nameof(IsArenaIdUsed))]
        [SerializeField, Required] private BotArenaConfig _arenaConfig;

        [HorizontalGroup(Width = 130), AssetSelector, HideLabel, ShowIf(nameof(IsActivityIdUsed))]
        [SerializeField, Required] private ActivityTaskConfig _activityConfig;

        // TODO: валидировать, что такой Id задан
        [HorizontalGroup(Width = 130), HideLabel, ShowIf(nameof(IsTutorialIdUsed))]
        [SerializeField] private TutorialId _tutorialId;

        [HorizontalGroup(Width = 130), HideLabel, ShowIf(nameof(IsImpactTypeUsed))]
        [SerializeField] private TutorialImpactType _impactType = TutorialImpactType.Any;

        [HorizontalGroup(Width = 50), HideLabel, ShowIf(nameof(_type), TutorialConditionType.Delay), MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _delay;

        public TutorialConditionType Type => _type;
        public TimeSpan Delay => TimeSpan.FromSeconds(_delay);
        public string ArenaId => IsArenaIdUsed ? _arenaConfig.Id : throw wrongTypeException;
        public string ActivityId => IsActivityIdUsed ? _activityConfig.Id : throw wrongTypeException;
        public TutorialId TutorialId => IsTutorialIdUsed ? _tutorialId : throw wrongTypeException;
        public TutorialImpactType ImpactType => IsImpactTypeUsed ? _impactType : throw wrongTypeException;

        private bool IsArenaIdUsed => _type is
            TutorialConditionType.OnArenaStart or TutorialConditionType.OnArenaEnd;
        private bool IsActivityIdUsed => _type is
            TutorialConditionType.OnActivityStart or TutorialConditionType.OnActivityEnd;
        private bool IsTutorialIdUsed => _type is
            TutorialConditionType.OnTutorialStart or TutorialConditionType.OnTutorialEnd;
        private bool IsImpactTypeUsed => _type == TutorialConditionType.OnLocalPlayerImpact;

        private static readonly Exception wrongTypeException =
            new NotImplementedException($"Wrong {nameof(TutorialConditionType)}");
    }
}