using UnityEngine;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Definitions
{
    [HelpURL("https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c")]
    [CreateAssetMenu(fileName = nameof(TutorialConfig),
        menuName = TutorialAssetMenus.Menu + "/" + nameof(TutorialConfig))]
    public sealed class TutorialConfig : ScriptableObject
    {
        [SerializeField] private bool _enabled = true;

        // TODO: Валидация на уникальность Id и на корректность данных в Condition
        [ListDrawerSettings(NumberOfItemsPerPage = 2)]
        [SerializeField] private TutorialStep[] _steps;

        public bool Enabled => _enabled;
        public IReadOnlyList<TutorialStep> Steps => _steps;
    }
}