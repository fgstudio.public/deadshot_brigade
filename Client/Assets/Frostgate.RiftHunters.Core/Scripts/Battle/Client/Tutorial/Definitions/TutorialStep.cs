using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Definitions
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c"/>
    /// </summary>
    [Serializable]
    public sealed class TutorialStep
    {
        [GUIColor(1f, 0.99f, 0.64f)]
        [SerializeField] private TutorialId _id;
        [SerializeField] private bool _needSlowTime;
        [SerializeField, ShowIf(nameof(_needSlowTime))] private SlowTimeInterruptType _slowTimeType;
        [SerializeField, ShowIf(nameof(SlowTimeType), SlowTimeInterruptType.Delay)]
        private float _delayToEndSlowTime;

        [MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _activationDelay;

        [MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _completionDelay;

        [SerializeField] private TutorialCondition[] _activationConditions;
        [SerializeField] private TutorialCondition[] _completionConditions;

        public TutorialId Id => _id;
        public bool NeedSlowTime => _needSlowTime;
        public IReadOnlyList<TutorialCondition> ActivationConditions => _activationConditions;
        public IReadOnlyList<TutorialCondition> CompletionConditions => _completionConditions;

        public float ActivationDelay => _activationDelay;
        public float CompletionDelay => _completionDelay;
        public SlowTimeInterruptType SlowTimeType => _slowTimeType;
        public float DelayToEndSlowTime => _delayToEndSlowTime;
    }
}