﻿using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Awaiters
{
    public class TutorialArenaAwaiter : BaseAwaiterCondition
    {
        private readonly IReadOnlyBotArenaState _arenaState;
        private readonly string _arenaId;
        private readonly bool _isTriggerStartArena;

        public TutorialArenaAwaiter(
            [NotNull] ILogger logger,
            [NotNull] IReadOnlyBotArenaState arenaState,
            [NotNull] string arenaId,
            bool isTriggerStartArena) : base(logger)
        {
            _arenaState = arenaState;
            _arenaId = arenaId;
            _isTriggerStartArena = isTriggerStartArena;
        }

        protected override void OnActivated()
        {
            _arenaState.DataUpdated.AddListener(OnArenaDataUpdated);
            _arenaState.Data.Keys.ForEach(OnArenaDataUpdated);
        }

        protected override void OnDisposed() => _arenaState.DataUpdated.RemoveListener(OnArenaDataUpdated);

        private void OnArenaDataUpdated(string arenaId)
        {
            if (arenaId != _arenaId)
                return;

            var targetState = _isTriggerStartArena ? BotArenaStatus.Activated : BotArenaStatus.Completed;
            if (_arenaState.GetStatus(arenaId) == targetState)
                SetDone();
        }
    }
}