using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial
{
    public enum TutorialStatus
    {
        Inactive,
        Active,
        Completed
    }

    public delegate void TutorialDataUpdated(TutorialId id, TutorialStatus status);

    public interface IReadOnlyTutorialState
    {
        event TutorialDataUpdated DataUpdated;

        TutorialStatus GetStatus(TutorialId id);
        IEnumerable<TutorialId> ActiveTutorials { get; }
    }

    public interface IEditableTutorialState : IReadOnlyTutorialState
    {
        void SetStatus(TutorialId id, TutorialStatus status);
    }

    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/e480a4ddc85a448f87922a14ed3d641c"/>
    /// </summary>
    public sealed class TutorialState : IEditableTutorialState
    {
        public event TutorialDataUpdated DataUpdated;

        private readonly Dictionary<TutorialId, TutorialStatus> _statuses = new();
        private HashSet<TutorialId> _activeTutorials = new();

        public TutorialStatus GetStatus(TutorialId id) =>
            _statuses.TryGetValue(id, out TutorialStatus status)
                ? status : TutorialStatus.Inactive;

        public IEnumerable<TutorialId> ActiveTutorials => _activeTutorials;

        public void SetStatus(TutorialId id, TutorialStatus status)
        {
            if (GetStatus(id) != status)
            {
                _statuses[id] = status;
                if (status == TutorialStatus.Active)
                {
                    _activeTutorials.Add(id);
                }
                else
                {
                    if (_activeTutorials.Contains(id))
                        _activeTutorials.Remove(id);
                }

                DataUpdated?.Invoke(id, status);
            }
        }
    }
}