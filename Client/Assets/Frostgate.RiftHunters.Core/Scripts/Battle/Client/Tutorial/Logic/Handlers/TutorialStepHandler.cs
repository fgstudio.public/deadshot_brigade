﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Awaiters;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial
{
    public class TutorialStepHandler : IDisposable
    {
        public event Action<TutorialId> Completed;
        public event Action<TutorialId> Activated;

        private readonly TutorialId Id;
        private readonly BaseAwaiterCondition _activationAwaiter;
        private readonly BaseAwaiterCondition _completionAwaiter;
        private float _activationDelay;
        private float _completionDelay;

        private bool _isActivationDisposed;
        private bool _isDisposed;

        private CancellationTokenSource _cts;

        public TutorialStepHandler(
            TutorialId id,
            [NotNull] BaseAwaiterCondition activationAwaiter,
            [NotNull] BaseAwaiterCondition completionAwaiter,
            float activationDelay,
            float completionDelay)
        {
            Id = id;
            _activationAwaiter = activationAwaiter;
            _completionAwaiter = completionAwaiter;
            _activationDelay = activationDelay;
            _completionDelay = completionDelay;

            _cts = new CancellationTokenSource();

            _activationAwaiter.Done += OnActivated;
            _completionAwaiter.Done += OnCompleted;

            activationAwaiter.Activate();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _cts.Cancel();
            _cts.Dispose();

            if (!_isActivationDisposed)
                DisposeActivation();

            _completionAwaiter.Done -= OnCompleted;
            _completionAwaiter.Dispose();

            _isDisposed = true;
        }

        private void DisposeActivation()
        {
            _activationAwaiter.Done -= OnActivated;
            _activationAwaiter.Dispose();

            _isActivationDisposed = true;
        }

        private async void OnActivated()
        {
            _cts = new CancellationTokenSource();
            await UniTask.Delay(TimeSpan.FromSeconds(_activationDelay)).AttachExternalCancellation(_cts.Token);

            _completionAwaiter.Activate();
            Activated?.Invoke(Id);

            DisposeActivation();
        }

        private async void OnCompleted()
        {
            _cts = new CancellationTokenSource();
            await UniTask.Delay(TimeSpan.FromSeconds(_completionDelay)).AttachExternalCancellation(_cts.Token);
            Completed?.Invoke(Id);
        }
    }
}