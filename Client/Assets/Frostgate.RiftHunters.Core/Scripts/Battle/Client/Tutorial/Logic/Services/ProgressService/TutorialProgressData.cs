﻿using System;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services
{
    [Serializable]
    public class TutorialProgressData
    {
        public List<string> CompletedTutorials = new();
    }
}