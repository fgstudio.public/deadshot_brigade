﻿using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Awaiters
{
    public class TutorialTriggerAwaiter : BaseAwaiterCondition
    {
        private readonly TutorialId _id;
        private readonly bool _isTriggerActivation;

        public TutorialTriggerAwaiter([NotNull] ILogger logger, TutorialId id, bool isTriggerActivation) : base(logger)
        {
            _id = id;
            _isTriggerActivation = isTriggerActivation;
        }

        protected override void OnActivated()
        {
            TutorialObjectComponent.Registered += OnComponentRegistered;
            TutorialObjectComponent.Unregistered += OnComponentUnregistered;

            TutorialObjectComponent.Components.ForEach(OnComponentRegistered);
        }

        protected override void OnDisposed()
        {
            TutorialObjectComponent.Registered -= OnComponentRegistered;
            TutorialObjectComponent.Unregistered -= OnComponentUnregistered;
        }

        private void OnComponentRegistered(TutorialObjectComponent component)
        {
            if (component.Id == _id)
            {
                component.Activated += OnComponentActivated;
                component.Completed += OnComponentCompleted;
            }
        }

        private void OnComponentUnregistered(TutorialObjectComponent component)
        {
            if (component.Id == _id)
            {
                component.Activated -= OnComponentActivated;
                component.Completed -= OnComponentCompleted;
            }
        }

        private void OnComponentActivated(TutorialId id)
        {
            if (_isTriggerActivation && id == _id)
                SetDone();
        }

        private void OnComponentCompleted(TutorialId id)
        {
            if (!_isTriggerActivation && id == _id)
                SetDone();
        }
    }
}