﻿using System;
using System.Runtime.InteropServices;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Awaiters
{
    public class BaseAwaiterCondition : IDisposable
    {
        public event Action Done;
        public bool IsDone { get; private set; }

        [CanBeNull] private readonly ILogger _logger;
        private bool _isDisposed;

        protected BaseAwaiterCondition([CanBeNull] ILogger logger)
        {
            _logger = logger;
        }

        public void Activate()
        {
            OnActivated();
        }

        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
                _logger?.Log("Finished");

                OnDisposed();
            }
        }

        protected virtual void OnActivated() { }
        protected virtual void OnDisposed() { }

        protected void SetDone()
        {
            if (!IsDone)
            {
                IsDone = true;
                _logger?.Log("Done");

                Done?.Invoke();
            }
        }
    }
}