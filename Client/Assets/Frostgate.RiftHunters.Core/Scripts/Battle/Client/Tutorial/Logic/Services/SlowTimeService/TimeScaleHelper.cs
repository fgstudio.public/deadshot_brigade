﻿using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services
{
    public static class TimeScaleHelper
    {
        public static event Action TimeScaleChanged;
        public static float MultiplierToNormalTimeScale { get; private set; } = 1f;
        private const float _slowTimeScale = 0.01f;

        public static void SetSlowTimeScale(bool enable)
        {
            if (enable)
            {
                MultiplierToNormalTimeScale = 100f;
                ChangeTimeScale(_slowTimeScale);
            }
            else
            {
                MultiplierToNormalTimeScale = 1f;
                ChangeTimeScale(1f);
            }
        }

        private static void ChangeTimeScale(float newValue)
        {
            if (Math.Abs(Time.timeScale - newValue) > 0.02f)
            {
                Time.timeScale = newValue;
                TimeScaleChanged?.Invoke();
            }
        }
    }
}