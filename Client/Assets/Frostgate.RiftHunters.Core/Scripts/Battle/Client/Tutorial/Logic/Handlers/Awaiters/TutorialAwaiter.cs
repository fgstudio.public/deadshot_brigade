﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Awaiters
{
    public class TutorialAwaiter : BaseAwaiterCondition
    {
        [NotNull] private readonly IReadOnlyTutorialState _tutorialState;
        private readonly TutorialId _id;
        private readonly bool _isTriggerStartTutorial;

        public TutorialAwaiter(
            [NotNull] ILogger logger,
            [NotNull] IReadOnlyTutorialState tutorialState,
            TutorialId id,
            bool isStartTutorial) : base(logger)
        {
            _tutorialState = tutorialState;
            _id = id;
            _isTriggerStartTutorial = isStartTutorial;
        }

        protected override void OnActivated() => _tutorialState.DataUpdated += OnTutorialStateDataUpdated;
        protected override void OnDisposed() => _tutorialState.DataUpdated -= OnTutorialStateDataUpdated;

        private void OnTutorialStateDataUpdated(TutorialId id, TutorialStatus status)
        {
            if (id != _id)
                return;

            var targetState = _isTriggerStartTutorial ? TutorialStatus.Active : TutorialStatus.Completed;
            if (targetState == status)
                SetDone();
        }
    }
}