﻿using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Awaiters
{
    public class TutorialActivityAwaiter : BaseAwaiterCondition
    {
        private readonly IReadOnlyActivityTaskState _activityState;
        private readonly string _activityId;
        private readonly bool _isTriggerStartActivity;

        public TutorialActivityAwaiter(
            [NotNull] ILogger logger,
            [NotNull] IReadOnlyActivityTaskState activityState,
            [NotNull] string activityId,
            bool isTriggerStartActivity) : base(logger)
        {
            _activityState = activityState;
            _activityId = activityId;
            _isTriggerStartActivity = isTriggerStartActivity;
        }

        protected override void OnActivated()
        {
            _activityState.Activated.AddListener(CheckCondition);
            _activityState.Completed.AddListener(CheckCondition);

            if (_activityState.Status == ActivityStatus.Active)
                CheckCondition();
        }

        protected override void OnDisposed()
        {
            _activityState.Activated.RemoveListener(CheckCondition);
            _activityState.Completed.RemoveListener(CheckCondition);
        }

        private void CheckCondition()
        {
            if (_activityState.ConfigId != _activityId)
                return;

            var targetStatus = _isTriggerStartActivity ? ActivityStatus.Active : ActivityStatus.Completed;
            if (_activityState.Status == targetStatus)
                SetDone();
        }
    }
}