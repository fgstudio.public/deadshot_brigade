﻿using System;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services
{
    public class TutorialProgressMediator : IDisposable
    {
        private readonly TutorialProgressService _tutorialProgressService;
        private readonly IReadOnlyTutorialState _tutorialState;

        public TutorialProgressMediator(IReadOnlyTutorialState tutorialState, TutorialProgressService tutorialProgressService)
        {
            _tutorialProgressService = tutorialProgressService;

            _tutorialState = tutorialState;
            _tutorialState.DataUpdated += OnTutorialStateUpdated;
        }

        public void Dispose()
        {
            _tutorialState.DataUpdated -= OnTutorialStateUpdated;
        }

        private void OnTutorialStateUpdated(TutorialId id, TutorialStatus status)
        {
            if (status == TutorialStatus.Completed)
                _tutorialProgressService.SetReadyTutorialStep(id);
        }
    }
}