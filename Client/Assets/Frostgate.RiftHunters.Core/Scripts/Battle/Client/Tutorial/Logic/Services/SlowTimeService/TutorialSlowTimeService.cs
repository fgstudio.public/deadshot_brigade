﻿using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Definitions;
using Mirror;
using Zenject;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services
{
    public class TutorialSlowTimeService : NetworkBehaviour
    {
        private IReadOnlyTutorialState _tutorialState;
        private IReadOnlyDictionary<TutorialId, TutorialStep> _steps;

        private HashSet<TutorialId> _activeSlowTimeTutorials = new();

        private ILogger _logger = LoggerFactory.CreateLogger<TutorialSlowTimeService>();

        [Inject]
        private void MonoConstruct(IReadOnlyTutorialState tutorialState,
            IReadOnlyDictionary<TutorialId, TutorialStep> steps)
        {
            _steps = steps;
            _tutorialState = tutorialState;
        }

        public override void OnStartServer()
        {
            if (isClient && isServer)
            {
                _tutorialState.DataUpdated += OnTutorialStateUpdated;
                _logger.Log("Initialized!");
            }
        }

        public override void OnStartClient()
        {
            if (isClientOnly)
                _logger.LogError("You can't use slow time service in multiplayer!");
        }

        private void OnDestroy()
        {
            if (_tutorialState != null)
                _tutorialState.DataUpdated -= OnTutorialStateUpdated;
        }

        private void OnTutorialStateUpdated(TutorialId id, TutorialStatus status)
        {
            if (_steps.TryGetValue(id, out var step))
            {
                if (step.NeedSlowTime)
                {
                    switch (status)
                    {
                        case TutorialStatus.Active:
                            _activeSlowTimeTutorials.Add(id);
                            TimeScaleHelper.SetSlowTimeScale(true);
                            break;
                        case TutorialStatus.Completed:
                            _activeSlowTimeTutorials.Remove(id);
                            if (_activeSlowTimeTutorials.Count == 0)
                                TimeScaleHelper.SetSlowTimeScale(false);
                            break;
                    }
                }
            }
        }
    }
}