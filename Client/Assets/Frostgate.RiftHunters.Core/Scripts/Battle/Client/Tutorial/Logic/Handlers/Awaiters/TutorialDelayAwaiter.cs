﻿using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Awaiters
{
    public class TutorialDelayAwaiter : BaseAwaiterCondition
    {
        [NotNull] private readonly ITimer _timer;

        public TutorialDelayAwaiter(
            TimeSpan delaySec,
            [NotNull] ILogger logger) : base(logger)
        {
            _timer = new Timer(delaySec, true);
        }

        protected override void OnActivated()
        {
            _timer.Elapsed += SetDone;
            _timer.Start();
        }

        protected override void OnDisposed()
        {
            _timer.Stop();
            _timer.Elapsed -= SetDone;

            _timer.Dispose();
        }
    }
}