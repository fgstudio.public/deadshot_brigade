﻿using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Awaiters;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Definitions;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial
{
    public class TutorialStepHandlerFactory
    {
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _units;
        [NotNull] private readonly IReadOnlyBotArenaState _arenaState;
        [NotNull] private readonly IReadOnlyActivityTaskState _activityState;
        [NotNull] private readonly IReadOnlyTutorialState _tutorialState;

        private readonly ILogger _logger = LoggerFactory.CreateLogger<TutorialStepHandlerFactory>();

        public TutorialStepHandlerFactory(
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> units,
            [NotNull] IReadOnlyBotArenaState arenaState,
            // [NotNull] IReadOnlyActivityTaskState activityState,
            [NotNull] IEditableTutorialState tutorialState)
        {
            _units = units;
            _arenaState = arenaState;
            //todo: разобраться, почему не залетает активити стейт
            // _activityState = activityState;
            _tutorialState = tutorialState;
        }

        public TutorialStepHandler Create(TutorialStep step)
        {
            return new TutorialStepHandler(
                step.Id,
                CreateActivationAwaiters(step),
                CreateCompletionAwaiters(step),
                step.ActivationDelay,
                step.CompletionDelay);
        }

        private BaseAwaiterCondition CreateActivationAwaiters(TutorialStep step)
        {
            return CreateAwaiter(step.ActivationConditions.ToArray(), step.Id, true);
        }

        private BaseAwaiterCondition CreateCompletionAwaiters(TutorialStep step)
        {
            return CreateAwaiter(step.CompletionConditions.ToArray(), step.Id, false);
        }

        private BaseAwaiterCondition CreateAwaiter(TutorialCondition[] conditions, TutorialId tutorialId, bool isActivation)
        {
            List<BaseAwaiterCondition> _awaiters = new();

            // if (TryCreateActivityAwaiter(conditions, out var activityAwaiter))
            //     _awaiters.Add(activityAwaiter);

            if (TryCreateArenaAwaiter(conditions, out var arenaAwaiter))
                _awaiters.Add(arenaAwaiter);

            if (TryCreateTriggerAwaiter(conditions, tutorialId, isActivation, out var triggerAwaiter))
                _awaiters.Add(triggerAwaiter);

            if (TryCreateTutorialAwaiter(conditions, out var tutorialAwaiter))
                _awaiters.Add(tutorialAwaiter);

            if (TryCreateLocalPlayerAwaiter(conditions, out var localPlayerAwaiter))
                _awaiters.Add(localPlayerAwaiter);

            if (TryCreateDelayAwaiter(conditions, out var delayAwaiter))
                _awaiters.Add(delayAwaiter);

            if (CheckAwaiter(_awaiters, out var result))
                return result;

            _logger.LogError("Activation and Completion conditions cannot be empty!");
            return null;
        }

        private bool TryCreateActivityAwaiter(TutorialCondition[] conditions, out BaseAwaiterCondition awaiter)
        {
            List<BaseAwaiterCondition> _awaiters = new();
            foreach (var condition in conditions)
            {
                if (condition.Type == TutorialConditionType.OnActivityEnd)
                    _awaiters.Add(new TutorialActivityAwaiter(
                        LoggerFactory.CreateLogger<TutorialActivityAwaiter>(),
                        _activityState,
                        condition.ActivityId,
                        false));
                if (condition.Type == TutorialConditionType.OnActivityStart)
                    _awaiters.Add(new TutorialActivityAwaiter(
                        LoggerFactory.CreateLogger<TutorialActivityAwaiter>(),
                        _activityState,
                        condition.ActivityId,
                        true));
            }

            return CheckAwaiter(_awaiters, out awaiter);
        }

        private bool TryCreateArenaAwaiter(TutorialCondition[] conditions, out BaseAwaiterCondition awaiter)
        {
            List<BaseAwaiterCondition> _awaiters = new();
            foreach (var condition in conditions)
            {
                if (condition.Type == TutorialConditionType.OnArenaEnd)
                    _awaiters.Add(new TutorialArenaAwaiter(
                        LoggerFactory.CreateLogger<TutorialArenaAwaiter>(),
                        _arenaState,
                        condition.ArenaId,
                        false));
                if (condition.Type == TutorialConditionType.OnArenaStart)
                    _awaiters.Add(new TutorialArenaAwaiter(
                        LoggerFactory.CreateLogger<TutorialArenaAwaiter>(),
                        _arenaState,
                        condition.ArenaId,
                        true));
            }

            return CheckAwaiter(_awaiters, out awaiter);
        }

        private bool TryCreateTutorialAwaiter(TutorialCondition[] conditions, out BaseAwaiterCondition awaiter)
        {
            List<BaseAwaiterCondition> _awaiters = new();
            foreach (var condition in conditions)
            {
                if (condition.Type == TutorialConditionType.OnTutorialEnd)
                    _awaiters.Add(new TutorialAwaiter(
                        LoggerFactory.CreateLogger<TutorialAwaiter>(),
                        _tutorialState,
                        condition.TutorialId,
                        false));
                if (condition.Type == TutorialConditionType.OnTutorialStart)
                    _awaiters.Add(new TutorialAwaiter(
                        LoggerFactory.CreateLogger<TutorialAwaiter>(),
                        _tutorialState,
                        condition.TutorialId,
                        true));
            }

            return CheckAwaiter(_awaiters, out awaiter);
        }

        private bool TryCreateTriggerAwaiter(TutorialCondition[] conditions, TutorialId id,
            bool isActivation, out BaseAwaiterCondition awaiter)
        {
            List<BaseAwaiterCondition> _awaiters = new();
            foreach (var condition in conditions)
            {
                if (condition.Type == TutorialConditionType.Handling)
                    _awaiters.Add(new TutorialTriggerAwaiter(
                        LoggerFactory.CreateLogger<TutorialTriggerAwaiter>(),
                        id,
                        isActivation));
            }

            return CheckAwaiter(_awaiters, out awaiter);
        }

        private bool TryCreateLocalPlayerAwaiter(TutorialCondition[] conditions, out BaseAwaiterCondition awaiter)
        {
            List<BaseAwaiterCondition> _awaiters = new();
            foreach (var condition in conditions)
            {
                if (condition.Type == TutorialConditionType.OnLocalPlayerImpact ||
                    condition.Type == TutorialConditionType.OnLocalPlayerEnergyFilled ||
                    condition.Type == TutorialConditionType.OnLocalPlayerEnergyReset)
                    _awaiters.Add(new TutorialLocalPlayerAwaiter(
                        LoggerFactory.CreateLogger<TutorialLocalPlayerAwaiter>(),
                        _units,
                        condition.Type,
                        condition.ImpactType));
            }

            return CheckAwaiter(_awaiters, out awaiter);
        }

        private bool TryCreateDelayAwaiter(TutorialCondition[] conditions, out BaseAwaiterCondition awaiter)
        {
            List<BaseAwaiterCondition> _awaiters = new();
            foreach (var condition in conditions)
            {
                if (condition.Type == TutorialConditionType.Delay)
                    _awaiters.Add(new TutorialDelayAwaiter(
                        condition.Delay,
                        LoggerFactory.CreateLogger<TutorialDelayAwaiter>()));
            }

            return CheckAwaiter(_awaiters, out awaiter);
        }

        private bool CheckAwaiter(List<BaseAwaiterCondition> _awaiters, out BaseAwaiterCondition awaiter)
        {
            if (_awaiters.Count == 0)
            {
                awaiter = null;
                return false;
            }

            awaiter = new CompositeAwaiterCondition(_awaiters);
            return true;
        }
    }
}