﻿using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Definitions;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Awaiters
{
    public class TutorialLocalPlayerAwaiter : BaseAwaiterCondition
    {
        private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _units;
        private readonly TutorialConditionType _conditionType;
        private readonly TutorialImpactType _impactType;

        [CanBeNull] private UnitNetwork _localPlayer;

        public TutorialLocalPlayerAwaiter(
            [NotNull] ILogger logger,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> units,
            TutorialConditionType conditionType,
            TutorialImpactType impactType) : base(logger)
        {
            _units = units;
            _conditionType = conditionType;
            _impactType = impactType;
        }

        protected override void OnActivated()
        {
            _units.Added += OnUnitAdded;
            _units.Removed += OnUnitRemoved;

            _units.Values.ForEach(OnUnitAdded);
        }

        protected override void OnDisposed()
        {
            _units.Added -= OnUnitAdded;
            _units.Removed -= OnUnitRemoved;
        }

        private void OnUnitAdded(UnitNetwork unit)
        {
            if (!unit.Identity.isLocalPlayer) return;

            _localPlayer = unit;
            unit.UnitNetworkState.EnergyReset += OnLocalPlayerEnergyReset;
            unit.UnitNetworkState.EnergyFilled += OnLocalPlayerEnergyFilled;
            unit.ImpactState.CastStarted.AddListener(OnLocalPlayerCastStarted);
        }

        private void OnUnitRemoved(UnitNetwork unit)
        {
            if (!unit.Identity.isLocalPlayer) return;

            _localPlayer = null;
            unit.UnitNetworkState.EnergyReset -= OnLocalPlayerEnergyReset;
            unit.UnitNetworkState.EnergyFilled -= OnLocalPlayerEnergyFilled;
            unit.ImpactState.CastStarted.RemoveListener(OnLocalPlayerCastStarted);
        }

        private void OnLocalPlayerEnergyReset()
        {
            if (_conditionType == TutorialConditionType.OnLocalPlayerEnergyReset)
                SetDone();
        }

        private void OnLocalPlayerEnergyFilled()
        {
            if (_conditionType == TutorialConditionType.OnLocalPlayerEnergyFilled)
                SetDone();
        }

        private void OnLocalPlayerCastStarted(string impactId)
        {
            if (_conditionType == TutorialConditionType.OnLocalPlayerImpact)
                IsImpactMatched(impactId, _impactType);
        }

        private void IsImpactMatched(string impactId, TutorialImpactType impactType)
        {
            if (_localPlayer == null) return;
            if (impactType == TutorialImpactType.Any) SetDone();
            if (impactType == TutorialImpactType.None) return;

            ImpactModelRoster impactRoster = _localPlayer.ImpactRoster;
            if (impactType.HasFlag(TutorialImpactType.Death) && impactRoster.Death.Data.Config?.Id == impactId) SetDone();
            if (impactType.HasFlag(TutorialImpactType.Ability) && impactRoster.Ability.Data.Config?.Id == impactId) SetDone();
            if (impactType.HasFlag(TutorialImpactType.Ultimate) && impactRoster.Ultimate.Data.Config?.Id == impactId) SetDone();
            if (impactType.HasFlag(TutorialImpactType.ExtraAbility) && impactRoster.ExtraAbility.Data.Config?.Id == impactId) SetDone();
        }
    }
}