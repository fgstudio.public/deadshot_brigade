﻿using System;
using System.Collections.Generic;
using System.Threading;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Definitions;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial
{
    public class TutorialMediator : IDisposable
    {
        private readonly IEditableTutorialState _tutorialState;
        private readonly IReadOnlyDictionary<TutorialId, TutorialStep> _steps;
        private readonly TutorialStepHandlerFactory _stepHandlerFactory;
        private readonly TutorialProgressService _tutorialProgressService;
        private readonly TutorialProgressMediator _tutorialProgressMediator;

        private readonly CancellationTokenSource _cts = new();
        private readonly ILogger _logger = LoggerFactory.CreateLogger<TutorialMediator>();

        private IMissionConfig _missionConfig;
        private HashSet<TutorialStepHandler> _stepHandlers = new();
        private HashSet<TutorialId> _completedTutorials = new();

        public TutorialMediator(
            [NotNull] IEditableTutorialState tutorialState,
            [NotNull] IReadOnlyDictionary<TutorialId, TutorialStep> steps,
            [NotNull] TutorialStepHandlerFactory stepHandlerFactory)
        {
            _tutorialState = tutorialState;
            _steps = steps;
            _stepHandlerFactory = stepHandlerFactory;
            _tutorialProgressService = new TutorialProgressService();
            _tutorialProgressMediator = new TutorialProgressMediator(_tutorialState, _tutorialProgressService);

            foreach (var tutorialStep in _steps.Values)
            {
                var stepHandler = _stepHandlerFactory.Create(tutorialStep);
                _stepHandlers.Add(stepHandler);
                stepHandler.Activated += OnStepHandlerActivated;
                stepHandler.Completed += OnStepHandlerCompleted;
            }
        }

        private void OnStepHandlerActivated(TutorialId id)
        {
            _tutorialState.SetStatus(id, TutorialStatus.Active);
        }

        private void OnStepHandlerCompleted(TutorialId id)
        {
            _tutorialState.SetStatus(id, TutorialStatus.Completed);
            _completedTutorials.Add(id);

            if (AllStepsCompleted)
                SaveProgress();
        }

        private void SaveProgress()
        {
            _tutorialProgressService.SetReadyTutorialMission(_missionConfig.Id);
            _logger.Log($"Tutorial mission |{_missionConfig.Id}| completed! Progress saved!");
        }

        private bool AllStepsCompleted => _completedTutorials.Count == _stepHandlers.Count;

        public void InitMissionConfig(IMissionConfig missionConfig)
        {
            _missionConfig = missionConfig;
        }

        public void Dispose()
        {
            _cts?.Dispose();

            _stepHandlers.ForEach(t => t.Dispose());
        }
    }
}