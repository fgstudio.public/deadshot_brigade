﻿namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services
{
    public enum SlowTimeInterruptType
    {
        EndStep = 0,
        Delay = 1,
        ClickScreen = 2,
        Button = 3
    }
}