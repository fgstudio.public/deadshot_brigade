﻿using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Awaiters
{
    public class CompositeAwaiterCondition : BaseAwaiterCondition
    {
        private readonly List<BaseAwaiterCondition> _awaiters;

        public CompositeAwaiterCondition(List<BaseAwaiterCondition> awaiters, ILogger logger = null) : base(logger)
        {
            _awaiters = awaiters;
        }

        protected override void OnActivated()
        {
            foreach (var awaiter in _awaiters)
            {
                awaiter.Done += OnAwaiterDone;
                awaiter.Activate();
            }
        }

        protected override void OnDisposed()
        {
            foreach (var awaiter in _awaiters)
            {
                awaiter.Done -= OnAwaiterDone;
                awaiter.Dispose();
            }
        }

        private void OnAwaiterDone()
        {
            SetDone();
        }
    }
}