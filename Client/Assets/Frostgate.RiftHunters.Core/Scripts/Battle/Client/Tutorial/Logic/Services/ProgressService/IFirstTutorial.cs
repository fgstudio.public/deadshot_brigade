﻿namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services
{
    public interface IFirstTutorial
    {
        MissionConfig FirstTutorialConfig { get; }
    }
}