﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services
{
    public class TutorialProgressService
    {
        private static string _tutorialProgressPath = "TutorialProgress";
        private TutorialProgressData _chachedData;

        public TutorialProgressService()
        {
            if (PlayerPrefs.HasKey(_tutorialProgressPath))
            {
                _chachedData = JsonUtility.FromJson<TutorialProgressData>(PlayerPrefs.GetString(_tutorialProgressPath));
            }
            else
            {
                _chachedData = new TutorialProgressData();
            }
        }

        public bool CheckReadyTutorialStep(TutorialId id)
        {
            return _chachedData.CompletedTutorials.Contains(id.ToString());
        }

        public bool CheckReadyTutorialMission(string missionId)
        {
            return PlayerPrefs.HasKey(_tutorialProgressPath + "_" + missionId[^6..]);
        }

        public void SetReadyTutorialStep(TutorialId id)
        {
            if (!_chachedData.CompletedTutorials.Contains(id.ToString()))
            {
                _chachedData.CompletedTutorials.Add(id.ToString());
                var json = JsonUtility.ToJson(_chachedData);
                PlayerPrefs.SetString(_tutorialProgressPath, json);
            }
        }

        public void SetReadyTutorialMission(string missionId)
        {
            PlayerPrefs.SetString(_tutorialProgressPath + "_" + missionId[^6..], missionId);
        }
    }
}