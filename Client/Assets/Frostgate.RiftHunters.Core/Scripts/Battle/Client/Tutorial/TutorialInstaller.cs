using System.Linq;
using System.Collections.Generic;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Definitions;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Scene;

namespace Frostgate.RiftHunters.Core.Battle.Client.Tutorial
{
    [HelpURL("https://www.notion.so/frostgate/Day-1-0e5f94937235435e99510a10bd7b0010")]
    public sealed class TutorialInstaller : MonoInstaller
    {
        [SerializeField, AssetsOnly, Required, ValidateInput(nameof(CheckIds))] private TutorialConfig[] _configs;

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<TutorialState>().AsSingle();
            Container.Bind<TutorialStepHandlerFactory>().AsSingle();

            Container.Bind<IReadOnlyDictionary<TutorialId, TutorialStep>>().FromInstance(
                _configs.Where(c => c.Enabled).SelectMany(c => c.Steps)
                    .ToDictionary(s => s.Id, s => s)).AsSingle();
        }

        [Button]
        private bool CheckIds()
        {
            HashSet<TutorialId> hasConfig = new();
            HashSet<TutorialId> allNeedConfig = new();
            var allComponents = FindObjectsOfType<TutorialObjectComponent>();
            foreach (var tutorialObjectComponent in allComponents)
            {
                allNeedConfig.Add(tutorialObjectComponent.Id);
            }

            foreach (var tutorialConfig in _configs)
            {
                foreach (var tutorialStep in tutorialConfig.Steps)
                {
                    hasConfig.Add(tutorialStep.Id);
                }
            }

            UnityEngine.Debug.LogError($"|TutorialInstaller| Check configs! Result {hasConfig.Count}/{allNeedConfig.Count}");
            if (hasConfig.Count != allNeedConfig.Count)
            {
                HashSet<TutorialId> needConfig = new();
                foreach (var tutorialId in allNeedConfig)
                {
                    if (!hasConfig.Contains(tutorialId))
                        needConfig.Add(tutorialId);
                }

                foreach (var needTutorialId in needConfig)
                {
                    UnityEngine.Debug.LogError($"|TutorialInstaller| You need to add {needTutorialId}");
                }

                return false;
            }

            return true;
        }
    }
}