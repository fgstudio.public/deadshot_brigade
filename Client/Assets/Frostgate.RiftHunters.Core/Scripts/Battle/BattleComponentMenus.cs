﻿namespace Frostgate.RiftHunters.Core
{
    public static class BattleComponentMenus
    {
        public const string Menu = ComponentMenus.Menu + "/Battle";

        public static class Components
        {
            public const string Menu = BattleComponentMenus.Menu + "/" + nameof(Components);

            public static class P2P { public const string Menu = Components.Menu + "/" + nameof(P2P); }
            public static class Filters { public const string Menu = Components.Menu + "/" + nameof(Filters); }
            public static class Observable { public const string Menu = Components.Menu + "/" + nameof(Observable); }
        }

        public static class Shared
        {
            public const string Menu = BattleComponentMenus.Menu + "/" + nameof(Shared);

            public static class Utils { public const string Menu = Shared.Menu + "/" + nameof(Utils); }
            public static class Tools { public const string Menu = Shared.Menu + "/" + nameof(Tools); }
            public static class Cheats { public const string Menu = Shared.Menu + "/" + nameof(Cheats); }
            public static class Impacts { public const string Menu = Shared.Menu + "/" + nameof(Impacts); }
        }

        public static class Client
        {
            public const string Menu = BattleComponentMenus.Menu + "/" + nameof(Client);

            public static class Input { public const string Menu = Client.Menu + "/" + nameof(Input); }
            public static class FX { public const string Menu = Client.Menu + "/" + nameof(FX); }
            public static class Targeting { public const string Menu = Client.Menu + "/" + nameof(Targeting); }
            public static class Impacts { public const string Menu = Client.Menu + "/" + nameof(Impacts); }

            public static class UI
            {
                public const string Menu = Client.Menu + "/" + nameof(UI);

                public static class Animations { public const string Menu = UI.Menu + "/" + nameof(Animations); }
                public static class Impacts { public const string Menu = UI.Menu + "/" + nameof(Impacts); }
                public static class Targeting { public const string Menu = UI.Menu + "/" + nameof(Targeting); }
            }
        }

        public static class Server
        {
            public const string Menu = BattleComponentMenus.Menu + "/" + nameof(Server);

            public static class Impacts { public const string Menu = Server.Menu + "/" + nameof(Impacts); }
            public static class Systems { public const string Menu = Server.Menu + "/" + nameof(Systems); }
        }

    }
}