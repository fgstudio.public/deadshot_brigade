using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle
{
    // TODO: лишняя прослойка для создания объектов,
    // т.к. Mediator'ы и Facade'ы на сервре создаются явно в ServerBattleLogic
    // и их нельзя заинжектить или как-то удобно получиться для вызова создания объекта
    public sealed class ObjectCommandSender<TObject> where TObject : class
    {
        private readonly ObjectCommandReceiver<TObject> _commandReceiver;

        public ObjectCommandSender(ObjectCommandReceiver<TObject> commandReceiver) =>
            _commandReceiver = commandReceiver;

        public void SendInstantiateForUnit([NotNull] TObject prefab, [CanBeNull] NetworkIdentity targetIdentity, Vector3 position) =>
            _commandReceiver.InvokeInstantiateForUnit(prefab, targetIdentity, position);

        public void SendSpawnOnNearestSpawner(
            [NotNull] BotSpawnerConfig spawnerConfig, [NotNull] IPositionProvider positionProvider) =>
            _commandReceiver.InvokeSendSpawnerOnNearestSpawner(spawnerConfig, positionProvider);
    }
}