using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle
{
    public interface IObjectCommandReceiver<TObject> where TObject : class
    {
        delegate void InstantiateCommand<TPayload1, TPayload2>([NotNull] TObject prefab, TPayload1 p1, TPayload2 p2);
        delegate void SpawnCommand<TPayload>([NotNull] BotSpawnerConfig config, TPayload payload);

        event InstantiateCommand<NetworkIdentity, Vector3> InstantiateForUnitReceived;
        event SpawnCommand<IPositionProvider> SpawnOnNearestSpawnerReceived;
    }

    // TODO: лишняя прослойка для создания объектов,
    // т.к. Mediator'ы и Facade'ы на сервре создаются явно в ServerBattleLogic
    // и их нельзя заинжектить или как-то удобно получиться для вызова создания объекта
    public sealed class ObjectCommandReceiver<TObject> : IObjectCommandReceiver<TObject> where TObject : class
    {
        public event IObjectCommandReceiver<TObject>.InstantiateCommand<NetworkIdentity, Vector3> InstantiateForUnitReceived;
        public event IObjectCommandReceiver<TObject>.SpawnCommand<IPositionProvider> SpawnOnNearestSpawnerReceived;

        public void InvokeInstantiateForUnit([NotNull] TObject prefab,
            [CanBeNull] NetworkIdentity targetIdentity, Vector3 position) =>
            InstantiateForUnitReceived?.Invoke(prefab, targetIdentity, position);

        public void InvokeSendSpawnerOnNearestSpawner(
            [NotNull] BotSpawnerConfig spawnerConfig, [NotNull] IPositionProvider positionProvider) =>
            SpawnOnNearestSpawnerReceived?.Invoke(spawnerConfig, positionProvider);
    }
}