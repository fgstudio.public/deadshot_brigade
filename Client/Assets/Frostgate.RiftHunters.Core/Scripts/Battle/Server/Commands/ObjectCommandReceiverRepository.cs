using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle
{
    public interface IReadOnlyObjectCommandReceiverRepository
    {
        bool Contains<TObject>() where TObject : class;
        [NotNull] IObjectCommandReceiver<TObject> Get<TObject>() where TObject : class;
    }

    // TODO: лишняя прослойка для создания объектов,
    // т.к. Mediator'ы и Facade'ы на сервре создаются явно в ServerBattleLogic
    // и их нельзя заинжектить или как-то удобно получиться для вызова создания объекта
    public sealed class ObjectCommandReceiverRepository : IReadOnlyObjectCommandReceiverRepository
    {
        private readonly Dictionary<Type, object> _receivers = new();

        public void Add<TObject>([NotNull] IObjectCommandReceiver<TObject> receiver) where TObject : class =>
            _receivers[typeof(TObject)] = receiver;

        public bool Contains<TObject>() where TObject : class =>
            _receivers.ContainsKey(typeof(TObject));

        public IObjectCommandReceiver<TObject> Get<TObject>() where TObject : class =>
            (_receivers[typeof(TObject)] as IObjectCommandReceiver<TObject>)!;

        public void Remove<TObservable>() where TObservable : class =>
            _receivers.Remove(typeof(TObservable));
    }
}
