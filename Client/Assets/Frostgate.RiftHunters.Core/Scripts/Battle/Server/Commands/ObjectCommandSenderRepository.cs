using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle
{
    public interface IReadOnlyObjectCommandSenderRepository
    {
        bool Contains<TObject>() where TObject : class;
        [NotNull] ObjectCommandSender<TObject> Get<TObject>() where TObject : class;
    }

    // TODO: лишняя прослойка для создания объектов,
    // т.к. Mediator'ы и Facade'ы на сервре создаются явно в ServerBattleLogic
    // и их нельзя заинжектить или как-то удобно получиться для вызова создания объекта
    public sealed class ObjectCommandSenderRepository : IReadOnlyObjectCommandSenderRepository
    {
        private readonly Dictionary<Type, object> _senders = new();

        public void Add<TObject>([NotNull] ObjectCommandSender<TObject> receiver) where TObject : class =>
            _senders[typeof(TObject)] = receiver;

        public bool Contains<TObject>() where TObject : class =>
            _senders.ContainsKey(typeof(TObject));

        public ObjectCommandSender<TObject> Get<TObject>() where TObject : class =>
            (_senders[typeof(TObject)] as ObjectCommandSender<TObject>)!;

        public void Remove<TObservable>() where TObservable : class =>
            _senders.Remove(typeof(TObservable));
    }
}
