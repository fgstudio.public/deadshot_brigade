﻿using System.Linq;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.ClearArenaActivity;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.ClearArenaActivity
{
    public sealed class ClearArenaActivityTask : ActivityTask<ClearArenaActivityConfig>
    {
        // TODO: Сохранять созданных ботов в модели, на клиенте подписывать UI волн на модель.
        [NotNull] private readonly ClearArenaTaskState _taskState;
        [NotNull] private readonly IReadOnlyBotSpawnState _botSpawnState;

        public ClearArenaActivityTask(
            [NotNull] ClearArenaTaskSceneData taskSceneData,
            [NotNull] ClearArenaActivityConfig config,
            [NotNull] IReadOnlyBotSpawnState botSpawnState) : base(
            taskSceneData.TaskState, config)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData, nameof(taskSceneData));

            _botSpawnState = botSpawnState;
            _taskState = taskSceneData.TaskState;
        }

        protected override void Dispose(bool dispose) =>
            _botSpawnState.DataUpdated.RemoveListener(OnDataUpdated);

        protected override void OnActivated()
        {
            _botSpawnState.DataUpdated.AddListener(OnDataUpdated);
            _botSpawnState.Data.Keys.ToArray().ForEach(OnDataUpdated);
            base.OnActivated();
        }

        protected override void OnDeactivated()
        {
            _botSpawnState.DataUpdated.RemoveListener(OnDataUpdated);
            base.OnDeactivated();
        }

        private void OnDataUpdated(string spawnerId)
        {
            if (!_botSpawnState.Data.TryGetValue(spawnerId, out BotSpawnerData data)) return;
            if (data.Status != BotSpawnerStatus.Completed) return;
            if (!_taskState.IsSpawnerIdTarget(spawnerId)) return;
            if (_taskState.IsSpawnerIdCompleted(spawnerId)) return;

            _taskState.AddCompletedSpawnerId(spawnerId);

            if (_taskState.AreTargetSpawnersCompleted)
                Complete();
        }
    }
}