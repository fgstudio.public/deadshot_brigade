﻿using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.SurviveArenaActivity
{
    public sealed class SurviveArenaActivityTask : ActivityTask<SurviveArenaActivityConfig>
    {
        // TODO: Сохранять созданных ботов в модели, на клиенте подписывать UI волн на модель.
        [NotNull] private readonly SurviveArenaTaskState _taskState;
        [NotNull] private readonly IReadOnlyBotSpawnState _spawnState;

        public SurviveArenaActivityTask(
            [NotNull] SurviveArenaTaskSceneData taskSceneData,
            [NotNull] SurviveArenaActivityConfig config,
            [NotNull] IReadOnlyBotSpawnState spawnState) : base(
            taskSceneData.TaskState, config)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData, nameof(taskSceneData));

            _spawnState = spawnState;
            _taskState = taskSceneData.TaskState;
        }

        protected override void OnActivated()
        {
            Subscribe(_spawnState);
            base.OnActivated();
        }

        protected override void OnDeactivated()
        {
            Unsubscribe(_spawnState);
            base.OnDeactivated();
        }

        protected override void Dispose(bool dispose) =>
            Unsubscribe(_spawnState);

        private void Subscribe([NotNull] IReadOnlyBotSpawnState spawnState) =>
            spawnState.DataUpdated.AddListener(OnDataUpdated);

        private void Unsubscribe([NotNull] IReadOnlyBotSpawnState spawnState) =>
            spawnState.DataUpdated.RemoveListener(OnDataUpdated);

        private void OnDataUpdated(string spawnerId)
        {
            if (!_spawnState.Data.TryGetValue(spawnerId, out BotSpawnerData data)) return;
            if (data.Status != BotSpawnerStatus.Completed) return;
            if (!_taskState.IsSpawnerIdTarget(spawnerId)) return;
            if (_taskState.IsSpawnerIdCompleted(spawnerId)) return;

            _taskState.AddCompletedSpawnerId(spawnerId);

            if (_taskState.AreTargetSpawnersCompleted)
                Complete();
        }
    }
}