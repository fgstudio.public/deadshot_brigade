﻿using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.CartEscortActivity
{
    public sealed class PlayerUnitsKiller
    {
        [NotNull] private readonly IDamageService _damageService;
        [NotNull] private readonly IDamageCaster _damageCaster;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;

        public PlayerUnitsKiller([NotNull] IDamageService damageService, [NotNull] IDamageCaster damageCaster,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(damageService, nameof(damageService));
            ThrowHelper.ThrowIfArgumentNullOrDefault(damageCaster, nameof(damageCaster));
            ThrowHelper.ThrowIfArgumentNullOrDefault(unitCollection, nameof(unitCollection));

            _damageService = damageService;
            _damageCaster = damageCaster;
            _unitCollection = unitCollection;
        }

        public void Kill()
        {
            UnitNetwork[] unitsToKill = _unitCollection.Where(IsCanBeKilled).ToArray();
            unitsToKill.ForEach(Kill);
        }

        private void Kill(UnitNetwork unit) => _damageService.KillInstantly(_damageCaster, unit);

        private bool IsCanBeKilled(UnitNetwork unit) =>
            unit != null && !unit.IsDestroyedOrDead && unit.UnitType == BattleUnitType.Player;
    }
}