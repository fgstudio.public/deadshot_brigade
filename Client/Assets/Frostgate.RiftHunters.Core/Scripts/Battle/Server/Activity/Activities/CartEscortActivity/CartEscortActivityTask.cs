﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.CartEscortActivity
{
    public sealed class CartEscortActivityTask : ActivityTask<CartEscortActivityConfig>
    {
        [NotNull] private readonly CartEscortTaskState _taskState;
        [NotNull] private readonly CartEscortCapturableArea _capturableArea;
        [NotNull] private readonly Cart _cart;

        public CartEscortActivityTask([NotNull] CartEscortTaskSceneData taskSceneData,
            [NotNull] CartEscortActivityConfig config) : base(taskSceneData.TaskState, config)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData.TaskState, nameof(taskSceneData.TaskState));
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData.CapturableArea,
                nameof(taskSceneData.CapturableArea));
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData.Cart, nameof(taskSceneData.Cart));

            _taskState = taskSceneData.TaskState;
            _capturableArea = taskSceneData.CapturableArea;
            _cart = taskSceneData.Cart;

            InitActivityObjects();
        }

        protected override void OnActivated()
        {
            _capturableArea.Capturing.Catcher.Caught.AddListener(OnInvaderCaught);
            _capturableArea.Capturing.Catcher.Lost.AddListener(OnInvaderLost);

            _cart.MovementBehaviour.PathCompleted.AddListener(OnCartMovementCompleted);
            _cart.MovementBehaviour.WaypointReached.AddListener(OnCartWaypointReached);

            _capturableArea.Capturing.Enable();

            base.OnActivated();
        }

        private void OnCartWaypointReached(int waypointIndex)
        {
            if (waypointIndex == 0)
                _cart.View.MovementIndicator.DisableIndication();
        }

        protected override void OnDeactivated()
        {
            _capturableArea.Capturing.Catcher.Caught.RemoveListener(OnInvaderCaught);
            _capturableArea.Capturing.Catcher.Lost.RemoveListener(OnInvaderLost);

            _cart.MovementBehaviour.PathCompleted.RemoveListener(OnCartMovementCompleted);
            _cart.MovementBehaviour.WaypointReached.RemoveListener(OnCartWaypointReached);

            _capturableArea.Capturing.Disable();

            base.OnDeactivated();
        }

        private void CompleteActivity()
        {
            _capturableArea.Disable();

            _capturableArea.Capturing.Catcher.Caught.RemoveListener(OnInvaderCaught);
            _capturableArea.Capturing.Catcher.Lost.RemoveListener(OnInvaderLost);

            _cart.MovementBehaviour.PathCompleted.RemoveListener(OnCartMovementCompleted);
            _cart.MovementBehaviour.WaypointReached.RemoveListener(OnCartWaypointReached);

            Complete();
        }

        private void OnInvaderCaught(Collider collider)
        {
            UpdateCartSpeed();
            _cart.MovementBehaviour.Direction = MovementDirection.Forward;
            _cart.MovementBehaviour.StartPathMovement();
        }

        private void OnInvaderLost(Collider collider)
        {
            UpdateCartSpeed();
            if (_capturableArea.Capturing.Catcher.Objects.Count == 0)
                _cart.MovementBehaviour.Direction = MovementDirection.Backward;
        }

        private void InitActivityObjects()
        {
            _cart.MovementBehaviour.MovementSpeed = _taskState.Config!.GetForwardSpeed(1);
            _cart.MovementBehaviour.RotationSpeed = _taskState.Config!.CartRotationSpeed;
            _cart.MovementBehaviour.ResetPathMovement();

            _cart.View.SetActive(false);

            _capturableArea.Capturing.Radius = _taskState.Config!.CartCapturableAreaRadius;
            _capturableArea.Disable();
        }

        private void OnCartMovementCompleted()
        {
            _cart.MovementBehaviour.StopPathMovement();
            CompleteActivity();
        }

        private void UpdateCartSpeed()
        {
            CartEscortActivityConfig config = _taskState.Config!;

            _cart.MovementBehaviour.MovementSpeed =
                config!.GetForwardSpeed(_capturableArea.Capturing.Catcher.Objects.Count);
            _cart.MovementBehaviour.RotationSpeed = config.CartRotationSpeed;
        }
    }
}