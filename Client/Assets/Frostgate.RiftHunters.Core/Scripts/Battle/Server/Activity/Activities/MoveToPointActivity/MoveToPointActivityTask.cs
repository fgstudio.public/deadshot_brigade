﻿using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.MoveToPointActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;
using JetBrains.Annotations;
using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.MoveToPointActivity
{
    public class MoveToPointActivityTask : ActivityTask<MoveToPointActivityConfig>
    {
        private readonly ObjectCatcher _objectCatcher;
        private readonly MoveToPointActivityConfig.MoveToPointActivityType _moveToPointType;

        private readonly HashSet<Collider> _caughtUnits = new();

        public MoveToPointActivityTask(
            [NotNull] MoveToPointTaskSceneData taskSceneData,
            [NotNull] MoveToPointActivityConfig config) : base(
            taskSceneData.TaskState, config)
        {
            _objectCatcher = taskSceneData.TargetPoint;
            _moveToPointType = config.Type;
        }

        protected override void OnActivated()
        {
            Subscribes();
        }

        protected override void OnDeactivated()
        {
            UnSubscribes();
        }

        protected override void Dispose(bool dispose)
        {
            _caughtUnits.Clear();
            base.Dispose(dispose);
        }

        private void Subscribes()
        {
            _objectCatcher.Caught.AddListener(OnCaught);
            _objectCatcher.Lost.AddListener(OnLost);

            _objectCatcher.Objects.ForEach(OnCaught);
        }

        private void UnSubscribes()
        {
            _objectCatcher.Caught.RemoveListener(OnCaught);
            _objectCatcher.Lost.RemoveListener(OnLost);
        }

        private void OnCaught(Collider playerCollider)
        {
            _caughtUnits.Add(playerCollider);
            if (CheckCompleteConditions())
            {
                UnSubscribes();
                Complete();
            }
        }

        private void OnLost(Collider playerCollider)
        {
            _caughtUnits.Remove(playerCollider);
        }

        private bool CheckCompleteConditions()
        {
            switch (_moveToPointType)
            {
                case MoveToPointActivityConfig.MoveToPointActivityType.Any:
                    return true;
                case MoveToPointActivityConfig.MoveToPointActivityType.All:
                    if (_caughtUnits.Count >= NetworkServer.connections.Count)
                        return true;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return false;
        }
    }
}