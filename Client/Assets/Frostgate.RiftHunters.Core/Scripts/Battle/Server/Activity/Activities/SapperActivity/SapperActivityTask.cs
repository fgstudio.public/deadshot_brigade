﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using System;
using System.Linq;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.SapperActivity
{
    public sealed class SapperActivityTask : ActivityTask<SapperActivityConfig>
    {
        [NotNull] private readonly SapperTaskState _taskState;
        [NotNull] private readonly CapturableAreaRoster _areaRoster;
        [CanBeNull] private readonly ILogger<SapperActivityTask> _logger;

        public SapperActivityTask([NotNull] SapperTaskState taskState,
            [NotNull] SapperActivityConfig config, [NotNull] CapturableAreaRoster areaRoster,
            [CanBeNull] ILogger<SapperActivityTask> logger) : base(taskState, config)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskState, nameof(taskState));
            ThrowHelper.ThrowIfArgumentNullOrDefault(areaRoster, nameof(areaRoster));

            _taskState = taskState;
            _areaRoster = areaRoster;
            _logger = logger;

            InitActivityObjects();
        }

        protected override void OnActivated()
        {
            base.OnActivated();

            // Fallback на случай, если кто-то сделает рестарт сапёра.
            // По ГДД при рестарте прогресс на минах не сбрасывается,
            // если рестартанём задание со всеми захваченными минами, то упрёмся в софтлок.
            bool isAllCaptured = _areaRoster.All(a => a.IsCaptured);
            if (isAllCaptured)
            {
                _logger?.LogWarning("Activation requested for activity when all areas captured. Fallback and reset.");
                ResetActivity();
            }

            _areaRoster.ForEach(a =>
            {
                if (!a.IsCaptured)
                {
                    a.Captured.AddListener(OnAreaCaptured);
                    a.Enable();
                }
            });
        }

        protected override void OnDeactivated()
        {
            base.OnDeactivated();

            _areaRoster.ForEach(a =>
            {
                a.Captured.RemoveListener(OnAreaCaptured);
                a.Disable();
            });
        }

        private void CompleteActivity() => Complete();

        private void OnAreaCaptured(CapturableArea area)
        {
            area.Captured.RemoveListener(OnAreaCaptured);
            area.Disable();
            _taskState.CompletedAreasCount++;

            if (_taskState.CompletedAreasCount == _taskState.TotalAreasCount)
                CompleteActivity();
        }

        private void InitActivityObjects()
        {
            _taskState.TotalAreasCount = _areaRoster.Count;
            _areaRoster.ForEach(InitArea);
        }

        private void InitArea(CapturableArea area)
        {
            ICapturableAreaConfig config = _taskState.Config!.GetAreaConfigurationById(area.Id);
            area.Init(config);
        }

        private void ResetActivity()
        {
            _areaRoster.ForEach(a => a.ResetProgress());
            _taskState.CompletedAreasCount = 0;
        }
    }
}