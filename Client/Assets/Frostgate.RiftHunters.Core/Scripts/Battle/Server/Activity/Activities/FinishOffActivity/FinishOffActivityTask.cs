﻿using System.Linq;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.FinishOffActivity;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.FinishOffActivity
{
    public sealed class FinishOffActivityTask : ActivityTask<FinishOffActivityConfig>
    {
        [NotNull] private readonly FinishOffTaskState _taskState;
        [NotNull] private readonly IReadOnlyBotSpawnState _botSpawnState;

        public FinishOffActivityTask(
            [NotNull] FinishOffTaskSceneData taskSceneData,
            [NotNull] FinishOffActivityConfig config,
            [NotNull] IReadOnlyBotSpawnState botSpawnState) : base(
            taskSceneData.TaskState, config)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData, nameof(taskSceneData));
            _taskState = taskSceneData.TaskState;
            _botSpawnState = botSpawnState;
        }

        protected override void Dispose(bool dispose) =>
            _botSpawnState.DataUpdated.RemoveListener(OnDataUpdated);

        protected override void OnActivated()
        {
            _botSpawnState.DataUpdated.AddListener(OnDataUpdated);
            if (CanComplete()) Complete();

            base.OnActivated();
        }

        protected override void OnDeactivated()
        {
            _botSpawnState.DataUpdated.RemoveListener(OnDataUpdated);
            base.OnDeactivated();
        }

        private void OnDataUpdated(string spawnerId)
        {
            if (CanComplete())
                Complete();
        }

        private bool CanComplete() =>
            _taskState.Status != ActivityStatus.Completed &&
            _botSpawnState.Data.Values.All(v => v.Status != BotSpawnerStatus.Awaiting &&
                                                v.Status != BotSpawnerStatus.Spawning &&
                                                v.AliveBots == 0);
    }
}