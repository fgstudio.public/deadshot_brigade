﻿using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.KillUnitsActivity;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.KillUnitsActivity
{
    public sealed class KillUnitsActivityTask : ActivityTask<KillUnitsActivityConfig>
    {
        [NotNull] private readonly KillUnitsTaskState _taskState;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;

        public KillUnitsActivityTask(
            [NotNull] KillUnitsTaskSceneData taskSceneData,
            [NotNull] KillUnitsActivityConfig config,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection) : base(
            taskSceneData.TaskState, config)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskSceneData, nameof(taskSceneData));

            _unitCollection = unitCollection;
            _taskState = taskSceneData.TaskState;
        }

        protected override void OnActivated()
        {
            Subscribe(_unitCollection);
            base.OnActivated();
        }

        protected override void OnDeactivated()
        {
            Unsubscribe(_unitCollection);
            base.OnDeactivated();
        }

        protected override void Dispose(bool dispose)
        {
            Unsubscribe(_unitCollection);
            base.Dispose(dispose);
        }

        private void Subscribe(IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            unitCollection.Added += OnAdded;
            unitCollection.Removed += OnRemoved;
            unitCollection.ForEach(OnAdded);
        }

        private void Unsubscribe(IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            unitCollection.Added -= OnAdded;
            unitCollection.Removed -= OnRemoved;
            unitCollection.ForEach(OnRemoved);
        }

        private void OnAdded(UnitNetwork unit)
        {
            if (KillUnitsActivityHelper.IsUnitTarget(unit.UnitNetworkState, _taskState))
            {
                unit.UnitNetworkState.Died += OnUnitDied;
                unit.UnitNetworkState.UnitConfigChanged += OnUnitConfigChanged;
            }
        }

        private void OnRemoved(UnitNetwork unitNetwork)
        {
            unitNetwork.UnitNetworkState.Died -= OnUnitDied;
            unitNetwork.UnitNetworkState.UnitConfigChanged -= OnUnitConfigChanged;
        }

        private void OnUnitDied(UnitNetworkState state) =>
            HandleUnitConfigFinished(state.UnitConfig.Id);

        private void OnUnitConfigChanged(UnitConfig oldConfig, UnitConfig newConfig) =>
            HandleUnitConfigFinished(oldConfig.Id);

        private void HandleUnitConfigFinished(string unitId)
        {
            if (!_taskState.IsUnitTarget(unitId)) return;
            if (_taskState.IsUnitKilled(unitId)) return;

            _taskState.AddKilledUnit(unitId);

            if (_taskState.AreTargetUnitsKilled)
                Complete();
        }
    }
}