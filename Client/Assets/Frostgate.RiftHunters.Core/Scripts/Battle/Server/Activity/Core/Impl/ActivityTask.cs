﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using JetBrains.Annotations;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity
{
    public abstract class ActivityTask<TConfig> : IActivityTask<TConfig> where TConfig : ActivityTaskConfig
    {
        public IReadOnlyActivityTaskState<TConfig> State => (IReadOnlyActivityTaskState<TConfig>)_state;

        IReadOnlyActivityTaskState IActivityTask.State => (IReadOnlyActivityTaskState)_state;


        private readonly IActivityTaskState<TConfig> _state;

        private bool _isDisposed;

        protected ActivityTask([NotNull] IActivityTaskState<TConfig> taskState, [NotNull] TConfig config)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(taskState, nameof(taskState));
            ThrowHelper.ThrowIfArgumentNullOrDefault(config, nameof(config));

            _state = taskState;
            _state.Init(config);
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            Dispose(true);
            _isDisposed = true;
        }

        protected virtual void OnActivated()
        {
        }

        protected virtual void OnDeactivated()
        {
        }

        protected void Complete() => _state.SetCompleted();

        protected virtual void Dispose(bool dispose)
        {
        }

        void IActivityTask.Activate()
        {
            _state.SetActivated();
            OnActivated();
        }

        void IActivityTask.Deactivate()
        {
            _state.SetDeactivated();
            OnDeactivated();
        }

        void IActivityTask.ResetProgress()
        {
            _state.ResetProgress();
        }

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{GetType().Name} already disposed.");
        }
    }
}