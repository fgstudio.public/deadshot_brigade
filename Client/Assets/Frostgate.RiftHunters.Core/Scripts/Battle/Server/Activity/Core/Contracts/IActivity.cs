﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity
{
    public interface IActivity
    {
        [NotNull] IReadOnlyActivityState State { get; }

        void Activate();
        void Deactivate();
        void ResetProgress();
    }
}