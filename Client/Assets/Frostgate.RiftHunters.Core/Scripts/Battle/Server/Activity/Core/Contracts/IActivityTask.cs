﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity
{
    public interface IActivityTask : IDisposable
    {
        IReadOnlyActivityTaskState State { get; }

        void Activate();
        void Deactivate();
        void ResetProgress();
    }

    public interface IActivityTask<out TConfig> : IActivityTask
        where TConfig : ActivityTaskConfig
    {
        new IReadOnlyActivityTaskState<TConfig> State { get; }
    }
}