﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.ClearArenaActivity;
using Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.ClearArenaActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity;
using Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.CartEscortActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.MoveToPointActivity;
using Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.MoveToPointActivity;
using Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.KillUnitsActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.KillUnitsActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity;
using Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.SapperActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;
using Mirror;
using System;
using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.FinishOffActivity;
using Frostgate.RiftHunters.Core.Battle.Server.Activity.Activities.SurviveArenaActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.FinishOffActivity;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity
{
    public sealed class ActivityFactory : IActivityFactory
    {
        [NotNull] private readonly IDdaSystem _ddaSystem;
        [NotNull] private readonly IReadOnlyBotSpawnState _botSpawnState;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;

        public ActivityFactory(
            [NotNull] IDdaSystem ddaSystem,
            [NotNull] IReadOnlyBotSpawnState botSpawnState,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _ddaSystem = ddaSystem;
            _botSpawnState = botSpawnState;
            _unitCollection = unitCollection;
        }

        public IActivity Create(ActivityConfig activityConfig, ActivitySceneData activityData)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(activityConfig, nameof(activityConfig));
            ThrowHelper.ThrowIfArgumentNullOrDefault(activityData, nameof(activityData));

            IReadOnlyDictionary<Type, ActivityTaskConfig> taskConfigs = activityConfig.GetAllTasks()
                .ToDictionary(c => c.GetType());

            IReadOnlyDictionary<Type, ActivityTaskSceneData> taskDatas = activityData.TasksData.ToDictionary(d =>
            {
                Type stateType = d.State.GetType();
                return stateType.GetInterface(typeof(IActivityTaskState<>).Name).GetGenericArguments()[0];
            });

            IReadOnlyList<IActivityTask<ActivityTaskConfig>> tasks = CreateActivityTasks(taskConfigs, taskDatas);

            return new Activity(activityData.State, activityConfig, tasks);
        }

        private IReadOnlyList<IActivityTask<ActivityTaskConfig>> CreateActivityTasks(
            IReadOnlyDictionary<Type, ActivityTaskConfig> taskConfigs,
            IReadOnlyDictionary<Type, ActivityTaskSceneData> taskDatas)
        {
            var activityTasks = new List<IActivityTask<ActivityTaskConfig>>(taskConfigs.Count);
            foreach ((Type configType, ActivityTaskConfig config) in taskConfigs)
            {
                IActivityTask<ActivityTaskConfig> task = CreateActivityTask(config, taskDatas[configType]);
                activityTasks.Add(task);
            }

            return activityTasks;
        }

        private IActivityTask<ActivityTaskConfig> CreateActivityTask(ActivityTaskConfig config,
            ActivityTaskSceneData taskSceneData)
        {
            return config switch
            {
                ClearArenaActivityConfig clearArenaConfig => CreateClearArenaActivityTask(clearArenaConfig,
                    (ClearArenaTaskSceneData)taskSceneData),

                FinishOffActivityConfig finishOffConfig => CreateFinishOffActivityTask(finishOffConfig,
                    (FinishOffTaskSceneData)taskSceneData),

                KillUnitsActivityConfig killUnitsConfig => CreateKillUnitsActivityTask(killUnitsConfig,
                    (KillUnitsTaskSceneData)taskSceneData),

                SurviveArenaActivityConfig surviveConfig => CreateSurviveArenaActivityTask(surviveConfig,
                    (SurviveArenaTaskSceneData)taskSceneData),

                CartEscortActivityConfig cartEscortConfig => CreateCartEscortActivityTask(cartEscortConfig,
                    (CartEscortTaskSceneData)taskSceneData),

                SapperActivityConfig sapperActivityConfig => CreateSapperActivityTask(sapperActivityConfig,
                    (SapperTaskSceneData)taskSceneData),

                MoveToPointActivityConfig moveToPointConfig => CreateMoveToPointActivityTask(moveToPointConfig,
                    (MoveToPointTaskSceneData)taskSceneData),

                _ => throw new NotImplementedException($"Server handler for '{config.GetType().Name}' not implemented.")
            };
        }

        private IActivityTask<ActivityTaskConfig> CreateClearArenaActivityTask(ClearArenaActivityConfig config,
            ClearArenaTaskSceneData taskSceneData) =>
            new ClearArenaActivityTask(taskSceneData, config, _botSpawnState);

        private IActivityTask<ActivityTaskConfig> CreateFinishOffActivityTask(FinishOffActivityConfig config,
            FinishOffTaskSceneData taskSceneData) =>
            new FinishOffActivityTask(taskSceneData, config, _botSpawnState);

        private IActivityTask<ActivityTaskConfig> CreateKillUnitsActivityTask(KillUnitsActivityConfig config,
            KillUnitsTaskSceneData taskSceneData) =>
            new KillUnitsActivityTask(taskSceneData, config, _unitCollection);

        private IActivityTask<ActivityTaskConfig> CreateSurviveArenaActivityTask(SurviveArenaActivityConfig config,
            SurviveArenaTaskSceneData taskSceneData) =>
            new SurviveArenaActivityTask(taskSceneData, config, _botSpawnState);

        private IActivityTask<ActivityTaskConfig> CreateCartEscortActivityTask(CartEscortActivityConfig config,
            CartEscortTaskSceneData taskSceneData) => new CartEscortActivityTask(taskSceneData, config);

        private IActivityTask<ActivityTaskConfig> CreateSapperActivityTask(SapperActivityConfig sapperActivityConfig,
            SapperTaskSceneData taskSceneData) =>
            new SapperActivityTask(taskSceneData.TaskState, sapperActivityConfig, taskSceneData.AreaRoster,
                LoggerFactory.CreateLogger<SapperActivityTask>());

        private IActivityTask<ActivityTaskConfig> CreateMoveToPointActivityTask(MoveToPointActivityConfig config,
            MoveToPointTaskSceneData taskSceneData) => new MoveToPointActivityTask(taskSceneData, config);
    }
}