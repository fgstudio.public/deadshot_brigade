﻿using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity
{
    public sealed class Activity : IActivity
    {
        public IReadOnlyActivityState State => _state;
        private bool AreAllTasksCompleted => _tasks.Count == _completedTasks.Count;
        private ActivityConfig Config => _state.Config;


        [NotNull] private readonly ActivityState _state;
        [NotNull] private readonly IReadOnlyDictionary<ActivityTaskConfig, IActivityTask<ActivityTaskConfig>> _tasks;

        private readonly IDictionary<ActivityTaskConfig, IActivityTask<ActivityTaskConfig>> _activeTasks =
            new Dictionary<ActivityTaskConfig, IActivityTask<ActivityTaskConfig>>();

        private readonly IDictionary<ActivityTaskConfig, IActivityTask<ActivityTaskConfig>> _completedTasks =
            new Dictionary<ActivityTaskConfig, IActivityTask<ActivityTaskConfig>>();

        public Activity([NotNull] ActivityState state, [NotNull] ActivityConfig config,
            [NotNull] IReadOnlyList<IActivityTask<ActivityTaskConfig>> tasks)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(config, nameof(config));
            ThrowHelper.ThrowIfArgumentNullOrDefault(state, nameof(state));
            ThrowHelper.ThrowIfArgumentNullOrDefault(tasks, nameof(tasks));

            _state = state;
            _tasks = tasks.ToDictionary(t => t.State.Config);

            _state.Init(config);
        }

        public void Activate()
        {
            _state.SetActivated();
            Config.GetAllTasks().ForEach(c => Activate(_tasks[c]));
        }

        public void Deactivate()
        {
            _state.SetDeactivated();
            _tasks.Values.ForEach(Deactivate);
        }

        public void ResetProgress() => _tasks.Values.ForEach(ResetProgress);

        private void Activate(IActivityTask<ActivityTaskConfig> task)
        {
            ActivityTaskConfig config = task.State.Config;
            _activeTasks.Add(config, task);

            task.State.StatusChanged += OnTaskStatusChanged;
            task.Activate();
        }

        private void Deactivate(IActivityTask<ActivityTaskConfig> task)
        {
            ActivityTaskConfig config = task.State.Config;
            _activeTasks.Remove(config);
            _completedTasks.Remove(config);

            task.State.StatusChanged -= OnTaskStatusChanged;
            task.Deactivate();
        }

        private void ResetProgress(IActivityTask task) => task.ResetProgress();

        private void OnTaskStatusChanged(IReadOnlyActivityTaskState<ActivityTaskConfig> state, ActivityStatus _,
            ActivityStatus status)
        {
            if (status != ActivityStatus.Completed)
                return;

            ActivityTaskConfig taskConfig = state.Config;
            IActivityTask<ActivityTaskConfig> task = _activeTasks[taskConfig];

            _activeTasks.Remove(taskConfig);
            _completedTasks.Add(taskConfig, task);

            if (AreAllTasksCompleted)
                _state.SetCompleted();
        }
    }
}