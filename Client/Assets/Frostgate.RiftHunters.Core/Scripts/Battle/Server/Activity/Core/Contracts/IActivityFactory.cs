﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Server.Activity
{
    public interface IActivityFactory
    {
        IActivity Create([NotNull] ActivityConfig activityConfig, [NotNull] ActivitySceneData activityData);
    }
}