﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    [Serializable]
    public class ImpactAIConditions
    {
        [Tooltip("Доступен ли импакт для применения аишкой. А то мало ли, тут по задумке не только абилки могут быть.")]
        [SerializeField] private bool _aiEnabled;
        [Tooltip("Если эта галка снята, то абилка может применятся при отсутсвие целй. Это актуально для лечилок.")]
        [SerializeField] private bool _onlyAttackState;
        [Tooltip("Радиус в котором считается вес врагов")]
        [SerializeField] private float _enemyRadius = 8;
        [Tooltip("Лимит суммарного веса противников в радиусе. Если вес больше или равен лимиту то абилку стоит применять. " +
                 "Обычный юнит весит 1, боссы больше, чтобы на боссах всегда срабатывало.")]
        [SerializeField] private float _enemyWeightMinLimit = 1;
        [Tooltip("Радиус в котором мониторятся союзники")]
        [SerializeField] private float _allyRadius;
        [Tooltip("Лимит здоровья союзника, ниже которого абилку стоит применять.")]
        [SerializeField] private float _allyHealthMinLimit = 0.5f;

        public bool Check(UnitAIData data)
        {
            if (!_aiEnabled)
                return false;

            if (_onlyAttackState && data.AttackTarget == null)
                return false;

            if (_enemyRadius > 0)
                if (HasEnoughEnemiesWeight(data.Unit, data.VisibleUnits, _enemyWeightMinLimit))
                    return true;

            if (_allyRadius > 0)
                if (HasAllyToHeal(data.Unit, data.VisibleUnits, _allyHealthMinLimit))
                    return true;

            return false;
        }

        private bool HasEnoughEnemiesWeight(UnitNetwork impactingUnit,
            IEnumerable<UnitNetwork> visibleUnits, float minWeightThreshold)
        {
            float weight = 0f;
            float sqrEnemyRadius = Mathf.Pow(_enemyRadius, 2);

            foreach (UnitNetwork visibleUnit in visibleUnits)
            {
                if (visibleUnit.IsFriendly(impactingUnit))
                    continue;

                float sqrDist = Vector3.SqrMagnitude(visibleUnit.Transform.position - impactingUnit.Transform.position);
                if (sqrDist > sqrEnemyRadius)
                    continue;

                weight += visibleUnit.UnitNetworkState.UnitConfig.IsBoss ? 1000 : 1;
                if (weight >= minWeightThreshold)
                    return true;
            }

            return false;
        }

        private bool HasAllyToHeal(UnitNetwork impactingUnit,
            IEnumerable<UnitNetwork> visibleUnits, float minHealthRatioThreshold)
        {
            foreach (UnitNetwork visibleUnit in visibleUnits)
            {
                if (!visibleUnit.IsFriendly(impactingUnit))
                    continue;

                float healthRatio = visibleUnit.UnitNetworkState.Health / visibleUnit.UnitNetworkState.PropertiesProvider!.Health;
                if (healthRatio <= minHealthRatioThreshold)
                    return true;
            }

            return false;
        }
    }
}