using System;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public class UnitAI : IDisposable
    {
        private UnitAIData Data { get; }

        private UnitAIState _currentState;
        private UnitAIState _idleState;
        private UnitAIState _attackState;
        private UnitAIState _retreatState;

        public UnitAI(UnitNetwork unit, IDamageService damageService, UnitAIGroupVision unitAIGroupVision,
            IPositionProvider homePositionProvider)
        {
            Data = new UnitAIData(unit, damageService, unitAIGroupVision, homePositionProvider);

            switch (Data.Unit.UnitType)
            {
                case BattleUnitType.Player:
                    CreatePlayerStates();
                    break;
                case BattleUnitType.Bot:
                    CreateBotStates();
                    break;
            }

            Data.OnEvent += OnEvent;

            SwitchState(_idleState);
        }

        private void CreateBotStates()
        {
            _idleState = new IdleAIState(Data);

            if (Data.HasRangeWeapon)
            {
                _attackState = new RangeAttackAIState(Data);
                _retreatState = new RangeRetreatAIState(Data);
            }
            else
            {
                _attackState = new OnlyMeleeAttackAIState(Data);
                _retreatState = new OnlyMeleeRetreatAIState(Data);
            }
        }

        private void CreatePlayerStates()
        {
            _idleState = new EscortAllyAIState(Data);

            if (Data.HasRangeWeapon)
            {
                _attackState = new EscortAllyAndRangeAttackAIState(Data);
                _retreatState = new RangeRetreatAIState(Data);
            }
            else
            {
                _attackState = new OnlyMeleeAttackAIState(Data);
                _retreatState = new OnlyMeleeRetreatAIState(Data);
            }
        }

        public void Update(float deltaTime) =>
            _currentState.Update(deltaTime);

        private void OnEvent(UnitAIEvents unitAiEvent)
        {
            switch (unitAiEvent)
            {
                case UnitAIEvents.RetreatCompleted:
                    SwitchState(_attackState);
                    break;
                case UnitAIEvents.AttackTargetDetected:
                    SwitchState(_attackState);
                    break;
                case UnitAIEvents.AttackTargetLost:
                    SwitchState(_idleState);
                    break;
                case UnitAIEvents.NeedRetreat:
                    SwitchState(_retreatState);
                    break;
            }
        }

        private void SwitchState(UnitAIState state)
        {
            if (_currentState == state)
                return;

            _currentState?.OnExit();
            _currentState = state;
            _currentState?.OnEnter();
        }

        public void Dispose()
        {
            Data.OnEvent -= OnEvent;
            Data.Dispose();
        }
    }
}