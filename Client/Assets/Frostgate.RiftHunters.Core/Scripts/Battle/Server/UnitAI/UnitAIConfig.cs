using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    [CreateAssetMenu(fileName = nameof(UnitAIConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(UnitAIConfig))]
    public class UnitAIConfig : ScriptableObject
    {
        [field: SerializeField] public float VisionRadius { get; private set; }
        [field: SerializeField] public float VisibilityDuration { get; private set; } = 10;
        [field: SerializeField] public float CommunicationRadius { get; private set; }
        [field: SerializeField] public Vector2 DodgeForwardRange { get; private set; }
        [field: SerializeField] public float RetreatHealthLimit { get; private set; }
        [field: SerializeField] public float RetreatDuration { get; private set; } = 3;
        [field: SerializeField] public bool CanDodgeImpact { get; private set; }
    }
}