using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using UnityEngine;
using UnityEngine.AI;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public class UnitAIPathFinder
    {
        public Vector3 Direction
        {
            get
            {
                if (_isDirty)
                    OnChange();

#if UNITY_EDITOR
                DrawPath();
#endif

                return _direction;
            }
        }

        public Vector3 EndPoint
        {
            get
            {
                if (_isDirty)
                    OnChange();

                return _endPoint;
            }
        }

        private readonly List<Vector3> _calculatedPath = new();
        private readonly NavMeshPath _navMeshPath = new();
        private Vector3 _myPos;
        private Vector3 _targetPos;
        private Vector3 _direction;
        private Vector3 _endPoint;
        private float _nextUpdateTime;

        private const int AreaMask = 1;
        private const float LongDistUpdateInterval = 1;
        private const float ShortDistUpdateInterval = 0.2f;
        private const float ShortDistance = 3;
        private const float Accuracy = 0.1f;
        private bool _isDirty;

        public void Reset()
        {
            _nextUpdateTime = 0;
            _isDirty = true;
        }

        public void Update(Vector3 myPos, Vector3 targetPos)
        {
            if (_myPos == myPos && _targetPos == targetPos)
                return;

            _isDirty = true;
            _myPos = myPos;
            _targetPos = targetPos;
        }

        public void Update(Vector3 myPos)
        {
            if (_myPos == myPos)
                return;

            _isDirty = true;
            _myPos = myPos;
        }

        void OnChange()
        {
            if (Time.time > _nextUpdateTime && TryRecalculatePath())
            {
                var dist = Vector3.Distance(_myPos, _targetPos);
                _nextUpdateTime = Time.time + dist > ShortDistance ? LongDistUpdateInterval : ShortDistUpdateInterval;
            }

            MoveOnPath();
            RecalculateDirection();
            _isDirty = false;
        }

        private bool TryRecalculatePath()
        {
            const int magicNumber = 5;

            _calculatedPath.Clear();

            //TODO: надо реализовать другой метод для ближайшей точки. Чтобы ближайшая точка всегда была.
            if (!PositionHelper.TryCorrectSpawnPosition(_myPos, out var closestMyPos, magicNumber))
                return false;

            if (!PositionHelper.TryCorrectSpawnPosition(_targetPos, out var closestTargetPos, magicNumber))
                return false;

            AddPathPoint(closestMyPos);

            if (NavMesh.CalculatePath(closestMyPos, closestTargetPos, AreaMask, _navMeshPath))
                foreach (var corner in _navMeshPath.corners)
                    AddPathPoint(corner);

            AddPathPoint(closestTargetPos, true);

            _endPoint = closestTargetPos;
            return true;
        }

        private void AddPathPoint(Vector3 point, bool forceMode = false)
        {
            if (_calculatedPath.Count > 0)
            {
                var prevPointIndex = _calculatedPath.Count - 1;
                var prevPoint = _calculatedPath[prevPointIndex];
                var xDelta = Mathf.Abs(prevPoint.x - point.x);
                var zDelta = Mathf.Abs(prevPoint.z - point.z);

                if (zDelta < Accuracy && xDelta < Accuracy)
                {
                    if (forceMode)
                        _calculatedPath.RemoveAt(prevPointIndex);
                    else
                        return;
                }
            }

            _calculatedPath.Add(point);
        }

        private void MoveOnPath()
        {
            if (_calculatedPath.Count <= 1)
                return;

            //Работает норм, но есть подозрение что надо детектить момент когда мы перескакиваем первую точку маршрута а не просто приблизились к ней.
            var nextPoint = _calculatedPath[0];
            var xDelta = nextPoint.x - _myPos.x;
            var zDelta = nextPoint.z - _myPos.z;
            if (Mathf.Abs(xDelta) < Accuracy && Mathf.Abs(zDelta) < Accuracy)
                _calculatedPath.RemoveAt(0);
        }

        private void RecalculateDirection()
        {
            if (_calculatedPath.Count <= 0)
            {
                _direction = Vector3.zero;
                return;
            }

            var nextPoint = _calculatedPath[0];
            var xDelta = nextPoint.x - _myPos.x;
            var zDelta = nextPoint.z - _myPos.z;

            _direction = new Vector3(xDelta, 0, zDelta).normalized;
        }

#if UNITY_EDITOR
        private void DrawPath()
        {
            if (_calculatedPath.Count < 1)
                return;

            var offset = Vector3.up * 0.1f;
            UnityEngine.Debug.DrawLine(_myPos + offset, _calculatedPath[0] + offset, Color.green);

            for (var i = 1; i < _calculatedPath.Count; i++)
            {
                var prevPoint = _calculatedPath[i - 1];
                var point = _calculatedPath[i];

                UnityEngine.Debug.DrawLine(prevPoint + offset, point + offset, Color.green);
            }

            UnityEngine.Debug.DrawLine(_myPos + offset, _myPos + _direction + offset, Color.cyan);
        }
#endif
    }
}