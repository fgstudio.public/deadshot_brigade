using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public enum UnitAIEvents
    {
        AttackTargetDetected,
        AttackTargetLost,
        NeedRetreat,
        RetreatCompleted,
    }

    public class UnitAIData : IDisposable
    {
        public event Action<UnitAIEvents> OnEvent;

        public IEnumerable<UnitNetwork> VisibleUnits => _unitAIGroupVision.GetVisibleUnits(Unit);
        public UnitNetwork Unit { get; }
        public IReadOnlyUnitPropertiesProvider Properties => Unit.UnitNetworkState.PropertiesProvider;
        public UnitAIConfig AIConfig => Unit.UnitNetworkState.UnitConfig.UnitAIConfig;
        public UnitAIPathFinder Path { get; }
        [CanBeNull] public UnitNetwork AttackTarget { get; private set; }
        public float DistToTarget { get; set; }
        public Vector3 VectorToTarget { get; set; }
        public Vector3 AttackTargetPos { get; set; }
        public Vector3 MyPos { get; set; }
        public Vector3 HomePosition => _homePositionProvider.Position;
        public Vector3 RetreatPoint { get; private set; }
        public float LastDamageTime { get; private set; }
        public float NextRecalculateMoveTime { get; set; }
        public bool HasDodge { get; }
        public bool HasRangeWeapon { get; }

        private readonly IDamageService _damageService;
        private readonly UnitAIGroupVision _unitAIGroupVision;
        private readonly IPositionProvider _homePositionProvider;

        private float _nextBypassCalculateTime;
        private float _bypassMoveEndTime;
        private Vector3 _bypassMoveDir;
        private float _nextTimeRecalculateAttackTarget;
        private float _dodgeImpactNextTime;
        private UnitNetwork _escortTarget;

        private static readonly int[] bypassAngles = { -30, 30 };
        private static float BypassCalculateInterval => Random.Range(1, 2f);
        private static float RecalculateAttackTargetInterval => Random.Range(0.5f, 1.0f);
        public bool CanRecalculateAttackTarget => Time.time > _nextTimeRecalculateAttackTarget;

        public UnitAIData(UnitNetwork unit, IDamageService damageService,
            UnitAIGroupVision unitAIGroupVision, IPositionProvider homePositionProvider)
        {
            Unit = unit;
            HasDodge = unit.UnitNetworkState.UnitConfig.UnitActionRoster.TryGetAction(UnitActionTypes.Dodge, out _);
            HasRangeWeapon = Unit.RangeWeapon;

            Path = new UnitAIPathFinder();
            _damageService = damageService;
            _unitAIGroupVision = unitAIGroupVision;
            _homePositionProvider = homePositionProvider;

            _damageService.Damaged.AddListener(OnDamaged);
        }

        public void Dispose()
        {
            _damageService.Damaged.RemoveListener(OnDamaged);
        }

        public void SetAttackTarget(UnitNetwork attackTarget)
        {
            _nextTimeRecalculateAttackTarget = Time.time + RecalculateAttackTargetInterval;

            if (AttackTarget == attackTarget)
                return;

            var prevTarget = AttackTarget;
            AttackTarget = attackTarget;

            if (prevTarget == null)
                ExecuteEvent(UnitAIEvents.AttackTargetDetected);

            if (AttackTarget == null)
                ExecuteEvent(UnitAIEvents.AttackTargetLost);
        }

        private void OnDamaged(DamageEventData data)
        {
            if (data.ReceiverNetId == Unit.Identity.netId)
                LastDamageTime = Time.time;
        }

        public void SetRetreatPoint(Vector3 retreatPoint)
        {
            RetreatPoint = retreatPoint;
            ExecuteEvent(UnitAIEvents.NeedRetreat);
        }

        public void SetRetreatCompleted() =>
            ExecuteEvent(UnitAIEvents.RetreatCompleted);

        // Стараемся (но не сильно), обойти впереди бегущего союзника.
        public Vector3 BypassAllyDir(Vector3 targetMoveDir)
        {
            if (DistToTarget < 2)
                return targetMoveDir;

            if (_bypassMoveEndTime > Time.time)
                return _bypassMoveDir;

            if (_nextBypassCalculateTime > Time.time)
                return targetMoveDir;

            _nextBypassCalculateTime = Time.time + BypassCalculateInterval;

            foreach (var visibleUnit in VisibleUnits)
            {
                if (!visibleUnit.IsFriendly(Unit))
                    continue;

                var vector = visibleUnit.transform.position - Unit.transform.position;
                if (vector.sqrMagnitude > 9)
                    continue;

                if (Vector3.Angle(vector, targetMoveDir) > 20)
                    continue;

                _bypassMoveDir = targetMoveDir.RotateY(bypassAngles.GetRandom());
                _bypassMoveEndTime = Time.time + Random.Range(0.5f, 1);
                return _bypassMoveDir;
            }

            return targetMoveDir;
        }

        public bool CheckCanDodgeImpact()
        {
            if (!AIConfig.CanDodgeImpact)
                return false;

            if (_dodgeImpactNextTime > Time.time)
                return false;

            _dodgeImpactNextTime = Time.time + 1;
            return true;
        }

        public UnitNetwork GetEscortTarget()
        {
            if (_escortTarget != null)
                return _escortTarget;

            foreach (var visibleUnit in VisibleUnits)
            {
                if (!visibleUnit.IsFriendly(Unit) || visibleUnit.ControlledByAI)
                    continue;

                _escortTarget = visibleUnit;
                break;
            }

            return _escortTarget;
        }

        public void ExecuteEvent(UnitAIEvents unitAiEvent) => OnEvent?.Invoke(unitAiEvent);
    }
}