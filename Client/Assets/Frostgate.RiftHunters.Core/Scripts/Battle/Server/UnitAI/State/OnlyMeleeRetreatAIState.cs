namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public class OnlyMeleeRetreatAIState : UnitAIState
    {
        public OnlyMeleeRetreatAIState(UnitAIData data) : base(data)
        {
        }

        public override void OnEnter() => Data.Path.Reset();

        public override void Update(float deltaTime)
        {
            UnitAIHelper.TryExecuteBaseAttackLogic(Data);

            UnitAIHelper.RetreatMove(Data);

            if (UnitAIHelper.CheckRetreatCompleted(Data))
                Data.SetRetreatCompleted();

            UnitAIHelper.TryMeleeAttack(Data);
        }
    }
}