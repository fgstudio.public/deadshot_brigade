namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public class OnlyMeleeAttackAIState : UnitAIState
    {
        public OnlyMeleeAttackAIState(UnitAIData data) : base(data)
        {
        }

        public override void OnEnter() => Data.Path.Reset();

        public override void Update(float deltaTime)
        {
            if (!UnitAIHelper.TryExecuteBaseAttackLogic(Data))
            {
                Data.ExecuteEvent(UnitAIEvents.AttackTargetLost);
                return;
            }

            if (UnitAIHelper.TryBeginRetreatLogic(Data))
                return;

            UnitAIHelper.DamagedDodgeMeleeLogic(Data);

            if (UnitAIHelper.TryMeleeAttack(Data))
            {
                UnitAIHelper.MoveInMeleeAttack(Data);
            }
            else
            {
                UnitAIHelper.ForwardDodgeLogic(Data);
                UnitAIHelper.MoveToAttackTarget(Data);
            }
        }
    }
}