using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public class EscortAllyAIState : UnitAIState
    {
        private Vector3 _offset;
        private float _offsetRecalculateTime;
        private bool _escortMoveInProcess;

        public EscortAllyAIState(UnitAIData data) : base(data)
        {
        }

        public override void OnEnter()
        {
            // Не понял почему, но на серваке тоже юнит инициализируется не сразу. И тут кинет ошибку.
            if (Data.Unit.UnitNetworkState.IsInitialized)
                Data.Unit.UnitNetworkInput.Move = Vector2.zero;

            var myPos = Data.Unit.Transform.position;
            Data.Path.Reset();
            Data.Path.Update(myPos, myPos);
        }

        public override void Update(float deltaTime)
        {
            UnitAIHelper.RecalculateAttackTarget(Data);
            UnitAIHelper.ExecuteImpactLogic(Data);

            var target = Data.GetEscortTarget();

            if (target == null)
            {
                // TODO: тут можно было бы бежать к ближайшему выходу или к следующим противникам
                Data.Unit.UnitNetworkInput.Move = Vector2.zero;
                return;
            }

            Data.MyPos = Data.Unit.Transform.position;
            var dist = Vector3.Distance(Data.MyPos, target.Transform.position);

            const float escortRadius = 4;
            const float stopDist = 0.5f;

            if (dist > escortRadius)
            {
                _escortMoveInProcess = true;
                var pos = GetEscortPos(target);
                Data.Path.Update(Data.MyPos, pos);
                Data.Unit.UnitNetworkInput.YRotation = Vector3.SignedAngle(Vector3.forward, Data.Path.Direction, Vector3.up);

                Data.Unit.UnitNetworkInput.Move = Vector2.up;
            }

            if (_escortMoveInProcess)
            {
                var pos = GetEscortPos(target);
                Data.Path.Update(Data.MyPos, pos);
                Data.Unit.UnitNetworkInput.YRotation = Vector3.SignedAngle(Vector3.forward, Data.Path.Direction, Vector3.up);

                Data.Unit.UnitNetworkInput.Move = Vector2.up;

                if (Vector3.Distance(pos, Data.MyPos) < stopDist)
                {
                    Data.Unit.UnitNetworkInput.YRotation = target.UnitNetworkInput.YRotation;
                    _escortMoveInProcess = false;
                }
            }
            else
            {
                Data.Unit.UnitNetworkInput.Move = Vector2.zero;
            }
        }

        private Vector3 GetEscortPos(UnitNetwork escortTarget)
        {
            if (escortTarget == null)
                return Data.MyPos;

            if (_offsetRecalculateTime < Time.time)
            {
                _offsetRecalculateTime = Time.time + Random.Range(3f, 6f);
                _offset = new Vector3(Random.Range(-3f, 3f), 0, 1f);
                _offset = escortTarget.Transform.rotation * _offset;
            }

            return escortTarget.Transform.position + _offset;
        }
    }
}