using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public class IdleAIState : UnitAIState
    {
        private float _nextRecalculateHomePointTime;

        private static int RecalculateHomePointOnEnterInterval => Random.Range(2, 3);
        private static int RecalculateHomePointInUpdateInterval => Random.Range(5, 10);

        public IdleAIState(UnitAIData data) : base(data)
        {
        }

        public override void OnEnter()
        {
            // Не понял почему, но на серваке тоже юнит инициализируется не сразу. И тут кинет ошибку.
            if (Data.Unit.UnitNetworkState.IsInitialized)
                Data.Unit.UnitNetworkInput.Move = Vector2.zero;

            var isFirstIdleEnter = _nextRecalculateHomePointTime <= 0;

            // На первом вхождении в стейт бездействия, сразу бежим в домашнюю позицию. На последующих с небольшой задержкой.
            if (!isFirstIdleEnter)
                _nextRecalculateHomePointTime = Time.time + RecalculateHomePointOnEnterInterval;

            var myPos = Data.Unit.Transform.position;
            Data.Path.Reset();
            Data.Path.Update(myPos, myPos);
        }

        public override void Update(float deltaTime)
        {
            UnitAIHelper.RecalculateAttackTarget(Data);

            var myPos = Data.Unit.Transform.position;
            var distToTargetPoint = Vector3.Distance(myPos, Data.Path.EndPoint);

            const float stopDist = 0.5f;
            if (distToTargetPoint > stopDist)
            {
                Data.Path.Update(myPos);
                Data.Unit.UnitNetworkInput.YRotation = Vector3.SignedAngle(Vector3.forward, Data.Path.Direction, Vector3.up);

                Data.Unit.UnitNetworkInput.Move = Vector2.up;
            }
            else
            {
                Data.Unit.UnitNetworkInput.Move = Vector2.zero;
            }

            if (Time.time > _nextRecalculateHomePointTime)
            {
                const float randomRadius = 5;
                var randomOffset = new Vector3(Random.Range(-randomRadius, randomRadius), 0, Random.Range(-randomRadius, randomRadius));
                var randomPos = Data.HomePosition + randomOffset;

                if (PositionHelper.TryCorrectSpawnPosition(randomPos, out var moveTargetPosition, randomRadius))
                {
                    Data.Path.Reset();
                    Data.Path.Update(myPos, moveTargetPosition);
                }

                _nextRecalculateHomePointTime = Time.time + RecalculateHomePointInUpdateInterval;
            }
        }
    }
}