namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public class RangeRetreatAIState : UnitAIState
    {
        public RangeRetreatAIState(UnitAIData data) : base(data)
        {
        }

        public override void OnEnter() => Data.Path.Reset();
        public override void OnExit() => Data.Unit.UnitNetworkInput.SetShooting(false);

        public override void Update(float deltaTime)
        {
            UnitAIHelper.TryExecuteBaseAttackLogic(Data);

            UnitAIHelper.RetreatMove(Data);

            if (UnitAIHelper.CheckRetreatCompleted(Data))
                Data.SetRetreatCompleted();

            if (!UnitAIHelper.TryRangeAttack(Data))
                UnitAIHelper.TryMeleeAttack(Data);
        }
    }
}