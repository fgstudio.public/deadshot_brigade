namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public class RangeAttackAIState : UnitAIState
    {
        private float _nextRecalculateMoveTime;

        public RangeAttackAIState(UnitAIData data) : base(data)
        {
        }

        public override void OnEnter() => Data.Path.Reset();
        public override void OnExit() => Data.Unit.UnitNetworkInput.SetShooting(false);

        public override void Update(float deltaTime)
        {
            if (!UnitAIHelper.TryExecuteBaseAttackLogic(Data))
            {
                Data.ExecuteEvent(UnitAIEvents.AttackTargetLost);
                return;
            }

            if (UnitAIHelper.TryBeginRetreatLogic(Data))
                return;
            
            if (Data.AIConfig.CanDodgeImpact)
                UnitAIHelper.ImpactDodgeLogic(Data);
            
            UnitAIHelper.DamagedDodgeRangeLogic(Data);

            if (Data.Unit.UnitNetworkState.PatronsCount <= 0)
                Data.Unit.UnitNetworkInput.ExecuteReloading();

            if (UnitAIHelper.TryRangeAttack(Data) || UnitAIHelper.TryMeleeAttack(Data))
                UnitAIHelper.MoveInRangeAttack(Data);
            else
                UnitAIHelper.MoveToAttackTarget(Data);
        }
    }
}