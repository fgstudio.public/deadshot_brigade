namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public abstract class UnitAIState
    {
        protected UnitAIData Data { get; }

        protected UnitAIState(UnitAIData data) =>
            Data = data;

        public abstract void Update(float deltaTime);
        public virtual void OnEnter() {}
        public virtual void OnExit() {}
    }
}