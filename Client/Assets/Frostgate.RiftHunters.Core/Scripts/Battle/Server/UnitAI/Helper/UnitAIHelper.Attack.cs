using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public static partial class UnitAIHelper
    {
        public static bool TryMeleeAttack(UnitAIData data)
        {
            if (data.Unit.MeleeWeapon == null)
                return false;

            if (data.Unit.MeleeWeapon.MaxDistance > data.DistToTarget &&
                ShootingHelper.CheckMeleeAttackAngle(data.Unit, data.AttackTarget) &&
                !ShootingHelper.IsShooterInsideTarget(data.Unit, data.AttackTarget))
            {
                data.Unit.UnitNetworkInput.ExecuteMeleeAttack();
                return true;
            }

            return false;
        }

        public static bool TryRangeAttack(UnitAIData data)
        {
            if (data.Unit.RangeWeapon == null)
                return false;

            bool canAttack = CanRangeShootByDistance(data) &&
                             CanShootXZ(data) &&
                             CanRangeRaycastToTarget(data);

            if (canAttack)
            {
                data.Unit.UnitNetworkInput.SetShooting(true);
                data.Unit.UnitNetworkInput.SetAimDirection(data.VectorToTarget);
                return true;
            }
            else
            {
                data.Unit.UnitNetworkInput.SetShooting(false);
                return false;
            }
        }

        private static bool CanRangeShootByDistance(UnitAIData data) =>
            data.Unit.RangeWeapon!.MaxDistance >= data.DistToTarget;

        private static bool CanShootXZ(UnitAIData data)
        {
            if (data.AttackTarget == null)
                return false;

            Vector3 shootPointWithOffset = ShootingHelper.GetShootingProjectionStartPoint(data.Unit);
            Vector3 vectorToUnitWithOffset = data.AttackTarget.FocusPoint - shootPointWithOffset;

            return ShootingHelper.CanShootXZ(data.Unit, vectorToUnitWithOffset);
        }

        private static bool CanRangeRaycastToTarget(UnitAIData data)
        {
            var dist = data.DistToTarget;
            var origin = data.Unit.ShootPoint;
            var dir = data.VectorToTarget.normalized;

            if (Physics.Raycast(origin, dir, dist, PhysicsLayers.WallMask))
                return false;

            return true;
        }
    }
}