using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public static partial class UnitAIHelper
    {
        public static void ImpactDodgeLogic(UnitAIData data)
        {
            if (!data.HasDodge)
                return;
            
            if (!data.CheckCanDodgeImpact())
                return;

            if (!data.AttackTarget.ImpactState.IsCastingAny)
                return;

            var rotation = impactDodgeAngles.GetRandom();
            data.Unit.TryExecuteUnitAction(UnitActionTypes.Dodge, data.Unit.UnitNetworkInput.YRotation + rotation);
        }
        
        public static void DamagedDodgeRangeLogic(UnitAIData data) => DamagedDodgeLogic(data, damagedDodgeRangeAngles);
        public static void DamagedDodgeMeleeLogic(UnitAIData data) => DamagedDodgeLogic(data, damagedDodgeMeleeAngles);

        private static void DamagedDodgeLogic(UnitAIData data, int[] damagedDodgeAngles)
        {
            if (!data.HasDodge)
                return;

            const float damagedDodgeExecuteInterval = 1f;
            if (data.LastDamageTime + damagedDodgeExecuteInterval < Time.time)
                return;

            var rotation = damagedDodgeAngles.GetRandom();
            data.Unit.TryExecuteUnitAction(UnitActionTypes.Dodge, data.Unit.UnitNetworkInput.YRotation + rotation);
        }

        public static void ForwardDodgeLogic(UnitAIData data)
        {
            if (!data.HasDodge)
                return;

            if (data.DistToTarget > data.AIConfig.DodgeForwardRange.y)
                return;

            if (data.DistToTarget < data.AIConfig.DodgeForwardRange.x)
                return;

            data.Unit.TryExecuteUnitAction(UnitActionTypes.Dodge, data.Unit.UnitNetworkInput.YRotation);
        }
    }
}