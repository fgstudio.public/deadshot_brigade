using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public static partial class UnitAIHelper
    {
        private static readonly RaycastHit[] raycastHits = new RaycastHit[20];
        private static readonly int[] impactDodgeAngles = {0, -45, 45, 90, 90, 135, -135, 180};
        private static readonly int[] damagedDodgeRangeAngles = {-90, 90, 180};
        private static readonly int[] damagedDodgeMeleeAngles = {-45, 45};
        private static readonly List<Vector3> positionsCache = new();
    }
}