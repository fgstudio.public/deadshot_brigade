using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public static partial class UnitAIHelper
    {
        public static void ExecuteImpactLogic(UnitAIData data)
        {
            if (data.Unit.ImpactState.IsCastingAny)
                return;

            ImpactModelRoster impactRoster = data.Unit.ImpactRoster;
            UnitNetworkInput networkInput = data.Unit.UnitNetworkInput;

            if (CanExecuteImpact(impactRoster.Ultimate, data))
                networkInput.ExecuteUltimate();

            else if (CanExecuteImpact(impactRoster.Ability, data))
                networkInput.ExecuteAbility();

            else if (CanExecuteImpact(impactRoster.ExtraAbility, data))
                networkInput.ExecuteExtraAbility();
        }

        private static bool CanExecuteImpact(ImpactModel impactModel, UnitAIData data) =>
            impactModel.CanExecute && impactModel.Data.Config!.AIConditions.Check(data);
    }
}