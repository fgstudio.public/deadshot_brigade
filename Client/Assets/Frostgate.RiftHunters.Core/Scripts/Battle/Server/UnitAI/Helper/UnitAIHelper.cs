using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public static partial class UnitAIHelper
    {
        public static bool TryExecuteBaseAttackLogic(UnitAIData data)
        {
            RecalculateAttackTarget(data);

            if (data.AttackTarget == null)
                return false;

            TurnToAttackTarget(data);
            ExecuteImpactLogic(data);

            return true;
        }

        public static void RecalculateAttackTarget(UnitAIData data)
        {
            if (data.CanRecalculateAttackTarget)
            {
                UnitNetwork attackTarget = null;

                if (data.Unit.UnitNetworkEffects.Params.AgroTarget != null)
                {
                    attackTarget = data.Unit.UnitNetworkEffects.Params.AgroTarget;
                }
                else
                {
                    var closestTargetDist = float.MaxValue;

                    foreach (var unit in data.VisibleUnits)
                    {
                        if (unit.IsDestroyedOrDead)
                            continue;

                        if (ShootingHelper.IsFriendly(data.Unit, unit))
                            continue;

                        var dist = Vector3.Distance(data.Unit.FocusPoint, unit.FocusPoint);
                        if (dist >= closestTargetDist)
                            continue;

                        attackTarget = unit;
                        closestTargetDist = dist;
                    }
                }

                data.SetAttackTarget(attackTarget);
            }

            if (data.AttackTarget != null)
            {
                data.VectorToTarget = data.AttackTarget.FocusPoint - data.Unit.ShootPoint;
                data.AttackTargetPos = data.AttackTarget.transform.position;
                data.MyPos = data.Unit.Transform.position;
                data.DistToTarget = data.VectorToTarget.magnitude;
            }
        }
    }
}