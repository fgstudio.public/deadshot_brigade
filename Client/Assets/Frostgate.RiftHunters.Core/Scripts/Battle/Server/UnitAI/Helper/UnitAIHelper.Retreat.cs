using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public static partial class UnitAIHelper
    {
        public static bool TryBeginRetreatLogic(UnitAIData data)
        {
            if (CheckNeedRetreat(data))
                if (TryCalculateRetreatPoint(data, out var retreatPoint))
                {
                    data.SetRetreatPoint(retreatPoint);
                    return true;
                }

            return false;
        }

        public static bool CheckNeedRetreat(UnitAIData data)
        {
            if (data.Unit.UnitNetworkState.BehaviourState == BehaviourState.Reload)
                return true;

            if (data.LastDamageTime + 5 < Time.time)
                return false;

            if (data.Unit.UnitNetworkState.Health / data.Properties.Health > data.AIConfig.RetreatHealthLimit)
                return false;

            return true;
        }

        public static bool CheckRetreatCompleted(UnitAIData data)
        {
            if (data.Unit.UnitNetworkState.BehaviourState == BehaviourState.Reload)
                return false;

            if (data.Unit.UnitNetworkState.Health / data.Properties.Health > data.AIConfig.RetreatHealthLimit)
                return true;

            if (data.LastDamageTime + data.AIConfig.RetreatDuration > Time.time)
                return false;

            return true;
        }

        public static bool TryCalculateRetreatPoint(UnitAIData data, out Vector3 retreatPoint)
        {
            retreatPoint = default;
            if (data.AttackTarget == null)
                return false;

            const float retreatMaxDist = 15;
            const float retreatPointSearchRadius = 20;
            const int variantsCount = 3;
            const int angleStep = 45;
            const int startAngle = -45;

            positionsCache.Clear();

            // Берём несколько точек с разными отклонениями от противника
            for (var i = 0; i < variantsCount; i++)
            {
                var rotation = Quaternion.Euler(0, startAngle + i * angleStep, 0);
                var retreatVector = rotation * -data.VectorToTarget;
                var searchCenter = data.MyPos + retreatVector.normalized * retreatMaxDist;

                if (!PositionHelper.TryCorrectSpawnPosition(searchCenter, out var point, retreatPointSearchRadius))
                    continue;

                if (!PositionHelper.IsPointReachable(point, data.Unit.Transform.position))
                    continue;

                positionsCache.Add(point);
            }

            if (positionsCache.Count == 0)
                return false;

            var farthestDist = 0f;

            // Выбираем точку которая проходибельна и дальше других
            foreach (var point in positionsCache)
            {
                #if UNITY_EDITOR
                UnityEngine.Debug.DrawLine(data.MyPos, point, Color.cyan, 3);
                #endif

                var dist = Vector3.Distance(data.AttackTargetPos, point);
                if (dist < farthestDist)
                    continue;

                farthestDist = dist;
                retreatPoint = point;
            }

            return true;
        }
    }
}