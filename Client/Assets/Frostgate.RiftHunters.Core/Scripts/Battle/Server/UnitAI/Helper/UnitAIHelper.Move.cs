using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public static partial class UnitAIHelper
    {
        private static float RecalculateMoveInterval => Random.Range(1f, 2f);

        public static void TurnToAttackTarget(UnitAIData data)
        {
            var vectorToTarget = data.AttackTargetPos - data.MyPos;
            var yRotation = Vector3.SignedAngle(Vector3.forward, vectorToTarget, Vector3.up);
            data.Unit.UnitNetworkInput.YRotation = yRotation;
        }

        public static void MoveToTarget(UnitAIData data, Transform target)
        {
            const float stopDist = 2f;

            var dist = Vector3.Distance(data.MyPos, target.position);

            if (dist <= stopDist)
                return;

            data.Path.Update(data.MyPos, target.position);
            var moveDirection = data.Path.Direction;

            var inverseTargetRotation = Quaternion.Euler(0, -data.Unit.UnitNetworkInput.YRotation, 0);
            moveDirection = inverseTargetRotation * moveDirection;
            data.Unit.UnitNetworkInput.Move = new Vector2(moveDirection.x, moveDirection.z).normalized;
        }

        public static void MoveToAttackTarget(UnitAIData data)
        {
            data.Path.Update(data.MyPos, data.AttackTargetPos);

            var moveDirection = data.Path.Direction;

            moveDirection = data.BypassAllyDir(moveDirection);

            var inverseTargetRotation = Quaternion.Euler(0, -data.Unit.UnitNetworkInput.YRotation, 0);
            moveDirection = inverseTargetRotation * moveDirection;
            data.Unit.UnitNetworkInput.Move = new Vector2(moveDirection.x, moveDirection.z).normalized;
        }

        public static void MoveInRangeAttack(UnitAIData data)
        {
            var minDist = (data.Unit.RangeWeapon?.MaxDistance ?? 1) * 0.8f;
            var prefDist = (data.Unit.RangeWeapon?.MaxDistance ?? 1) * 0.9f;

            if (data.DistToTarget < minDist)
            {
                data.Unit.UnitNetworkInput.Move = Vector2.down;
                return;
            }

            if (data.DistToTarget > prefDist)
            {
                data.Unit.UnitNetworkInput.Move = Vector2.up;
                return;
            }

            if (data.NextRecalculateMoveTime > Time.time)
                return;

            data.NextRecalculateMoveTime = Time.time + RecalculateMoveInterval;

            if (Random.value > 0.5f)
                data.Unit.UnitNetworkInput.Move = Vector2.right;
            else
                data.Unit.UnitNetworkInput.Move = Vector2.left;
        }

        public static void MoveInMeleeAttack(UnitAIData data)
        {
            var minDist = data.Unit.MeleeWeapon?.MaxDistance ?? default;
            data.Unit.UnitNetworkInput.Move =
                data.DistToTarget <= minDist ? Vector2.zero : Vector2.up;
        }

        public static void RetreatMove(UnitAIData data)
        {
            data.Path.Update(data.MyPos, data.RetreatPoint);

            var moveDirection = data.Path.Direction;

            var dodgeAngle = Vector3.SignedAngle(Vector3.forward, moveDirection, Vector3.up);
            data.Unit.TryExecuteUnitAction(UnitActionTypes.Dodge, dodgeAngle);

            var inverseTargetRotation = Quaternion.Euler(0, -data.Unit.UnitNetworkInput.YRotation, 0);
            moveDirection = inverseTargetRotation * moveDirection;
            data.Unit.UnitNetworkInput.Move = new Vector2(moveDirection.x, moveDirection.z).normalized;
        }
    }
}