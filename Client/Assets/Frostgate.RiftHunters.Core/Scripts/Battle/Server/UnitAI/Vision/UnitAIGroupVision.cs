﻿using System;
using System.Collections;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Ai;
using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public class UnitAIGroupVision : IDisposable
    {
        private readonly Queue<Dictionary<UnitNetwork, float>> _pool = new();
        private readonly Dictionary<UnitNetwork, Dictionary<UnitNetwork, float>> _visibleUnits = new();
        private readonly List<UnitNetwork> _temporaryList = new();
        private readonly IEnumerator _updateEnumerator;
        private readonly IReadOnlyObjectCollection<Collider, UnitNetwork> _unitColliderCollection;
        private readonly IReadOnlyObjectCollection<uint, IAimTarget> _aimTargetsIdCollection;
        private float _nextRecalculateTime;
        private bool _visibleUnitsIsDirty;
        private readonly IDamageService _damageService;

        private const float RecalculateInterval = 0.25f;

        public UnitAIGroupVision(IReadOnlyObjectCollection<Collider, UnitNetwork> unitColliderCollection,
            IReadOnlyObjectCollection<uint, IAimTarget> aimTargetsIdCollection,
            IDamageService damageService)
        {
            _updateEnumerator = UpdateVision();
            _unitColliderCollection = unitColliderCollection;
            _aimTargetsIdCollection = aimTargetsIdCollection;
            _damageService = damageService;
            _damageService.Blocked.AddListener(OnBlocked);
            _damageService.Damaged.AddListener(OnDamaged);
            _damageService.Died.AddListener(OnDied);
        }

        public void Dispose()
        {
            _damageService.Blocked.RemoveListener(OnBlocked);
            _damageService.Damaged.RemoveListener(OnDamaged);
            _damageService.Died.RemoveListener(OnDied);
        }

        public IEnumerable<UnitNetwork> GetVisibleUnits([NotNull] UnitNetwork unit)
        {
            if (!_visibleUnits.ContainsKey(unit))
                AddUnit(unit);

            foreach (UnitNetwork visibleUnit in _visibleUnits[unit].Keys)
                if (visibleUnit != null)
                    yield return visibleUnit;

            if (_nextRecalculateTime < Time.time)
            {
                _updateEnumerator.MoveNext();
                _nextRecalculateTime = Time.time + RecalculateInterval;
            }
        }

        private IEnumerator UpdateVision()
        {
            var unitEnumerator = _visibleUnits.Keys.GetEnumerator();

            while (true)
            {
                AddGlobalVision();
                yield return null;

                RecalculateVisible();
                yield return null;

                // Если добавлялись или убавлялись юниты, то надо переполучить энумератор.
                if (_visibleUnitsIsDirty)
                {
                    unitEnumerator = _visibleUnits.Keys.GetEnumerator();
                    _visibleUnitsIsDirty = false;
                }
                // Добавление зрения друзей довольно затратно. Ограничиваю кол-во обрабатываемых юнитов ха один тик.
                const int maxFriendIterations = 10;
                for (var i = 0; i < maxFriendIterations; i++)
                {
                    var result = unitEnumerator.MoveNext();
                    AddFriendsVision(unitEnumerator.Current);
                    if (!result)
                        break;
                }
                yield return null;

                CleanDisposedUnits();
                yield return null;
            }
        }

        private void OnDied(DiedEventData data) => AddVisibilityByDamage(data.KillerId, data.DiedId);
        private void OnDamaged(DamageEventData data) => AddVisibilityByDamage(data.CasterNetId, data.ReceiverNetId);
        private void OnBlocked(BlockEventData data) => AddVisibilityByDamage(data.CasterId, data.ReceiverId);

        private void AddVisibilityByDamage(uint casterId, uint receiverId)
        {
            if (!_aimTargetsIdCollection.TryGet(casterId, out IAimTarget caster)) return;
            if (!_aimTargetsIdCollection.TryGet(receiverId, out IAimTarget receiver)) return;
            if (caster is not IAiTarget casterTarget) return;
            if (receiver is not IAiTarget receiverTarget) return;

            AddVisibilityByDamage(casterTarget, receiverTarget);
        }

        private void AddVisibilityByDamage([NotNull] IAiTarget caster, [NotNull] IAiTarget receiver)
        {
            if (caster.AimTarget == null || receiver.AimTarget == null)
                return;

            if (!_unitColliderCollection.TryGet(caster.AimTarget, out UnitNetwork aiTargetUnit))
                return;

            if (!_unitColliderCollection.TryGet(receiver.AimTarget, out UnitNetwork damageReceiverUnit))
                return;

            if (!_visibleUnits.ContainsKey(damageReceiverUnit))
                return;

            float duration = damageReceiverUnit.UnitNetworkState.UnitConfig.UnitAIConfig.VisibilityDuration;
            _visibleUnits[damageReceiverUnit][aiTargetUnit] = duration + Time.time;
        }

        private void CleanByTime()
        {
            foreach (UnitNetwork unit in _visibleUnits.Keys)
                CleanVisibleCollectionByTime(_visibleUnits[unit]);
        }

        private void RecalculateVisible()
        {
            foreach (UnitNetwork unit in _visibleUnits.Keys)
            {
                if (unit == null || unit.IsDestroyedOrDead)
                    continue;

                Vector3 pos = unit.FocusPoint;
                float radius = unit.UnitNetworkState.UnitConfig.UnitAIConfig.VisionRadius;
                float duration = unit.UnitNetworkState.UnitConfig.UnitAIConfig.VisibilityDuration;
                float visibilityCleanTime = duration + Time.time;
                int count = Physics.SphereCastNonAlloc(pos, radius, Vector3.up,
                    PhysicHelper.HitResults, 0f, PhysicsLayers.AimTargetMask);

                _temporaryList.Clear();

                for (int i = 0; i < count; i++)
                {
                    RaycastHit rh = PhysicHelper.HitResults[i];
                    if (!_unitColliderCollection.TryGet(rh.collider, out UnitNetwork visibleUnit))
                        continue;
                    if (visibleUnit == unit)
                        continue;

                    _temporaryList.Add(visibleUnit);
                }

                foreach (UnitNetwork visibleUnit in _temporaryList)
                {
                    Vector3 vecToUnit = visibleUnit.FocusPoint - unit.FocusPoint;
                    bool inMyGroup = _visibleUnits.ContainsKey(visibleUnit);

                    if (!inMyGroup)
                    {
                        // Стены блочат зрение
                        bool wallBetweenUs = Physics.Raycast(pos, vecToUnit,
                            vecToUnit.magnitude, PhysicsLayers.WallMask);

                        if (wallBetweenUs)
                            continue;
                    }

                    _visibleUnits[unit][visibleUnit] = visibilityCleanTime;
                }
            }
        }

        private void AddFriendsVision(UnitNetwork unit)
        {
            if (unit == null || unit.IsDestroyedOrDead)
                return;

            var unitVisionCollection = _visibleUnits[unit];

            _temporaryList.Clear();

            // Выбираем из списка видимых юнитов только юнитов-друзей с которыми возможна связь
            foreach (KeyValuePair<UnitNetwork, float> visibleUnit in unitVisionCollection)
            {
                bool inMyGroup = _visibleUnits.ContainsKey(visibleUnit.Key);
                if (!inMyGroup)
                    continue;

                Vector3 myPos = unit.Transform.position;
                Vector3 friendPos = visibleUnit.Key.Transform.position;
                float communicationRadius = unit.UnitNetworkState.UnitConfig.UnitAIConfig.CommunicationRadius;

                if (Vector3.Distance(myPos, friendPos) > communicationRadius)
                    continue;

                _temporaryList.Add(visibleUnit.Key);
            }

            foreach (UnitNetwork visibleFriend in _temporaryList)
            {
                // Юнит видит не только тех кто находится в радиусе зрения, но и тех кого видят его друзья находящиеся в радиусе связи
                foreach (KeyValuePair<UnitNetwork, float> visibleByFriendUnit in _visibleUnits[visibleFriend])
                {
                    if (visibleByFriendUnit.Key == unit)
                        continue;

                    if (!unitVisionCollection.ContainsKey(visibleByFriendUnit.Key))
                        unitVisionCollection.Add(visibleByFriendUnit.Key, visibleByFriendUnit.Value);
                }
            }
        }

        private void AddGlobalVision()
        {
            _temporaryList.Clear();

            foreach (AIGlobalVisionArea area in AIGlobalVisionTrigger.AllGlobalVisionAreas)
            {
                int count = Physics.SphereCastNonAlloc(area.Position, area.Radius,
                    Vector3.up, PhysicHelper.HitResults, 0f, PhysicsLayers.AimTargetMask);

                for (int i = 0; i < count; i++)
                {
                    RaycastHit rh = PhysicHelper.HitResults[i];
                    if (!_unitColliderCollection.TryGet(rh.collider, out UnitNetwork visibleUnit))
                        continue;

                    if (_visibleUnits.ContainsKey(visibleUnit))
                        continue;

                    _temporaryList.Add(visibleUnit);
                }
            }

            foreach (UnitNetwork unit in _visibleUnits.Keys)
            {
                if (unit == null) continue;

                float duration = unit.UnitNetworkState.UnitConfig.UnitAIConfig.VisibilityDuration;
                float visibilityCleanTime = duration + Time.time;

                foreach (UnitNetwork visibleUnit in _temporaryList)
                {
                    if (unit == visibleUnit)
                        continue;

                    if (_visibleUnits[unit].ContainsKey(visibleUnit))
                        _visibleUnits[unit][visibleUnit] = visibilityCleanTime;
                    else
                        _visibleUnits[unit].Add(visibleUnit, visibilityCleanTime);
                }
            }
        }

        private void CleanVisibleCollectionByTime(Dictionary<UnitNetwork, float> visibleUnits)
        {
            _temporaryList.Clear();

            foreach (KeyValuePair<UnitNetwork, float> visibleUnit in visibleUnits)
            {
                if (visibleUnit.Value < Time.time)
                    _temporaryList.Add(visibleUnit.Key);
            }

            foreach (var unit in _temporaryList)
                visibleUnits.Remove(unit);
        }

        private void CleanDisposedUnits()
        {
            _temporaryList.Clear();

            foreach (UnitNetwork unit in _visibleUnits.Keys)
                if (unit == null || unit.IsDestroyedOrDead)
                    _temporaryList.Add(unit);

            foreach (UnitNetwork unit in _temporaryList)
                RemoveUnit(unit);
        }

        private void ReturnCollectionToPool(Dictionary<UnitNetwork, float> collection) =>
            _pool.Enqueue(collection);

        private Dictionary<UnitNetwork, float> TakeCollectionFromPool()
        {
            if (_pool.Count == 0)
                return new Dictionary<UnitNetwork, float>();

            Dictionary<UnitNetwork, float> collection = _pool.Dequeue();
            collection.Clear();
            return collection;
        }

        private void AddUnit([NotNull] UnitNetwork unit)
        {
            unit.Destroyed.AddListener(() => RemoveUnit(unit));
            _visibleUnits.Add(unit, TakeCollectionFromPool());
            _visibleUnitsIsDirty = true;
        }

        private void RemoveUnit([CanBeNull] UnitNetwork unit)
        {
            if (_visibleUnits.TryGetValue(unit!, out Dictionary<UnitNetwork, float> collection))
            {
                ReturnCollectionToPool(collection);
                _visibleUnits.Remove(unit);
                _visibleUnitsIsDirty = true;
            }
        }
    }
}