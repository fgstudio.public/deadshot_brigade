using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.UnitAI
{
    public class AIGlobalVisionArea
    {
        public Vector3 Position { get; }
        public float Radius { get; }

        public AIGlobalVisionArea(Vector3 position, float radius)
        {
            Position = position;
            Radius = radius;
        }
    }

    public class AIGlobalVisionTrigger : MonoBehaviour
    {
        [SerializeField] private float _radius;

        public static IEnumerable<AIGlobalVisionArea> AllGlobalVisionAreas => allGlobalVisionAreas;
        private static readonly List<AIGlobalVisionArea> allGlobalVisionAreas = new();

        private AIGlobalVisionArea _area;

        private void Start()
        {
            _area = new AIGlobalVisionArea(transform.position, _radius);
            allGlobalVisionAreas.Add(_area);
        }

        private void OnDestroy() =>
            allGlobalVisionAreas.Remove(_area);

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = new Color(0, 1, 1, 0.3f);
            Gizmos.DrawSphere(transform.position, _radius);
        }
#endif
    }
}