using System;
using System.Threading;
using Mirror;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;
using Frostgate.RiftHunters.Core.Battle.Shared.StreakBonus;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.Objects.StreakBonus
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [AddComponentMenu(StreakBonusComponentMenu.Server + "/" + nameof(StreakBonusServerModule))]
    public sealed class StreakBonusServerModule : MonoBehaviour
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private ObjectCatcher _catcher;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private StreakBonusBlocker _blocker;

        [CanBeNull] private IEditableStreakBonusState _state;

        private IBonusService _bonusService;
        private StreakBonusConfig _config;

        public void Init(
            [NotNull] IEditableStreakBonusState state,
            [NotNull] StreakBonusConfig config,
            [NotNull] IBonusService bonusService,
            [NotNull] StreakBonusMechanicSettings mechanicSettings)
        {
            _state = state;
            _config = config;
            _bonusService = bonusService;

            TimeSpan dropDuration = mechanicSettings.DropDuration;
            _blocker.BlockFor(dropDuration);

            TimeSpan lifeTime = mechanicSettings.LifeTime;
            CancellationToken token = this.GetCancellationTokenOnDestroy();
            UniTask.Delay(lifeTime, cancellationToken: token)
                .ContinueWith(_state.SetExpired).SuppressCancellationThrow();
        }

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _catcher ??= GetComponentInChildren<ObjectCatcher>();
            _blocker ??= GetComponentInChildren<StreakBonusBlocker>();
        }

        private void OnEnable()
        {
            _catcher.Caught.AddListener(OnCaught);
            _catcher.Objects.ForEach(OnCaught);
        }

        private void OnDisable() =>
            _catcher.Caught.RemoveListener(OnCaught);

        private void OnCaught(Collider caughtObj)
        {
            if (_state!.Status != StreakBonusStatus.Active) return;
            if (!caughtObj.TryGetComponentInHierarchy(out NetworkIdentity identity)) return;

            _state.SetCollected(identity.netId);
            _bonusService.ApplyBonus(_config, identity);
        }
    }
}