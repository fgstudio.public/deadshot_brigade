using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Battle.Server.Objects.StreakBonus
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [AddComponentMenu(StreakBonusComponentMenu.Server + "/" + nameof(StreakBonusBlocker))]
    public sealed class StreakBonusBlocker : MonoBehaviour
    {
        [Inject] private readonly ILogger<StreakBonusBlocker> _logger;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private ObjectCatcher _catcher;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _catcher ??= GetComponentInChildren<ObjectCatcher>();

        public UniTask BlockFor(TimeSpan duration)
        {
            Block();

            _logger.Log($"Wait {duration.TotalSeconds} sec");
            CancellationToken token = this.GetCancellationTokenOnDestroy();
            return UniTask.Delay(duration, cancellationToken: token)
                .ContinueWith(Unblock);
        }

        [Button]
        public void Block()
        {
            _logger.Log("Block");
            _catcher.gameObject.SetActive(false);
            _catcher.LoseAllObjects();
        }

        [Button]
        public void Unblock()
        {
            _logger.Log("Unblock");
            _catcher.gameObject.SetActive(true);
        }
    }
}
