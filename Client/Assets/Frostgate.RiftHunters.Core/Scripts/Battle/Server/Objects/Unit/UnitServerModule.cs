using UnityEngine;
using Frostgate.RiftHunters.Core.Components;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.Objects.Unit
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Server.Menu + "/" + nameof(UnitServerModule))]
    public sealed class UnitServerModule : ObjectModule<UnitNetwork>
    { }
}