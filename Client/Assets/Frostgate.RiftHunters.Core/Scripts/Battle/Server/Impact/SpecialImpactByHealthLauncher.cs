using Mirror;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Components;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Server.Impacts.Menu + "/" + nameof(SpecialImpactByHealthLauncher))]
    public sealed class SpecialImpactByHealthLauncher : ObjectModuleComponent<UnitNetwork>
    {
        private const int DefaultLevel = -1;
        private const int FullPercentage = 100;

        [Range(0, FullPercentage), SuffixLabel("%", true)]
        public int HealthDecreasingPercentToLaunch;

        public bool AutoLaunchOnStart;

        [CanBeNull] private UnitNetwork _unit;
        private float[] _orderedLaunchHealthLevels;
        private int _lastLaunchedLevel = DefaultLevel;

        private readonly ILogger _logger = LoggerFactory.CreateLogger<SpecialImpactByHealthLauncher>();

        [Server]
        public override void Init(UnitNetwork unit)
        {
            _unit = unit;
            _lastLaunchedLevel = DefaultLevel;

            float initialHealth = unit.UnitNetworkState.Health;
            _orderedLaunchHealthLevels = GenerateOrderedHealthLevels(initialHealth);

            _logger.Log($"{ExtractName(_unit)} is initialized with health levels: " +
                        $"{string.Join(", ", _orderedLaunchHealthLevels)}");

            unit.UnitNetworkState.HealthChanged += OnHealthChanged;
            OnHealthChanged(default);
        }

        [Server]
        public override void Deinit()
        {
            _lastLaunchedLevel = DefaultLevel;
            _orderedLaunchHealthLevels = null;

            _logger.Log($"{ExtractName(_unit)} is deinitialized");

            if (_unit?.UnitNetworkState != null)
            {
                _unit.UnitNetworkState.HealthChanged -= OnHealthChanged;
                _unit = null;
            }
        }

        private float[] GenerateOrderedHealthLevels(float initialHealth)
        {
            float healthDecreasingRatioToLaunch = (float)HealthDecreasingPercentToLaunch / FullPercentage;
            float healthDecreasingPortion = initialHealth * healthDecreasingRatioToLaunch;
            bool ignoreInitialHealthAsLevel = !AutoLaunchOnStart;

            int healthLevelsCount = FullPercentage / HealthDecreasingPercentToLaunch;
            if (ignoreInitialHealthAsLevel) healthLevelsCount--;

            var orderedLaunchHealthLevels = new float[healthLevelsCount];
            orderedLaunchHealthLevels[0] = ignoreInitialHealthAsLevel
                ? initialHealth - healthDecreasingPortion : initialHealth;

            for (int i = 1; i < healthLevelsCount; i++)
            {
                float prevHealthLevel = orderedLaunchHealthLevels[i - 1];
                orderedLaunchHealthLevels[i] = prevHealthLevel - healthDecreasingPortion;
            }

            return orderedLaunchHealthLevels;
        }

        private void OnHealthChanged(float diff)
        {
            if (_unit == null) return;

            float currentHealth = _unit.UnitNetworkState.Health;
            int currentHealthLevel = CalcCurrentHealthLevel(currentHealth);

            if (currentHealthLevel != _lastLaunchedLevel)
            {
                _logger.Log($"{ExtractName(_unit)}'s special is launched " +
                            $"on {currentHealthLevel}nd health level");

                (_unit.ImpactRoster.Special as IServerImpactModel).Execute();
                _lastLaunchedLevel = currentHealthLevel;
            }
        }

        private int CalcCurrentHealthLevel(float currentHealth)
        {
            int level = DefaultLevel;

            for (int i = 0; i < _orderedLaunchHealthLevels.Length; i++)
            {
                if (_orderedLaunchHealthLevels[i] > currentHealth)
                    level = i;
                else
                    break;
            }

            return level;
        }

        private string ExtractName([CanBeNull] UnitNetwork unit) =>
            unit?.UnitNetworkState?.UnitConfig?.name ?? "";
    }
}