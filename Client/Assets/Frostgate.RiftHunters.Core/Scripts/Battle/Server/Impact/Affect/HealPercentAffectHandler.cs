using System.Threading;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared.Healing;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect
{
    // TODO: для этого есть StatHandler
    public class HealPercentAffectHandler : IAffectHandler<IEffectAffectHealPercentData>
    {
        private readonly IHealingService _healingService;

        public HealPercentAffectHandler([NotNull] IHealingService healingService) =>
            _healingService = healingService;

        UniTask IAffectHandler.HandleAsync(EffectAffectData data, IImpactSource source, IImpactTarget target, AffectHandlerAdditionalData additionalData, CancellationToken cancellationToken) =>
            HandleAsync(data, source, target, additionalData, cancellationToken);


        public UniTask HandleAsync(IEffectAffectHealPercentData data, IImpactSource source, IImpactTarget target, AffectHandlerAdditionalData additionalData, CancellationToken cancellationToken)
        {
            // при продолжительной обработке объекты могут быть уничтожены (дисконнект и т.д.)
            if (source != null && target != null)
            {
                if (target is IHealingReceiver healingReceiver)
                {
                    var healingCaster = (IHealingCaster) source;
                    var health = healingReceiver.State.MaxHealth * data.HealPercent;
                    var dto = new HealingSourceDTO(health, HealingType.Ability);
                    _healingService.Heal(dto, healingCaster, healingReceiver);
                }
            }

            return UniTask.CompletedTask;
        }
    }
}