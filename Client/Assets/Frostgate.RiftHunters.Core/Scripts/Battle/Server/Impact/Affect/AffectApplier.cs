using System;
using System.Threading;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using static Frostgate.RiftHunters.Core.Battle.Impact.Effect.EffectAffectData;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect
{
    public sealed class AffectApplier
    {
        [NotNull] private readonly IReadOnlyDictionary<AffectType, IAffectHandler> _affectHandlers;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _units;
        // TODO: враппер над коллекцией юнитов для накопления только игроков

        public AffectApplier([NotNull] IReadOnlyDictionary<AffectType, IAffectHandler> affectHandlers,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> units)
        {
            _affectHandlers = affectHandlers;
            _units = units;
        }

        public UniTask ApplyAsync([NotNull] IEnumerable<EffectAffectData> affectData,
            [NotNull] IImpactSource source, [NotNull] IImpactTarget target,
            CancellationToken cancellationToken)
        {
            IEnumerable<UniTask> tasks = affectData
                .Select(data => ApplyAsync(data, source, target, cancellationToken));

            return UniTask.WhenAll(tasks);
        }

        public async UniTask ApplyAsync([NotNull] EffectAffectData affectData,
            [CanBeNull] IImpactSource source, [CanBeNull] IImpactTarget target,
            CancellationToken cancellationToken)
        {
            int repeats = CalcRepeatsCount(affectData.RepeatsPerTarget);
            IAffectHandler handler = _affectHandlers[affectData.Type];

            for (int i = 0; i < repeats; i++)
            {
                if (cancellationToken.IsCancellationRequested) return;
                if (source == null || target == null) return;
                // объекты могут быть удалены за время await

                var addData = new AffectHandlerAdditionalData(repeats, i);
                await handler.HandleAsync(affectData, source, target, addData, cancellationToken);
            }
        }

        private int CalcRepeatsCount([NotNull] CountData countData)
        {
            int count = countData.Type switch
            {
                CountData.CountType.Exact => countData.ExactCount,
                // TODO: На нагруженных локациях может аффектить производительность,
                // размер ObjectCollection может превышать 100 объектов во время итерации.
                CountData.CountType.ByPlayers => GetPlayersCount(),
                _ => throw new ArgumentOutOfRangeException(nameof(CountData.CountType))
            };

            if (countData.UseLimits)
                count = Mathf.Clamp(count, countData.Limits.x, countData.Limits.y);

            return count;
        }

        private int GetPlayersCount()
        {
            int counter = 0;
            foreach (UnitNetwork unit in _units.Values)
            {
                if (unit.UnitType == BattleUnitType.Player)
                    counter++;
            }

            return counter;
        }
    }
}