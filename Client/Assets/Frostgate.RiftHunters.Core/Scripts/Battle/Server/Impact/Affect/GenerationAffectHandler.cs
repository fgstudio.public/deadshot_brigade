using System;
using System.Linq;
using System.Threading;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect
{
    public sealed class GenerationAffectHandler : IAffectHandler<IEffectAffectGenerationData>
    {
        [NotNull] private readonly GenerationPositionCalculator _positionCalculator;
        [NotNull] private readonly IReadOnlyObjectCommandSenderRepository _commandSenderRepository;

        public GenerationAffectHandler(
            [NotNull] AreaRandom areaRandom,
            [NotNull] IReadOnlyObjectCommandSenderRepository commandSenderRepository)
        {
            _positionCalculator = new GenerationPositionCalculator(areaRandom);
            _commandSenderRepository = commandSenderRepository;
        }

        UniTask IAffectHandler.HandleAsync(EffectAffectData data, IImpactSource source,
            IImpactTarget target, AffectHandlerAdditionalData additionalData, CancellationToken cancellationToken) =>
            HandleAsync(data, source, target, additionalData, cancellationToken);

        public UniTask HandleAsync(IEffectAffectGenerationData affectData, IImpactSource source,
            IImpactTarget target, AffectHandlerAdditionalData additionalData, CancellationToken cancellationToken)
        {
            bool success = TrySendInstantiateCommandForType<ITrap>(affectData, target, additionalData) ||
                           TrySendInstantiateCommandForType<ITotem>(affectData, target, additionalData) ||
                           TrySendInstantiateCommandForType<IMine>(affectData, target, additionalData) ||
                           TrySendInstantiateCommandForType<IWaveSpawner>(affectData, target, additionalData) ||
                           TrySendInstantiateCommandForType<ILinkSpawner>(affectData, target, additionalData) ||
                           TrySendInstantiateCommandForType<IShield>(affectData, target, additionalData);

            if (!success)
                throw new NotSupportedException(nameof(affectData.Prefab));

            return UniTask.CompletedTask;
        }

        private bool TrySendInstantiateCommandForType<TType>(
            [NotNull] IEffectAffectGenerationData affectData, [NotNull] IImpactTarget target, AffectHandlerAdditionalData additionalData)
            where TType : class, ISpawnable
        {
            if (!affectData.AllowedTypes.Contains(typeof(TType))) return false;
            if (!affectData.Prefab.TryGetComponent(out TType component)) return false;

            Vector3 position = _positionCalculator.Calc(affectData.PrefabOffsetData, target.Position,
                additionalData.CurrentRepeatNumber, additionalData.TotalRepeatsNumber, component.Type);
            _commandSenderRepository.Get<TType>().SendInstantiateForUnit(component, target.Identity, position);

            return true;
        }
    }
}