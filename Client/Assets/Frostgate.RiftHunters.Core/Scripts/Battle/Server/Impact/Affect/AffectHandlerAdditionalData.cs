﻿namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect
{
    public readonly struct AffectHandlerAdditionalData
    {
        public int TotalRepeatsNumber { get; }
        public int CurrentRepeatNumber { get; }

        public AffectHandlerAdditionalData(int totalRepeatsNumber, int currentRepeatNumber)
        {
            TotalRepeatsNumber = totalRepeatsNumber;
            CurrentRepeatNumber = currentRepeatNumber;
        }
    }
}