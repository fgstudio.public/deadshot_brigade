using System;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared;
using static Frostgate.RiftHunters.Core.Battle.Impact.Effect.PointOffsetData;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect
{
    public sealed class GenerationPositionCalculator
    {
        [NotNull] private readonly AreaRandom _areaRandom;

        public GenerationPositionCalculator([NotNull] AreaRandom areaRandom) =>
            _areaRandom = areaRandom;

        public Vector3 Calc([NotNull] PointOffsetData offsetData, Vector3 targetPosition, int currentRepeatNumber, 
            int totalRepeatsNumber, EntityType entityType)
        {
            float radius = offsetData.Radius;

            return offsetData.Type switch
            {
                OffsetType.None => targetPosition,

                OffsetType.RandomInRadius =>
                    GenerateSpawnPosition(targetPosition, radius, entityType, _areaRandom.SphereY),

                OffsetType.RandomOnRadius =>
                    GenerateSpawnPosition(targetPosition, radius, entityType, _areaRandom.CircleBorderY),

                OffsetType.EvenlyOnRadius =>
                    GenerateSpawnPosition(targetPosition, radius, entityType, (c, r) =>
                        Area.CircleBorderY(c, r, currentRepeatNumber, totalRepeatsNumber)),

                _ => throw new ArgumentOutOfRangeException(nameof(OffsetType))
            };
        }

        private Vector3 GenerateSpawnPosition(
            Vector3 targetPosition, float targetRadius, EntityType entityType,
            [NotNull] Func<Vector3, float, Vector3> randomPosition)
        {
            bool isPositionReady;
            Vector3 position;

            do
            {
                // пробуем сгенерировать валидную позицию для заданного радиуса
                float radius = targetRadius;
                isPositionReady = TryRandomValidPosition(
                    validatePosition: p => PositionHelper.IsValidToSpawn(p, entityType: entityType) &&
                                           PositionHelper.IsPointReachable(p, targetPosition),
                    randomPosition: () => randomPosition(targetPosition, radius),
                    out position);

                // если не получилось, то уменьшаем радиус
                if (isPositionReady) break;
                else targetRadius *= 0.8f;

            } while (targetRadius > 1);

            // на совсем крайний случай сдвигаем в ближайшую доступную точку
            if (!isPositionReady)
                PositionHelper.TryCorrectSpawnPosition(position, out position);

            return position;
        }

        private bool TryRandomValidPosition(
            [NotNull] Func<Vector3, bool> validatePosition,
            [NotNull] Func<Vector3> randomPosition, out Vector3 position)
        {
            int counter = 0;
            bool isPositionValid;

            do
            {
                counter++;
                position = randomPosition();
                isPositionValid = validatePosition(position);

            } while (counter < 5 && !isPositionValid);

            return isPositionValid;
        }
    }
}
