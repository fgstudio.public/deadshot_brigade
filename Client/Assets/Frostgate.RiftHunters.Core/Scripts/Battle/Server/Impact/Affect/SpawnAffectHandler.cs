﻿using System.Threading;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect
{
    public sealed class SpawnAffectHandler : IAffectHandler<IEffectAffectSpawnData>
    {
        [NotNull] private readonly IReadOnlyObjectCommandSenderRepository _commandSenderRepository;

        public SpawnAffectHandler([NotNull] IReadOnlyObjectCommandSenderRepository commandSenderRepository) =>
            _commandSenderRepository = commandSenderRepository;

        UniTask IAffectHandler.HandleAsync(EffectAffectData data, IImpactSource source,
            IImpactTarget target, AffectHandlerAdditionalData additionalData, CancellationToken cancellationToken) =>
            HandleAsync(data, source, target, additionalData, cancellationToken);

        public UniTask HandleAsync(IEffectAffectSpawnData data, IImpactSource source,
            IImpactTarget target, AffectHandlerAdditionalData _, CancellationToken cancellationToken)
        {
            if (source.Identity == null) return UniTask.CompletedTask;
            if (target.Identity == null) return UniTask.CompletedTask;
            if (data.SpawnerConfig == null) return UniTask.CompletedTask;

            DynamicPositionProvider positionProvider = new(target.Identity.transform);
            _commandSenderRepository.Get<BotSpawnerConfig>()
                .SendSpawnOnNearestSpawner(data.SpawnerConfig, positionProvider);

            return UniTask.CompletedTask;
        }
    }
}