using Mirror;
using System.Threading;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect
{
    public sealed class BuffAffectHandler : IAffectHandler<IEffectAffectBuffData>
    {
        [NotNull] private readonly UnitEffectStateFactory _effectStateFactory;

        public BuffAffectHandler([NotNull] UnitEffectStateFactory effectStateFactory) =>
            _effectStateFactory = effectStateFactory;

        UniTask IAffectHandler.HandleAsync(EffectAffectData data, IImpactSource source,
            IImpactTarget target, AffectHandlerAdditionalData additionalData, CancellationToken cancellationToken) =>
            HandleAsync(data, source, target, additionalData, cancellationToken);

        public UniTask HandleAsync(IEffectAffectBuffData data, IImpactSource source,
            IImpactTarget target, AffectHandlerAdditionalData _, CancellationToken cancellationToken)
        {
            if (source.Identity == null) return UniTask.CompletedTask;
            if (target is not IUnitEffectReceiver receiver) return UniTask.CompletedTask;

            foreach (UnitEffectConfig effect in data.Buffs)
                if (receiver.CanReceiveUnitEffect(effect))
                    ApplyEffect(effect, source.Identity, receiver);

            return UniTask.CompletedTask;
        }

        private void ApplyEffect([NotNull] UnitEffectConfig effect,
            [NotNull] NetworkIdentity source, [NotNull] IUnitEffectReceiver target)
        {
            UnitEffectState effectState = _effectStateFactory.Create(effect, source);
            target.AddUnitEffect(effectState);
        }
    }
}