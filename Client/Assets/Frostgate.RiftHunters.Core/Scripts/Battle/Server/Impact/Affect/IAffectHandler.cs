using System.Threading;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect
{
    public interface IAffectHandler
    {
        UniTask HandleAsync([NotNull] EffectAffectData data, [NotNull] IImpactSource source,
            [NotNull] IImpactTarget target, AffectHandlerAdditionalData additionalData,
            CancellationToken cancellationToken);
    }

    public interface IAffectHandler<in TData> : IAffectHandler
        where TData : ITypeEffectAffectData
    {
        UniTask HandleAsync([NotNull] TData data, [NotNull] IImpactSource source,
            [NotNull] IImpactTarget target, AffectHandlerAdditionalData additionalData,
            CancellationToken cancellationToken);
    }
}