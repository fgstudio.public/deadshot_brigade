using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using System.Threading;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect
{
    public sealed class DetonationAffectHandler : IAffectHandler<IDetonationEffectAffectData>
    {
        [NotNull] private readonly UnitEffectStateFactory _effectStateFactory;
        [NotNull] private readonly IDamageService _damageService;

        public DetonationAffectHandler([NotNull] UnitEffectStateFactory effectStateFactory,
            [NotNull] IDamageService damageService)
        {
            _effectStateFactory = effectStateFactory;
            _damageService = damageService;
        }

        UniTask IAffectHandler.HandleAsync(EffectAffectData data, IImpactSource source,
            IImpactTarget target, AffectHandlerAdditionalData additionalData, CancellationToken cancellationToken) =>
            HandleAsync(data, source, target, additionalData, cancellationToken);

        public UniTask HandleAsync(IDetonationEffectAffectData data, IImpactSource source,
            IImpactTarget target, AffectHandlerAdditionalData _, CancellationToken cancellationToken)
        {
            float damage = 0;
            int damageMulticast = 0;

            if (target is IUnitEffectReceiver effectReceiver)
                foreach (UnitEffectReplacementData replacementData in data.UnitEffectReplacements)
                    if (TryReplaceUnitEffect(source, effectReceiver, replacementData))
                    {
                        damage += replacementData.Damage;
                        damageMulticast++;
                    }

            if (damage > 0 && source is IDamageCaster damageCaster && target is IDamageReceiver damageReceiver)
                TakeDamage(damage, damageMulticast, damageCaster, damageReceiver);

            return UniTask.CompletedTask;
        }

        private bool TryReplaceUnitEffect(
            [NotNull] IImpactSource source,
            [NotNull] IUnitEffectReceiver effectReceiver,
            [NotNull] UnitEffectReplacementData replacementData)
        {
            UnitEffectConfig replaceableEffect = replacementData.ReplaceableEffect;
            if (!effectReceiver.TryGetUnitEffect(replaceableEffect, out UnitEffectState state))
                return false;

            effectReceiver.RemoveUnitEffect(state);

            NetworkIdentity sourceIdentity = source.Identity;
            UnitEffectConfig replacingEffect = replacementData.ReplacingEffect;

            if (replacingEffect != null && sourceIdentity != null)
            {
                UnitEffectState targetState = _effectStateFactory.Create(replacingEffect, sourceIdentity);
                effectReceiver.AddUnitEffect(targetState);
            }

            return true;
        }

        private void TakeDamage(float damage, int damageMulticast,
            [NotNull] IDamageCaster caster, [NotNull] IDamageReceiver receiver)
        {
            DamageType damageType = damageMulticast switch
            {
                2 => DamageType.ReplacementX2,
                3 => DamageType.ReplacementX3,
                4 => DamageType.ReplacementX4,
                _ => DamageType.Replacement
            };

            var dto = new DamageSourceDTO(damage, damageType);
            _damageService.TakeDamage(dto, caster, receiver, default);
        }
    }
}