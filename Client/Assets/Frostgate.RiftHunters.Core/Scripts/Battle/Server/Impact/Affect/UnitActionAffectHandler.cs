﻿using System.Threading;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect
{
    public sealed class UnitActionAffectHandler : IAffectHandler<IEffectAffectUnitActionData>
    {
        [NotNull] private readonly IDamageService _damageService;

        public UnitActionAffectHandler([NotNull] IDamageService damageService)
        {
            _damageService = damageService;
        }

        UniTask IAffectHandler.HandleAsync(EffectAffectData data, IImpactSource source, IImpactTarget target,
            AffectHandlerAdditionalData additionalData, CancellationToken cancellationToken) =>
            HandleAsync(data, source, target, additionalData, cancellationToken);

        public UniTask HandleAsync(IEffectAffectUnitActionData data, IImpactSource source, IImpactTarget target,
            AffectHandlerAdditionalData _, CancellationToken cancellationToken)
        {
            return !cancellationToken.IsCancellationRequested &&
                   target is IDamageReceiver receiver && source is IDamageCaster caster
                ? HandleInternalAsync(data, caster, receiver) : UniTask.CompletedTask;
        }

        private UniTask HandleInternalAsync(IEffectAffectUnitActionData data, IDamageCaster caster,
            IDamageReceiver receiver)
        {
            DamageSourceDTO dto = new(0, DamageType.Ability, data.ActionTriggers);
            _damageService.TakeDamage(dto, caster, receiver, default);

            return UniTask.CompletedTask;
        }
    }
}