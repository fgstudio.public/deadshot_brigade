using System;
using System.Threading;
using System.Diagnostics;
using UnityEngine;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Healing;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

using static Frostgate.RiftHunters.Core.Battle.Impact.Effect.EffectAffectDurationData;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect
{
    public sealed class StatAffectHandler : IAffectHandler<IEffectAffectStatData>
    {
        private delegate void HandleAction(float diff);

        [NotNull] private readonly IDamageService _damageService;
        [NotNull] private readonly IHealingService _healingService;

        public StatAffectHandler(
            [NotNull] IDamageService damageService, [NotNull] IHealingService healingService)
        {
            _damageService = damageService;
            _healingService = healingService;
        }

        UniTask IAffectHandler.HandleAsync(EffectAffectData data, IImpactSource source,
            IImpactTarget target, AffectHandlerAdditionalData additionalData, CancellationToken cancellationToken) =>
            HandleAsync(data, source, target, additionalData, cancellationToken);

        public UniTask HandleAsync(IEffectAffectStatData data, IImpactSource source,
            IImpactTarget target, AffectHandlerAdditionalData _, CancellationToken cancellationToken)
        {
            float health = data.Health;
            EffectAffectDurationData durationData = data.StatDurationData;

            return durationData.Type switch
            {
                DurationType.Instantly =>
                    HandleInstantlyAsync(health, source, target),

                DurationType.ContinuousFor =>
                    HandleContinuousAsync(health, durationData.Duration,
                        handleAction: diff => HandleInstantlyAsync(diff, source, target)),

                DurationType.RepeatableFor =>
                    HandleRepeatableAsync(health, durationData.RepeatCooldown, durationData.RepeatCount,
                        handleAction: diff => HandleInstantlyAsync(diff, source, target)),

                _ => throw new ArgumentOutOfRangeException(nameof(DurationType))
            };
        }

        private async UniTask HandleContinuousAsync(
            float health, TimeSpan duration, [NotNull] HandleAction handleAction)
        {
            var stopwatch = new Stopwatch();
            TimeSpan timeSpan = TimeSpan.Zero;

            stopwatch.Start();

            while (timeSpan < duration)
            {
                TimeSpan elapsed = stopwatch.Elapsed;
                TimeSpan dt = elapsed - timeSpan;
                float ratio = Mathf.Max(0f, (float)dt.Ticks / duration.Ticks);

                float portion = health * ratio;
                handleAction(portion);

                timeSpan += dt;
                await UniTask.Yield();
            }

            stopwatch.Stop();
        }

        private async UniTask HandleRepeatableAsync(
            float health, TimeSpan repeatCooldown, float repeatCount, [NotNull] HandleAction handleAction)
        {
            for (int i = 0; i < repeatCount; i++)
            {
                handleAction(health);
                await UniTask.Delay(repeatCooldown);
            }
        }

        private UniTask HandleInstantlyAsync(
            float health, [CanBeNull] IImpactSource source, [CanBeNull] IImpactTarget target)
        {
            // при продолжительной обработке объекты могут быть уничтожены (дисконнект и т.д.)
            if (source != null && target != null)
            {
                if (health > 0 && target is IHealingReceiver healingReceiver)
                {
                    var healingCaster = (IHealingCaster)source;
                    HealingSourceDTO dto = CreateHealingDto(health);
                    _healingService.Heal(dto, healingCaster, healingReceiver);
                }
                else if (health < 0 && target is IDamageReceiver damageReceiver && source is IDamageCaster damageCaster)
                {
                    DamageSourceDTO dto = CreateDamageDto(-health);
                    _damageService.TakeDamage(dto, damageCaster, damageReceiver, default);
                }
            }

            return UniTask.CompletedTask;
        }

        private HealingSourceDTO CreateHealingDto(float healing) => new(healing, HealingType.Ability);
        private DamageSourceDTO CreateDamageDto(float damage) => new(damage, DamageType.Ability);
    }
}