using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

using static Frostgate.RiftHunters.Core.Battle.Impact.Effect.EffectTargetData;
using static Frostgate.RiftHunters.Core.Battle.Impact.Effect.EffectAffectAreaData;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Effect
{
    public sealed class AffectedTargetsFinder
    {
        [NotNull] private readonly IImpactSource _source;
        [NotNull] private readonly ObjectFinder<IImpactTarget> _finder;

        public AffectedTargetsFinder([NotNull] IImpactSource source, [NotNull] ObjectFinder<IImpactTarget> finder)
        {
            _source = source;
            _finder = finder;
        }

        [NotNull, ItemNotNull]
        public IEnumerable<IImpactTarget> Find([NotNull] EffectData effectData, Vector3 targetPosition)
        {
            EffectAffectAreaData areaData = effectData.AffectAreaData;
            EffectTargetFilterData filterData = effectData.TargetData.FilterData;

            return effectData.TargetData.Type switch
            {
                TargetType.Self => _source is IImpactTarget target
                    ? target.AsEnumerable()
                    : Enumerable.Empty<IImpactTarget>(),

                TargetType.NoAim =>
                    (new StubImpactTarget(targetPosition, _source.Identity, ImpactTargetType.None)
                        as IImpactTarget).AsEnumerable(),

                TargetType.Filtered => effectData.AffectAreaData.Type switch
                {
                    AreaType.Rect => Filter(filterData, FindInRect(areaData, targetPosition)),
                    AreaType.Point => Filter(filterData, FindInPoint(areaData, targetPosition)),
                    AreaType.Radius => Filter(filterData, FindInRadius(areaData, targetPosition)),
                    AreaType.Sector => Filter(filterData, FindInSector(areaData, targetPosition)),
                    _ => throw new ArgumentOutOfRangeException(nameof(AreaType))
                },
                _ => throw new ArgumentOutOfRangeException(nameof(TargetType))
            };
        }

        private IEnumerable<IImpactTarget> Filter(
            [NotNull] EffectTargetFilterData filterData,
            [NotNull] IEnumerable<IImpactTarget> targets) =>
            targets.Where(filterData.IsMatching);

        private IEnumerable<IImpactTarget> FindInRect(
            [NotNull] EffectAffectAreaData areaData, Vector3 targetPosition)
        {
            Transform source = _source.GameObject!.transform;

            Quaternion rotation = source.rotation;
            rotation = Quaternion.Euler(rotation.eulerAngles + Vector3.up * areaData.Rotation);

            Vector3 findingPosition = CalcFindingPosition(source, targetPosition, areaData.Offset);
            RectYData rectData = new(areaData.Size, findingPosition, rotation);

            return _finder.FindInRectY(rectData);
        }

        private IEnumerable<IImpactTarget> FindInPoint(
            [NotNull] EffectAffectAreaData areaData, Vector3 targetPosition)
        {
            Transform source = _source.GameObject!.transform;
            Vector3 findingPosition = CalcFindingPosition(source, targetPosition, areaData.Offset);
            SphereData sphereData = new(findingPosition, areaData.Radius);

            return _finder.FindInSphere(sphereData);
        }

        private IEnumerable<IImpactTarget> FindInRadius(
            [NotNull] EffectAffectAreaData areaData, Vector3 targetPosition)
        {
            Transform source = _source.GameObject!.transform;
            Vector3 findingPosition = CalcFindingPosition(source, targetPosition, areaData.Offset);
            SphereData sphereData = new(findingPosition, areaData.Radius);

            return _finder.FindInSphere(sphereData);
        }

        private IEnumerable<IImpactTarget> FindInSector(
            [NotNull] EffectAffectAreaData areaData, Vector3 targetPosition)
        {
            Transform source = _source.GameObject!.transform;
            Vector3 forward = source.transform.forward;
            Vector2 direction = forward.RotateY(areaData.Rotation).CutY();
            Vector3 findingPosition = CalcFindingPosition(source, targetPosition, areaData.Offset);

            SectorYData sectorData = new(findingPosition, direction,
                areaData.Radius, areaData.AngleWidth);

            return _finder.FindInSectorY(sectorData);
        }

        private Vector3 CalcFindingPosition(
            [NotNull] Transform sourceTransform, Vector3 targetPosition, Vector3 offset)
        {
            return (targetPosition + offset)
                .RotateAroundPivot(targetPosition, sourceTransform.rotation);
        }
    }
}
