using System;
using System.Linq;
using UnityEngine;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

using static Frostgate.RiftHunters.Core.Battle.Impact.Effect.EffectTargetData;
using static Frostgate.RiftHunters.Core.Battle.Impact.Effect.EffectTargetDetectionData;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Effect
{
    public sealed class TargetPositionsFinder
    {
        [NotNull] private readonly IImpactSource _source;
        [NotNull] private readonly ObjectFinder<IImpactTarget> _finder;

        public TargetPositionsFinder([NotNull] IImpactSource source, [NotNull] ObjectFinder<IImpactTarget> finder)
        {
            _source = source;
            _finder = finder;
        }

        [NotNull]
        public IEnumerable<Vector3> Find([NotNull] EffectData effectData)
        {
            Transform sourceTransform = _source.GameObject!.transform;

            EffectTargetData targetData = effectData.TargetData;
            EffectAffectAreaData affectAreaData = effectData.AffectAreaData;

            return targetData.Type switch
            {
                TargetType.Self => sourceTransform.position.AsEnumerable(),
                TargetType.NoAim => CalcFindingPosition(sourceTransform, affectAreaData.Offset).AsEnumerable(),
                TargetType.Filtered => targetData.DetectionData.Type switch
                {
                    DetectionType.AutoAimInPoint => FindPositionsInPoint(sourceTransform, targetData.DetectionData.Offset),
                    DetectionType.AutoAimInRect => FindPositions(targetData, FindTargetsInRect),
                    DetectionType.AutoAimInRadius => FindPositions(targetData, FindTargetsInRadius),
                    DetectionType.AutoAimInSector => FindPositions(targetData, FindTargetsInSector),
                    _ => throw new ArgumentOutOfRangeException(nameof(DetectionType))
                },
                _ => throw new ArgumentOutOfRangeException(nameof(TargetType))
            };
        }

        private IEnumerable<Vector3> FindPositionsInPoint([NotNull] Transform sourceTransform, Vector3 offset) =>
            CalcFindingPosition(sourceTransform, offset).AsEnumerable();

        private IEnumerable<Vector3> FindPositions(
            [NotNull] EffectTargetData targetData,
            [NotNull] Func<EffectTargetData, IEnumerable<IImpactTarget>> findTargets)
        {
            EffectTargetFilterData filterData = targetData.FilterData;
            EffectTargetDetectionData detectionData = targetData.DetectionData;
            Vector3 sourcePosition = _source.GameObject!.transform.position;

            return findTargets(targetData)
                .Where(filterData.IsMatching)
                .OrderBy(filterData.CalcOrderIndex)
                .ThenBy(t => Vector3.Distance(t.Position, sourcePosition))
                .Select(t => t.Position)
                .Take(detectionData.MaxTargets);
        }

        private IEnumerable<IImpactTarget> FindTargetsInRadius([NotNull] EffectTargetData targetData)
        {
            Transform sourceTransform = _source.GameObject!.transform;
            EffectTargetDetectionData detectionData = targetData.DetectionData;

            Vector3 position = CalcFindingPosition(sourceTransform, detectionData.Offset);
            SphereData sphereData = new(position, detectionData.Radius);

            return _finder.FindInSphere(sphereData);
        }

        private IEnumerable<IImpactTarget> FindTargetsInSector([NotNull] EffectTargetData targetData)
        {
            Transform sourceTransform = _source.GameObject!.transform;
            EffectTargetDetectionData detectionData = targetData.DetectionData;

            Vector3 position = CalcFindingPosition(sourceTransform, detectionData.Offset);
            Vector2 direction = sourceTransform.forward.RotateY(detectionData.Rotation).CutY();

            SectorYData sectorData = new(
                position, direction,
                detectionData.Radius, detectionData.AngleWidth);

            return _finder.FindInSectorY(sectorData);
        }

        private IEnumerable<IImpactTarget> FindTargetsInRect([NotNull] EffectTargetData targetData)
        {
            Transform sourceTransform = _source.GameObject!.transform;
            EffectTargetDetectionData detectionData = targetData.DetectionData;

            Vector3 rotation = sourceTransform.rotation.eulerAngles;
            rotation.y += detectionData.Rotation;
            Vector3 position = CalcFindingPosition(sourceTransform, detectionData.Offset);
            RectYData rectData = new(detectionData.Size, position, Quaternion.Euler(rotation));

            return _finder.FindInRectY(rectData);
        }

        private Vector3 CalcFindingPosition([NotNull] Transform sourceTransform, Vector3 offset)
        {
            Vector3 sourcePosition = sourceTransform.position;
            Quaternion sourceRotation = sourceTransform.rotation;

            return (sourcePosition + offset)
                .RotateAroundPivot(sourcePosition, sourceRotation);
        }
    }
}
