using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Effect
{
    public readonly struct SectorYData
    {
        public readonly Vector3 Position;
        public readonly Vector2 Direction;
        public readonly float Radius;
        public readonly float Angle;

        public SectorYData(Vector3 position, Vector2 direction, float radius, float angle)
        {
            Position = position;
            Direction = direction;
            Radius = radius;
            Angle = angle;
        }
    }
}
