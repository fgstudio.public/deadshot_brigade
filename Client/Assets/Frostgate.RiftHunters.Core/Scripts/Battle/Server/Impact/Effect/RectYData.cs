using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Effect
{
    public readonly struct RectYData
    {
        public readonly Vector2 Size;
        public readonly Vector3 Position;
        public readonly Quaternion Rotation;

        public RectYData(Vector2 size, Vector3 position, Quaternion rotation)
        {
            Size = size;
            Position = position;
            Rotation = rotation;
        }
    }
}
