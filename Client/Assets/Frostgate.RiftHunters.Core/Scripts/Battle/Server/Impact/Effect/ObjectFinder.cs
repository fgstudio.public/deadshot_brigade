using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Effect
{
    public sealed class ObjectFinder<TObject> where TObject : IPositioned
    {
        private const int BufferSize = 100;

        [NotNull] private readonly Collider[] _buffer = new Collider[BufferSize];

        [NotNull, ItemNotNull]
        public IEnumerable<TObject> FindInSectorY(SectorYData sectorData) =>
            FindInSphere(new SphereData(sectorData.Position, sectorData.Radius))
                .Where(u => IsPositionInSectorY(u.Position, sectorData));

        [NotNull, ItemNotNull]
        public IEnumerable<TObject> FindInSphere(SphereData sphereData) =>
            OverlapSphere(sphereData.Position, sphereData.Radius)
                .Select(c => c.GetComponentInParent<TObject>())
                .Where(u => u != null);

        [NotNull, ItemNotNull]
        public IEnumerable<TObject> FindInRectY(RectYData rectData) =>
            OverlapRect(rectData.Position, rectData.Rotation, rectData.Size)
                .Select(c => c.GetComponentInParent<TObject>())
                .Where(u => u != null);

        [NotNull, ItemNotNull]
        private IEnumerable<Collider> OverlapSphere(Vector3 position, float radius)
        {
            int count = Physics.OverlapSphereNonAlloc(position, radius,
                _buffer, PhysicsLayers.AimTargetMask);

            return _buffer.Take(count);
        }

        [NotNull, ItemNotNull]
        private IEnumerable<Collider> OverlapRect(Vector3 position, Quaternion rotation, Vector2 size)
        {
            Vector3 halfExtents = new Vector3(size.x, 10f, size.y) / 2f;

            int count = Physics.OverlapBoxNonAlloc(position, halfExtents,
                _buffer, rotation, PhysicsLayers.AimTargetMask);

            return _buffer.Take(count);
        }

        private bool IsPositionInSectorY(Vector3 position, SectorYData sectorData)
        {
            Vector3 vectorToTarget = position - sectorData.Position;
            Vector2 distanceVector = new Vector2(vectorToTarget.x, vectorToTarget.z);
            float angleToTarget = Vector2.Angle(sectorData.Direction, distanceVector);

            return Mathf.Abs(angleToTarget) <= sectorData.Angle / 2f;
        }
    }
}
