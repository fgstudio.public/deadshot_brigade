using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Effect
{
    public readonly struct SphereData
    {
        public readonly Vector3 Position;
        public readonly float Radius;

        public SphereData(Vector3 position, float radius)
        {
            Position = position;
            Radius = radius;
        }
    }
}
