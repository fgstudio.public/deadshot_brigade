using System;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact.Effect
{
    public sealed class EffectExecutor
    {
        public delegate void Event(string effectId, Vector3 position);

        public event Event EffectActivatedInPosition;
        public event Event EffectAffectingInPosition;
        public event Event EffectAffectingTarget;
        public event Event EffectAffectedTarget;

        [NotNull] private readonly IImpactSource _source;
        [NotNull] private readonly EffectData _effectData;
        [NotNull] private readonly AffectApplier _affectApplier;
        [NotNull] private readonly DurationCalculator _durationCalculator;
        [NotNull] private readonly AffectedTargetsFinder _affectedTargetsFinder;
        [NotNull] private readonly TargetPositionsFinder _targetPositionsFinder;

        [CanBeNull] private CancellationTokenSource _cts;

        private string EffectId => _effectData.Id;

        public EffectExecutor([NotNull] IImpactSource source, [NotNull] EffectData effectData,
            [NotNull] AffectApplier affectApplier, [NotNull] DurationCalculator durationCalculator,
            [NotNull] AffectedTargetsFinder affectedTargetsFinder, [NotNull] TargetPositionsFinder targetPositionsFinder)
        {
            _source = source;
            _effectData = effectData;
            _affectApplier = affectApplier;
            _durationCalculator = durationCalculator;
            _affectedTargetsFinder = affectedTargetsFinder;
            _targetPositionsFinder = targetPositionsFinder;
        }

        public async UniTask ExecuteAsync()
        {
            _cts = new CancellationTokenSource();
            CancellationToken token = _cts.Token;

            EffectRepeatsData repeatsData = _effectData.RepeatsData;
            int repeatsCount = repeatsData.RepeatsCount;

            for (int i = 0; i < repeatsCount; i++)
            {
                bool isLastRepeat = i == repeatsCount - 1;

                if (!token.IsCancellationRequested)
                    await ExecuteEffectAsync(token);

                if (!token.IsCancellationRequested && !isLastRepeat)
                    await UniTask.Delay(repeatsData.RepeatDelay, cancellationToken: token);
            }
        }

        public void Interrupt()
        {
            if (_cts != null)
            {
                _cts.Cancel();
                _cts.Dispose();
                _cts = null;
            }
        }

        private UniTask ExecuteEffectAsync(CancellationToken cancellationToken)
        {
            IEnumerable<Vector3> targetPositions =
                _targetPositionsFinder.Find(_effectData);

            IEnumerable<UniTask> tasks =
                targetPositions.Select(tp => ExecuteInPositionAsync(tp, cancellationToken));

            return UniTask.WhenAll(tasks);
        }

        private UniTask ExecuteInPositionAsync(Vector3 targetPosition, CancellationToken cancellationToken)
        {
            Vector3 sourcePosition = _source.GameObject!.transform.position;
            EffectActivationData activationData = _effectData.ActivationData;

            EffectActivatedInPosition?.Invoke(EffectId, targetPosition);

            TimeSpan delay =
                _durationCalculator.CalcEffectDelay(activationData, sourcePosition, targetPosition);

            return delay.Ticks == 0
                ? ActivateInPositionAsync(targetPosition, cancellationToken)
                : UniTask.Delay(delay, cancellationToken: cancellationToken)
                    .ContinueWith(() => ActivateInPositionAsync(targetPosition, cancellationToken));
        }

        private UniTask ActivateInPositionAsync(Vector3 position, CancellationToken cancellationToken)
        {
            IEnumerable<IImpactTarget> targets =
                _affectedTargetsFinder.Find(_effectData, position);

            EffectAffectingInPosition?.Invoke(EffectId, position);

            IEnumerable<UniTask> tasks = targets
                .Select(t => ExecuteForTargetAsync(t, cancellationToken));

            return UniTask.WhenAll(tasks);
        }

        private UniTask ExecuteForTargetAsync([NotNull] IImpactTarget target, CancellationToken cancellationToken)
        {
            Vector3 position = target.Position;
            EffectAffectingTarget?.Invoke(EffectId, position);

            return _affectApplier.ApplyAsync(_effectData.AffectData, _source, target, cancellationToken)
                .ContinueWith(() => EffectAffectedTarget?.Invoke(EffectId, position));
        }
    }
}
