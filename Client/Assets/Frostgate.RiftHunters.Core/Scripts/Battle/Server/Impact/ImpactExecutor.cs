using System;
using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Server.Impact.Effect;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    public sealed class ImpactExecutor : IDisposable
    {
        public event Action<string> CastStarted;
        public event Action<string> CastFinished;
        public event Action<string> Interrupted;

        public event Action<string, string, Vector3> EffectActivatedInPosition;
        public event Action<string, string, Vector3> EffectAffectingInPosition;
        public event Action<string, string, Vector3> EffectAffectingTarget;
        public event Action<string, string, Vector3> EffectAffectedTarget;

        private readonly ITimer _timer;
        private readonly ImpactConfig _config;
        private readonly IReadOnlyDictionary<EffectData, EffectExecutor> _effectExecutors;

        public ImpactExecutor([NotNull] ImpactConfig config, [NotNull] ITimer timer,
            [NotNull] IReadOnlyDictionary<EffectData, EffectExecutor> effectExecutors)
        {
            _timer = timer;
            _config = config;
            _effectExecutors = effectExecutors;

            foreach (EffectExecutor executor in effectExecutors.Values)
                SubscribeExecutor(executor);

            SubscribeTimer(timer);
        }

        public void Dispose()
        {
            Interrupt();

            foreach (EffectExecutor executor in _effectExecutors.Values)
                UnsubscribeExecutor(executor);

            UnsubscribeTimer(_timer);
        }

        public UniTask ExecuteAsync()
        {
            _timer.Start();

            IEnumerable<UniTask> tasks = _config.Effects
                .Select(e => _effectExecutors[e].ExecuteAsync());

            return UniTask.WhenAll(tasks);
        }

        public void Interrupt()
        {
            _timer.Stop();

            foreach (EffectExecutor executor in _effectExecutors.Values)
                executor.Interrupt();

            Interrupted?.Invoke(_config.Id);
        }

        private void SubscribeExecutor([NotNull] EffectExecutor executor)
        {
            executor.EffectActivatedInPosition += OnActivatedInPosition;
            executor.EffectAffectingInPosition += OnAffectingInPosition;
            executor.EffectAffectingTarget += OnAffectingTarget;
            executor.EffectAffectedTarget += OnAffectedTarget;
        }

        private void UnsubscribeExecutor([NotNull] EffectExecutor executor)
        {
            executor.EffectActivatedInPosition -= OnActivatedInPosition;
            executor.EffectAffectingInPosition -= OnAffectingInPosition;
            executor.EffectAffectingTarget -= OnAffectingTarget;
            executor.EffectAffectedTarget -= OnAffectedTarget;
        }

        private void SubscribeTimer([NotNull] ITimer timer)
        {
            timer.Started += OnTimerStart;
            timer.Elapsed += OnTimerFinish;
        }

        private void UnsubscribeTimer([NotNull] ITimer timer)
        {
            timer.Started -= OnTimerStart;
            timer.Elapsed -= OnTimerFinish;
        }

        private void OnActivatedInPosition(string effectId, Vector3 position) => EffectActivatedInPosition?.Invoke(_config.Id, effectId, position);
        private void OnAffectingInPosition(string effectId, Vector3 position) => EffectAffectingInPosition?.Invoke(_config.Id, effectId, position);
        private void OnAffectingTarget(string effectId, Vector3 position) => EffectAffectingTarget?.Invoke(_config.Id, effectId, position);
        private void OnAffectedTarget(string effectId, Vector3 position) => EffectAffectedTarget?.Invoke(_config.Id, effectId, position);

        private void OnTimerStart() => CastStarted?.Invoke(_config.Id);
        private void OnTimerFinish() => CastFinished?.Invoke(_config.Id);
    }
}
