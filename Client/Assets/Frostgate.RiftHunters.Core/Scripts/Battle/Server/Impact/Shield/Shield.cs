using Mirror;
using Zenject;
using UnityEngine;
using System;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    public interface IShield : IGameObject, ISpawnable
    {
        TimeSpan LifeTime { get; }
    }

    public class Shield : NetworkBehaviour, IShield, IBulletProcessor
    {
        [SerializeField] private Collider _collider;
        [SerializeField] private float _lifeTime;
        [SerializeField] private Transform _view;
        [SerializeField] private UnitActionTypes _startActionType = UnitActionTypes.UltraDamage;
        [SerializeField] private float _startActionRadius = 5;

        [SyncVar] private BattleUnitType _faction;

        private IReadOnlyObjectCollection<uint, UnitNetwork> _units;
        private BulletProcessors _bulletProcessors;
        private int _physicMask;

        public EntityType Type => EntityType.Shield;
        GameObject IGameObject.GameObject => this != null? gameObject : null;
        public TimeSpan LifeTime => TimeSpan.FromSeconds(_lifeTime);

        [Inject]
        private void MonoConstructor(SharedDependencies sharedDependencies)
        {
            _units = sharedDependencies.UnitCollections.Get<uint>();
            _bulletProcessors = sharedDependencies.BulletProcessors;
        }

        public void ServerInit(BattleUnitType faction)
        {
            _faction = faction;
            SharedInit();

            var pos = transform.position;
            foreach (var unit in _units.Values)
            {
                if (unit.UnitType == _faction)
                    continue;

                if (Vector3.Distance(pos, unit.Transform.position) > _startActionRadius)
                    continue;

                unit.TryExecuteUnitAction(_startActionType, pos);
            }
        }

        private void SharedInit()
        {
            foreach (var unit in _units)
                OnUnitAdd(unit);

            _physicMask = 1 << _collider.gameObject.layer;
            _units.Added += OnUnitAdd;
            _bulletProcessors.Add(this);
            _collider.enabled = true;
        }

        public override void OnStartClient()
        {
            if (isClientOnly)
                SharedInit();

            _view.gameObject.SetActive(true);
        }

        private void OnUnitAdd(UnitNetwork unit)
        {
            if (unit.UnitType == _faction)
            {
                Physics.IgnoreCollision(_collider, unit.Body, true);
                Physics.IgnoreCollision(_collider, unit.AimTarget, true);
            }
        }

        private void OnDestroy()
        {
            if (_units != null)
                _units.Added -= OnUnitAdd;

            if (_bulletProcessors != null)
                _bulletProcessors.Remove(this);
        }

        // Остановка коллайдером вражеских пуль
        public void Process(BulletData bullet, UnitNetwork unit, ref float step)
        {
            if (unit.UnitType == _faction)
                return;

            var count = Physics.RaycastNonAlloc(bullet.Position, bullet.Direction, PhysicHelper.HitResults, step, _physicMask);

            for (var i = 0; i < count; i++)
            {
                var rh = PhysicHelper.HitResults[i];

                if (rh.collider != _collider)
                    continue;

                step = rh.distance;
                bullet.Distance = rh.distance;
                break;
            }
        }
    }
}