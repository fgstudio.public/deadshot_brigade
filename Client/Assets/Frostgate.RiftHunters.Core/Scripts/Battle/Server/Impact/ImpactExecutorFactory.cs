using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect;
using Frostgate.RiftHunters.Core.Battle.Server.Impact.Effect;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    public sealed class ImpactExecutorFactory
    {
        [NotNull] private readonly IImpactSource _source;
        [NotNull] private readonly AffectApplier _affectApplier;
        [NotNull] private readonly DurationCalculator _durationCalculator;
        [NotNull] private readonly AffectedTargetsFinder _affectedTargetsFinder;
        [NotNull] private readonly TargetPositionsFinder _targetPositionsFinder;

        public ImpactExecutorFactory(
            [NotNull] IImpactSource source, [NotNull] ObjectFinder<IImpactTarget> objectFinder,
            [NotNull] AffectApplier affectApplier, [NotNull] DurationCalculator durationCalculator)
        {
            _source = source;
            _affectApplier = affectApplier;
            _durationCalculator = durationCalculator;
            _affectedTargetsFinder = new AffectedTargetsFinder(source, objectFinder);
            _targetPositionsFinder = new TargetPositionsFinder(source, objectFinder);
        }

        public ImpactExecutor Create([NotNull] ImpactConfig impactConfig) =>
            new
            (
                impactConfig,
                CreateTimer(impactConfig),
                CreateEffectExecutors(impactConfig)
            );

        private ITimer CreateTimer([NotNull] ImpactConfig impactConfig) =>
            new Timer(impactConfig.CastDuration);

        private IReadOnlyDictionary<EffectData, EffectExecutor> CreateEffectExecutors(
            [NotNull] ImpactConfig impactConfig) =>
            impactConfig.Effects.ToDictionary
            (
                e => e,
                e => new EffectExecutor(_source, e, _affectApplier,
                    _durationCalculator, _affectedTargetsFinder, _targetPositionsFinder)
            );
    }
}
