using System;
using System.Threading;
using Zenject;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Filters;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    public interface IWaveSpawner : IGameObject, ISpawnable
    {
        UnityEvent<int> WaveStarted { get; }
        UnityEvent Started { get; }
        UnityEvent Finished { get; }

        int CurrentWaveIndex { get; }
        bool IsActive { get; }

        UniTask StartEffect([NotNull] UnitNetwork host);
        void FinishEffect();
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5")]
    [AddComponentMenu(BattleComponentMenus.Server.Impacts.Menu + "/" + nameof(WaveSpawner))]
    // TODO: логику вынести в нативный класс. оставить только вьюху для генерации на клиенте и коллайдер — на сервере.
    public sealed class WaveSpawner : MonoBehaviour, IWaveSpawner
    {
        [Header("Settings")]
        [SerializeField, Required, AssetsOnly, AssetSelector] private WaveSpawnerConfig _config;
        [Required, HideLabel, BoxGroup(nameof(FilterToDamage))] public UnitFilterParams FilterToDamage;
        [Required, HideLabel, BoxGroup(nameof(FilterToDistribute))] public UnitFilterParams FilterToDistribute;

        [Header("References")]
        [Required, ChildGameObjectsOnly] public ObjectCatcher ObjectCatcher;

        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent<int> WaveStarted { get; private set; }
        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent<int> WaveFinished { get; private set; }
        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Started { get; private set; }
        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Finished { get; private set; }

        [Title("Info")]
        [ShowInInspector, ReadOnly] public int CurrentWaveIndex { get; private set; }
        [ShowInInspector, ReadOnly] public bool IsActive { get; private set; }

        public event Action ConfigChanged;


        [CanBeNull] private WaveSpawnerFactory _factory;
        [CanBeNull] private IDamageService _damageService;

        [CanBeNull] private UnitNetwork _host;
        [CanBeNull] private ILogger<WaveSpawner> _logger;
        [CanBeNull] private CancellationTokenSource _cts;
        [CanBeNull] private IWaveSpawnerViewInvoker _viewInvoker;
        [CanBeNull] private WaveTargetCollection _targetCollection;

        [NotNull] private readonly ListCache<UnitNetwork> _targetsCache = new();

        public EntityType Type => EntityType.WaveSpawner;

        public WaveSpawnerConfig Config
        {
            get => _config;
            set
            {
                if (_config != value)
                {
                    _config = value;
                    ConfigChanged?.Invoke();
                }
            }
        }

        GameObject IGameObject.GameObject => this != null? gameObject : null;

        [Inject]
        private void MonoConstructor(IPrefabFactory prefabFactory,
            IServiceRoster serviceRoster, ILogger<WaveSpawner> logger)
        {
            _logger = logger;
            _factory = new WaveSpawnerFactory(logger, this, prefabFactory);
            _damageService = serviceRoster.DamageService;
        }

        private void OnDestroy() => FinishEffect();

        public UniTask StartEffect(UnitNetwork host)
        {
            if (IsActive)
                FinishEffect();

            _viewInvoker ??= host.GetComponentInChildren<IWaveSpawnerViewInvoker>();

            _targetCollection = new WaveTargetCollection(
                _logger!, host, ObjectCatcher, FilterToDamage,
                Config.Wave.DistributingSpawnerConfig != null ? FilterToDistribute : null);

            InitTargets(_targetCollection);
            SubscribeCollection(_targetCollection);

            _host = host;
            SubscribeHost(host);

            IsActive = true;
            Started?.Invoke();
            _logger!.Log($"Started spawning for {host.name}");
            _viewInvoker?.StartSpawner(Config.Wave.Radius, Config.Duration);

            _cts = new CancellationTokenSource();
            return WaveLaunchingRoutine(_cts.Token).ContinueWith(FinishEffect);
        }

        public void FinishEffect()
        {
            CurrentWaveIndex = default;

            if (_cts != null)
            {
                _cts.Cancel();
                _cts.Dispose();
                _cts = null;
            }

            if (_host != null)
            {
                UnsubscribeHost(_host);
            }

            if (_targetCollection != null)
            {
                UnsubscribeCollection(_targetCollection);
                DeinitTargets(_targetCollection);
                _targetCollection.Dispose();
                _targetCollection = null;
            }

            if (IsActive)
            {
                IsActive = false;
                Finished?.Invoke();
                _viewInvoker?.FinishSpawner();
                _logger!.Log(_host ? $"Finished spawning for {_host.name}" : "Finished spawning");
            }
        }

        private void InitTargets([NotNull] WaveTargetCollection targetCollection)
        {
            foreach (UnitFilterParams filterParams in targetCollection.Keys)
            foreach (UnitNetwork unitNetwork in targetCollection[filterParams])
                if (unitNetwork != null)
                    OnAdded(filterParams, unitNetwork);
        }

        private void DeinitTargets([NotNull] WaveTargetCollection targetCollection)
        {
            foreach (UnitFilterParams filterParams in targetCollection.Keys)
            foreach (UnitNetwork unitNetwork in targetCollection[filterParams])
                if (unitNetwork != null)
                    OnRemoved(filterParams, unitNetwork);
        }

        private void SubscribeHost([NotNull] UnitNetwork host)
        {
            host.Destroyed.AddListener(FinishEffect);
            host.UnitNetworkState.HealthChanged += OnHostHealthChanged;
        }

        private void UnsubscribeHost([NotNull] UnitNetwork host)
        {
            host.Destroyed.RemoveListener(FinishEffect);
            host.UnitNetworkState.HealthChanged -= OnHostHealthChanged;
        }

        private void SubscribeCollection([NotNull] WaveTargetCollection targetCollection)
        {
            targetCollection.Added += OnAdded;
            targetCollection.Removed += OnRemoved;
        }

        private void UnsubscribeCollection([NotNull] WaveTargetCollection targetCollection)
        {
            targetCollection.Added -= OnAdded;
            targetCollection.Removed -= OnRemoved;
        }

        private void OnHostHealthChanged(float diff)
        {
            if (_host != null && _host.UnitNetworkState.IsDead)
                FinishEffect();
        }

        private void OnAdded([NotNull] UnitFilterParams filterParams, [NotNull] UnitNetwork unitNetwork)
        {
            if (filterParams == FilterToDistribute)
            {
                _factory!.CreateSpawner(unitNetwork, Config.Wave.DistributingSpawnerConfig);
                _logger!.Log($"Start distributing from {_host!.name} to {unitNetwork.name}");
            }
        }

        private void OnRemoved([NotNull] UnitFilterParams filterParams, [NotNull] UnitNetwork unitNetwork)
        {
            if (filterParams == FilterToDistribute)
            {
                _factory!.RemoveSpawner(unitNetwork, Config.Wave.DistributingSpawnerConfig);
                _logger!.Log($"Stop distributing from {_host!.name} to {unitNetwork.name}");
            }
        }

        private async UniTask WaveLaunchingRoutine(CancellationToken token)
        {
            while(CurrentWaveIndex < Config.Count && !token.IsCancellationRequested)
            {
                WaveStarted?.Invoke(CurrentWaveIndex);
                _viewInvoker?.StartWave(CurrentWaveIndex, Config.Wave.Radius, Config.Wave.Duration);

                _logger!.Log("Start wave iteration");
                await WaveRoutine(token);
                _logger!.Log("Finish wave iteration");

                WaveFinished?.Invoke(CurrentWaveIndex);
                _viewInvoker?.FinishWave(CurrentWaveIndex);

                await UniTask.Delay(Config.Cooldown, cancellationToken: token);
                CurrentWaveIndex++;
            }
        }

        private async UniTask WaveRoutine(CancellationToken cancellationToken)
        {
            var damageSource = new DamageSourceDTO(
                Config.Wave.Damage, DamageType.Ability, Config.Wave.DamageUnitActions);

            await UniTask.Delay(Config.Wave.Duration / 2, cancellationToken: cancellationToken);

            _targetsCache.Cache(_targetCollection![FilterToDamage]);
            foreach (UnitNetwork target in _targetsCache)
                if (!cancellationToken.IsCancellationRequested && target != null && _host != null)
                {
                    _damageService!.TakeDamage(damageSource, _host, target, default);
                    _logger!.Log($"Damaged by wave {damageSource.GetDamage()} from {_host.name} to {target.name}");
                }

            _targetsCache.Cache(_targetCollection![FilterToDistribute]);
            foreach (UnitNetwork target in _targetsCache)
                if (!cancellationToken.IsCancellationRequested && target != null && _host != null)
                {
                    target.GetComponentInChildren<WaveSpawner>().StartEffect(target);
                    _logger!.Log($"Distributed by wave from {_host.name} to {target.name}");
                }

            await UniTask.Delay(Config.Wave.Duration / 2, cancellationToken: cancellationToken);
        }
    }
}