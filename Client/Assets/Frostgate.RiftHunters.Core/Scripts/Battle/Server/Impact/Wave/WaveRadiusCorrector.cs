using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5")]
    [AddComponentMenu(BattleComponentMenus.Server.Impacts.Menu + "/" + nameof(WaveRadiusCorrector))]
    public sealed class WaveRadiusCorrector : MonoBehaviour
    {
        [Header("References")]
        [Required, ChildGameObjectsOnly] public WaveSpawner Spawner;
        [Required, ChildGameObjectsOnly] public SphereCollider Collider;

        private void Awake() =>
            InitReferences();

        private void OnValidate()
        {
            InitReferences();
            UpdateRadius();
        }

        private void Start()
        {
            Spawner.ConfigChanged += UpdateRadius;
            UpdateRadius();
        }

        private void OnDestroy() =>
            Spawner.ConfigChanged -= UpdateRadius;

        private void InitReferences()
        {
            Spawner ??= GetComponentInChildren<WaveSpawner>();
            Collider ??= GetComponentInChildren<SphereCollider>();
        }

        private void UpdateRadius()
        {
            if (Spawner.Config != null)
                Collider.radius = Spawner.Config.Wave.Radius;
        }
    }
}