using UnityEngine;
using JetBrains.Annotations;
using System;
using System.Linq;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Filters;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5"/>
    /// </summary>
    public sealed class WaveTargetCollection : IDisposable
    {
        public event Action<UnitFilterParams, UnitNetwork> Added;
        public event Action<UnitFilterParams, UnitNetwork> Removed;

        [NotNull] private readonly ILogger _logger;
        [NotNull] private readonly ObjectCatcher _objectCatcher;

        [CanBeNull] private readonly UnitNetwork _host;
        [CanBeNull] private readonly UnitFilterParams _filterToDamage;
        [CanBeNull] private readonly UnitFilterParams _filterToDistribute;

        [NotNull] private readonly Dictionary<UnitFilterParams, List<UnitNetwork>> _targets;

        [ItemNotNull] public IEnumerable<UnitFilterParams> Keys => _targets.Keys;
        [ItemCanBeNull] public IReadOnlyList<UnitNetwork> this[UnitFilterParams f] =>
            _targets.TryGetValue(f, out List<UnitNetwork> list)
                ? list : Array.Empty<UnitNetwork>();

        public WaveTargetCollection(
            [NotNull] ILogger logger, [NotNull] UnitNetwork host, [NotNull] ObjectCatcher objectCatcher,
            [CanBeNull] UnitFilterParams filterToDamage, [CanBeNull] UnitFilterParams filterToDistribute)
        {
            _host = host;
            _logger = logger;
            _objectCatcher = objectCatcher;
            _filterToDamage = filterToDamage;
            _filterToDistribute = filterToDistribute;

            _targets = new Dictionary<UnitFilterParams, List<UnitNetwork>>();
            if (_filterToDamage != null) _targets[_filterToDamage] = new List<UnitNetwork>();
            if (_filterToDistribute != null) _targets[_filterToDistribute] = new List<UnitNetwork>();

            foreach (Collider o in _objectCatcher.Objects) OnCaught(o);
            Subscribe(_objectCatcher);
        }

        public void Dispose()
        {
            Unsubscribe(_objectCatcher);

            UnitNetwork[] unitNetworks = _targets.SelectMany(l => l.Value).Distinct().ToArray();
            foreach (UnitNetwork unitNetwork in unitNetworks) RemoveTarget(unitNetwork);

            _targets.Clear();
        }

        private void Subscribe([NotNull] ObjectCatcher objectCatcher)
        {
            objectCatcher.Caught.AddListener(OnCaught);
            objectCatcher.Lost.AddListener(OnLost);
        }

        private void Unsubscribe([NotNull] ObjectCatcher objectCatcher)
        {
            objectCatcher.Caught.RemoveListener(OnCaught);
            objectCatcher.Lost.RemoveListener(OnLost);
        }

        private void OnCaught(Collider obj)
        {
            _logger.Log($"Caught target {obj.name}");

            if (_host != null &&
                // TODO: получать из коллекции
                obj.TryGetComponentInHierarchy(out UnitNetwork unitNetwork) &&
                _host != unitNetwork)
                AddTarget(unitNetwork);
        }

        private void OnLost(Collider obj)
        {
            _logger.Log($"Lost target {obj.name}");

            if (_host != null &&
                // TODO: получать из коллекции
                obj.TryGetComponentInHierarchy(out UnitNetwork unitNetwork) &&
                _host != unitNetwork)
                RemoveTarget(unitNetwork);
        }

        private void AddTarget([NotNull] UnitNetwork unitNetwork)
        {
            if (TryAddTarget(_filterToDamage, unitNetwork))
                _logger.Log($"Added to collection {unitNetwork.name} for damage");

            if (TryAddTarget(_filterToDistribute, unitNetwork))
                _logger.Log($"Added to collection {unitNetwork.name} for distribution");
        }

        private void RemoveTarget([NotNull] UnitNetwork unitNetwork)
        {
            if (TryRemoveTarget(_filterToDamage, unitNetwork))
                _logger.Log($"Removed from collection {unitNetwork.name} from damage");

            if (TryRemoveTarget(_filterToDistribute, unitNetwork))
                _logger.Log($"Removed from collection {unitNetwork.name} from distribution");
        }

        private bool TryAddTarget([CanBeNull] UnitFilterParams filter, [NotNull] UnitNetwork unitNetwork)
        {
            if (filter == null) return false;
            if (!filter.IsMatching(unitNetwork)) return false;

            _targets[filter].Add(unitNetwork);
            Added?.Invoke(filter, unitNetwork);
            return true;
        }

        private bool TryRemoveTarget([CanBeNull] UnitFilterParams filter, [NotNull] UnitNetwork unitNetwork)
        {
            if (filter == null) return false;
            if (!filter.IsMatching(unitNetwork)) return false;

            _targets[filter].Remove(unitNetwork);
            Removed?.Invoke(filter, unitNetwork);
            return true;
        }
    }
}