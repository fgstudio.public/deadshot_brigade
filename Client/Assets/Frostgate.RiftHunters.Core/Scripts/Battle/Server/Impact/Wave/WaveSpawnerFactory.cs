using System.Linq;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5"/>
    /// </summary>
    public sealed class WaveSpawnerFactory
    {
        [NotNull] private readonly ILogger _logger;
        [NotNull] private readonly WaveSpawner _prefab;
        [NotNull] private readonly IPrefabFactory _prefabFactory;

        public WaveSpawnerFactory(
            [NotNull] ILogger logger,
            [NotNull] WaveSpawner prefab,
            [NotNull] IPrefabFactory prefabFactory)
        {
            _logger = logger;
            _prefab = prefab;
            _prefabFactory = prefabFactory;
        }

        [NotNull]
        public WaveSpawner CreateSpawner([NotNull] UnitNetwork target, [NotNull] WaveSpawnerConfig config)
        {
            var waveSpawner = _prefabFactory.Instantiate<WaveSpawner>(_prefab, target.transform);
            waveSpawner.Config = config;

            _logger.Log($"Created spawner {config.name} for {target.name}");
            return waveSpawner;
        }

        public void RemoveSpawner([NotNull] UnitNetwork target, [NotNull] WaveSpawnerConfig config)
        {
            WaveSpawner spawner = target
                .GetComponentsInChildren<WaveSpawner>()
                .FirstOrDefault(s => s.Config == config);

            if (spawner != null)
            {
                Object.Destroy(spawner.gameObject);
                _logger.Log($"Removed spawner {config.name} for {target.name}");
            }
        }
    }
}