using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Core.Battle.Server.Impact;
using Frostgate.RiftHunters.Core.Battle.Server.Impact.Affect;
using Frostgate.RiftHunters.Core.Battle.Server.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using static Frostgate.RiftHunters.Core.Battle.Impact.Effect.EffectAffectData.AffectType;

namespace Frostgate.RiftHunters.Core.Battle.Impact
{
    public sealed class ImpactStateInitializer
    {
        [NotNull] private readonly ObjectFinder<IImpactTarget> _targetFinder;
        [NotNull] private readonly AffectApplier _affectApplier;
        [NotNull] private readonly DurationCalculator _durationCalculator;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _units;

        public ImpactStateInitializer(
            [NotNull] AreaRandom areaRandom,
            [NotNull] IServiceRoster serviceRoster,
            [NotNull] UnitEffectStateFactory effectStateFactory,
            [NotNull] IReadOnlyObjectCommandSenderRepository commandSenderRepository,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> units)
        {
            _units = units;
            var affectHandlers = new Dictionary<EffectAffectData.AffectType, IAffectHandler>()
            {
                [Stat] = new StatAffectHandler(serviceRoster.DamageService, serviceRoster.HealingService),
                [Buff] = new BuffAffectHandler(effectStateFactory),
                [Detonation] = new DetonationAffectHandler(effectStateFactory, serviceRoster.DamageService),
                [Generation] = new GenerationAffectHandler(areaRandom, commandSenderRepository),
                [UnitAction] = new UnitActionAffectHandler(serviceRoster.DamageService),
                [HealPercent] = new HealPercentAffectHandler(serviceRoster.HealingService),
                [Spawn] = new SpawnAffectHandler(commandSenderRepository)
            };

            _targetFinder = new ObjectFinder<IImpactTarget>();
            _durationCalculator = new DurationCalculator();
            _affectApplier = new AffectApplier(affectHandlers, _units);
        }

        public void Init([NotNull] ImpactState impactState, [NotNull] IImpactSource impactSource)
        {
            var impactExecutorFactory = new ImpactExecutorFactory(
                impactSource, _targetFinder, _affectApplier, _durationCalculator);

            impactState.Init(impactExecutorFactory);
        }
    }
}
