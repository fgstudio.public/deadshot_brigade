using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/fa2f1c562cb9488fbbafe2c04b2ea6c4")]
    [AddComponentMenu(BattleComponentMenus.Server.Impacts.Menu + "/" + nameof(LinkRadiusCorrector))]
    public sealed class LinkRadiusCorrector : MonoBehaviour
    {
        [Header("References")]
        [Required, ChildGameObjectsOnly] public LinkSpawner Spawner;
        [Required, ChildGameObjectsOnly] public SphereCollider Collider;

        private void Awake() =>
            InitReferences();

        private void OnValidate()
        {
            InitReferences();
            UpdateRadius();
        }

        private void Start()
        {
            Spawner.ConfigChanged += UpdateRadius;
            UpdateRadius();
        }

        private void OnDestroy() =>
            Spawner.ConfigChanged -= UpdateRadius;

        private void InitReferences()
        {
            Spawner ??= GetComponentInChildren<LinkSpawner>();
            Collider ??= GetComponentInChildren<SphereCollider>();
        }

        private void UpdateRadius()
        {
            if (Spawner.Config != null)
                Collider.radius = Spawner.Config.Radius;
        }
    }
}