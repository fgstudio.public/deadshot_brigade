using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using Zenject;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    public interface IReadOnlyLinkSpawner : ILinkRankProvider
    {
        UnityEvent<UnitNetwork> UnitLinked { get; }
        UnityEvent<UnitNetwork> UnitUnlinked { get; }
        UnityEvent Started { get; }
        UnityEvent Finished { get; }

        [NotNull, ItemNotNull] IEnumerable<UnitNetwork> LinkedUnits { get; }
        LinkSpawnerConfig Config { get; }
        bool IsActive { get; }
    }

    public interface ILinkSpawner : IReadOnlyLinkSpawner, IGameObject, ISpawnable
    {
        UniTask StartEffect([NotNull] UnitNetwork host, [NotNull] IMasterLinkSpawner mainSpawner);
        void FinishEffect();
    }

    public interface IMasterLinkSpawner : ILinkSpawner
    {
        void UpdateActualEffects();
    }

    public interface IChildLinkSpawner : ILinkSpawner
    {
        void UpdateActualEffects([NotNull, ItemNotNull] IReadOnlyList<UnitEffectConfig> effects);
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/fa2f1c562cb9488fbbafe2c04b2ea6c4")]
    [AddComponentMenu(BattleComponentMenus.Server.Impacts.Menu + "/" + nameof(LinkSpawner))]
    // TODO: здесь очень грязно. Последовательность всех строк важна!
    // TODO: логику вынести в нативный класс. оставить только вьюху для генерации на клиенте и коллайдер — на сервере.
    public sealed class LinkSpawner : MonoBehaviour, IMasterLinkSpawner, IChildLinkSpawner
    {
        private const int DefaultEffectKey = -1;

        [Header("Settings")]
        [SerializeField, Required, AssetsOnly, AssetSelector] private LinkSpawnerConfig _config;

        [Header("References")]
        [Required, ChildGameObjectsOnly] public ObjectCatcher ObjectCatcher;

        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent<UnitNetwork> UnitLinked { get; private set; }
        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent<UnitNetwork> UnitUnlinked { get; private set; }
        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Started { get; private set; }
        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Finished { get; private set; }

        [Title("Info")]
        [ShowInInspector, ReadOnly]
        public int LinkRank => ReferenceEquals(_mainSpawner, this)
            ? LinkedUnits.Count()
            : _mainSpawner?.LinkRank ?? default;

        [ShowInInspector, ReadOnly]
        public IEnumerable<UnitNetwork> LinkedUnits =>
            IsActive
                ? _spawners.Values
                .SelectMany(s => s.LinkedUnits)
                .Union(_spawners.Keys)
                .Union(_host.AsEnumerable())
                .Where(u => u != null)
                .Distinct()
                : Enumerable.Empty<UnitNetwork>();

        public EntityType Type => EntityType.LinkSpawner;

        [ShowInInspector, ReadOnly] public bool IsActive { get; private set; }

        public event Action ConfigChanged;

        [CanBeNull] private LinkSpawnerFactory _factory;

        [CanBeNull] private UnitNetwork _host;
        [CanBeNull] private ILogger<LinkSpawner> _logger;
        [CanBeNull] private CancellationTokenSource _cts;
        [CanBeNull] private IMasterLinkSpawner _mainSpawner;
        [CanBeNull] private ILinkSpawnerViewInvoker _viewInvoker;
        [CanBeNull] private LinkTargetCollection _targetCollection;
        [CanBeNull] private LinkEffectApplier _effectApplier;
        [CanBeNull] private LinkEffectApplierFactory _effectApplierFactory;

        private readonly Dictionary<UnitNetwork, IChildLinkSpawner> _spawners = new();

        private int _currentEffectKey = DefaultEffectKey;

        public LinkSpawnerConfig Config
        {
            get => _config;
            set
            {
                if (_config != value)
                {
                    _config = value;
                    ConfigChanged?.Invoke();
                }
            }
        }

        GameObject IGameObject.GameObject => this != null? gameObject : null;

        [Inject]
        private void MonoConstructor(IPrefabFactory prefabFactory,
            UnitEffectStateFactory effectStateFactory, ILogger<LinkSpawner> logger)
        {
            _logger = logger;
            _factory = new LinkSpawnerFactory(this, prefabFactory);
            _effectApplierFactory = new LinkEffectApplierFactory(effectStateFactory, logger);
        }

        private void OnDestroy() => FinishEffect();

        public UniTask StartEffect(UnitNetwork host, IMasterLinkSpawner mainSpawner)
        {
            if (IsActive)
                FinishEffect();

            _mainSpawner = mainSpawner;

            _host = host;
            SubscribeHost(host);

            _viewInvoker ??= host.GetComponentInChildren<ILinkSpawnerViewInvoker>();
            _effectApplier = _effectApplierFactory!.Create(host);
            _targetCollection = new LinkTargetCollection(_logger!, host,
                () => _mainSpawner.LinkedUnits, ObjectCatcher, Config.FilterParams);

            InitTargets(_targetCollection);
            SubscribeCollection(_targetCollection);

            IsActive = true;

            _mainSpawner.UpdateActualEffects();
            _viewInvoker?.StartSpawner(Config.Radius, Config.Duration, this);

            Started?.Invoke();
            _logger!.Log($"Started spawning for {host.name}");

            _cts = new CancellationTokenSource();

            return UniTask.Delay(Config.Duration,
                    cancellationToken: _cts.Token)
                .ContinueWith(FinishEffect);
        }

        public void FinishEffect()
        {
            if (_cts != null)
            {
                _cts.Cancel();
                _cts.Dispose();
                _cts = null;
            }

            if (_host != null)
            {
                UnsubscribeHost(_host);
            }

            if (_targetCollection != null)
            {
                UnsubscribeCollection(_targetCollection);
                DeinitTargets(_targetCollection);
                _targetCollection.Dispose();
                _targetCollection = null;
            }

            if (_spawners.Count > 0)
            {
                UnitNetwork[] targets = _spawners.Keys.ToArray();
                foreach (UnitNetwork target in targets) FinishSpawnerFor(target);
                _spawners.Clear();
            }

            if (_effectApplier != null)
            {
                _effectApplier.Dispose();
                _effectApplier = null;
            }

            if (IsActive)
            {
                IsActive = false;
                _mainSpawner?.UpdateActualEffects();
                _viewInvoker?.FinishSpawner();
                _logger!.Log(_host ? $"Finished spawning for {_host.name}" : "Finished spawning");

                Finished?.Invoke();
            }
        }

        private void InitTargets([NotNull] LinkTargetCollection targetCollection)
        {
            foreach (UnitNetwork unitNetwork in targetCollection)
                if (unitNetwork != null) OnAdded(unitNetwork);
        }

        private void DeinitTargets([NotNull] LinkTargetCollection targetCollection)
        {
            foreach (UnitNetwork unitNetwork in targetCollection)
                if (unitNetwork != null) OnRemoved(unitNetwork);
        }

        private void SubscribeHost([NotNull] UnitNetwork host)
        {
            host.Destroyed.AddListener(FinishEffect);
            host.UnitNetworkState.HealthChanged += OnHostHealthChanged;
        }

        private void UnsubscribeHost([NotNull] UnitNetwork host)
        {
            host.Destroyed.RemoveListener(FinishEffect);
            host.UnitNetworkState.HealthChanged -= OnHostHealthChanged;
        }

        private void SubscribeCollection([NotNull] LinkTargetCollection targetCollection)
        {
            targetCollection.Added += OnAdded;
            targetCollection.Removed += OnRemoved;
        }

        private void UnsubscribeCollection([NotNull] LinkTargetCollection targetCollection)
        {
            targetCollection.Added -= OnAdded;
            targetCollection.Removed -= OnRemoved;
        }

        private void OnHostHealthChanged(float diff)
        {
            if (_host != null && _host.UnitNetworkState.IsDead)
                FinishEffect();
        }

        private void OnAdded([NotNull] UnitNetwork unitNetwork)
        {
            if (Config.DistributingConfig != null && _mainSpawner != null)
            {
                CreateSpawnerFor(unitNetwork, _mainSpawner, Config.DistributingConfig);
                _logger!.Log($"Start distributing from {_host!.name} to {unitNetwork.name}");
            }
        }

        private void OnRemoved([NotNull] UnitNetwork unitNetwork)
        {
            if (_spawners.ContainsKey(unitNetwork))
            {
                FinishSpawnerFor(unitNetwork);
                _logger!.Log($"Stop distributing from {_host!.name} to {unitNetwork.name}");
            }
        }

        private void CreateSpawnerFor([NotNull] UnitNetwork unitNetwork,
            [NotNull] IMasterLinkSpawner mainSpawner, [NotNull] LinkSpawnerConfig config)
        {
            LinkSpawner linkSpawner = _factory!.CreateSpawner(unitNetwork, config);
            _spawners.Add(unitNetwork, linkSpawner);
            _logger!.Log($"Created spawner {config} for {unitNetwork.name}");

            linkSpawner.StartEffect(unitNetwork, mainSpawner);
            _viewInvoker!.SetLink(_host!.Identity, unitNetwork.Identity);
        }

        private void FinishSpawnerFor([NotNull] UnitNetwork unitNetwork)
        {
            IChildLinkSpawner spawner = _spawners[unitNetwork];
            _spawners.Remove(unitNetwork);
            spawner.FinishEffect();

            Destroy(spawner.GameObject);

            _logger!.Log($"Removed spawner {spawner.Config.name} for {unitNetwork.name}");
            _viewInvoker!.RemoveLink(_host!.Identity, unitNetwork.Identity);
        }

        void IMasterLinkSpawner.UpdateActualEffects()
        {
            if (_effectApplier == null) return;

            int effectKey = CalcEffectKey(LinkRank);

            if (_currentEffectKey != effectKey)
            {
                _currentEffectKey = effectKey;
                _logger!.Log($"Updated current effectKey to {_currentEffectKey}");

                if (!Config.Effects.TryGetValue(effectKey, out IReadOnlyList<UnitEffectConfig> effects))
                    effects = Array.Empty<UnitEffectConfig>();

                UpdateActualEffects(_effectApplier, effects);
            }
        }

        void IChildLinkSpawner.UpdateActualEffects(IReadOnlyList<UnitEffectConfig> effects)
        {
            if (_effectApplier != null)
                UpdateActualEffects(_effectApplier, effects);
        }

        private void UpdateActualEffects(
            [NotNull] LinkEffectApplier effectApplier,
            [NotNull, ItemNotNull] IReadOnlyList<UnitEffectConfig> effects)
        {
            effectApplier.RemoveAllAppliedEffects();
            effectApplier.ApplyEffects(effects);

            foreach (IChildLinkSpawner spawner in _spawners.Values)
                spawner.UpdateActualEffects(effects);
        }

        private int CalcEffectKey(int targetCount)
        {
            int? key = null;
            int index = default;
            int[] keys = Config.Effects.Keys.OrderBy(k => k).ToArray();

            while (index < keys.Length && targetCount >= keys[index])
            {
                key = keys[index];
                index++;
            }

            return key ?? DefaultEffectKey;
        }
    }
}