using System;
using Mirror;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    public sealed class LinkEffectApplier : IDisposable
    {
        [CanBeNull] private readonly IUnitEffectReceiver _target;
        [NotNull] private readonly UnitEffectStateFactory _effectStateFactory;
        [NotNull] private readonly ILogger _logger;

        [NotNull] private readonly HashSet<UnitEffectState> _appliedEffects = new();

        public LinkEffectApplier([CanBeNull] IUnitEffectReceiver target,
            [NotNull] UnitEffectStateFactory effectStateFactory, [NotNull] ILogger logger)
        {
            _target = target;
            _logger = logger;
            _effectStateFactory = effectStateFactory;
        }

        public void Dispose() =>
            RemoveAllAppliedEffects();

        public void ApplyEffects([NotNull, ItemNotNull] IEnumerable<UnitEffectConfig> effects)
        {
            if (_target == null) return;
            if (_target.Identity == null) return;

            foreach (UnitEffectConfig effect in effects)
                if (_target.CanReceiveUnitEffect(effect))
                {
                    UnitEffectState effectState = _effectStateFactory.Create(effect, _target.Identity);
                    _target.AddUnitEffect(effectState);
                    _appliedEffects.Add(effectState);

                    _logger!.Log($"Applied effect {effect.name} for {_target.Identity.name}");
                }
        }

        public void RemoveAllAppliedEffects()
        {
            if (_target != null)
                foreach (UnitEffectState effect in _appliedEffects)
                    if (_target.HasUnitEffect(effect))
                    {
                        _target.RemoveUnitEffect(effect);
                        _logger!.Log($"Removed effect {effect.Config!.name} for {_target.Identity.name}");
                    }

            _appliedEffects.Clear();
        }
    }
}
