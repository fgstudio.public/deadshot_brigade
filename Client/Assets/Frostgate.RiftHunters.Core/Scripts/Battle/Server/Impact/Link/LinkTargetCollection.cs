using UnityEngine;
using JetBrains.Annotations;
using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Filters;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/fa2f1c562cb9488fbbafe2c04b2ea6c4"/>
    /// </summary>
    public sealed class LinkTargetCollection : IEnumerable<UnitNetwork>, IDisposable
    {
        public event Action<UnitNetwork> Added;
        public event Action<UnitNetwork> Removed;

        [NotNull] private readonly ILogger _logger;
        [NotNull] private readonly ObjectCatcher _objectCatcher;

        [CanBeNull] private readonly UnitNetwork _host;
        [CanBeNull] private readonly UnitFilterParams _filter;

        [NotNull] private readonly HashSet<UnitNetwork> _targets;
        [NotNull] private readonly Func<IEnumerable<UnitNetwork>> _linkedTargets;

        public LinkTargetCollection(
            [NotNull] ILogger logger, [NotNull] UnitNetwork host,
            [NotNull] Func<IEnumerable<UnitNetwork>> linkedTargets,
            [NotNull] ObjectCatcher objectCatcher, [CanBeNull] UnitFilterParams filter)
        {
            _host = host;
            _linkedTargets = linkedTargets;
            _logger = logger;
            _objectCatcher = objectCatcher;
            _filter = filter;

            _targets = new HashSet<UnitNetwork>();
            foreach (Collider o in _objectCatcher.Objects) OnCaught(o);

            Subscribe(_objectCatcher);
        }

        public void Dispose()
        {
            Unsubscribe(_objectCatcher);

            UnitNetwork[] unitNetworks = _targets.ToArray();
            foreach (UnitNetwork unitNetwork in unitNetworks) RemoveTarget(unitNetwork);

            _targets.Clear();
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public IEnumerator<UnitNetwork> GetEnumerator() => _targets.GetEnumerator();

        private void Subscribe([NotNull] ObjectCatcher objectCatcher)
        {
            objectCatcher.Caught.AddListener(OnCaught);
            objectCatcher.Lost.AddListener(OnLost);
        }

        private void Unsubscribe([NotNull] ObjectCatcher objectCatcher)
        {
            objectCatcher.Caught.RemoveListener(OnCaught);
            objectCatcher.Lost.RemoveListener(OnLost);
        }

        private void OnCaught(Collider obj)
        {
            _logger.Log($"Caught target {obj.name}");

            if (_host == null) return;
            // TODO: получать из коллекции
            if (!obj.TryGetComponentInHierarchy(out UnitNetwork unitNetwork)) return;
            if (_linkedTargets().Contains(unitNetwork)) return;

            if (_filter == null || _filter.IsMatching(unitNetwork))
                if (!_targets.Contains(unitNetwork))
                    AddTarget(unitNetwork);
        }

        private void OnLost(Collider obj)
        {
            _logger.Log($"Lost target {obj.name}");

            // TODO: получать из коллекции
            if (!obj.TryGetComponentInHierarchy(out UnitNetwork unitNetwork)) return;

            if (_targets.Contains(unitNetwork))
                RemoveTarget(unitNetwork);
        }

        private void AddTarget([NotNull] UnitNetwork unitNetwork)
        {
            _targets.Add(unitNetwork);
            Added?.Invoke(unitNetwork);

            _logger.Log($"Added to collection {unitNetwork.name}");
        }

        private void RemoveTarget([NotNull] UnitNetwork unitNetwork)
        {
            _targets.Remove(unitNetwork);
            Removed?.Invoke(unitNetwork);

            _logger.Log($"Removed from collection {unitNetwork.name}");
        }
    }
}