using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/fa2f1c562cb9488fbbafe2c04b2ea6c4"/>
    /// </summary>
    public sealed class LinkSpawnerFactory
    {
        [NotNull] private readonly LinkSpawner _prefab;
        [NotNull] private readonly IPrefabFactory _prefabFactory;

        public LinkSpawnerFactory(
            [NotNull] LinkSpawner prefab,
            [NotNull] IPrefabFactory prefabFactory)
        {
            _prefab = prefab;
            _prefabFactory = prefabFactory;
        }

        [NotNull]
        public LinkSpawner CreateSpawner([NotNull] UnitNetwork target, [NotNull] LinkSpawnerConfig config)
        {
            var linkSpawner = _prefabFactory.Instantiate<LinkSpawner>(_prefab, target.transform);
            linkSpawner.Config = config;

            return linkSpawner;
        }
    }
}