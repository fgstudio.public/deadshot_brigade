using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Server.Impact
{
    public sealed class LinkEffectApplierFactory
    {
        [NotNull] private readonly UnitEffectStateFactory _effectStateFactory;
        [NotNull] private readonly ILogger _logger;

        public LinkEffectApplierFactory(
            [NotNull] UnitEffectStateFactory effectStateFactory,
            [NotNull] ILogger logger)
        {
            _effectStateFactory = effectStateFactory;
            _logger = logger;
        }

        public LinkEffectApplier Create([NotNull] IUnitEffectReceiver effectReceiver) =>
            new(effectReceiver, _effectStateFactory, _logger);
    }
}
