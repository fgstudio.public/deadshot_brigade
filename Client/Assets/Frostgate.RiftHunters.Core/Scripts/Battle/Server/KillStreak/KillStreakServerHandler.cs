﻿using System;
using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Network.Server;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Network.Shared.KillStreak;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Scripts.Network.Server.KillStreak
{
    public sealed class KillStreakServerHandler : IDisposable
    {
        [NotNull] private readonly SimulationTime _simulationTime;
        [NotNull] private readonly ILogger _logger;

        [NotNull] private readonly KillStreakConfig _config;
        [NotNull] private readonly IDamageService _damageService;
        [NotNull] private readonly KillStreakState _streakState;
        [NotNull] private readonly StreakBonusMediator _bonusMediator;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;

        private readonly ITimer _streakTimer;

        public KillStreakServerHandler(
            [NotNull] IDamageService damageService,
            [NotNull] SimulationTime simulationTime,
            [NotNull] ILogger logger,
            [NotNull] KillStreakState streakState,
            [NotNull] KillStreakConfig config,
            [NotNull] StreakBonusMediator bonusMediator,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _simulationTime = simulationTime;
            _bonusMediator = bonusMediator;
            _logger = logger;

            _unitCollection = unitCollection;
            _damageService = damageService;
            _streakState = streakState;
            _config = config;

            _streakTimer = new Timer(config.StreakCooldown);

            _streakTimer.Elapsed += OnTimerElapsed;
            _damageService.Died.AddListener(OnDied);

            _logger.Log("Initialized");
        }

        public void Dispose()
        {
            _streakTimer.Elapsed -= OnTimerElapsed;
            _damageService.Died.RemoveListener(OnDied);

            _streakTimer?.Dispose();
        }

        private void OnDied(DiedEventData eventData)
        {
            if (_unitCollection.TryGet(eventData.DiedId, out UnitNetwork diedUnit) &&
                diedUnit != null && diedUnit.UnitType == BattleUnitType.Bot)
                UpdateStreakDuration(diedUnit);
        }

        private void OnTimerElapsed()
        {
            _logger.Log($"Total kill streak: {_streakState.StreakCount}. Streak finished!");
            _streakState.ResetStreak();
        }

        private void UpdateStreakDuration(UnitNetwork unit)
        {
            _streakTimer.Stop();

            _streakState.AddCount(unit.netId, _simulationTime.Time);
            if (_streakState.StreakCount % _config.KillsForBonus == 0)
                CreateBonus(unit.Identity);
            _logger.Log($"Kill streak: {_streakState.StreakCount}");

            _streakTimer.Start();
        }

        private void CreateBonus(NetworkIdentity bonusSource)
        {
            StreakBonusConfig bonusConfig = _config.BonusConfigs.GetRandom();
            _bonusMediator.CreateBonus(bonusConfig, bonusSource);

            _logger.Log($"Create bonus for [netId:{bonusSource.netId}] " +
                        $"from config [Id:{bonusConfig.Id}]");
        }
    }
}