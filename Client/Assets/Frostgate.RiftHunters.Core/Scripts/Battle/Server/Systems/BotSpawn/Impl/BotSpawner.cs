﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Network.Server;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    // TODO: сейчас этот класс обрабатывает все волны в спаунере. Нужно сделать сущность по обработке толькой одной волны
    public sealed class BotSpawner
    {
        private readonly string _spawnerId;
        [NotNull] private readonly SimulationTime _time;
        [NotNull] private readonly IBotWaveSource _waveSource;
        [NotNull] private readonly IEditableBotSpawnState _state;
        [NotNull] private readonly IBotSpawnFacade _botSpawnFacade;
        [NotNull] private readonly ISpawnPositionProvider _spawnPositionProvider;
        [CanBeNull] private readonly IPositionProvider _homePositionProvider;

        [NotNull] private readonly BotsInWave _spawnedBots = new();
        [NotNull] private readonly CancellationTokenSource _lifetimeCts = new();

        private UniTask _spawningTask = UniTask.CompletedTask;

        [CanBeNull] private CancellationTokenSource _spawnCts;

        public bool IsActive { get; private set; }
        public UniTask Task => UniTask.WaitWhile(() => IsActive);

        public BotSpawner(
            string spawnerId, [NotNull] SimulationTime time, [NotNull] IBotWaveSource waveSource,
            [NotNull] IEditableBotSpawnState state, [NotNull] IBotSpawnFacade botSpawnFacade,
            [NotNull] ISpawnPositionProvider spawnPositionProvider,
            [CanBeNull] IPositionProvider homePositionProvider = null)
        {
            _state = state;
            _time = time;
            _waveSource = waveSource;
            _spawnerId = spawnerId;
            _botSpawnFacade = botSpawnFacade;
            _spawnPositionProvider = spawnPositionProvider;
            _homePositionProvider = homePositionProvider;
        }

        [Server]
        public void Dispose()
        {
            _lifetimeCts.Cancel();
            _lifetimeCts.Dispose();

            CancelSpawn();
            _spawnPositionProvider.Reset();
        }

        [Server]
        public UniTask StartSpawn()
        {
            if (IsActive)
                throw new InvalidOperationException(
                    "Can't start new routine while the current one is active!");

            _spawnCts = new CancellationTokenSource();
            _spawningTask = SpawningRoutine(_spawnCts.Token);
            return _spawningTask;
        }

        [Server]
        public UniTask CancelSpawn()
        {
            UnspawnBots();

            if (!IsActive)
                _state.SetFinished(_spawnerId, CompletionStatus.Cancelled);

            return StopSpawn();
        }

        [Server]
        public UniTask StopSpawn()
        {
            if (_spawnCts != null)
            {
                _spawnCts.Cancel();
                _spawnCts.Dispose();
                _spawnCts = null;
            }

            return IsActive ? Task : UniTask.CompletedTask;
        }

        private async UniTask SpawningRoutine(CancellationToken token)
        {
            IsActive = true;
            int waveCounter = 0;

            if (_waveSource.StartupDelay.Ticks > 0)
            {
                _state.SetAwaiting(_spawnerId, _time.Time);
                await UniTask.Delay(_waveSource.StartupDelay, cancellationToken: token)
                    .SuppressCancellationThrow();
            }

            while (_waveSource.MoveNext() && !token.IsCancellationRequested)
            {
                waveCounter++;
                BotWave botWave = _waveSource.Current;
                int maxUnitsCount = _waveSource.MaxUnitsAtTheMoment;

                _spawnPositionProvider.Reset();

                if (!token.IsCancellationRequested)
                    await SpawnBotsInWave(botWave, waveCounter, maxUnitsCount, token)
                        .SuppressCancellationThrow();

                if (!token.IsCancellationRequested)
                    await WaitWaveEnd(botWave, token)
                        .SuppressCancellationThrow();

                if (!token.IsCancellationRequested && botWave.Delay.Ticks > 0)
                {
                    _state.SetAwaiting(_spawnerId, _time.Time);
                    await UniTask.Delay(botWave.Delay, cancellationToken: token)
                        .SuppressCancellationThrow();
                }
            }

            if (!token.IsCancellationRequested)
                await UniTask.WaitUntil(AreBotsKilled, cancellationToken: token)
                    .SuppressCancellationThrow();

            CompletionStatus completionStatus =
                token.IsCancellationRequested
                    ? CompletionStatus.Cancelled
                    : CompletionStatus.Success;

            _state.SetFinished(_spawnerId, completionStatus);

            if (_waveSource.AutoUnspawnBots)
                UnspawnBots();

            _waveSource.Reset();
            IsActive = false;
        }

        private async UniTask WaitWaveEnd(BotWave wave, CancellationToken token)
        {
            if (!token.IsCancellationRequested &&
                wave.WaveEndCondition.HasFlag(WaveEndCondition.WaitTime) && wave.WaveEndTime.Ticks > 0)
                await UniTask.Delay(wave.WaveEndTime, cancellationToken: token);

            if (!token.IsCancellationRequested && wave.WaveEndCondition.HasFlag(WaveEndCondition.KillAll))
                await UniTask.WaitUntil(AreBotsKilled, cancellationToken: token);
        }

        private bool AreBotsKilled() =>
            !_spawnedBots.IsWaveExist || _spawnedBots.IsWaveKilled;

        private async UniTask SpawnBotsInWave(BotWave wave, int waveIndex,
            int maxUnitsCount, CancellationToken token)
        {
            const int delayFramesCount = 10;

            int currentUnitsCount = 0;
            int countInWave = wave.BotConfigs.Count;

            while (!token.IsCancellationRequested && currentUnitsCount < countInWave)
            {
                int aliveCount = _spawnedBots.AliveBotsCount;
                int countToSpawn = Mathf.Min(
                    maxUnitsCount, maxUnitsCount - aliveCount, countInWave - currentUnitsCount);

                _state.SetSpawning(_spawnerId, waveIndex);

                IEnumerable<UnitConfig> botsToSpawn = wave.BotConfigs
                    .Skip(currentUnitsCount).Take(countToSpawn);

                foreach (UnitConfig config in botsToSpawn)
                {
                    UnitNetwork bot = null;

                    while (!token.IsCancellationRequested && !TrySpawnBot(config, out bot))
                    {
                        TimeSpan retryDelay = TimeSpan.FromSeconds(0.3f);
                        await UniTask.Delay(retryDelay, cancellationToken: token);
                    }

                    if (!token.IsCancellationRequested)
                    {
                        _spawnedBots.Add(bot);
                        _state.SetSpawning(_spawnerId, waveIndex);
                        _state.SetBotsCount(_spawnerId,
                            _spawnedBots.DeadBotsCount, _spawnedBots.TotalBotsCount);

                        currentUnitsCount++;
                        await UniTask.Yield();
                    }
                }

                if (!token.IsCancellationRequested)
                    await UniTask.DelayFrame(delayFramesCount, cancellationToken: token);
            }
        }

        private bool TrySpawnBot([NotNull] UnitConfig botConfig, out UnitNetwork bot)
        {
            bot = null;

            if (!_spawnPositionProvider.TryGetPosition(out Vector3 position, out Vector3 homePosition))
                return false;

            IPositionProvider homePositionProvider = _homePositionProvider ?? new StaticPositionProvider(homePosition);
            bot = _botSpawnFacade.SpawnEnemyBot(botConfig, position, homePositionProvider);

            SubscribeBot(bot);

            return true;
        }

        private void UnspawnBots()
        {
            _spawnedBots.Bots
                .Where(b => b != null)
                .ForEach(UnspawnBot);

            _spawnedBots.Clear();
            _state.SetBotsCount(_spawnerId, default, default);
        }

        private void UnspawnBot([NotNull] UnitNetwork bot)
        {
            UnsubscribeBot(bot);
            _botSpawnFacade.DestroyBot(bot);

            _state.SetBotsCount(_spawnerId,
                _spawnedBots.DeadBotsCount, _spawnedBots.TotalBotsCount);
        }

        private void SubscribeBot([NotNull] UnitNetwork bot) => bot.UnitNetworkState.Died += OnBotDied;
        private void UnsubscribeBot([NotNull] UnitNetwork bot) => bot.UnitNetworkState.Died -= OnBotDied;

        private async void OnBotDied([CanBeNull] UnitNetworkState state)
        {
            CancellationToken token = _lifetimeCts.Token;
            TimeSpan delayBeforeUnspawn = TimeSpan.FromSeconds(10);

            await UniTask.Delay(delayBeforeUnspawn, cancellationToken: token)
                .SuppressCancellationThrow();

            if (!token.IsCancellationRequested && state?.UnitNetwork != null)
                UnspawnBot(state.UnitNetwork);
        }
    }
}