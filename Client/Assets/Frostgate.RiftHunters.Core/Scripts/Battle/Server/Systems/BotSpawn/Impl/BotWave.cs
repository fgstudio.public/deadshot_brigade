﻿using System;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    public readonly struct BotWave
    {
        public readonly TimeSpan Delay;

        public readonly WaveEndCondition WaveEndCondition;
        public readonly TimeSpan WaveEndTime;

        public readonly IReadOnlyList<UnitConfig> BotConfigs;

        public BotWave([NotNull] IReadOnlyList<UnitConfig> botConfigs, TimeSpan delay,
            WaveEndCondition waveEndCondition, TimeSpan waveEndTime)
        {
            Delay = delay;

            WaveEndCondition = waveEndCondition;
            WaveEndTime = waveEndTime;

            BotConfigs = botConfigs;
        }
    }
}
