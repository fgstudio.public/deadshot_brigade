using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    public sealed class BotWaveEnumerator
    {
        private const int DefaultWaveIndex = -1;

        [NotNull] private readonly BotSpawnerConfig _spawnerConfig;

        [CanBeNull] private IncreasingCounter _counter;
        [CanBeNull] private IList<BotWavePerDdaLevel> _ddaSetups;

        private int _waveIndex = DefaultWaveIndex;

        public BotWaveEnumerator([NotNull] BotSpawnerConfig spawnerConfig) =>
            _spawnerConfig = spawnerConfig;

        public void Dispose()
        {
            _waveIndex = DefaultWaveIndex;
            _counter = null;
        }

        public bool MoveNext()
        {
            if (_spawnerConfig.Waves.Count == 0)
                return false;

            if (_counter is { IsElapsed: false })
            {
                _counter.Increase();
                return true;
            }

            if (_waveIndex == _spawnerConfig.Waves.Count - 1)
            {
                if (!_spawnerConfig.IsEndless)
                    return false;

                _waveIndex = DefaultWaveIndex;
            }

            _waveIndex++;
            _counter = new IncreasingCounter(_spawnerConfig.Waves[_waveIndex].RepeatsCount);
            _ddaSetups = _spawnerConfig.Waves[_waveIndex].DdaSetups.OrderBy(s => s.DdaLevel).ToArray();
            _counter.Increase();

            return true;
        }

        public void Reset()
        {
            _waveIndex = DefaultWaveIndex;
            _counter = null;
        }

        public BotWave GetCurrent(int ddaLevel)
        {
            BotWaveConfig wave = _spawnerConfig.Waves[_waveIndex];
            return new BotWave(botConfigs: GetUnitSetupByDdaLevel(ddaLevel),
                wave.Delay, wave.WaveEndCondition, wave.WaveEndTime);
        }

        private IReadOnlyList<UnitConfig> GetUnitSetupByDdaLevel(int ddaLevel)
        {
            for (var i = 1; i < _ddaSetups!.Count; i++)
            {
                if (_ddaSetups![i - 1].DdaLevel == ddaLevel)
                    return _ddaSetups[i - 1].BotConfigs;

                if (_ddaSetups[i - 1].DdaLevel < ddaLevel && _ddaSetups[i].DdaLevel > ddaLevel)
                    return _ddaSetups[i - 1].BotConfigs;
            }

            return _ddaSetups[^1].BotConfigs;
        }
    }
}
