﻿namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    public sealed class IncreasingCounter
    {
        public int Limit { get; }
        public int Current { get; private set; }
        public bool IsElapsed => Current >= Limit;

        public IncreasingCounter(int limit) =>
            Limit = limit;

        public void Increase() => Current++;
        public void Reset() => Current = default;
    }
}
