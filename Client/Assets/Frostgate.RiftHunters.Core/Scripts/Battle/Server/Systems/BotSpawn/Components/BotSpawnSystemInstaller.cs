﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    [HelpURL("https://www.notion.so/frostgate/59ba6d9bdc3c4504b5787e636ec174ec")]
    [AddComponentMenu(BattleComponentMenus.Server.Systems.Menu + "/" + nameof(BotSpawnSystemInstaller))]
    [DisallowMultipleComponent]
    public sealed class BotSpawnSystemInstaller : MonoInstaller
    {
        [SerializeField, Required, ChildGameObjectsOnly] private BotSpawnState _spawnState;
        [SerializeField, Required, ChildGameObjectsOnly] private BotArenaState _arenaState;
        [SerializeField, Required, ChildGameObjectsOnly] private BotArenaSceneConfiguration _arenaSceneConfiguration;

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<BotSpawnState>().FromInstance(_spawnState);
            Container.BindInterfacesAndSelfTo<BotArenaState>().FromInstance(_arenaState);
            Container.Bind<BotArenaSceneConfiguration>().FromInstance(_arenaSceneConfiguration);

            Container.Bind<SpawnAreaFinder>().AsSingle();
            Container.BindInterfacesTo<BotArenaCollection>().AsSingle();
        }
    }
}