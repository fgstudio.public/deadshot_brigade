﻿using Frostgate.RiftHunters.Core.Systems;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    public readonly struct PointRandomizer
    {
        private readonly Vector3 _center;
        private readonly float _radius;
        private readonly AreaRandom _random;

        private float Diameter => _radius * 2;

        public PointRandomizer(Vector3 center, float radius, [NotNull] AreaRandom random)
        {
            _center = center;
            _radius = radius;
            _random = random;
        }

        public Vector3 Randomize() =>
            _random.RectY(_center, Diameter, Diameter);
    }
}