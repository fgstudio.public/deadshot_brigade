﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    public interface IBotWaveSource : IEnumerator<BotWave>
    {
        bool AutoUnspawnBots { get; }
        TimeSpan StartupDelay { get; }
        int MaxUnitsAtTheMoment { get; }
    }

    // Очень душный энумератор для инкапсуляции доступа к элементам "матрицы" как к одномерному массиву
    public sealed class BotWaveSource : IBotWaveSource, IDdaService
    {
        public bool AutoUnspawnBots { get; }
        public int MaxUnitsAtTheMoment { get; }
        public TimeSpan StartupDelay { get; }

        public BotWave Current => _enumerator.GetCurrent(_ddaLevel);
        object IEnumerator.Current => Current;

        [NotNull] private readonly BotWaveEnumerator _enumerator;

        [CanBeNull] private IEnumerator<BotWaveConfig> _repetitionEnumerator;
        [CanBeNull] private IList<BotWavePerDdaLevel> _ddaSetups;

        private int _ddaLevel;

        public BotWaveSource([NotNull] BotSpawnerConfig spawnerConfig)
        {
            _enumerator = new BotWaveEnumerator(spawnerConfig);

            StartupDelay = spawnerConfig.StartupDelay;
            AutoUnspawnBots = spawnerConfig.AutoUnspawnBots;
            MaxUnitsAtTheMoment = spawnerConfig.MaxUnitsAtTheMoment;
        }

        public void Dispose() => _enumerator.Dispose();

        public void OnRegistered() { }
        public void OnUnregistered() { }

        public void SetLevel(int level) => _ddaLevel = level;
        public bool MoveNext() => _enumerator.MoveNext();
        public void Reset() => _enumerator.Reset();
    }
}
