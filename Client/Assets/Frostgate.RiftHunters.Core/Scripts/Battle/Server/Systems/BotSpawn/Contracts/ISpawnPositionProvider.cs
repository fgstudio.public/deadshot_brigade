﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    public interface ISpawnPositionProvider
    {
        bool TryGetPosition(out Vector3 position, out Vector3 homePosition);
        void Reset();
    }
}