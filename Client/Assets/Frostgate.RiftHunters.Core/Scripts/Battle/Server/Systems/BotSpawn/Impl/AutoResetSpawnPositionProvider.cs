using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    /// <summary>
    /// СБрасывает провайдер позиций, если найти позицию не удалось,
    /// и производит поиск повторно.
    /// </summary>
    public sealed class AutoResetSpawnPositionProvider : ISpawnPositionProvider
    {
        [NotNull] private readonly ISpawnPositionProvider _provider;

        public AutoResetSpawnPositionProvider([NotNull] ISpawnPositionProvider provider) =>
            _provider = provider;

        public bool TryGetPosition(out Vector3 position, out Vector3 homePosition)
        {
            if (_provider.TryGetPosition(out position, out homePosition))
                return true;

            _provider.Reset();
            return _provider.TryGetPosition(out position, out homePosition);
        }

        public void Reset() => _provider.Reset();
    }
}
