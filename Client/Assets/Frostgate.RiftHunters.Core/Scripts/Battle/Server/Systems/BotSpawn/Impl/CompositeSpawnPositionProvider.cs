﻿using UnityEngine;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    public sealed class CompositeSpawnPositionProvider : ISpawnPositionProvider
    {
        [NotNull] private readonly IEnumerator<ISpawnPositionProvider> _providersEnumerator;

        public CompositeSpawnPositionProvider([NotNull] IEnumerable<ISpawnPositionProvider> providers) =>
            _providersEnumerator = providers.GetEnumerator();

        public bool TryGetPosition(out Vector3 position, out Vector3 homePosition) =>
            GetNextProvider().TryGetPosition(out position, out homePosition);

        public void Reset()
        {
            _providersEnumerator.Reset();

            while(_providersEnumerator.MoveNext())
                _providersEnumerator.Current?.Reset();

            _providersEnumerator.Reset();
        }

        private ISpawnPositionProvider GetNextProvider()
        {
            if (!_providersEnumerator.MoveNext())
            {
                _providersEnumerator.Reset();
                _providersEnumerator.MoveNext();
            }

            return _providersEnumerator.Current;
        }
    }
}
