﻿using UnityEngine;
using UnityEngine.AI;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    /// <summary>
    /// Кэширует ранее занятые позиции для спауна.
    /// Подходит для спауна группы объектов единовременно и одноразово.
    /// Для спауна новой группы следует вызвать Reset.
    /// </summary>
    public class CachedSpawnPositionProvider : ISpawnPositionProvider
    {
        private const float MaxDistanceToNavmesh = 1f;
        private const int IterationCutoffCount = 50;

        private readonly Vector3 _center;
        private readonly float _radius;
        private readonly Vector3 _homeOffset;
        private readonly float _distanceBetweenBots;
        private readonly ILogger _logger;
        private readonly PointRandomizer _pointRandomizer;
        private readonly ICollection<Vector3> _usedPositions = new HashSet<Vector3>();

        public CachedSpawnPositionProvider(
            [NotNull] AreaRandom random,
            [NotNull] ILogger logger,
            Vector3 center, float radius,
            float distanceBetweenBots, Vector3 homeOffset)
        {
            _logger = logger;
            _center = center;
            _radius = radius;
            _homeOffset = homeOffset;

            _distanceBetweenBots = distanceBetweenBots;
            _pointRandomizer = new PointRandomizer(center, radius, random);
        }

        public void Reset() =>
            _usedPositions.Clear();

        public bool TryGetPosition(out Vector3 position, out Vector3 homePosition)
        {
            if (TryGeneratePosition(out position))
            {
                CachePosition(position);
                return TryCalcHomePosition(position, out homePosition);
            }
            else
            {
                homePosition = position;
                return false;
            }
        }

        private bool TryGeneratePosition(out Vector3 position)
        {
            int itCounter = 0;
            while (itCounter++ < IterationCutoffCount)
                if (TryFindPositionOnNavmesh(out position) && IsPositionValid(position))
                    return true;

            _logger.LogWarning(
                "Position cannot be generated on center " +
                $"'{_center}' with '{_radius}' radius.");

            position = default;
            return false;
        }

        private bool TryCalcHomePosition(Vector3 position, out Vector3 homePosition)
        {
            if (_homeOffset != Vector3.zero)
            {
                homePosition = position + _homeOffset;

                bool success = PositionHelper.TryCorrectSpawnPosition(homePosition, out homePosition, MaxDistanceToNavmesh);
                if (!success)
                    _logger.LogWarning($"{nameof(CachedSpawnPositionProvider)} incorrect homePosition");

                return success;
            }
            else
            {
                homePosition = position;
                return true;
            }
        }

        private bool TryFindPositionOnNavmesh(out Vector3 position)
        {
            Vector3 randomPoint = _pointRandomizer.Randomize();

            bool result = NavMesh.SamplePosition(randomPoint, out NavMeshHit hit,
                    MaxDistanceToNavmesh, NavMesh.AllAreas);

            position = hit.position;

            return result;
        }

        private bool IsPositionValid(Vector3 position) =>
            !IsPositionCached(position) && IsPositionDistanced(position);

        private bool IsPositionDistanced(Vector3 position)
        {
            float sqrMaxDistance = Mathf.Pow(_distanceBetweenBots, 2);

            foreach (Vector3 usedPos in _usedPositions)
                if (Vector3.SqrMagnitude(position - usedPos) < sqrMaxDistance)
                    return false;

            return true;
        }

        private void CachePosition(Vector3 position) => _usedPositions.Add(position);
        private bool IsPositionCached(Vector3 position) => _usedPositions.Contains(position);
    }
}