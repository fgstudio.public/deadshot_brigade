using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    public sealed class StubSpawnPositionProvider : ISpawnPositionProvider
    {
        public bool TryGetPosition(out Vector3 position, out Vector3 homePosition)
        {
            position = Vector3.zero;
            homePosition = Vector3.zero;
            return true;
        }

        public void Reset() { }
    }
}
