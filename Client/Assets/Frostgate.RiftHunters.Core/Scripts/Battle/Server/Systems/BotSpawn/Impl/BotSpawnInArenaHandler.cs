using System;
using System.Linq;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    public sealed class BotSpawnInArenaHandler : IDisposable
    {
        [NotNull] private readonly ILogger _logger;
        [NotNull] private readonly BotSpawner _spawner;
        [NotNull] private readonly BotSpawnerAction _activation;
        [NotNull] private readonly BotSpawnerAction _deactivation;
        [NotNull] private readonly IReadOnlyBotSpawnState _spawnState;

        public UniTask Task => _spawner.Task;

        public BotSpawnInArenaHandler(
            [NotNull] ILogger logger,
            [NotNull] BotSpawner spawner,
            [NotNull] BotSpawnerAction activation,
            [NotNull] BotSpawnerAction deactivation,
            [NotNull] IReadOnlyBotSpawnState spawnState)
        {
            _logger = logger;
            _spawner = spawner;
            _activation = activation;
            _deactivation = deactivation;
            _spawnState = spawnState;
        }

        public void Dispose() =>
            _spawner.Dispose();

        public void HandleArenaActivation()
        {
            if (_activation.Type == BotSpawnerActionType.ByArenaLifecycle)
                StartSpawn();

            if (_activation.Trigger != null)
            {
                _activation.Trigger.Triggered.AddListener(StartSpawn);
                if (_activation.Trigger.IsUsed) StartSpawn();
            }

            if (_deactivation.Trigger != null)
            {
                _deactivation.Trigger.Triggered.AddListener(StopSpawn);
                if (_deactivation.Trigger.IsUsed) StopSpawn();
            }

            if (_activation.Spawner != null || _deactivation.Spawner != null)
            {
                _spawnState.DataUpdated.AddListener(OnDataUpdated);
                _spawnState.Data.Keys.ToArray().ForEach(OnDataUpdated);
            }
        }

        public void HandleArenaComplete()
        {
            ResetTriggers();
            StopListeningState();
            StopListeningTriggers();

            StopSpawn();
        }

        public void HandleArenaDeactivation()
        {
            ResetTriggers();
            StopListeningState();
            StopListeningTriggers();

            // нужно отменить спаунер при любых условиях.
            // арена для рестарта ждёт завершения всех спаунеров.
            CancelSpawn();
        }

        private void OnDataUpdated(string spawnerId)
        {
            if (_spawnState.Data.TryGetValue(spawnerId, out BotSpawnerData spawnerData))
            {
                HandleAction(_activation, spawnerData, StartSpawn);
                HandleAction(_deactivation, spawnerData, StopSpawn);
            }
        }

        private void HandleAction(BotSpawnerAction action, BotSpawnerData spawnerData, Action success)
        {
            if (action.Spawner?.Config.Id == spawnerData.Id)
            {
                if (action.Type == BotSpawnerActionType.BySpawnerStart &&
                    spawnerData.Status == BotSpawnerStatus.Spawning)
                    success();

                if (action.Type == BotSpawnerActionType.BySpawnerFinish &&
                    spawnerData.Status == BotSpawnerStatus.Completed)
                    success();
            }
        }

        private void ResetTriggers()
        {
            _activation.Trigger?.Reset();
            _deactivation.Trigger?.Reset();
        }

        private void StopListeningTriggers()
        {
            _activation.Trigger?.Triggered.RemoveListener(StartSpawn);
            _deactivation.Trigger?.Triggered.RemoveListener(StopSpawn);
        }

        private void StopListeningState()
        {
            _spawnState.DataUpdated.RemoveListener(OnDataUpdated);
        }

        private async void StartSpawn()
        {
            try
            {
                if (!_spawner.IsActive)
                    await _spawner.StartSpawn();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        private async void StopSpawn()
        {
            try
            {
                await _spawner.StopSpawn();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        private async void CancelSpawn()
        {
            try
            {
                await _spawner.CancelSpawn();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }
    }
}