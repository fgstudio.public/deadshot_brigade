﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Server.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    // ReSharper disable Unity.NoNullPropagation
    public sealed class BotArena : IDisposable
    {
        [NotNull] private readonly IEditableBotArenaState _state;
        [CanBeNull] private readonly UnitEffectsArea _effectsArea;
        [CanBeNull] private readonly BotArenaPassageState _exitPassage;
        [NotNull] private readonly EnumerableEnumerator<IActivity> _activitiesEnumerator;
        [NotNull, ItemNotNull] private readonly IReadOnlyList<GameObject> _activeObjects;
        [NotNull, ItemNotNull] private readonly IReadOnlyList<BotSpawnInArenaHandler> _handlers;
        [NotNull] private readonly Checkpoint _respawnPoint;
        [NotNull] private readonly IActualRespawnPoint _actualRespawnPoint;

        public int ArenaIndex { get; }
        [NotNull] public BotArenaConfig Config { get; }

        public BotArenaStatus Status
        {
            get => _state.GetStatus(Config.Id);
            private set => _state.SetStatus(Config.Id, ArenaIndex, value);
        }

        public BotArena(
            int arenaIndex,
            [NotNull] BotArenaConfig config,
            [NotNull] IEditableBotArenaState state,
            [CanBeNull] UnitEffectsArea effectsArea,
            [CanBeNull] BotArenaPassageState exitPassage,
            [NotNull] EnumerableEnumerator<IActivity> activitiesEnumerator,
            [NotNull, ItemNotNull] IReadOnlyList<GameObject> activeObjects,
            [NotNull, ItemNotNull] IReadOnlyList<BotSpawnInArenaHandler> handlers,
            [NotNull] Checkpoint respawnPoint,
            [NotNull] IActualRespawnPoint actualRespawnPoint)
        {
            ArenaIndex = arenaIndex;
            Config = config;
            _state = state;
            _handlers = handlers;
            _exitPassage = exitPassage;
            _effectsArea = effectsArea;
            _activeObjects = activeObjects;
            _activitiesEnumerator = activitiesEnumerator;
            _respawnPoint = respawnPoint;
            _actualRespawnPoint = actualRespawnPoint;
            if (!_actualRespawnPoint.IsInitialized)
                _actualRespawnPoint.SetRespawnPoint(_respawnPoint);

            _activitiesEnumerator.ForEach(h => h.State.StatusChanged += OnSomeActivityStatusChanged);
            if (_activitiesEnumerator.All(h => h.State.Status == ActivityStatus.Completed))
                Complete();

            _activitiesEnumerator.MoveNext();
            _activeObjects.ForEach(o => o.SetActive(false));
        }

        public void Dispose()
        {
            _activitiesEnumerator.ForEach(h => h.State.StatusChanged -= OnSomeActivityStatusChanged);

            _handlers.ForEach(h =>
                h.Dispose());
        }

        public async UniTask Restart()
        {
            await Deactivate();
            await Activate();
        }

        public UniTask Activate()
        {
            if (Status == BotArenaStatus.Activated)
                return UniTask.CompletedTask;

            _actualRespawnPoint.SetRespawnPoint(_respawnPoint);

            Status = BotArenaStatus.Activated;

            _effectsArea?.State.SetActive(false);
            _exitPassage?.SetInPassage(BotArenaPassageState.PassageStatus.Allowed);
            _exitPassage?.SetOutPassage(BotArenaPassageState.PassageStatus.Blocked);

            _activeObjects.ForEach(o => o.SetActive(true));
            _handlers.ForEach(h => h.HandleArenaActivation());

            if (MoveToFirstNotCompleted(_activitiesEnumerator))
                _activitiesEnumerator.Current!.Activate();

            return UniTask.WhenAll(_handlers.Select(c => c.Task));
        }

        public UniTask Deactivate()
        {
            if (Status != BotArenaStatus.Activated)
                return UniTask.CompletedTask;

            Status = BotArenaStatus.Deactivated;

            _effectsArea?.State.SetActive(true);
            _exitPassage?.SetInPassage(BotArenaPassageState.PassageStatus.Blocked);
            _exitPassage?.SetOutPassage(BotArenaPassageState.PassageStatus.Allowed);

            _activeObjects.ForEach(o => o.SetActive(false));
            _handlers.ForEach(h => h.HandleArenaDeactivation());

            IActivity currentActivity = _activitiesEnumerator.Current;
            if (currentActivity is { State: { Status: ActivityStatus.Active } })
            {
                currentActivity.Deactivate();
                currentActivity.ResetProgress();
            }

            return UniTask.WhenAll(_handlers.Select(c => c.Task));
        }

        private void OnSomeActivityStatusChanged(IReadOnlyActivityState _, ActivityStatus __, ActivityStatus status)
        {
            if (status != ActivityStatus.Completed)
                return;

            if (MoveToFirstNotCompleted(_activitiesEnumerator))
                _activitiesEnumerator.Current!.Activate();
            else
                Complete();
        }

        private void Complete()
        {
            if (Status != BotArenaStatus.Activated)
                return;

            Status = BotArenaStatus.Completed;

            _effectsArea?.State.SetActive(true);
            _exitPassage?.SetInPassage(BotArenaPassageState.PassageStatus.Blocked);
            _exitPassage?.SetOutPassage(BotArenaPassageState.PassageStatus.Allowed);

            _activeObjects.ForEach(o => o.SetActive(false));
            _handlers.ForEach(h => h.HandleArenaComplete());
        }

        private bool MoveToFirstNotCompleted(IEnumerator<IActivity> activities)
        {
            while (_activitiesEnumerator.Current is { State: { Status: ActivityStatus.Completed } })
                _activitiesEnumerator.MoveNext();

            return _activitiesEnumerator.Current != null;
        }
    }
}