using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn
{
    // TODO: сделать в качестве ключа int
    [UsedImplicitly]
    public sealed class BotArenaCollection : ObjectBaseCollection<BotArenaConfig, BotArena>
    {
        public BotArenaCollection([NotNull] ILogger logger)
            : base(logger) { }

        protected override string ExtractName(BotArena value) =>
            value.Config != null ? value.Config.name : string.Empty;
    }
}