﻿using System;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA
{
    internal interface IDdaLevelReporter : IDisposable
    {
        int DifficultyLevel { get; }
        event Action LevelIncreased;
        event Action LevelDecreased;
    }
}
