﻿using System.Collections;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA
{
    internal class DdaServiceRepository : IDdaServiceRepository
    {
        public int Count => _services.Count;
        public bool IsReadOnly => _services.IsReadOnly;

        private readonly ICollection<IDdaService> _services = new HashSet<IDdaService>();

        public IEnumerator<IDdaService> GetEnumerator() => _services.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public void Add(IDdaService item) => _services.Add(item);
        public void Remove(IDdaService item) => _services.Remove(item);
    }
}
