﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA.Impl
{
    internal sealed class DdaLevelReporter : IDdaLevelReporter
    {
        public event Action LevelIncreased;
        public event Action LevelDecreased;

        public int DifficultyLevel { get; private set; }

        private readonly IObjectCollection<uint, UnitNetwork> _unitCollection;
        private bool _isDisposed;

        public DdaLevelReporter(IObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _unitCollection = unitCollection;
            Subscribe();
            CalcLevel();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            Unsubscribe();
            _isDisposed = true;
        }

        private void Subscribe()
        {
            _unitCollection.Added += OnUnitAdded;
            _unitCollection.Removed += OnUnitRemoved;
        }

        private void Unsubscribe()
        {
            _unitCollection.Added -= OnUnitAdded;
            _unitCollection.Removed -= OnUnitRemoved;
        }

        private void OnUnitAdded(UnitNetwork obj)
        {
            if (IsPlayer(obj))
                IncreaseLevel();
        }

        private void OnUnitRemoved(UnitNetwork obj)
        {
            if (IsPlayer(obj))
                DecreaseLevel();
        }

        private void CalcLevel()
        {
            foreach (UnitNetwork unit in _unitCollection)
            {
                if (IsPlayer(unit))
                    IncreaseLevel();
            }
        }

        private void IncreaseLevel()
        {
            DifficultyLevel++;
            LevelIncreased?.Invoke();
        }

        private void DecreaseLevel()
        {
            DifficultyLevel--;
            LevelDecreased?.Invoke();
        }

        private bool IsPlayer(UnitNetwork unit) => unit.CompareTag(BattleUnitTypeHelper.Player);
    }
}