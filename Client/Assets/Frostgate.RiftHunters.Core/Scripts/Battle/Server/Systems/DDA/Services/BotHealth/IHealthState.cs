﻿namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA.Services
{
    public interface IHealthState
    {
        void ApplyModifier(float modifier);
    }
}