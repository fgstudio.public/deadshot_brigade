﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA
{
    /// <summary>
    /// Хелпер для поиска ближайшего набора в коллекции к уровню DDA-системы.
    /// </summary>
    public static class DdaSetFinder
    {
        // TODO: Переделать алгоритм на O(n) трудоёмкость через Abs(sets[i].DdaLevel - ddaLevel)
        public static bool TryFindNearest<TSet>(int ddaLevel, [CanBeNull] IReadOnlyList<TSet> levelSets, out TSet set)
            where TSet : IDdaLevelSet
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(levelSets, nameof(levelSets));

            set = default;
            if (levelSets!.Count == 0)
                return false;

            IReadOnlyList<TSet> sets = SortSets(levelSets);
            for (var i = 1; i < sets.Count; i++)
            {
                if ((sets[i - 1].DdaLevel >= ddaLevel) ||
                    (sets[i - 1].DdaLevel < ddaLevel && sets[i].DdaLevel > ddaLevel))
                {
                    set = sets[i - 1];
                    break;
                }

                if (sets[i].DdaLevel == ddaLevel)
                {
                    set = sets[i];
                    break;
                }
            }

            if (EqualityComparer<TSet>.Default.Equals(set, default))
                set = sets[^1];

            return true;
        }

        /// <summary>
        /// Сортировка наборов по возрастанию уровня.
        /// </summary>
        /// <param name="levelSets"></param>
        /// <typeparam name="TSet"></typeparam>
        /// <returns></returns>
        private static IReadOnlyList<TSet> SortSets<TSet>(IReadOnlyList<TSet> levelSets) where TSet : IDdaLevelSet =>
            levelSets.OrderBy(set => set.DdaLevel).ToArray();
    }
}