﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA.Services
{
    [CreateAssetMenu(menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(HealthIncreasingServiceConfig),
        fileName = nameof(HealthIncreasingServiceConfig))]
    public sealed class HealthIncreasingServiceConfig : ScriptableObject, IConfig, ISerializationCallbackReceiver
    {
        [Serializable]
        private class EntitiesData
        {
            public IReadOnlyList<HealthModifierPerDdaLevel> Modifiers => _modifiers;
            public IReadOnlyList<UnitConfig> Entities => _entities;

            [SerializeField] private List<HealthModifierPerDdaLevel> _modifiers;
            [SerializeField] private List<UnitConfig> _entities;

            public void OnAfterDeserialize() =>
                _modifiers?
                    .Sort((a, b) => a.DdaLevel <= b.DdaLevel ? 0 : 1);
        }

        public string Id => name;

        private Dictionary<string, IReadOnlyList<HealthModifierPerDdaLevel>> _overrideIds;

        public float GetModifier(int level, string configId)
        {
            if (_overrideIds == null)
            {
                _overrideIds = new Dictionary<string, IReadOnlyList<HealthModifierPerDdaLevel>>();
                foreach (var data in _overrideEntitiesData)
                foreach (var entity in data.Entities)
                    _overrideIds.Add(entity.Id, data.Modifiers);
            }

            var modifiers = _overrideIds.ContainsKey(configId)
                ? _overrideIds[configId]
                : _defaultModifiers;

            if (modifiers.Count == 0)
                return 1f;

            for (var i = 1; i < modifiers.Count; i++)
            {
                if (modifiers[i - 1].DdaLevel == level)
                    return modifiers[i - 1].HealthModifier;

                if (modifiers[i - 1].DdaLevel <= level && modifiers[i].DdaLevel > level)
                    return modifiers[i - 1].HealthModifier;
            }

            return modifiers[^1].HealthModifier;
        }

        [SerializeField] private List<HealthModifierPerDdaLevel> _defaultModifiers;
        [SerializeField] private List<EntitiesData> _overrideEntitiesData;

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            _defaultModifiers?
                .Sort((a, b) => a.DdaLevel <= b.DdaLevel ? 0 : 1);

            foreach (var entitiesData in _overrideEntitiesData)
                entitiesData.OnAfterDeserialize();
        }
    }
}