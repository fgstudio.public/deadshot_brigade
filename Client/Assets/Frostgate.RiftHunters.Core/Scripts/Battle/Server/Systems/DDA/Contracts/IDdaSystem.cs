﻿using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA
{
    public interface IDdaSystem : IDisposable
    {
        int DifficultyLevel { get; }

        void RegisterService([NotNull] IDdaService service);
        void UnregisterService([NotNull] IDdaService service);
    }
}