﻿namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA
{
    public interface IDdaLevelSet
    {
        public int DdaLevel { get; }
    }
}