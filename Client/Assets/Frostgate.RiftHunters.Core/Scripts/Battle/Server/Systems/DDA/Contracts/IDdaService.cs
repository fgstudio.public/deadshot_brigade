﻿namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA
{
    public interface IDdaService
    {
        void OnRegistered();
        void OnUnregistered();
        void SetLevel(int level);
    }
}
