﻿using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA
{
    public interface IDdaServiceRepository : IEnumerable<IDdaService>
    {
        void Add([NotNull] IDdaService service);
        void Remove([NotNull] IDdaService service);
    }
}
