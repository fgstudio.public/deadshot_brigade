﻿using Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA.Impl;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA.Services;
using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA
{
    [HelpURL("https://app.clickup.com/t/2mybeub")]
    [AddComponentMenu(BattleComponentMenus.Server.Systems.Menu + "/" + nameof(DdaSystemInstaller))]
    [DisallowMultipleComponent]
    public sealed class DdaSystemInstaller : MonoInstaller
    {
        private IDdaSystem _system;

        public override void InstallBindings()
        {
            Container.Bind<IDdaSystem>().FromMethod(CreateSystem).AsSingle();

            _system = Container.Resolve<IDdaSystem>();
            RegisterServices();
        }

        private void OnDestroy() => _system?.Dispose();

        // Реализация через factory-method необходима для подавления warning message из zenject
        private IDdaSystem CreateSystem()
        {
            IDdaLevelReporter reporter = Container.Instantiate<DdaLevelReporter>();
            IDdaServiceRepository serviceRepository = new DdaServiceRepository();

            return new DdaSystem(reporter, serviceRepository);
        }

        private void RegisterServices()
        {
            IHealthStateRepository repository = Container.Instantiate<HealthStateRepository>();
            _system.RegisterService(Container.Instantiate<HealthIncreasingService>(new[] { repository }));
        }
    }
}