﻿using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA.Services
{
    /// <summary>
    /// Сервис меняющий ХП ботов в зависимости от количества игроков
    /// </summary>
    [HelpURL("https://app.clickup.com/t/2mybeub")]
    public sealed class HealthIncreasingService : IDdaService
    {
        private readonly HealthIncreasingServiceConfig _serviceConfig;
        private readonly IHealthStateRepository _stateRepository;

        private int _level = 1;

        public HealthIncreasingService([NotNull] IConfigAccessor<HealthIncreasingServiceConfig> serviceConfig,
            [NotNull] IHealthStateRepository stateRepository)
        {
            _serviceConfig = serviceConfig.Config;
            _stateRepository = stateRepository;
        }

        public void OnRegistered() => _stateRepository.Added += OnStateAdded;

        public void OnUnregistered() => _stateRepository.Added -= OnStateAdded;

        public void SetLevel(int level) => _level = level;

        private void OnStateAdded(IHealthState state, string configId)
        {
            var modifier = _serviceConfig.GetModifier(_level, configId);
            state.ApplyModifier(modifier);
        }
    }
}
