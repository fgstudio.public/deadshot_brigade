﻿using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA.Services
{
    [Serializable]
    public sealed class HealthModifierPerDdaLevel
    {
        [field: SerializeField, Range(1, 10)] public int DdaLevel { get; private set; }
        [field: SerializeField, Range(1, 100)] public float HealthModifier { get; private set; }
    }
}
