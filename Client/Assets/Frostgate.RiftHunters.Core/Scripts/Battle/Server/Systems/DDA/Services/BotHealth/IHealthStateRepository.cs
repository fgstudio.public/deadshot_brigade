﻿using System;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA.Services
{
    public interface IHealthStateRepository : IEnumerable<IHealthState>, IDisposable
    {
        event Action<IHealthState, string> Added;
        event Action<IHealthState, string> Removed;
    }
}
