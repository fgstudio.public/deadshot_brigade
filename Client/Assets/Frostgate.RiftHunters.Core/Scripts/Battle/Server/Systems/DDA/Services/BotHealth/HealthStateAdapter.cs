﻿using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA.Services
{
    internal sealed class HealthStateAdapter : IHealthState
    {
        private readonly IUnitHealthState _healthState;
        private float _currentModifier = 1;

        public HealthStateAdapter([NotNull] IUnitHealthState healthState)
        {
            _healthState = healthState;
        }

        public void ApplyModifier(float modifier)
        {
            CacheModifier(modifier);
            RecalculateHealthState();
        }

        private void CacheModifier(float modifier) => _currentModifier = modifier;

        private void RecalculateHealthState()
        {
            float damage = _healthState.MaxHealth - _healthState.Health;

            _healthState.MaxHealth *= _currentModifier;
            _healthState.Health = _healthState.MaxHealth - damage;
        }
    }
}
