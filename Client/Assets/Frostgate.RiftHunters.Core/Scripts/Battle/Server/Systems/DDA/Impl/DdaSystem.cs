﻿using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA.Impl
{
    // Dynamic Difficulty Adjustment System
    [HelpURL("https://app.clickup.com/t/2mybeub")]
    internal sealed class DdaSystem : IDdaSystem
    {
        public int DifficultyLevel => _levelReporter.DifficultyLevel;

        private readonly IDdaLevelReporter _levelReporter;
        private readonly IDdaServiceRepository _serviceRepository;

        private bool _isDisposed;

        public DdaSystem([NotNull] IDdaLevelReporter levelReporter, [NotNull] IDdaServiceRepository serviceRepository)
        {
            _levelReporter = levelReporter;
            _serviceRepository = serviceRepository;

            Subscribe();
        }

        public void RegisterService(IDdaService service)
        {
            _serviceRepository.Add(service);
            UpdateService(service);
            service.OnRegistered();
        }

        public void UnregisterService(IDdaService service)
        {
            _serviceRepository.Remove(service);
            UpdateService(service);
            service.OnUnregistered();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            Unsubscribe();
            _levelReporter.Dispose();
            _isDisposed = true;
        }

        private void Subscribe() => _levelReporter.LevelIncreased += OnLevelIncreased;

        private void Unsubscribe() => _levelReporter.LevelDecreased -= OnLevelDecreased;

        private void OnLevelIncreased() => UpdateServices();

        private void OnLevelDecreased() => UpdateServices();

        private void UpdateServices()
        {
            foreach (IDdaService service in _serviceRepository)
                UpdateService(service);
        }

        private void UpdateService(IDdaService service) => service.SetLevel(DifficultyLevel);
    }
}
