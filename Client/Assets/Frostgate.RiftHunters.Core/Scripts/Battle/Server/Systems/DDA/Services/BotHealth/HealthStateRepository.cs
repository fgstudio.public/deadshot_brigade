﻿using System;
using System.Collections;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA.Services
{
    internal sealed class HealthStateRepository : IHealthStateRepository
    {
        public event Action<IHealthState, string> Added;
        public event Action<IHealthState, string> Removed;

        private readonly IDictionary<UnitNetwork, HealthStateAdapter> _healthStateAdapters =
            new Dictionary<UnitNetwork, HealthStateAdapter>();

        private readonly IObjectCollection<uint, UnitNetwork> _unitCollection;
        private bool _isDisposed;

        public HealthStateRepository([NotNull] IObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _unitCollection = unitCollection;
            Init();
            Subscribe();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            Unsubscribe();
            _isDisposed = true;
        }

        public IEnumerator<IHealthState> GetEnumerator() => _healthStateAdapters.Values.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        private void Init()
        {
            foreach (UnitNetwork unit in _unitCollection)
            {
                if (IsBot(unit))
                    RegisterBot(unit);
            }
        }

        private void Subscribe()
        {
            _unitCollection.Added += OnUnitAdded;
            _unitCollection.Removed += OnUnitRemoved;
        }

        private void Unsubscribe()
        {
            _unitCollection.Added -= OnUnitAdded;
            _unitCollection.Removed -= OnUnitRemoved;
        }

        private void OnUnitAdded(UnitNetwork unit)
        {
            if (IsBot(unit))
                RegisterBot(unit);
        }

        private void OnUnitRemoved(UnitNetwork unit)
        {
            if (IsBot(unit))
                UnregisterBot(unit);
        }

        private bool IsBot(UnitNetwork unit) => unit.CompareTag(BattleUnitTypeHelper.Bot);

        private void RegisterBot(UnitNetwork unit)
        {
            var adapter = new HealthStateAdapter(unit.UnitNetworkState.HealthState);
            _healthStateAdapters[unit] = adapter;

            Added?.Invoke(adapter, unit.UnitNetworkState.Id);
        }

        private void UnregisterBot(UnitNetwork unit)
        {
            HealthStateAdapter adapter = _healthStateAdapters[unit];
            _healthStateAdapters.Remove(unit);

            Removed?.Invoke(adapter, unit.UnitNetworkState.Id);
        }
    }
}