using System;
using System.Threading;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoLoss.Health
{
    /// <summary>
    /// Класс, отвечающий за процесс уменьшения здоровья.
    /// </summary>
    public sealed class Loss : IDisposable
    {
        public UniTask Task { get; private set; }

        private readonly IHpAutoLost _lost;

        private CancellationTokenSource _tokenSource;

        public Loss([NotNull] IHpAutoLost lost)
        {
            _lost = lost;
            StartLosing();
        }

        public void Dispose() =>
            StopLosing();

        private void StartLosing()
        {
            StopLosing();

            _tokenSource = new CancellationTokenSource();
            Task = LossRoutine(_tokenSource.Token);
        }

        private void StopLosing()
        {
            if (_tokenSource != null)
            {
                _tokenSource.Cancel();
                _tokenSource.Dispose();
                _tokenSource = null;
            }
        }

        private async UniTask LossRoutine(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await UniTask.Yield();

                if (CanBeUnderLoss(_lost))
                    Lose(_lost, Time.deltaTime);
            }

            if (!cancellationToken.IsCancellationRequested)
                StopLosing();
        }

        private bool CanBeUnderLoss(IHpAutoLost lost) =>
            lost.Config.Speed > 0 && !lost.State.IsDead;

        // TODO: вероятно, это нужно делать через новый сервис, т.к. тащить всё через целый DamageService не хочется.
        private void Lose(IHpAutoLost lost, float dt) =>
            lost.State.Health -= lost.Config.Speed * dt;
    }
}