using System;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoLoss.Health;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems
{
    /// <summary>
    /// Система для автоуменьшения здоровья. Работает автоматически. Требует только установки.
    /// </summary>
    public sealed class HpAutoLossSystem : IDisposable
    {
        private readonly IReadOnlyObjectCollection<Collider, IAimTarget> _targetCollection;
        private readonly LossStorage _recoveryStorage;

        private bool _isDisposed;

        public HpAutoLossSystem([NotNull] IReadOnlyObjectCollection<Collider, IAimTarget> targetCollection)
        {
            _targetCollection = targetCollection;
            _recoveryStorage = new LossStorage();

            foreach (IAimTarget target in _targetCollection)
                TryCreateLoss(target);

            Subscribe();
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            _recoveryStorage.Dispose();
            Unsubscribe();

            _isDisposed = true;
        }

        private void Subscribe()
        {
            _targetCollection.Added += TryCreateLoss;
            _targetCollection.Removed += RemoveLoss;
        }

        private void Unsubscribe()
        {
            _targetCollection.Added -= TryCreateLoss;
            _targetCollection.Removed -= RemoveLoss;
        }

        private void TryCreateLoss(IAimTarget target)
        {
            if (CanBeLost(target))
                _recoveryStorage.Create(target);
        }

        private void RemoveLoss(IAimTarget target) =>
            _recoveryStorage.Remove(target);

        private bool CanBeLost(IAimTarget target) =>
            target is IHpAutoLost lost && lost.Config.Speed > 0;
    }
}
