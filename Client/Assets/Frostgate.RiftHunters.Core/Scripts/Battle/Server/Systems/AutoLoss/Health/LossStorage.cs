using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoLoss.Health
{
    public sealed class LossStorage : IDisposable
    {
        private readonly Dictionary<INetworkIdentifiable, Loss> _losts = new();

        public void Dispose()
        {
            foreach (Loss recovery in _losts.Values)
                recovery.Dispose();

            _losts.Clear();
        }

        public bool Has([NotNull] INetworkIdentifiable receiver) =>
            _losts.ContainsKey(receiver);

        public void Create([NotNull] INetworkIdentifiable lost) =>
            _losts[lost] = new Loss((IHpAutoLost)lost);

        public void Remove([NotNull] INetworkIdentifiable receiver)
        {
            if (_losts.TryGetValue(receiver, out Loss lost))
            {
                lost.Dispose();
                _losts.Remove(receiver);
            }
        }
    }
}
