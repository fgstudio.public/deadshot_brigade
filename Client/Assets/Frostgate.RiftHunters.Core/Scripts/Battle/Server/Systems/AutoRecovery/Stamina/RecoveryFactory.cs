using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Stamina;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoRecovery.Stamina
{
    public sealed class RecoveryFactory
    {
        private readonly IStaminaService _staminaService;

        public RecoveryFactory([NotNull] IStaminaService staminaService) =>
            _staminaService = staminaService;

        public Recovery Create([NotNull] IStaminaAutoRecoverable recoverable) =>
            new(recoverable, _staminaService);
    }
}