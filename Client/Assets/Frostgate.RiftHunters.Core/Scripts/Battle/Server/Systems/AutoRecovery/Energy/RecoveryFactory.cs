using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoRecovery.Energy
{
    public sealed class RecoveryFactory
    {
        public Recovery Create([NotNull] IEnergyAutoRecoverable recoverable) =>
            new(recoverable);
    }
}
