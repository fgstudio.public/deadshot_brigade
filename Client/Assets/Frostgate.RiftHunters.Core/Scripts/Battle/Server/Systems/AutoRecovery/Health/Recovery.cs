using System;
using System.Threading;
using System.Diagnostics;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared.Healing;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoRecovery.Health
{
    /// <summary>
    /// Класс, отвечающий за процесс восстановления здоровья.
    /// <see cref="https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#e417470319264003b313af6970c072ce"/>
    /// </summary>
    public sealed class Recovery : IDisposable
    {
        public UniTask Task { get; private set; }

        private readonly IHpAutoRecoverable _recoverable;
        private readonly IHealingService _healingService;

        private readonly Stopwatch _stopwatch;

        private CancellationTokenSource _tokenSource;
        private bool _isDisposed;

        public Recovery([NotNull] IHpAutoRecoverable recoverable, [NotNull] IHealingService healingService)
        {
            _recoverable = recoverable;
            _healingService = healingService;

            _stopwatch = new Stopwatch();
        }

        public void Dispose()
        {
            if (!_isDisposed)
            {
                Stop();
                _isDisposed = true;
            }
        }

        public void Start()
        {
            Stop();

            if (CanBeUnderRecovery(_recoverable)) // чтобы избежать лишней аллокации
            {
                _tokenSource = new CancellationTokenSource();
                Task = RecoveryRoutine(_tokenSource.Token);
            }
        }

        public void Stop()
        {
            if (_tokenSource != null)
            {
                _tokenSource.Cancel();
                _tokenSource.Dispose();
                _tokenSource = null;
            }

            _stopwatch.Stop();
        }

        private async UniTask RecoveryRoutine(CancellationToken cancellationToken)
        {
            while (CanBeUnderRecovery(_recoverable) && !cancellationToken.IsCancellationRequested)
            {
                float dt = (float)_stopwatch.Elapsed.TotalSeconds;
                Recover(_recoverable, dt);

                _stopwatch.Restart();
                await UniTask.Yield();
            }

            if (!cancellationToken.IsCancellationRequested)
                Stop();
        }

        private bool CanBeUnderRecovery(IHpAutoRecoverable recoverable) =>
            recoverable.Config.PercentPerSecond > 0 && !recoverable.State.IsDead && !recoverable.State.IsHealthFull;

        private void Recover(IHpAutoRecoverable recoverable, float dt)
        {
            var dto = new HealingSourceDTO(recoverable.Config.PercentPerSecond * recoverable.State.MaxHealth * dt, HealingType.AutoRecovery);
            _healingService.Heal(dto, _recoverable, recoverable);
        }
    }
}
