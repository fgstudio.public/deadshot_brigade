using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoRecovery.Stamina
{
    public sealed class RecoveryStorage : IDisposable
    {
        private readonly RecoveryFactory _factory;
        private readonly Dictionary<INetworkIdentifiable, Recovery> _recoveries;

        public RecoveryStorage([NotNull] RecoveryFactory factory)
        {
            _factory = factory;
            _recoveries = new Dictionary<INetworkIdentifiable, Recovery>();
        }

        public void Dispose()
        {
            foreach (Recovery recovery in _recoveries.Values)
                recovery.Dispose();

            _recoveries.Clear();
        }

        public bool Has([NotNull] INetworkIdentifiable receiver) =>
            _recoveries.ContainsKey(receiver);

        public void Create([NotNull] INetworkIdentifiable receiver) =>
            _recoveries[receiver] = _factory.Create((IStaminaAutoRecoverable)receiver);

        public void Remove([NotNull] INetworkIdentifiable receiver)
        {
            if (_recoveries.TryGetValue(receiver, out Recovery recovery))
            {
                recovery.Dispose();
                _recoveries.Remove(receiver);
            }
        }
    }
}