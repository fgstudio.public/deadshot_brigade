using System;
using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoRecovery.Energy;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems
{
    /// <summary>
    /// Система для автовостановления Энергии. Работает автоматически. Требует только установки.
    /// </summary>
    public sealed class EnergyAutoRecoverySystem : IDisposable
    {
        private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;
        private readonly RecoveryStorage _recoveryStorage;

        private bool _isDisposed;

        public EnergyAutoRecoverySystem([NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _unitCollection = unitCollection;

            var factory = new RecoveryFactory();
            _recoveryStorage = new RecoveryStorage(factory);

            foreach (UnitNetwork unitNetwork in _unitCollection)
                OnAddUnit(unitNetwork);

            Subscribe();
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            _recoveryStorage.Dispose();
            Unsubscribe();

            _isDisposed = true;
        }

        private void Subscribe()
        {
            _unitCollection.Added += OnAddUnit;
            _unitCollection.Removed += OnRemoveUnit;
        }

        private void Unsubscribe()
        {
            _unitCollection.Added -= OnAddUnit;
            _unitCollection.Removed -= OnRemoveUnit;
        }

        private void OnAddUnit(UnitNetwork unitNetwork)
        {
            if (CanBeRecovered(unitNetwork))
                _recoveryStorage.Create(unitNetwork);
        }

        private void OnRemoveUnit(UnitNetwork unitNetwork) =>
            _recoveryStorage.Remove(unitNetwork);

        private bool CanBeRecovered(UnitNetwork unitNetwork) =>
            unitNetwork.UnitNetworkState.PropertiesProvider?.EnergyAutoRecovery.Speed > 0;
    }
}