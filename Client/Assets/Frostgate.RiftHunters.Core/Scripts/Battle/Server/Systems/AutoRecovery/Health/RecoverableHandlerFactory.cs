using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Healing;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoRecovery.Health
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#e417470319264003b313af6970c072ce"/>
    /// </summary>
    public sealed class RecoverableHandlerFactory
    {
        private readonly IHealingService _healingService;

        public RecoverableHandlerFactory([NotNull] IHealingService healingService) =>
            _healingService = healingService;

        public RecoverableHandler Create([NotNull] IHpAutoRecoverable recoverable) =>
            new RecoverableHandler(recoverable, _healingService);
    }
}
