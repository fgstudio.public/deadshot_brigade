using System;
using System.Threading;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared.Stamina;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoRecovery.Stamina
{
    /// <summary>
    /// Класс, отвечающий за процесс восстановления стамины.
    /// </summary>
    public sealed class Recovery : IDisposable
    {
        public UniTask Task { get; private set; }

        private readonly IStaminaAutoRecoverable _recoverable;
        private readonly IStaminaService _staminaService;

        private CancellationTokenSource _tokenSource;

        public Recovery([NotNull] IStaminaAutoRecoverable recoverable, [NotNull] IStaminaService staminaService)
        {
            _recoverable = recoverable;
            _staminaService = staminaService;

            StartRecovering();
        }

        public void Dispose() =>
            StopRecovering();

        private void StartRecovering()
        {
            StopRecovering();

            _tokenSource = new CancellationTokenSource();
            Task = RecoveryRoutine(_tokenSource.Token);
        }

        private void StopRecovering()
        {
            if (_tokenSource != null)
            {
                _tokenSource.Cancel();
                _tokenSource.Dispose();
                _tokenSource = null;
            }
        }

        private async UniTask RecoveryRoutine(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await UniTask.Yield();

                if (CanBeUnderRecovery(_recoverable))
                    Recover(_recoverable, Time.deltaTime);
            }

            if (!cancellationToken.IsCancellationRequested)
                StopRecovering();
        }

        private bool CanBeUnderRecovery(IStaminaAutoRecoverable recoverable) =>
            recoverable.Config.Speed > 0 && !recoverable.State.IsStaminaFull && !recoverable.State.IsDead;

        private void Recover(IStaminaAutoRecoverable recoverable, float dt)
        {
            var dto = new StaminaSourceDTO(recoverable.Config.Speed * dt);
            _staminaService.Recover(recoverable, dto);
        }
    }
}