using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Healing;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoRecovery.Health
{
    /// <summary>
    /// Обработчик процесса автовосстановления Hp. Содержит верхнеуровневую логику.
    /// <see cref="https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#e417470319264003b313af6970c072ce"/>
    /// </summary>
    public sealed class RecoverableHandler : IDisposable
    {
        private readonly Recovery _recovery;
        private readonly IHpAutoRecoverable _recoverable;

        private bool _isDisposed;
        private CancellationTokenSource _tokenSource;
        private UniTask Task { get; set; }

        public RecoverableHandler([NotNull] IHpAutoRecoverable recoverable, [NotNull] IHealingService healingService)
        {
            _recoverable = recoverable;
            _recovery = new Recovery(recoverable, healingService);
            _tokenSource = new CancellationTokenSource();
            _recovery.Start();
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            _tokenSource.Cancel();
            _recovery.Dispose();

            _isDisposed = true;
        }

        public void GetDamage()
        {
            _recovery.Stop();

            _tokenSource.Cancel();
            _tokenSource = new CancellationTokenSource();
            Task = StartRecoveryAsync(_tokenSource);
        }

        private async UniTask StartRecoveryAsync(CancellationTokenSource cancellationTokenSource)
        {
            var damageTime = Time.time;
            while (!cancellationTokenSource.IsCancellationRequested)
            {
                if (Time.time >= _recoverable.Config.DamageCooldown + damageTime)
                {
                    _recovery.Start();
                    break;
                }

                await UniTask.Yield();
            }
        }

        public void Reanimate() =>
            _recovery.Start();
    }
}
