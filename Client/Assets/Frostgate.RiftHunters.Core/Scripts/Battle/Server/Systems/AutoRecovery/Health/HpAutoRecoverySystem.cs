using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Healing;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Reanimation;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoRecovery.Health;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems
{
    /// <summary>
    /// Система для автовостановления здоровья. Работает автоматически. Требует только установки.
    /// <see cref="https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#e417470319264003b313af6970c072ce"/>
    /// </summary>
    public sealed class HpAutoRecoverySystem : IDisposable
    {
        private readonly IDamageService _damageService;
        private readonly IReanimationService _reanimationService;
        private readonly RecoverableHandlerProvider _handlerProvider;

        private bool _isDisposed;

        public HpAutoRecoverySystem([NotNull] IDamageService damageService, [NotNull] IReanimationService reanimationService,
            [NotNull] IHealingService healingService)
        {
            _damageService = damageService;
            _reanimationService = reanimationService;

            var factory = new RecoverableHandlerFactory(healingService);
            _handlerProvider = new RecoverableHandlerProvider(factory);

            Subscribe();
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            _handlerProvider.Dispose();
            Unsubscribe();

            _isDisposed = true;
        }

        private void Subscribe()
        {
            _damageService.Damaged.AddListener(OnDamaged);
            _reanimationService.Reanimated.AddListener(OnReanimated);
        }

        private void Unsubscribe()
        {
            _damageService.Damaged.RemoveListener(OnDamaged);
            _reanimationService.Reanimated.RemoveListener(OnReanimated);
        }

        private void OnDamaged(DamageEventData data)
        {
            if (_handlerProvider.TryGet(data.ReceiverNetId, out RecoverableHandler handler))
                handler.GetDamage();
        }

        private void OnReanimated(uint reanimatedId)
        {
            if (_handlerProvider.TryGet(reanimatedId, out RecoverableHandler handler))
                handler.Reanimate();
        }
    }
}