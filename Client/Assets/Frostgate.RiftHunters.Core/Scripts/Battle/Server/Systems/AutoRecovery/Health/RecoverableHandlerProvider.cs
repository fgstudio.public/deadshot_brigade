using System;
using Mirror;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoRecovery.Health
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#e417470319264003b313af6970c072ce"/>
    /// </summary>
    public sealed class RecoverableHandlerProvider : IDisposable
    {
        private readonly RecoverableHandlerFactory _factory;
        private readonly Dictionary<uint, RecoverableHandler> _handlers;

        private bool _isDisposed;

        public RecoverableHandlerProvider([NotNull] RecoverableHandlerFactory factory)
        {
            _factory = factory;
            _handlers = new Dictionary<uint, RecoverableHandler>();
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            foreach (RecoverableHandler handler in _handlers.Values)
                handler.Dispose();

            _handlers.Clear();

            _isDisposed = true;
        }

        public bool TryGet(uint receiverId, out RecoverableHandler handler)
        {
            if (_handlers.TryGetValue(receiverId, out handler))
                return true;

            if (NetworkServer.spawned.TryGetValue(receiverId, out NetworkIdentity receiverIdentity) &&
                receiverIdentity.TryGetComponent(out IHpAutoRecoverable recoverable))
            {
                _handlers[receiverId] = handler = _factory.Create(recoverable);
                return true;
            }

            return false;
        }
    }
}