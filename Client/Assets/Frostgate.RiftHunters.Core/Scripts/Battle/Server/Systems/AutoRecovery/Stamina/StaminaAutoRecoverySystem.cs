using System;
using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoRecovery.Stamina;
using Frostgate.RiftHunters.Core.Battle.Shared.Stamina;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems
{
    /// <summary>
    /// Система для автовостановления стамины. Работает автоматически. Требует только установки.
    /// </summary>
    public sealed class StaminaAutoRecoverySystem : IDisposable
    {
        private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;
        private readonly RecoveryStorage _recoveryStorage;

        private bool _isDisposed;

        public StaminaAutoRecoverySystem([NotNull] IStaminaService _staminaService,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _unitCollection = unitCollection;

            var factory = new RecoveryFactory(_staminaService);
            _recoveryStorage = new RecoveryStorage(factory);

            foreach (UnitNetwork unitNetwork in _unitCollection)
                TryCreateRecovery(unitNetwork);

            Subscribe();
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            _recoveryStorage.Dispose();
            Unsubscribe();

            _isDisposed = true;
        }

        private void Subscribe()
        {
            _unitCollection.Added += TryCreateRecovery;
            _unitCollection.Removed += RemoveRecovery;
        }

        private void Unsubscribe()
        {
            _unitCollection.Added -= TryCreateRecovery;
            _unitCollection.Removed -= RemoveRecovery;
        }

        private void TryCreateRecovery(UnitNetwork unitNetwork)
        {
            if (CanBeRecovered(unitNetwork))
                _recoveryStorage.Create(unitNetwork);
        }

        private void RemoveRecovery(UnitNetwork unitNetwork) =>
            _recoveryStorage.Remove(unitNetwork);

        private bool CanBeRecovered(UnitNetwork unitNetwork) =>
            unitNetwork.UnitNetworkState.PropertiesProvider?.StaminaAutoRecovery.Speed > 0;
    }
}