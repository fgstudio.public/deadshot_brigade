using System;
using System.Threading;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.AutoRecovery.Energy
{
    /// <summary>
    /// Класс, отвечающий за процесс восстановления энерегии.
    /// </summary>
    public sealed class Recovery : IDisposable
    {
        public UniTask Task { get; private set; }

        private readonly IEnergyAutoRecoverable _recoverable;

        private CancellationTokenSource _tokenSource;

        public Recovery([NotNull] IEnergyAutoRecoverable recoverable)
        {
            _recoverable = recoverable;

            StartRecovering();
        }

        public void Dispose() =>
            StopRecovering();

        private void StartRecovering()
        {
            StopRecovering();

            _tokenSource = new CancellationTokenSource();
            Task = RecoveryRoutine(_tokenSource.Token);
        }

        private void StopRecovering()
        {
            if (_tokenSource != null)
            {
                _tokenSource.Cancel();
                _tokenSource.Dispose();
                _tokenSource = null;
            }
        }

        private async UniTask RecoveryRoutine(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await UniTask.Yield();

                if (CanBeUnderRecovery(_recoverable))
                    Recover(_recoverable, Time.deltaTime);
            }

            if (!cancellationToken.IsCancellationRequested)
                StopRecovering();
        }

        private bool CanBeUnderRecovery(IEnergyAutoRecoverable recoverable) =>
            recoverable.Config.Speed > 0 && !recoverable.State.IsEnergyFull && !recoverable.State.IsDead
            && recoverable.RecoveryMultiplier > 0;

        private void Recover(IEnergyAutoRecoverable recoverable, float dt)
        {
            var energy = recoverable.Config.Speed * recoverable.RecoveryMultiplier * dt;
            recoverable.State.Energy += energy;
        }
    }
}