using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public sealed class WhenAllPlayersDieAwaiter : BaseAwaiter
    {
        [NotNull] private readonly PlayersDieObserver _playersDieObserver;

        public WhenAllPlayersDieAwaiter( [NotNull] UnitNetwork unit,
            [NotNull] PlayersDieObserver playersDieObserver, [NotNull] ILogger logger)
            : base(unit, logger)
        {
            _playersDieObserver = playersDieObserver;
            _playersDieObserver.AllPlayersDied += SetDone;

            if (_playersDieObserver.AreAllPlayersDead)
                SetDone();
        }

        protected override void OnDisposed() =>
            _playersDieObserver.AllPlayersDied -= SetDone;
    }
}