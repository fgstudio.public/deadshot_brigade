using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public sealed class UnitDeadObserver : IDisposable
    {
        [CanBeNull] private readonly UnitNetwork _unit;
        [NotNull] private readonly UnitDeadHandlerFactory _handlerFactory;

        [CanBeNull] private UnitDeadHandler _handler;

        public UnitDeadObserver([NotNull] UnitNetwork unit,
            [NotNull] UnitDeadHandlerFactory handlerFactory)
        {
            _unit = unit;
            _handlerFactory = handlerFactory;

            _unit.UnitNetworkState.BehaviourStateChanged += OnStateChanged;
            OnStateChanged();
        }

        public void Dispose()
        {
            if (_unit != null)
                _unit.UnitNetworkState.BehaviourStateChanged -= OnStateChanged;

            _handler?.Dispose();
        }

        private void OnStateChanged()
        {
            if (_unit == null) return;
            if (_unit.UnitNetworkState.UnitType != BattleUnitType.Player) return;

            _handler?.Dispose();

            if (_unit.UnitNetworkState.BehaviourState == BehaviourState.Dead)
                _handler = _handlerFactory.Create(_unit);
        }
    }
}