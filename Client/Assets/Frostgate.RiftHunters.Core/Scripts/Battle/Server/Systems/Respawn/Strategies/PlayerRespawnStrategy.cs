using System;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints;
using Frostgate.RiftHunters.Core.Battle.Shared.Invincibility;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public sealed class PlayerRespawnStrategy : IRespawnStrategy
    {
        private readonly TimeSpan _invincibleDuration;
        [NotNull] private readonly IInvincibleService _invincibleService;
        [NotNull] private readonly IRespawnUnitService _respawnUnitService;
        [NotNull] private readonly UnitPlayerPointCalculator _pointCalculator;

        public PlayerRespawnStrategy(TimeSpan invincibleDuration,
            [NotNull] IInvincibleService invincibleService,
            [NotNull] IRespawnUnitService respawnUnitService,
            [NotNull] UnitPlayerPointCalculator pointCalculator)
        {
            _invincibleDuration = invincibleDuration;
            _invincibleService = invincibleService;
            _respawnUnitService = respawnUnitService;
            _pointCalculator = pointCalculator;
        }

        public async void Respawn(UnitNetwork unit)
        {
            //todo: без задержки, игроки, которые уже мертвы и у которых уже активирована механика реанимации
            //реанимация не отключается, поэтому добавлена задержка (как было и раньше)
            await UniTask.Delay(TimeSpan.FromSeconds(0.5f));

            if (unit != null)
            {
                PointData respawnPointData = _pointCalculator.CalcForCheckpoint();

                _respawnUnitService.Respawn(unit, respawnPointData);
                _invincibleService.SetInvincible(unit, _invincibleDuration);
            }
        }
    }
}