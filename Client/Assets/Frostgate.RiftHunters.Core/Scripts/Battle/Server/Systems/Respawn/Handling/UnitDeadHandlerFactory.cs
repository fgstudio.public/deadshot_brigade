using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public sealed class UnitDeadHandlerFactory
    {
        [NotNull] private readonly AwaiterFactory _awaiterFactory;
        [NotNull] private readonly RespawnStrategyFactory _strategyFactory;
        [NotNull] private readonly ArenaConfigProvider _arenaConfigProvider;
        [NotNull] private readonly SimulationTime _simulationTime;
        [NotNull] private readonly BotRespawnState _botRespawnState;

        public UnitDeadHandlerFactory(
            [NotNull] AwaiterFactory awaiterFactory,
            [NotNull] RespawnStrategyFactory strategyFactory,
            [NotNull] ArenaConfigProvider arenaConfigProvider,
            [NotNull] SimulationTime simulationTime,
            [NotNull] BotRespawnState botRespawnState)
        {
            _awaiterFactory = awaiterFactory;
            _strategyFactory = strategyFactory;
            _arenaConfigProvider = arenaConfigProvider;
            _simulationTime = simulationTime;
            _botRespawnState = botRespawnState;
        }

        [CanBeNull]
        public UnitDeadHandler Create([NotNull] UnitNetwork unit)
        {
            if (!_arenaConfigProvider.TryGetCurrent(out BotArenaConfig arenaConfig))
                return null;

            BotArenaRespawnData respawnData = arenaConfig.RespawnData;
            CompositeAwaiter awaiter = _awaiterFactory.Create(unit, respawnData);
            IRespawnStrategy strategy = _strategyFactory.Create(respawnData.InvincibleDuration);

            UnitAwaiterHandler awaiterHandler = new UnitAwaiterHandler(
                unit, awaiter, _botRespawnState, _simulationTime,
                LoggerFactory.CreateLogger<UnitAwaiterHandler>());

            return new UnitDeadHandler(unit, awaiter, strategy, awaiterHandler);
        }
    }
}