﻿using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public sealed class AwaiterFactory
    {
        [NotNull] private readonly PlayersDieObserver _playersDieObserver;

        public AwaiterFactory([NotNull] PlayersDieObserver playersDieObserver)
        {
            _playersDieObserver = playersDieObserver;
        }

        [NotNull]
        public CompositeAwaiter Create([NotNull] UnitNetwork unit,
            [NotNull] BotArenaRespawnData respawnData)
        {
            IEnumerable<BaseAwaiter> subAwaiters = CreateSubAwaiters(unit, respawnData);

            ILogger<CompositeAwaiter> logger =
                LoggerFactory.CreateLogger<CompositeAwaiter>();

            return new CompositeAwaiter(unit, subAwaiters, logger);
        }

        private IEnumerable<BaseAwaiter> CreateSubAwaiters(
            [NotNull] UnitNetwork unit, [NotNull] BotArenaRespawnData respawnData)
        {
            if (respawnData.RespawnTypeFlags.HasFlag(RespawnType.ByTimer))
                yield return new ByTimerAwaiter(unit,
                    LoggerFactory.CreateLogger<ByTimerAwaiter>(),
                    respawnData.RespawnTimerInterval);

            if (respawnData.RespawnTypeFlags.HasFlag(RespawnType.WhenAllPlayersDie))
                yield return new WhenAllPlayersDieAwaiter(unit,
                    _playersDieObserver,
                    LoggerFactory.CreateLogger<WhenAllPlayersDieAwaiter>());
        }
    }
}