﻿using System;
using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public sealed class UnitAwaiterHandler : IDisposable
    {
        [CanBeNull] private readonly UnitNetwork _unit;
        [NotNull] private readonly CompositeAwaiter _compositeAwaiter;
        [NotNull] private readonly BotRespawnState _botRespawnState;
        [NotNull] private readonly SimulationTime _simulationTime;
        [NotNull] private readonly ILogger _logger;

        public UnitAwaiterHandler([NotNull] UnitNetwork unit, [NotNull] CompositeAwaiter compositeAwaiter,
            [NotNull] BotRespawnState botRespawnState, [NotNull] SimulationTime simulationTime, ILogger logger)
        {
            _unit = unit;
            _logger = logger;
            _compositeAwaiter = compositeAwaiter;
            _botRespawnState = botRespawnState;
            _simulationTime = simulationTime;

            Subscribe(_compositeAwaiter);
            Initialize(_compositeAwaiter);
        }

        public void Dispose()
        {
            UnSubscribe(_compositeAwaiter);

            if (_unit != null)
                _botRespawnState.RespawnUnits.Remove(_unit.netId);
        }

        private void Subscribe(CompositeAwaiter compositeAwaiter)
        {
            IEnumerable<ByTimerAwaiter> timerAwaiters = compositeAwaiter.Awaiters.OfType<ByTimerAwaiter>();
            foreach (ByTimerAwaiter awaiter in timerAwaiters)
            {
                awaiter.Continued += OnTimerContinued;
                awaiter.Paused += OnTimerPaused;
            }

            compositeAwaiter.Done += OnDone;
        }

        private void UnSubscribe(CompositeAwaiter compositeAwaiter)
        {
            IEnumerable<ByTimerAwaiter> timerAwaiters = compositeAwaiter.Awaiters.OfType<ByTimerAwaiter>();
            foreach (ByTimerAwaiter awaiter in timerAwaiters)
            {
                awaiter.Continued -= OnTimerContinued;
                awaiter.Paused -= OnTimerPaused;
            }

            compositeAwaiter.Done -= OnDone;
        }

        private void Initialize(CompositeAwaiter compositeAwaiter)
        {
            IEnumerable<ByTimerAwaiter> timerAwaiters = compositeAwaiter.Awaiters.OfType<ByTimerAwaiter>();
            ByTimerAwaiter timedAwaiter = timerAwaiters.FirstOrDefault(a => a.Interval.TotalSeconds > 0);
            TimeSpan interval = timedAwaiter?.Interval ?? TimeSpan.Zero;

            UpdateState(RespawnStatus.Respawning, interval);
        }

        private void OnDone() => UpdateState(RespawnStatus.Done, TimeSpan.Zero);
        private void OnTimerPaused(TimeSpan timeLeft) => UpdateState(RespawnStatus.Paused, timeLeft);
        private void OnTimerContinued(TimeSpan timeLeft) => UpdateState(RespawnStatus.Respawning, timeLeft);

        private void UpdateState(RespawnStatus status, TimeSpan timerInterval)
        {
            if (_unit != null)
            {
                var data = new RespawnData(status, _simulationTime.Time, timerInterval.TotalSeconds);
                _botRespawnState.RespawnUnits[_unit.netId] = data;

                _logger.Log($"State Updated: {data}");
            }
        }
    }
}