using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public sealed class CompositeAwaiter : BaseAwaiter
    {
        [NotNull, ItemNotNull]
        public readonly IReadOnlyList<BaseAwaiter> Awaiters;

        public CompositeAwaiter([NotNull] UnitNetwork unit,
            [NotNull, ItemNotNull] IEnumerable<BaseAwaiter> awaiters, [NotNull] ILogger logger)
            : base(unit, logger)
        {
            Awaiters = awaiters.ToArray();
            Awaiters.ForEach(Subscribe);

            if (Awaiters.Any(h => h.IsDone))
                OnAnyAwaiterDone();
        }

        protected override void OnDisposed()
        {
            foreach (BaseAwaiter awaiter in Awaiters)
            {
                Unsubscribe(awaiter);
                awaiter.Dispose();
            }
        }

        private void Subscribe([NotNull] BaseAwaiter awaiter) =>
            awaiter.Done += OnAnyAwaiterDone;

        private void Unsubscribe([NotNull] BaseAwaiter awaiter) =>
            awaiter.Done -= OnAnyAwaiterDone;

        private void OnAnyAwaiterDone()
        {
            Awaiters.ForEach(Unsubscribe);
            SetDone();
        }
    }
}