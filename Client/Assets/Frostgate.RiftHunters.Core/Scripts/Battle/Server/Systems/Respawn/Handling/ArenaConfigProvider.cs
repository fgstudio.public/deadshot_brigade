using System.Linq;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public sealed class ArenaConfigProvider
    {
        [NotNull] private readonly IReadOnlyBotArenaState _arenaState;
        [NotNull] private readonly IReadOnlyObjectCollection<BotArenaConfig, BotArena> _arenaCollection;

        public ArenaConfigProvider(
            [NotNull] IReadOnlyBotArenaState arenaState,
            [NotNull] IReadOnlyObjectCollection<BotArenaConfig, BotArena> arenaCollection)
        {
            _arenaState = arenaState;
            _arenaCollection = arenaCollection;
        }

        public bool TryGetCurrent(out BotArenaConfig arenaConfig)
        {
            string currentArenaId = _arenaState.CurrentArenaId;
            arenaConfig = _arenaCollection.Keys.FirstOrDefault(c => c.Id == currentArenaId);
            return arenaConfig != null;
        }
    }
}