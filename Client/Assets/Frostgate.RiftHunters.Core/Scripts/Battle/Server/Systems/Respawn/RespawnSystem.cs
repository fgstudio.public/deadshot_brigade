using System;
using System.Collections.Generic;
using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public sealed class RespawnSystem : IDisposable
    {
        private readonly Dictionary<UnitNetwork, UnitDeadObserver> _deadObservers = new();

        [NotNull] private readonly UnitDeadHandlerFactory _handlerFactory;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;

        public RespawnSystem(
            [NotNull] UnitDeadHandlerFactory handlerFactory,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _handlerFactory = handlerFactory;
            _unitCollection = unitCollection;

            _unitCollection.Added += OnAdded;
            _unitCollection.Removed += OnRemoved;

            _unitCollection.Values.ForEach(OnAdded);
        }

        public void Dispose()
        {
            _unitCollection.Added -= OnAdded;
            _unitCollection.Removed -= OnRemoved;

            foreach (UnitDeadObserver observer in _deadObservers.Values)
                observer.Dispose();

            _deadObservers.Clear();
        }

        private void OnAdded([CanBeNull] UnitNetwork unit)
        {
            if (unit != null)
                _deadObservers[unit] = CreateDeadObserver(unit);
        }

        private void OnRemoved([CanBeNull] UnitNetwork unit)
        {
            if (unit == null) return;
            if (!_deadObservers.TryGetValue(unit, out UnitDeadObserver observer)) return;

            observer.Dispose();
            _deadObservers.Remove(unit);
        }

        private UnitDeadObserver CreateDeadObserver([NotNull] UnitNetwork unit) =>
            new(unit, _handlerFactory);
    }
}