using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public abstract class BaseAwaiter : IDisposable
    {
        public event Action Done;
        public bool IsDone { get; private set; }

        [CanBeNull] protected readonly UnitNetwork Unit;
        [NotNull] private readonly ILogger _logger;

        private bool _isDisposed;

        protected BaseAwaiter(
            [CanBeNull] UnitNetwork unit,
            [NotNull] ILogger logger)
        {
            Unit = unit;
            _logger = logger;
            Log("Started");
        }

        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
                Log("Finished");

                OnDisposed();
            }
        }

        protected virtual void OnDisposed() { }

        protected void SetDone()
        {
            if (!IsDone)
            {
                IsDone = true;
                Log("Done");

                Done?.Invoke();
            }
        }

        protected void Log(string message)
        {
            string unitId = Unit != null ? Unit.Identity.netId.ToString() : "Undefined";
            _logger.Log($"[Unit {unitId}] {message}");
        }
    }
}