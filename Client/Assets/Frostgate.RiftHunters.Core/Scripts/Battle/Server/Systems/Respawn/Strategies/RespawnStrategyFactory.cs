using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints;
using Frostgate.RiftHunters.Core.Battle.Shared.Invincibility;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public sealed class RespawnStrategyFactory
    {
        [NotNull] private readonly IInvincibleService _invincibleService;
        [NotNull] private readonly IRespawnUnitService _respawnUnitService;
        [NotNull] private readonly UnitPlayerPointCalculator _pointCalculator;

        public RespawnStrategyFactory(
            [NotNull] IInvincibleService invincibleService,
            [NotNull] IRespawnUnitService respawnUnitService,
            [NotNull] UnitPlayerPointCalculator pointCalculator)
        {
            _invincibleService = invincibleService;
            _respawnUnitService = respawnUnitService;
            _pointCalculator = pointCalculator;
        }

        [NotNull]
        public IRespawnStrategy Create(TimeSpan invincibleDuration) =>
            new PlayerRespawnStrategy(invincibleDuration, _invincibleService,
                _respawnUnitService, _pointCalculator);
    }
}