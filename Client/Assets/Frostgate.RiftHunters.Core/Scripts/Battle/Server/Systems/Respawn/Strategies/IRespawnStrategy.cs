using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public interface IRespawnStrategy
    {
        void Respawn([CanBeNull] UnitNetwork unit);
    }
}
