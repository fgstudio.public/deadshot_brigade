using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public sealed class UnitDeadHandler : IDisposable
    {
        [CanBeNull] private readonly UnitNetwork _unit;
        [NotNull] private readonly BaseAwaiter _awaiter;
        [NotNull] private readonly IRespawnStrategy _respawnStrategy;
        [NotNull] private readonly UnitAwaiterHandler _unitAwaiterHandler;

        public UnitDeadHandler(
            [NotNull] UnitNetwork unit,
            [NotNull] BaseAwaiter awaiter,
            [NotNull] IRespawnStrategy respawnStrategy,
            [NotNull] UnitAwaiterHandler awaiterHandler)
        {
            _unit = unit;
            _awaiter = awaiter;
            _respawnStrategy = respawnStrategy;
            _unitAwaiterHandler = awaiterHandler;

            _awaiter.Done += OnAwaiterDone;
            if (_awaiter.IsDone)
                OnAwaiterDone();
        }

        public void Dispose() =>
            DisposeAwaiter(_awaiter);

        private void OnAwaiterDone()
        {
            _respawnStrategy.Respawn(_unit);
            DisposeAwaiter(_awaiter);
        }

        private void DisposeAwaiter([NotNull] BaseAwaiter awaiter)
        {
            _unitAwaiterHandler.Dispose();
            awaiter.Done -= OnAwaiterDone;
            awaiter.Dispose();
        }
    }
}