using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn
{
    public sealed class ByTimerAwaiter : BaseAwaiter
    {
        public event Action<TimeSpan> Paused;
        public event Action<TimeSpan> Continued;

        [NotNull] private readonly ITimer _timer;

        public TimeSpan Interval => _timer.Interval;
        public TimeSpan Left => _timer.Left;

        public ByTimerAwaiter([NotNull] UnitNetwork unit, [NotNull] ILogger logger, TimeSpan timerInterval)
            : base(unit,logger)
        {
            _timer = new Timer(timerInterval);
            _timer.Start();

            _timer.Elapsed += SetDone;
            unit.ReanimationMechanic.Activated.AddListener(OnReanimationActivated);
            unit.ReanimationMechanic.Restored.AddListener(OnReanimationDeactivated);

            if (unit.ReanimationMechanic.IsActive)
                OnReanimationActivated();

            if (_timer.IsElapsed)
                SetDone();
        }

        protected override void OnDisposed()
        {
            if (Unit != null)
            {
                Unit.ReanimationMechanic.Activated.RemoveListener(OnReanimationActivated);
                Unit.ReanimationMechanic.Restored.RemoveListener(OnReanimationDeactivated);
            }

            _timer.Elapsed -= SetDone;
            _timer.Dispose();
        }

        private void OnReanimationActivated()
        {
            _timer.Pause();
            Paused?.Invoke(_timer.Left);
            Log($"Paused when left {_timer.Left.TotalSeconds:F1} sec");
        }

        private void OnReanimationDeactivated()
        {
            _timer.Start();
            Continued?.Invoke(_timer.Left);
            Log($"Continued when left {_timer.Left.TotalSeconds:F1} sec");
        }
    }
}