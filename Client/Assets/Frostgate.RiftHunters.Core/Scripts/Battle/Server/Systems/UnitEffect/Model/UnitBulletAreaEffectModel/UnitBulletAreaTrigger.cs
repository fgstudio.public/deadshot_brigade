using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect
{
    public class UnitBulletAreaTrigger : MonoBehaviour
    {
        private UnitEffectConfig[] _effects;
        private IObjectCollection<Collider, UnitNetwork> _units;
        private UnitEffectStateFactory _factory;
        private UnitNetwork _sourceUnit;

        public void Init(UnitEffectConfig[] effects, IObjectCollection<Collider, UnitNetwork> units, UnitEffectStateFactory factory, UnitNetwork sourceUnit)
        {
            _sourceUnit = sourceUnit;
            _effects = effects;
            _units = units;
            _factory = factory;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!_units.Contains(other))
                return;

            var unit = _units[other];

            if (unit.IsFriendly(_sourceUnit))
                return;

            foreach (var unitEffectConfig in _effects)
            {
                var state = _factory.Create(unitEffectConfig, _sourceUnit.NetworkIdentity);
                unit.AddUnitEffect(state);
            }
        }
    }
}