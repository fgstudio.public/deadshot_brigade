﻿using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect
{
    public class UnitInfiniteAmmoEffectModel : UnitEffectModel
    {
        public UnitInfiniteAmmoEffectModel(
            [NotNull] UnitEffectState state,
            [NotNull] UnitNetwork unitNetwork) : base(state)
        {
            if (unitNetwork.UnitNetworkState.BehaviourState == BehaviourState.Reload)
                unitNetwork.UnitNetworkState.BehaviourState = BehaviourState.None;

            unitNetwork.UnitNetworkState.PatronsCount = unitNetwork.UnitNetworkState.MaxPatrons;
        }
    }
}