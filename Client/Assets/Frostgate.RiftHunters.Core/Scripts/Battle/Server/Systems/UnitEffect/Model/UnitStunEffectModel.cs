using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect
{
    public class UnitStunEffectModel : UnitEffectModel
    {
        public override bool IsEffectInterrupted => _targetUnit.BehaviourState == BehaviourState.Dead;
        private readonly UnitNetworkState _targetUnit;

        public UnitStunEffectModel(UnitEffectState state, UnitNetworkState unitState) : base(state)
        {
            _targetUnit = unitState;
            _targetUnit.BehaviourState = BehaviourState.Stun;
            InterruptAllCastingImpacts(_targetUnit.UnitNetwork.ImpactRoster);
        }

        public override void Dispose()
        {
            base.Dispose();

            if (_targetUnit.BehaviourState == BehaviourState.Stun)
                _targetUnit.BehaviourState = BehaviourState.None;
        }

        protected override void OnTick()
        {
            if (_targetUnit.BehaviourState == BehaviourState.None)
                _targetUnit.BehaviourState = BehaviourState.Stun;
        }

        private void InterruptAllCastingImpacts(IServerImpactModelRoster impactModelRoster)
        {
            foreach (IServerImpactModel impactModel in impactModelRoster.AllImpacts)
                if (impactModel.IsCasting)
                    impactModel.Interrupt();
        }
    }
}