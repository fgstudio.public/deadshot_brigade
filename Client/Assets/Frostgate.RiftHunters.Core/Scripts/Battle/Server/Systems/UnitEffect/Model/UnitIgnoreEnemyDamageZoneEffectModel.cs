using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect
{
    public class UnitIgnoreEnemyDamageZoneEffectModel : UnitEffectModel
    {
        public override bool IsEffectInterrupted => _shootCount >= _shootCountLimit;

        [NotNull] private readonly IShootingSharedLogic _shootingSharedLogic;
        private readonly float _shootCountLimit;
        private int _shootCount;

        public UnitIgnoreEnemyDamageZoneEffectModel(
            [NotNull] UnitEffectState state, [NotNull] UnitNetwork unit) : base(state)
        {
            _shootCountLimit = (state.Config?.Value ?? 1) * (unit.RangeWeapon?.PatronsCount ?? 1);
            _shootingSharedLogic = unit.ShootingSharedLogic;
            _shootingSharedLogic.OnShoot += OnShoot;
        }

        public override void Dispose() => _shootingSharedLogic.OnShoot -= OnShoot;
        private void OnShoot() => _shootCount++;
    }
}