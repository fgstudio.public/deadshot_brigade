using System;
using System.Threading;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect
{
    public class UnitFireDamageEffectModel : UnitEffectModel
    {
        [NotNull] private readonly IDamageService _damageService;
        [NotNull] private readonly CancellationTokenSource _tokenSource;

        public UnitFireDamageEffectModel([NotNull] UnitEffectState state,
            [NotNull] UnitNetwork unit, [NotNull] IDamageService damageService) : base(state)
        {
            _damageService = damageService;

            var damage = state.Config!.Value * unit.UnitNetworkState.MaxHealth;
            DamageSourceDTO dto = new(damage, DamageType.Fire);
            var interval = TimeSpan.FromSeconds(state.Config.Interval);

            _tokenSource = new CancellationTokenSource();

            DoRepeatableDamageAsync(dto, interval, unit, _tokenSource.Token)
                .AsAsyncUnitUniTask();
        }

        public override void Dispose()
        {
            _tokenSource.Cancel();
            _tokenSource.Dispose();
        }

        private async UniTask DoRepeatableDamageAsync(DamageSourceDTO dto,
            TimeSpan interval, [CanBeNull] UnitNetwork unit, CancellationToken token)
        {
            while (!token.IsCancellationRequested && unit != null)
            {
                if (!unit.UnitNetworkState.IsDead)
                    _damageService.TakeDamage(dto, unit, unit, default);

                await UniTask.Delay(interval, cancellationToken: token);
            }
        }
    }
}