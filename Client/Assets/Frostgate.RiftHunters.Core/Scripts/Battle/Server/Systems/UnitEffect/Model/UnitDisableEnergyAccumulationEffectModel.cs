using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect
{
    public class UnitDisableEnergyAccumulationEffectModel : UnitEffectModel
    {
        [CanBeNull] private readonly UnitNetwork _targetUnit;

        public UnitDisableEnergyAccumulationEffectModel(
            [NotNull] UnitEffectState state, [NotNull] UnitNetwork targetUnit) : base(state)
        {
            _targetUnit = targetUnit;
            _targetUnit.UnitCollectEnergyCapsule.Disable();
        }

        public override void Dispose()
        {
            _targetUnit?.UnitCollectEnergyCapsule.Enable();
            base.Dispose();
        }
    }
}
