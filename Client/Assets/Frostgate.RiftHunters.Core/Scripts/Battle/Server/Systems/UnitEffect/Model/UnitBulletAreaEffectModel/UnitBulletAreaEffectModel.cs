using System;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect
{
    public class UnitBulletAreaEffectModel : UnitEffectModel, IBulletProcessor
    {
        [NotNull] private readonly BulletProcessors _bulletProcessors;
        [CanBeNull] private readonly UnitNetwork _unit;
        [NotNull] private readonly GameObject _areaObject;

        private const int Layer = 30;
        private const int LayerMask = 1 << Layer;

        public UnitBulletAreaEffectModel([NotNull] UnitEffectState state,
            [NotNull] UnitNetwork unit, [NotNull] BulletProcessors bulletProcessors,
            IObjectCollection<Collider, UnitNetwork> units, UnitEffectStateFactory factory) : base(state)
        {
            _unit = unit;

            Vector3 dir = Vector3.forward.RotateY(state.RotationY);
            BulletAreaData data = State.Config?.BulletAreaData;
            float forwardOffset = data?.ForwardOffset ?? default;

            _areaObject = new GameObject("BulletArea")
            {
                layer = Layer,
                transform =
                {
                    position = state.Position + dir * forwardOffset,
                    eulerAngles = new Vector3(0, state.RotationY, 0)
                }
            };

            var collider = _areaObject.AddComponent<BoxCollider>();
            collider.size = data?.Size ?? Vector3.zero;

            _bulletProcessors = bulletProcessors;
            _bulletProcessors.Add(this);

            var triggerObject = new GameObject("BulletArea")
            {
                layer = PhysicsLayers.UnitIndex,
                transform =
                {
                    position = _areaObject.transform.position,
                    eulerAngles = _areaObject.transform.eulerAngles,
                    parent = _areaObject.transform
                }
            };

            var triggerCollider = triggerObject.AddComponent<BoxCollider>();
            triggerCollider.size = data?.Size ?? Vector3.zero;
            triggerCollider.isTrigger = true;

            var trigger = triggerObject.AddComponent<UnitBulletAreaTrigger>();
            trigger.Init(State.Config?.Effects, units, factory, unit);
        }

        void IBulletProcessor.Process(BulletData bullet, UnitNetwork unit, ref float step)
        {
            if (_unit == null) return;
            if (!ShootingHelper.IsFriendly(_unit, unit)) return;

            if (!Physics.Raycast(bullet.Position, bullet.Direction, out RaycastHit rh, step, LayerMask))
                return;

            if (rh.transform.gameObject != _areaObject)
                return;

            bullet.Effects.AddRange(State.Config?.Effects ?? Array.Empty<UnitEffectConfig>());
        }

        public override void Dispose()
        {
            _bulletProcessors.Remove(this);
            Object.Destroy(_areaObject);
        }
    }
}