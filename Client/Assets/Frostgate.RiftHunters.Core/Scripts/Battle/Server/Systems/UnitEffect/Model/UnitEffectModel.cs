using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect
{
    public class UnitEffectModel
    {
        [NotNull] public UnitEffectState State { get; }
        public bool IsEffectDone => IsEffectExpired || IsEffectInterrupted;
        public bool IsEffectExpired => _currentTime > State.EndTime;
        public virtual bool IsEffectInterrupted => false;

        private float _currentTime;

        public UnitEffectModel([NotNull] UnitEffectState state) =>
            State = state;

        public virtual void Dispose()
        {
        }

        public void Tick(float time)
        {
            _currentTime = time;
            OnTick();
        }

        protected virtual void OnTick()
        {
        }
    }
}