using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect
{
    public sealed class UnitEffectsController : IDisposable
    {
        [CanBeNull] private readonly UnitNetwork _unitNetwork;

        [NotNull] private readonly SimulationTime _time;
        [NotNull] private readonly IDamageService _damageService;
        [NotNull] private readonly UnitEffectStateFactory _effectStateFactory;
        [NotNull] private readonly UnitEffectModelFactory _unitEffectModelFactory;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, IAimTarget> _aimTargets;

        [NotNull] private readonly List<UnitEffectState> _removeList = new();
        [NotNull] private readonly Dictionary<UnitEffectState, UnitEffectModel> _models = new();

        public UnitEffectsController([NotNull] UnitNetwork unitNetwork,
            [NotNull] IDamageService damageService, [NotNull] SimulationTime time,
            [NotNull] IReadOnlyObjectCollection<uint, IAimTarget> aimTargets,
            [NotNull] UnitEffectStateFactory effectStateFactory, UnitEffectModelFactory unitEffectModelFactory)
        {
            _aimTargets = aimTargets;
            _unitNetwork = unitNetwork;
            _damageService = damageService;
            _effectStateFactory = effectStateFactory;
            _unitEffectModelFactory = unitEffectModelFactory;

            _time = time;
            _time.OnUpdate += Tick;
            _damageService.Damaged.AddListener(OnDamaged);

            unitNetwork.UnitNetworkEffects.OnAdd += OnAddEffect;
            unitNetwork.UnitNetworkEffects.OnRemove += OnRemoveEffect;
        }

        public void Dispose()
        {
            _time.OnUpdate -= Tick;
            _damageService.Damaged.RemoveListener(OnDamaged);

            if (_unitNetwork != null)
            {
                _unitNetwork.UnitNetworkEffects.OnAdd -= OnAddEffect;
                _unitNetwork.UnitNetworkEffects.OnRemove -= OnRemoveEffect;
            }

            _models.Keys.ToArray()
                .ForEach(OnRemoveEffect);
        }

        private void OnAddEffect([NotNull] UnitEffectState state)
        {
            UnitEffectModel model = _unitEffectModelFactory.Create(state, _unitNetwork);
            _models.Add(state, model);
        }

        private void OnRemoveEffect([NotNull] UnitEffectState state)
        {
            if (!_models.ContainsKey(state))
            {
                var msg = $"{nameof(UnitEffectsController)} {nameof(OnRemoveEffect)} state not contains in collection";
                UnityEngine.Debug.LogWarning(msg);
                return;
            }

            _models[state].Dispose();
            _models.Remove(state);
        }

        private void Tick(float deltaTime)
        {
            foreach (UnitEffectModel model in _models.Values)
                model.Tick(_time.Time);
            RemoveDoneEffects();
        }

        private void RemoveDoneEffects()
        {
            _removeList.Clear();

            foreach (UnitEffectModel model in _models.Values)
                if (model.IsEffectDone)
                    _removeList.Add(model.State);

            if (_unitNetwork != null)
                foreach (UnitEffectState state in _removeList)
                    _unitNetwork.UnitNetworkEffects.Remove(state);
        }

        private void OnDamaged(DamageEventData data)
        {
            if (_unitNetwork == null) return;
            if (_unitNetwork.CurrentWeapon == null) return;
            if (data.CasterNetId != _unitNetwork.Identity.netId) return;
            if (_aimTargets.TryGet(data.ReceiverNetId, out IAimTarget receiver)) return;

            foreach (UnitEffectConfig config in _unitNetwork.CurrentWeapon.UnitEffects)
                Execute(_unitNetwork, config, receiver);
        }

        private void Execute([NotNull] UnitNetwork sourceUnit,
            [NotNull] UnitEffectConfig effectConfig, [CanBeNull] IUnitEffectReceiver target)
        {
            if (sourceUnit.Identity == null) return;
            UnitEffectState effect = _effectStateFactory.Create(effectConfig, sourceUnit.Identity);

            switch (effectConfig.Target)
            {
                case UnitEffectTarget.EnemiesInRadius:
                    AddEffectOnEnemiesInRadius(sourceUnit, effectConfig, effect);
                    break;
                case UnitEffectTarget.FriendsInRadius:
                    AddEffectOnFriendsInRadius(sourceUnit, effectConfig, effect);
                    break;
                case UnitEffectTarget.Target:
                    if (target != null)
                        target.AddUnitEffect(effect);
                    break;
                case UnitEffectTarget.Self:
                    sourceUnit.UnitNetworkEffects.Add(effect);
                    break;
                case UnitEffectTarget.SelfAndFriendsInRadius:
                    sourceUnit.UnitNetworkEffects.Add(effect);
                    AddEffectOnFriendsInRadius(sourceUnit, effectConfig, effect);
                    break;
            }
        }

        private void AddEffectOnFriendsInRadius([NotNull] UnitNetwork sourceUnit,
            [NotNull] UnitEffectConfig effectConfig, [NotNull] UnitEffectState effectState)
        {
            foreach (IUnitEffectReceiver receiver in GetUnitsInRadius(sourceUnit, effectConfig))
                if (receiver.IsFriendly(sourceUnit))
                    receiver.AddUnitEffect(effectState);
        }

        private void AddEffectOnEnemiesInRadius([NotNull] UnitNetwork sourceUnit,
            [NotNull] UnitEffectConfig effectConfig, [NotNull] UnitEffectState effectState)
        {
            foreach (IUnitEffectReceiver receiver in GetUnitsInRadius(sourceUnit, effectConfig))
                if (!receiver.IsFriendly(sourceUnit))
                    receiver.AddUnitEffect(effectState);
        }

        [NotNull, ItemNotNull]
        private IEnumerable<IUnitEffectReceiver> GetUnitsInRadius(
            [NotNull] UnitNetwork sourceUnit, [NotNull] UnitEffectConfig effectConfig)
        {
            Vector3 pos = sourceUnit.transform.position;
            float radius = effectConfig.Radius;
            int count = Physics.SphereCastNonAlloc(pos, radius, Vector3.up,
                PhysicHelper.HitResults, 0f, PhysicsLayers.UnitMask);

            for (int i = 0; i < count; i++)
            {
                RaycastHit hit = PhysicHelper.HitResults[i];
                IUnitEffectReceiver receiver = hit.collider.GetComponentInParent<IUnitEffectReceiver>();

                if (receiver != null && receiver.CanReceiveUnitEffect(effectConfig))
                    yield return receiver;
            }
        }
    }
}