using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect
{
    public class UnitAgroEffectModel : UnitEffectModel
    {
        public override bool IsEffectInterrupted => _targetUnit == null || _targetUnit.IsDead;
        [CanBeNull] private readonly UnitNetworkState _targetUnit;
        public UnitAgroEffectModel([NotNull] UnitEffectState state) : base(state) =>
            _targetUnit = state.Target?.GetComponent<UnitNetworkState>();
    }
}