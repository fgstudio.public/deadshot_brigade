﻿using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect
{
    public sealed class UnitEffectModelFactory
    {
        private readonly BulletProcessors _bulletProcessors;
        private readonly IDamageService _damageService;
        private readonly UnitEffectStateFactory _factory;
        private readonly ObjectCollectionRepository<UnitNetwork> _unitCollections;

        public UnitEffectModelFactory(BulletProcessors bulletProcessors, IDamageService damageService,
            ObjectCollectionRepository<UnitNetwork> unitCollections, UnitEffectStateFactory factory)
        {
            _bulletProcessors = bulletProcessors;
            _damageService = damageService;
            _unitCollections = unitCollections;
            _factory = factory;
        }

        public UnitEffectModel Create(
            [NotNull] UnitEffectState state, [NotNull] UnitNetwork unit) =>
            state.Config!.Type switch
            {
                UnitEffectTypes.Agro => new UnitAgroEffectModel(state),
                UnitEffectTypes.Stun => new UnitStunEffectModel(state, unit.UnitNetworkState),
                UnitEffectTypes.FireDamage => new UnitFireDamageEffectModel(state, unit, _damageService),
                UnitEffectTypes.BulletArea => new UnitBulletAreaEffectModel(state, unit, _bulletProcessors, _unitCollections.Get<Collider>(), _factory),
                UnitEffectTypes.IgnoreEnemyDamageZone => new UnitIgnoreEnemyDamageZoneEffectModel(state, unit),
                UnitEffectTypes.DisableEnergyAccumulation => new UnitDisableEnergyAccumulationEffectModel(state, unit),
                UnitEffectTypes.InfiniteAmmo => new UnitInfiniteAmmoEffectModel(state, unit),
                _ => new UnitEffectModel(state)
            };
    }
}