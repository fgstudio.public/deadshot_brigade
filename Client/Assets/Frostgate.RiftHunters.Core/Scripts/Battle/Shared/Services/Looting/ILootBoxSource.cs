using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Looting
{
    public interface ILootBoxSource : INetworkIdentifiable
    {
        Transform Transform { get; }
        LootSetConfig LootSetForDeath { get; }
    }
}
