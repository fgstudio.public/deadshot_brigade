using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Looting
{
    public interface ILootBoxCollector : IUnitEffectReceiver
    {
        ILootBoxCollectorState State { get; }
    }
}
