using Mirror;
using Zenject;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Looting
{
    public interface ICollectLootBoxService
    {
        [Server] void Collect([NotNull] ILootBoxCollector lootBoxCollector, [NotNull] IServerLootBox lootBox);
    }

    /// <summary>
    /// Сервис для подбора юнитом лутбокса.
    /// </summary>
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(CollectLootBoxService))]
    public sealed class CollectLootBoxService : NetworkBehaviour, ICollectLootBoxService
    {
        private UnitEffectStateFactory _effectStateFactory;

        [Inject]
        private void MonoConstructor([InjectOptional] UnitEffectStateFactory effectStateFactory) =>
            _effectStateFactory = effectStateFactory;

        [Server]
        public void Collect(ILootBoxCollector lootBoxCollector, IServerLootBox lootBox)
        {
            if (lootBoxCollector.Identity == null) return;

            lootBox.State.CollectorId = lootBoxCollector.Identity.netId;
            lootBoxCollector.State.LootBoxCounter.Increase(lootBox.State.Type);

            foreach (UnitEffectConfig effectConfig in lootBox.State.UnitEffects)
                ApplyEffect(effectConfig, lootBoxCollector.Identity, lootBoxCollector);
        }

        [Server]
        private void ApplyEffect([NotNull] UnitEffectConfig effect,
            [NotNull] NetworkIdentity source, [NotNull] IUnitEffectReceiver target)
        {
            UnitEffectState state = _effectStateFactory.Create(effect, source);
            target.AddUnitEffect(state);
        }
    }
}