using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Looting
{
    public interface ILootBoxCollectorState
    {
        ILootBoxStateCounter LootBoxCounter { get; }
    }

    public interface ILootBoxStateCounter : IReadOnlyLootBoxStateCounter
    {
        void Increase(LootBoxType type);
        void Decrease(LootBoxType type);
    }

    public interface IReadOnlyLootBoxStateCounter
    {
        event Action<LootBoxType> Changed;
        int GetCount(LootBoxType type);
    }
}
