﻿using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    [Serializable]
    public readonly struct DamageData
    {
        public readonly uint CasterId;
        public readonly uint ReceiverId;
        public readonly bool IsBlocked;
        public readonly bool IsCritical;
        public readonly bool IsFatal;
        public readonly float Damage;
        public readonly DamageType DamageType;
        public readonly float DamageZoneMultiplier;
        public readonly float DamageByDistanceMultiplier;

        public DamageData(uint casterId, uint receiverId, bool isBlocked, bool isCritical, bool isFatal,
            float damage, DamageType damageType, float damageZoneMultiplier, float damageByDistanceMultiplier)
        {
            CasterId = casterId;
            ReceiverId = receiverId;
            IsBlocked = isBlocked;
            IsCritical = isCritical;
            IsFatal = isFatal;
            Damage = damage;
            DamageType = damageType;
            DamageZoneMultiplier = damageZoneMultiplier;
            DamageByDistanceMultiplier = damageByDistanceMultiplier;
        }
    }
}