using System;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    public readonly struct DamageSourceDTO : IDamageSource
    {
        public DamageType Type { get; }
        public float BreakShieldChance { get; }
        public float CriticalDamageChance => 0;
        public float CriticalDamageMultiplier => 0;
        public IReadOnlyList<UnitActionDamageTrigger> UnitActionDamageTriggers { get; }

        private readonly float _damage;

        public float GetDamage(float distance = 0) => _damage;
        public float GetDistanceMultiplier(float distance = 0) => 1f;

        public DamageSourceDTO(float damage, DamageType type, float breakShieldChance = 1f)
            : this(damage, type, Array.Empty<UnitActionDamageTrigger>(), breakShieldChance) { }

        public DamageSourceDTO(float damage, DamageType type,
            [NotNull, ItemNotNull] IReadOnlyList<UnitActionDamageTrigger> actionTriggers,
            float breakShieldChance = 1f)
        {
            _damage = damage;
            Type = type;
            BreakShieldChance = breakShieldChance;
            UnitActionDamageTriggers = actionTriggers;
        }
    }
}