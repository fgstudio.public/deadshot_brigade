using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    public interface IDamageReceiverState : IHealth
    {
        float BodyRadius { get; }
        float Defense { get; }
        bool IsInvincible { get; }
    }

    public interface IDamageReceiver : IGameObject, IUnitEffectReceiver
    {
        IDamageReceiverState State { get; }
        bool DamageImmunity { get; }

        DamageZone GetDamageZone(Vector3 hitPoint, float breakShieldChance);
        DamageZone GetDamageZone(float zoneMultiplier);
        void TryExecuteUnitAction(UnitActionTypes actionType, Vector3 casterPosition);
        float GetDamageMultiplier(DamageType damageType);
    }
}