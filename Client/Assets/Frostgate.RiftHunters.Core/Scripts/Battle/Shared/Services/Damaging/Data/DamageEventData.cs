﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    public readonly struct DamageEventData
    {
        public uint CasterNetId { get; }
        public uint ReceiverNetId { get; }
        public float ZoneMultiplier { get; }
        public DamageType Type { get; }
        public float Damage { get; }
        public bool IsCritical { get; }
        public float DamageByDistanceMultiplier { get; }

        public DamageEventData(uint casterNetId, uint receiverNetId, DamageType type, float damage,
            bool isCritical, float zoneMultiplier, float distanceMultiplier)
        {
            CasterNetId = casterNetId;
            ReceiverNetId = receiverNetId;
            ZoneMultiplier = zoneMultiplier;
            Type = type;
            Damage = damage;
            IsCritical = isCritical;
            DamageByDistanceMultiplier = distanceMultiplier;
        }
    }
}