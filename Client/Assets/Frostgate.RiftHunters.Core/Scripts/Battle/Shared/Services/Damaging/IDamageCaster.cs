using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    public interface IDamageCasterState
    {
        Vector3 Position { get; }
    }

    public interface IDamageCaster : INetworkIdentifiable
    {
        public float AttackMultiplier { get; }
        IDamageCasterState State { get; }
        public bool PiercingBullets { get; }
        public bool IgnoreEnemyDamageZone { get; }
    }
}