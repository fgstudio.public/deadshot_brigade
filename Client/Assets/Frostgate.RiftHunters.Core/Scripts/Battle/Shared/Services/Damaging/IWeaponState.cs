﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    public interface IWeaponState
    {
        int PatronsCount { set; }
        int MaxPatrons { get; }
    }
}
