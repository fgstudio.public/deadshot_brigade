using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    public interface IUnitEffectReceiver : INetworkIdentifiable
    {
        bool CanReceiveUnitEffect(UnitEffectConfig config);
        bool IsFriendly(UnitNetwork sourceUnit);
        bool HasUnitEffect(UnitEffectState effect);
        void AddUnitEffect(UnitEffectState effect);
        void RemoveUnitEffect(UnitEffectState effect);
        bool TryGetUnitEffect(UnitEffectConfig config, out UnitEffectState state);
    }
}