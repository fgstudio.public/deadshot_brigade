using UnityEngine;
using Random = UnityEngine.Random;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    public class DamageCalculator
    {
        public DamageData Calc(IDamageSource source, IDamageCaster damageCaster, IDamageReceiver damageReceiver, Vector3 hitPoint)
        {
            DamageZone damageZone = damageReceiver.GetDamageZone(hitPoint, damageCaster.IgnoreEnemyDamageZone ? 1 : source.BreakShieldChance);

            uint casterId = damageCaster.Identity.netId;
            uint receiverId = damageReceiver.Identity.netId;

            bool isBlocked = damageZone.Multiplier == 0;
            bool isCritical = source.CriticalDamageChance > 0 && source.CriticalDamageChance >= Random.value;

            if (isBlocked)
                return new DamageData(casterId, receiverId, true, false, false, 0, source.Type, 0, 1);

            float damageMultiplier = source.Type == DamageType.Range ? damageZone.Multiplier : 1;
            damageMultiplier *= damageReceiver.GetDamageMultiplier(source.Type);
            if (isCritical) damageMultiplier *= source.CriticalDamageMultiplier;

            float distance = damageCaster.PiercingBullets ? 0 : Vector3.Distance(damageCaster.State.Position, hitPoint);
            float sourceDamage = source.GetDamage(distance);
            float distanceMultiplier = source.GetDistanceMultiplier(distance);
            float damage = Mathf.Max(0, sourceDamage * damageCaster.AttackMultiplier * damageMultiplier - damageReceiver.State.Defense);
            bool isFatal = Mathf.Approximately(0, Mathf.Max(0, damageReceiver.State.Health - damage));

            return new DamageData(casterId, receiverId, false, isCritical, isFatal, damage,
                source.Type, damageZone.Multiplier, distanceMultiplier);
        }
    }
}