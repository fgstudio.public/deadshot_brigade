﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    public readonly struct DiedEventData
    {
        public uint KillerId { get; }
        public uint DiedId { get; }

        public DiedEventData(uint killerId, uint diedId)
        {
            KillerId = killerId;
            DiedId = diedId;
        }
    }
}