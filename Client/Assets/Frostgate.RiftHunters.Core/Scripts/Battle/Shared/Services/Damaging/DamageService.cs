using Mirror;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    public interface IDamageService
    {
        UnityEvent<DamageEventData> Damaged { get; }
        UnityEvent<DiedEventData> Died { get; }
        UnityEvent<BlockEventData> Blocked { get; }

        void TakeDamage([NotNull] IDamageSource source, [NotNull] IDamageCaster caster, [NotNull] IDamageReceiver receiver, Vector3 hitPoint);
        void KillInstantly([NotNull] IDamageCaster caster, [NotNull] IDamageReceiver receiver);
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(DamageService))]
    public class DamageService : NetworkBehaviour, IDamageService
    {
        private const string EventsFoldoutName = "Events";

        [field: SerializeField, FoldoutGroup(EventsFoldoutName)]
        public UnityEvent<DamageEventData> Damaged { get; private set; }

        [field: SerializeField, FoldoutGroup(EventsFoldoutName)]
        public UnityEvent<DiedEventData> Died { get; private set; }

        [field: SerializeField, FoldoutGroup(EventsFoldoutName)]
        public UnityEvent<BlockEventData> Blocked { get; private set; }

        private readonly DamageCalculator _damageCalculator = new();
        private readonly ILogger _logger = LoggerFactory.CreateLogger<DamageService>();

        public void TakeDamage(IDamageSource source, IDamageCaster caster, IDamageReceiver receiver, Vector3 hitPoint)
        {
            if (receiver.DamageImmunity || receiver.State.IsInvincible || receiver.State.IsDead)
                return;

            DamageData damageData = _damageCalculator.Calc(source, caster, receiver, hitPoint);

            if (isServer)
            {
                if (!damageData.IsBlocked)
                    receiver.State.Health -= damageData.Damage;

                ActivateDamageSourceTriggers(source.UnitActionDamageTriggers, damageData, caster.State.Position,
                    receiver);
            }

            InvokeEvents(damageData, receiver);
        }

        [Server]
        public void KillInstantly(IDamageCaster caster, IDamageReceiver receiver)
        {
            float damage = receiver.State.Health;
            var damageData = new DamageData(caster.Identity.netId, receiver.Identity.netId, false, false, true, damage, DamageType.Fire, 1, 1);

            receiver.State.Health -= damage;
            InvokeEvents(damageData, receiver);
        }

        [Server]
        private void ActivateDamageSourceTriggers(IReadOnlyList<UnitActionDamageTrigger> sourceDamageTriggers,
            DamageData damageData, Vector3 casterPosition, IDamageReceiver receiver)
        {
            if (!damageData.IsBlocked)
                foreach (UnitActionDamageTrigger trigger in sourceDamageTriggers)
                    if ((damageData is { IsFatal: true } && trigger.IsFatalDamage) ||
                        (damageData is { IsFatal: false, IsCritical: true } && trigger.IsCriticalDamage) ||
                        (damageData is { IsFatal: false, IsCritical: false } && trigger.IsDamage))
                    {
                        receiver.TryExecuteUnitAction(trigger.ActionType, casterPosition);
                        _logger.Log($"Caster {damageData.CasterId} triggered Receiver {damageData.ReceiverId} for {trigger.ActionType:F}");
                    }
        }

        private void InvokeEvents(DamageData damageData, IDamageReceiver receiver)
        {
            uint casterId = damageData.CasterId;
            uint receiverId = damageData.ReceiverId;

            if (damageData.IsBlocked)
            {
                var eventData = new BlockEventData(casterId, receiverId, damageData.DamageZoneMultiplier);
                _logger.Log($"Damage from Caster {casterId} was blocked by Receiver {receiverId}");
                Blocked?.Invoke(eventData);
            }

            else
            {
                var eventData = new DamageEventData(casterId, receiver.Identity.netId, damageData.DamageType,
                    damageData.Damage, damageData.IsCritical, damageData.DamageZoneMultiplier, damageData.DamageByDistanceMultiplier);

                _logger.Log($"Caster {damageData.CasterId} took {damageData.Damage:F1} " +
                            $"{damageData.DamageType:F}-damage to Receiver {damageData.ReceiverId}");

                Damaged?.Invoke(eventData);
            }

            if (damageData.IsFatal)
            {
                var eventData = new DiedEventData(casterId, receiverId);
                _logger.Log($"Caster {casterId} killed Receiver {receiverId}");
                Died?.Invoke(eventData);
            }
        }
    }
}