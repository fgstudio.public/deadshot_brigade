﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    public readonly struct BlockEventData
    {
        public uint CasterId { get; }
        public uint ReceiverId { get; }
        public float ZoneMultiplier { get; }

        public BlockEventData(uint casterId, uint receiverId, float zoneMultiplier)
        {
            CasterId = casterId;
            ReceiverId = receiverId;
            ZoneMultiplier = zoneMultiplier;
        }
    }
}