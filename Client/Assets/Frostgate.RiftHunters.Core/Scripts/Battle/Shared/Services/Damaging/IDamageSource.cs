using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    public interface IDamageSource
    {
        public DamageType Type { get; }
        public float BreakShieldChance { get; }
        public float CriticalDamageChance { get; }
        public float CriticalDamageMultiplier { get; }
        public IReadOnlyList<UnitActionDamageTrigger> UnitActionDamageTriggers { get; }

        float GetDamage(float distance = 0);
        float GetDistanceMultiplier(float distance = 0);
    }
}