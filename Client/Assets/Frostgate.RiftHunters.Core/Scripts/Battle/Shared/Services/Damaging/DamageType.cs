namespace Frostgate.RiftHunters.Core.Battle.Shared.Damaging
{
    public enum DamageType : byte
    {
        Melee = 0,
        Range = 1,
        Ability = 2,
        Fire = 3,
        Replacement = 4,
        ReplacementX2 = 5,
        ReplacementX3 = 6,
        ReplacementX4 = 7,
        Special = 8
    }
}