namespace Frostgate.RiftHunters.Core.Battle.Shared.Exp
{
    public interface IExpReceiver : INetworkIdentifiable, IFxHolder
    {
        IExpReceiverState State { get; }
        BattleUnitType UnitType { get; }
    }
}