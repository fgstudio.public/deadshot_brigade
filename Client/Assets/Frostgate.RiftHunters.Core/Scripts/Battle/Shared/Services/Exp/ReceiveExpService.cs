using System;
using JetBrains.Annotations;
using Mirror;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Exp
{
    public interface IReceiveExpService
    {
        UnityEvent<ExpEventData> Received { get; }

        [Server] void Receive([NotNull]IExpReceiver expReceiver, [NotNull]IExpSource expSource);
    }

    /// <summary>
    /// Сервис для получения юнитом опыта за убийство.
    /// <see cref="https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#f6918c33089f4e14b33e639b0ad8b2c0"/>
    /// </summary>
    public sealed class ReceiveExpService : NetworkBehaviour, IReceiveExpService
    {
        [field:SerializeField] public UnityEvent<ExpEventData> Received { get; private set; }

        private readonly ILogger _logger = LoggerFactory.CreateLogger<ReceiveExpService>();

        [Server]
        public void Receive(IExpReceiver expReceiver, IExpSource expSource)
        {
            var eventData = CreateEventData(expReceiver, expSource);
            expReceiver.State.Exp += eventData.Exp;
            HandleEventData(eventData);
        }

        [Server]
        private static ExpEventData CreateEventData(IExpReceiver expReceiver, IExpSource expSource)
        {
            int expValue = Math.Max(0, expSource.ExpReward);
            return new ExpEventData(expReceiver.Identity.netId, expValue);
        }

        [Server]
        private void HandleEventData(ExpEventData eventData)
        {
            InvokeEvent(eventData);
            if (isServerOnly) InvokeEventRpc(eventData);
        }

        [ClientRpc]
        private void InvokeEventRpc(ExpEventData eventData) =>
            InvokeEvent(eventData);

        private void InvokeEvent(ExpEventData eventData)
        {
            const float logThreshold = 0.1f;

            if (eventData.Exp >= logThreshold)
                _logger.Log($"Receiver {eventData.ReceiverId} received {eventData.Exp} exp");

            Received?.Invoke(eventData);
        }
    }
}