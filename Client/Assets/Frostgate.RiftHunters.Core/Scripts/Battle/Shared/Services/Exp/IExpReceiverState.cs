namespace Frostgate.RiftHunters.Core.Battle.Shared.Exp
{
    public interface IExpReceiverState
    {
        int Exp { get; set; }
    }
}
