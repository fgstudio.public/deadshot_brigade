﻿using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Exp
{
    [Serializable]
    public readonly struct ExpEventData
    {
        public readonly uint ReceiverId;
        public readonly int Exp;

        public ExpEventData(uint receiverId, int exp)
        {
            ReceiverId = receiverId;
            Exp = exp;
        }
    }
}