namespace Frostgate.RiftHunters.Core.Battle.Shared.Exp
{
    public interface IExpSource
    {
        int ExpReward { get; }
    }
}