using System;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public class SimulationTime : NetworkBehaviour
    {
        public event Action<float> OnUpdate;

        public float Time => _time;
        public TimeSpan TimeSpan => TimeSpan.FromSeconds(_time);

        [SyncVar]
        private float _time;

        private void FixedUpdate()
        {
            _time += UnityEngine.Time.fixedDeltaTime;
            OnUpdate?.Invoke(UnityEngine.Time.fixedDeltaTime);
        }
    }
}