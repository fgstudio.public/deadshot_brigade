using Frostgate.RiftHunters.Core.Battle.Shared.Healing;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Reanimation
{
    public interface IReanimated : INetworkIdentifiable
    {
        IHealingState State { get; }
    }
}
