using Mirror;
using Zenject;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Reanimation
{
    public interface IReanimationService
    {
        UnityEvent<uint> Reanimated { get; }
        [Server] void Reanimate([NotNull] IReanimated reanimated);
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e23a099eb137443b8a16bd7b023dc6d8")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(ReanimationService))]
    public sealed class ReanimationService : NetworkBehaviour, IReanimationService
    {
        private const string EventsFoldoutName = "Events";

        [field: SerializeField, FoldoutGroup(EventsFoldoutName)]
        public UnityEvent<uint> Reanimated { get; private set; }

        private ReanimationSettings _reanimationSettings;

        [Inject]
        private void MonoConstructor(ReanimationSettings reanimationSettings) =>
            _reanimationSettings = reanimationSettings;

        [Server]
        public void Reanimate(IReanimated reanimated)
        {
            reanimated.State.Health = reanimated.State.MaxHealth * _reanimationSettings.ReanimatedHealthRatio;

            InvokeOnReanimatedEvent(reanimated.Identity.netId);
            RpcOnReanimated(reanimated.Identity.netId);
        }

        [ClientRpc]
        private void RpcOnReanimated(uint reanimatedId)
        {
            if (isClientOnly)
                InvokeOnReanimatedEvent(reanimatedId);
        }

        private void InvokeOnReanimatedEvent(uint reanimatedId) =>
            Reanimated?.Invoke(reanimatedId);
    }
}