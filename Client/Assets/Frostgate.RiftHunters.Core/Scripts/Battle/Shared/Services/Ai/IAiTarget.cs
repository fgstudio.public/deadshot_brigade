using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Ai
{
    public interface IAiTarget
    {
        CapsuleCollider AimTarget { get; }
    }
}