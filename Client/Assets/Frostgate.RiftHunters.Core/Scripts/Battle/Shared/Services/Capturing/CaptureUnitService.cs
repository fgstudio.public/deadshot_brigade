using Mirror;
using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    public interface ICaptureUnitService
    {
        [Server] void Capture([NotNull] ICaptured captured);
        [Server] void Release([NotNull] ICaptured released);
    }

    /// <summary>
    /// Сервис для обездвиживания юнитов.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Server.Menu + "/" + nameof(CaptureUnitService))]
    public sealed class CaptureUnitService : NetworkBehaviour, ICaptureUnitService
    {
        private readonly ILogger _logger = LoggerFactory.CreateLogger<CaptureUnitService>();

        [Server]
        public void Capture(ICaptured captured)
        {
            captured.State.IsCaptured = true;
            _logger.Log($"Unit {captured.Identity.netId} was captured");
        }

        [Server]
        public void Release(ICaptured released)
        {
            released.State.IsCaptured = false;
            _logger.Log($"Unit {released.Identity.netId} was released");
        }
    }
}