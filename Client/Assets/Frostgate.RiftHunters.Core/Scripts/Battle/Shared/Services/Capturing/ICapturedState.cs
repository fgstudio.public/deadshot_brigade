namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    public interface ICapturedState
    {
        bool IsCaptured { get; set; }
    }
}
