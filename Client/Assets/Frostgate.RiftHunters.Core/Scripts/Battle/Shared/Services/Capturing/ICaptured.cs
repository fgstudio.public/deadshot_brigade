namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    public interface ICaptured : INetworkIdentifiable
    {
        ICapturedState State { get; }
    }
}
