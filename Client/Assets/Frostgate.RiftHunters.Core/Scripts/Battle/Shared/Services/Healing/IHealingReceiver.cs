namespace Frostgate.RiftHunters.Core.Battle.Shared.Healing
{
    public interface IHealingReceiver : INetworkIdentifiable
    {
        IHealingState State { get; }
    }
}