namespace Frostgate.RiftHunters.Core.Battle.Shared.Healing
{
    public interface IHealingState
    {
        float Health { get; set; }
        float MaxHealth { get; }

        bool IsDead { get; }
        bool IsHealthFull { get; }
    }
}