namespace Frostgate.RiftHunters.Core.Battle.Shared.Healing
{
    public interface IHealingSource
    {
        public HealingType Type { get; }
        public float Healing { get; }
    }
}
