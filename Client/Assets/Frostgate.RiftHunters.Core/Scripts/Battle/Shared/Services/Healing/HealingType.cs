namespace Frostgate.RiftHunters.Core.Battle.Shared.Healing
{
    public enum HealingType : byte
    {
        AutoRecovery,
        Ability,
        HealPack
    }
}