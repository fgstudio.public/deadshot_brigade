namespace Frostgate.RiftHunters.Core.Battle.Shared.Healing
{
    public readonly struct HealingSourceDTO : IHealingSource
    {
        public float Healing { get; }
        public HealingType Type { get; }

        public HealingSourceDTO(float healing, HealingType type)
        {
            Healing = healing;
            Type = type;
        }
    }
}
