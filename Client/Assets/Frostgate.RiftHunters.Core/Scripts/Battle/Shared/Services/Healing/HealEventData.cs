﻿using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Healing
{
    [Serializable]
    public readonly struct HealEventData
    {
        public uint CasterId { get; }
        public uint ReceiverId { get; }
        public HealingType Type { get; }
        public float Heal { get; }

        public HealEventData(uint casterId, uint receiverId, HealingType type, float heal)
        {
            CasterId = casterId;
            ReceiverId = receiverId;
            Type = type;
            Heal = heal;
        }
    }
}