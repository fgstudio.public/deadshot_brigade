namespace Frostgate.RiftHunters.Core.Battle.Shared.Healing
{
    public enum TimeType
    {
        Immediately,
        Continuously
    }
}
