using Mirror;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Healing
{
    public interface IHealingService
    {
        UnityEvent<HealEventData> Healed { get; }

        [Server] void Heal([NotNull]IHealingSource source, [NotNull]IHealingCaster caster, [NotNull]IHealingReceiver receiver);
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(HealingService))]
    public sealed class HealingService : NetworkBehaviour, IHealingService
    {
        [field: SerializeField, FoldoutGroup("Events")]
        public UnityEvent<HealEventData> Healed { get; private set; }

        private readonly ILogger _logger = LoggerFactory.CreateLogger<HealingService>();

        [Server]
        public void Heal(IHealingSource source, IHealingCaster caster, IHealingReceiver receiver)
        {
            if (!(receiver.State.Health >= receiver.State.MaxHealth))
            {
                receiver.State.Health += source.Healing;
                var eventData = CreateEventData(source, caster, receiver);
                HandleEventData(eventData);
            }
        }

        [Server]
        private static HealEventData CreateEventData(IHealingSource source, IHealingCaster caster, IHealingReceiver receiver) =>
            new(caster.Identity.netId, receiver.Identity.netId, source.Type, source.Healing);

        [Server]
        private void HandleEventData(HealEventData eventData)
        {
            InvokeEvent(eventData);
            if (isServerOnly) InvokeEventRpc(eventData);
        }

        [ClientRpc]
        private void InvokeEventRpc(HealEventData eventData) =>
            InvokeEvent(eventData);

        private void InvokeEvent(HealEventData eventData)
        {
            const float logThreshold = 0.1f;

            if (Mathf.Abs(eventData.Heal) >= logThreshold)
                _logger.Log($"Caster {eventData.CasterId} healed Receiver {eventData.ReceiverId} " +
                            $"for {eventData.Heal:00} by {eventData.Type:F}");

            Healed?.Invoke(eventData);
        }
    }
}