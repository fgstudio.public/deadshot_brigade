using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Energy;
using Frostgate.RiftHunters.Core.Battle.Shared.Healing;
using Frostgate.RiftHunters.Core.Battle.Shared.Stamina;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Respawn
{
    public interface IRespawnableState : IHealingState, IEnergyReceiverState, IWeaponState, IHavingStaminaState
    {

    }
}
