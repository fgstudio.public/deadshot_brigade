using Mirror;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Respawn
{
    public interface IRespawnUnitService
    {
        UnityEvent<uint> Respawned { get; }
        [Server] void Respawn([NotNull] IRespawnable respawnable, PointData pointData);
    }

    /// <summary>
    /// Сервис для респауна юнитов.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(RespawnUnitService))]
    public sealed class RespawnUnitService : NetworkBehaviour, IRespawnUnitService
    {
        private const string EventsFoldoutName = "Events";

        [field: SerializeField, FoldoutGroup(EventsFoldoutName)]
        public UnityEvent<uint> Respawned { get; private set; }

        [Server]
        public void Respawn(IRespawnable respawnable, PointData pointData)
        {
            respawnable.ImpactState.Reset();
            respawnable.State.Energy = default;
            respawnable.State.Health = respawnable.State.MaxHealth;
            respawnable.State.Stamina = respawnable.State.MaxStamina;
            respawnable.State.PatronsCount = respawnable.State.MaxPatrons;
            respawnable.UnitMove.ResetPosition(pointData.Position);
            respawnable.UnitMove.ResetRotation(pointData.Rotation);

            OnRespawned(respawnable.Identity.netId);
            RpcOnRespawned(respawnable.Identity.netId);
        }

        [ClientRpc]
        private void RpcOnRespawned(uint respawnableId)
        {
            if (isClientOnly)
                OnRespawned(respawnableId);
        }

        private void OnRespawned(uint respawnableId) =>
            Respawned?.Invoke(respawnableId);
    }
}