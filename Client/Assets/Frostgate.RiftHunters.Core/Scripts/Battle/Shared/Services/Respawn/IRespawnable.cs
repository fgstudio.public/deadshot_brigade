using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Respawn
{
    public interface IRespawnable : IGameObject, INetworkIdentifiable
    {
        UnitMove UnitMove { get; }
        ImpactState ImpactState { get; }
        IRespawnableState State { get; }
    }
}