namespace Frostgate.RiftHunters.Core.Battle.Shared.Invincibility
{
    public interface IInvincible
    {
        IInvincibleState State { get; }
    }
}
