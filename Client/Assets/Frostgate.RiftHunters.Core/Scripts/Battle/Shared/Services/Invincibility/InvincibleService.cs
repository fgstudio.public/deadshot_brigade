using System;
using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Invincibility
{
    public interface IInvincibleService
    {
        void SetInvincible([NotNull] IInvincible invincible, TimeSpan duration);
    }

    /// <summary>
    /// Сервис для включения неуязвимости персонажа.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(InvincibleService))]
    public sealed class InvincibleService : NetworkBehaviour, IInvincibleService
    {
        [Server]
        public void SetInvincible(IInvincible invincible, TimeSpan duration)
        {
            if (!isServer) return;
            if (duration.Ticks <= 0) return;

            StartInvincibleMode(invincible);

            UniTask.Delay(duration)
                .ContinueWith(() => CancelInvincibleMode(invincible))
                .AsAsyncUnitUniTask();
        }

        private void StartInvincibleMode([CanBeNull] IInvincible invincible)
        {
            if (invincible == null) return;
            invincible.State.IsInvincible = true;
        }

        private void CancelInvincibleMode([CanBeNull] IInvincible invincible)
        {
            if (invincible == null) return;
            invincible.State.IsInvincible = false;
        }
    }
}