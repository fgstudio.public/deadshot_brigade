namespace Frostgate.RiftHunters.Core.Battle.Shared.Invincibility
{
    public interface IInvincibleState
    {
        bool IsInvincible { get; set; }
    }
}
