namespace Frostgate.RiftHunters.Core.Battle.Shared.Stamina
{
    public interface IStaminaChange
    {
        float Stamina { get; }
    }
}