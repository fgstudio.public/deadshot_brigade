using Mirror;
using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Stamina
{
    public interface IStaminaService
    {
        void Recover([NotNull] IStaminaOwner staminaReceiver, [NotNull] IStaminaChange change);
        void Lose([NotNull] IStaminaOwner staminaOwner, [NotNull] IStaminaChange expenses);
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(StaminaService))]
    public sealed class StaminaService : NetworkBehaviour, IStaminaService
    {
        [Server]
        public void Recover(IStaminaOwner staminaReceiver, IStaminaChange change) =>
            staminaReceiver.State.Stamina += change.Stamina;

        [Server]
        public void Lose(IStaminaOwner staminaOwner, IStaminaChange change) =>
            staminaOwner.State.Stamina -= change.Stamina;
    }
}