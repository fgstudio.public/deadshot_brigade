namespace Frostgate.RiftHunters.Core.Battle.Shared.Stamina
{
    public interface IStaminaOwner : INetworkIdentifiable
    {
        IHavingStaminaState State { get; }
    }
}