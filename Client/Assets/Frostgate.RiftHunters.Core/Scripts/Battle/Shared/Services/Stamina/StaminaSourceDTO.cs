namespace Frostgate.RiftHunters.Core.Battle.Shared.Stamina
{
    public readonly struct StaminaSourceDTO : IStaminaChange
    {
        public float Stamina { get; }

        public StaminaSourceDTO(float stamina) =>
            Stamina = stamina;
    }
}
