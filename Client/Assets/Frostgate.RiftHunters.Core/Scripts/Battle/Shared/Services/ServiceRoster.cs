using Mirror;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Exp;
using Frostgate.RiftHunters.Core.Battle.Shared.Energy;
using Frostgate.RiftHunters.Core.Battle.Shared.Healing;
using Frostgate.RiftHunters.Core.Battle.Shared.Stamina;
using Frostgate.RiftHunters.Core.Battle.Shared.Looting;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Reanimation;
using Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.Invincibility;
using Frostgate.RiftHunters.Core.Battle.Shared.StreakBonus;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface IServiceRoster : INetworkIdentifiable
    {
        IHealingService HealingService { get; }
        IDamageService DamageService { get; }
        IInvincibleService InvincibleService { get; }
        ICollectLootBoxService CollectLootBoxService { get; }
        ICollectEnergyCapsuleService CollectEnergyCapsuleService { get; }
        IStaminaService StaminaService { get; }
        IReceiveExpService ReceiveExpService { get; }
        IRespawnUnitService RespawnUnitService { get; }
        IReanimationService ReanimationService { get; }
        ICaptureUnitService CaptureUnitService { get; }
        IBonusService BonusService { get; }
    }

    [DisallowMultipleComponent]
    [RequireComponent(typeof(HealingService))]
    [RequireComponent(typeof(DamageService))]
    [RequireComponent(typeof(InvincibleService))]
    [RequireComponent(typeof(NetworkIdentity))]
    [RequireComponent(typeof(CollectLootBoxService))]
    [RequireComponent(typeof(CollectEnergyCapsuleService))]
    [RequireComponent(typeof(StaminaService))]
    [RequireComponent(typeof(ReceiveExpService))]
    [RequireComponent(typeof(RespawnUnitService))]
    [RequireComponent(typeof(ReanimationService))]
    [RequireComponent(typeof(CaptureUnitService))]
    [RequireComponent(typeof(BonusService))]
    public sealed class ServiceRoster : MonoBehaviour, IServiceRoster
    {
        [field: Header("References")]
        [field: SerializeField, Required, ReadOnly, ChildGameObjectsOnly] public NetworkIdentity Identity { get; private set; }
        [field: SerializeField, Required, ReadOnly, ChildGameObjectsOnly] public HealingService HealingService { get; private set; }
        [field: SerializeField, Required, ReadOnly, ChildGameObjectsOnly] public DamageService DamageService { get; private set; }
        [field: SerializeField, Required, ReadOnly, ChildGameObjectsOnly] public InvincibleService InvincibleService { get; private set; }
        [field: SerializeField, Required, ReadOnly, ChildGameObjectsOnly] public CollectLootBoxService CollectLootBoxService { get; private set; }
        [field: SerializeField, Required, ReadOnly, ChildGameObjectsOnly] public CollectEnergyCapsuleService CollectEnergyCapsuleService { get; private set; }
        [field: SerializeField, Required, ReadOnly, ChildGameObjectsOnly] public StaminaService StaminaService { get; private set; }
        [field: SerializeField, Required, ReadOnly, ChildGameObjectsOnly] public ReceiveExpService ReceiveExpService { get; private set; }
        [field: SerializeField, Required, ReadOnly, ChildGameObjectsOnly] public RespawnUnitService RespawnUnitService { get; private set; }
        [field: SerializeField, Required, ReadOnly, ChildGameObjectsOnly] public ReanimationService ReanimationService { get; private set; }
        [field: SerializeField, Required, ReadOnly, ChildGameObjectsOnly] public CaptureUnitService CaptureUnitService { get; private set; }
        [field: SerializeField, Required, ReadOnly, ChildGameObjectsOnly] public BonusService BonusService { get; private set; }

        NetworkIdentity INetworkIdentifiable.Identity => Identity;
        IHealingService IServiceRoster.HealingService => HealingService;
        IDamageService IServiceRoster.DamageService => DamageService;
        IInvincibleService IServiceRoster.InvincibleService => InvincibleService;
        ICollectLootBoxService IServiceRoster.CollectLootBoxService => CollectLootBoxService;
        ICollectEnergyCapsuleService IServiceRoster.CollectEnergyCapsuleService => CollectEnergyCapsuleService;
        IStaminaService IServiceRoster.StaminaService => StaminaService;
        IReceiveExpService IServiceRoster.ReceiveExpService => ReceiveExpService;
        IRespawnUnitService IServiceRoster.RespawnUnitService => RespawnUnitService;
        IReanimationService IServiceRoster.ReanimationService => ReanimationService;
        ICaptureUnitService IServiceRoster.CaptureUnitService => CaptureUnitService;
        IBonusService IServiceRoster.BonusService => BonusService;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();

        private void InitReferences()
        {
            Identity ??= GetComponent<NetworkIdentity>();
            HealingService ??= GetComponent<HealingService>();
            DamageService ??= GetComponent<DamageService>();
            InvincibleService ??= GetComponent<InvincibleService>();
            CollectLootBoxService ??= GetComponent<CollectLootBoxService>();
            CollectEnergyCapsuleService ??= GetComponent<CollectEnergyCapsuleService>();
            StaminaService ??= GetComponent<StaminaService>();
            ReceiveExpService ??= GetComponent<ReceiveExpService>();
            RespawnUnitService ??= GetComponent<RespawnUnitService>();
            ReanimationService ??= GetComponent<ReanimationService>();
            CaptureUnitService ??= GetComponent<CaptureUnitService>();
            BonusService ??= GetComponent<BonusService>();
        }
    }
}