﻿using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Battle.Shared.StreakBonus
{
    public interface IBonusService
    {
        void ApplyBonus([NotNull] StreakBonusConfig config, [NotNull] NetworkIdentity collectorIdentity);
    }
}