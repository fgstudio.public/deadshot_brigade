﻿using System;
using System.Linq;
using Mirror;
using Zenject;
using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Battle.Shared.StreakBonus
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(BonusService))]
    public sealed class BonusService : NetworkBehaviour, IBonusService
    {
        private UnitEffectStateFactory _effectFactory;
        private IReadOnlyObjectCollection<uint, UnitNetwork> _units;

        private ILogger _logger;

        [Inject]
        private void MonoConstruct(
            ILogger<BonusService> logger,
            UnitEffectStateFactory effectStateFactory,
            IReadOnlyObjectCollection<uint, UnitNetwork> units)
        {
            _units = units;
            _logger = logger;
            _effectFactory = effectStateFactory;
        }

        [Server]
        public void ApplyBonus(StreakBonusConfig config, NetworkIdentity collectorIdentity)
        {
            switch (config.Coverage)
            {
                case StreakBonusCoverage.OnlyCollector:
                    if (_units.TryGet(collectorIdentity.netId, out UnitNetwork collector))
                        ApplyUnitEffects(config, collector);
                    break;

                case StreakBonusCoverage.AllPlayers:
                    _units.Values.Where(t => t.UnitType == BattleUnitType.Player)
                        .ForEach(p => ApplyUnitEffects(config, p));
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ApplyUnitEffects(StreakBonusConfig config, UnitNetwork unit)
        {
            foreach (UnitEffectConfig effectConfig in config.Effects)
                if (unit.CanReceiveUnitEffect(effectConfig))
                {
                    UnitEffectState effectState = _effectFactory
                        .Create(effectConfig, unit.NetworkIdentity);

                    unit.AddUnitEffect(effectState);
                }

            _logger.Log($"Apply bonus {config.Id} to unit {unit.name}");
        }
    }
}