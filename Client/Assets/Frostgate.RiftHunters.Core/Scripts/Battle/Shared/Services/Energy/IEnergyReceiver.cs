namespace Frostgate.RiftHunters.Core.Battle.Shared.Energy
{
    public interface IEnergyReceiver : INetworkIdentifiable
    {
        IEnergyReceiverState State { get; }
    }
}
