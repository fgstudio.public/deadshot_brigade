using Mirror;
using Zenject;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Energy
{
    public interface ICollectEnergyCapsuleService
    {
        [Server] void Collect([NotNull] IEnergyReceiver energyReceiver, [NotNull] IServerEnergyCapsule energyCapsule);
    }

    /// <summary>
    /// Сервис для подбора юнитом энергетических капсул.
    /// </summary>
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(CollectEnergyCapsuleService))]
    public sealed class CollectEnergyCapsuleService : NetworkBehaviour, ICollectEnergyCapsuleService
    {
        private IObjectCollection<uint, UnitNetwork> _units;

        [Inject]
        private void MonoConstruct(ObjectCollectionRepository<UnitNetwork> units) =>
            _units = units.Get<uint>();

        [Server]
        public void Collect(IEnergyReceiver energyReceiver, IServerEnergyCapsule energyCapsule)
        {
            energyCapsule.State.CollectorId = energyReceiver.Identity.netId;

            // Собранную энергию получают все игроки.
            foreach (var unit in _units.Values)
            {
                if (!unit.CanCollectEnergy)
                    continue;

                if (unit is IEnergyReceiver receiver)
                    receiver.State.Energy += energyCapsule.State.Energy;
            }
        }
    }
}