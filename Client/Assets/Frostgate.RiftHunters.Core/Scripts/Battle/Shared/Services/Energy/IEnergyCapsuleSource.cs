using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Energy
{
    public interface IEnergyCapsuleSource : INetworkIdentifiable
    {
        Transform Transform { get; }
        EnergyCapsuleConfig EnergyCapsuleForDeath { get; }
    }
}
