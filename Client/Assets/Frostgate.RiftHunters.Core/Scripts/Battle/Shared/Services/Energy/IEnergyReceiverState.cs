namespace Frostgate.RiftHunters.Core.Battle.Shared.Energy
{
    public interface IEnergyReceiverState
    {
        float Energy { get; set; }
        float MaxEnergy { get; }
        bool IsEnergyFull { get; }
        bool IsDead { get; }
    }
}
