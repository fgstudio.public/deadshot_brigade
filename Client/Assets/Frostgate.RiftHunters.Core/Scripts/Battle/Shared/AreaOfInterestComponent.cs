using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface IAreaOfInterest
    {
        UnityEvent<UnitNetwork> VisitorRegistered { get; }
        UnityEvent<UnitNetwork> VisitorUnregistered { get; }
    }

    public abstract class AreaOfInterestComponent : Component, IAreaOfInterest
    {
        private const string EventsFoldoutName = "Area Events";

        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private ObjectCatcher _catcher;
        [SerializeField, Required, ChildGameObjectsOnly] private SphereCollider _collider;

        [field: Header("Info")]
        [field: ShowInInspector, DisplayAsString] public int AreaVisitorsCount { get; private set; }

        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent<UnitNetwork> VisitorRegistered { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent<UnitNetwork> VisitorUnregistered { get; private set; }

        public void SetRadius(float radius)
        {
            _collider.radius = radius;
            SetRadiusInner(radius);
        }

        protected abstract void SetRadiusInner(float radius);

        private void OnValidate() => _catcher ??= GetComponentInChildren<ObjectCatcher>();

        protected sealed override void OnEnabled()
        {
            AreaVisitorsCount = default;
            _catcher.Lost.AddListener(OnLost);
            _catcher.Caught.AddListener(OnCollected);
            OnEnabledInner();
        }

        protected sealed override void OnDisabled()
        {
            AreaVisitorsCount = default;
            _catcher.Lost.RemoveListener(OnLost);
            _catcher.Caught.RemoveListener(OnCollected);
            OnDisabledInner();
        }

        protected virtual void OnEnabledInner() { }
        protected virtual void OnDisabledInner() { }

        protected void LoseAllVisitors() =>
            _catcher.LoseAllObjects();

        private void OnLost(Collider visitor)
        {
            if (TryGetPlayerUnitNetwork(visitor, out UnitNetwork unit) && AreaVisitorsCount > 0)
            {
                AreaVisitorsCount--;
                VisitorUnregistered?.Invoke(unit);
            }
        }

        private void OnCollected(Collider visitor)
        {
            if (TryGetPlayerUnitNetwork(visitor, out UnitNetwork unit))
            {
                AreaVisitorsCount++;
                VisitorRegistered?.Invoke(unit);
            }
        }

        private bool TryGetPlayerUnitNetwork(Collider visitor, out UnitNetwork unit) =>
            // TODO: получать из коллекции
            visitor.TryGetComponentInHierarchy(out unit) &&
            unit.UnitType == BattleUnitType.Player;
    }
}