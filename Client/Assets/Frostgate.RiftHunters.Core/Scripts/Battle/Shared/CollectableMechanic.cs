using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Components;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// Логика механики сбора.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(CollectableMechanic))]
    public sealed class CollectableMechanic : MonoBehaviour, IResettable
    {
        [field: SerializeField, Required, ChildGameObjectsOnly] public AutoDestroyComponent AutoDestroy { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public CollectComponent CollectComponent { get; private set; }
        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Destroyed { get; private set; }

        public UnityEvent Collected => CollectComponent.Enabled;
        public bool IsCollected => CollectComponent.enabled;

        private void Start()
        {
            AutoDestroy.DestroyComponent.Destroyed.AddListener(Destroyed.Invoke);
            CollectComponent.DestroyComponent.Destroyed.AddListener(Destroyed.Invoke);
        }

        private void OnDestroy()
        {
            AutoDestroy.DestroyComponent.Destroyed.RemoveListener(Destroyed.Invoke);
            CollectComponent.DestroyComponent.Destroyed.RemoveListener(Destroyed.Invoke);
        }

        private void Awake() => Init();
        private void OnValidate() => Init();

        private void Init()
        {
            InitReferences();
            InitComponents();
        }

        private void InitReferences()
        {
            AutoDestroy ??= this.GetComponentInHierarchy<AutoDestroyComponent>();
            CollectComponent ??= this.GetComponentInHierarchy<CollectComponent>();
        }

        private void InitComponents()
        {
            AutoDestroy.Enable();
            CollectComponent.Disable();
        }

        [ContextMenu(nameof(Reset))]
        public void Reset() =>
            InitComponents();

        [ContextMenu(nameof(Collect))]
        public void Collect()
        {
            AutoDestroy.Disable();
            CollectComponent.Enable();
        }
    }
}