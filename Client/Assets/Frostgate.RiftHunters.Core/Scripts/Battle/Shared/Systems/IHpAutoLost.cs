using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems
{
    public interface IHpAutoLost
    {
        IHealth State { get; }
        IHpAutoLossConfig Config { get; }
    }

    public interface IHpAutoLossConfig
    {
        float Speed { get; }
    }
}
