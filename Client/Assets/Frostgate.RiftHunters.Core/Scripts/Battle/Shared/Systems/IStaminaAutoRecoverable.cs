using Frostgate.RiftHunters.Core.Battle.Shared.Stamina;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems
{
    public interface IStaminaAutoRecoverable : IStaminaOwner
    {
        IStaminaAutoRecoveryConfig Config { get; }
    }

    public interface IStaminaAutoRecoveryConfig
    {
        float Speed { get; }
    }
}