using Frostgate.RiftHunters.Core.Battle.Shared.Energy;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems
{
    public interface IEnergyAutoRecoverable : IEnergyReceiver
    {
        float RecoveryMultiplier { get; }
        IEnergyAutoRecoveryConfig Config { get; }
    }

    public interface IEnergyAutoRecoveryConfig
    {
        float Speed { get; }
    }
}
