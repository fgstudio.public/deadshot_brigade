using Frostgate.RiftHunters.Core.Battle.Shared.Healing;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#e417470319264003b313af6970c072ce"/>
    /// </summary>
    public interface IHpAutoRecoverable : IHealingCaster, IHealingReceiver
    {
        IHpAutoRecoveryConfig Config { get; }
    }

    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#e417470319264003b313af6970c072ce"/>
    /// </summary>
    public interface IHpAutoRecoveryConfig
    {
        float PercentPerSecond { get; }
        float DamageCooldown { get; }
    }
}
