using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Components;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// Компонент подбора ресурсов
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(CollectComponent))]
    public sealed class CollectComponent : Component
    {
        [field: Header("References")]
        [field: SerializeField, Required, ChildGameObjectsOnly] public DestroyComponent DestroyComponent { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public TimerComponent DestroyTimerComponent { get; private set; }
        [field: SerializeField, Required] public GameObject[] CollectedLayoutObjects { get; private set; }
        [field: SerializeField, Required] public GameObject[] NotCollectedLayoutObjects { get; private set; }

        private void Awake() => Init();
        private void OnValidate() => Init();

        private void Init()
        {
            InitReferences();
            InitComponents();
        }

        private void InitReferences()
        {
            DestroyComponent ??= this.GetComponentInHierarchy<DestroyComponent>();
            DestroyTimerComponent ??= this.GetComponentInHierarchy<TimerComponent>();
        }

        private void InitComponents()
        {
            DestroyComponent.Disable();
            DestroyTimerComponent.Disable();
            SetCollectedLayout(false);
        }

        protected override void OnEnabled()
        {
            DestroyComponent.Disable();
            DestroyTimerComponent.Elapsed.AddListener(DestroyComponent.Enable);

            SetCollectedLayout(true);
            DestroyTimerComponent.Enable();
        }

        protected override void OnDisabled()
        {
            DestroyComponent.Disable();
            DestroyTimerComponent.Elapsed.RemoveListener(DestroyComponent.Enable);

            SetCollectedLayout(false);
            DestroyTimerComponent.Disable();
        }

        private void SetCollectedLayout(bool enabled)
        {
            CollectedLayoutObjects.ForEach(e => e.SetActive(enabled));
            NotCollectedLayoutObjects.ForEach(e => e.SetActive(!enabled));
        }
    }
}