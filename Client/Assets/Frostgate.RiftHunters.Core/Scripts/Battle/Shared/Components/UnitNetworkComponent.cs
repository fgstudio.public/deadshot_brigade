using Frostgate.RiftHunters.Core.Battle.Client;
using Frostgate.RiftHunters.Core.Client.Audio;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public abstract class UnitNetworkComponent : MonoBehaviour
    {
        public abstract void Init(UnitView unitView, AudioService audioService);
    }
}