﻿using DG.Tweening;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.CapturableArea
{
    public struct SpriteRendererFader
    {
        private Tweener _tweener;

        public void Do([NotNull] SpriteRenderer renderer, float alphaValue, float duration)
        {
            _tweener?.Kill();
            _tweener = renderer.DOFade(alphaValue, duration);
        }
    }
}