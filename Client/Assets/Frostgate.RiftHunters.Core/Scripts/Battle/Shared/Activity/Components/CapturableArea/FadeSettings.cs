﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.CapturableArea
{
    [Serializable]
    public struct FadeSettings
    {
        [field: SerializeField, MinValue(0)] public float UnfadedAlphaValue { get; set; }
        [field: SerializeField, MinValue(0)] public float FadedAlphaValue { get; set; }
        [field: SerializeField, MinValue(0)] public float Duration { get; set; }
    }
}