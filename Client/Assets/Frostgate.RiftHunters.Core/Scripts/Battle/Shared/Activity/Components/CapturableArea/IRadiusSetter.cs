﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.CapturableArea
{
    public interface IRadiusSetter
    {
        float Radius { set; }
    }
}