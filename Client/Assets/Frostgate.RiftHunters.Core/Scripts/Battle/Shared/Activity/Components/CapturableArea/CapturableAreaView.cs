﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.CapturableArea
{
    public abstract class CapturableAreaView : MonoBehaviour
    {
        [Button]
        public abstract void Show();

        [Button]
        public abstract void Hide();
    }
}