﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.CapturableArea
{
    public interface ICapturableArea<TView, TCapDetector> where TView : CapturableAreaView
        where TCapDetector : CapturingDetector
    {
        TView View { get; }
        TCapDetector Capturing { get; }

        void Enable();
        void Disable();
    }
}