﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.CapturableArea
{
    public sealed class FadeableRoundCapturableAreaView : CapturableAreaView
    {
        public float Radius
        {
            get => _radius;
            set => SetRadius(value);
        }

        [field: SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        public SpriteRenderer Renderer { get; private set; }

        [field: SerializeField, FoldoutGroup("Settings")]
        public FadeSettings FadeSetting { get; set; }


        [ShowInInspector, ReadOnly, FoldoutGroup("Info")]
        private IRadiusSetter[] _radiusSetters;

        [SerializeField, MinValue(0), FoldoutGroup("Settings")]
        private float _radius;

        private SpriteRendererFader _fader = new();

        private void OnValidate() => Init();

        private void Awake() => Init();

        public override void Show() => _fader.Do(Renderer, FadeSetting.UnfadedAlphaValue, FadeSetting.Duration);

        public override void Hide() => _fader.Do(Renderer, FadeSetting.FadedAlphaValue, FadeSetting.Duration);

        private void Init()
        {
            DiscoverRadiusSetters();
            SetRadius(_radius);
        }

        private void DiscoverRadiusSetters()
        {
            _radiusSetters ??= GetComponentsInChildren<IRadiusSetter>();
        }

        private void SetRadius(float value)
        {
            if (Math.Abs(value - _radius) < 0.001)
                return;

            if (value < 0)
                throw new ArgumentException("Area radius must be zero or greater.");

            _radiusSetters.ForEach(s => s.Radius = value);
            _radius = value;
        }
    }
}