﻿using Frostgate.RiftHunters.Core.Battle.Shared.Catching;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.CapturableArea
{
    public class CapturingDetector : MonoBehaviour
    {
        [field: SerializeField, FoldoutGroup("Events")]
        public UnityEvent Captured;

        [field: SerializeField, FoldoutGroup("Events")]
        public UnityEvent Released;

        public bool IsCaptured => _isCaptured;

        [field: SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        public ObjectCatcher Catcher { get; private set; }

        public IDetectionCondition DetectionCondition
        {
            get => _detectionCondition;
            set
            {
                if (value == _detectionCondition)
                    return;

                _detectionCondition = value;
                HandleCatcherEvent();
            }
        }

        [ShowInInspector, ReadOnly, FoldoutGroup("Info")]
        private bool _isCaptured;

        [ShowInInspector, ReadOnly, FoldoutGroup("Info")]
        private bool _isEnabled;

        [ShowInInspector, ReadOnly, FoldoutGroup("Info")]
        private IDetectionCondition _detectionCondition = new ObjectsMoreThanZeroCondition();

        [Button]
        public void Enable()
        {
            if (_isEnabled)
                return;

            EnableInternal();
            Subscribe();
            HandleCatcherEvent();

            _isEnabled = true;
        }

        [Button]
        public void Disable()
        {
            if (!_isEnabled)
                return;

            DisableInternal();
            Unsubscribe();
            Catcher.LoseAllObjects();
            SetReleased();

            _isEnabled = false;
        }

        protected virtual void EnableInternal()
        {
        }

        protected virtual void DisableInternal()
        {
        }

        private void Subscribe()
        {
            Catcher.Caught.AddListener(OnCaught);
            Catcher.Lost.AddListener(OnLost);
        }

        private void Unsubscribe()
        {
            Catcher.Caught.RemoveListener(OnCaught);
            Catcher.Lost.RemoveListener(OnLost);
        }

        private void OnCaught(Collider _) => HandleCatcherEvent();
        private void OnLost(Collider _) => HandleCatcherEvent();

        private void HandleCatcherEvent()
        {
            if (DetectionCondition.IsDetected(Catcher))
                SetCaptured();
            else
                SetReleased();
        }

        private void SetCaptured()
        {
            if (_isCaptured)
                return;

            Captured.Invoke();
            _isCaptured = true;
        }

        private void SetReleased()
        {
            if (!_isCaptured)
                return;

            Released.Invoke();
            _isCaptured = false;
        }

        public interface IDetectionCondition
        {
            bool IsDetected(ObjectCatcher catcher);
        }

        public sealed class ObjectsMoreThanZeroCondition : IDetectionCondition
        {
            public bool IsDetected(ObjectCatcher catcher) => catcher.Objects.Count > 0;
        }
    }
}