﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.CapturableArea
{
    [DisallowMultipleComponent]
    public sealed class RoundCapturingDetector : CapturingDetector
    {
        [field: SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        public SphereCollider Collider { get; private set; }

        [ShowInInspector, ReadOnly, FoldoutGroup("Info")]
        public float Radius
        {
            get => Collider?.radius ?? 0f;
            set => Collider.radius = value;
        }

        protected override void EnableInternal()
        {
            Collider.enabled = true;
        }

        protected override void DisableInternal()
        {
            Collider.enabled = false;
        }
    }
}