﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.CapturableArea
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(SpriteRenderer))]
    public sealed class SpriteRendererRadiusSetter : MonoBehaviour, IRadiusSetter
    {
        public const float RendererSizeToWorldUnitFactor = 2f;

        [field: SerializeField, Required, FoldoutGroup("References")] public SpriteRenderer Renderer { get; private set; }

        public float Radius
        {
            set => Renderer.size = Vector2.one * value * RendererSizeToWorldUnitFactor;
        }

        private void OnValidate() => InitReferences();

        private void Awake() => InitReferences();

        private void InitReferences()
        {
            Renderer ??= GetComponent<SpriteRenderer>();
        }
    }
}