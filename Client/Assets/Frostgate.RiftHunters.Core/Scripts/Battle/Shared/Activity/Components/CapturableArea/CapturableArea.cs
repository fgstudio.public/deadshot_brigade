﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.CapturableArea
{
    [DisallowMultipleComponent]
    public class CapturableArea<TView, TCapDetector> : MonoBehaviour, ICapturableArea<TView, TCapDetector>
        where TView : CapturableAreaView where TCapDetector : CapturingDetector
    {
        [field: SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        public TView View { get; private set; }

        [field: SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        public TCapDetector Capturing { get; private set; }

        [Button]
        public void Enable()
        {
            View.Show();
            Capturing.Enable();
        }

        [Button]
        public void Disable()
        {
            View.Hide();
            Capturing.Disable();
        }
    }
}