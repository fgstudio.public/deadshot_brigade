﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    [HelpURL("https://www.notion.so/frostgate/2a10871acbe84347820a3b456ee7ecaf")]
    [DisallowMultipleComponent]
    public sealed class PathMovementBehaviour : MonoBehaviour
    {
        [field: SerializeField, FoldoutGroup("Events")]
        public UnityEvent PathCompleted { get; private set; }

        [field: SerializeField, FoldoutGroup("Events")]
        public UnityEvent<int> WaypointReached { get; private set; }

        [field: SerializeField, Required, FoldoutGroup("References")]
        public MovementPath Path { get; private set; }

        [ShowInInspector] public float MovementSpeed { get; set; }
        [ShowInInspector] public float RotationSpeed { get; set; }

        [ShowInInspector]
        public MovementDirection Direction
        {
            get => _direction;
            set
            {
                if (value == _direction)
                    return;

                _direction = value;
                UpdateWaypointIndex();
            }
        }


        private IReadOnlyList<Transform> Waypoints => Path.Waypoints;
        public Vector3 TargetPosition => Waypoints[_currentWaypointIndex].position;

        private Coroutine _movementRoutine;
        private int _currentWaypointIndex;
        private MovementDirection _direction;

        private bool _isInMove;


        [Button]
        public void StartPathMovement()
        {
            if (_isInMove)
                return;

            _movementRoutine = StartCoroutine(MovementRoutine());
            _isInMove = true;
        }

        [Button]
        public void StopPathMovement()
        {
            if (!_isInMove)
                return;

            StopCoroutine(_movementRoutine);
            _isInMove = false;
        }

        [Button]
        public void ResetPathMovement()
        {
            StopPathMovement();
            _currentWaypointIndex = 0;

            if (Waypoints.Count > 0)
                transform.position = Waypoints[0].position;

            if (Waypoints.Count > 1)
                transform.LookAt(Waypoints[1]);
        }

        private IEnumerator MovementRoutine()
        {
            while (true)
            {
                yield return null;
                bool isPositionReached = IsPositionReached();

                if (!isPositionReached)
                {
                    MovementTick(Time.deltaTime);
                    RotationTick(Time.deltaTime);
                }

                if (isPositionReached)
                {
                    WaypointReached?.Invoke(_currentWaypointIndex);

                    if (_currentWaypointIndex == Waypoints.Count - 1)
                        break;

                    UpdateWaypointIndex();
                }
            }

            _isInMove = false;
            PathCompleted.Invoke();
        }

        private void UpdateWaypointIndex()
        {
            if (_currentWaypointIndex < Waypoints.Count - 1 && Direction == MovementDirection.Forward)
                _currentWaypointIndex++;
            else if (_currentWaypointIndex > 0 && Direction == MovementDirection.Backward)
                _currentWaypointIndex--;
        }

        private bool IsPositionReached() =>
            Vector3.Distance(transform.position, TargetPosition) <= 0.001f;

        private void MovementTick(float deltaTime)
        {
            Vector3 newPosition = Vector3.MoveTowards(transform.position, TargetPosition, MovementSpeed * deltaTime);
            transform.position = newPosition;
        }

        private void RotationTick(float deltaTime)
        {
            Vector3 directionToTarget =
                (TargetPosition - transform.position).normalized * (Direction == MovementDirection.Forward ? 1f : -1f);

            Quaternion to = Quaternion.LookRotation(directionToTarget);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, to, RotationSpeed * deltaTime);
        }
    }
}