﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity
{
    public interface ICapturableAreaConfig
    {
        float Radius { get; }
        float RequiredCapturePoints { get; }

        [NotNull]
        ICaptureRate GetCaptureRate(int invadersCount);
    }
}