﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity
{
    public static class ActivityComponentMenu
    {
        public const string Menu = Activity.ActivityComponentMenu.Menu + "/" + nameof(SapperActivity);
    }
}