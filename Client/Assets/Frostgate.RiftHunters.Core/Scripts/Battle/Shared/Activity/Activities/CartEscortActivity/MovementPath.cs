﻿using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    [HelpURL("https://www.notion.so/frostgate/2a10871acbe84347820a3b456ee7ecaf")]
    [DisallowMultipleComponent]
    public sealed partial class MovementPath : MonoBehaviour
    {
        [InlineButton(nameof(DetectWaypoints), "Detect")]
        [ValidateInput(nameof(HasDuplicateElements), "Sequence contains duplications.")]
        [SerializeField, Required, ChildGameObjectsOnly]
        private Transform[] _waypoints = Array.Empty<Transform>();

        public Transform[] Waypoints => _waypoints;

        private void DetectWaypoints() =>
            _waypoints = transform.GetAllChildren().ToArray();
    }
}