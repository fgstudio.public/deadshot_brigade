﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(SapperTaskSceneData))]
    [DisallowMultipleComponent]
    public sealed class SapperTaskSceneData : ActivityTaskSceneData
    {
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private SapperTaskState _taskState;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private CapturableAreaRoster _areaRoster;

        public SapperTaskState TaskState => _taskState;
        public override IActivityTaskState State => _taskState;
        public CapturableAreaRoster AreaRoster => _areaRoster;


        private void OnValidate() => DiscoverReferences();

        [Button]
        private void Reset()
        {
            _taskState = null;
            _areaRoster = null;
        }

        private void Awake() => DiscoverReferences();

        private void DiscoverReferences()
        {
            _taskState ??= GetComponentInChildren<SapperTaskState>();
            _areaRoster ??= GetComponentInChildren<CapturableAreaRoster>();
        }
    }
}