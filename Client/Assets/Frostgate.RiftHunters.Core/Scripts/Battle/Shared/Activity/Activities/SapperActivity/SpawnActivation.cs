﻿using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Threading;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity
{
    [AddComponentMenu(ComponentMenus.Menu + "/" + nameof(SapperActivity) + nameof(SpawnActivation))]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(CapturableArea))]
    public sealed class SpawnActivation : MonoBehaviour
    {
        [SerializeField, ChildGameObjectsOnly, Required, FoldoutGroup("References")]
        private CapturableArea _area;

        [SerializeField, ChildGameObjectsOnly, CanBeNull, FoldoutGroup("References")]
        private BotSpawnerTrigger _activationTrigger;

        [SerializeField, ChildGameObjectsOnly, CanBeNull, FoldoutGroup("References")]
        private BotSpawnerTrigger _deactivationTrigger;

        [SerializeField, MinValue(0), SuffixLabel("sec", true)]
        [ShowIf(nameof(_activationTrigger)), FoldoutGroup("Settings")]
        private float _activationThreshold;

        [SerializeField, MinValue(0), SuffixLabel("sec", true)]
        [ShowIf(nameof(_deactivationTrigger)), FoldoutGroup("Settings")]
        private float _deactivationThreshold;

        [CanBeNull] private CancellationTokenSource _activationCts;
        [CanBeNull] private CancellationTokenSource _deactivationCts;

        private bool _isActivationScheduled;
        private bool _isDeactivationScheduled;

        private void OnValidate() => DiscoverReferences();

        [Button]
        private void Reset()
        {
            _area = null!;
            _activationTrigger = null;
            _deactivationTrigger = null;

            _activationThreshold = 0f;
            _deactivationThreshold = 0f;
        }

        private void Awake() => DiscoverReferences();


        private void OnEnable() => Subscribe();

        private void OnDisable() => Unsubscribe();

        private void DiscoverReferences()
        {
            _area ??= GetComponent<CapturableArea>();
        }

        private void Subscribe()
        {
            _area.InvaderCaught.AddListener(OnInvaderCaught);
            _area.InvaderLost.AddListener(OnInvaderLost);
            _area.Captured.AddListener(OnAreaCaptured);
        }

        private void Unsubscribe()
        {
            _area.InvaderCaught.RemoveListener(OnInvaderCaught);
            _area.InvaderLost.RemoveListener(OnInvaderLost);
            _area.Captured.RemoveListener(OnAreaCaptured);
        }

        private void OnInvaderCaught()
        {
            bool hasObjects = _area.InvadersCount > 0;
            if (hasObjects && _activationCts == null && _activationTrigger != null)
            {
                CancelAction(ref _deactivationCts);
                _activationCts = ScheduleAction(_activationThreshold, _activationTrigger.Trigger);
            }
        }

        private void OnInvaderLost()
        {
            bool hasNoObjects = _area.InvadersCount == 0;
            if (hasNoObjects && _deactivationCts == null && _deactivationTrigger != null)
            {
                CancelAction(ref _activationCts);
                _deactivationCts = ScheduleAction(_deactivationThreshold, _deactivationTrigger.Trigger);
            }
        }

        private void OnAreaCaptured(CapturableArea _)
        {
            CancelAction(ref _activationCts);
            CancelAction(ref _deactivationCts);

            if (_deactivationTrigger != null)
                ScheduleAction(0, _deactivationTrigger.Trigger);
        }

        private CancellationTokenSource ScheduleAction(float secondsTime, Action action)
        {
            var actionCts = new CancellationTokenSource();
            UniTask.Delay(TimeSpan.FromSeconds(secondsTime), cancellationToken: actionCts.Token)
                .ContinueWith(action.Invoke)
                .AsAsyncUnitUniTask()
                .SuppressCancellationThrow();

            return actionCts;
        }

        private void CancelAction([CanBeNull] ref CancellationTokenSource source)
        {
            if (source is { IsCancellationRequested: false })
            {
                source.Cancel();
                source.Dispose();

                source = null;
            }
        }
    }
}