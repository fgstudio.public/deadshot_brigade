﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(SapperActivity) + nameof(CapturableAreaRoster))]
    [DisallowMultipleComponent]
    public sealed class CapturableAreaRoster : MonoBehaviour, IReadOnlyList<CapturableArea>
    {
        public int Count => _areas.Length;
        public CapturableArea this[int index] => _areas[index];

        [SerializeField, Required, ChildGameObjectsOnly, ValidateInput(nameof(HasNoIdDuplicates))]
        private CapturableArea[] _areas = Array.Empty<CapturableArea>();

        private void OnValidate() => DiscoverReferences();

        [Button]
        private void Reset()
        {
            _areas = Array.Empty<CapturableArea>();
        }

        private void Awake() => DiscoverReferences();

        public bool TryGetById(string id, out CapturableArea area)
        {
            area = null;
            foreach (CapturableArea a in _areas)
            {
                if (a.Id == id)
                {
                    area = a;
                    break;
                }
            }

            return area != null;
        }

        public IEnumerator<CapturableArea> GetEnumerator() => ((IEnumerable<CapturableArea>)_areas).GetEnumerator();

        private void DiscoverReferences()
        {
            if (_areas.Length == 0)
                _areas = GetComponentsInChildren<CapturableArea>();
        }

        private bool HasNoIdDuplicates(CapturableArea[] areas, ref string errMsg, ref InfoMessageType? msgType)
        {
            var idSet = new HashSet<string>();
            foreach (CapturableArea a in areas)
            {
                string id = a.Id;
                if (!idSet.Contains(id))
                {
                    idSet.Add(id);
                }
                else
                {
                    errMsg = $"Capturable area Id '{id}' duplication found.";
                    msgType = InfoMessageType.Error;

                    return false;
                }
            }

            return true;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}