﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.FinishOffActivity
{
    public static class ActivityComponentMenu
    {
        public const string Menu = Shared.Activity.ActivityComponentMenu.Menu + "/" + nameof(FinishOffActivity);
    }
}