﻿using Mirror;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.KillUnitsActivity
{
    public interface IReadOnlyKillUnitsTaskState : IReadOnlyActivityTaskState<KillUnitsActivityConfig>
    {
        UnityEvent<string> UnitKilled { get; }
        bool AreTargetUnitsKilled { get; }
        bool IsUnitTarget(string unitId);
        bool IsUnitKilled(string unitId);
    }

    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(KillUnitsTaskState))]
    [DisallowMultipleComponent]
    public sealed class KillUnitsTaskState : ActivityTaskState<KillUnitsTaskState, KillUnitsActivityConfig>,
        IReadOnlyKillUnitsTaskState
    {
        [field: SerializeField] public UnityEvent<string> UnitKilled { get; private set; }

        private readonly SyncHashSet<string> _killedUnitIds = new();

        private IEnumerable<string> TargetUnitIds =>
            Config!.Units.Select(s => s.Id);

        public bool AreTargetUnitsKilled =>
            TargetUnitIds.All(_killedUnitIds.Contains);

        public bool IsUnitTarget(string unitId) =>
            TargetUnitIds.Contains(unitId);

        public bool IsUnitKilled(string unitId) =>
            _killedUnitIds.Contains(unitId);

        public override void ResetProgress()
        {
            base.ResetProgress();
            _killedUnitIds.Clear();
        }

        [Server]
        public void AddKilledUnit(string unitId)
        {
            _killedUnitIds.Add(unitId);
            InvokeUnitKilled(unitId);
            RpcInvokeUnitKilled(unitId);
        }

        [ClientRpc]
        private void RpcInvokeUnitKilled(string unitId)
        {
            if (isClientOnly)
                InvokeUnitKilled(unitId);
        }

        private void InvokeUnitKilled(string unitId) =>
            UnitKilled?.Invoke(unitId);
    }
}