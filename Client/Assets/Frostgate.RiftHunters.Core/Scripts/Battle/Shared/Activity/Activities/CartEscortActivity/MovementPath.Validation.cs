﻿using Frostgate.RiftHunters.Core.Utils;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    public sealed partial class MovementPath
    {
        private bool HasNullElements(Transform[] elements) => !Validator.HasNullElements(elements);
        private bool HasDuplicateElements(Transform[] elements) => !Validator.HasDuplicateElements(elements);
    }
}