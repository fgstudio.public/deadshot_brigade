﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.CapturableArea;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(CartCapturableAreaView))]
    [DisallowMultipleComponent]
    public sealed class CartCapturableAreaView : CapturableAreaView
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private FadeableRoundCapturableAreaView _inactiveView;

        [SerializeField, Required, ChildGameObjectsOnly]
        private FadeableRoundCapturableAreaView _activeView;

        public float Radius
        {
            get => _radius;
            set
            {
                _inactiveView.Radius = value;
                _activeView.Radius = value;
                _radius = value;
            }
        }

        [ShowInInspector]
        public ViewState State
        {
            get => _state;
            set
            {
                _state = value;
                UpdateView();
            }
        }

        private float _radius;
        private ViewState _state;

        private bool _isHidden;

        public override void Show()
        {
            _isHidden = false;
            UpdateView();
        }

        public override void Hide()
        {
            _isHidden = true;
            UpdateView();
        }

        private void UpdateView()
        {
            if (_isHidden)
            {
                _inactiveView.Hide();
                _activeView.Hide();
            }
            else if (_state == ViewState.Inactive)
            {
                _inactiveView.Show();
                _activeView.Hide();
            }
            else
            {
                _inactiveView.Hide();
                _activeView.Show();
            }
        }

        public enum ViewState : byte
        {
            Inactive,
            Active
        }
    }
}