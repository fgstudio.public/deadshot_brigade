﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.CapturableArea;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(CartEscortCapturableArea))]
    [DisallowMultipleComponent]
    public sealed class CartEscortCapturableArea : CapturableArea<CartCapturableAreaView, RoundCapturingDetector>
    {
    }
}