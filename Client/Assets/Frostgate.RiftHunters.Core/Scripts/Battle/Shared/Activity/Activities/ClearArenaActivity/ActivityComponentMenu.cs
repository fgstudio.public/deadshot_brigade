﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.ClearArenaActivity
{
    public static class ActivityComponentMenu
    {
        public const string Menu = Shared.Activity.ActivityComponentMenu.Menu + "/" + nameof(ClearArenaActivity);
    }
}