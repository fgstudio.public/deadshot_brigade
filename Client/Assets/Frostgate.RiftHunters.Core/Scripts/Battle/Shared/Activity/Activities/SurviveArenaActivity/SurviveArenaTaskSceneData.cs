﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(SurviveArenaTaskSceneData))]
    [DisallowMultipleComponent]
    public sealed class SurviveArenaTaskSceneData : ActivityTaskSceneData
    {
        [field: SerializeField, Required, ChildGameObjectsOnly] public SurviveArenaTaskState TaskState { get; private set; }

        [field: SerializeField, Required, ChildGameObjectsOnly]
        public WaveGate[] Gates { get; private set; }

        public override IActivityTaskState State => TaskState;

        private void OnValidate() => DiscoverDependencies();
        private void Awake() => DiscoverDependencies();

        private void DiscoverDependencies()
        {
            TaskState ??= GetComponent<SurviveArenaTaskState>();
            Gates ??= GetComponentsInChildren<WaveGate>();
        }
    }
}