﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.MoveToPointActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(MoveToPointTaskState))]
    [DisallowMultipleComponent]
    public class MoveToPointTaskState : ActivityTaskState<MoveToPointTaskState, MoveToPointActivityConfig>,
        IReadOnlyMoveToPointTaskState
    {
    }
}