﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(Cart))]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(PathMovementBehaviour))]
    public sealed class Cart : MonoBehaviour
    {
        [field: SerializeField, Required] public PathMovementBehaviour MovementBehaviour { get; private set; }

        [field: SerializeField, Required] public RailCartView View { get; private set; }

        private void OnValidate() => DiscoverDependencies();
        private void Awake() => DiscoverDependencies();

        [Button]
        private void Reset()
        {
            MovementBehaviour = null;
        }

        private void DiscoverDependencies()
        {
            MovementBehaviour ??= GetComponent<PathMovementBehaviour>();
        }
    }
}