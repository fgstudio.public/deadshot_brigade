﻿using Mirror;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity
{
    public interface IReadOnlySurviveArenaActivityTaskState : IReadOnlyActivityTaskState<SurviveArenaActivityConfig>
    {
        bool AreTargetSpawnersCompleted { get; }
        bool IsSpawnerIdTarget(string spawnerId);
        bool IsSpawnerIdCompleted(string spawnerId);
    }

    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(SurviveArenaTaskState))]
    [DisallowMultipleComponent]
    public sealed class SurviveArenaTaskState : ActivityTaskState<SurviveArenaTaskState, SurviveArenaActivityConfig>,
        IReadOnlySurviveArenaActivityTaskState
    {
        // TODO: получать данные из стэйта спауна в реальном времени, когда они там появятся
        private readonly SyncHashSet<string> _completedSpawnerIds = new();

        private IEnumerable<string> TargetSpawnerIds =>
            Config!.Spawners.Select(s => s.Id);

        public bool AreTargetSpawnersCompleted =>
            TargetSpawnerIds.All(_completedSpawnerIds.Contains);

        [Server]
        public void AddCompletedSpawnerId(string spawnerId) =>
            _completedSpawnerIds.Add(spawnerId);

        public bool IsSpawnerIdTarget(string spawnerId) =>
            TargetSpawnerIds.Contains(spawnerId);

        public bool IsSpawnerIdCompleted(string spawnerId) =>
            _completedSpawnerIds.Contains(spawnerId);

        public override void ResetProgress()
        {
            base.ResetProgress();
            _completedSpawnerIds.Clear();
        }
    }
}