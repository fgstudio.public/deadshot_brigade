﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    [CreateAssetMenu(fileName = nameof(CartEscortActivityConfig),
        menuName = ActivityAssetMenu.Menu + "/" + nameof(CartEscortActivityConfig))]
    public sealed class CartEscortActivityConfig : ActivityTaskConfig, IInheritedConfig
    {
        [SerializeField, MinValue(0), SuffixLabel("units/sec", true), FoldoutGroup("Movement")]
        private float _cartForwardSpeed = 1.5f;

        [SerializeField, MinValue(0), SuffixLabel("units/sec", true), FoldoutGroup("Movement")]
        private float _cartBackwardSpeed = 1f;

        [SerializeField, MinValue(0), SuffixLabel("units/sec", true), FoldoutGroup("Movement")]
        private float _cartRotationSpeed = 6.5f;

        [SerializeField]
        private InvadersCountRelatedScale[] _forwardSpeedScales = Array.Empty<InvadersCountRelatedScale>();

        [SerializeField, MinValue(0), SuffixLabel("units", true), FoldoutGroup("Other")]
        private float _cartCapturableAreaRadius = 6f;

        public float CartBackwardSpeed => _cartBackwardSpeed;
        public float CartRotationSpeed => _cartRotationSpeed;
        public float CartCapturableAreaRadius => _cartCapturableAreaRadius;

        private IDictionary<int, InvadersCountRelatedScale> _forwardSpeedScalesCache;

        public float GetForwardSpeed(int invadersCount)
        {
            _forwardSpeedScalesCache ??= new Dictionary<int, InvadersCountRelatedScale>();
            if (!_forwardSpeedScalesCache.TryGetValue(invadersCount, out InvadersCountRelatedScale scale))
            {
                bool hasNearest = _forwardSpeedScales.TryGetNearest(invadersCount, s => s.InvadersCount, out scale);
                if (!hasNearest)
                {
                    scale = InvadersCountRelatedScale.CreateDefault();
                    _forwardSpeedScalesCache[invadersCount] = scale;
                }
            }

            return _cartForwardSpeed * scale.Scale;
        }

        private void Reset()
        {
            _cartForwardSpeed = 0f;
            _cartBackwardSpeed = 0f;
            _cartRotationSpeed = 0f;

            _forwardSpeedScales = Array.Empty<InvadersCountRelatedScale>();
        }

        [Serializable]
        private struct InvadersCountRelatedScale
        {
            [SerializeField, MinValue(0)] private int _invadersCount;
            [SerializeField, MinValue(0)] private float _scale;

            public int InvadersCount => _invadersCount;
            public float Scale => _scale;

            public static InvadersCountRelatedScale CreateDefault() => new()
            {
                _invadersCount = 1,
                _scale = 1f
            };
        }
    }
}