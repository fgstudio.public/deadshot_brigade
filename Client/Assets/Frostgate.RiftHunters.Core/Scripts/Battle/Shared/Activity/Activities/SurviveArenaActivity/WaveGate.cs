﻿using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(SurviveArenaActivity) + nameof(WaveGate))]
    [DisallowMultipleComponent]
    public class WaveGate : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Transform _indicatorTransform;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private Transform _pointerTransform;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private GameObject _floorArea;

        [NotNull] public Transform IndicatorTransform => _indicatorTransform;
        [NotNull] public Transform PointerTransform => _pointerTransform;
        [NotNull] public GameObject FloorArea => _floorArea;
    }
}