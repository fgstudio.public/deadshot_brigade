﻿using Frostgate.RiftHunters.Core.Battle.Shared.Catching;
using UnityEngine.Assertions;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using System.Collections;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using UnityEngine.Events;
using UnityEngine;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(SapperActivity) + nameof(CapturableArea))]
    [DisallowMultipleComponent]
    public sealed class CapturableArea : NetworkBehaviour
    {
        [field: SerializeField] public UnityEvent<CapturableArea> Captured { get; private set; } = new();

        [field: SerializeField]
        public UnityEvent<float, float> CurrentCapturePointsChanged { get; private set; } = new();

        [field: SerializeField] public UnityEvent InvaderCaught { get; private set; } = new();
        [field: SerializeField] public UnityEvent InvaderLost { get; private set; } = new();

        [SerializeField, Required] private string _id;

        [SerializeField, Required, ChildGameObjectsOnly]
        private ObjectCatcher _catcher;

        [SerializeField, Required, ChildGameObjectsOnly]
        private SphereCollider _collider;

        public string Id => _id;
        public bool IsCaptured => CurrentCapturePoints >= RequiredCapturePoints;
        public bool IsInvaderCaught => InvadersCount > 0;
        public int InvadersCount => _catcher.Objects.Count;
        public float Radius => _config.Radius;
        public float RequiredCapturePoints => _config.RequiredCapturePoints;
        public float CurrentCapturePoints => _currentCapturePoints;

        private ICapturableAreaConfig _config;

        [SyncVar(hook = nameof(CurrentCapturePointsChangedHook))]
        private float _currentCapturePoints;

        private float _pointAccumulationRate;

        [CanBeNull] private Coroutine _capturePointAccRoutine;

        private bool _isInitialized;

        private void OnValidate() => DiscoverReferences();

        [Button]
        private void Reset()
        {
            _id = string.Empty;
            _catcher = null;
            _collider = null;
        }

        private void Awake() => DiscoverReferences();

        private void Start() => Subscribe();

        private void OnDestroy() => Unsubscribe();

        public void Init([NotNull] ICapturableAreaConfig config)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(config, nameof(config));

            _config = config;
            _collider.radius = _config.Radius;

            _isInitialized = true;
        }

        public void Enable()
        {
            AssertInitialization();

            _collider.enabled = true;
            _catcher.enabled = true;
        }

        public void Disable()
        {
            AssertInitialization();

            _collider.enabled = false;
            _catcher.enabled = false;

            if (isServer)
                StopPointAccRoutineIfNotNull();
        }

        [Server]
        public void ResetProgress()
        {
            _pointAccumulationRate = 0f;
            SetCurrentCapturePoints(0f);
        }

        private void DiscoverReferences()
        {
            _catcher ??= GetComponentInChildren<ObjectCatcher>();
            _collider ??= GetComponentInChildren<SphereCollider>();
        }

        private void Subscribe()
        {
            _catcher.Caught.AddListener(OnInvaderCaught);
            _catcher.Lost.AddListener(OnInvaderLost);
        }

        private void Unsubscribe()
        {
            _catcher.Caught.RemoveListener(OnInvaderCaught);
            _catcher.Lost.RemoveListener(OnInvaderLost);
        }

        private void OnInvaderCaught(Collider invader)
        {
            // TODO: получать из коллекции
            var unit = invader.GetComponentInParent<UnitNetwork>();
            if (unit != null)
                unit.InvokeOnCaughtArea();

            InvaderCaught.Invoke();

            if (isServer)
            {
                UpdatePointAccumulationRate();
                StartPointAccRoutineIfNull();
            }
        }

        private void OnInvaderLost(Collider _)
        {
            InvaderLost.Invoke();

            if (isServer)
            {
                UpdatePointAccumulationRate();
                StartPointAccRoutineIfNull();
            }
        }

        [Server]
        private void SetCurrentCapturePoints(float capturePoints)
        {
            if (Mathf.Approximately(capturePoints, _currentCapturePoints))
                return;

            capturePoints = Mathf.Clamp(capturePoints, 0f, _config.RequiredCapturePoints);
            float oldPoints = _currentCapturePoints;
            _currentCapturePoints = capturePoints;

            CurrentCapturePointsChanged.Invoke(oldPoints, _currentCapturePoints);
            if (Mathf.Approximately(capturePoints, _config.RequiredCapturePoints))
                Captured.Invoke(this);
        }

        private IEnumerator CapturePointsAccRoutine()
        {
            while (!Mathf.Approximately(_currentCapturePoints, _config.RequiredCapturePoints))
            {
                SetCurrentCapturePoints(_currentCapturePoints + _pointAccumulationRate * Time.deltaTime);
                yield return null;
            }
        }

        private void StartPointAccRoutineIfNull()
        {
            _capturePointAccRoutine ??= StartCoroutine(CapturePointsAccRoutine());
        }

        private void StopPointAccRoutineIfNotNull()
        {
            if (_capturePointAccRoutine != null)
            {
                StopCoroutine(_capturePointAccRoutine);
                _capturePointAccRoutine = null;
            }
        }

        private void CurrentCapturePointsChangedHook(float oldPoints, float newPoints)
        {
            if (isClientOnly)
            {
                CurrentCapturePointsChanged.Invoke(oldPoints, newPoints);
                if (Mathf.Approximately(newPoints, _config.RequiredCapturePoints))
                    Captured.Invoke(this);
            }
        }

        private ICaptureRate GetCaptureRate() => _config.GetCaptureRate(_catcher.Objects.Count);

        private void UpdatePointAccumulationRate()
        {
            ICaptureRate rate = GetCaptureRate();
            _pointAccumulationRate = _catcher.Objects.Count > 0 ? rate.IncreaseRate : -Mathf.Abs(rate.DecreaseRate);
        }

        private void AssertInitialization() =>
            Assert.IsTrue(_isInitialized, $"{nameof(CapturableArea)} with id '{_id}' not initialized.");
    }
}