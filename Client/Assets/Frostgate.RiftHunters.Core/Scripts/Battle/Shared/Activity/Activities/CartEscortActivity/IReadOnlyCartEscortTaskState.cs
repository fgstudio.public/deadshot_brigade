﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    public interface IReadOnlyCartEscortTaskState : IReadOnlyActivityTaskState<CartEscortActivityConfig>
    {
    }
}