﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using System;
using UnityEngine.Serialization;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity
{
    [Serializable]
    public sealed class CapturableAreaConfig : ICapturableAreaConfig
    {
        [SerializeField, Required] private string _id;

        [SerializeField, MinValue(1), SuffixLabel("m", true)]
        private float _radius = 1f;

        [SerializeField, MinValue(0), SuffixLabel("point", true)]
        private float _requiredCapturePoints = 100f;

        [SerializeField, MinValue(0), SuffixLabel("p/sec", true)]
        private float _captureIncreaseRate = 5f;

        [SerializeField, MinValue(0), SuffixLabel("p/sec", true)]
        private float _captureDecreaseRate = 3f;

        [SerializeField, MinValue(0)]
        private InvadersCountRelatedScale[] _increaseRateScales = Array.Empty<InvadersCountRelatedScale>();

        public string Id => _id;
        public float Radius => _radius;
        public float RequiredCapturePoints => _requiredCapturePoints;

        private IDictionary<int, ICaptureRate> _captureRatesDict;

        private CapturableAreaConfig(string id)
        {
            _id = id;
        }

        public ICaptureRate GetCaptureRate(int invadersCount)
        {
            _captureRatesDict ??= new Dictionary<int, ICaptureRate>();
            if (!_captureRatesDict.TryGetValue(invadersCount, out ICaptureRate rate))
            {
                rate = _increaseRateScales.TryGetNearest(invadersCount, s => s.InvadersCount,
                    out InvadersCountRelatedScale scale)
                    ? new CaptureRate(_captureIncreaseRate * scale.Scale, _captureDecreaseRate)
                    : new CaptureRate(_captureIncreaseRate, _captureDecreaseRate);

                _captureRatesDict[invadersCount] = rate;
            }

            return rate;
        }

        public static CapturableAreaConfig CreateDefault(string id) => new(id);

        [Serializable]
        private struct InvadersCountRelatedScale
        {
            [SerializeField, MinValue(0)] private int _invadersCount;
            [SerializeField, MinValue(0)] private float _scale;

            public int InvadersCount => _invadersCount;
            public float Scale => _scale;
        }

        private sealed class CaptureRate : ICaptureRate
        {
            public float IncreaseRate { get; }
            public float DecreaseRate { get; }

            public CaptureRate(float increaseRate, float decreaseRate)
            {
                IncreaseRate = increaseRate;
                DecreaseRate = decreaseRate;
            }
        }
    }
}