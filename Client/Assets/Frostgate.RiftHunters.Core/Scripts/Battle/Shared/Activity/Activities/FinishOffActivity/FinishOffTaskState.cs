﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.FinishOffActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(FinishOffTaskState))]
    [DisallowMultipleComponent]
    public sealed class FinishOffTaskState : ActivityTaskState<FinishOffTaskState, FinishOffActivityConfig>,
        IReadOnlyFinishOffTaskState
    {
    }
}