﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.MoveToPointActivity
{
    [CreateAssetMenu(menuName = ActivityAssetMenu.Menu + "/" + nameof(MoveToPointActivityConfig),
        fileName = nameof(MoveToPointActivityConfig))]
    public class MoveToPointActivityConfig : ActivityTaskConfig, IInheritedConfig
    {
        [SerializeField] private MoveToPointActivityType _type;

        public MoveToPointActivityType Type => _type;

        public enum MoveToPointActivityType
        {
            Any,
            All
        }
    }
}