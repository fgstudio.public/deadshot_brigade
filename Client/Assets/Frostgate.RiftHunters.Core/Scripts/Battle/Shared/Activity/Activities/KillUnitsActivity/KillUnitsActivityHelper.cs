using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.KillUnitsActivity
{
    public static class KillUnitsActivityHelper
    {
        public static bool IsUnitTarget(
            [NotNull] UnitNetworkState state, [NotNull] IReadOnlyKillUnitsTaskState taskState)
        {
            UnitConfig config = state.UnitConfig;

            return IsUnitTarget(config, taskState);
        }

        private static bool IsUnitTarget([CanBeNull] UnitConfig config, IReadOnlyKillUnitsTaskState state)
        {
            if (config is null)
                return false;

            // ReSharper disable once Unity.NoNullPropagation
            return state.IsUnitTarget(config.Id) || IsUnitTarget(config.UnitPhaseConfig?.UnitConfig, state);
        }
    }
}