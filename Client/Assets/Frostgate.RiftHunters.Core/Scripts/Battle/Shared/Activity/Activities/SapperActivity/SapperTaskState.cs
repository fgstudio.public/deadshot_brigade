﻿using JetBrains.Annotations;
using UnityEngine.Events;
using UnityEngine;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity
{
    [AddComponentMenu(ActivityAssetMenu.Menu + "/" + nameof(SapperTaskState))]
    [DisallowMultipleComponent]
    public sealed class SapperTaskState : ActivityTaskState<SapperTaskState, SapperActivityConfig>, IReadOnlySapperTaskState
    {
        [field: SerializeField] public UnityEvent<int, int> CompletedAreasCountChanged { get; private set; } = new();

        public int TotalAreasCount
        {
            get => _totalAreasCount;
            [Server]
            set
            {
                ThrowHelper.ThrowIfArgumentNotFulfillCondition(value, v => v >= 0, nameof(value));
                _totalAreasCount = value;
            }
        }

        public int CompletedAreasCount
        {
            get => _completedAreasCount;
            [Server] set => SetCompletedAreasCount(value);
        }

        [SyncVar] private int _totalAreasCount;

        [SyncVar(hook = nameof(CompletedAreasCountChangedHook))]
        private int _completedAreasCount;

        private void SetCompletedAreasCount(int areasCount)
        {
            ThrowHelper.ThrowIfArgumentNotFulfillCondition(areasCount, c => c >= 0 && c <= _totalAreasCount);
            if (areasCount == _completedAreasCount)
                return;

            int oldCount = _completedAreasCount;
            _completedAreasCount = areasCount;

            CompletedAreasCountChanged.Invoke(oldCount, _completedAreasCount);
        }

        private void CompletedAreasCountChangedHook(int oldCount, int newCount)
        {
            if (isClientOnly)
                CompletedAreasCountChanged.Invoke(oldCount, newCount);
        }
    }
}