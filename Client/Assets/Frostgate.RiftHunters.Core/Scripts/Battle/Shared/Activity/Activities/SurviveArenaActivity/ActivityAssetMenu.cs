﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity
{
    public static class ActivityAssetMenu
    {
        public const string Menu = Shared.Activity.ActivityAssetMenu.Menu + "/" + nameof(SurviveArenaActivity);
    }
}