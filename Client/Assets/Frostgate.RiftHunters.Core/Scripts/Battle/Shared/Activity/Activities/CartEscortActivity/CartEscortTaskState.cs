﻿using UnityEngine.Events;
using UnityEngine;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(CartEscortTaskState))]
    [DisallowMultipleComponent]
    public sealed class CartEscortTaskState : ActivityTaskState<CartEscortTaskState, CartEscortActivityConfig>, IReadOnlyCartEscortTaskState
    {
        [field: SerializeField]
        public UnityEvent<CartMovementAction, CartMovementAction> CartActionChanged { get; private set; } = new();

        [SyncVar(hook = nameof(OnCartActionChangedHook))]
        private CartMovementAction _cartAction = CartMovementAction.Standing;

        public CartMovementAction CartAction
        {
            get => _cartAction;
            [Server] set => SetCartAction(value);
        }

        private void SetCartAction(CartMovementAction value)
        {
            if (_cartAction == value)
                return;

            CartMovementAction oldAction = _cartAction;
            _cartAction = value;

            CartActionChanged.Invoke(oldAction, _cartAction);
        }

        private void OnCartActionChangedHook(CartMovementAction oldAction, CartMovementAction newAction)
        {
            if (isClientOnly)
                CartActionChanged.Invoke(oldAction, newAction);
        }
    }

    public enum CartMovementAction
    {
        MovingBackward,
        Standing,
        MovingForward,
    }
}