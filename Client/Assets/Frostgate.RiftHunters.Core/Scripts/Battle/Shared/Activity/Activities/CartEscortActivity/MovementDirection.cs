﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    public enum MovementDirection : byte
    {
        Forward,
        Backward,
    }
}