﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.FinishOffActivity
{
    [CreateAssetMenu(menuName = ActivityAssetMenu.Menu + "/" + nameof(FinishOffActivityConfig),
        fileName = nameof(FinishOffActivityConfig))]
    public sealed class FinishOffActivityConfig : ActivityTaskConfig, IInheritedConfig
    {
    }
}