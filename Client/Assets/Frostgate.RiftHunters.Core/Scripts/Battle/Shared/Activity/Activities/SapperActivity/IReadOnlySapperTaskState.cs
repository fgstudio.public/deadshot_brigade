﻿using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity
{
    public interface IReadOnlySapperTaskState : IReadOnlyActivityTaskState<SapperActivityConfig>
    {
        public UnityEvent<int, int> CompletedAreasCountChanged { get; }

        public int TotalAreasCount { get; }
        public int CompletedAreasCount { get; }
    }
}