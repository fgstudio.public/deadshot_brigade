﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.ClearArenaActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(ClearArenaTaskSceneData))]
    [DisallowMultipleComponent]
    public sealed class ClearArenaTaskSceneData : ActivityTaskSceneData
    {
        [field: SerializeField, Required, ChildGameObjectsOnly] public ClearArenaTaskState TaskState { get; private set; }

        public override IActivityTaskState State => TaskState;

        private void OnValidate() => DiscoverDependencies();
        private void Awake() => DiscoverDependencies();

        private void DiscoverDependencies() =>
            TaskState ??= GetComponent<ClearArenaTaskState>();
    }
}