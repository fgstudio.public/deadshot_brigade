﻿using Frostgate.RiftHunters.Core.Battle.Client.Targeting;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.KillUnitsActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(KillUnitsTaskSceneData))]
    [DisallowMultipleComponent]
    public sealed class KillUnitsTaskSceneData : ActivityTaskSceneData
    {
        [field: SerializeField, Required, ChildGameObjectsOnly] public KillUnitsTaskState TaskState { get; private set; }
        [field: SerializeField, Required, AssetsOnly] public TargetMarkerComponent TargetMarkerPrefab { get; private set; }
        public override IActivityTaskState State => TaskState;

        private void OnValidate() => DiscoverDependencies();
        private void Awake() => DiscoverDependencies();

        private void DiscoverDependencies() =>
            TaskState ??= GetComponent<KillUnitsTaskState>();
    }
}