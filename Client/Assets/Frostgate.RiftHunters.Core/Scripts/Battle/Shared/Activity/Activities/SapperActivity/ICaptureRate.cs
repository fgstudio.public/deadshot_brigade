﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity
{
    public interface ICaptureRate
    {
        float IncreaseRate { get; }
        float DecreaseRate { get; }
    }
}