﻿using Frostgate.RiftHunters.Core.Battle.Client.Targeting;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(CartEscortTaskSceneData))]
    [DisallowMultipleComponent]
    public sealed class CartEscortTaskSceneData : ActivityTaskSceneData
    {
        [field: SerializeField, FoldoutGroup("References"), ChildGameObjectsOnly, Required]
        public CartEscortTaskState TaskState { get; private set; }

        [field: SerializeField, FoldoutGroup("References"), ChildGameObjectsOnly, Required]
        public CartEscortCapturableArea CapturableArea { get; private set; }

        [field: SerializeField, FoldoutGroup("References"), ChildGameObjectsOnly, Required]
        public Cart Cart { get; private set; }

        [SerializeField, FoldoutGroup("View"), LabelText("Target (pointer)")]
        private TargetMarkerComponent _targetMarker;

        public TargetMarkerComponent TargetMarker => _targetMarker;

        public override IActivityTaskState State => TaskState;

        private void OnValidate() => DiscoverDependencies();
        private void Awake() => DiscoverDependencies();


        [Button]
        private void Reset()
        {
            TaskState = null;
            CapturableArea = null;
            Cart = null;
        }

        private void DiscoverDependencies()
        {
            TaskState ??= GetComponentInChildren<CartEscortTaskState>();
            CapturableArea ??= GetComponentInChildren<CartEscortCapturableArea>();
            Cart ??= GetComponentInChildren<Cart>();
        }
    }
}