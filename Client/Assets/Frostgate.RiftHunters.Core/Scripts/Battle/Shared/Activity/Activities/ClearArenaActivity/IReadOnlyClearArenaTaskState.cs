﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.ClearArenaActivity
{
    public interface IReadOnlyClearArenaTaskState : IReadOnlyActivityTaskState<ClearArenaActivityConfig>
    {
        bool AreTargetSpawnersCompleted { get; }
        bool IsSpawnerIdTarget(string spawnerId);
        bool IsSpawnerIdCompleted(string spawnerId);
    }
}