﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.FinishOffActivity
{
    public static class ActivityAssetMenu
    {
        public const string Menu = Shared.Activity.ActivityAssetMenu.Menu + "/" + nameof(FinishOffActivity);
    }
}