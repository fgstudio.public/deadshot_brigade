﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.FinishOffActivity
{
    public interface IReadOnlyFinishOffTaskState : IReadOnlyActivityTaskState<FinishOffActivityConfig>
    { }
}