﻿using Frostgate.RiftHunters.Core.UI;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(UICartMovementIndicator))]
    [DisallowMultipleComponent]
    public sealed class UICartMovementIndicator : CartMovementIndicator<UISmoothActivator>
    {
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private TMP_Text _labelText;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private TMP_Text _speedText;

        public override void DisableIndication()
        {
            base.DisableIndication();
            SetTextActive(_labelText, false);
            SetTextActive(_speedText, false);
        }

        protected override void UpdateObjects()
        {
            base.UpdateObjects();
            SetTextActive(_labelText, true);
            SetTextActive(_speedText, true);

            _speedText.text = InvadersCount.ToString();
        }

        private void SetTextActive(TMP_Text text, bool isActive)
        {
            GameObject go = text.gameObject;

            if (go.activeSelf != isActive)
                go.SetActive(isActive);
        }
    }
}