﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.KillUnitsActivity
{
    public static class ActivityAssetMenu
    {
        public const string Menu = Shared.Activity.ActivityAssetMenu.Menu + "/" + nameof(KillUnitsActivity);
    }
}