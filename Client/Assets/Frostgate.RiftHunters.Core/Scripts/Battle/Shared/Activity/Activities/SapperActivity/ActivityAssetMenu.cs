﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity
{
    public static class ActivityAssetMenu
    {
        public const string Menu = Activity.ActivityAssetMenu.Menu + "/" + nameof(SapperActivity);
    }
}