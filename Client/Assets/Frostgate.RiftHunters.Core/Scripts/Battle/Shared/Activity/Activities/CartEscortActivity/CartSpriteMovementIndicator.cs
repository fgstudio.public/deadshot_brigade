﻿using UnityEngine;
using Frostgate.RiftHunters.Core.UI;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(CartSpriteMovementIndicator))]
    [DisallowMultipleComponent]
    public sealed class CartSpriteMovementIndicator : CartMovementIndicator<UISmoothSpriteActivator>
    {
    }
}