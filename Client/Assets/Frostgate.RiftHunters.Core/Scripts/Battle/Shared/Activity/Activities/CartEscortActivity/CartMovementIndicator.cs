﻿using System;
using Frostgate.RiftHunters.Core.UI;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    public abstract class CartMovementIndicator : MonoBehaviour
    {
        [Button]
        public abstract int InvadersCount { get; set; }
        
        [Button]
        public abstract void DisableIndication();
    }

    public abstract class CartMovementIndicator<TActivator> : CartMovementIndicator where TActivator : ISmoothActivator
    {
        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private TActivator _backwardIndication;

        [SerializeField, Required, ChildGameObjectsOnly, FoldoutGroup("References")]
        private TActivator[] _forwardIndications = Array.Empty<TActivator>();

        public sealed override int InvadersCount
        {
            get => _invadersCount;
            set
            {
                if (value == _invadersCount)
                    return;

                _invadersCount = value;
                UpdateObjects();
            }
        }

        private int _invadersCount;

        public override void DisableIndication()
        {
            SetIndicationEnabled(_backwardIndication, false);
            _forwardIndications.ForEach(a => SetIndicationEnabled(a, false));
        }

        protected virtual void UpdateObjects()
        {
            UpdateBackwardIndication();
            UpdateForwardIndications();
        }

        private void SetIndicationEnabled(TActivator indication, bool isEnabled)
        {
            if (isEnabled)
                indication.Activate();
            else
                indication.Deactivate();
        }

        private void UpdateBackwardIndication()
        {
            bool isEnabled = _invadersCount == 0;
            SetIndicationEnabled(_backwardIndication, isEnabled);
        }

        private void UpdateForwardIndications()
        {
            for (var i = 0; i < _forwardIndications.Length; i++)
            {
                TActivator indication = _forwardIndications[i];
                bool isEnabled = i < _invadersCount;
                SetIndicationEnabled(indication, isEnabled);
            }
        }
    }
}