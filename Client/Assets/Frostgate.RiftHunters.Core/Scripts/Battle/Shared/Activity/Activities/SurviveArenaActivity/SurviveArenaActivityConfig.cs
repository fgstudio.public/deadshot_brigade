﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SurviveArenaActivity
{
    [CreateAssetMenu(menuName = ActivityAssetMenu.Menu + "/" + nameof(SurviveArenaActivityConfig),
        fileName = nameof(SurviveArenaActivityConfig))]
    public sealed class SurviveArenaActivityConfig : ActivityTaskConfig, IInheritedConfig
    {
        [Required, AssetsOnly, AssetSelector]
        [SerializeField] private BotSpawnerConfig[] _spawners;

        public IReadOnlyList<BotSpawnerConfig> Spawners => _spawners;
    }
}