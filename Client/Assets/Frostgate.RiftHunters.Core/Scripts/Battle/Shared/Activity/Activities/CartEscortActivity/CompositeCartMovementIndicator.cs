﻿using Sirenix.OdinInspector;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(CompositeCartMovementIndicator))]
    [DisallowMultipleComponent]
    public sealed class CompositeCartMovementIndicator : CartMovementIndicator
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private CartMovementIndicator[] _indicators = Array.Empty<CartMovementIndicator>();

        public override int InvadersCount
        {
            get => _invadersCount;
            set
            {
                _indicators.ForEach(i => i.InvadersCount = value);
                _invadersCount = value;
            }
        }

        private int _invadersCount;

        public override void DisableIndication() => _indicators.ForEach(i => i.DisableIndication());
    }
}