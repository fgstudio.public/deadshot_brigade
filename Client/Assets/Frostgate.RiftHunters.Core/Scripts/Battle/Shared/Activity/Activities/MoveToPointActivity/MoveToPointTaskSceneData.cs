﻿using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;
using Frostgate.RiftHunters.Core.Battle.Client.Targeting;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.MoveToPointActivity
{
    public class MoveToPointTaskSceneData : ActivityTaskSceneData
    {
        [SerializeField, FoldoutGroup("References"), Required]
        private MoveToPointTaskState _taskState;

        [SerializeField, FoldoutGroup("References"), Required]
        private ObjectCatcher _targetPoint;

        [SerializeField, FoldoutGroup("View"), LabelText("Target (pointer)")]
        private TargetMarkerComponent _targetMarker;

        public MoveToPointTaskState TaskState
        {
            get => _taskState;
            private set => _taskState = value;
        }

        public override IActivityTaskState State => _taskState;
        public ObjectCatcher TargetPoint => _targetPoint;
        public TargetMarkerComponent TargetMarker => _targetMarker;

        private void OnValidate() => DiscoverDependencies();
        private void Awake() => DiscoverDependencies();

        private void DiscoverDependencies() =>
            TaskState ??= GetComponent<MoveToPointTaskState>();
    }
}