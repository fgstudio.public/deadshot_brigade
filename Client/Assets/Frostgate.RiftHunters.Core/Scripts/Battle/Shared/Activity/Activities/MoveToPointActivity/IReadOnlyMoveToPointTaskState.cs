﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.MoveToPointActivity
{
    public interface IReadOnlyMoveToPointTaskState : IReadOnlyActivityTaskState<MoveToPointActivityConfig>
    {
    }
}