﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.ClearArenaActivity
{
    [CreateAssetMenu(menuName = ActivityAssetMenu.Menu + "/" + nameof(ClearArenaActivityConfig),
        fileName = nameof(ClearArenaActivityConfig))]
    public sealed class ClearArenaActivityConfig : ActivityTaskConfig, IInheritedConfig
    {
        [Required, AssetSelector]
        [SerializeField] private BotSpawnerConfig[] _targetsToClear;

        public IReadOnlyList<BotSpawnerConfig> TargetsToClear => _targetsToClear;
    }
}