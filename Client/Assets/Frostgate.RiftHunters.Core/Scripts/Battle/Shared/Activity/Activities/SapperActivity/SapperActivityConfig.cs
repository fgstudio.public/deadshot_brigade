﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using System.Linq;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.SapperActivity
{
    [CreateAssetMenu(fileName = nameof(SapperActivityConfig),
        menuName = ActivityAssetMenu.Menu + "/" + nameof(SapperActivityConfig))]
    public sealed class SapperActivityConfig : ActivityTaskConfig, IInheritedConfig
    {
        public IReadOnlyList<ICapturableAreaConfig> AreaConfigs => _areaConfigs;

        [SerializeField, Required, ValidateInput(nameof(HasNoIdDuplicates))]
        private CapturableAreaConfig[] _areaConfigs = Array.Empty<CapturableAreaConfig>();

        private IDictionary<string, CapturableAreaConfig> ConfigsById =>
            _configsById ??= _areaConfigs.ToDictionary(c => c.Id);

        private IDictionary<string, CapturableAreaConfig> _configsById;

        [Button]
        private void Reset()
        {
            _areaConfigs = Array.Empty<CapturableAreaConfig>();
        }

        public ICapturableAreaConfig GetAreaConfigurationById(string bindingId)
        {
            if (!ConfigsById.TryGetValue(bindingId, out CapturableAreaConfig config))
                config = CapturableAreaConfig.CreateDefault(bindingId);

            return config;
        }

        private bool HasNoIdDuplicates(CapturableAreaConfig[] configs, ref string errMsg, ref InfoMessageType? msgType)
        {
            if (configs.Length == 0)
                return true;

            var set = new HashSet<string>();
            foreach (CapturableAreaConfig c in configs)
            {
                string id = c.Id;
                if (!set.Contains(id))
                {
                    set.Add(id);
                }
                else
                {
                    errMsg = $"Duplicate BindingId '{id}' found.";
                    msgType = InfoMessageType.Error;

                    return false;
                }
            }

            return true;
        }
    }
}