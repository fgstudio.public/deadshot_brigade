﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    public sealed partial class MovementPath
    {
        private void OnDrawGizmos()
        {
            if (Waypoints.Length == 0)
                return;

            Color oldColor = Gizmos.color;
            Gizmos.color = Color.green;

            DrawPathGizmo();

            Gizmos.color = oldColor;
        }

        private void DrawPathGizmo()
        {
            for (int i = 1; i < Waypoints.Length; i++)
            {
                HighlightPoint(Waypoints[i - 1]);
                ConnectPoints(Waypoints[i - 1], Waypoints[i]);
            }

            HighlightPoint(Waypoints[^1]);

            void ConnectPoints(Transform point1, Transform point2) => Gizmos.DrawLine(point1.position, point2.position);
            void HighlightPoint(Transform point) => Gizmos.DrawSphere(point.position, 0.1f);
        }
    }
}