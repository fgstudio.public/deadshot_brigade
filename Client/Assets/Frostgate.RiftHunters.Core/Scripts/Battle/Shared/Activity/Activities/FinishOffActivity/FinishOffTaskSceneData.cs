﻿using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.FinishOffActivity
{
    [DisallowMultipleComponent]
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(FinishOffTaskSceneData))]
    public sealed class FinishOffTaskSceneData : ActivityTaskSceneData
    {
        [field: SerializeField, Required, ChildGameObjectsOnly] public FinishOffTaskState TaskState { get; private set; }

        public override IActivityTaskState State => TaskState;

        private void OnValidate() => DiscoverDependencies();
        private void Awake() => DiscoverDependencies();

        private void DiscoverDependencies() =>
            TaskState ??= GetComponent<FinishOffTaskState>();
    }
}