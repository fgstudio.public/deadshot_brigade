﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.CartEscortActivity
{
    [AddComponentMenu(Activity.ActivityAssetMenu.Menu + "/" + nameof(RailCartView))]
    [DisallowMultipleComponent]
    public class RailCartView : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private CartMovementIndicator _movementIndicator;

        [SerializeField, Required, ChildGameObjectsOnly]
        private GameObject _model;

        public CartMovementIndicator MovementIndicator => _movementIndicator;

        [Button]
        public void SetActive(bool isActive)
        {
            _model.SetActive(isActive);
        }
    }
}