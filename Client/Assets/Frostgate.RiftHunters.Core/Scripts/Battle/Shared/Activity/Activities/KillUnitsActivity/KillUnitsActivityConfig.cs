﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Activities.KillUnitsActivity
{
    [CreateAssetMenu(menuName = ActivityAssetMenu.Menu + "/" + nameof(KillUnitsActivityConfig),
        fileName = nameof(KillUnitsActivityConfig))]
    public sealed class KillUnitsActivityConfig : ActivityTaskConfig, IInheritedConfig
    {
        [Required, AssetsOnly, AssetSelector]
        [SerializeField] private UnitConfig[] _units;

        public IReadOnlyList<UnitConfig> Units => _units;
    }
}