﻿using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Internal;
using JetBrains.Annotations;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity
{
    public static class ActivityConfigSynchronization
    {
        private const string NullConfigId = "5e3d98e98570f178d0fa0f18cec4b655";

        private static IReadOnlyDictionary<string, ActivityConfigBase> _configs;


        public static void Init([NotNull] IConfigCollection<ActivityConfig> activityConfigs)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(activityConfigs, nameof(activityConfigs));

            _configs = activityConfigs.SelectMany(c =>
                {
                    var taskConfigs = c.GetAllTasks();
                    var configs = new List<ActivityConfigBase>(taskConfigs.Count + 1) { c };
                    configs.AddRange(taskConfigs);

                    return configs;
                })
                .ToDictionary(c => c.Id);
        }

        [UsedImplicitly]
        public static void WriteActivityConfig(this NetworkWriter writer, ActivityConfigBase config)
        {
            // ReSharper disable once Unity.NoNullPropagation
            writer.WriteString(config?.Id ?? NullConfigId);
        }

        [UsedImplicitly]
        public static ActivityConfigBase ReadActivityConfig(this NetworkReader reader)
        {
            string id = reader.ReadString();

            return id == NullConfigId ? null : _configs[id];
        }
    }
}