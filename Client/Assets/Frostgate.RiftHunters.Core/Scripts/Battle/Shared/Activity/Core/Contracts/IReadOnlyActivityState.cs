﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity
{
    public interface IReadOnlyActivityState
    {
        event StateStatusChanged<IReadOnlyActivityState> StatusChanged;

        ActivityStatus Status { get; }
        ActivityConfig Config { get; }
    }
}