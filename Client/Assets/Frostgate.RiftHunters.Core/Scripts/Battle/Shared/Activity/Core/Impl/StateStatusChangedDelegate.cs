﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity
{
    public delegate void StateStatusChanged<in T>(T state, ActivityStatus prevStatus, ActivityStatus newStatus);
}