﻿using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Internal;
using System;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity
{
    public abstract class ActivityTaskState<TDerived, TConfig> : ActivityStateBase<TDerived, TConfig>,
        IActivityTaskState<TConfig>, IReadOnlyActivityTaskState<TConfig>
        where TDerived : ActivityTaskState<TDerived, TConfig>
        where TConfig : ActivityTaskConfig
    {
        [SerializeField] private UnityEvent _activated = new();
        [SerializeField] private UnityEvent _deactivated = new();
        [SerializeField] private UnityEvent _completed = new();

        event StateStatusChanged<IActivityTaskState> IActivityTaskState.StatusChanged
        {
            add => _eventDelegates.Add(value);
            remove => _eventDelegates.Remove(value);
        }

        event StateStatusChanged<IActivityTaskState<TConfig>> IActivityTaskState<TConfig>.StatusChanged
        {
            add => _eventDelegates.Add(value);
            remove => _eventDelegates.Remove(value);
        }

        event StateStatusChanged<IReadOnlyActivityTaskState> IReadOnlyActivityTaskState.StatusChanged
        {
            add => _eventDelegates.Add(value);
            remove => _eventDelegates.Remove(value);
        }

        event StateStatusChanged<IReadOnlyActivityTaskState<TConfig>> IReadOnlyActivityTaskState<TConfig>.StatusChanged
        {
            add => _eventDelegates.Add(value);
            remove => _eventDelegates.Remove(value);
        }

        // Велосипед с событиями нужен для избегания ArgumentException при ковариации делегатов.
        // StateStatusChanged<IActivityTaskState> IActivityTaskState.StatusChanged
        //{
        //      add => StatusChanged += value;
        //      remove => StatusChanged -= value;
        //}, где StatusChanged == ActivityStateBase.StatusChanged. Этот способ не сработает.

        public string ConfigId => Config.Id;
        public UnityEvent Activated => _activated;
        public UnityEvent Deactivated => _deactivated;
        public UnityEvent Completed => _completed;

        private readonly HashSet<StateStatusChanged<TDerived>> _eventDelegates = new();


        [Server]
        public virtual void ResetProgress()
        {
        }

        protected override void OnStatusChanged(ActivityStatus prevStatus, ActivityStatus newStatus)
        {
            base.OnStatusChanged(prevStatus, newStatus);

            _eventDelegates.ForEach(d => d.Invoke((TDerived)this, prevStatus, newStatus));
            GetUnityEventByStatus(newStatus).Invoke();
        }

        private UnityEvent GetUnityEventByStatus(ActivityStatus status)
        {
            return status switch
            {
                ActivityStatus.Inactive => _deactivated,
                ActivityStatus.Active => _activated,
                ActivityStatus.Completed => _completed,
                _ => throw new ArgumentOutOfRangeException($"UnityEvent for status '{status}' not implemented."),
            };
        }
    }
}