﻿using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Internal;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity
{
    [CreateAssetMenu(
        fileName = nameof(ActivityConfig),
        menuName = ActivityAssetMenu.Menu + "/" + nameof(ActivityConfig))]
    public sealed class ActivityConfig : ActivityConfigBase
    {
        [SerializeField, BoxGroup] private TaskRootNode _taskSequence;

        private IReadOnlyList<ActivityTaskConfig> _rootTasksCache;
        private IDictionary<ActivityTaskConfig, IReadOnlyList<ActivityTaskConfig>> _childTasksCache;

        public IReadOnlyList<ActivityTaskConfig> GetAllTasks()
        {
            return _rootTasksCache ??= _taskSequence.Nodes.Select(n => n.Config).ToArray();
        }
    }
}