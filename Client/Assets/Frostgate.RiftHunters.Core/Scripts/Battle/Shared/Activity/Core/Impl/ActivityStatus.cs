﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity
{
    public enum ActivityStatus : byte
    {
        Inactive = 0,
        Active = 10,
        Completed = 20
    }
}