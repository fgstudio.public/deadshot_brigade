﻿using Mirror;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Assertions;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Internal
{
    public abstract class ActivityStateBase : NetworkBehaviour
    {
        public ActivityStatus Status => _status;
        public bool IsInitialized => _isInitialized;

        protected ActivityConfigBase BaseConfig => _config;

        [SyncVar(hook = nameof(OnStatusChangedHook)), SerializeField]
        private ActivityStatus _status;

        [SyncVar] private bool _isInitialized;
        [SyncVar] private ActivityConfigBase _config;

        [Server]
        public void SetActivated()
        {
            Assert.AreNotEqual(ActivityStatus.Active, _status, $"{GetType().Name} activated when was active.");
            SetStatus(ActivityStatus.Active);
        }

        [Server]
        public void SetDeactivated()
        {
            Assert.AreNotEqual(ActivityStatus.Inactive, _status, $"{GetType().Name} deactivated when was inactive.");
            SetStatus(ActivityStatus.Inactive);
        }

        [Server]
        public void SetCompleted()
        {
            Assert.AreNotEqual(ActivityStatus.Completed, _status, $"{GetType().Name} completed when was completed.");
            SetStatus(ActivityStatus.Completed);
        }

        [Server]
        protected void Init([NotNull] ActivityConfigBase config)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(config, nameof(config));
            Assert.IsNull(_config, $"{GetType().Name} config changed twice.");

            _config = config;
            _isInitialized = true;
        }

        protected virtual void OnStatusChanged(ActivityStatus prevStatus, ActivityStatus newStatus)
        {
        }

        private void SetStatus(ActivityStatus status)
        {
            ActivityStatus prevStatus = _status;
            _status = status;

            OnStatusChanged(prevStatus, _status);
        }

        private void OnStatusChangedHook(ActivityStatus prevStatus, ActivityStatus newStatus)
        {
            if (isClientOnly)
                OnStatusChanged(prevStatus, newStatus);
        }
    }

    public abstract class ActivityStateBase<TDerived, TConfig> : ActivityStateBase
        where TDerived : ActivityStateBase<TDerived, TConfig>
        where TConfig : ActivityConfigBase
    {
        [SerializeField] private UnityEvent<TDerived, ActivityStatus, ActivityStatus> _statusChanged = new();

        public event StateStatusChanged<TDerived> StatusChanged = delegate { };

        public TConfig Config => (TConfig)BaseConfig;

        [Server]
        public void Init([NotNull] TConfig config) => base.Init(config);

        protected override void OnStatusChanged(ActivityStatus prevStatus, ActivityStatus newStatus)
        {
            base.OnStatusChanged(prevStatus, newStatus);

            InvokeStatusChanged(prevStatus, newStatus);
        }

        private void InvokeStatusChanged(ActivityStatus prevStatus, ActivityStatus newStatus)
        {
            _statusChanged.Invoke((TDerived)this, prevStatus, newStatus);
            StatusChanged.Invoke((TDerived)this, prevStatus, newStatus);
        }
    }
}