﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Internal
{
    [Serializable]
    public struct TaskRootNode
    {
        [SerializeField] private TaskNode[] _nodes;
        
        public IReadOnlyList<TaskNode> Nodes => _nodes ??= Array.Empty<TaskNode>();
        
        public TaskRootNode(params TaskNode[] nodes)
        {
            _nodes = nodes;
        }
    }
}