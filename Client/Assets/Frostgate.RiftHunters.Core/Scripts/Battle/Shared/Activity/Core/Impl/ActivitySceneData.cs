﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(ActivitySceneData))]
    [DisallowMultipleComponent]
    public sealed class ActivitySceneData : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private ActivityState _state;

        [SerializeField, Required, ChildGameObjectsOnly]
        private ActivityTaskSceneData[] _tasksData = Array.Empty<ActivityTaskSceneData>();

        public ActivityState State => _state;
        public IReadOnlyList<ActivityTaskSceneData> TasksData => _tasksData;

        private void OnValidate() => DiscoverDependencies();
        private void Awake() => DiscoverDependencies();

        private void DiscoverDependencies()
        {
            _state ??= GetComponentInChildren<ActivityState>();

            _tasksData = _tasksData.Length > 0
                ? _tasksData
                : GetComponentsInChildren<ActivityTaskSceneData>();
        }
    }
}