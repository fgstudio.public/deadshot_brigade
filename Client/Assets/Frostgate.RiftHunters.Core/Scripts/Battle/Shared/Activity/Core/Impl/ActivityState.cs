﻿using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity.Internal;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity
{
    [AddComponentMenu(ActivityComponentMenu.Menu + "/" + nameof(ActivityState))]
    [DisallowMultipleComponent]
    public sealed class ActivityState : ActivityStateBase<ActivityState, ActivityConfig>, IActivityState,
        IReadOnlyActivityState
    {
        event StateStatusChanged<IActivityState> IActivityState.StatusChanged
        {
            add => _eventDelegates.Add(value);
            remove => _eventDelegates.Remove(value);
        }

        event StateStatusChanged<IReadOnlyActivityState> IReadOnlyActivityState.StatusChanged
        {
            add => _eventDelegates.Add(value);
            remove => _eventDelegates.Remove(value);
        }

        // Велосипед с событиями нужен для избегания ArgumentException при ковариации делегатов.
        // StateStatusChanged<IActivityState> IActivityTaskState.StatusChanged
        //{
        //      add => StatusChanged += value;                                                     
        //      remove => StatusChanged -= value;
        //}, где StatusChanged == ActivityStateBase.StatusChanged. Этот способ не сработает.

        private readonly HashSet<StateStatusChanged<ActivityState>> _eventDelegates = new();
        
        protected override void OnStatusChanged(ActivityStatus prevStatus, ActivityStatus newStatus)
        {
            base.OnStatusChanged(prevStatus, newStatus);
            
            _eventDelegates.ForEach(d => d.Invoke(this, prevStatus, newStatus));
        }
    }
}