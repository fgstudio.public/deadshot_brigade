﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity
{
    public interface IActivityState
    {
        event StateStatusChanged<IActivityState> StatusChanged;

        ActivityStatus Status { get; }
        ActivityConfig Config { get; }

        void Init([NotNull] ActivityConfig config);
        void SetActivated();
        void SetDeactivated();
        void SetCompleted();
    }
}