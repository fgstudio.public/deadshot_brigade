﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity
{
    public abstract class ActivityTaskSceneData : MonoBehaviour
    {
        [SerializeField, Required]
        private Transform[] _staticTargets = Array.Empty<Transform>();

        public abstract IActivityTaskState State { get; }

        public IReadOnlyList<Transform> StaticTargets => _staticTargets;
    }
}