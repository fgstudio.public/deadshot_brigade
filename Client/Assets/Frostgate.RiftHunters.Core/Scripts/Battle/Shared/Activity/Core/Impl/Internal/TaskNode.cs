﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity.Internal
{
    [Serializable]
    public struct TaskNode
    {
        [SerializeField, Required, AssetSelector, AssetsOnly]
        private ActivityTaskConfig _config;

        public ActivityTaskConfig Config => _config;

        public TaskNode([NotNull] ActivityTaskConfig config)
        {
            _config = config;
        }
    }
}