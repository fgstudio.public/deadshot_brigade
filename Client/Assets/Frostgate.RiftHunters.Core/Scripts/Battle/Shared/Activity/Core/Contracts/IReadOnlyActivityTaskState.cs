﻿using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity
{
    public interface IReadOnlyActivityTaskState
    {
        event StateStatusChanged<IReadOnlyActivityTaskState> StatusChanged;
        string ConfigId { get; }
        UnityEvent Activated { get; }
        UnityEvent Deactivated { get; }
        UnityEvent Completed { get; }

        ActivityStatus Status { get; }
    }

    public interface IReadOnlyActivityTaskState<out TConfig> : IReadOnlyActivityTaskState
        where TConfig : ActivityTaskConfig
    {
        new event StateStatusChanged<IReadOnlyActivityTaskState<TConfig>> StatusChanged;

        public TConfig Config { get; }
    }
}