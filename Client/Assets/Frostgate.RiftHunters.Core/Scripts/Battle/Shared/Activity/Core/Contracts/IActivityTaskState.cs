﻿using UnityEngine.Events;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Activity
{
    public interface IActivityTaskState
    {
        event StateStatusChanged<IActivityTaskState> StatusChanged;
        string ConfigId { get; }
        UnityEvent Activated { get; }
        UnityEvent Deactivated { get; }
        UnityEvent Completed { get; }

        ActivityStatus Status { get; }
        bool IsInitialized { get; }

        void SetActivated();
        void SetDeactivated();
        void SetCompleted();
        void ResetProgress();
    }

    public interface IActivityTaskState<TConfig> : IActivityTaskState
        where TConfig : ActivityTaskConfig
    {
        new event StateStatusChanged<IActivityTaskState<TConfig>> StatusChanged;

        TConfig Config { get; }

        void Init([NotNull] TConfig config);
    }
}