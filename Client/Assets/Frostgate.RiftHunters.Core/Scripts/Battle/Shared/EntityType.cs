﻿using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [Flags]
    public enum EntityType
    {
        All = ~0, // до 1 << 30 включительно
        Other = 1,

        Trap = 2,
        Totem = 4,
        Mine = 8,
        WaveSpawner = 16,
        LinkSpawner = 32,
        Shield = 64,
    }
}