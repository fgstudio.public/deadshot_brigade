﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.MissionScore
{
    public interface IMissionScoreFactory
    {
        IMissionScore Create([NotNull] IMissionScoreConfig config);
    }

    public sealed class MissionScoreFactory : IMissionScoreFactory
    {
        [NotNull] private readonly IScoreConditionValidatorFactory _validatorFactory;

        public MissionScoreFactory([NotNull] IScoreConditionValidatorFactory validatorFactory)
        {
            _validatorFactory = validatorFactory;
        }

        public IMissionScore Create(IMissionScoreConfig config) => new MissionScore(config, _validatorFactory);
    }
}