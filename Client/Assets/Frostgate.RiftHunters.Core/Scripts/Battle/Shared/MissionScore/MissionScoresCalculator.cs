﻿using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Network.Shared;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.MissionScore
{
    public interface IMissionScoresCalculator
    {
        public int Calculate([NotNull] UnitNetwork unit);
    }

    public sealed class MissionScoresCalculator : IMissionScoresCalculator
    {
        [NotNull] private readonly IEnumerable<IMissionScore> _scores;

        public MissionScoresCalculator([NotNull] IMissionScoresConfig config,
            [NotNull] IMissionScoreFactory scoreFactory)
        {
            _scores = config.ScoreConfigs.Select(scoreFactory.Create);
        }

        public int Calculate(UnitNetwork unit) => _scores.Count(s => s.IsCredited(unit));
    }
}