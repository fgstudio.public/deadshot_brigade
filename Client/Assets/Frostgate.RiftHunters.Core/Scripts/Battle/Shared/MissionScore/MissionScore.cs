﻿using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Network.Shared;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.MissionScore
{
    public interface IMissionScore
    {
        bool IsCredited([NotNull] UnitNetwork unit);
    }

    public sealed class MissionScore : IMissionScore
    {
        [NotNull] private IEnumerable<IScoreConditionValidator> _validators;

        public MissionScore([NotNull] IMissionScoreConfig config,
            [NotNull] IScoreConditionValidatorFactory validatorFactory)
        {
            _validators = config.Conditions.Select(validatorFactory.Create);
        }

        public bool IsCredited(UnitNetwork unit) => _validators.All(v => v.IsFulfilled(unit));
    }
}