﻿using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.MissionScore
{
    public interface IScoreConditionValidator
    {
        bool IsFulfilled([NotNull] UnitNetwork unit);
    }
}