﻿using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.MissionScore
{
    public sealed class PlayerDeathsNumberLessThanValidator : IScoreConditionValidator
    {
        [NotNull] private readonly IPlayerDeathsNumberLessThanCondition _condition;

        public PlayerDeathsNumberLessThanValidator([NotNull] IPlayerDeathsNumberLessThanCondition condition)
        {
            _condition = condition;
        }

        public bool IsFulfilled(UnitNetwork unit) => unit.UnitNetworkState.DeathsCounter < _condition.DeathsNumber;
    }
}