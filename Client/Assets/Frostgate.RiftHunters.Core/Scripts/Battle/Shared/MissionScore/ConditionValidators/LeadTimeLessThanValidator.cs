﻿using System;
using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.MissionScore
{
    public sealed class LeadTimeLessThanValidator : IScoreConditionValidator
    {
        [NotNull] private readonly ILeadTimeCondition _condition;
        [NotNull] private readonly IReadOnlyDictionary<string, BotArenaData> _arenaDatas;

        public LeadTimeLessThanValidator([NotNull] ILeadTimeCondition condition,
            [NotNull] IReadOnlyDictionary<string, BotArenaData> arenaDatas)
        {
            _condition = condition;
            _arenaDatas = arenaDatas;
        }

        public bool IsFulfilled(UnitNetwork _)
        {
            BotArenaData startArena = _arenaDatas[_condition.StartArenaId];
            BotArenaData lastArena = _arenaDatas.Values.OrderBy(a => a.Index).Last();

            TimeSpan sessionDuration = TimeSpan.FromSeconds(lastArena.CompletionTime - startArena.StartTime);

            return sessionDuration < _condition.LeadTime;
        }
    }
}