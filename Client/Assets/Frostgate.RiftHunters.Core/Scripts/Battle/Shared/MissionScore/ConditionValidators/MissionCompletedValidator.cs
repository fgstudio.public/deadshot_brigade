﻿using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Network.Shared;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.MissionScore
{
    public sealed class MissionCompletedValidator : IScoreConditionValidator
    {
        [NotNull] private readonly IMissionCompletedCondition _condition;
        [NotNull] private readonly IReadOnlyBotArenaState _arenaState;

        public MissionCompletedValidator([NotNull] IMissionCompletedCondition condition,
            [NotNull] IReadOnlyBotArenaState arenaState)
        {
            _condition = condition;
            _arenaState = arenaState;
        }

        public bool IsFulfilled(UnitNetwork _) => _arenaState.AreAllArenasCompleted;
    }
}