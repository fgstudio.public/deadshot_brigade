﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.MissionScore
{
    public interface IScoreConditionValidatorFactory
    {
        IScoreConditionValidator Create([NotNull] IMissionScoreCondition condition);
    }

    public sealed class ScoreConditionValidatorFactory : IScoreConditionValidatorFactory
    {
        [NotNull] private readonly IReadOnlyBotArenaState _arenaState;

        public ScoreConditionValidatorFactory([NotNull] IReadOnlyBotArenaState arenaState)
        {
            _arenaState = arenaState;
        }

        public IScoreConditionValidator Create(IMissionScoreCondition condition)
        {
            return condition.Type switch
            {
                ConditionType.MissionCompleted => CreateMissionCompletedValidator(condition),
                ConditionType.LeadTimeLessThan => CreateLeadTimeLessThanValidator(condition),
                ConditionType.PlayerDeathsNumberLessThan => CreatePlayerDeathsNumberLessThanValidator(condition),
                _ => throw new ArgumentOutOfRangeException(
                    $"Condition validator for '{condition.Type}' not implemented.")
            };
        }

        private IScoreConditionValidator CreateMissionCompletedValidator(IMissionScoreCondition condition)
        {
            return new MissionCompletedValidator((IMissionCompletedCondition)condition, _arenaState);
        }

        private IScoreConditionValidator CreateLeadTimeLessThanValidator(IMissionScoreCondition condition)
        {
            return new LeadTimeLessThanValidator((ILeadTimeCondition)condition, _arenaState.Data);
        }

        private IScoreConditionValidator CreatePlayerDeathsNumberLessThanValidator(IMissionScoreCondition condition)
        {
            return new PlayerDeathsNumberLessThanValidator((IPlayerDeathsNumberLessThanCondition)condition);
        }
    }
}