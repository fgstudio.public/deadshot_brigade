﻿using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.MissionScore
{
    public interface IMissionScoresConfig : IConfig
    {
        string StartArenaId { get; }
        IReadOnlyList<IMissionScoreConfig> ScoreConfigs { get; }
    }

    [HelpURL("https://www.notion.so/frostgate/e2b6f26199d042b6a7fa1b767ce3b237")]
    [CreateAssetMenu(menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(MissionScoresConfig),
        fileName = nameof(MissionScoresConfig))]
    public sealed class MissionScoresConfig : ScriptableConfig, IMissionScoresConfig
    {
        [SerializeField, Required, AssetSelector, AssetsOnly]
        private BotArenaConfig _startArenaConfig;

        [SerializeField, ListDrawerSettings(Expanded = true, CustomAddFunction = nameof(AddScoreConfig))]
        private List<MissionScoreConfig> _scoreConfigs = new();

        public string StartArenaId => _startArenaConfig.Id;
        public IReadOnlyList<MissionScoreConfig> ScoreConfigs => _scoreConfigs;

        IReadOnlyList<IMissionScoreConfig> IMissionScoresConfig.ScoreConfigs => ScoreConfigs;

        private void AddScoreConfig()
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(_startArenaConfig, nameof(_startArenaConfig));

            var config = new MissionScoreConfig(StartArenaId);
            _scoreConfigs.Add(config);
        }
    }
}