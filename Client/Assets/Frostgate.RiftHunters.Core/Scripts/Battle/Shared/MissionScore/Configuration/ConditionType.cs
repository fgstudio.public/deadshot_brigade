﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.MissionScore
{
    public enum ConditionType : byte
    {
        MissionCompleted,
        LeadTimeLessThan,
        PlayerDeathsNumberLessThan,
    }
}