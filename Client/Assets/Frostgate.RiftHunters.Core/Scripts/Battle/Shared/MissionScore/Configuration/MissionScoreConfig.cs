﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.MissionScore
{
    public interface IMissionScoreConfig
    {
        IReadOnlyList<IMissionScoreCondition> Conditions { get; }
    }

    [Serializable]
    public sealed class MissionScoreConfig : IMissionScoreConfig
    {
        [SerializeField, HideInInspector] private string _startArenaId;

        [SerializeField, ListDrawerSettings(Expanded = true, CustomAddFunction = nameof(AddCondition))]
        private List<MissionCondition> _conditions = new();

        public IReadOnlyList<MissionCondition> Conditions => _conditions;

        IReadOnlyList<IMissionScoreCondition> IMissionScoreConfig.Conditions => Conditions;

        public MissionScoreConfig(string startArenaId)
        {
            _startArenaId = startArenaId;
        }

        private void AddCondition()
        {
            var condition = new MissionCondition(_startArenaId);
            _conditions.Add(condition);
        }
    }
}