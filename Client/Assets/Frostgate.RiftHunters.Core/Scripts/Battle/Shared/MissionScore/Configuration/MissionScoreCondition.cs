﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.MissionScore
{
    #region Condition Interfaces

    public interface IMissionScoreCondition
    {
        ConditionType Type { get; }
    }

    public interface IMissionCompletedCondition : IMissionScoreCondition
    {
    }
    
    public interface ILeadTimeCondition : IMissionScoreCondition
    {
        string StartArenaId { get; }
        TimeSpan LeadTime { get; }
    }

    public interface IPlayerDeathsNumberLessThanCondition : IMissionScoreCondition
    {
        int DeathsNumber { get; }
    }

    #endregion

    [Serializable]
    public sealed class MissionCondition :
        IMissionScoreCondition,
        IMissionCompletedCondition,
        ILeadTimeCondition,
        IPlayerDeathsNumberLessThanCondition
    {
        [SerializeField, HideInInspector] private string _startArenaId;
        [SerializeField] private ConditionType _type;

        #region LeadTimeLessThan Parameters
        
        [SerializeField, MinValue(0), ShowIf(nameof(_type), ConditionType.LeadTimeLessThan)]
        private float _leadTimeSeconds;

        #endregion

        #region PlayerDeathsNumberLessThan Parameters

        [SerializeField, MinValue(0), ShowIf(nameof(_type), ConditionType.PlayerDeathsNumberLessThan)]
        private int _deathsNumber;

        #endregion

        public ConditionType Type => _type;

        public string StartArenaId => _startArenaId;
        public TimeSpan LeadTime => TimeSpan.FromSeconds(_leadTimeSeconds);
        public int DeathsNumber => _deathsNumber;

        public MissionCondition(string startArenaId)
        {
            _startArenaId = startArenaId;
        }
    }
}