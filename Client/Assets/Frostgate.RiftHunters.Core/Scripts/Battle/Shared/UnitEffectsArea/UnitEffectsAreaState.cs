using Mirror;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(UnitEffectsAreaState))]
    public sealed class UnitEffectsAreaState : NetworkBehaviour
    {
        [SyncVar(hook = nameof(OnActiveChanged))]
        [SerializeField] private bool _isActive;

        [field: SerializeField] public UnityEvent<bool> ActiveChanged { get; private set; }

        public bool IsActive => _isActive;

        [Server]
        public void SetActive(bool status) =>
            NetworkHelper.ModifySyncVar(
                _isActive, status, v => _isActive = v, OnActiveChanged);

        private void OnActiveChanged(bool oldStatus, bool newStatus)
        {
            if (oldStatus != newStatus)
                ActiveChanged?.Invoke(newStatus);
        }
    }
}