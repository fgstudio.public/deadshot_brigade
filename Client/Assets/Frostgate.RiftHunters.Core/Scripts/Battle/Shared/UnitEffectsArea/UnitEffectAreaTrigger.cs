using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Filters;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using JetBrains.Annotations;
using Mirror;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public class UnitEffectAreaTrigger : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private int _filteringParentIndex;

        [Header("Info")]
        [Sirenix.OdinInspector.ShowInInspector, ReadOnly] private IFilter[] _filters;

        private class UnitData
        {
            public UnitNetwork UnitNetwork;
            public List<UnitEffectState> States;
        }

        private UnitEffectConfig[] _effects;
        private IObjectCollection<Collider, UnitNetwork> _units;
        private UnitEffectStateFactory _factory;
        private NetworkIdentity _source;
        private readonly Dictionary<Collider, UnitData> _effectedUnits = new();

        public void Init(UnitEffectConfig[] effects, IObjectCollection<Collider, UnitNetwork> units, UnitEffectStateFactory factory, NetworkIdentity source)
        {
            var trigger = gameObject.AddComponent<BoxCollider>();
            trigger.isTrigger = true;

            _source = source;
            _effects = effects;
            _units = units;
            _factory = factory;
        }

        private void Awake() => InitFilters();

        private void OnDisable()
        {
            foreach (var effectedUnitsValue in _effectedUnits.Values)
                ClearStates(effectedUnitsValue);

            _effectedUnits.Clear();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!_units.Contains(other) || _effectedUnits.ContainsKey(other))
                return;

            GameObject go = other.gameObject;

            if (!IsFiltered(go))
                return;

            var unit = _units[other];

            var unitData = new UnitData {UnitNetwork = unit, States = new List<UnitEffectState>()};
            _effectedUnits.Add(other, unitData);

            foreach (var unitEffectConfig in _effects)
            {
                var state = _factory.Create(unitEffectConfig, _source);
                unitData.States.Add(state);
                unit.AddUnitEffect(state);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (!_effectedUnits.ContainsKey(other))
                return;

            var unitData = _effectedUnits[other];

            ClearStates(unitData);

            _effectedUnits.Remove(other);
        }

        private bool IsFiltered([NotNull] GameObject gameObject)
        {
            GameObject filteringObject = gameObject.transform
                .GetRecursiveParent(_filteringParentIndex)?.gameObject;

            if (filteringObject == null)
                return false;

            for (int i = 0; i < _filters.Length; i++)
            {
                bool isMatching = _filters[i].IsMatching(filteringObject);
                if (!isMatching) return false;
            }

            return true;
        }

        private void InitFilters() => _filters = GetComponents<IFilter>();

        private void ClearStates(UnitData unitData)
        {
            foreach (var unitEffectState in unitData.States)
                unitData.UnitNetwork.RemoveUnitEffect(unitEffectState);
        }
    }
}