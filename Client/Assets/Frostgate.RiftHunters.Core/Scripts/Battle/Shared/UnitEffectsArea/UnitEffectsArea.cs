using Mirror;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(UnitEffectsArea))]
    public sealed class UnitEffectsArea : NetworkBehaviour
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private UnitEffectsAreaState _state;

        [SerializeField, CanBeNull] private Transform _viewPrefab;
        [SerializeField, CanBeNull] private UnitEffectAreaTrigger _trigger;
        [SerializeField, Required] private UnitEffectConfig[] _effects;

        private IObjectCollection<Collider, UnitNetwork> _units;
        private UnitEffectStateFactory _stateFactory;

        [CanBeNull] private Transform _view;

        public UnitEffectsAreaState State => _state;

        [Inject]
        private void MonoConstructor(
            ObjectCollectionRepository<UnitNetwork> unitCollections,
            UnitEffectStateFactory stateFactory)
        {
            _units = unitCollections.Get<Collider>();
            _stateFactory = stateFactory;
        }

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _state ??= GetComponentInChildren<UnitEffectsAreaState>();

        private void Start()
        {
            Subscribe(_state);
            UpdateComponents(_state.IsActive);
        }

        private void OnDestroy() =>
            Unsubscribe(_state);

        public override void OnStartServer() => InitTrigger();
        public override void OnStartClient() => InitView();

        private void Subscribe([NotNull] UnitEffectsAreaState state) =>
            state.ActiveChanged.AddListener(UpdateComponents);

        private void Unsubscribe([NotNull] UnitEffectsAreaState state) =>
            state.ActiveChanged.RemoveListener(UpdateComponents);

        private void InitTrigger()
        {
            _trigger!.Init(_effects, _units, _stateFactory, netIdentity);

            UpdateComponents(_state.IsActive);
        }

        private void InitView()
        {
            _view = Instantiate(_viewPrefab, transform);
            UpdateComponents(_state.IsActive);
        }

        private void UpdateComponents(bool status)
        {
            _trigger?.gameObject.SetActive(status);
            _view?.gameObject.SetActive(status);
        }
    }
}