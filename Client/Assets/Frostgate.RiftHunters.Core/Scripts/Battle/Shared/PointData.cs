using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [Serializable]
    public readonly struct PointData
    {
        public Vector3 Position { get; }
        public Vector3 Rotation { get; }

        public PointData(Vector3 position, Vector3 rotation)
        {
            Position = position;
            Rotation = rotation;
        }
    }
}
