﻿namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface ISpawnable
    {
        EntityType Type { get; }
    }
}