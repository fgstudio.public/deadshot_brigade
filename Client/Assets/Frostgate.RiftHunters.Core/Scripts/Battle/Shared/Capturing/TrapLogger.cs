using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Trap))]
    [HelpURL("https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(TrapLogger))]
    public sealed class TrapLogger : MonoBehaviour
    {
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly]
        private Trap _trap;

        private ILogger _logger;

        [Inject]
        private void MonoConstruct(ILogger<Trap> logger) =>
            _logger = logger;

        private void OnValidate() => Init();
        private void Awake()
        {
            Init();
            Subscribe();
        }
        private void OnDestroy()
        {
            Unsubscribe();
        }

        private void Init() =>
            _trap ??= GetComponent<Trap>();

        private void Subscribe()
        {
            _trap.Destroyed.AddListener(OnDestroyed);
            _trap.State.UnitLost += OnUnitLost;
            _trap.State.UnitCaptured += OnUnitCaptured;
        }

        private void Unsubscribe()
        {
            _trap.Destroyed.RemoveListener(OnDestroyed);
            _trap.State.UnitLost -= OnUnitLost;
            _trap.State.UnitCaptured -= OnUnitCaptured;
        }

        private void OnDestroyed() => ShortLog(nameof(_trap.Destroyed));
        private void OnUnitLost(uint unitNetId) => FullLog(nameof(_trap.State.UnitLost), unitNetId);
        private void OnUnitCaptured(uint unitNetId) => FullLog(nameof(_trap.State.UnitCaptured), unitNetId);

        private void FullLog(string action, uint unitNetId) =>
            _logger.Log($"{action}, {nameof(Trap)} netId: {_trap.Identity.netId}, Unit netId: {unitNetId}");

        private void ShortLog(string action) =>
            _logger.Log($"{action}, {nameof(Trap)} netId: {_trap.Identity.netId}");
    }
}