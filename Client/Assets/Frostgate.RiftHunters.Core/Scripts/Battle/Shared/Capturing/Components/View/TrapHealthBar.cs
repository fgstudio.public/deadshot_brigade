using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(TrapHealthBar))]
    public sealed class TrapHealthBar : MonoBehaviour
    {
        [Required, ChildGameObjectsOnly] public Bar Bar;
        [Required, ChildGameObjectsOnly] public TrapState State;

        private void OnValidate()
        {
            Bar ??= GetComponentInChildren<Bar>();
            State ??= GetComponentInChildren<TrapState>();
        }

        private void Start()
        {
            State.HealthChanged += UpdateHealth;
            UpdateHealth(default);
        }

        private void OnDestroy()
        {
            State.HealthChanged -= UpdateHealth;
        }

        [ContextMenu(nameof(UpdateHealth))]
        private void UpdateHealth(float diff) =>
            Bar.Set(State.Health, State.MaxHealth);
    }
}