using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    public interface ITrapPhysics
    {
        Collider Collider { get; }
        float BodyRadius { get; }
        Vector3 FocusPoint { get; }
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(TrapPhysics))]
    public sealed class TrapPhysics : Component, ITrapPhysics
    {
        [Required, ChildGameObjectsOnly] public BoxCollider BodyCollider;
        [Required, ChildGameObjectsOnly] public Collider[] Colliders;
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _focusPoint;

        [Title("Info")]
        [ShowInInspector, ReadOnly] public float BodyRadius => BodyCollider.size.x;
        [ShowInInspector, ReadOnly] public Vector3 FocusPoint => _focusPoint.position;

        Collider ITrapPhysics.Collider => BodyCollider;

        private void OnValidate()
        {
            BodyCollider ??= GetComponentInChildren<BoxCollider>(true);
            Colliders ??= GetComponentsInChildren<Collider>(true);
            _focusPoint ??= GetComponentInChildren<Transform>(true);
        }

        protected override void OnEnabled() => SetCollidersEnabled(Colliders, true);
        protected override void OnDisabled() => SetCollidersEnabled(Colliders, false);

        private void SetCollidersEnabled(Collider[] colliders, bool enabled)
        {
            foreach (Collider collider in colliders)
                collider.enabled = enabled;
        }
    }
}