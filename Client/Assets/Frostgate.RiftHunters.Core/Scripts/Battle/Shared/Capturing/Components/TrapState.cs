using System;
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    public interface ITrapState : IDamageReceiverState
    {
        delegate void UnitAction(uint unitId);

        event Action Died;
        event Action Revived;
        event UnitAction UnitLost;
        event UnitAction UnitCaptured;
        event Action<ITrap> Initialized;

        ITrap Trap { get; }
        bool IsInitialized { get; }
        uint CapturedUnitId { get; set; }
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(TrapState))]
    public sealed class TrapState : NetworkBehaviour, ITrapState
    {
        private const string EventsFoldoutGroup = "Events";

        // TODO: Это нужно сделать SyncVar и задавать в фабрике
        [field: SerializeField] public TrapConfig Config { get; private set; }
        [field: SerializeField, Required] public Trap Trap { get; private set; }
        ITrap ITrapState.Trap => Trap;

        [SyncVar(hook = nameof(OnHealthChanged))] private float _health;
        [SyncVar(hook = nameof(OnCapturedUnitIdChanged))] private uint _capturedUnitId;

        [SerializeField, FoldoutGroup(EventsFoldoutGroup)] private UnityEvent _died;
        [SerializeField, FoldoutGroup(EventsFoldoutGroup)] private UnityEvent _revived;
        [SerializeField, FoldoutGroup(EventsFoldoutGroup)] private UnityEvent<float> _healthChanged;
        [SerializeField, FoldoutGroup(EventsFoldoutGroup)] private UnityEvent<uint> _unitLost;
        [SerializeField, FoldoutGroup(EventsFoldoutGroup)] private UnityEvent<uint> _unitCaptured;

        public event Action Died;
        public event Action Revived;
        public event Action<ITrap> Initialized;
        public event ITrapState.UnitAction UnitLost;
        public event ITrapState.UnitAction UnitCaptured;
        public event IReadOnlyHealth.Changed HealthChanged;

        public float MaxHealth => Config.Health;

        public float Health
        {
            get => _health;
            set => NetworkHelper.ModifySyncVar(_health, value, v => _health = v, OnHealthChanged);
        }

        public uint CapturedUnitId
        {
            get => _capturedUnitId;
            set => NetworkHelper.ModifySyncVar(_capturedUnitId, value, v => _capturedUnitId = v, OnCapturedUnitIdChanged);
        }

        public bool IsDead => Health <= 0;
        public float Defense => 0;
        public float BodyRadius => 0;
        public bool InDodge => false;
        public bool IsInvincible => false;
        public Vector3 Position => transform.position;
        public bool IsInitialized { get; private set; }

        public override void OnStartClient() => SetInitialized(true);
        public override void OnStartServer() => SetInitialized(true);
        public override void OnStopClient() => SetInitialized(false);
        public override void OnStopServer() => SetInitialized(false);
        private void SetInitialized(bool status)
        {
            if (IsInitialized != status)
            {
                IsInitialized = status;
                if (status) Initialized?.Invoke(Trap);
            }
        }

        private void OnHealthChanged(float oldValue, float newValue)
        {
            if (Mathf.Approximately(oldValue, newValue)) return;

            InvokeHealthChanged(newValue - oldValue);
            if (oldValue > 0 && newValue <= 0) InvokeDied();
            if (oldValue <= 0 && newValue > 0) InvokeRevived();
        }

        private void OnCapturedUnitIdChanged(uint oldValue, uint newValue)
        {
            if (oldValue == newValue) return;
            if (oldValue != default) InvokeUnitLost(oldValue);
            if (newValue != default) InvokeUnitCaptured(newValue);
            // Mirror не использует default-значения в качестве netId
        }

        private void InvokeHealthChanged(float diff)
        {
            HealthChanged?.Invoke(diff);
            _healthChanged?.Invoke(diff);
        }

        private void InvokeDied()
        {
            Died?.Invoke();
            _died?.Invoke();
        }

        private void InvokeRevived()
        {
            Revived?.Invoke();
            _revived?.Invoke();
        }

        private void InvokeUnitLost(uint unitId)
        {
            UnitLost?.Invoke(unitId);
            _unitLost?.Invoke(unitId);
        }

        private void InvokeUnitCaptured(uint unitId)
        {
            UnitCaptured?.Invoke(unitId);
            _unitCaptured?.Invoke(unitId);
        }
    }
}