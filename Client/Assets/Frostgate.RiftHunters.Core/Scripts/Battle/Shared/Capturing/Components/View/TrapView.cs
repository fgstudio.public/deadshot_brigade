using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    public interface ITrapView : IAimTargetView
    { }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(TrapView))]
    public sealed class TrapView : MonoBehaviour, ITrapView
    {
        [field: Header("References")]
        [field: SerializeField, Required, ChildGameObjectsOnly] public Transform FxContainer { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public Transform DamageFxTarget { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public Transform DamageReflectFxTarget { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public TrapAnimator Animator { get; private set; }

        public Transform WorldFxContainer => transform;
    }
}