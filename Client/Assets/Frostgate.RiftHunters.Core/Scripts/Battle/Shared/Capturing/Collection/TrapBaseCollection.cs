using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    /// <summary>
    /// КО: Коллекция ловушек.
    /// <see cref="https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80"/>
    /// </summary>
    [UsedImplicitly]
    public abstract class TrapBaseCollection<TKey> : ObjectBaseCollection<TKey, ITrap>
    {
        protected TrapBaseCollection([NotNull] ILogger logger) : base(logger) { }

        protected sealed override string ExtractName(ITrap trap) =>
            (trap.Identity != null ? trap.Identity.netId : default).ToString();
    }
}
