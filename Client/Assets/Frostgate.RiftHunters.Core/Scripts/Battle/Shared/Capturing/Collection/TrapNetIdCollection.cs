using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    /// <summary>
    /// КО: Коллекция ловушек.
    /// <see cref="https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80"/>
    /// </summary>
    [UsedImplicitly]
    public sealed class TrapNetIdCollection : TrapBaseCollection<uint>
    {
        public TrapNetIdCollection([NotNull] ILogger<TrapNetIdCollection> logger)
            : base(logger) { }
    }
}