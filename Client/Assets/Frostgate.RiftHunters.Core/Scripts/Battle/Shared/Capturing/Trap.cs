using Mirror;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Components;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    public interface ITrap : IImpactTarget, IAimTarget, IPoolObject, IAutoReleased, IDestroyable, IHpAutoLost, ISpawnable
    {
        ITrapPhysics Physics { get; }
        new ITrapView View { get; }
        new ITrapState State { get; }
    }

    [DisallowMultipleComponent]
    [RequireComponent(typeof(NetworkIdentity))]
    [HelpURL("https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(Trap))]
    public sealed class Trap : MonoBehaviour, ITrap
    {
        private const string EventsFoldoutGroup = "Events";
        [Required, ChildGameObjectsOnly] public NetworkIdentity Identity;
        [Required, ChildGameObjectsOnly] public TrapView View;
        [Required, ChildGameObjectsOnly] public TrapState State;
        [Required, ChildGameObjectsOnly] public TrapPhysics Physics;
        [Required, ChildGameObjectsOnly] public AutoDestroyComponent AutoDestroyComponent;
        [ChildGameObjectsOnly] public NetworkPlayerObjectComponent[] PlayerComponents;
        [Required] public DamageZone DamageZone;

        [field: SerializeField, FoldoutGroup(EventsFoldoutGroup)] public UnityEvent Destroyed { get; private set; }

        public EntityType Type => EntityType.Trap;
        public bool IsPriorityTarget => true;
        public bool CapturedByDirectAiming => false;
        public Collider AimTarget => Physics.BodyCollider;
        public ImpactTargetType ImpactTargetType => ImpactTargetType.Trap;

        IHealth IHpAutoLost.State => State;
        IHpAutoLossConfig IHpAutoLost.Config => State.Config.HpAutoLoss;
        GameObject IGameObject.GameObject => this != null? gameObject : null;
        Vector3 IPositioned.Position => transform != null ? transform.position : Vector3.zero;
        NetworkIdentity INetworkIdentifiable.Identity => Identity;
        ITrapView ITrap.View => View;
        ITrapState ITrap.State => State;
        ITrapPhysics ITrap.Physics => Physics;
        IDamageReceiverState IDamageReceiver.State => State;
        IAimTargetView IAimTarget.View => View;
        BattleUnitType IAimTarget.UnitType => BattleUnitType.Bot;
        bool IDamageReceiver.DamageImmunity => false;
        float IAimTarget.BodyRadius => Physics.BodyRadius;
        Vector3 IAimTarget.FocusPoint => Physics.FocusPoint;
        bool IAimTarget.IsDestroyedOrDead => this == null || State.IsDead;

        DamageZone IDamageReceiver.GetDamageZone(Vector3 hitPoint, float breakShieldChance) => DamageZone;
        DamageZone IDamageReceiver.GetDamageZone(float zoneMultiplier) => DamageZone;
        public void TryExecuteUnitAction(UnitActionTypes actionType, Vector3 casterPosition) { }
        public float GetDamageMultiplier(DamageType damageType) => 1;

        public void AddUnitEffect(UnitEffectState effect) {}
        public bool HasUnitEffect(UnitEffectState effect) => false;
        public void RemoveUnitEffect(UnitEffectState effect) {}
        public bool TryGetUnitEffect(UnitEffectConfig config, out UnitEffectState state)
        {
            state = null;
            return false;
        }

        public bool CanReceiveUnitEffect(UnitEffectConfig config) => false;
        public bool IsFriendly(UnitNetwork sourceUnit) => false;

        private UnityAction<IAutoReleased> _readyForReleaseAction;
        event UnityAction<IAutoReleased> IAutoReleased.ReadyForRelease
        {
            add => _readyForReleaseAction += value;
            remove => _readyForReleaseAction -= value;
        }

        void IPoolObject.OnGet() => InitComponents();
        void IPoolObject.OnRelease() => View.Animator.Reset();

        private void OnValidate() => Init();
        private void Awake() => Init();
        private void Start()
        {
            InitState();
            Subscribe();
        }
        private void OnDestroy()
        {
            Unsubscribe();
            Destroyed?.Invoke();
        }

        private void Init()
        {
            InitReferences();
            InitComponents();
        }

        private void InitReferences()
        {
            Identity ??= GetComponentInChildren<NetworkIdentity>();
            State ??= GetComponentInChildren<TrapState>();
            View ??= GetComponentInChildren<TrapView>();
            Physics ??= GetComponentInChildren<TrapPhysics>();
            AutoDestroyComponent ??= GetComponentInChildren<AutoDestroyComponent>();
            PlayerComponents ??= GetComponentsInChildren<NetworkPlayerObjectComponent>(true);
        }

        private void InitComponents()
        {
            Physics.Enable();
            AutoDestroyComponent.Disable();
        }

        private void InitState()
        {
            if (State.IsDead) OnDied();
            else OnRevived();

            UpdatePlayerComponents();
        }

        private void Subscribe()
        {
            State.Died += OnDied;
            State.Revived += OnRevived;
            State.UnitLost += OnCapturedUnitChanged;
            State.UnitCaptured += OnCapturedUnitChanged;
            AutoDestroyComponent.DestroyComponent.Destroyed.AddListener(Destroy);
        }

        private void Unsubscribe()
        {
            State.Died -= OnDied;
            State.Revived -= OnRevived;
            State.UnitLost -= OnCapturedUnitChanged;
            State.UnitCaptured -= OnCapturedUnitChanged;
            AutoDestroyComponent.DestroyComponent.Destroyed.RemoveListener(Destroy);
        }

        private void OnDied()
        {
            Physics.Disable();
            AutoDestroyComponent.Enable();
            View.Animator.PlayDeactivating();
        }

        private void OnRevived()
        {
            InitComponents();
            View.Animator.PlayActivating();
        }

        private void OnCapturedUnitChanged(uint _) =>
            UpdatePlayerComponents();

        private void UpdatePlayerComponents()
        {
            foreach (NetworkPlayerObjectComponent component in PlayerComponents)
                component.NetId = State.CapturedUnitId;
        }

        [ContextMenu(nameof(Destroy))] public void Destroy() => InvokeReadyForRelease();
        private void InvokeReadyForRelease() => _readyForReleaseAction?.Invoke(this);
    }
}