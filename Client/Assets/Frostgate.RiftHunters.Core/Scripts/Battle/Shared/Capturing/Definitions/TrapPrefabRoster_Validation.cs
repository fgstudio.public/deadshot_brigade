using System.Linq;
using Frostgate.RiftHunters.Core.Utils;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    public sealed partial class TrapPrefabRoster
    {
        private const string EmptyError = "Is empty";
        private const string NullPrefabsError = "Has null prefabs";
        private const string DuplicateTypesError = "Has duplicate types";

        private bool HasElements() => _prefabs.Length > 0;
        private bool ArePrefabsNotNull() => !Validator.HasNullElements(_prefabs);
        private bool AreAllTypesUnique() => !Validator.HasDuplicateElements(
            _prefabs.Where(p => p != null).Select(p => p.Identity.assetId));
    }
}
