using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    public interface ITrapPrefabRoster
    {
        IEnumerable<ITrap> Prefabs { get; }
        ITrap FindPrefab(Guid guid);
    }

    /// <summary>
    /// Ростер префабов для ловушек.
    /// <see cref="https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80"/>
    /// </summary>
    [CreateAssetMenu(
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(TrapPrefabRoster),
        fileName = nameof(TrapPrefabRoster))]
    [HelpURL("https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80")]
    public sealed partial class TrapPrefabRoster : ScriptableObject, ITrapPrefabRoster
    {
        [ValidateInput(nameof(HasElements), EmptyError)]
        [ValidateInput(nameof(ArePrefabsNotNull), NullPrefabsError)]
        [ValidateInput(nameof(AreAllTypesUnique), DuplicateTypesError)]
        [SerializeField] private Trap[] _prefabs;

        IEnumerable<ITrap> ITrapPrefabRoster.Prefabs => Prefabs;
        public IEnumerable<Trap> Prefabs => _prefabs;

        ITrap ITrapPrefabRoster.FindPrefab(Guid guid) => FindPrefab(guid);
        public Trap FindPrefab(Guid guid) => _prefabs.FirstOrDefault(l => l.Identity.assetId == guid);
    }
}