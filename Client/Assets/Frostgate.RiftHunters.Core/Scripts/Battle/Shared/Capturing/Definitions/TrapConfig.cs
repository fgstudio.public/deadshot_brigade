using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Capturing
{
    [HelpURL("https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80")]
    [CreateAssetMenu(fileName = nameof(TrapConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(TrapConfig))]
    public sealed class TrapConfig : ScriptableObject
    {
        [field: SerializeField, MinValue(0), SuffixLabel("hp", true)] public float Health { get; private set; }
        [field: SerializeField, HideLabel, BoxGroup(nameof(HpAutoLoss))] public HpAutoLossConfig HpAutoLoss { get; private set; }
    }
}