﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints
{
    public class ActualRespawnPoint : IActualRespawnPoint
    {
        public Checkpoint GetRespawnPoint => _respawnPoint;
        public bool IsInitialized => _respawnPoint != null;

        private Checkpoint _respawnPoint;

        public void SetRespawnPoint(Checkpoint respawnPoint)
        {
            _respawnPoint = respawnPoint;
        }
    }
}