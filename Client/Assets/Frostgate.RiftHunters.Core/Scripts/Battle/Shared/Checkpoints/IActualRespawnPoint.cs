﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints
{
    public interface IReadOnlyActualRespawnPoint
    {
        Checkpoint GetRespawnPoint { get; }
        bool IsInitialized { get; }
    }

    public interface IActualRespawnPoint : IReadOnlyActualRespawnPoint
    {
        void SetRespawnPoint(Checkpoint respawnPoint);
    }
}