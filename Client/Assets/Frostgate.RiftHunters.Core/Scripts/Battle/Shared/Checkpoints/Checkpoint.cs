using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(Checkpoint))]
    public sealed class Checkpoint : MonoBehaviour
    {
        [InfoBox("Коллайдер определяет зону для триггера чекпоинта и зону для респауна персонажей")]
        [InfoBox("Поворот объекта определяет поворот персонажа при респауне в зоне чекпоинта")]

        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Collider _collider;
        [SerializeField, Required, ChildGameObjectsOnly] private ObjectCatcher _catcher;

        public Bounds Bounds => _collider.bounds;

        private void OnValidate()
        {
            _collider ??= GetComponentInChildren<Collider>();
            _catcher ??= GetComponentInChildren<ObjectCatcher>();
        }
    }
}