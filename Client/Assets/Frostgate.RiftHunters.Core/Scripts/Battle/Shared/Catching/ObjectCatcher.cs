using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Filters;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Catching
{
    [RequireComponent(typeof(Collider))]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(ObjectCatcher))]
    public sealed class ObjectCatcher : MonoBehaviour
    {
        private const string EventsFoldoutName = "Events";

        [field: FoldoutGroup(EventsFoldoutName), SerializeField] public UnityEvent<Collider> Caught { get; private set; }
        [field: FoldoutGroup(EventsFoldoutName), SerializeField] public UnityEvent<Collider> Lost { get; private set; }

        [Header("Settings")]
        [SerializeField] private int _filteringParentIndex;

        [Header("Info")]
        [ShowInInspector, ReadOnly] private readonly List<Collider> _objects = new();
        [ShowInInspector, ReadOnly] private IFilter[] _filters;

        public IReadOnlyCollection<Collider> Objects => _objects;

        private void OnValidate() => InitFilters();
        private void Awake() => InitFilters();
        private void OnDestroy() => LoseAllObjects();
        private void Update() => LoseNullAndNonActiveObjects();

        private void OnTriggerEnter(Collider collider)
        {
            GameObject go = collider.gameObject;

            if (IsFiltered(go) && !IsCaught(collider))
                Catch(collider);
        }

        private void OnTriggerExit(Collider collider)
        {
            if (IsCaught(collider))
                Lose(collider);
        }

        [Button]
        public void LoseAllObjects()
        {
            for (int i = _objects.Count - 1; i >= 0; i--)
                Lose(_objects[i]);

            _objects.Clear();
        }

        private void InitFilters() =>
            _filters = GetComponents<IFilter>();

        private void LoseNullAndNonActiveObjects()
        {
            for (int i = _objects.Count - 1; i >= 0; i--)
            {
                if (_objects[i] == null)
                    _objects.RemoveAt(i);

                else if (!_objects[i].gameObject.activeInHierarchy)
                    Lose(_objects[i]);
            }
        }

        private bool IsFiltered([NotNull] GameObject gameObject)
        {
            GameObject filteringObject = gameObject.transform
                .GetRecursiveParent(_filteringParentIndex)?.gameObject;

            if (filteringObject == null)
                return false;

            for (int i = 0; i < _filters.Length; i++)
            {
                bool isMatching = _filters[i].IsMatching(filteringObject);
                if (!isMatching) return false;
            }

            return true;
        }

        private bool IsCaught([NotNull] Collider collider) =>
            _objects.Contains(collider);

        private void Catch([CanBeNull] Collider collider)
        {
            if (collider == null) return;
            _objects.Add(collider);
            Caught?.Invoke(collider);
        }

        private void Lose([CanBeNull] Collider collider)
        {
            if (collider == null) return;
            _objects.Remove(collider);
            Lost?.Invoke(collider);
        }
    }
}