using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Helpers
{
    public static class PhysicHelper
    {
        public static RaycastHit[] HitResults { get; } = new RaycastHit[256];
    }
}