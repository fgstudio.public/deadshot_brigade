namespace Frostgate.RiftHunters.Core.Battle.Shared.Helpers
{
    public static class PhysicsLayers
    {
        public const int WallIndex = 0;
        public const int WallMask = 1 << WallIndex;

        public const int CameraBlocker = 17;

        public const int UnitIndex = 9;
        public const int UnitMask = 1 << UnitIndex;

        public const int UnitBodyPartIndex = 20;
        public const int UnitBodyPartMask = 1 << UnitBodyPartIndex;
        public const int UnitBodyPartAndWallMask = UnitBodyPartMask | WallMask | MovableObstacleMask;

        public const int PassableAimTargetIndex = 21;
        public const int PassableAimTargetMask = 1 << PassableAimTargetIndex;

        public const int ImpassableAimTargetIndex = 22;
        public const int ImpassableAimTargetMask = 1 << ImpassableAimTargetIndex;

        public const int AimTargetMask = PassableAimTargetMask | ImpassableAimTargetMask;
        public const int AimTargetAndWallMask = AimTargetMask | WallMask | MovableObstacleMask;

        public const int SpawnBlockZoneIndex = 19;
        public const int SpawnBlockZoneMask = 1 << SpawnBlockZoneIndex;

        public const int MovableObstacleIndex = 25;
        public const int MovableObstacleMask = 1 << MovableObstacleIndex;
    }
}