using System.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Helpers
{
    public static class PositionHelper
    {
        private const float PointAccuracy = 0.2f;
        private const float SampleAccuracy = 2;
        private const int NavMeshAreas = NavMesh.AllAreas;
        private const int PhysicsLayers = Helpers.PhysicsLayers.SpawnBlockZoneMask;

        private static readonly NavMeshPath pathCache = new();
        private static readonly Collider[] colliderBuffer = new Collider[1];

        private static readonly ILogger logger;

        static PositionHelper() =>
            logger = LoggerFactory.CreateLogger(nameof(PositionHelper));

        public static bool IsValidToSpawn(Vector3 position, EntityType entityType = EntityType.Other,
            float objectRadius = PointAccuracy)
        {
            if (!NavMesh.SamplePosition(position, out _, objectRadius, NavMeshAreas))
                return false;

            int blockZonesCount = Physics.OverlapSphereNonAlloc(position, PointAccuracy, colliderBuffer, PhysicsLayers);
            if (blockZonesCount == 0)
                return true;

            return colliderBuffer.Take(blockZonesCount)
                .All(c => c.TryGetComponent(out SpawnBlockZone blockZone) && !blockZone.IsBlocked(entityType));
        }

        public static bool TryCorrectSpawnPosition(Vector3 position, out Vector3 correctedPosition,
            float objectRadius = SampleAccuracy)
        {
            bool success = NavMesh.SamplePosition(position, out NavMeshHit hit, objectRadius, NavMeshAreas);
            correctedPosition = success ? hit.position : position;

            if (!success)
                logger.LogInfo("Can't sample position");

            return success;
        }

        public static bool IsPointReachable(Vector3 point, Vector3 reachableFrom) =>
            NavMesh.CalculatePath(reachableFrom, point, NavMesh.AllAreas, pathCache);
    }
}