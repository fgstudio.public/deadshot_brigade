using System;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Filters
{
    [Serializable]
    public class UnitFilterPropertyParam
    {
        public enum PropertyType { IsDead, IsCaptured, IsDodging }
        [Flags] public enum ConditionType { True = 1, False = 2 }

        [HorizontalGroup, HideLabel] public PropertyType Property;
        [HorizontalGroup, HideLabel] public ConditionType Condition;

        public UnitFilterPropertyParam Copy() =>
            new()
            {
                Property = Property,
                Condition = Condition
            };

        public bool IsMatching([NotNull] Object obj)
        {
            bool propertyValue = obj is UnitNetwork unit && ExtractPropertyValue(unit);
            ConditionType conditionType = DetectConditionType(propertyValue);

            return Condition.HasFlag(conditionType);
        }

        private bool ExtractPropertyValue([NotNull] UnitNetwork unit) =>
            Property switch
            {
                PropertyType.IsDead => unit.UnitNetworkState.IsDead,
                PropertyType.IsCaptured => unit.UnitNetworkState.IsCaptured,
                PropertyType.IsDodging => unit.UnitActionState.CurrentActionType == UnitActionTypes.Dodge,
                _ => throw new ArgumentOutOfRangeException(nameof(PropertyType))
            };

        private ConditionType DetectConditionType(bool value) =>
            value switch
            {
                true => ConditionType.True,
                false => ConditionType.False
            };
    }
}
