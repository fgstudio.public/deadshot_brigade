using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Filters
{
    public interface IFilter
    {
        public bool IsMatching([CanBeNull] GameObject gameObject);
    }
}
