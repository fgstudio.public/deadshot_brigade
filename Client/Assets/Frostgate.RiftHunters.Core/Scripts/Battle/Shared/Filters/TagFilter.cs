using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Filters
{
    [AddComponentMenu(BattleComponentMenus.Components.Filters.Menu + "/" + nameof(TagFilter))]
    public sealed class TagFilter : MonoBehaviour, IFilter
    {
        [SerializeField, TagSelector] private string _targetTag;

        public bool IsMatching(GameObject gameObject) =>
            gameObject != null && gameObject.CompareTag(_targetTag);
    }
}