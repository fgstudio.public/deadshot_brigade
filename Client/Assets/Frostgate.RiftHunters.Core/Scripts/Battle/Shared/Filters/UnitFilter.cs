using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Linq;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Filters
{
    [HelpURL("https://www.notion.so/frostgate/aa61b0f5d1a045a7aaa8dc875bb468c3")]
    [AddComponentMenu(BattleComponentMenus.Components.Filters.Menu + "/" + nameof(UnitFilter))]
    public sealed class UnitFilter : MonoBehaviour, IFilter
    {
        [Title(nameof(FilterParams))]
        [HideLabel] public UnitFilterParams FilterParams = new();

        public IEnumerable<Object> Filter(IEnumerable<Object> objs) => objs.Where(IsMatching);
        public bool IsMatching([NotNull] Object obj) => FilterParams.IsMatching(obj);

        bool IFilter.IsMatching(GameObject gameObject) =>
            gameObject != null && IsMatching(gameObject);
    }
}