using System;
using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Object = System.Object;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Filters
{
    [Serializable]
    public sealed class UnitFilterParams
    {
        [Flags] public enum UnitType { Player = 1, Bot = 2 }
        [Flags] public enum PlayerType { LocalPlayer = 1, NonLocalPlayer = 2 }

        public UnitType UnitTypeSelector;
        [ShowIf(nameof(IsPlayerTypeActual))] public PlayerType PlayerTypeSelector;

        public UnitFilterPropertyParam[] PropertiesSelector;

        private bool IsPlayerTypeActual => UnitTypeSelector.HasFlag(UnitType.Player);

        public void CopyParams(UnitFilterParams filterParams)
        {
            UnitTypeSelector = filterParams.UnitTypeSelector;
            PlayerTypeSelector = filterParams.PlayerTypeSelector;
            PropertiesSelector = filterParams.PropertiesSelector.Select(s => s.Copy()).ToArray();
        }

        public bool IsMatching([NotNull] Object obj)
        {
            UnitNetwork unit = ExtractUnitNetwork(obj);
            UnitType unitType = unit ? DetectUnitType(unit) : UnitType.Bot;
            PlayerType playerType = unit ? DetectPlayerType(unit) : PlayerType.NonLocalPlayer;

            if (!UnitTypeSelector.HasFlag(unitType)) return false;
            if (IsPlayerTypeActual && !PlayerTypeSelector.HasFlag(playerType)) return false;
            if (!PropertiesSelector.All(p => p.IsMatching(obj))) return false;

            return true;
        }

        [CanBeNull]
        private UnitNetwork ExtractUnitNetwork([NotNull] Object obj) =>
            obj switch
            {
                UnitNetwork unit => unit,
                GameObject go => go.GetComponent<UnitNetwork>(),
                UnityEngine.Component comp => comp.GetComponent<UnitNetwork>(),
                _ => null
            };

        private UnitType DetectUnitType([NotNull] UnitNetwork unit) =>
            unit.UnitNetworkState.UnitType switch
            {
                BattleUnitType.Player => UnitType.Player,
                BattleUnitType.Bot => UnitType.Bot,
                _ => throw new ArgumentOutOfRangeException($"{GetType().Name} can't handle {nameof(BattleUnitType)}.{unit.UnitNetworkState.UnitType}")
            };

        private PlayerType DetectPlayerType([NotNull] UnitNetwork unit) =>
            // TODO: при игре с хостом все клиенты считаются LocalPlayer, что приводит к неверной работе
            unit.Identity.isLocalPlayer ? PlayerType.LocalPlayer : PlayerType.NonLocalPlayer;
    }
}
