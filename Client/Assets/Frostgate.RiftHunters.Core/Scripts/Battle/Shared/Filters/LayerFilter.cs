using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Filters
{
    [AddComponentMenu(BattleComponentMenus.Components.Filters.Menu + "/" + nameof(LayerFilter))]
    public sealed class LayerFilter : MonoBehaviour, IFilter
    {
        [SerializeField] private LayerMask _layerMask;

        public bool IsMatching(GameObject gameObject) =>
            gameObject != null && _layerMask.HasLayer(gameObject.layer);
    }
}