using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Looting;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// Компонент юнита для подбора лут-бокса.
    /// <see cref="UnitNetwork"/>
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    [RequireComponent(typeof(ObjectCatcher))]
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(UnitCollectLootBox))]
    public sealed class UnitCollectLootBox : MonoBehaviour, IEnabling
    {
        [Header("References")]
        [SerializeField, Required, CanBeNull] private UnitNetwork _unit;
        [SerializeField, Required, ChildGameObjectsOnly] private ObjectCatcher _catcher;

        [Header("Info")]
        [ShowInInspector] private ICollectLootBoxService _collectLootBoxService;

        public bool IsEnabled => gameObject.activeInHierarchy;

        [Inject]
        private void MonoConstructor(IServiceRoster serviceRoster) =>
            _collectLootBoxService = serviceRoster.CollectLootBoxService;

        private void Awake() => InitReferences();
        private void Start() => Subscribe(_catcher);
        private void OnDestroy() => Unsubscribe(_catcher);
        private void OnDisable() => _catcher.LoseAllObjects();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _unit ??= this.GetComponentInHierarchy<UnitNetwork>();
            _catcher ??= GetComponentInChildren<ObjectCatcher>();
        }

        public void Enable() => gameObject.SetActive(true);
        public void Disable() => gameObject.SetActive(false);

        private void Subscribe([NotNull] ObjectCatcher catcher) => catcher.Caught.AddListener(OnCollected);
        private void Unsubscribe([NotNull] ObjectCatcher catcher) => catcher.Caught.RemoveListener(OnCollected);

        private void OnCollected(Collider collected)
        {
            if (_unit == null) return;
            if (!_unit.Identity.isServer) return;
            if (_unit.UnitNetworkState.IsDead) return;
            if (_unit.UnitNetworkState.UnitType != BattleUnitType.Player) return;
            // TODO: получать из коллекции
            if (!collected.TryGetComponentInHierarchy(out LootBox lootBox)) return;
            if (lootBox.P2PObject.TargetId != _unit.Identity.netId) return;

            if (!lootBox.State.IsCollected) _collectLootBoxService?.Collect(_unit, lootBox);
        }
    }
}