using Mirror;
using UnityEngine;
using System.Linq;
using System.Collections;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Energy;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Random = UnityEngine.Random;

namespace Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget
{
    public class BarrelNetwork : ShootingTargetNetwork, IEnergyCapsuleSource
    {
        [Header("Barrel Configuration")]
        [SerializeField] private float _explosionRadius;
        [SerializeField] private Vector2 _deathDelay;
        [SerializeField] private float _selfDamagePerSecond;
        [SerializeField] private BarrelDamageParams[] _damageParams;
        [SerializeField] private EnergyCapsuleConfig _energyCapsuleConfig;

        public override ImpactTargetType ImpactTargetType => ImpactTargetType.Barrel;
        public Transform Transform => transform;
        public EnergyCapsuleConfig EnergyCapsuleForDeath => _energyCapsuleConfig;

        private IDamageCaster LastDamageCaster =>
            SharedDependencies != null && SharedDependencies.UnitCollections.Get<uint>()
                .TryGet(_lastDamageCasterId, out UnitNetwork lastDamageCasterUnit)
                ? lastDamageCasterUnit : this;

        private uint _lastDamageCasterId;
        private Coroutine _fireDamageCoroutine;

        protected override void OnConstructed() =>
            SharedDependencies?.ServiceRoster.DamageService.Damaged.AddListener(OnDamaged);

        protected override void OnDestroyed() =>
            SharedDependencies?.ServiceRoster.DamageService.Damaged.RemoveListener(OnDamaged);

        [Server]
        protected override void ServerDeath()
        {
            StopAllCoroutines();
            _fireDamageCoroutine = null;

            Invoke(nameof(DelayedDeath), Random.Range(_deathDelay.x, _deathDelay.y));
        }

        private void DelayedDeath()
        {
            if (SharedDependencies == null)
                return;

            if (isServerOnly)
                Death();

            RpcDeath();

            IDamageService damageService = SharedDependencies.ServiceRoster.DamageService;

            int count = Physics.SphereCastNonAlloc(
                transform.position, _explosionRadius, Vector3.up, PhysicHelper.HitResults);

            for (int i = 0; i < count && i < PhysicHelper.HitResults.Length; i++)
            {
                var hit = PhysicHelper.HitResults[i];
                if (!SharedDependencies.AimTargetColliderCollection.TryGet(hit.collider, out var target))
                    continue;

                if (target.IsNullDestroyedOrDead())
                    continue;

                if (ReferenceEquals(this, target))
                    continue;

                BarrelDamageSource damageSource = _damageParams
                    .FirstOrDefault(p => p.FilterParams.IsMatching(target))
                    ?.DamageSource;

                if (damageSource != default)
                    damageService.TakeDamage(
                        damageSource, LastDamageCaster, target, target.FocusPoint);
            }
        }

        private void OnDamaged(DamageEventData data)
        {
            if (data.ReceiverNetId != netId) return;

            // Запоминаем ссылку на моно компонент вместо интерфейса, чтобы на случай дестроя не словить эксепшенов.
            _lastDamageCasterId = data.CasterNetId;

            bool isRoutineLaunched = _fireDamageCoroutine != null;
            bool hasSelfDamage = Mathf.Approximately(_selfDamagePerSecond, 0);

            if (isServer && !isRoutineLaunched && hasSelfDamage)
            {
                var dto = new DamageSourceDTO(_selfDamagePerSecond, DamageType.Ability);
                _fireDamageCoroutine = StartCoroutine(FireDamage(dto));
            }
        }

        [Server]
        private IEnumerator FireDamage(DamageSourceDTO damageSourceDto)
        {
            while (!IsDead)
            {
                yield return new WaitForSeconds(1);

                SharedDependencies?.ServiceRoster
                    .DamageService.TakeDamage(damageSourceDto, LastDamageCaster, this, FocusPoint);
            }
        }
    }
}