using System;
using Mirror;
using Zenject;
using UnityEngine;
using UnityEngine.AI;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Ai;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Core.Battle.Client.ShootingTarget;

namespace Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget
{
    public abstract class ShootingTargetNetwork : NetworkBehaviour,
        IImpactTarget, IDamageCaster, IAimTarget, IDamageReceiverState, IDamageCasterState, IAiTarget
    {
        public event Action<ShootingTargetNetwork> OnDeath;
        public event IReadOnlyHealth.Changed HealthChanged;
        public event Action<ShootingTargetNetwork> OnRespawn;

        public NetworkIdentity Identity => netIdentity;
        public float AttackMultiplier => 1;
        IDamageCasterState IDamageCaster.State => this;
        IDamageReceiverState IDamageReceiver.State => this;
        public bool PiercingBullets => false;
        public bool IgnoreEnemyDamageZone => false;
        CapsuleCollider IAiTarget.AimTarget => null;
        public IAimTargetView View => _view;
        public BattleUnitType UnitType => BattleUnitType.Bot;
        public bool DamageImmunity => false;
        public float Defense => 0;
        public float MaxHealth => _health;
        public float BodyRadius => _bodyRadius;
        public bool IsDead => _currentHealth <= 0;
        public bool IsInvincible => false;
        public Vector3 Position => transform.position;
        public bool IsDestroyedOrDead => transform == null || IsDead;
        public Vector3 FocusPoint => _focusPoint.position;
        public bool IsPriorityTarget => false;
        public bool CapturedByDirectAiming => true;
        public abstract ImpactTargetType ImpactTargetType { get; }

        [SyncVar(hook = nameof(OnHealthVarChanged))]
        private float _currentHealth;

        #region Health
        private void OnHealthVarChanged(float oldValue, float newValue)
        {
            HealthChanged?.Invoke(newValue - oldValue);

            if (IsDead && isServer)
                ServerDeath();
        }

        public float Health
        {
            get => _currentHealth;
            set
            {
                if (IsDead)
                    return;

                var prevValue = _currentHealth;
                _currentHealth = value;

                if (isServerOnly)
                    OnHealthVarChanged(prevValue, _currentHealth);
            }
        }
        #endregion

        [Header("References")]
        [SerializeField] private ShootingTargetView _viewPrefab;
        [SerializeField] private NavMeshObstacle _navMeshObstacle;
        [SerializeField] private Collider _collider;
        [SerializeField] private Transform _focusPoint;

        [Header("Configuration")]
        [SerializeField] private float _health;
        [SerializeField] private float _bodyRadius = 0.3f;
        [SerializeField] private DamageZone _damageZone;

        [CanBeNull] protected SharedDependencies SharedDependencies;
        [CanBeNull] private ShootingTargetView _view;

        [Inject]
        private void MonoConstruct(SharedDependencies sharedDependencies)
        {
            SharedDependencies = sharedDependencies;
            OnConstructed();
        }
        protected virtual void OnConstructed() {}

        public override void OnStartServer()
        {
            _currentHealth = _health;
            SharedInit();
        }

        //todo: создание объектов лучше перенести в отдельную сущность
        public override void OnStartClient()
        {
            _view = SharedDependencies?.PrefabFactory
                .Instantiate<ShootingTargetView>(_viewPrefab, transform);

            _view?.Init(this, SharedDependencies);

            if (IsDead)
                DisablePhysic();

            if (isClientOnly)
                SharedInit();
        }

        private void SharedInit()
        {
            SharedDependencies?.ShootingTargetCollections.Get<uint>().Add(Identity.netId, this);
            SharedDependencies?.ShootingTargetCollections.Get<Collider>().Add(AimTarget, this);
        }

        private void OnDestroy()
        {
            SharedDependencies?.ShootingTargetCollections.Get<uint>().Remove(Identity.netId);
            SharedDependencies?.ShootingTargetCollections.Get<Collider>().Remove(AimTarget);
            OnDestroyed();
        }
        protected virtual void OnDestroyed() { }

        public virtual DamageZone GetDamageZone(Vector3 hitPoint, float breakShieldChance) => _damageZone;

        public virtual DamageZone GetDamageZone(float zoneMultiplier) => _damageZone;

        public void TryExecuteUnitAction(UnitActionTypes actionType, Vector3 casterPosition)
        {
        }

        public float GetDamageMultiplier(DamageType damageType) => 1;

        public Collider AimTarget => _collider;
        GameObject IGameObject.GameObject => this != null? gameObject : null;

        public void AddUnitEffect(UnitEffectState effect) {}
        public bool HasUnitEffect(UnitEffectState effect) => false;
        public void RemoveUnitEffect(UnitEffectState effect) {}
        public bool TryGetUnitEffect(UnitEffectConfig config, out UnitEffectState state)
        {
            state = null;
            return false;
        }

        public bool CanReceiveUnitEffect(UnitEffectConfig config) => false;

        public bool IsFriendly(UnitNetwork sourceUnit) => false;

        [Server]
        public void ServerRespawn()
        {
            _currentHealth = _health;

            if (isServerOnly)
                Respawn();

            RpcRespawn();
        }

        [ClientRpc]
        private void RpcRespawn() => Respawn();

        private void Respawn()
        {
            EnablePhysic();
            OnRespawn?.Invoke(this);
        }

        [Server]
        protected virtual void ServerDeath()
        {
            if (isServerOnly)
                Death();

            RpcDeath();
        }

        [ClientRpc]
        protected void RpcDeath() => Death();

        protected void Death()
        {
            DisablePhysic();
            OnDeath?.Invoke(this);
        }

        private void DisablePhysic()
        {
            _collider.enabled = false;
            _navMeshObstacle.enabled = false;
        }

        private void EnablePhysic()
        {
            _collider.enabled = true;
            _navMeshObstacle.enabled = true;
        }
    }
}