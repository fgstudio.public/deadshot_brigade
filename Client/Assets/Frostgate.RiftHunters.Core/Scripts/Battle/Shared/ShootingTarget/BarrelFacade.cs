﻿using System;
using System.Collections.Generic;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget
{
    public class BarrelFacade : IDisposable
    {
        private readonly IObjectCollection<uint, ShootingTargetNetwork> _sharedCollection;

        private readonly HashSet<ShootingTargetNetwork> _allBarrels = new();
        private readonly HashSet<ShootingTargetNetwork> _destroyedBarrels = new();

        public BarrelFacade(IObjectCollection<uint, ShootingTargetNetwork> sharedCollection)
        {
            _sharedCollection = sharedCollection;
            _sharedCollection.Added += OnAdd;
            foreach (var targetNetwork in _sharedCollection.Values)
            {
                OnAdd(targetNetwork);
            }
        }

        public void Dispose()
        {
            foreach (var barrel in _allBarrels)
            {
                barrel.OnDeath -= OnDeathBarrel;
            }

            _sharedCollection.Added -= OnAdd;
            _destroyedBarrels.Clear();
            _allBarrels.Clear();
        }

        public void ResetBarrels()
        {
            RespawnDestroyedBarrels();
            RestoreHealthBarrels();
        }

        private void RespawnDestroyedBarrels()
        {
            foreach (var barrel in _destroyedBarrels)
            {
                barrel.ServerRespawn();
            }
            _destroyedBarrels.Clear();
        }

        private void RestoreHealthBarrels()
        {
            foreach (var barrel in _allBarrels)
            {
                barrel.Health = barrel.MaxHealth;
            }
        }

        private void OnAdd(ShootingTargetNetwork barrel)
        {
            if (barrel is BarrelNetwork)
            {
                _allBarrels.Add(barrel);
                barrel.OnDeath += OnDeathBarrel;
            }
        }

        private void OnDeathBarrel(ShootingTargetNetwork targetNetwork)
        {
            _destroyedBarrels.Add(targetNetwork);
        }
    }
}