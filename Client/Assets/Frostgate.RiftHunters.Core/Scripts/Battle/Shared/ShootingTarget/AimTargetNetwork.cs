using Mirror;
using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

namespace Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget
{
    public class AimTargetNetwork : ShootingTargetNetwork
    {
        [SerializeField] private float _respawnInterval = 20;

        public override ImpactTargetType ImpactTargetType => ImpactTargetType.Unit;

        [Server]
        protected override void ServerDeath()
        {
            base.ServerDeath();

            Invoke(nameof(ServerRespawn), _respawnInterval);
        }
    }
}