using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget
{
    [Serializable]
    public sealed class BarrelDamageSource : IDamageSource
    {
        [SerializeField] private DamageType _damageType;
        [SerializeField, Min(0)] private float _damage;
        [SerializeField, Min(0)] private float _distanceMultiplier;
        [SerializeField, Min(0)] private float _breakShieldChance;
        [SerializeField, Min(0)] private float _criticalDamageChance;
        [SerializeField, Min(0)] private float _criticalDamageMultiplier;
        [SerializeField, Required] private UnitActionDamageTrigger[] _triggers;

        public DamageType Type => _damageType;
        public float BreakShieldChance => _breakShieldChance;
        public float CriticalDamageChance => _criticalDamageChance;
        public float CriticalDamageMultiplier => _criticalDamageMultiplier;
        public IReadOnlyList<UnitActionDamageTrigger> UnitActionDamageTriggers => _triggers;

        public float GetDamage(float distance = 0) => _damage;
        public float GetDistanceMultiplier(float distance = 0) => _distanceMultiplier;
    }
}