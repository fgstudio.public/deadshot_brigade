using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget
{
    [UsedImplicitly]
    public sealed class ShootingTargetNetworkCollection : ShootingTargetBaseCollection<uint>
    {
        public ShootingTargetNetworkCollection([NotNull] ILogger<ShootingTargetNetworkCollection> logger)
            : base(logger) { }
    }
}