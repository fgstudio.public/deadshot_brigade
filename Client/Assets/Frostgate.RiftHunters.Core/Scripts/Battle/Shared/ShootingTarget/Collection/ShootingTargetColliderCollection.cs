using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget
{
    [UsedImplicitly]
    public class ShootingTargetColliderCollection : ShootingTargetBaseCollection<Collider>
    {
        public ShootingTargetColliderCollection([NotNull] ILogger<ShootingTargetColliderCollection> logger)
            : base(logger) { }
    }
}