using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget
{
    public abstract class ShootingTargetBaseCollection<TKey> : ObjectBaseCollection<TKey, ShootingTargetNetwork>
    {
        protected ShootingTargetBaseCollection([NotNull] ILogger logger) : base(logger) { }

        protected sealed override string ExtractName(ShootingTargetNetwork target) =>
            (target.Identity != null ? target.Identity.netId : default).ToString();
    }
}
