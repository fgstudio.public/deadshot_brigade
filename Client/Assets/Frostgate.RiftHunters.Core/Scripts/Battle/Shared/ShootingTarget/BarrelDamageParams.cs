using System;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Filters;

namespace Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget
{
    [Serializable]
    public sealed class BarrelDamageParams
    {
        [HideLabel]
        [SerializeField] private UnitFilterParams _filterParams;

        [HideLabel]
        [SerializeField] private BarrelDamageSource _damageSource;

        public UnitFilterParams FilterParams => _filterParams;
        public BarrelDamageSource DamageSource => _damageSource;
    }
}
