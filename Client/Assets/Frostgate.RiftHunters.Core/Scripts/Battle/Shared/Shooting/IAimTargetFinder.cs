using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Shooting
{
    public interface IAimTargetFinder
    {
        bool TryGetAimTarget(IShooter shooter, IEnumerable<IAimTarget> possibleTargets, out IAimTarget aimTarget);

        IEnumerable<IAimTarget> GetAimTargets(IShooter shooter, IEnumerable<IAimTarget> possibleTargets);

        RaycastHitResult TryShootRaycast([NotNull] IShooter shooter, Vector3 direction, Action<AttackCastResult> onHit = null, Action<RaycastHit> onWall = null);

        RaycastHitResult TryShootRaycast(IShooter shooter, Vector3 origin, Vector3 direction, float distance, Action<AttackCastResult> onHit = null, Action<RaycastHit> onWall = null);

        /// <summary>
        /// Кастуем милишную атаку
        /// </summary>
        /// <param name="shooter"></param>
        /// <param name="result">Данные по попаданиям милишной атакой</param>
        /// <returns>Возвращает кол-во попаданий милишной атакой</returns>
        int CastMeleeAttack(UnitNetwork shooter, out AttackCastResult[] result);
    }
}
