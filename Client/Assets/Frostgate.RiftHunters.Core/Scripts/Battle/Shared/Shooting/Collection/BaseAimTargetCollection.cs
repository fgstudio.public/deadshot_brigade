using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Shooting
{
    public abstract class BaseAimTargetCollection<TKey> : IReadOnlyObjectCollection<TKey, IAimTarget>, IDisposable
    {
        public event Action<IAimTarget> Added;
        public event Action<IAimTarget> Removed;

        private readonly IReadOnlyObjectCollection<TKey, UnitNetwork> _unitCollection;
        private readonly IReadOnlyObjectCollection<TKey,ShootingTargetNetwork> _shootingTargetCollections;
        private readonly IReadOnlyObjectCollection<TKey, ITotem> _totemCollection;
        private readonly IReadOnlyObjectCollection<TKey, ITrap> _trapCollection;

        public IEnumerable<TKey> Keys => _unitCollection.Keys.Union(_totemCollection.Keys).Union(_trapCollection.Keys);
        public IEnumerable<IAimTarget> Values => _unitCollection.Values.OfType<IAimTarget>().Union(_totemCollection).Union(_trapCollection.Values);
        public IAimTarget this[TKey key] => Get(key);

        public BaseAimTargetCollection(
            [NotNull] IReadOnlyObjectCollection<TKey, UnitNetwork> unitCollection,
            [NotNull] IReadOnlyObjectCollection<TKey, ShootingTargetNetwork> shootingTargetCollections,
            [NotNull] IReadOnlyObjectCollection<TKey, ITotem> totemCollection,
            [NotNull] IReadOnlyObjectCollection<TKey, ITrap> trapCollection)
        {
            _unitCollection = unitCollection;
            Subscribe(_unitCollection);

            _shootingTargetCollections = shootingTargetCollections;
            Subscribe(_shootingTargetCollections);

            _totemCollection = totemCollection;
            Subscribe(_totemCollection);

            _trapCollection = trapCollection;
            Subscribe(_trapCollection);
        }

        public void Dispose()
        {
            Unsubscribe(_unitCollection);
            Unsubscribe(_shootingTargetCollections);
            Unsubscribe(_totemCollection);
            Unsubscribe(_trapCollection);
        }

        private void Subscribe<TValue>(IReadOnlyObjectCollection<TKey, TValue> collection) where TValue : IAimTarget
        {
            collection.Added += OnAdded;
            collection.Removed += OnRemoved;
        }

        private void Unsubscribe<TValue>(IReadOnlyObjectCollection<TKey, TValue> collection) where TValue : IAimTarget
        {
            collection.Added -= OnAdded;
            collection.Removed -= OnRemoved;
        }

        private void OnAdded<TValue>(TValue target) where TValue : IAimTarget => Added?.Invoke(target);
        private void OnRemoved<TValue>(TValue target) where TValue : IAimTarget => Removed?.Invoke(target);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        public IEnumerator<IAimTarget> GetEnumerator() => GetEnumerable().GetEnumerator();
        private IEnumerable<IAimTarget> GetEnumerable()
        {
            foreach (UnitNetwork unit in _unitCollection) yield return unit;
            foreach (ShootingTargetNetwork environmentShootingTarget in _shootingTargetCollections) yield return environmentShootingTarget;
            foreach (ITotem totem in _totemCollection) yield return totem;
            foreach (ITrap trap in _trapCollection) yield return trap;
        }

        public bool Contains(TKey key) =>
            _unitCollection.Contains(key) || _shootingTargetCollections.Contains(key) || _trapCollection.Contains(key);

        public bool TryGet(TKey key, out IAimTarget target)
        {
            if (_unitCollection.TryGet(key, out UnitNetwork unit))
            {
                target = unit;
                return true;
            }
            else if (_shootingTargetCollections.TryGet(key, out ShootingTargetNetwork environmentShootingTarget))
            {
                target = environmentShootingTarget;
                return true;
            }
            else if (_totemCollection.TryGet(key, out ITotem totem))
            {
                target = totem;
                return true;
            }
            else if (_trapCollection.TryGet(key, out ITrap trap))
            {
                target = trap;
                return true;
            }
            else
            {
                target = default;
                return false;
            }
        }

        public IAimTarget Get(TKey key)
        {
            if (TryGet(key, out var target))
                return target;
            else
                throw new ArgumentOutOfRangeException(nameof(key), key,
                    "Collections doesn't contain element with such key");
        }
    }
}