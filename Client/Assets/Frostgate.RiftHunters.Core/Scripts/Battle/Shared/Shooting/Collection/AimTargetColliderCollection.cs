using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Shooting
{
    public sealed class AimTargetColliderCollection : BaseAimTargetCollection<Collider>
    {
        public AimTargetColliderCollection(
            [NotNull] IReadOnlyObjectCollection<Collider, UnitNetwork> unitCollection,
            [NotNull] IReadOnlyObjectCollection<Collider, ShootingTargetNetwork> shootingTargetCollections,
            [NotNull] IReadOnlyObjectCollection<Collider, ITotem> totemCollection,
            [NotNull] IReadOnlyObjectCollection<Collider, ITrap> trapCollection)
            : base(unitCollection, shootingTargetCollections, totemCollection, trapCollection)
        { }
    }
}