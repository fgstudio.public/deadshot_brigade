using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Shooting
{
    public sealed class AimTargetNetIdCollection : BaseAimTargetCollection<uint>
    {
        public AimTargetNetIdCollection(
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection,
            [NotNull] IReadOnlyObjectCollection<uint, ShootingTargetNetwork> shootingTargetCollections,
            [NotNull] IReadOnlyObjectCollection<uint, ITotem> totemCollection,
            [NotNull] IReadOnlyObjectCollection<uint, ITrap> trapCollection)
            : base(unitCollection, shootingTargetCollections, totemCollection, trapCollection)
        { }
    }
}