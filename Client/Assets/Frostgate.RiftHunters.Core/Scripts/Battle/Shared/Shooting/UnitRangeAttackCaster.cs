using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public class UnitRangeAttackCaster : MonoBehaviour
    {
        public event Action<RaycastHit> OnWall; 

        [CanBeNull] private UnitNetwork _unit;
        [CanBeNull] private RangeWeaponConfig _rangeWeaponConfig;
        private IDamageService _damageService;
        private ShootingSharedLogic _shootingSharedLogic;
        private float _timer;

        private BulletsSimulator _bulletsSimulator;
        private UnitEffectStateFactory _effectStateFactory;

        public void Init([NotNull] UnitNetwork unit, [NotNull] IDamageService damageService,
            [NotNull] IAimTargetFinder aimTargetFinder, [NotNull] BulletProcessors bulletProcessors,
            [NotNull] UnitEffectStateFactory effectStateFactory)
        {
            _effectStateFactory = effectStateFactory;
            _unit = unit;
            _damageService = damageService;
            _rangeWeaponConfig = unit.RangeWeapon;

            _unit.ShootingSharedLogic.OnShoot += Shoot;
            _unit.ShootingSharedLogic.OnBeginAttack += _unit.UnitNetworkState.SingleAttack;

            _bulletsSimulator = new BulletsSimulator(unit, aimTargetFinder, bulletProcessors, OnHit, OnWallHit);
        }

        private void FixedUpdate() =>
            _bulletsSimulator.Update(Time.fixedDeltaTime);

        private void Shoot()
        {
            if (_unit == null)
                return;

            var directions =
                ShootingHelper.TakeBulletDirections(
                    _unit.UnitNetworkInput.AimDirection,
                    _rangeWeaponConfig,
                    _unit.UnitNetworkState.Random);

            foreach (Vector3 direction in directions)
                _bulletsSimulator.Shoot(direction);

            if (_unit.isServer && _rangeWeaponConfig?.UsePatrons == true && !_unit.UnitNetworkEffects.Params.InfiniteAmmo)
                _unit.UnitNetworkState.PatronsCount--;
        }

        private void OnHit(AttackCastResult hit, IEnumerable<UnitEffectConfig> unitEffectConfigs)
        {
            IAimTarget target = hit.Target;

            if (!target.IsDestroyedOrDead && _unit != null && _unit.Identity != null && _unit.RangeWeapon != null)
            {
                _damageService.TakeDamage(_unit.RangeWeapon, _unit, target, hit.HitPoint);

                foreach (UnitEffectConfig config in unitEffectConfigs)
                    if (target.CanReceiveUnitEffect(config))
                        target.AddUnitEffect(_effectStateFactory.Create(config, _unit.Identity));
            }
        }

        private void OnWallHit(RaycastHit rh)
        {
            OnWall?.Invoke(rh);
        }

        private void OnDestroy()
        {
            if (_unit != null)
            {
                _unit.ShootingSharedLogic.OnShoot -= Shoot;
                _unit.ShootingSharedLogic.OnBeginAttack -= _unit.UnitNetworkState.SingleAttack;
            }
        }
    }
}