using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface IShootingSharedLogic
    {
        public event Action OnShoot;
        public event Action OnBeginAttack;
        public bool NeedFreeze { get; }
        public bool InProcess { get; }

        public void Tick(float deltaTime);
    }

    public class EmptyShootingSharedLogic : IShootingSharedLogic
    {
        public event Action OnShoot;
        public event Action OnBeginAttack;
        public bool NeedFreeze => false;
        public bool InProcess => false;

        public void Tick(float deltaTime)
        {
        }
    }

    public class ShootingSharedLogic : IShootingSharedLogic
    {
        public event Action OnShoot;
        public event Action OnBeginAttack;

        public bool NeedFreeze { get; private set; }
        public bool InProcess { get; private set; }

        [CanBeNull] private RangeWeaponConfig RangeWeaponConfig =>
            _unit.RangeWeapon;

        private float Cooldown => RangeWeaponConfig != null
            ? RangeWeaponConfig.Cooldown * _unit.UnitNetworkEffects.Params.AttackCooldownMultiplier
            : default;

        private readonly UnitNetwork _unit;
        private float _currentTime;
        private float _prevTime;

        public ShootingSharedLogic(UnitNetwork unit)
        {
            _unit = unit;
            _currentTime = Cooldown;
        }

        public void Tick(float deltaTime)
        {
            if (RangeWeaponConfig == null)
                return;

            if (!_unit.CanRangeAttack)
            {
                if (InProcess)
                {
                    NeedFreeze = false;
                    InProcess = false;
                    _currentTime = 0;
                }

                return;
            }

            if (_unit.UnitNetworkInput.ShootingEnabled)
                InProcess = true;

            if (!InProcess)
                return;

            if (_currentTime >= Cooldown)
            {
                _currentTime -= Cooldown + deltaTime;
                _prevTime = 0;
            }
            else
            {
                _prevTime = _currentTime;
                _currentTime += deltaTime;
            }

            RecalculateFreeze();

            var firstTick = _prevTime == 0;
            if (firstTick && RangeWeaponConfig.SyncEveryShoot)
                OnBeginAttack?.Invoke();

            var shouldShoot =
                _prevTime <= RangeWeaponConfig.ShootTime &&
                _currentTime > RangeWeaponConfig.ShootTime;

            if (shouldShoot)
                OnShoot?.Invoke();

            // Стрельба прекращается после завершения уже начатого выстрела и отжатия курка.
            if (_currentTime >= Cooldown && !_unit.UnitNetworkInput.ShootingEnabled)
            {
                InProcess = false;
                NeedFreeze = false;
            }
        }

        private void RecalculateFreeze() =>
            NeedFreeze = RangeWeaponConfig != null &&
                         RangeWeaponConfig.FreezeBeforeShootDuration > 0 &&
                         _currentTime >= RangeWeaponConfig.Cooldown - RangeWeaponConfig.FreezeBeforeShootDuration;
    }
}