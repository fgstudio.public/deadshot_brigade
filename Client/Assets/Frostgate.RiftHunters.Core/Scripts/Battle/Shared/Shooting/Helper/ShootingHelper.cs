using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// Набор методов позволяющий определить, находится ли цель в области стрельбы, т.е. возможно ли вести по ней огонь
    /// Область стрельбы представляет собой четырехгранную пирамиду с усеченной верхней частью
    /// Центр верхнего сечения пирамиды находится в точке стрельбы, а центр дна пирамиды расположне перед персонажем по его оси Forward
    /// </summary>
    public static partial class ShootingHelper
    {
        private const float MaxShootAngle = 90;

        private static Vector2 _xzShooterForward;
        private static Vector2 _xzVectorFromProjectionStartToUnit;

        // Проверяет, находится ли цель в передней полусфере стрелка.
        // Именно этот метод отсекает верхнюю часть пирамиды "пирамиды", находящуюся за стрелком
        public static bool IsAimDirectionLookForward(Vector3 shooterForward, Vector3 aimDirection) =>
            Vector3.Angle(shooterForward, aimDirection) <= MaxShootAngle;

        public static bool IsShooterLookToFocusPoint(IShooter shooter, float inputYRotation, Vector3 targetFocusPoint)
        {
            if (shooter.RangeWeapon == null)
                return false;

            var targetRotation = Quaternion.Euler(0, inputYRotation, 0);
            var lookDirection = targetRotation * Vector3.forward;
            var shootPointWithOffset = GetShootingProjectionStartPoint(shooter.RangeWeapon, shooter.ShootPoint, lookDirection);

            var vectorFromProjectionStartToUnit = targetFocusPoint - shootPointWithOffset;

            if (!CanAttackXZ(lookDirection, shooter.RangeWeapon.AttackAngle, vectorFromProjectionStartToUnit))
                return false;

            var aimDirection = targetFocusPoint - shooter.ShootPoint;
            return IsAimDirectionLookForward(lookDirection, aimDirection);
        }

        public static bool CanShootXZ(IShooter shooter, Vector3 vectorFromProjectionStartToUnit) =>
            shooter.RangeWeapon != null &&
            CanAttackXZ(shooter.ForwardAxis, shooter.RangeWeapon.AttackAngle, vectorFromProjectionStartToUnit);

        public static Vector3 GetShootingProjectionStartPoint(IShooter shooter) =>
            GetShootingProjectionStartPoint(shooter.RangeWeapon, shooter.ShootPoint, shooter.ForwardAxis);

        public static bool CheckMeleeAttackAngle(UnitNetwork shooter, IAimTarget aimTarget)
        {
            var weapon = shooter.MeleeWeapon;

            if (weapon == null)
                return false;

            var shootPointWithOffset = GetShootingProjectionStartPoint(weapon, shooter.ShootPoint, shooter.ForwardAxis);
            var vectorFromProjectionStartToUnit = aimTarget.FocusPoint - shootPointWithOffset;

            return CanAttackXZ(shooter.ForwardAxis, weapon.AttackAngle, vectorFromProjectionStartToUnit);
        }

        public static bool IsFriendly([NotNull] IShooter shooter, [NotNull] IAimTarget aimTarget) =>
            shooter.UnitType == aimTarget.UnitType;

        public static bool IsShooterInsideTarget([CanBeNull] IShooter shooter, [CanBeNull] IAimTarget aimTarget)
        {
            if (shooter?.GameObject == null || aimTarget?.GameObject == null) return false;
            float sqrDistance = (shooter.GameObject.transform.position - aimTarget.GameObject.transform.position).sqrMagnitude;
            return sqrDistance <= Mathf.Pow(aimTarget.BodyRadius, 2);
        }

        // Проверяет, попадает ли цель в область стрельбы, но только в горизонтальной плоскости
        private static bool CanAttackXZ(Vector3 shooterForward, float attackAngle, Vector3 vectorFromProjectionStartToUnit)
        {
            _xzShooterForward.Set(shooterForward.x, shooterForward.z);
            _xzVectorFromProjectionStartToUnit.Set(vectorFromProjectionStartToUnit.x, vectorFromProjectionStartToUnit.z);

            var angleXZ = Vector2.Angle(_xzShooterForward, _xzVectorFromProjectionStartToUnit);

            return angleXZ <= attackAngle;
        }

        // Вычисляет вершину не усеченной пирамиды, которая как правило находится за персонажем в не простреливаемой области.
        public static Vector3 GetShootingProjectionStartPoint([CanBeNull] IWeapon weapon, Vector3 shootPoint, Vector3 shooterForward)
        {
            if (weapon == null) return Vector3.zero;

            var forwardOffset = -weapon.AttackOriginHalfSize / Mathf.Tan(weapon.AttackAngle * Mathf.Deg2Rad);
            var projectionStartPoint = shootPoint + shooterForward * forwardOffset;

            return projectionStartPoint;
        }

        public static IShootingSharedLogic CreateShootingLogic(UnitNetwork unit)
        {
            if (unit.RangeWeapon == null)
                return new EmptyShootingSharedLogic();

            return new ShootingSharedLogic(unit);
        }
    }
}