using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public static partial class ShootingHelper
    {
        public static IEnumerable<Vector3> TakeBulletDirections(Vector3 aimDirection, RangeWeaponConfig weaponConfig, UnitRandom random)
        {
            for (var i = 0; i < weaponConfig.BulletsPerShoot; i++)
            {
                if (weaponConfig.BulletsPerShoot > 1 && i == 0)
                {
                    yield return aimDirection;
                    continue;
                }

                var scatterAngle = weaponConfig.ScatterAngle;

                var direction = random.RandomizeBulletDirection(aimDirection, scatterAngle);

                yield return direction;
            }
        }
    }
}