using System;
using Zenject;
using UnityEngine;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Shooting
{
    [DisallowMultipleComponent]
    public sealed class AimTargetFinder : MonoBehaviour, IAimTargetFinder
    {
        private IReadOnlyObjectCollection<Collider, IAimTarget> _aimTargetCollection;
        private readonly AttackCastResult[] _attackCastResults = new AttackCastResult[256];

        [Inject]
        private void Mono_Constructor(IReadOnlyObjectCollection<Collider, IAimTarget> aimTargetCollection) =>
            _aimTargetCollection = aimTargetCollection;

        public bool TryGetAimTarget(IShooter shooter, IEnumerable<IAimTarget> possibleTargets, out IAimTarget aimTarget)
        {
            aimTarget = null;
            var closestDistance = float.MaxValue;
            var shootPointWithOffset = ShootingHelper.GetShootingProjectionStartPoint(shooter);

            if (shooter.RangeWeapon == null)
                return false;

            foreach (IAimTarget potentialTarget in possibleTargets)
            {
                if (!CanShoot(shooter, potentialTarget))
                    continue;

                if (ShootingHelper.IsShooterInsideTarget(shooter, potentialTarget))
                    continue;

                var vectorToUnit = potentialTarget.FocusPoint - shooter.ShootPoint;
                var distance = vectorToUnit.magnitude;

                if (distance > shooter.RangeWeapon.MaxCastDistance)
                    continue;

                var vectorToUnitWithOffset = potentialTarget.FocusPoint - shootPointWithOffset;
                bool priorityTarget = false;

                if (potentialTarget.IsPriorityTarget)
                    priorityTarget = CheckPriorityTarget(shooter, potentialTarget, distance, vectorToUnitWithOffset, false);

                if (potentialTarget.CapturedByDirectAiming)
                {
                    priorityTarget = CheckPriorityTarget(shooter, potentialTarget, distance, vectorToUnitWithOffset, priorityTarget);
                    if (!priorityTarget)
                        continue;
                }

                if (closestDistance < distance && !priorityTarget)
                    continue;

                if (!ShootingHelper.CanShootXZ(shooter, vectorToUnitWithOffset))
                    continue;

                var vectorToUnitRotation = Vector3.SignedAngle(Vector3.forward, vectorToUnitWithOffset, Vector3.up);
                var forwardedVectorToUnit = vectorToUnitWithOffset.RotateY(-vectorToUnitRotation);

                var angleYZ = Vector2.Angle(
                    Vector2.right,
                    new Vector2(forwardedVectorToUnit.z, forwardedVectorToUnit.y));

                if (angleYZ > shooter.RangeWeapon.AttackVerticalAngle)
                    continue;

                if (!ShootingHelper.IsAimDirectionLookForward(shooter.ForwardAxis, vectorToUnit))
                    continue;

                if (!TryShootRaycast(shooter, vectorToUnit).HasFlag(RaycastHitResult.Hit))
                    continue;

                closestDistance = distance;
                aimTarget = potentialTarget;

                if (priorityTarget)
                    break;
            }

            return aimTarget != null;
        }

        public IEnumerable<IAimTarget> GetAimTargets(IShooter shooter, IEnumerable<IAimTarget> possibleTargets)
        {
            if (shooter.RangeWeapon == null)
                yield break;

            var shootPointWithOffset = ShootingHelper.GetShootingProjectionStartPoint(shooter);

            foreach (IAimTarget potentialTarget in possibleTargets)
            {
                if (!CanShoot(shooter, potentialTarget))
                    continue;

                if (ShootingHelper.IsShooterInsideTarget(shooter, potentialTarget))
                    continue;

                var vectorToUnit = potentialTarget.FocusPoint - shooter.ShootPoint;
                var distance = vectorToUnit.magnitude;

                if (distance > shooter.RangeWeapon.MaxCastDistance)
                    continue;

                var vectorToUnitWithOffset = potentialTarget.FocusPoint - shootPointWithOffset;

                if (!ShootingHelper.CanShootXZ(shooter, vectorToUnitWithOffset))
                    continue;

                var vectorToUnitRotation = Vector3.SignedAngle(Vector3.forward, vectorToUnitWithOffset, Vector3.up);
                var forwardedVectorToUnit = vectorToUnitWithOffset.RotateY(-vectorToUnitRotation);

                var angleYZ = Vector2.Angle(
                    Vector2.right,
                    new Vector2(forwardedVectorToUnit.z, forwardedVectorToUnit.y));

                if (angleYZ > shooter.RangeWeapon.AttackVerticalAngle)
                    continue;

                if (!ShootingHelper.IsAimDirectionLookForward(shooter.ForwardAxis, vectorToUnit))
                    continue;

                if (!TryShootRaycast(shooter, vectorToUnit).HasFlag(RaycastHitResult.Hit))
                    continue;

                yield return potentialTarget;
            }
        }

        private static bool CheckPriorityTarget(IShooter shooter, IAimTarget potentialTarget, float distance, Vector3 vectorToUnitWithOffset, bool priorityTarget)
        {
            var angleLimitForSpecialTarget = Mathf.Asin(potentialTarget.BodyRadius / distance) * Mathf.Rad2Deg;
            var planedVectorToUnit = new Vector3(vectorToUnitWithOffset.x, 0, vectorToUnitWithOffset.z);
            var angleOffset = Vector3.Angle(planedVectorToUnit, shooter.ForwardAxis);

            if (angleOffset <= angleLimitForSpecialTarget)
                priorityTarget = true;
            return priorityTarget;
        }

        public RaycastHitResult TryShootRaycast(IShooter shooter, Vector3 direction, Action<AttackCastResult> onHit = null, Action<RaycastHit> onWall = null)
        {
            if (shooter.RangeWeapon == null) return default;
            var distance = shooter.RangeWeapon.MaxCastDistance;
            return TryShootRaycast(shooter, shooter.ShootPoint, direction, distance, onHit, onWall);
        }

        public RaycastHitResult TryShootRaycast(IShooter shooter, Vector3 origin, Vector3 direction, float distance, Action<AttackCastResult> onHit = null, Action<RaycastHit> onWall = null)
        {
            var count = Physics.RaycastNonAlloc(origin, direction, PhysicHelper.HitResults, distance, PhysicsLayers.AimTargetAndWallMask);

            var distanceToWall = float.MaxValue;
            var closestTargetDist = float.MaxValue;
            var unitHitCount = 0;
            var result = RaycastHitResult.None;
            RaycastHit wallRaycastHit = default;

            for (var i = 0; i < count && i < PhysicHelper.HitResults.Length; i++)
            {
                if (unitHitCount >= _attackCastResults.Length)
                    break;

                var hit = PhysicHelper.HitResults[i];

                if (!_aimTargetCollection.TryGet(hit.collider, out IAimTarget aimTarget))
                {
                    result |= RaycastHitResult.Wall;
                    wallRaycastHit = hit;

                    // Порядок хитов не гарантирован. Кешируем расстояние до ближайшей стены.
                    if (hit.distance < distanceToWall)
                        distanceToWall = hit.distance;
                    continue;
                }

                if (ShootingHelper.IsFriendly(shooter, aimTarget))
                    continue;

                if (ShootingHelper.IsShooterInsideTarget(shooter, aimTarget))
                    continue;

                if (!shooter.PiercingBullets)
                    if (hit.distance > closestTargetDist)
                        continue;
                    else
                        closestTargetDist = hit.distance;

                var resultIndex = shooter.PiercingBullets ? unitHitCount : 0;

                _attackCastResults[resultIndex].Target = aimTarget;
                _attackCastResults[resultIndex].HitPoint = hit.point;
                _attackCastResults[resultIndex].Distance = hit.distance;

                if (shooter.PiercingBullets)
                    unitHitCount++;
                else
                    unitHitCount = 1;
            }

            var hasHit = false;
            for (var i = 0; i < unitHitCount; i++)
            {
                if (_attackCastResults[i].Distance > distanceToWall)
                    continue;

                onHit?.Invoke(_attackCastResults[i]);
                hasHit = true;
            }

            if (hasHit)
                result |= RaycastHitResult.Hit;

            if (result.HasFlag(RaycastHitResult.Wall))
                onWall?.Invoke(wallRaycastHit);

            return result;
        }

        /// <summary>
        /// Кастуем милишную атаку
        /// </summary>
        /// <param name="shooter"></param>
        /// <param name="result">Данные по попаданиям милишной атакой</param>
        /// <returns>Возвращает кол-во попаданий милишной атакой</returns>
        public int CastMeleeAttack(UnitNetwork shooter, out AttackCastResult[] result)
        {
            var count = 0;

            var radius = shooter.MeleeWeapon?.MaxCastDistance ?? default;
            var hitCount =
                Physics.SphereCastNonAlloc(shooter.ShootPoint, radius, Vector3.up, PhysicHelper.HitResults,
                    0, PhysicsLayers.AimTargetMask);

            for (var i = 0; i < hitCount && i < PhysicHelper.HitResults.Length && count < _attackCastResults.Length; i++)
            {
                var hit = PhysicHelper.HitResults[i];

                if (!_aimTargetCollection.TryGet(hit.collider, out var targetUnit))
                    continue;

                if (targetUnit == null || targetUnit.State.IsDead)
                    continue;

                if (shooter.gameObject == targetUnit.GameObject)
                    continue;

                if (ShootingHelper.IsShooterInsideTarget(shooter, targetUnit))
                    continue;

                if (ShootingHelper.IsFriendly(shooter, targetUnit))
                    continue;

                if (!ShootingHelper.CheckMeleeAttackAngle(shooter, targetUnit))
                    continue;

                _attackCastResults[count].Target = targetUnit;
                _attackCastResults[count].HitPoint = hit.point;
                _attackCastResults[count].Distance = hit.distance;
                count++;
            }

            result = _attackCastResults;
            return count;
        }

        private static bool CanShoot(IShooter shooter, IAimTarget potentialTarget)
        {
            return !potentialTarget.IsNullDestroyedOrDead()
                   && !ReferenceEquals(potentialTarget, shooter)
                   && !ShootingHelper.IsFriendly(shooter, potentialTarget);
        }
    }

    public struct AttackCastResult
    {
        public IAimTarget Target;
        public Vector3 HitPoint;
        public float Distance;
    }
}