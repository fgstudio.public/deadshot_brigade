using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Shooting
{
    public interface IShooter : IGameObject
    {
        [CanBeNull] RangeWeaponConfig RangeWeapon { get; }
        Vector3 ForwardAxis { get; }
        Vector3 ShootPoint { get; }
        BattleUnitType UnitType { get; }
        bool PiercingBullets { get; }
    }
}