namespace Frostgate.RiftHunters.Core.Battle.Shared.Shooting
{
    public static class AimTargetExtension
    {
        public static bool IsNullDestroyedOrDead(this IAimTarget aimTarget) => aimTarget == null || aimTarget.IsDestroyedOrDead;
    }
}