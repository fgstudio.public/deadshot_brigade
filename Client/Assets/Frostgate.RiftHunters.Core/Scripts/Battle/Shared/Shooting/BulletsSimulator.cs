using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Shooting
{
    public interface IBulletProcessor
    {
        void Process([NotNull] BulletData bullet, [NotNull] UnitNetwork unit, ref float step);
    }

    public class BulletProcessors
    {
        private readonly List<IBulletProcessor> _processors = new();

        public void Add(IBulletProcessor processor) =>
            _processors.Add(processor);

        public void Remove(IBulletProcessor processor) =>
            _processors.Remove(processor);

        public void Process(BulletData bullet, UnitNetwork unit, ref float step)
        {
            foreach (var handler in _processors)
                handler.Process(bullet, unit, ref step);
        }
    }

    public class BulletData
    {
        public Vector3 Position;
        public Vector3 Direction;
        public float Distance;
        [CanBeNull] public RangeWeaponConfig RangeWeaponConfig;
        public readonly List<UnitEffectConfig> Effects = new();
    }

    public class BulletsSimulator
    {
        private readonly List<BulletData> _bullets = new();
        private readonly List<BulletData> _bulletsRemoveList = new();
        private readonly IAimTargetFinder _aimTargetFinder;
        private readonly UnitNetwork _unit;
        private readonly Action<AttackCastResult, IEnumerable<UnitEffectConfig>> _onHit;
        private readonly Action<RaycastHit> _onWall;
        private readonly BulletProcessors _bulletProcessors;
        [CanBeNull] private readonly RangeWeaponConfig _rangeWeaponConfig;

        public BulletsSimulator(UnitNetwork unit, IAimTargetFinder aimTargetFinder, [CanBeNull] BulletProcessors bulletProcessors,
            Action<AttackCastResult, IEnumerable<UnitEffectConfig>> onHit = null, Action<RaycastHit> onWall = null)
        {
            _bulletProcessors = bulletProcessors;
            _unit = unit;
            _aimTargetFinder = aimTargetFinder;
            _onHit = onHit;
            _onWall = onWall;
            _rangeWeaponConfig = _unit.RangeWeapon;
        }

        public void Shoot(Vector3 direction)
        {
            direction = direction.normalized;
            var maxDist = _unit.RangeWeapon?.MaxCastDistance ?? default;
            CreateBullet(_unit.ShootPoint, direction, maxDist);
        }

        public void Update(float deltaTime)
        {
            foreach (var bulletData in _bullets)
            {
                if (bulletData.RangeWeaponConfig == null)
                    continue;

                var step = bulletData.RangeWeaponConfig.BulletSpeed * deltaTime;
                var pos = bulletData.Position;
                var dir = bulletData.Direction;

                if (step > bulletData.Distance)
                    step = bulletData.Distance;

                _bulletProcessors?.Process(bulletData, _unit, ref step);

#if UNITY_EDITOR
                UnityEngine.Debug.DrawLine(bulletData.Position, bulletData.Position + dir * step, Color.red, deltaTime);
#endif

                var hitResult = _aimTargetFinder.TryShootRaycast(_unit, pos, dir, step, x => OnHit(x, bulletData.Effects), OnWall);
                bulletData.Distance -= step;
                bulletData.Position += dir * step;

                if (NeedStopBulletByDist(bulletData) || NeedStopBulletByHit(hitResult) || NeedStopBulletByWall(hitResult))
                    _bulletsRemoveList.Add(bulletData);
            }

            foreach (var bulletData in _bulletsRemoveList)
                _bullets.Remove(bulletData);

            _bulletsRemoveList.Clear();
        }

        private bool NeedStopBulletByWall(RaycastHitResult hitResult) => hitResult.HasFlag(RaycastHitResult.Wall);
        private bool NeedStopBulletByHit(RaycastHitResult hitResult) => hitResult.HasFlag(RaycastHitResult.Hit) && !_unit.PiercingBullets;
        private bool NeedStopBulletByDist(BulletData bulletData) => bulletData.Distance <= 0;

        private void CreateBullet(Vector3 origin, Vector3 direction, float distance) =>
            _bullets.Add(new BulletData
            {
                Position = origin,
                Direction = direction,
                Distance = distance,
                RangeWeaponConfig = _rangeWeaponConfig
            });

        private void OnHit(AttackCastResult hit, IEnumerable<UnitEffectConfig> effects) => _onHit?.Invoke(hit, effects);

        private void OnWall(RaycastHit raycastHit) => _onWall?.Invoke(raycastHit);
    }
}