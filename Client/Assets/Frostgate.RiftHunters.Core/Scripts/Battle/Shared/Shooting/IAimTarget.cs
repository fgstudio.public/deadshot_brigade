using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Shooting
{
    public interface IAimTargetView : IFxHolder
    {
        Transform DamageFxTarget { get; }
        Transform DamageReflectFxTarget { get; }
    }

    public interface IAimTarget : IDamageReceiver
    {
        BattleUnitType UnitType { get; }
        IAimTargetView View { get; }
        float BodyRadius { get; }
        bool IsDestroyedOrDead { get; }
        Vector3 FocusPoint { get; }
        bool IsPriorityTarget { get; }
        bool CapturedByDirectAiming { get; }
    }
}