using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Shooting
{
    [Flags]
    public enum RaycastHitResult
    {
        None = 0,
        Wall = 1,
        Hit = 2
    }
}