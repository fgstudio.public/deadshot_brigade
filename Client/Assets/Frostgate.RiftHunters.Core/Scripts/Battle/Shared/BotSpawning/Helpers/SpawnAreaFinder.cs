﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    public sealed class SpawnAreaFinder
    {
        [NotNull] private readonly IReadOnlyBotArenaState _arenaState;
        [NotNull] private readonly BotArenaSceneConfiguration _sceneConfiguration;

        public SpawnAreaFinder([NotNull] IReadOnlyBotArenaState arenaState, [NotNull] BotArenaSceneConfiguration sceneConfiguration)
        {
            _arenaState = arenaState;
            _sceneConfiguration = sceneConfiguration;
        }

        public BotSpawnArea FindNearestOnCurrentArena(Vector3 nearestPosition, bool isCustom)
        {
            BotArenaSceneData arenaSceneData = FindCurrentArenaSceneData();

            IEnumerable<BotSpawnArea> spawnAreas = arenaSceneData.SpawnerData
                .SelectMany(d => d.SpawnAreas)
                .Where(a => !isCustom || a.AvailableForCustomSpawner);

            return spawnAreas
                .OrderBy(a => Vector3.SqrMagnitude(a.Center - nearestPosition))
                .First();
        }

        private BotArenaSceneData FindCurrentArenaSceneData()
        {
            string currentArenaId = _arenaState.CurrentArenaId;
            return _sceneConfiguration.ArenaData
                .First(d => d.Config.Id == currentArenaId);
        }
    }
}