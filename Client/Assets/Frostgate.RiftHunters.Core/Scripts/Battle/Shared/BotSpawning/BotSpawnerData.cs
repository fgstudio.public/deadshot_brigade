namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    public enum BotSpawnerStatus
    {
        Spawning,
        Awaiting,
        Completed,
        Cancelled
    }

    public readonly struct BotSpawnerData
    {
        public readonly string Id;
        public readonly int WaveIndex;
        public readonly int DeadBots;
        public readonly int TotalBots;
        public readonly float WaitingStartTime;
        public readonly BotSpawnerStatus Status;

        public int AliveBots => TotalBots - DeadBots;

        public BotSpawnerData(string id, int waveIndex, int deadBots,
            int totalBots, float waitingStartTime, BotSpawnerStatus status)
        {
            Id = id;
            WaveIndex = waveIndex;
            DeadBots = deadBots;
            TotalBots = totalBots;
            WaitingStartTime = waitingStartTime;
            Status = status;
        }
    }
}