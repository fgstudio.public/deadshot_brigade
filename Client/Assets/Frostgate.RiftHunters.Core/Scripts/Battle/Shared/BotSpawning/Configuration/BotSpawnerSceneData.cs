using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(BotSpawnerSceneData))]
    public sealed class BotSpawnerSceneData : MonoBehaviour
    {
        [Required, AssetsOnly, AssetSelector]
        [SerializeField] private BotSpawnerConfig _config;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private BotSpawnArea[] _spawnAreas;

        [SerializeField] private BotSpawnerAction _activation;
        [SerializeField] private BotSpawnerAction _deactivation;

        public BotSpawnerConfig Config => _config;
        public IReadOnlyList<BotSpawnArea> SpawnAreas => _spawnAreas;
        public BotSpawnerAction Activation => _activation;
        public BotSpawnerAction Deactivation => _deactivation;
    }
}