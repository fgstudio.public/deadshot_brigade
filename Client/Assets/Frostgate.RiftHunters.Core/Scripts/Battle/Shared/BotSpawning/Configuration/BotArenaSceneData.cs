using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(BotArenaSceneData))]
    public sealed class BotArenaSceneData : MonoBehaviour
    {
        [Required, AssetsOnly, AssetSelector]
        [SerializeField] private BotArenaConfig _config;

        [SceneObjectsOnly, Required]
        [SerializeField] private ActivitySceneData[] _activities;

        [ChildGameObjectsOnly]
        [SerializeField, Required] private Checkpoint _respawnPoint;

        [ChildGameObjectsOnly]
        [SerializeField] private BotArenaPassage _exitPassage;

        [ChildGameObjectsOnly]
        [SerializeField] private UnitEffectsArea _effectsArea;

        [Required]
        [SerializeField] private GameObject[] _activeObjects;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private BotSpawnerSceneData[] _spawnerData;

        public BotArenaConfig Config => _config;
        public Checkpoint RespawnPoint => _respawnPoint;
        [NotNull] public IReadOnlyList<ActivitySceneData> Activities => _activities;
        [CanBeNull] public BotArenaPassage ExitPassage => _exitPassage;
        [CanBeNull] public UnitEffectsArea EffectsArea => _effectsArea;
        [NotNull, ItemNotNull] public IReadOnlyList<GameObject> ActiveObjects => _activeObjects;
        public IReadOnlyList<BotSpawnerSceneData> SpawnerData => _spawnerData;

        private void OnValidate()
        {
            _respawnPoint ??= GetComponentInChildren<Checkpoint>();
        }
    }
}