using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Server.Systems.Menu + "/" + nameof(BotArenaSceneConfiguration))]
    public sealed class BotArenaSceneConfiguration : MonoBehaviour
    {
        [SerializeField, Required] private BotArenaSceneData[] _arenaData;

        [NotNull, ItemNotNull] public IReadOnlyList<BotArenaSceneData> ArenaData => _arenaData;
    }
}