using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    public enum BotSpawnerActionType
    {
        ByArenaLifecycle,
        ByTrigger,
        BySpawnerStart,
        BySpawnerFinish
    }

    [Serializable, InlineProperty]
    public sealed class BotSpawnerAction
    {
        [HideLabel, HorizontalGroup] [SerializeField]
        private BotSpawnerActionType _type;

        [HideLabel, HorizontalGroup, ShowIf(nameof(IsTriggerUsed))]
        [SerializeField, Required, SceneObjectsOnly] private BotSpawnerTrigger _trigger;

        [HideLabel, HorizontalGroup, ShowIf(nameof(IsSpawnerUsed))]
        [SerializeField, Required, SceneObjectsOnly] private BotSpawnerSceneData _spawner;

        public BotSpawnerActionType Type => _type;
        [CanBeNull] public BotSpawnerTrigger Trigger => IsTriggerUsed ? _trigger : null;
        [CanBeNull] public BotSpawnerSceneData Spawner => IsSpawnerUsed ? _spawner : null;

        private bool IsTriggerUsed => _type == BotSpawnerActionType.ByTrigger;
        private bool IsSpawnerUsed => _type is
            BotSpawnerActionType.BySpawnerStart or
            BotSpawnerActionType.BySpawnerFinish;
    }
}