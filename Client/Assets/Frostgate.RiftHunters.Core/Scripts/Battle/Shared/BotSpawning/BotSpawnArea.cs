using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [HelpURL("https://www.notion.so/frostgate/59ba6d9bdc3c4504b5787e636ec174ec")]
    [AddComponentMenu(BattleComponentMenus.Server.Systems.Menu + "/" + nameof(BotSpawnArea))]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Transform))]
    // TODO: отделить серверный SpawnPositionProvider от shared SpawnArea
    public sealed class BotSpawnArea : Component, ISpawnPositionProvider
    {
        private const int MinRadius = 1;
        private const int MaxRadius = 1000;

        private const int MinBotOffset = 1;
        private const int MaxBotOffset = 1000;

        public Vector3 HomePositionOffset;

        [ShowInInspector, ReadOnly] public Vector3 Center => transform.position;

        [field: SerializeField, Range(MinRadius, MaxRadius)]
        public float Radius { get; private set; }

        [SerializeField, Range(MinBotOffset, MaxBotOffset)]
        private float _distanceBetweenBots;

        [field: SerializeField]
        public bool AvailableForCustomSpawner { get; private set; }

        private ISpawnPositionProvider _spawnPositionProvider;

        [Inject]
        public void MonoConstructor(AreaRandom random,
            ILogger<CachedSpawnPositionProvider> logger)
        {
            var provider = new CachedSpawnPositionProvider(
                random, logger, Center, Radius, _distanceBetweenBots, HomePositionOffset);

            _spawnPositionProvider = new AutoResetSpawnPositionProvider(provider);
        }

        bool ISpawnPositionProvider.TryGetPosition(out Vector3 position, out Vector3 homePosition)
        {
            // TODO-SG: Некоторое время "послушать" исключения, если они будут. Потом удалить.
            try
            {
                return _spawnPositionProvider.TryGetPosition(out position, out homePosition);
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogException(e);
                UnityEngine.Debug.LogError(
                    $"BotSpawnArea error. '{gameObject.transform.parent.name}.{gameObject.name}' cannot provide spawn position.");
                throw;
            }
        }

        void ISpawnPositionProvider.Reset() => _spawnPositionProvider.Reset();
    }
}