using Mirror;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(BotArenaPassageState))]
    public sealed class BotArenaPassageState : NetworkBehaviour
    {
        public enum PassageStatus : byte
        {
            Allowed = 0,
            Blocked = 1
        }

        [SyncVar(hook = nameof(OnInPassageChanged))]
        [SerializeField] private PassageStatus _inPassage;

        [SyncVar(hook = nameof(OnOutPassageChanged))]
        [SerializeField] private PassageStatus _outPassage;

        [field: SerializeField] public UnityEvent<PassageStatus> InPassageChanged { get; private set; }
        [field: SerializeField] public UnityEvent<PassageStatus> OutPassageChanged { get; private set; }

        public PassageStatus InPassage => _inPassage;
        public PassageStatus OutPassage => _outPassage;

        [Server]
        public void SetInPassage(PassageStatus status) =>
            NetworkHelper.ModifySyncEnum(
                _inPassage, status, v => _inPassage = v, OnInPassageChanged);

        [Server]
        public void SetOutPassage(PassageStatus status) =>
            NetworkHelper.ModifySyncEnum(
                _outPassage, status, v => _outPassage = v, OnOutPassageChanged);

        private void OnInPassageChanged(PassageStatus oldStatus, PassageStatus newStatus)
        {
            if (oldStatus != newStatus)
                InPassageChanged?.Invoke(newStatus);
        }

        private void OnOutPassageChanged(PassageStatus oldStatus, PassageStatus newStatus)
        {
            if (oldStatus != newStatus)
                OutPassageChanged?.Invoke(newStatus);
        }
    }
}