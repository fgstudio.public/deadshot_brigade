using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(BotArenaPassage))]
    public sealed class BotArenaPassage : MonoBehaviour
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private BotArenaPassageState _state;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private GameObject _inPassageObject;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private GameObject _outPassageObject;

        public BotArenaPassageState State => _state;

        private void Awake()
        {
            _state.InPassageChanged.AddListener(OnInPassageChanged);
            _state.OutPassageChanged.AddListener(OnOutPassageChanged);

            OnInPassageChanged(_state.InPassage);
            OnOutPassageChanged(_state.OutPassage);
        }

        private void OnDestroy()
        {
            _state.InPassageChanged.RemoveListener(OnInPassageChanged);
            _state.OutPassageChanged.RemoveListener(OnOutPassageChanged);
        }

        private void OnInPassageChanged(BotArenaPassageState.PassageStatus status) =>
            _inPassageObject.SetActive(IsObjectActive(status));

        private void OnOutPassageChanged(BotArenaPassageState.PassageStatus status) =>
            _outPassageObject.SetActive(IsObjectActive(status));

        private static bool IsObjectActive(BotArenaPassageState.PassageStatus status) =>
            status switch
            {
                BotArenaPassageState.PassageStatus.Allowed => false,
                BotArenaPassageState.PassageStatus.Blocked => true,
                _ => throw new ArgumentOutOfRangeException(nameof(status), status, null)
            };
    }
}