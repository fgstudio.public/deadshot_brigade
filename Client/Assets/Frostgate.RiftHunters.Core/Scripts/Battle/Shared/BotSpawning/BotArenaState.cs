using System;
using System.Linq;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints;
using JetBrains.Annotations;
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using Zenject;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    public enum BotArenaStatus
    {
        Deactivated,
        Activated,
        Completed
    }

    public readonly struct BotArenaData
    {
        public readonly string Id;
        public readonly int Index;
        public readonly BotArenaStatus Status;
        public readonly float StartTime;
        public readonly float CompletionTime;

        public BotArenaData(string id, int index,BotArenaStatus status,
            float startTime = 0, float completionTime = 0)
        {
            Id = id;
            Index = index;
            Status = status;
            StartTime = startTime;
            CompletionTime = completionTime;
        }
    }

    public interface IReadOnlyBotArenaState
    {
        UnityEvent Initialized { get; }
        UnityEvent<string> DataUpdated { get; }
        UnityEvent<bool> AreAllArenasCompletedChanged { get; }

        bool IsInitialized { get; }
        /// <summary>
        /// Null when all arenas completed.
        /// </summary>
        [CanBeNull] string CurrentArenaId { get; }
        bool AreAllArenasCompleted { get; }
        IReadOnlyDictionary<string, BotArenaData> Data { get; }

        BotArenaStatus GetStatus(string id);
    }

    public interface IEditableBotArenaState : IReadOnlyBotArenaState
    {
        new bool AreAllArenasCompleted { get; [Server] set; }
        [Server] void SetStatus(string id, int index, BotArenaStatus status);
    }

    public sealed class BotArenaState : NetworkBehaviour, IEditableBotArenaState
    {
        [field: SerializeField] public UnityEvent Initialized { get; private set; }
        [field: SerializeField] public UnityEvent<string> DataUpdated { get; private set; }
        [field: SerializeField] public UnityEvent<bool> AreAllArenasCompletedChanged { get; private set; }

        [SyncVar(hook = nameof(AreAllArenasCompletedHook))]
        [SerializeField] private bool _areAllArenasCompleted;

        private readonly SyncDictionary<string, BotArenaData> _arenaData = new();

        private SimulationTime _simTime;

        public string CurrentArenaId => _arenaData
            .FirstOrDefault(kv => kv.Value.Status == BotArenaStatus.Activated).Key;

        public bool AreAllArenasCompleted => _areAllArenasCompleted;
        public IReadOnlyDictionary<string, BotArenaData> Data => _arenaData;
        public bool IsInitialized { get; private set; }

        bool IEditableBotArenaState.AreAllArenasCompleted
        {
            [Server] get => _areAllArenasCompleted;
            [Server] set => NetworkHelper.ModifySyncVar(_areAllArenasCompleted, value,
                v => _areAllArenasCompleted = v, AreAllArenasCompletedHook);
        }

        [Inject]
        public void MonoConstructor([NotNull] SimulationTime simTime)
        {
            _simTime = simTime;
        }

        public override void OnStartServer()
        {
            base.OnStartServer();
            Initialize();
        }

        public override void OnStartClient()
        {
            base.OnStartClient();
            if (isClientOnly) Initialize();
        }

        private void Initialize()
        {
            IsInitialized = true;
            Initialized?.Invoke();

            foreach (string id in _arenaData.Keys)
                DataUpdated?.Invoke(id);
        }

        private void Start() => _arenaData.Callback += OnDataUpdated;
        private void OnDestroy() => _arenaData.Callback -= OnDataUpdated;

        private void AreAllArenasCompletedHook(bool oldValue, bool newValue)
        {
            if (oldValue != newValue)
                AreAllArenasCompletedChanged?.Invoke(newValue);
        }

        private void OnDataUpdated(SyncIDictionary<string, BotArenaData>.Operation op, string key, BotArenaData item)
        {
            switch (op)
            {
                case SyncIDictionary<string, BotArenaData>.Operation.OP_ADD:
                case SyncIDictionary<string, BotArenaData>.Operation.OP_SET:
                case SyncIDictionary<string, BotArenaData>.Operation.OP_REMOVE:
                    DataUpdated?.Invoke(key);
                    break;
                case SyncIDictionary<string, BotArenaData>.Operation.OP_CLEAR:
                    foreach (string arenaId in _arenaData.Keys) DataUpdated?.Invoke(arenaId);
                    break;

                default: throw new ArgumentOutOfRangeException(nameof(op), op, null);
            }
        }

        [Server]
        void IEditableBotArenaState.SetStatus(string id, int index, BotArenaStatus status)
        {
            float startTime = status switch
            {
                BotArenaStatus.Deactivated => _arenaData.TryGetValue(id, out var prevData) ? prevData.StartTime : default,
                BotArenaStatus.Activated => _arenaData.TryGetValue(id, out var prevData) ? prevData.StartTime : _simTime.Time,
                BotArenaStatus.Completed => _arenaData[id].StartTime,
                _ => throw new ArgumentOutOfRangeException(status.GetType().Name)
            };

            float completionTime = status switch
            {
                BotArenaStatus.Deactivated => default,
                BotArenaStatus.Activated => default,
                BotArenaStatus.Completed => _simTime.Time,
                _ => throw new ArgumentOutOfRangeException(status.GetType().Name)
            };

            _arenaData[id] = new BotArenaData(id, index, status, startTime, completionTime);
        }

        public BotArenaStatus GetStatus(string id) =>
            _arenaData.TryGetValue(id, out BotArenaData data)
                ? data.Status : BotArenaStatus.Deactivated;
    }
}