using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Server.Systems.Menu + "/" + nameof(BotSpawnerTrigger))]
    public sealed class BotSpawnerTrigger : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] private bool _isReusable;

        [field: Title("Info"), ShowInInspector, ReadOnly]
        public bool IsUsed { get; private set; }

        [field: FoldoutGroup("Events"), PropertySpace]
        [field: SerializeField] public UnityEvent Triggered { get; private set; }

        public bool IsReusable => _isReusable;

        [PropertySpace, Button]
        public void Trigger()
        {
            if (IsReusable || !IsUsed)
            {
                IsUsed = true;
                Triggered?.Invoke();
            }
        }

        [Button]
        public void Reset() =>
            IsUsed = false;
    }
}