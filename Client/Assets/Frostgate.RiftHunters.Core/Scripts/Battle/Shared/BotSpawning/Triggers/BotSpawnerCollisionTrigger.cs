using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(BotSpawnerTrigger))]
    [AddComponentMenu(BattleComponentMenus.Server.Systems.Menu + "/" + nameof(BotSpawnerCollisionTrigger))]
    public sealed class BotSpawnerCollisionTrigger : MonoBehaviour
    {
        [Title("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private ObjectCatcher _catcher;
        [SerializeField, Required, ChildGameObjectsOnly] private BotSpawnerTrigger _botSpawnerTrigger;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _catcher ??= GetComponentInChildren<ObjectCatcher>();
            _botSpawnerTrigger ??= GetComponentInChildren<BotSpawnerTrigger>();
        }

        private void OnEnable()
        {
            _catcher.Caught.AddListener(OnCaught);
            if (_catcher.Objects.Count > 0) OnCaught();
        }

        private void OnDisable() =>
            _catcher.Caught.RemoveListener(OnCaught);

        private void OnCaught(Collider _) =>
            OnCaught();

        private void OnCaught() =>
            _botSpawnerTrigger.Trigger();
    }
}