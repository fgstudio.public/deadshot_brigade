﻿using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    public sealed partial class BotSpawnState
    {
        void IEditableBotSpawnState.SetSpawning(string spawnerId, int waveIndex)
        {
            if (_data.TryGetValue(spawnerId, out BotSpawnerData data))
                _data[spawnerId] = new BotSpawnerData(spawnerId, waveIndex, data.DeadBots,
                    data.TotalBots, data.WaitingStartTime, BotSpawnerStatus.Spawning);
            else
                _data[spawnerId] = new BotSpawnerData(spawnerId, waveIndex, default,
                    default, default, BotSpawnerStatus.Spawning);
        }

        void IEditableBotSpawnState.SetBotsCount(string spawnerId, int deadBots, int totalBots)
        {
            if (_data.TryGetValue(spawnerId, out BotSpawnerData data))
                _data[spawnerId] = new BotSpawnerData(spawnerId, data.WaveIndex, deadBots,
                    totalBots, data.WaitingStartTime, data.Status);
            else
                _data[spawnerId] = new BotSpawnerData(spawnerId, default, deadBots,
                    totalBots, default, BotSpawnerStatus.Spawning);
        }

        void IEditableBotSpawnState.SetAwaiting(string spawnerId, float waitingStartTime)
        {
            if (_data.TryGetValue(spawnerId, out BotSpawnerData data))
                _data[spawnerId] = new BotSpawnerData(spawnerId, data.WaveIndex, data.DeadBots,
                    data.TotalBots, waitingStartTime, BotSpawnerStatus.Awaiting);
            else
                _data[spawnerId] = new BotSpawnerData(spawnerId, default, default,
                    default, waitingStartTime, BotSpawnerStatus.Awaiting);
        }

        void IEditableBotSpawnState.SetFinished(string spawnerId, CompletionStatus status)
        {
            BotSpawnerStatus spawnerStatus = status switch
            {
                CompletionStatus.Success => BotSpawnerStatus.Completed,
                CompletionStatus.Cancelled => BotSpawnerStatus.Cancelled,
                _ => throw new ArgumentOutOfRangeException(nameof(status), status, null)
            };

            if (_data.TryGetValue(spawnerId, out BotSpawnerData data))
                _data[spawnerId] = new BotSpawnerData(spawnerId, data.WaveIndex, data.DeadBots,
                    data.TotalBots, data.WaitingStartTime,spawnerStatus);
            else
                _data[spawnerId] = new BotSpawnerData(spawnerId,
                    default, default, default, default, spawnerStatus);
        }
    }
}