﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(BotSpawnState))]
    [AddComponentMenu(BattleComponentMenus.Server.Systems.Menu + "/" + nameof(BotSpawnStateDataDrawer))]
    public sealed class BotSpawnStateDataDrawer : MonoBehaviour
    {
        [BoxGroup("Dependencies")]
        [SerializeField, Required, ReadOnly] private BotSpawnState _botSpawnState;

        [BoxGroup("Data")]
        [ShowInInspector, ReadOnly] private IReadOnlyDictionary<string, BotSpawnerData> Data =>
            _botSpawnState.Data;

        private void Awake() => InitDependencies();
        private void OnValidate() => InitDependencies();
        private void InitDependencies() => _botSpawnState ??= GetComponent<BotSpawnState>();
    }
}