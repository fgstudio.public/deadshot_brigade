﻿using Mirror;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    public enum CompletionStatus : byte
    {
        Success,
        Cancelled
    }

    public interface IEditableBotSpawnState : IReadOnlyBotSpawnState
    {
        [Server] void SetSpawning(string spawnerId, int waveIndex);
        [Server] void SetBotsCount(string spawnerId, int deadBots, int totalBots);
        [Server] void SetAwaiting(string spawnerId, float waitingStartTime);
        [Server] void SetFinished(string spawnerId, CompletionStatus status);
    }

    public interface IReadOnlyBotSpawnState
    {
        UnityEvent<string> DataUpdated { get; }
        IReadOnlyDictionary<string, BotSpawnerData> Data { get; }
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/59ba6d9bdc3c4504b5787e636ec174ec")]
    [AddComponentMenu(BattleComponentMenus.Server.Systems.Menu + "/" + nameof(BotSpawnState))]
    public sealed partial class BotSpawnState : NetworkBehaviour, IEditableBotSpawnState
    {
        [field: SerializeField] public UnityEvent<string> DataUpdated { get; private set; }

        private readonly SyncDictionary<string, BotSpawnerData> _data = new();

        public IReadOnlyDictionary<string, BotSpawnerData> Data => _data;

        private void Awake() => _data.Callback += OnDataUpdated;
        private void OnDestroy() => _data.Callback -= OnDataUpdated;

        public override void OnStartServer()
        {
            Initialize();
        }

        public override void OnStartClient()
        {
            if (isClientOnly)
                Initialize();
        }

        private void Initialize()
        {
            foreach (string id in _data.Keys)
                DataUpdated?.Invoke(id);
        }

        private void OnDataUpdated(SyncIDictionary<string, BotSpawnerData>.Operation op,
            string key, BotSpawnerData item) =>
            DataUpdated?.Invoke(key);
    }
}