﻿using System.Linq;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    // TODO: это должно быть данными в глобальном стэйте спауна
    public interface IBotsInWave
    {
        public bool IsWaveKilled { get; }
        public int DeadBotsCount { get; }
        public int AliveBotsCount { get; }
        public int TotalBotsCount { get; }
        public IReadOnlyList<UnitNetwork> Bots { get; }
    }

    public sealed class BotsInWave : IBotsInWave
    {
        private readonly List<UnitNetwork> _bots;

        public bool IsWaveKilled => DeadBotsCount == TotalBotsCount;
        public int AliveBotsCount => TotalBotsCount - DeadBotsCount;
        public int DeadBotsCount => Bots.Count(b => b == null || b.UnitNetworkState.IsDead);

        public int TotalBotsCount => Bots.Count;
        public bool IsWaveExist => Bots is { Count: > 0 };
        [NotNull, ItemCanBeNull] public IReadOnlyList<UnitNetwork> Bots => _bots;

        public BotsInWave() => _bots = new List<UnitNetwork>();

        public void Add(UnitNetwork bot) => _bots.Add(bot);
        public void AddRange(IEnumerable<UnitNetwork> bots) => _bots.AddRange(bots);
        public void Remove(UnitNetwork bot) => _bots.Remove(bot);
        public void Clear() => _bots.Clear();
    }
}
