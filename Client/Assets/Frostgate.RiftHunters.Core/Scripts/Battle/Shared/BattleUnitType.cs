using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public enum BattleUnitType
    {
        Player,
        Bot
    }

    public static class BattleUnitTypeHelper
    {
        public static string Player { get; } = BattleUnitType.Player.ToString();
        public static string Bot { get; } = BattleUnitType.Bot.ToString();

        public static BattleUnitType FromString(string type) =>
            (BattleUnitType) Enum.Parse(typeof(BattleUnitType), type);
    }
}
