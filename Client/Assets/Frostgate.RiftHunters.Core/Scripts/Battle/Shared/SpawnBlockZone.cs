﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [AddComponentMenu(BattleComponentMenus.Server.Systems.Menu + "/" + nameof(SpawnBlockZone))]
    [DisallowMultipleComponent]
    public sealed class SpawnBlockZone : MonoBehaviour
    {
        [SerializeField] private EntityType _blockedTypes;

        public bool IsBlocked(EntityType type) => _blockedTypes.HasFlag(type);
    }
}