using Mirror;
using Zenject;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Components;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface IMine : IPoolObject, IAutoReleased, IDestroyable, IGameObject, IDamageCaster, ISpawnable
    {
        UnityEvent Exploded { get; }
        new IMineState State { get; }
    }

    [DisallowMultipleComponent]
    [RequireComponent(typeof(NetworkIdentity))]
    [HelpURL("https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(Mine))]
    public sealed class Mine : MonoBehaviour, IMine, IImpactSource
    {
        private const string EventsFoldoutGroup = "Events";

        [Required, ChildGameObjectsOnly] public MineView View;
        [Required, ChildGameObjectsOnly] public MineState State;
        [Required, ChildGameObjectsOnly] public ImpactState ImpactState;
        [Required, ChildGameObjectsOnly] public NetworkIdentity Identity;
        [Required, ChildGameObjectsOnly] public SphereCollider Collider;
        [Required, ChildGameObjectsOnly] public ObjectCatcher Catcher;
        [Required, ChildGameObjectsOnly] public AutoDestroyComponent AutoDestroyOnExplodeComponent;
        [Required, ChildGameObjectsOnly] public AutoDestroyComponent AutoDestroyOnExpiredComponent;

        [field: SerializeField, FoldoutGroup(EventsFoldoutGroup)] public UnityEvent Exploded { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutGroup)] public UnityEvent Destroyed { get; private set; }

        [CanBeNull] private ILogger<Mine> _logger;

        public EntityType Type => EntityType.Mine;
        float IDamageCaster.AttackMultiplier => 1f;
        IDamageCasterState IDamageCaster.State => State;
        bool IDamageCaster.PiercingBullets => false;
        bool IDamageCaster.IgnoreEnemyDamageZone => true;

        IMineState IMine.State => State;
        NetworkIdentity INetworkIdentifiable.Identity => Identity;
        GameObject IGameObject.GameObject => this != null? gameObject : null;

        private UnityAction<IAutoReleased> _readyForReleaseAction;
        event UnityAction<IAutoReleased> IAutoReleased.ReadyForRelease
        {
            add => _readyForReleaseAction += value;
            remove => _readyForReleaseAction -= value;
        }

        void IPoolObject.OnGet() => ResetComponents();
        void IPoolObject.OnRelease() => Catcher.LoseAllObjects();

        [Inject]
        private void MonoConstructor(ILogger<Mine> logger) =>
            _logger = logger;

        private void OnValidate() => Init();
        private void Awake() => Init();

        private void Start()
        {
            ResetComponents();
            Subscribe();
        }

        private void OnDestroy()
        {
            Unsubscribe();
            Destroyed?.Invoke();
        }

        private void Init()
        {
            InitReferences();
            InitComponents();
        }

        private void InitReferences()
        {
            View ??= GetComponentInChildren<MineView>();
            State ??= GetComponentInChildren<MineState>();
            Identity ??= GetComponentInChildren<NetworkIdentity>();
            ImpactState ??= GetComponentInChildren<ImpactState>();
            Collider ??= GetComponentInChildren<SphereCollider>();
            Catcher ??= GetComponentInChildren<ObjectCatcher>();
        }

        private void InitComponents()
        {
            View.SetActive(true);
            AutoDestroyOnExplodeComponent?.Disable();
            AutoDestroyOnExpiredComponent?.Enable();

            Collider.radius = State.Config.DetectionRadius;
        }

        private void ResetComponents()
        {
            InitComponents();
            if (!NetworkClient.active) View.SetActive(false);
            if (NetworkServer.active) Catcher.gameObject.SetActive(true);
        }

        private void Subscribe()
        {
            ImpactState.CastStarted.AddListener(OnCasting);
            AutoDestroyOnExplodeComponent.DestroyComponent.Destroyed.AddListener(Destroy);
            AutoDestroyOnExpiredComponent.DestroyComponent.Destroyed.AddListener(Destroy);

            if (NetworkServer.active)
            {
                Catcher.Caught.AddListener(OnTargetDetected);
                Catcher.Objects.ForEach(OnTargetDetected);
            }
        }

        private void Unsubscribe()
        {
            if (NetworkServer.active)
            {
                Catcher.Caught.RemoveListener(OnTargetDetected);
            }

            ImpactState.CastStarted.RemoveListener(OnCasting);
            AutoDestroyOnExplodeComponent.DestroyComponent.Destroyed.RemoveListener(Destroy);
            AutoDestroyOnExpiredComponent.DestroyComponent.Destroyed.RemoveListener(Destroy);
        }

        private void OnCasting(string impactId) => OnExploded();

        [Server]
        private void OnTargetDetected([NotNull] Collider collider)
        {
            // TODO: получать из коллекции
            if (collider.TryGetComponentInHierarchy(out UnitNetwork unit))
            {
                string targetId = unit.UnitNetworkState.Id;
                _logger!.Log($"{targetId} Detected");
                Explode();
            }
        }

        [Server]
        private void Explode()
        {
            ImpactConfig impact = State.Config.Impact;
            _logger!.Log($"{impact.Id} Impact Applied");
            ImpactState.ExecuteImpactAsync(impact);
        }

        private void OnExploded()
        {
            View.SetActive(false);
            AutoDestroyOnExplodeComponent.Enable();
            AutoDestroyOnExplodeComponent.Disable();
            Catcher.gameObject.SetActive(false);
            Exploded?.Invoke();
        }

        [ContextMenu(nameof(Destroy))]
        public void Destroy() => InvokeReadyForRelease();
        private void InvokeReadyForRelease() => _readyForReleaseAction?.Invoke(this);
    }
}