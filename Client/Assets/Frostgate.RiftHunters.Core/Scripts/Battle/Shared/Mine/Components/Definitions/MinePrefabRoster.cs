using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface IMinePrefabRoster
    {
        IEnumerable<IMine> Prefabs { get; }
        IMine FindPrefab(Guid guid);
    }

    /// <summary>
    /// Ростер префабов для мин.
    /// <see cref="https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5"/>
    /// </summary>
    [CreateAssetMenu(
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(MinePrefabRoster),
        fileName = nameof(MinePrefabRoster))]
    [HelpURL("https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5")]
    public sealed partial class MinePrefabRoster : ScriptableObject, IMinePrefabRoster
    {
        [ValidateInput(nameof(HasElements), EmptyError)]
        [ValidateInput(nameof(ArePrefabsNotNull), NullPrefabsError)]
        [ValidateInput(nameof(AreAllTypesUnique), DuplicateTypesError)]
        [SerializeField] private Mine[] _prefabs;

        IEnumerable<IMine> IMinePrefabRoster.Prefabs => Prefabs;
        public IEnumerable<IMine> Prefabs => _prefabs;

        IMine IMinePrefabRoster.FindPrefab(Guid guid) => FindPrefab(guid);
        public Mine FindPrefab(Guid guid) => _prefabs.FirstOrDefault(l => l.Identity.assetId == guid);
    }
}