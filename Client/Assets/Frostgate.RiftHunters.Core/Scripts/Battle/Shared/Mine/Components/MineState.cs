using System;
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface IMineState
    {
        event Action<IMine> Initialized;
        event Action HostUnitIdChanged;

        IMine Mine { get; }
        bool IsInitialized { get; }
        uint HostUnitId { get; set; }
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(MineState))]
    public sealed class MineState : NetworkBehaviour, IMineState, IDamageReceiverState, IDamageCasterState
    {
        private const string EventsFoldoutGroup = "Events";

        // TODO: Это нужно сделать SyncVar и задавать в фабрике
        [field: SerializeField] public MineConfig Config { get; private set; }
        [field: SerializeField, Required] public Mine Mine { get; private set; }
        IMine IMineState.Mine => Mine;

        [SyncVar(hook = nameof(OnHostUnitIdChanged))] private uint _hostUnitId;

        [SerializeField, FoldoutGroup(EventsFoldoutGroup)] private UnityEvent _hostUnitIdChanged;

        public uint HostUnitId
        {
            get => _hostUnitId;
            set => NetworkHelper.ModifySyncVar(_hostUnitId, value, v => _hostUnitId = v, OnHostUnitIdChanged);
        }

        public event Action HostUnitIdChanged;
        public event Action<IMine> Initialized;
        event IReadOnlyHealth.Changed IReadOnlyHealth.HealthChanged
        {
            add { }
            remove { }
        }

        float IReadOnlyHealth.Health => default;
        float IHealth.Health { get; set; }
        float IReadOnlyHealth.MaxHealth => default;
        bool IReadOnlyHealth.IsDead => false;
        float IDamageReceiverState.Defense => default;
        float IDamageReceiverState.BodyRadius => default;
        bool IDamageReceiverState.IsInvincible => false;
        public Vector3 Position => transform.position;
        public bool IsInitialized { get; private set; }

        public override void OnStartClient() => SetInitialized(true);
        public override void OnStartServer() => SetInitialized(true);
        public override void OnStopClient() => SetInitialized(false);
        public override void OnStopServer() => SetInitialized(false);

        private void SetInitialized(bool status)
        {
            if (IsInitialized != status)
            {
                IsInitialized = status;
                if (status) Initialized?.Invoke(Mine);
            }
        }

        private void OnHostUnitIdChanged(uint oldValue, uint newValue)
        {
            if (oldValue != newValue)
                InvokeHostUnitIdChanged();
        }

        private void InvokeHostUnitIdChanged()
        {
            HostUnitIdChanged?.Invoke();
            _hostUnitIdChanged?.Invoke();
        }
    }
}