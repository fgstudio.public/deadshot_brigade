using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// КО: Коллекция мин.
    /// <see cref="https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5"/>
    /// </summary>
    [UsedImplicitly]
    public sealed class MineNetIdCollection : ObjectBaseCollection<uint, IMine>
    {
        public MineNetIdCollection([NotNull] ILogger<MineNetIdCollection> logger)
            : base(logger) { }

        protected override string ExtractName(IMine mine) =>
            (mine.Identity != null ? mine.Identity.netId : default).ToString();
    }
}