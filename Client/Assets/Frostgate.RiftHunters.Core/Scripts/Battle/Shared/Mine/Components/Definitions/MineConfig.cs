using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Impact;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [HelpURL("https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5")]
    [CreateAssetMenu(fileName = nameof(MineConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(MineConfig))]
    public sealed class MineConfig : ScriptableObject
    {
        [field: SerializeField, MinValue(0), SuffixLabel("units", true)]
        public float DetectionRadius { get; private set; }

        [field: SerializeField, Required]
        public ImpactConfig Impact { get; private set; }
    }
}