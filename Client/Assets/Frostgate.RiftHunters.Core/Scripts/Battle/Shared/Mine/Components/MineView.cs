using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5")]
    [AddComponentMenu(BattleComponentMenus.Client.Menu + "/" + nameof(MineView))]
    public sealed class MineView : MonoBehaviour
    {
        [Required, ChildGameObjectsOnly] public GameObject Model;
        [Required, ChildGameObjectsOnly] public GameObject DestroyVfx;

        public void SetActive(bool activity)
        {
            Model.SetActive(activity);
            DestroyVfx.SetActive(!activity);
        }
    }
}