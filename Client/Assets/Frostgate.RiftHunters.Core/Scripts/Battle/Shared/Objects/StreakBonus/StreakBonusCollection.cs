using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0"/>
    /// </summary>
    public sealed class StreakBonusCollection : ObjectBaseCollection<uint, ISharedStreakBonus>
    {
        public StreakBonusCollection([NotNull] ILogger logger) : base(logger) { }

        protected override string ExtractName(ISharedStreakBonus bonus) =>
            nameof(StreakBonus) + " " + bonus.Identity.netId;
    }
}
