using Mirror;
using Zenject;
using Sirenix.OdinInspector;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus
{
    public enum StreakBonusStatus : byte
    {
        None,
        Active,
        Collected,
        Expired
    }

    public interface IReadOnlyStreakBonusState
    {
        ISharedStreakBonus CoreObject { get; }

        string ConfigId { get; }
        uint SourceId { get; }
        uint CollectorId { get; }

        StreakBonusStatus Status { get; }
        UnityEvent<IReadOnlyStreakBonusState> StatusChanged { get; }

        bool IsInitialized { get; }
        UniTask InitializationTask { get; }
        UnityEvent<IReadOnlyStreakBonusState> Initialized { get; }
    }

    public interface IEditableStreakBonusState : IReadOnlyStreakBonusState
    {
        [Server] void Init(uint sourceId, string configId);
        [Server] void SetExpired();
        [Server] void SetCollected(uint collectorId);
    }

    [DisallowMultipleComponent]
    [RequireComponent(typeof(StreakBonusCoreObject))]
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [AddComponentMenu(StreakBonusComponentMenu.Shared + "/" + nameof(StreakBonusState))]
    public sealed class StreakBonusState : NetworkBehaviour, IEditableStreakBonusState
    {
        [Inject] private readonly ILogger<StreakBonusState> _logger;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private StreakBonusCoreObject _coreObject;

        [SyncVar, SerializeField] private string _configId;
        [SyncVar, SerializeField] private uint _sourceId;
        [SyncVar, SerializeField] private uint _collectorId;

        [SyncVar(hook = nameof(StatusHook))]
        [SerializeField] private StreakBonusStatus _status;

        [field: SerializeField]
        public UnityEvent<IReadOnlyStreakBonusState> Initialized { get; private set; }

        [field: SerializeField]
        public UnityEvent<IReadOnlyStreakBonusState> StatusChanged { get; private set; }

        public uint SourceId => _sourceId;
        public string ConfigId => _configId;
        public uint CollectorId => _collectorId;
        public StreakBonusStatus Status => _status;
        public ISharedStreakBonus CoreObject => _coreObject;
        public bool IsInitialized { get; private set; }

        public UniTask InitializationTask => IsInitialized
            ? UniTask.CompletedTask
            : UniTask.WaitUntil(() => IsInitialized)
                .AttachExternalCancellation(this.GetCancellationTokenOnDestroy());


        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _coreObject ??= GetComponent<StreakBonusCoreObject>();

        public override void OnStartClient()
        {
            if (isClientOnly)
                SetInitialized(true);
        }

        public override void OnStopClient() => SetInitialized(false);
        public override void OnStopServer() => SetInitialized(false);

        [Server]
        public void Init(uint sourceId, string configId)
        {
            _sourceId = sourceId;
            _configId = configId;
            SetStatus(StreakBonusStatus.Active);

            SetInitialized(true);
        }

        private void SetInitialized(bool status)
        {
            if (IsInitialized != status)
            {
                IsInitialized = status;

                _logger.Log(status
                    ? $"Initialized [netId:{netId}]"
                    : $"Deinitialized [netId:{netId}]");

                if (status)
                    Initialized?.Invoke(this);
            }
        }


        [Server]
        public void SetExpired()
        {
            _logger.Log($"Expired [netId:{netId}]");
            SetStatus(StreakBonusStatus.Expired);
        }

        [Server]
        public void SetCollected(uint collectorId)
        {
            _logger.Log($"Collected [netId:{netId}] by [netId:{collectorId}]");

            _collectorId = collectorId;
            SetStatus(StreakBonusStatus.Collected);
        }

        private void SetStatus(StreakBonusStatus status)
        {
            NetworkHelper.ModifySyncEnum(
                _status, status, v => _status = v, StatusHook);
        }

        private void StatusHook(StreakBonusStatus oldValue, StreakBonusStatus newValue)
        {
            if (oldValue != newValue)
                StatusChanged?.Invoke(this);
        }
    }
}