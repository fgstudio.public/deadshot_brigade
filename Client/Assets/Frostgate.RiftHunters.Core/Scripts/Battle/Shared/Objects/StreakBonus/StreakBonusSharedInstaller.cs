using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus
{
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [CreateAssetMenu(fileName = nameof(StreakBonusSharedInstaller),
        menuName = StreakBonusAssetMenu.Shared + "/" + nameof(StreakBonusSharedInstaller))]
    public sealed class StreakBonusSharedInstaller : ScriptableObjectInstaller
    {
        [Required, AssetSelector]
        [SerializeField] private StreakBonusMechanicSettings _mechanicSettings;

        [Required, AssetSelector]
        [SerializeField] private StreakBonusCoreObject _coreObjectPrefab;

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<StreakBonusCollection>().AsSingle();
            Container.Bind<StreakBonusMechanicSettings>().FromInstance(_mechanicSettings).AsSingle();
            Container.Bind<StreakBonusCoreObject>().FromInstance(_coreObjectPrefab).AsSingle();
        }
    }
}
