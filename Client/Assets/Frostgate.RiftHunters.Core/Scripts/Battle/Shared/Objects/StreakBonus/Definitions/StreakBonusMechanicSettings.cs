using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus
{
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [CreateAssetMenu(fileName = nameof(StreakBonusMechanicSettings),
        menuName = StreakBonusAssetMenu.Shared + "/" + nameof(StreakBonusMechanicSettings))]
    public sealed class StreakBonusMechanicSettings : ScriptableObject
    {
        private const string DropSettingsGroup = "Drop Settings";

        [BoxGroup(DropSettingsGroup), MinValue(0), SuffixLabel("units", true)]
        [SerializeField] private float _dropRadius;

        [BoxGroup(DropSettingsGroup), MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _dropDuration;

        [MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _lifeTime;

        public float DropRadius => _dropRadius;
        public TimeSpan DropDuration => TimeSpan.FromSeconds(_dropDuration);
        public TimeSpan LifeTime => TimeSpan.FromSeconds(_lifeTime);
    }
}
