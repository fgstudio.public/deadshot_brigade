using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus
{
    public enum StreakBonusCoverage
    {
        OnlyCollector,
        AllPlayers
    }

    public enum BonusType
    {
        InfiniteAmmo
    }

    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [CreateAssetMenu(fileName = nameof(StreakBonusConfig),
        menuName = StreakBonusAssetMenu.Shared + "/" + nameof(StreakBonusConfig))]
    public sealed class StreakBonusConfig : ScriptableConfig
    {
        [SerializeField] private BonusType _bonusType;
        [SerializeField] private StreakBonusCoverage _coverage;
        [SerializeField, Required] private UnitEffectConfig[] _effects;

        public BonusType BonusType => _bonusType;
        public StreakBonusCoverage Coverage => _coverage;
        public IReadOnlyList<UnitEffectConfig> Effects => _effects;
    }
}