namespace Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus
{
    public static class StreakBonusAssetMenu
    {
        public const string Client = BattleAssetMenus.Client.Menu + "/" + nameof(StreakBonus);
        public const string Server = BattleAssetMenus.Server.Menu + "/" + nameof(StreakBonus);
        public const string Shared = BattleAssetMenus.Shared.Menu + "/" + nameof(StreakBonus);
    }
}