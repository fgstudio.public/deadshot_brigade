using Mirror;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus
{
    public interface ISharedStreakBonus : INetworkIdentifiable, IGameObject
    {
        IReadOnlyStreakBonusState State { get; }
    }

    [DisallowMultipleComponent]
    [RequireComponent(typeof(NetworkIdentity))]
    [RequireComponent(typeof(StreakBonusState))]
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [AddComponentMenu(StreakBonusComponentMenu.Shared + "/" + nameof(StreakBonusCoreObject))]
    public sealed class StreakBonusCoreObject : MonoBehaviour, ISharedStreakBonus
    {
        [Required, ChildGameObjectsOnly]
        [SerializeField] private StreakBonusState _state;

        [Required, ChildGameObjectsOnly]
        [SerializeField] private NetworkIdentity _identity;

        IReadOnlyStreakBonusState ISharedStreakBonus.State => _state;
        GameObject IGameObject.GameObject => this != null? gameObject : null;

        public StreakBonusState State => _state;
        public NetworkIdentity Identity => _identity;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _state ??= GetComponent<StreakBonusState>();
            _identity ??= GetComponent<NetworkIdentity>();
        }
    }
}
