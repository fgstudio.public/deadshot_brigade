namespace Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus
{
    public static class StreakBonusComponentMenu
    {
        public const string Client = BattleComponentMenus.Client.Menu + "/" + nameof(StreakBonus);
        public const string Server = BattleComponentMenus.Server.Menu + "/" + nameof(StreakBonus);
        public const string Shared = BattleComponentMenus.Shared.Menu + "/" + nameof(StreakBonus);
    }
}