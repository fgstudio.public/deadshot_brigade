using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Energy;
using Frostgate.RiftHunters.Core.Battle.Shared.Catching;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// Компонент юнита для подбора энергетических капсул.
    /// <see cref="UnitNetwork"/>
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(ObjectCatcher))]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(UnitCollectEnergyCapsule))]
    public sealed class UnitCollectEnergyCapsule : MonoBehaviour, IEnabling
    {
        [Header("References")]
        [SerializeField, Required] private UnitNetwork _unit;
        [SerializeField, Required, ChildGameObjectsOnly] private ObjectCatcher _catcher;

        [Header("Info")]
        [ShowInInspector] private ICollectEnergyCapsuleService _collectEnergyCapsuleService;

        public bool IsEnabled => gameObject.activeInHierarchy;

        [Inject]
        private void MonoConstructor(IServiceRoster serviceRoster) =>
            _collectEnergyCapsuleService = serviceRoster.CollectEnergyCapsuleService;

        private void Awake() => InitReferences();
        private void Start() => Subscribe(_catcher);
        private void OnDestroy() => Unsubscribe(_catcher);
        private void OnDisable() => _catcher.LoseAllObjects();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _unit ??= this.GetComponentInHierarchy<UnitNetwork>();
            _catcher ??= GetComponentInChildren<ObjectCatcher>();
        }

        public void Enable() => gameObject.SetActive(true);
        public void Disable() => gameObject.SetActive(false);

        private void Subscribe([NotNull] ObjectCatcher catcher) => catcher.Caught.AddListener(OnCollected);
        private void Unsubscribe([NotNull] ObjectCatcher catcher) => catcher.Caught.RemoveListener(OnCollected);

        private void OnCollected(Collider collected)
        {
            if (_unit == null) return;
            if (!_unit.Identity.isServer) return;
            if (!_unit.CanCollectEnergy) return;
            if (_unit.UnitNetworkState.UnitType != BattleUnitType.Player) return;
            // TODO: получать из коллекции
            if (!collected.TryGetComponentInHierarchy(out EnergyCapsule energyCapsule)) return;

            if (!energyCapsule.State.IsCollected)
                _collectEnergyCapsuleService?.Collect(_unit, energyCapsule);
        }
    }
}