﻿using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Respawn
{
    [Serializable]
    public readonly struct RespawnData
    {
        public readonly RespawnStatus Status;
        public readonly double ServerEventTime;
        public readonly double IntervalTime;

        public RespawnData(RespawnStatus status, double serverEventTime, double intervalTime)
        {
            Status = status;
            ServerEventTime = serverEventTime;
            IntervalTime = intervalTime;
        }

        public override string ToString()
        {
            return $"Status: {Status}, ServerTime: {ServerEventTime}, TimerTime: {IntervalTime}";
        }
    }

    public enum RespawnStatus
    {
        Respawning,
        Paused,
        Done
    }
}