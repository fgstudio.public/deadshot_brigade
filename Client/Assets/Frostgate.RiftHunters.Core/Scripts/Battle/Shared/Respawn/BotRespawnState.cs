﻿using System;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using static Mirror.SyncIDictionary<uint,Frostgate.RiftHunters.Core.Battle.Shared.Respawn.RespawnData>;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Respawn
{
    public interface IReadOnlyBotRespawnState
    {
        UnityEvent Initialized { get; }
        UnityEvent<uint, RespawnData> Changed { get; }

        bool IsInitialized { get; }
        IReadOnlyDictionary<uint, RespawnData> RespawnUnitsData { get; }
    }

    [DisallowMultipleComponent]
    public sealed class BotRespawnState : NetworkBehaviour, IReadOnlyBotRespawnState
    {
        [field: SerializeField] public UnityEvent<uint, RespawnData> Changed { get; private set; }
        [field: SerializeField] public UnityEvent Initialized { get; private set; }

        public readonly SyncDictionary<uint, RespawnData> RespawnUnits = new();
        public IReadOnlyDictionary<uint, RespawnData> RespawnUnitsData => RespawnUnits;

        public bool IsInitialized { get; private set; }

        public override void OnStartServer()
        {
            Initialize();
        }

        public override void OnStartClient()
        {
            if (isClientOnly)
                Initialize();
        }

        private void Initialize()
        {
            RespawnUnits.Callback += OnChangedCallback;

            foreach (KeyValuePair<uint, RespawnData> kvp in RespawnUnits)
                OnChangedCallback(Operation.OP_ADD, kvp.Key, kvp.Value);

            IsInitialized = true;
            Initialized?.Invoke();
        }

        private void OnChangedCallback(Operation op, uint key, RespawnData item)
        {
            switch (op)
            {
                case Operation.OP_ADD:
                case Operation.OP_SET:
                    Changed?.Invoke(key, item);
                    break;
                case Operation.OP_CLEAR:
                case Operation.OP_REMOVE:
                    Changed?.Invoke(key,
                        new RespawnData(RespawnStatus.Done, item.ServerEventTime, 0));
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(op), op, null);
            }
        }
    }
}