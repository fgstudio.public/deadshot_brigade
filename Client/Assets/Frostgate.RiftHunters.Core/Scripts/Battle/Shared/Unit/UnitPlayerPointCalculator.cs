using Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public sealed class UnitPlayerPointCalculator
    {
        private readonly IRandom _random;
        private readonly AreaRandom _areaRandom;
        private readonly IReadOnlyActualRespawnPoint _respawnPoint;

        private static ILogger _logger = LoggerFactory.CreateLogger<UnitPlayerPointCalculator>();

        public UnitPlayerPointCalculator([NotNull] AreaRandom areaRandom, [NotNull] IReadOnlyActualRespawnPoint respawnPoint)
        {
            _areaRandom = areaRandom;
            _respawnPoint = respawnPoint;
        }

        public PointData CalcForCheckpoint()
        {
            Checkpoint checkpoint = _respawnPoint.GetRespawnPoint;
            Transform transform = checkpoint.gameObject.transform;
            Bounds bounds = checkpoint.Bounds;

            Vector3 playerPosition = BoundPlayerPosition(transform.position, bounds);
            Vector3 playerRotation = transform.rotation.eulerAngles;

            return new PointData(playerPosition, playerRotation);
        }

        private Vector3 BoundPlayerPosition(Vector3 checkpointPosition, Bounds bounds)
        {
            Vector3 playerPosition = checkpointPosition;
            int maxTry = 50;
            int currentTry = 0;

            //todo: возможно следует добавить более предметные проверки
            while (currentTry <= maxTry)
            {
                currentTry++;
                playerPosition = _areaRandom.RectY(checkpointPosition, bounds.size.x, bounds.size.z);
                if (PositionHelper.IsValidToSpawn(playerPosition))
                    break;
            }

            if (currentTry >= maxTry)
            {
                playerPosition = checkpointPosition;
                _logger.LogError("Failed to find the starting position. Perhaps the navmesh was not " +
                                 "baked or wrong checkpoint position.");
            }

            return playerPosition;
        }
    }
}