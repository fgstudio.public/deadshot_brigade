using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Reanimation;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(UnitNetwork))]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(UnitInstaller))]
    public sealed class UnitInstaller : MonoBehaviour, IResettable
    {
        [SerializeField, Required, ReadOnly] private UnitNetwork _unitNetwork;

        [Inject]
        private void MonoConstructor(ReanimationSettings reanimationSettings)
        {
            ReanimationMechanicComponent reanimationMechanic = _unitNetwork.ReanimationMechanic;

            reanimationMechanic.SetIncreaseProgressSpeed(reanimationSettings.IncreaseProgressSpeedPerUnit);
            reanimationMechanic.SetDecreaseProgressSpeed(reanimationSettings.DecreaseProgressSpeed);
            reanimationMechanic.SetAreaRadius(reanimationSettings.AreaRadius);
        }

        private void OnValidate()
        {
            InitReferences();
            InitComponents();
        }

        private void Awake()
        {
            InitReferences();
            InitComponents();
        }

        public void Reset()
        {
            InitReferences();
            InitComponents();
        }

        private void InitReferences() =>
            _unitNetwork ??= GetComponent<UnitNetwork>();

        private void InitComponents()
        {
            _unitNetwork.UnitCollectLootBox.Enable();
            _unitNetwork.UnitCollectEnergyCapsule.Enable();
            _unitNetwork.ReanimationMechanic.Disable();
        }
    }
}