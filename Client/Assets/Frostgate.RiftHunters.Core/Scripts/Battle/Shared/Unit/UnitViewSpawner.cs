using Mirror;
using Zenject;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Battle.Client;
using Frostgate.RiftHunters.Core.Battle.Client.FX;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Client.Audio;
using Frostgate.RiftHunters.Core.Systems.Input;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public class UnitViewSpawner : NetworkBehaviour
    {
        [SerializeField] private UnitNetwork _unit;
        [SerializeField] private Transform _viewContainer;

        public UnitView View { get; private set; }

        private IFxRoster _fxRoster;
        private BattleCamera _battleCamera;
        private IUIBattleMediator _uiBattleMediator;

        private IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollections;
        private PlayerInput _input;

        private IUIService _uiService;
        private AudioService _audioService;

        [Inject]
        private void MonoConstructor([InjectOptional] IFxRoster fxRoster,
            [InjectOptional] BattleCamera battleCamera, IUIBattleMediator uiBattleMediator,
            IReadOnlyObjectCollection<uint, UnitNetwork> unitNetworks, PlayerInput input,
            IUIService uiService, AudioService audioService)
        {
            _fxRoster = fxRoster;
            _battleCamera = battleCamera;
            _uiBattleMediator = uiBattleMediator;
            _unitCollections = unitNetworks;
            _input = input;
            _uiService = uiService;
            _audioService = audioService;
        }

        [Client]
        public void Init()
        {
            // TODO: вынести это в LocalObserver на клиенте
            if (isLocalPlayer)
            {
                _battleCamera.StartFollowing(_viewContainer);

                _uiBattleMediator.EnableGameplayMode(_unit);
                _uiBattleMediator.Hud.SpecialProgressBars.SetObservableUnit(_unit.UnitNetworkEffects, _unit.UnitNetworkState);

                _uiService.Hide();
            }

            SpawnUnitView(_uiBattleMediator);
            Subscribe();
        }

        [Client]
        public void Deinit()
        {
            if (isLocalPlayer)
            {
                _battleCamera.StopFollowing();
                _uiBattleMediator.Hud.SpecialProgressBars.RemoveObservableUnit();
            }

            UnSubscribe();

            if (View != null)
                UnspawnUnitView(View);
        }

        private void Subscribe()
        {
            _unit.UnitNetworkState.UnitConfigChanged += OnUnitConfigChanged;
            _unit.UnitNetworkState.HealthChanged += OnUnitHealthChanged;
            _unit.UnitNetworkState.EnergyChanged += OnUnitEnergyChanged;
            _unit.UnitNetworkState.StaminaChanged += OnUnitStaminaChanged;
        }

        private void UnSubscribe()
        {
            _unit.UnitNetworkState.UnitConfigChanged -= OnUnitConfigChanged;
            _unit.UnitNetworkState.HealthChanged -= OnUnitHealthChanged;
            _unit.UnitNetworkState.EnergyChanged -= OnUnitEnergyChanged;
            _unit.UnitNetworkState.StaminaChanged -= OnUnitStaminaChanged;
        }

        private void OnUnitConfigChanged(UnitConfig _, UnitConfig __) => SpawnUnitView(_uiBattleMediator);
        private void OnUnitHealthChanged(float _) => _unit.View?.HealthBar.SetCurrent(_unit.UnitNetworkState.Health);
        private void OnUnitEnergyChanged(float _) => _unit.View?.EnergyBar.SetCurrent(_unit.UnitNetworkState.Energy);
        private void OnUnitStaminaChanged() => _unit.View?.StaminaBar?.SetCurrent(_unit.UnitNetworkState.Stamina);

        private void SpawnUnitView(IUIBattleMediator uiBattleMediator)
        {
            if (_unit.UnitNetworkState.UnitConfig == null)
                return;

            if (View != null)
                UnspawnUnitView(View);

            View = Instantiate(_unit.UnitNetworkState.UnitConfig.View, _viewContainer);
            View.Init(_unit, _fxRoster, uiBattleMediator, _unitCollections, _input, _audioService);
        }

        private void UnspawnUnitView([NotNull] UnitView view) =>
            Destroy(view.gameObject);
    }
}