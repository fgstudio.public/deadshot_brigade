using Mirror;
using JetBrains.Annotations;
using System;
using System.Linq;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public sealed class PlayersDieObserver : IDisposable
    {
        public event Action AllPlayersDied;

        [NotNull] private readonly HashSet<UnitNetwork> _players = new();
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;

        public bool AreAllPlayersDead => _players
            .Select(p => p.UnitNetworkState.BehaviourState)
            .All(s => s == BehaviourState.Dead);

        private bool _lastConditionValue;

        public PlayersDieObserver([NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _unitCollection = unitCollection ?? throw new ArgumentNullException(nameof(unitCollection));
            SubscribeCollection(unitCollection);
        }

        public void Dispose() =>
            UnsubscribeCollection(_unitCollection);

        private void SubscribeCollection([NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            unitCollection.Added += OnUnitAdded;
            unitCollection.Removed += OnUnitRemoved;

            foreach (UnitNetwork unit in unitCollection)
                OnUnitAdded(unit);
        }

        private void UnsubscribeCollection([NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            unitCollection.Added -= OnUnitAdded;
            unitCollection.Removed -= OnUnitRemoved;

            foreach (UnitNetwork unit in unitCollection)
                OnUnitRemoved(unit);
        }

        private void OnUnitAdded([NotNull] UnitNetwork unit)
        {
            if (unit.UnitType == BattleUnitType.Player)
            {
                _players.Add(unit);
                SubscribePlayer(unit);
            }
        }

        private void OnUnitRemoved([NotNull] UnitNetwork unit)
        {
            if (unit.UnitType == BattleUnitType.Player)
            {
                _players.Remove(unit);
                UnsubscribePlayer(unit);

                UpdateAllDiedCondition();
            }
        }

        private void SubscribePlayer([NotNull] UnitNetwork unit) =>
            unit.UnitNetworkState.BehaviourStateChanged += UpdateAllDiedCondition;

        private void UnsubscribePlayer([NotNull] UnitNetwork unit) =>
            unit.UnitNetworkState.BehaviourStateChanged -= UpdateAllDiedCondition;

        private void UpdateAllDiedCondition()
        {
            bool areAllPlayersDead = AreAllPlayersDead;

            if (!_lastConditionValue && areAllPlayersDead)
                AllPlayersDied?.Invoke();

            _lastConditionValue = areAllPlayersDead;
        }
    }
}