using System;
using System.Linq;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Client;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect;
using Frostgate.RiftHunters.Core.Battle.Server.UnitAI;
using Frostgate.RiftHunters.Core.Battle.Shared.Ai;
using Frostgate.RiftHunters.Core.Battle.Shared.Exp;
using Frostgate.RiftHunters.Core.Battle.Shared.Energy;
using Frostgate.RiftHunters.Core.Battle.Shared.Healing;
using Frostgate.RiftHunters.Core.Battle.Shared.Stamina;
using Frostgate.RiftHunters.Core.Battle.Shared.Looting;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Reanimation;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.Invincibility;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Reanimation;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Scripts.Battle.Shared.Unit.Network;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using Mirror;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using Zenject;
using BehaviourState = Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM.BehaviourState;
using Random = UnityEngine.Random;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public class UnitNetwork : NetworkBehaviour, IAimTarget, IShooter, IRespawnable,
        IExpReceiver, IExpSource,
        IDamageCaster, IReanimated,
        IHpAutoRecoverable, IEnergyAutoRecoverable,
        ILootBoxSource, ILootBoxCollector,
        IEnergyCapsuleSource, IAiUnit, IAiTarget, IInvincible,
        ICaptured, IImpactSource, IImpactTarget,
        IStaminaAutoRecoverable, IPoolObject
    {
        public event Action OnCaughtArea;

        [field: SerializeField, Required, ChildGameObjectsOnly] public NetworkIdentity NetworkIdentity { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public UnitNetworkState UnitNetworkState { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public ImpactState ImpactState { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public UnitNetworkEffects UnitNetworkEffects { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public UnitActionState UnitActionState { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public UnitNetworkInput UnitNetworkInput { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public UnitCollectLootBox UnitCollectLootBox { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public UnitCollectEnergyCapsule UnitCollectEnergyCapsule { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public ReanimationMechanicComponent ReanimationMechanic { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public UnitMove UnitMove { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public Rigidbody Rigidbody { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public UnitViewSpawner ViewSpawner { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public UnitNetworkEmotion Emotion { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public LinkSpawnerView LinkSpawnerView { get; private set; }

        public UnitEmotionRoster UnitEmotionRoster => _unitEmotionRoster;
        [SerializeField, Required] private UnitEmotionRoster _unitEmotionRoster;

        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Destroyed { get; private set; }

        [CanBeNull] public UnitView View => ViewSpawner.View;
        public BattleUnitType UnitType => UnitNetworkState.UnitType;
        public bool CanCollectEnergy => !IsDestroyedOrDead && UnitType == BattleUnitType.Player && !UnitNetworkEffects.Params.DisableEnergyAccumulation;
        public bool DamageImmunity => UnitActionState.DamageImmunity;

        public Collider AimTarget => UnitNetworkState.ViewConfig != null
            ? UnitNetworkState.ViewConfig.AimTarget : null;
        public Collider Body => UnitNetworkState.ViewConfig != null
            ? UnitNetworkState.ViewConfig.Body : null;
        public float BodyRadius => UnitNetworkState.ViewConfig != null
            ? UnitNetworkState.ViewConfig.Body.radius : 0f;
        public Vector3 FocusPoint => UnitNetworkState.ViewConfig != null
            ? UnitNetworkState.ViewConfig.FocusPoint : Vector3.zero;
        public Vector3 ShootPoint => UnitNetworkState.ViewConfig != null
            ? UnitNetworkState.ViewConfig.ShootPoint.transform.position : Vector3.zero;
        public bool IsDestroyedOrDead => this == null || UnitNetworkState.IsDead;
        public bool IsPriorityTarget => false;
        public bool CapturedByDirectAiming => false;
        public Vector3 ForwardAxis => transform.forward;
        public NetworkIdentity Identity => NetworkIdentity;
        public ImpactModelRoster ImpactRoster { get; private set; }

        [CanBeNull] public IWeapon CurrentWeapon =>
            UnitNetworkState.BehaviourState == BehaviourState.MeleeAttack ? MeleeWeapon : RangeWeapon;

        [CanBeNull] public RangeWeaponConfig RangeWeapon => UnitNetworkState.PropertiesProvider?.RangeWeapon;
        [CanBeNull] public IMeleeWeapon MeleeWeapon => UnitNetworkState.PropertiesProvider?.MeleeWeapon;
        public Transform Transform => transform;
        public EnergyCapsuleConfig EnergyCapsuleForDeath => UnitNetworkState.UnitConfig.DeathRewards.EnergyCapsule;
        public float AttackMultiplier => UnitNetworkEffects.Params.AttackMultiplier;
        public bool PiercingBullets => UnitNetworkEffects.Params.PiercingBullets;
        public bool IgnoreEnemyDamageZone => UnitNetworkEffects.Params.IgnoreEnemyDamageZone;
        public ImpactTargetType ImpactTargetType => ImpactTargetType.Unit;
        IExpReceiverState IExpReceiver.State => UnitNetworkState;
        [CanBeNull] CapsuleCollider IAiTarget.AimTarget => UnitNetworkState.ViewConfig != null
            ? UnitNetworkState.ViewConfig.AimTarget : null;
        int IExpSource.ExpReward => UnitNetworkState.UnitConfig.DeathRewards.Exp;
        Transform IFxHolder.FxContainer => View?.FxContainer;
        Transform IFxHolder.WorldFxContainer => View?.WorldFxContainer;
        IAimTargetView IAimTarget.View => View;
        IDamageReceiverState IDamageReceiver.State => UnitNetworkState;
        IDamageCasterState IDamageCaster.State => UnitNetworkState;
        IInvincibleState IInvincible.State => UnitNetworkState;
        Vector3 IPositioned.Position => transform != null ? transform.position : Vector3.zero;

        IHpAutoRecoveryConfig IHpAutoRecoverable.Config =>
            UnitNetworkEffects.Params.HpAutoRecoveryConfig ?? UnitNetworkState.PropertiesProvider?.HpAutoRecovery;
        IEnergyAutoRecoveryConfig IEnergyAutoRecoverable.Config =>
            UnitNetworkEffects.Params.EnergyAutoRecoveryConfig ?? UnitNetworkState.PropertiesProvider?.EnergyAutoRecovery;
        IStaminaAutoRecoveryConfig IStaminaAutoRecoverable.Config =>
            UnitNetworkEffects.Params.StaminaAutoRecoveryConfig ?? UnitNetworkState.PropertiesProvider?.StaminaAutoRecovery;

        IHealingState IReanimated.State => UnitNetworkState;
        IHealingState IHealingReceiver.State => UnitNetworkState;
        IHavingStaminaState IStaminaOwner.State => UnitNetworkState;
        IRespawnableState IRespawnable.State => UnitNetworkState;
        ICapturedState ICaptured.State => UnitNetworkState;
        GameObject IGameObject.GameObject => this != null? gameObject : null;
        ILootBoxCollectorState ILootBoxCollector.State => UnitNetworkState;
        IEnergyReceiverState IEnergyReceiver.State => UnitNetworkState;
        float IEnergyAutoRecoverable.RecoveryMultiplier =>
            UnitNetworkEffects.Params.DisableEnergyAccumulation ? 0f : UnitNetworkEffects.Params.EnergyAccumulationMultiplier;

        public LootSetConfig LootSetForDeath => UnitNetworkState.UnitConfig.DeathRewards.LootSet;

        public DamageZonesConfig DamageZonesConfig => UnitNetworkEffects.Params.DamageZonesConfig != null
            ? UnitNetworkEffects.Params.DamageZonesConfig
            : UnitNetworkState.UnitConfig.ViewConfig.DamageZones;

        public IShootingSharedLogic ShootingSharedLogic => _shootingSharedLogic;
        [CanBeNull] public UnitRangeAttackCaster UnitRangeAttackCaster { get; private set; }

        public bool CanMove => !IsDestroyedOrDead &&
                               !ShootingSharedLogic.NeedFreeze &&
                               CurrentBehaviourStateAllowMove &&
                               CurrentUnitActionType == UnitActionTypes.None &&

                               ImpactRoster.AllImpacts.All(i =>
                                   !i.IsCasting ||
                                   i.Data.Config == null ||
                                   i.Data.Config.CanMove);

        public bool CanRangeAttack => !IsDestroyedOrDead && CurrentBehaviourState == BehaviourState.None &&
                                      CurrentUnitActionType == UnitActionTypes.None && RangeWeapon != null &&
                                      (!RangeWeapon.UsePatrons || UnitNetworkState.PatronsCount > 0 || UnitNetworkEffects.Params.InfiniteAmmo) &&
                                      ImpactRoster.AllImpacts.All(i => !i.IsCasting || i.Data.Config == null);

        public bool CanMeleeAttack => !IsDestroyedOrDead && CurrentBehaviourState == BehaviourState.None &&
                                      CurrentUnitActionType == UnitActionTypes.None &&
                                      ImpactRoster.AllImpacts.All(i => !i.IsCasting || i.Data.Config == null);

        public bool CanBeginReloading => !IsDestroyedOrDead && CurrentBehaviourState == BehaviourState.None &&
                                         CurrentUnitActionType == UnitActionTypes.None &&
                                         UnitNetworkState.PatronsCount != UnitNetworkState.MaxPatrons &&
                                         ImpactRoster.AllImpacts.All(i => !i.IsCasting || i.Data.Config == null);

        public bool ControlledByAI => _unitAI != null;

        private UnitActionTypes CurrentUnitActionType => UnitActionState.CurrentActionType;
        private BehaviourState CurrentBehaviourState => UnitNetworkState.BehaviourState;
        private bool CurrentBehaviourStateAllowMove => CurrentBehaviourState == BehaviourState.None ||
                                                       CurrentBehaviourState == BehaviourState.Reload ||
                                                       CurrentBehaviourState == BehaviourState.MeleeAttack;

        private IAimTargetFinder AimTargetFinder => _sharedDependencies.AimTargetFinder;
        private AimTargetColliderCollection AimTargetCollection => _sharedDependencies.AimTargetColliderCollection;

        [CanBeNull] private BehaviourStateMachineHandler _stateMachineHandler;
        private SharedDependencies _sharedDependencies;
        private ServerDependencies _serverDependencies;
        private UnitEffectsController _unitEffectsController;
        private readonly List<IAimTarget> _closestAimTargets = new();
        private UnitAI _unitAI;
        private UnitAIGroupVision _unitAIGroupVision;
        private IShootingSharedLogic _shootingSharedLogic = new EmptyShootingSharedLogic();
        private IPreferences _preferences;

        [Inject]
        private void MonoConstruct([InjectOptional] ServerDependencies serverDependencies,
            SharedDependencies sharedDependencies, IPreferences preferences)
        {
            _serverDependencies = serverDependencies;
            _sharedDependencies = sharedDependencies;
            _preferences = preferences;
        }

        private void Awake()
        {
            Rigidbody.isKinematic = true;
            ImpactRoster = new ImpactModelRoster(this);
        }

        private void OnDestroy()
        {
            _unitAI?.Dispose();
            _stateMachineHandler?.Dispose();
            _unitEffectsController?.Dispose();
            ImpactRoster.Dispose();
            Emotion.Dispose();

            Destroyed?.Invoke();
        }

        void IPoolObject.OnGet() { }
        void IPoolObject.OnRelease() =>
            GetComponents<IResettable>().ForEach(r => r.Reset());

        [Server]
        public void InitServer(BattleUnitType unitType, UnitConfig unitConfig, IEnumerable<Equipment> equipments,
            string playerId, string playerName, int heroLevel)
        {
            tag = unitType switch
            {
                BattleUnitType.Bot => BattleUnitTypeHelper.Bot,
                BattleUnitType.Player => BattleUnitTypeHelper.Player,
                _ => throw new ArgumentOutOfRangeException(nameof(unitType), unitType, null)
            };

            IDamageService damageService = _sharedDependencies.ServiceRoster.DamageService;
            UnitEffectStateFactory effectStateFactory = _serverDependencies.UnitEffectStateFactory;

            UnitNetworkState.Init(unitType, unitConfig, equipments, playerId, playerName, heroLevel);
            _unitEffectsController = new UnitEffectsController(this,
                damageService, _sharedDependencies.SimulationTime,
                _sharedDependencies.AimTargetNetIdCollection,
                effectStateFactory, _serverDependencies.UnitEffectModelFactory);

            SharedInit();
        }

        private void SharedInit()
        {
            var damageService = _sharedDependencies.ServiceRoster.DamageService;
            var effectStateFactory = _serverDependencies.UnitEffectStateFactory;

            _shootingSharedLogic = ShootingHelper.CreateShootingLogic(this);

            if (RangeWeapon != null)
            {
                UnitRangeAttackCaster = gameObject.AddComponent<UnitRangeAttackCaster>();
                UnitRangeAttackCaster.Init(this, damageService, AimTargetFinder, _sharedDependencies.BulletProcessors,
                        effectStateFactory);

            }

            UnitConfig unitConfig = UnitNetworkState.UnitConfig;

            Rigidbody.mass = unitConfig.BodyMass;
            Rigidbody.isKinematic = false;
        }

        [Server]
        public void InitAI(UnitAIGroupVision unitAIGroupVision, IPositionProvider homePositionProvider)
        {
            IDamageService damageService = _sharedDependencies.ServiceRoster.DamageService;

            _unitAIGroupVision = unitAIGroupVision;
            _unitAI = new UnitAI(this, damageService, unitAIGroupVision, homePositionProvider);
        }

        [Server]
        public void DeinitAI()
        {
            _unitAI?.Dispose();
            _unitAI = null;
        }

        [Client]
        public void InitClient()
        {
            if (isClientOnly)
                SharedInit();

            if (isLocalPlayer)
            {
                MicroAIInit();
                Emotion.Initialize(this);
            }
        }

        [Client]
        public void DeinitClient()
        {
            if (isClientOnly)
            {
                _shootingSharedLogic = new EmptyShootingSharedLogic();
                Rigidbody.isKinematic = true;
            }

            if (isLocalPlayer)
                MicroAIDeinit();
        }

        [Client]
        private void MicroAIInit()
        {
            if (MeleeWeapon != null)
                gameObject.AddComponent<UnitMeleeAttackActivator>().Init(this, AimTargetCollection);

            if (RangeWeapon != null)
            {
                gameObject.AddComponent<ClosestAimTargetsFinder>().Init(this, _closestAimTargets, AimTargetCollection);
                gameObject.AddComponent<UnitAutoReloadingActivator>().Init(this);

                var effect = _preferences.BattleComponentRoster.SelectedUnitViewVFX;
                gameObject.AddComponent<UnitRangeAttackActivator>().Init(this, AimTargetFinder, _closestAimTargets, effect);
            }
        }

        [Client]
        private void MicroAIDeinit()
        {
            if (gameObject.TryGetComponent(out UnitMeleeAttackActivator meleeAttackActivator))
                Destroy(meleeAttackActivator);

            if (gameObject.TryGetComponent(out ClosestAimTargetsFinder aimTargetsFinder))
                Destroy(aimTargetsFinder);

            if (gameObject.TryGetComponent(out UnitAutoReloadingActivator reloadingActivator))
                Destroy(reloadingActivator);

            if (gameObject.TryGetComponent(out UnitRangeAttackActivator rangeAttackActivator))
                Destroy(rangeAttackActivator);
        }

        public void SetStateMachineHandler([CanBeNull] BehaviourStateMachineHandler stateHandler) =>
            _stateMachineHandler = stateHandler;

        public DamageZone GetDamageZone(Vector3 hitPoint, float breakShieldChance)
        {
            if (hitPoint == default)
                return DamageZonesConfig.DefaultZone;

            var t = transform;
            var vectorToHitPoint = hitPoint - t.position;
            var damageZone = DamageZonesConfig.GetDamageZone(vectorToHitPoint, ForwardAxis);

            if (damageZone.IsShield && breakShieldChance > Random.value)
                damageZone = DamageZonesConfig.DefaultZone;

            return damageZone;
        }

        public DamageZone GetDamageZone(float zoneMultiplier) =>
            DamageZonesConfig.GetDamageZoneByMultiplier(zoneMultiplier);

        public void TryExecuteUnitAction(UnitActionTypes actionType, Vector3 casterPosition)
        {
            if (UnitNetworkState.UnitConfig.UnitActionRoster.TryGetRandomAction(actionType, out UnitActionConfig unitAction))
            {
                var impactCasting = ImpactRoster.AllImpacts.Any(x => x.IsCasting);

                if(impactCasting && unitAction.IsNotFatalDamage)
                    return;

                var vectorToCaster = casterPosition - Transform.position;
                var angleY = Vector3.SignedAngle(Vector3.forward, vectorToCaster, Vector3.up);
                UnitActionState.TryExecute(unitAction, angleY);
            }
        }

        public float GetDamageMultiplier(DamageType damageType) => UnitNetworkState.UnitConfig.GetDamageMultiplier(damageType);

        public void TryExecuteUnitAction(UnitActionTypes actionType, float dirByYRotation)
        {
            if (UnitNetworkState.UnitConfig.UnitActionRoster.TryGetRandomAction(actionType, out UnitActionConfig unitAction))
                UnitActionState.TryExecute(unitAction, dirByYRotation);
        }

        public void AddUnitEffect(UnitEffectState effect) => UnitNetworkEffects.Add(effect);
        public bool HasUnitEffect(UnitEffectState effect) => UnitNetworkEffects.Contains(effect);
        public void RemoveUnitEffect(UnitEffectState effect) => UnitNetworkEffects.Remove(effect);
        public bool TryGetUnitEffect(UnitEffectConfig config, out UnitEffectState state) =>
            UnitNetworkEffects.TryGetState(config, out state);

        public bool CanReceiveUnitEffect(UnitEffectConfig config) =>
            transform != null && !IsDestroyedOrDead &&
            UnitNetworkState.BehaviourState != BehaviourState.Dead &&
            (config.IsBuff || CurrentUnitActionType != UnitActionTypes.Dodge);

        public bool IsFriendly(UnitNetwork sourceUnit) => ShootingHelper.IsFriendly(this, sourceUnit);

        private void FixedUpdate()
        {
            if (!UnitNetworkState.IsInitialized) return;

            ShootingSharedLogic.Tick(Time.fixedDeltaTime);

            if (UnitNetworkState.isClientOnly) return;

            if (UnitNetworkState.BehaviourState != BehaviourState.Dead)
                _unitAI?.Update(Time.fixedDeltaTime);

            //TODO: Тут переключаю конфиг юнита по здоровью. Поторопился. Надо будет более элегантное решение
            if (UnitNetworkState.UnitConfig.UnitPhaseConfig == null) return;

            float health = UnitNetworkState.HealthState.Health;
            float healthRatio = health / UnitNetworkState.HealthState.MaxHealth;
            UnitPhaseConfig phaseConfig = UnitNetworkState.UnitConfig.UnitPhaseConfig;

            if (healthRatio <= phaseConfig.ApplyConditions.CurrentHealthRatio)
            {
                UnitNetworkState.SwitchUnitConfig(UnitNetworkState.UnitConfig.UnitPhaseConfig.UnitConfig);

                if (phaseConfig.ApplyActions.InitEnergy)
                    UnitNetworkState.Energy = UnitNetworkState.MaxEnergy * phaseConfig.ApplyActions.InitialEnergyRatio;
            }
        }

        public void InvokeOnCaughtArea() => OnCaughtArea?.Invoke();
    }
}