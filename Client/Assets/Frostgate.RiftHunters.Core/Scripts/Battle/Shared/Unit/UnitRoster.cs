﻿using Frostgate.RiftHunters.Core.Battle;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [CreateAssetMenu(menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(UnitRoster),
        fileName = nameof(UnitRoster))]
    public sealed class UnitRoster : ScriptableRoster<UnitConfig>
    {
    }
}