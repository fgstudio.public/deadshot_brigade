using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    [CreateAssetMenu(fileName = nameof(UnitEmotionConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(UnitEmotionConfig))]
    public class UnitEmotionConfig : EntityConfig
    {
        [field: SerializeField] public Sprite GlowSprite { get; private set; }
        [field: SerializeField] public float ShowDuration { get; private set; } = 4f;
    }
}