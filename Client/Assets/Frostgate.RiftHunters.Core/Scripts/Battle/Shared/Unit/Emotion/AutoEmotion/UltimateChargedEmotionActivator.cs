using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public sealed class UltimateChargedEmotionActivator : IAutoEmotionActivator
    {
        private readonly EmotionByUnitData _autoEmotionData;
        private readonly UnitNetwork _unit;

        public UltimateChargedEmotionActivator(UnitNetwork unit, EmotionByUnitData autoEmotionData)
        {
            _autoEmotionData = autoEmotionData;
            _unit = unit;

            _unit.UnitNetworkState.EnergyFilled += EnergyFilled;
        }

        private void EnergyFilled()
        {
            _unit.Emotion.InvokeEmotion(_unit.netId, _autoEmotionData.Config);;
        }

        public void Dispose()
        {
            _unit.UnitNetworkState.EnergyFilled -= EnergyFilled;
        }
    }
}