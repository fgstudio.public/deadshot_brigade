using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    [CreateAssetMenu(fileName = nameof(UnitEmotionRoster),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(UnitEmotionRoster))]
    public class UnitEmotionRoster : ScriptableObject
    {
        public AutoEmotionRoster AutoEmotionRoster => _autoEmotionRoster;

        [SerializeField] private UnitEmotionConfig[] _emotionConfigs;
        [SerializeField] private AutoEmotionRoster _autoEmotionRoster;

        public bool TryGet(int index, out UnitEmotionConfig emotion)
        {
            emotion = null;
            if (index >= _emotionConfigs.Length)
                return false;

            emotion = _emotionConfigs[index];
            return true;
        }
    }
}