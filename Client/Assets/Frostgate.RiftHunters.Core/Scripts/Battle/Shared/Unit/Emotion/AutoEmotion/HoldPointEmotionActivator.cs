using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public sealed class HoldPointEmotionActivator : IAutoEmotionActivator
    {
        private readonly AutoEmotionData _autoEmotionData;
        private readonly UnitNetwork _unit;
        private float _lastTime;

        public HoldPointEmotionActivator(UnitNetwork unit, AutoEmotionData autoEmotionData)
        {
            _autoEmotionData = autoEmotionData;
            _unit = unit;

            _unit.OnCaughtArea += OnCaughtArea;
        }

        private void OnCaughtArea()
        {
            if (Time.time - _lastTime < _autoEmotionData.Cooldown)
                return;

            _unit.Emotion.InvokeEmotion(_unit.netId, _autoEmotionData.Config);
            _lastTime = Time.time;
        }

        public void Dispose()
        {
            _unit.OnCaughtArea -= OnCaughtArea;
        }
    }
}