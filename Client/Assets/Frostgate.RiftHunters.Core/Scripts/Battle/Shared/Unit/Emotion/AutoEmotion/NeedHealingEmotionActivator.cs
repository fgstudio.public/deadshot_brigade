using System.Collections;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public sealed class NeedHealingEmotionActivator : IAutoEmotionActivator
    {
        private readonly NeedHealingAutoEmotionData _autoEmotionData;
        private readonly UnitNetwork _unit;

        private IEnumerator _waitingLowHp;

        public NeedHealingEmotionActivator(UnitNetwork unit, NeedHealingAutoEmotionData autoEmotionData)
        {
            _autoEmotionData = autoEmotionData;
            _unit = unit;

            _waitingLowHp = WaitingLowHp();
            _unit.StartCoroutine(_waitingLowHp);
        }

        private IEnumerator WaitingLowHp()
        {
            while (true)
            {
                yield return null;
                var ratio = _unit.UnitNetworkState.Health / _unit.UnitNetworkState.MaxHealth;
                if (!_unit.UnitNetworkState.IsDead && ratio <= _autoEmotionData.HealthRatioLimit)
                {
                    _unit.Emotion.InvokeEmotion(_unit.netId, _autoEmotionData.Config);
                    yield return new WaitForSeconds(_autoEmotionData.Cooldown);
                }
            }
        }

        public void Dispose()
        {
            if (_unit != null && _waitingLowHp != null)
                _unit.StopCoroutine(_waitingLowHp);
            _waitingLowHp = null;
        }
    }
}