﻿using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Client;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Mirror;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Scripts.Battle.Shared.Unit.Network
{
    public class UnitNetworkEmotion : NetworkBehaviour, IDisposable
    {
        public event Action<uint, UnitEmotionConfig> ApplyEmotion;
        public event Action<uint> ClearEmotion;

        [SerializeField, Required] private UnitNetworkInput _input;

        private List<IAutoEmotionActivator> _autoEmotionActivators = new ();
        private UnitNetwork _unit;

        public void Initialize(UnitNetwork unitNetwork)
        {
            _unit = unitNetwork;

            CreateAutoEmotionActivators();
        }

        public void Dispose()
        {
            _autoEmotionActivators.ForEach(t => t.Dispose());
        }

        private void Awake()
        {
            _input.OnEmotion += InvokeEmotion;
        }

        private void OnDestroy()
        {
            _input.OnEmotion -= InvokeEmotion;
            Dispose();
        }

        public void InvokeEmotion(uint netId, UnitEmotionConfig config)
        {
            //todo: используется для режима Selfhost
            ApplyEmotion?.Invoke(_unit.netId, config);

            if (isClientOnly)
            {
                ApplyEmotion?.Invoke(_unit.netId, config);
                CmdOnEmotion(netId, config);
            }
            else
                RpcOnEmotion(netId, config);
        }

        public void InvokeClearEmotion(uint netId)
        {
            ClearEmotion?.Invoke(_unit.netId);

            if (isClientOnly)
            {
                ClearEmotion?.Invoke(_unit.netId);
                CmdOnClearEmotion(netId);
            }
            else
                RpcOnClearEmotion(netId);
        }

        private void CreateAutoEmotionActivators()
        {
            var autoEmotionRoster = _unit.UnitEmotionRoster.AutoEmotionRoster;

            _autoEmotionActivators = new List<IAutoEmotionActivator>()
            {
                new HoldPointEmotionActivator(_unit, autoEmotionRoster.HoldPoint),
                new KnockedDownEmotionActivator(_unit, autoEmotionRoster.KnockedDown),
                new NeedHealingEmotionActivator(_unit, autoEmotionRoster.NeedHealing),
                new UltimateChargedEmotionActivator(_unit, autoEmotionRoster.UltimateCharged),
                new UltimateStartedEmotionActivator(_unit, autoEmotionRoster.UltimateStarted),
                new ClearEmotionActivator(_unit)
            };
        }

        [ClientRpc(includeOwner = false)]
        private void RpcOnEmotion(uint netId, UnitEmotionConfig config)
        {
            if (isClientOnly)
            {
                ApplyEmotion?.Invoke(netId, config);
            }
        }

        [ClientRpc(includeOwner = false)]
        private void RpcOnClearEmotion(uint netId)
        {
            if (isClientOnly)
            {
                ClearEmotion?.Invoke(netId);
            }
        }

        [Command]
        private void CmdOnEmotion(uint netId, UnitEmotionConfig config)
        {
            ApplyEmotion?.Invoke(netId, config);
            RpcOnEmotion(netId, config);
        }

        [Command]
        private void CmdOnClearEmotion(uint netId)
        {
            ClearEmotion?.Invoke(netId);
            RpcOnClearEmotion(netId);
        }

    }
}