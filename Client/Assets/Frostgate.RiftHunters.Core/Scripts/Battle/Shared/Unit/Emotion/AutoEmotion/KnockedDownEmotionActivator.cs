using System.Collections;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public sealed class KnockedDownEmotionActivator : IAutoEmotionActivator
    {
        private readonly AutoEmotionData _autoEmotionData;
        private readonly UnitNetwork _unit;

        private IEnumerator _waitingDeathState;

        public KnockedDownEmotionActivator(UnitNetwork unit, AutoEmotionData autoEmotionData)
        {
            _autoEmotionData = autoEmotionData;
            _unit = unit;

            _waitingDeathState = WaitingDeath();
            _unit.StartCoroutine(_waitingDeathState);
        }

        private IEnumerator WaitingDeath()
        {
            while (true)
            {
                yield return null;
                if (_unit.UnitNetworkState.IsDead)
                {
                    _unit.Emotion.InvokeEmotion(_unit.netId, _autoEmotionData.Config);
                    yield return new WaitForSeconds(_autoEmotionData.Cooldown);
                }
            }
        }

        public void Dispose()
        {
            if (_unit != null && _waitingDeathState != null)
                _unit.StopCoroutine(_waitingDeathState);
            _waitingDeathState = null;
        }
    }
}