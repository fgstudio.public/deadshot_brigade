using System;
using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    [Serializable]
    public class AutoEmotionData
    {
        public UnitEmotionConfig Config => _config;
        public float Cooldown => _cooldown;

        [SerializeField] private UnitEmotionConfig _config;
        [SerializeField] private float _cooldown = 3;
    }

    [Serializable]
    public class NeedHealingAutoEmotionData : AutoEmotionData
    {
        public float HealthRatioLimit => _healthRatioLimit;

        [SerializeField] private float _healthRatioLimit = 0.3f;
    }

    [Serializable]
    public class EmotionByUnit
    {
        public UnitConfig Unit => _unit;
        public UnitEmotionConfig Emotion => _emotion;

        [SerializeField] private UnitConfig _unit;
        [SerializeField] private UnitEmotionConfig _emotion;
    }

    [Serializable]
    public class EmotionByUnitData : AutoEmotionData
    {
        [SerializeField] private EmotionByUnit[] _emotionByUnits;

        private Dictionary<UnitConfig, UnitEmotionConfig> _cache;

        // TODO: возможно стоит это выпилить и перенести хранение иконок эмоций в конфиг ульты.
        public UnitEmotionConfig GetEmotion(UnitConfig unit)
        {
            if (_cache == null)
            {
                _cache = new Dictionary<UnitConfig, UnitEmotionConfig>();
                foreach (var emotionByUnit in _emotionByUnits)
                    _cache.Add(emotionByUnit.Unit, emotionByUnit.Emotion);
            }

            if (_cache.ContainsKey(unit))
                return _cache[unit];

            return Config;
        }
    }

    [CreateAssetMenu(fileName = nameof(AutoEmotionRoster),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(AutoEmotionRoster))]
    public class AutoEmotionRoster : ScriptableObject
    {
        public NeedHealingAutoEmotionData NeedHealing => _needHealing;
        public AutoEmotionData KnockedDown => _knockedDown;
        public AutoEmotionData HoldPoint => _holdPoint;
        public EmotionByUnitData UltimateCharged => _ultimateCharged;
        public EmotionByUnitData UltimateStarted => _ultimateStarted;

        [SerializeField] private NeedHealingAutoEmotionData _needHealing;
        [SerializeField] private AutoEmotionData _knockedDown;
        [SerializeField] private AutoEmotionData _holdPoint;
        [SerializeField] private EmotionByUnitData _ultimateCharged;
        [SerializeField] private EmotionByUnitData _ultimateStarted;
    }
}