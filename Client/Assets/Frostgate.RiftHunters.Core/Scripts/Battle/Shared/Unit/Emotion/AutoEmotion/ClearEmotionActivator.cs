﻿using System.Collections;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class ClearEmotionActivator : IAutoEmotionActivator
    {
        private readonly UnitNetwork _unit;
        private bool _isCleared;

        private IEnumerator _waitingAliveEnumerator;

        public ClearEmotionActivator(UnitNetwork unit)
        {
            _unit = unit;

            _waitingAliveEnumerator = WaitingAlive();
            _unit.StartCoroutine(_waitingAliveEnumerator);
        }

        public void Dispose()
        {
            if (_unit != null && _waitingAliveEnumerator != null)
                _unit.StopCoroutine(_waitingAliveEnumerator);
        }

        private IEnumerator WaitingAlive()
        {
            while (true)
            {
                yield return null;
                if (_unit.UnitNetworkState.BehaviourState != BehaviourState.Dead)
                {
                    if (!_isCleared)
                    {
                        _unit.Emotion.InvokeClearEmotion(_unit.netId);
                        _isCleared = true;
                    }
                }
                else
                {
                    _isCleared = false;
                }
            }
        }
    }
}