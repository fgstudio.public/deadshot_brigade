using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class UltimateStartedEmotionActivator : IAutoEmotionActivator
    {
        private EmotionByUnitData _autoEmotionData;
        private UnitNetwork _unit;

        public UltimateStartedEmotionActivator(UnitNetwork unit, EmotionByUnitData autoEmotionData)
        {
            _autoEmotionData = autoEmotionData;
            _unit = unit;

            _unit.ImpactRoster.Ultimate.CastStarted += CastStarted;
        }

        private void CastStarted()
        {
            _unit.Emotion.InvokeEmotion(_unit.netId, _autoEmotionData.Config);;
        }

        public void Dispose()
        {
            _unit.ImpactRoster.Ultimate.CastStarted -= CastStarted;
        }
    }
}