using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.Reanimation
{
    public interface IReanimationMechanic
    {
        UnityEvent Enabled { get; }
        UnityEvent Disabled { get; }
        UnityEvent Completed { get; }
        UnityEvent Restored { get; }
        UnityEvent Activated { get; }
        UnityEvent Deactivated { get; }

        bool IsActive { get; }
        bool IsEnabled { get; }
        IProgress Progress { get; }
        Transform PointerPoint { get; }
        IAreaOfInterest AreaOfInterest { get; }
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e23a099eb137443b8a16bd7b023dc6d8")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(ReanimationMechanicComponent))]
    public sealed class ReanimationMechanicComponent : Component, IReanimationMechanic
    {
        private const string EventsFoldoutName = "Reanimation Events";

        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _pointerPoint;
        [SerializeField, Required, ChildGameObjectsOnly] private AutoCompletingSlider _progress;
        [SerializeField, Required, ChildGameObjectsOnly] private ReanimationAreaComponent _areaComponent;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject[] _objectsForMechanic;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject[] _activeAreaObjects;
        [SerializeField, Required, ChildGameObjectsOnly] private GameObject[] _inactiveAreaObjects;

        [Header("Info")]
        [ShowInInspector, DisplayAsString] private float _increaseProgressSpeedPerVisitor;
        [ShowInInspector, DisplayAsString] private float _decreaseProgressSpeed;

        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Completed { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Activated { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Deactivated { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Restored { get; private set; }


        public IProgress Progress => _progress;
        bool IReanimationMechanic.IsEnabled => enabled;
        public Transform PointerPoint => _pointerPoint;
        public IAreaOfInterest AreaOfInterest => _areaComponent;
        public bool IsActive => _areaComponent.AreaVisitorsCount > 0;

        public void SetIncreaseProgressSpeed(float progressSpeedPerVisitor)
        {
            _increaseProgressSpeedPerVisitor = progressSpeedPerVisitor;
        }

        public void SetDecreaseProgressSpeed(float decreaseProgressSpeed)
        {
            _decreaseProgressSpeed = decreaseProgressSpeed;
        }

        public void SetAreaRadius(float radius)
        {
            _areaComponent.SetRadius(radius);
        }

        private void OnValidate()
        {
            _progress ??= GetComponentInChildren<AutoCompletingSlider>();
            _areaComponent ??= GetComponentInChildren<ReanimationAreaComponent>();
        }

        protected override void OnEnabled()
        {
            _progress.Reset();

            _progress.Completed.AddListener(OnIncreased);
            _progress.Restored.AddListener(OnDecreased);
            _areaComponent.VisitorRegistered.AddListener(OnVisitorsCountUpdated);
            _areaComponent.VisitorUnregistered.AddListener(OnVisitorsCountUpdated);

            _areaComponent.Enable();

            _objectsForMechanic.ForEach(e => e.SetActive(true));
            _activeAreaObjects.ForEach(e => e.SetActive(false));
            _inactiveAreaObjects.ForEach(e => e.SetActive(true));
        }

        protected override void OnDisabled()
        {
            _progress.Completed.RemoveListener(OnIncreased);
            _progress.Restored.RemoveListener(OnDecreased);
            _areaComponent.VisitorRegistered.RemoveListener(OnVisitorsCountUpdated);
            _areaComponent.VisitorUnregistered.RemoveListener(OnVisitorsCountUpdated);

            _areaComponent.Disable();

            _objectsForMechanic.ForEach(e => e.SetActive(false));
            _activeAreaObjects.ForEach(e => e.SetActive(false));
            _inactiveAreaObjects.ForEach(e => e.SetActive(false));
        }

        private void OnIncreased() => Completed?.Invoke();
        private void OnDecreased() => Restored?.Invoke();

        private void OnVisitorsCountUpdated(UnitNetwork _)
        {
            if (IsActive) Activate();
            else Deactivate();
        }

        private void Activate()
        {
            _activeAreaObjects.ForEach(e => e.SetActive(true));
            _inactiveAreaObjects.ForEach(e => e.SetActive(false));

            _progress.StopProgress();
            _progress.Speed = _areaComponent.AreaVisitorsCount * _increaseProgressSpeedPerVisitor;
            _progress.StartIncreasingProgress();
            Activated?.Invoke();
        }

        private void Deactivate()
        {
            _activeAreaObjects.ForEach(e => e.SetActive(false));
            _inactiveAreaObjects.ForEach(e => e.SetActive(true));

            _progress.StopProgress();
            _progress.Speed = _decreaseProgressSpeed;
            _progress.StartDecreasingProgress();
            Deactivated?.Invoke();
        }
    }
}