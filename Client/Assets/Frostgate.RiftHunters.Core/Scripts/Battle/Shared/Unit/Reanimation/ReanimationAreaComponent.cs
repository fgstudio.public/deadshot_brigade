using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.Reanimation
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/e23a099eb137443b8a16bd7b023dc6d8")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(ReanimationAreaComponent))]
    public sealed class ReanimationAreaComponent : AreaOfInterestComponent
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private SpriteRenderer[] _renderers;
        [SerializeField, Required, ChildGameObjectsOnly] private Transform[] _scalingObjects;

        protected override void OnDisabledInner() =>
            LoseAllVisitors();

        protected override void SetRadiusInner(float radius)
        {
            foreach (SpriteRenderer renderer in _renderers)
                renderer.size = Vector2.one * radius * 2;

            foreach (Transform scalingObject in _scalingObjects)
                scalingObject.localScale *= radius;
        }
    }
}