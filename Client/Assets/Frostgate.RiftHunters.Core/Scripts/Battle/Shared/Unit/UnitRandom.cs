using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public class UnitRandom
    {
        public UnitRandomConfig Config { get; }

        public float Value
        {
            get
            {
                _tick++;
                return (float) Config.Get(_tick) / Config.Max;
            }
        }

        public float Range(float min, float max) => min + (max - min) * Value;

        private int _tick;

        public UnitRandom(UnitRandomConfig config) => Config = config;

        public void Sync(int tick) => _tick = tick;

        public Vector3 RandomizeBulletDirection(Vector3 direction, float angle)
        {
            float GetRandom()
            {
                var random = Range(-1f, 1f);
                return random * random * Mathf.Sign(random) * angle;
            }

            var x = GetRandom();
            var y = GetRandom();
            var z = GetRandom();

            return Quaternion.Euler(x, y, z) * direction;
        }
    }
}