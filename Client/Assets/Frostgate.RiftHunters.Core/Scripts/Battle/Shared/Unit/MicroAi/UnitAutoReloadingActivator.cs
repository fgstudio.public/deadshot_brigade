using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.Menu +"/" + nameof(UnitAutoReloadingActivator))]
    public sealed class UnitAutoReloadingActivator : MonoBehaviour
    {
        [CanBeNull] private UnitNetwork _unit;
        [CanBeNull] private RangeWeaponConfig _rangeWeaponConfig;

        public void Init([CanBeNull] UnitNetwork unit)
        {
            _unit = unit;
            _rangeWeaponConfig = unit?.RangeWeapon;
        }

        private void FixedUpdate()
        {
            if (_rangeWeaponConfig == null || !_rangeWeaponConfig.UsePatrons) return;
            if (_unit == null || !_unit.CanBeginReloading || _unit.UnitNetworkState.PatronsCount > 0) return;

            _unit.UnitNetworkInput.ExecuteReloading();
        }
    }
}