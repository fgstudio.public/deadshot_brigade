using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class ClosestAimTargetsFinder : MonoBehaviour
    {
        private UnitNetwork _unit;
        private List<IAimTarget> _closestAimTargets;
        private IReadOnlyObjectCollection<Collider, IAimTarget> _aimTargetCollection;

        public void Init(UnitNetwork unit, List<IAimTarget> closestAimTargets, IReadOnlyObjectCollection<Collider, IAimTarget> aimTargetCollection)
        {
            _unit = unit;
            _closestAimTargets = closestAimTargets;
            _aimTargetCollection = aimTargetCollection;
            StartCoroutine(FindClosestTargets());
        }

        private IEnumerator FindClosestTargets()
        {
            while (true)
            {
                const float delay = 1f;
                yield return new WaitForSeconds(delay);

                if (_unit.UnitNetworkState.IsDead || _unit.RangeWeapon == null)
                    continue;

                var hitCount =
                    Physics.SphereCastNonAlloc(_unit.ShootPoint, _unit.RangeWeapon.MaxDistance,
                        Vector3.up, PhysicHelper.HitResults, 0);

                _closestAimTargets.Clear();

                for (var i = 0; i < hitCount && i < PhysicHelper.HitResults.Length; i++)
                {
                    var hit = PhysicHelper.HitResults[i];

                    if (!_aimTargetCollection.TryGet(hit.collider, out IAimTarget aimTarget))
                        continue;

                    _closestAimTargets.Add(aimTarget);
                }
            }
        }
    }
}