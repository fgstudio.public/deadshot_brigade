using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public sealed partial class UnitRangeAttackActivator
    {
        private void OnDrawGizmos()
        {
            if (_aimTarget != null)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawRay(_unit.ShootPoint, Input.AimDirection);
                Gizmos.color = Color.red;
                Gizmos.DrawLine(_aimTarget.FocusPoint, _unit.ShootPoint);
            }
        }
    }
}
