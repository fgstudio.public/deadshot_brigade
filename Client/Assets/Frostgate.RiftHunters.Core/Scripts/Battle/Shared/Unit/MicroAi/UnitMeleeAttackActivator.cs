using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    public class UnitMeleeAttackActivator : MonoBehaviour
    {
        private UnitNetwork _unit;
        private IAimTarget _aimTarget;
        private IReadOnlyObjectCollection<Collider, IAimTarget> _aimTargetCollection;
        private float _triggerRadius;

        public void Init(UnitNetwork unit, IReadOnlyObjectCollection<Collider, IAimTarget> aimTargetCollection)
        {
            _unit = unit;
            _aimTargetCollection = aimTargetCollection;
            _triggerRadius = _unit.MeleeWeapon?.MaxDistance ?? default;
        }

        private void FixedUpdate()
        {
            if (!_unit.CanMeleeAttack)
                return;

            if (_aimTarget == null)
                SearchTarget();
            else
                CheckTarget();
        }

        // Вот это надо переделать на триггере вместо SphereCast. Можно из этого же скрипта сгенерить необходимый триггер в юните.
        // Пока оставлю так. Функционально будет работать точно так же, а переделывать долго.
        private void SearchTarget()
        {
            var hitCount =
                Physics.SphereCastNonAlloc(_unit.ShootPoint, _triggerRadius, Vector3.up, PhysicHelper.HitResults,
                    0, PhysicsLayers.AimTargetMask);

            if (hitCount <= 0)
                return;

            for (int i = 0; i < hitCount && i < PhysicHelper.HitResults.Length; i++)
            {
                RaycastHit hit = PhysicHelper.HitResults[i];

                // Исключаем из обработки коллайдер нашего юнита
                if (_unit.UnitNetworkState.ViewConfig.AimTarget == null ||
                    _unit.UnitNetworkState.ViewConfig.AimTarget == hit.collider)
                    continue;

                if (!_aimTargetCollection.TryGet(hit.collider, out IAimTarget aimTarget))
                    continue;

                if (ShootingHelper.IsFriendly(_unit, aimTarget))
                    continue;

                if (!ShootingHelper.CheckMeleeAttackAngle(_unit, aimTarget))
                    continue;

                if (ShootingHelper.IsShooterInsideTarget(_unit, aimTarget))
                    continue;

                _aimTarget = aimTarget;
                _unit.UnitNetworkState.Target = aimTarget.Identity;
                _unit.UnitNetworkInput.ExecuteMeleeAttack();
                return;
            }
        }

        private void CheckTarget()
        {
            bool targetInTrigger = false;

            if (!_aimTarget.State.IsDead)
            {
                int hitCount =
                    Physics.SphereCastNonAlloc(_unit.ShootPoint, _triggerRadius, Vector3.up, PhysicHelper.HitResults,
                        0, PhysicsLayers.AimTargetMask);

                for (int i = 0; i < hitCount && i < PhysicHelper.HitResults.Length; i++)
                {
                    RaycastHit hit = PhysicHelper.HitResults[i];

                    if (!_aimTargetCollection.TryGet(hit.collider, out IAimTarget aimTarget))
                        continue;

                    if (aimTarget != _aimTarget)
                        continue;

                    if (!ShootingHelper.CheckMeleeAttackAngle(_unit, aimTarget))
                        continue;

                    if (ShootingHelper.IsShooterInsideTarget(_unit, aimTarget))
                        continue;

                    targetInTrigger = true;
                    break;
                }
            }

            if (!targetInTrigger)
            {
                _unit.UnitNetworkState.Target = null;
                _aimTarget = null;
                SearchTarget();
                return;
            }

            _unit.UnitNetworkInput.ExecuteMeleeAttack();
        }
    }
}