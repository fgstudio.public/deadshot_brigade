using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;

namespace Frostgate.RiftHunters.Core.Battle.Client
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.Menu +"/" + nameof(UnitRangeAttackActivator))]
    public sealed partial class UnitRangeAttackActivator : MonoBehaviour
    {
        private UnitNetworkInput Input => _unit.UnitNetworkInput;

        private UnitNetwork _unit;
        [ShowInInspector, ReadOnly] private IAimTarget _aimTarget;
        private IAimTargetFinder _aimTargetFinder;
        private IReadOnlyObjectCollection<Collider, UnitNetwork> _unitCollection;
        private IReadOnlyList<IAimTarget> _closestAimTargets;
        private readonly Dictionary<UnitNetwork, UnitViewVFX> _selections = new();
        private UnitViewVFX _selectionVFX;
        private readonly List<UnitNetwork> _targetUnits = new();
        private readonly List<UnitNetwork> _removeList = new();

        public void Init(UnitNetwork unit, IAimTargetFinder aimTargetFinder,
            IReadOnlyList<IAimTarget> closestAimTargets, UnitViewVFX selectionVFX)
        {
            _unit = unit;
            _selectionVFX = selectionVFX;
            _aimTargetFinder = aimTargetFinder;
            _closestAimTargets = closestAimTargets;
        }

        private void FixedUpdate()
        {
            if (_unit == null)
                return;

            DetectAimTarget();
            AddSelectionVFX();

            if (!_unit.CanRangeAttack || _aimTarget.IsNullDestroyedOrDead())
            {
                Input.SetShooting(false);
            }
            else
            {
                var aimDir = (_aimTarget.FocusPoint - _unit.ShootPoint).normalized;
                Input.SetAimDirection(aimDir);

                // Стреляем только если камера тоже повернута к цели (ее поворот в инпут приходит),
                // чтобы не было стрельбы пока персонаж доворачивается.
                var shootingEnabled =
                    ShootingHelper.IsShooterLookToFocusPoint(_unit, Input.YRotation, _aimTarget.FocusPoint);
                Input.SetShooting(shootingEnabled);
            }
        }

        private void DetectAimTarget()
        {
            if (!_unit.CanRangeAttack)
                return;

            _aimTargetFinder.TryGetAimTarget(_unit, _closestAimTargets, out _aimTarget);
            _unit.UnitNetworkState.Target = _aimTarget?.Identity;
        }

        private void AddSelectionVFX()
        {
            var targets = _aimTargetFinder.GetAimTargets(_unit, _closestAimTargets);

            _targetUnits.Clear();

            foreach (IAimTarget target in targets)
            {
                UnitNetwork unit = target.GameObject!.GetComponent<UnitNetwork>();
                if (unit != null)
                    _targetUnits.Add(unit);
            }

            _removeList.Clear();

            foreach (var unit in _selections.Keys)
                if (!_targetUnits.Contains(unit))
                    _removeList.Add(unit);

            foreach (var unit in _targetUnits)
            {
                if (!_selections.ContainsKey(unit) && unit.View != null)
                {
                    var view = Instantiate(_selectionVFX, unit.View.transform);
                    view.Init(null, unit.View);

                    _selections.Add(unit, view);
                }
            }

            foreach (var unit in _removeList)
            {
                var vfx = _selections[unit];

                if (vfx != null)
                    vfx.Dispose();

                _selections.Remove(unit);
            }
        }
    }
}