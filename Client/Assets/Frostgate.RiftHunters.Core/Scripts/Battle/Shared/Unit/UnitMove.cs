using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public class UnitMove : MonoBehaviour
    {
        [SerializeField, Required] private UnitNetwork _unitNetwork;
        [SerializeField, Required] private UnitNetworkPosition _unitNetworkPosition;
        [SerializeField, Required] private UnitNetworkRotation _unitNetworkRotation;
        [SerializeField, Required] private Rigidbody _rigidbody;
        [SerializeField, Required] private Transform _transform;

        public UnityEvent RotationSet => _unitNetworkRotation.RotationSet;

        private UnitNetworkState UnitNetworkState => _unitNetwork.UnitNetworkState;
        private UnitNetworkEffects UnitNetworkEffects => _unitNetwork.UnitNetworkEffects;
        private UnitNetworkInput UnitNetworkInput => _unitNetwork.UnitNetworkInput;

        public void ResetPosition(Vector3 newPosition)
        {
            _transform.position = newPosition;
            _unitNetworkPosition.SendTeleportToAllClient(_transform.position);
        }

        public void ResetRotation(Vector3 eulerAngles) =>
            _unitNetworkRotation.SetRotation(eulerAngles.y);

        private void OnEnable()
        {
            if (UnitNetworkState.IsInitialized) OnInitialized(UnitNetworkState);
            else UnitNetworkState.Initialized += OnInitialized;
        }

        private void OnDisable()
        {
            UnitNetworkState.Initialized -= OnInitialized;
            UnitNetworkInput.OnSetYRotation -= OnPlayerSetYRotation;
        }

        private void OnInitialized(UnitNetworkState state)
        {
            if (UnitNetworkState.isLocalPlayer)
                UnitNetworkInput.OnSetYRotation += OnPlayerSetYRotation;
        }

        private void OnPlayerSetYRotation(float yRotation)
        {
            if (_unitNetwork.CanMove)
                _transform.eulerAngles = new Vector3(0, yRotation, 0);
        }

        private void FixedUpdate()
        {
            if (_unitNetwork.CanMove)
                WalkUpdate(Time.fixedDeltaTime);
            else
                StopHorizontalVelocity();
        }

        private void StopHorizontalVelocity() =>
            _rigidbody.velocity = new Vector3(0, _rigidbody.velocity.y, 0);

        private void WalkUpdate(float deltaTime)
        {
            var targetYRotation = UnitNetworkInput.YRotation;
            var targetRotation = Quaternion.Euler(0, targetYRotation, 0);
            _transform.eulerAngles = CalcWalkRotation(deltaTime, targetYRotation);

            var moveVector = new Vector3(UnitNetworkInput.Move.x, 0, UnitNetworkInput.Move.y);
            moveVector = Vector3.ClampMagnitude(moveVector, 1) * CalcSpeed();
            moveVector = targetRotation * moveVector;

            var ySpeed = CalcYSpeed();
            _rigidbody.velocity = new Vector3(moveVector.x, ySpeed, moveVector.z);
        }

        private Vector3 CalcWalkRotation(float deltaTime, float targetYRotation)
        {
            IReadOnlyUnitPropertiesProvider properties = UnitNetworkState.PropertiesProvider!;
            float turnStep = properties.TurnSpeed * deltaTime;
            float yRotation = Mathf.MoveTowardsAngle(_transform.eulerAngles.y, targetYRotation, turnStep);

            return new Vector3(0, yRotation, 0);
        }

        private float CalcYSpeed()
        {
            // Это надо чтобы юнит падал быстро а взлетал медленно.
            const float minYSpeed = -10;
            return Mathf.Clamp(_rigidbody.velocity.y, minYSpeed, 0);
        }

        private float CalcSpeed()
        {
            float speed = UnitNetworkState.PropertiesProvider!.Speed;
            float weaponModifier = CalcSpeedModifierByWeapon();
            float paramsModifier = UnitNetworkEffects.Params.MoveSpeedMultiplier;

            return speed * weaponModifier * paramsModifier;
        }

        private float CalcSpeedModifierByWeapon()
        {
            switch (UnitNetworkState.BehaviourState)
            {
                case BehaviourState.Reload when _unitNetwork.RangeWeapon != null:
                    return _unitNetwork.RangeWeapon.InReloadMoveSpeedMultiplier;

                case BehaviourState.MeleeAttack when _unitNetwork.MeleeWeapon != null:
                    return _unitNetwork.MeleeWeapon.InAttackMoveSpeedMultiplier;

                default:
                    bool inAttack = _unitNetwork.ShootingSharedLogic.InProcess ||
                                    UnitNetworkState.SingleAttackInProcess;

                    if (inAttack && _unitNetwork.RangeWeapon != null)
                        return _unitNetwork.RangeWeapon.InAttackMoveSpeedMultiplier;

                    return 1f;
            }
        }
    }
}