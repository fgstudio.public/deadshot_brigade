using System;
using System.Threading;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public sealed class DelayedMethodInvoker : IDisposable
    {
        private readonly CancellationTokenSource _cts;

        public DelayedMethodInvoker(TimeSpan delay, [NotNull] Func<UniTask> method)
        {
            _cts = new CancellationTokenSource();

            UniTask.Delay(delay, cancellationToken: _cts.Token)
                .ContinueWith(method).AsAsyncUnitUniTask();
        }

        public DelayedMethodInvoker(TimeSpan delay, [NotNull] Action method)
        {
            _cts = new CancellationTokenSource();

            UniTask.Delay(delay, cancellationToken: _cts.Token)
                .ContinueWith(method).AsAsyncUnitUniTask();
        }

        public void Dispose()
        {
            _cts.Dispose();
        }

        public void Cancel() => _cts.Cancel();
    }
}
