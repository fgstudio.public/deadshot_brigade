using System;
using Mirror;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public interface IClientImpactModel
    {
        event Action CastStarted;
        event Action CastFinished;
        event Action Interrupted;
        event Action CooldownUpdated;

        bool IsCasting { get; }
        bool CanExecute { get; }
        UnitImpactData Data { get; }
    }

    public interface IServerImpactModel : IClientImpactModel
    {
        public void Execute();
        public void Interrupt();
    }

    public abstract class ImpactModel : IServerImpactModel, IDisposable
    {
        public event Action CastStarted;
        public event Action CastFinished;
        public event Action Interrupted;
        public event Action CooldownUpdated;

        [NotNull] protected readonly UnitNetworkState UnitState;
        [NotNull] protected readonly ImpactState ImpactState;

        [CanBeNull] private DelayedMethodInvoker _methodInvoker;

        [NotNull] public abstract UnitImpactData Data { get; }
        protected abstract bool ExecutionConditions { get; }

        public bool CanExecute =>
            Data.Config != null && !IsCasting && RemainingCooldown.Ticks == 0 && ExecutionConditions;

        public bool IsCasting =>
            Data.Config != null && ImpactState.IsCasting(Data.Config.Id);

        public TimeSpan RemainingCooldown
        {
            get
            {
                if (Data.Config == null) return TimeSpan.Zero;
                if (Data.Cooldown.Ticks == 0) return TimeSpan.Zero;

                TimeSpan lastExecution = ImpactState.PassedFromLastExecution(Data.Config.Id);
                return Data.Cooldown > lastExecution ? Data.Cooldown - lastExecution : TimeSpan.Zero;
            }
        }

        protected ImpactModel([NotNull] UnitNetworkState unitState, [NotNull] ImpactState impactState)
        {
            UnitState = unitState;
            ImpactState = impactState;

            Subscribe(impactState);
        }

        public void Dispose()
        {
            _methodInvoker?.Dispose();
            Unsubscribe(ImpactState);
        }

        [Server] void IServerImpactModel.Execute()
        {
            TimeSpan delay = CalcExecuteDelay(Data);
            _methodInvoker?.Dispose();

            if (delay.Ticks > 0) _methodInvoker = new DelayedMethodInvoker(delay, ExecuteImpactAsync);
            else ExecuteImpactAsync();

            UniTask ExecuteImpactAsync() => ImpactState.ExecuteImpactAsync(Data.Config!);
        }

        [Server] void IServerImpactModel.Interrupt()
        {
            _methodInvoker?.Cancel();
            ImpactState.InterruptImpact(Data.Config!);
        }

        // TODO: Быстрое решение. Заставлет многих зависеть от избыточного функционала
        protected abstract TimeSpan CalcActionDuration();

        private TimeSpan CalcExecuteDelay([NotNull] UnitImpactData data) =>
            Data.DelayVariant switch
            {
                UnitImpactData.DelayType.None => TimeSpan.Zero,
                UnitImpactData.DelayType.Value => data.Delay,
                UnitImpactData.DelayType.UntilActionEnd => CalcActionDuration(),
                _ => throw new ArgumentOutOfRangeException(nameof(DelayType))
            };

        private void Subscribe([NotNull] ImpactState impactState)
        {
            impactState.CastStarted.AddListener(OnImpactCastStarted);
            impactState.CastFinished.AddListener(OnImpactCastFinished);
            impactState.Interrupted.AddListener(OnImpactInterrupted);
            impactState.CooldownUpdated.AddListener(OnCooldownUpdated);
        }

        private void Unsubscribe([NotNull] ImpactState impactState)
        {
            impactState.CastStarted.RemoveListener(OnImpactCastStarted);
            impactState.CastFinished.RemoveListener(OnImpactCastFinished);
            impactState.Interrupted.RemoveListener(OnImpactInterrupted);
            impactState.CooldownUpdated.RemoveListener(OnCooldownUpdated);
        }

        private void OnImpactCastStarted(string impactId)
        {
            if (IsImpactTarget(impactId))
                CastStarted?.Invoke();
        }

        private void OnImpactCastFinished(string impactId)
        {
            if (IsImpactTarget(impactId))
                CastFinished?.Invoke();
        }

        private void OnImpactInterrupted(string impactId)
        {
            if (IsImpactTarget(impactId))
                Interrupted?.Invoke();
        }

        private void OnCooldownUpdated(string impactId)
        {
            if (IsImpactTarget(impactId))
                CooldownUpdated?.Invoke();
        }

        private bool IsImpactTarget(string impactId) =>
            Data.Config != null && impactId == Data.Config.Id;
    }
}
