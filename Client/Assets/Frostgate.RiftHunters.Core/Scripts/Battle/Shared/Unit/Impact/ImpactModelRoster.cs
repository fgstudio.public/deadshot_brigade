using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public interface IClientImpactModelRoster
    {
        IClientImpactModel Death { get; }
        IClientImpactModel Ability { get; }
        IClientImpactModel Ultimate { get; }
        IClientImpactModel ExtraAbility { get; }
        IClientImpactModel Special { get; }
        IEnumerable<IClientImpactModel> AllImpacts { get; }
    }

    public interface IServerImpactModelRoster
    {
        IServerImpactModel Death { get; }
        IServerImpactModel Ability { get; }
        IServerImpactModel Ultimate { get; }
        IServerImpactModel ExtraAbility { get; }
        IServerImpactModel Special { get; }
        IEnumerable<IServerImpactModel> AllImpacts { get; }
    }

    public sealed class ImpactModelRoster : IClientImpactModelRoster, IServerImpactModelRoster, IDisposable
    {
        [NotNull] public readonly DeathImpactModel Death;
        [NotNull] public readonly AbilityImpactModel Ability;
        [NotNull] public readonly UltimateImpactModel Ultimate;
        [NotNull] public readonly ExtraAbilityImpactModel ExtraAbility;
        [NotNull] public readonly SpecialImpactModel Special;

        public IEnumerable<ImpactModel> AllImpacts
        {
            get
            {
                yield return Death;
                yield return Ability;
                yield return ExtraAbility;
                yield return Ultimate;
                yield return Special;
            }
        }

        IClientImpactModel IClientImpactModelRoster.Death => Death;
        IClientImpactModel IClientImpactModelRoster.Ability => Ability;
        IClientImpactModel IClientImpactModelRoster.Ultimate => Ultimate;
        IClientImpactModel IClientImpactModelRoster.ExtraAbility => ExtraAbility;
        IClientImpactModel IClientImpactModelRoster.Special => Special;
        IEnumerable<IClientImpactModel> IClientImpactModelRoster.AllImpacts => AllImpacts;

        IServerImpactModel IServerImpactModelRoster.Death => Death;
        IServerImpactModel IServerImpactModelRoster.Ability => Ability;
        IServerImpactModel IServerImpactModelRoster.Ultimate => Ultimate;
        IServerImpactModel IServerImpactModelRoster.ExtraAbility => ExtraAbility;
        IServerImpactModel IServerImpactModelRoster.Special => Special;
        IEnumerable<IServerImpactModel> IServerImpactModelRoster.AllImpacts => AllImpacts;

        public ImpactModelRoster([NotNull] UnitNetwork unitNetwork)
        {
            UnitNetworkState unitState = unitNetwork.UnitNetworkState;
            UnitActionState actionState = unitNetwork.UnitActionState;
            ImpactState impactState = unitNetwork.ImpactState;

            Death = new DeathImpactModel(unitState, impactState);
            Ability = new AbilityImpactModel(unitState, impactState);
            Ultimate = new UltimateImpactModel(unitState, impactState, actionState);
            ExtraAbility = new ExtraAbilityImpactModel(unitState, impactState);
            Special = new SpecialImpactModel(unitState, impactState);
        }

        public void Dispose()
        {
            Death.Dispose();
            Ability.Dispose();
            Ultimate.Dispose();
            ExtraAbility.Dispose();
            Special.Dispose();
        }
    }
}
