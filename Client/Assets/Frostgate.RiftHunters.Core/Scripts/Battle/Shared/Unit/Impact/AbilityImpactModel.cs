using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public sealed class AbilityImpactModel : ImpactModel
    {
        public override UnitImpactData Data => UnitState.PropertiesProvider!.Impacts.Ability;
        protected override bool ExecutionConditions => !UnitState.IsDead;

        public AbilityImpactModel([NotNull] UnitNetworkState unitState, [NotNull] ImpactState impactState)
            : base(unitState, impactState) { }

        protected override TimeSpan CalcActionDuration() => TimeSpan.Zero;
    }
}