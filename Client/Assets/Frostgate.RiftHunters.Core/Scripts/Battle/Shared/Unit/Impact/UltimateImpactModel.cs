using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public sealed class UltimateImpactModel : ImpactModel
    {
        [NotNull] private readonly UnitActionState _actionState;

        public override UnitImpactData Data => UnitState.PropertiesProvider!.Impacts.Ultimate;

        protected override bool ExecutionConditions =>
            UnitState.IsEnergyFull && !UnitState.IsDead &&
            UnitState.BehaviourState != BehaviourState.Stun &&
            UnitState.BehaviourState != BehaviourState.Dead &&
            _actionState.CurrentActionType == UnitActionTypes.None;

        public UltimateImpactModel([NotNull] UnitNetworkState unitState,
            [NotNull] ImpactState impactState, [NotNull] UnitActionState actionState)
        : base(unitState, impactState) =>
            _actionState = actionState;

        protected override TimeSpan CalcActionDuration() => TimeSpan.Zero;
    }
}