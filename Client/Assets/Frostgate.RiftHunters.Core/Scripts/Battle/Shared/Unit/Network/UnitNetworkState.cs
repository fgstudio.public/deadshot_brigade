using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Core.Battle.Client.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Exp;
using Frostgate.RiftHunters.Core.Battle.Shared.Looting;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.Invincibility;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using Mirror;
using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(UnitHealthState))]
    public class UnitNetworkState : NetworkBehaviour, IDamageCasterState, IEnergy, IExp, IDamageReceiverState,
        ILootBoxCollectorState, IRespawnableState, IExpReceiverState, IInvincibleState, ICapturedState
    {
        public event Action<UnitNetworkState> Initialized;
        // ReSharper disable once IdentifierTypo
        public event Action<UnitNetworkState> Deinitializing;
        public event Action SingleAttacked;
        public event Action<string, Vector3, Vector3> EffectSpawned;
        public event Action<string, Vector3> EffectDespawned;
        public event Action EffectsDestroy;

        #region HEALTH

        public UnitHealthState HealthState => _healthState;
        [SerializeField] private UnitHealthState _healthState;

        public event IReadOnlyHealth.Changed HealthChanged;
        public event Action Raised;
        public event Action<UnitNetworkState> Died;

        public bool IsDead => HealthState.IsDead;
        public bool IsHealthFull => HealthState.IsHealthFull;
        public float MaxHealth => HealthState.MaxHealth;

        public float Health
        {
            get => HealthState.Health;
            set => HealthState.Health = value;
        }

        private void OnHealthChanged(float oldValue, float newValue)
        {
            if (newValue - oldValue != 0)
                HealthChanged?.Invoke(newValue - oldValue);

            if (oldValue > 0 && Mathf.Approximately(newValue, 0))
            {
                Died?.Invoke(this);
                _deathsCounter++;
            }

            if (Mathf.Approximately(oldValue, 0) && newValue > 0)
                Raised?.Invoke();
        }

        #endregion

        #region ENERGY

        public event IEnergy.Changed EnergyChanged;
        public event Action EnergyFilled;
        public event Action EnergyReset;

        [SyncVar(hook = nameof(OnEnergyChanged))]
        private float _energy;

        public bool IsEnergyFull => Mathf.Approximately(Energy, MaxEnergy);
        public float MaxEnergy => PropertiesProvider?.Energy ?? default;

        public float Energy
        {
            get => _energy;
            set
            {
                float prev = _energy;
                _energy = Mathf.Clamp(value, default, MaxEnergy);
                OnEnergyChanged(prev, _energy);
            }
        }

        private void OnEnergyChanged(float oldValue, float newValue)
        {
            float diff = newValue - oldValue;
            if (!Mathf.Approximately(diff, 0))
            {
                 EnergyChanged?.Invoke(diff);
                if (IsEnergyFull) EnergyFilled?.Invoke();
                if (Mathf.Approximately(newValue, 0)) EnergyReset?.Invoke();
            }
        }

        #endregion // ENERGY

        #region STAMINA

        public UnitStaminaState StaminaState => _staminaState;
        [SerializeField] private UnitStaminaState _staminaState;

        public event Action StaminaChanged
        {
            add => StaminaState.StaminaChanged += value;
            remove => StaminaState.StaminaChanged -= value;
        }
        public event Action MaxStaminaChanged
        {
            add => StaminaState.MaxStaminaChanged += value;
            remove => StaminaState.MaxStaminaChanged -= value;
        }

        public bool IsStaminaFull => StaminaState.IsStaminaFull;

        public float MaxStamina
        {
            get => StaminaState.MaxStamina;
            set => StaminaState.MaxStamina = value;
        }

        public float Stamina
        {
            get => StaminaState.Stamina;
            set => StaminaState.Stamina = value;
        }

        #endregion

        #region USERNAME

        [SyncVar, SerializeField] private string _playerId;
        [SyncVar, SerializeField] private string _playerName;

        [CanBeNull] public string PlayerId => _playerId;
        [CanBeNull] public string PlayerName => _playerName;

        #endregion // USERNAME

        #region EXP

        public event IExp.Changed ExpChanged;

        [SyncVar(hook = nameof(OnExpChanged))] private int _exp;

        public int Exp
        {
            get => _exp;
            set
            {
                int prev = _exp;
                _exp = value;
                OnExpChanged(prev, _exp);
            }
        }

        private void OnExpChanged(int oldValue, int newValue)
        {
            if (oldValue != newValue)
                ExpChanged?.Invoke(newValue - oldValue);
        }

        #endregion // EXP

        #region TARGET

        public event Action<NetworkIdentity> TargetChanged;

        [SyncVar(hook = nameof(OnTargetChanged))]
        private NetworkIdentity _target;

        // TODO: тут возможно избыточная синхронизация. Сейчас сюда пишет MicroAI которая работает только на клиенте. А слушает это DamageZonePlayerSelector который на том же клиенте. Можно без сетки передать.
        public NetworkIdentity Target
        {
            get => _target;
            set
            {
                NetworkIdentity prev = _target;
                _target = value;
                OnTargetChanged(prev, _target);
            }
        }

        private void OnTargetChanged(NetworkIdentity oldValue, NetworkIdentity newValue)
        {
            if (oldValue != newValue)
                TargetChanged?.Invoke(newValue);
        }

        #endregion // TARGET

        #region PATRONS

        public event Action PatronsCountChanged;

        [SyncVar(hook = nameof(OnPatronsCountChanged))]
        private int _patronsCount;

        public int PatronsCount
        {
            get => _patronsCount;
            set =>
                NetworkHelper.ModifySyncVar(_patronsCount, value, v => { _patronsCount = v; }, OnPatronsCountChanged);
        }

        public int MaxPatrons => PropertiesProvider?.RangeWeapon?.PatronsCount ?? default;

        private void OnPatronsCountChanged(int oldValue, int newValue)
        {
            if (oldValue != newValue)
                PatronsCountChanged?.Invoke();
        }

        #endregion // PATRONS

        #region CAPTURED

        public event Action IsCapturedChanged;

        [SyncVar(hook = nameof(OnIsCapturedChanged))]
        private int _capturedCounter;

        public bool IsCaptured
        {
            get => _capturedCounter > 0;
            set => NetworkHelper.ModifySyncVar(
                _capturedCounter,
                _capturedCounter + (value ? 1 : -1),
                v =>
                {
                    _capturedCounter = v;
                    if (_capturedCounter < 0)
                    {
                        _capturedCounter = 0;
                        _logger.LogError($"netId: {netId} | Negative {nameof(_capturedCounter)}");
                    }
                },
                OnIsCapturedChanged);
        }

        private void OnIsCapturedChanged(int oldValue, int newValue)
        {
            _logger.Log($"netId: {netId} | {nameof(IsCaptured)}: {IsCaptured}, Counter: {_capturedCounter}");
            if ((oldValue <= 0 && newValue > 0) || (oldValue > 0 && newValue <= 0))
                IsCapturedChanged?.Invoke();
        }

        #endregion // CAPTURED

        #region UNIT_CONFIG

        public event Action<UnitConfig, UnitConfig> UnitConfigChanged;

        [SyncVar(hook = nameof(OnUnitConfigChanged))]
        private UnitConfig _unitConfig;

        public UnitConfig UnitConfig
        {
            get => _unitConfig;
            private set => NetworkHelper.ModifyObjectSyncVar(_unitConfig, value, v => _unitConfig = v, OnUnitConfigChanged);
        }

        private void OnUnitConfigChanged(UnitConfig oldUnit, UnitConfig newUnit)
        {
            if (oldUnit != newUnit)
            {
                SharedInit();

                PatronsCount = PropertiesProvider!.RangeWeapon?.PatronsCount ?? default;
                HealthState.Init(PropertiesProvider.Health);
                StaminaState.Init(PropertiesProvider.Stamina);

                _logger.LogInfo("UnitConfigChanged event will be invoked.");
                UnitConfigChanged?.Invoke(oldUnit, newUnit);
                _logger.LogInfo("UnitConfigChanged event has been invoked.");
            }
        }

        #endregion // UNIT_CONFIG

        #region INVINCIBLE

        public event Action IsInvincibleChanged;

        [SyncVar(hook = nameof(OnIsInvincibleChanged))]
        private bool _isInvincible;

        public bool IsInvincible
        {
            get => _isInvincible;
            set => NetworkHelper.ModifySyncVar(_isInvincible, value, v => { _isInvincible = v; }, OnIsInvincibleChanged);
        }

        public Vector3 Position => transform.position;

        private void OnIsInvincibleChanged(bool oldValue, bool newValue)
        {
            if (oldValue != newValue)
                IsInvincibleChanged?.Invoke();
        }

        #endregion // INVINCIBLE

        #region UNIT_TYPE

        public event Action UnitTypeChanged;

        [SyncVar(hook = nameof(OnUnitTypeChanged))]
        private BattleUnitType _unitType;

        public BattleUnitType UnitType
        {
            get => _unitType;
            set
            {
                BattleUnitType prev = _unitType;
                _unitType = value;
                OnUnitTypeChanged(prev, _unitType);
            }
        }

        private void OnUnitTypeChanged(BattleUnitType oldValue, BattleUnitType newValue)
        {
            if (oldValue != newValue)
                UnitTypeChanged?.Invoke();
        }

        #endregion // UNIT_TYPE

        #region BEHAVIOUR_STATE

        public event Action BehaviourStateChanged;

        [SyncVar(hook = nameof(OnBehaviourStateChanged))]
        private BehaviourState _behaviourState;

        public BehaviourState BehaviourState
        {
            get => _behaviourState;
            set
            {
                if (!CanSwitchStateTo(value))
                    return;

                var prev = _behaviourState;
                _behaviourState = value;

                // Для клиент-хоста событие дернется через хук
                var isClientHost = isClient && isServer;
                if (!isClientHost)
                    OnBehaviourStateChanged(prev, value);
            }
        }

        private void OnBehaviourStateChanged(BehaviourState oldValue, BehaviourState newValue)
        {
            SyncRandom();

            if (oldValue != newValue)
                BehaviourStateChanged?.Invoke();
        }

        private bool CanSwitchStateTo(BehaviourState nextState)
        {
            if (nextState == BehaviourState.Dead)
                return true;

            BehaviourState currentState = BehaviourState;

            if (currentState == BehaviourState.Dead && IsDead)
                return false;

            // TODO: абилку надо бы переделать на бехавиор стейт для единообразия. И тогда этой костыль выпилить
            if (UnitNetwork.ImpactRoster.Ultimate.IsCasting && nextState != BehaviourState.None)
                return false;

            return nextState switch
            {
                BehaviourState.Reload when _unitNetwork.UnitNetworkEffects.Params.InfiniteAmmo => false,
                BehaviourState.Reload when PropertiesProvider?.RangeWeapon == null => false,
                BehaviourState.Reload when currentState != BehaviourState.None => false,
                BehaviourState.Reload when PatronsCount >= PropertiesProvider.RangeWeapon?.PatronsCount => false,
                BehaviourState.MeleeAttack when currentState != BehaviourState.None => false,
                _ => true
            };
        }

        #endregion // BEHAVIOUR_STATE

        public int DeathsCounter => _deathsCounter;

        [SyncVar] private int _deathsCounter;
        [SyncVar] private int _heroLevel;

        private readonly SyncList<Equipment> _equipments = new();

        [SerializeField] private UnitRandomConfig _randomConfig;

        private UnitNetwork _unitNetwork;
        private ILogger<UnitNetworkState> _logger;
        private float _lastSingleAttackFixedTime = float.MinValue;
        private UnitPropertiesProviderFactory _propertiesProviderFactory;

        float IDamageReceiverState.BodyRadius => ViewConfig != null ? ViewConfig.BodyFxRadius : 0f;

        public UnitRandom Random { get; private set; }
        [CanBeNull] public UnitViewConfig ViewConfig { get; private set; }
        [CanBeNull] public IReadOnlyUnitPropertiesProvider PropertiesProvider { get; private set; }
        public bool IsInitialized => _isUnitInitialized && _isNetworkInitialized;

        public float Defense { get; } = 0;

        public string Id => UnitConfig?.Id ?? string.Empty;
        [CanBeNull] public UnitNetwork UnitNetwork => _unitNetwork ??= this != null ? GetComponent<UnitNetwork>() : null;
        public bool SingleAttackInProcess =>
            PropertiesProvider?.RangeWeapon != null &&
            PropertiesProvider.RangeWeapon.Cooldown + _lastSingleAttackFixedTime > Time.fixedTime;

        private bool _isUnitInitialized;
        private bool _isNetworkInitialized;

        public ILootBoxStateCounter LootBoxCounter => _lootBoxCounterLazy ??= new LootBoxStateCounter(_dict);
        private readonly SyncDictionary<LootBoxType, int> _dict = new();
        private LootBoxStateCounter _lootBoxCounterLazy;

        [Inject]
        private void Constructor(ILogger<UnitNetworkState> logger,
            UnitPropertiesProviderFactory propertiesProviderFactory)
        {
            _logger = logger;
            _propertiesProviderFactory = propertiesProviderFactory;
        }

        private void Awake()
        {
            _healthState ??= GetComponent<UnitHealthState>();
            _staminaState ??= GetComponent<UnitStaminaState>();
        }

        private void OnDestroy() =>
            HealthState.HealthChanged -= OnHealthChanged;

        public override void OnStartClient()
        {
            if (isClientOnly) SharedInit();
            SetNetworkInitialized(true);
        }

        public override void OnStopClient()
        {
            if (isClientOnly) SharedDeinit();
            SetNetworkInitialized(false);
        }

        public override void OnStartServer()
        {
            SetNetworkInitialized(true);
        }

        public override void OnStopServer()
        {
            SetNetworkInitialized(false);
        }

        //TODO: Практически изначально поднимался вопрос о том, что метод Init надо перенести в UnitNetwork.
        //Соответственно ссылки на всякие стейты (типа HealthState, AbilityModel) тоже стоит в UnitNetwork перенести.

        // Серверная инициализация юнита
        [Server]
        public void Init(BattleUnitType unitType, UnitConfig unitConfig, IEnumerable<Equipment> equipments, string playerId,
            string playerName, int heroLevel)
        {
            Energy = default;
            UnitType = unitType;
            _playerId = playerId;
            _playerName = playerName;
            _heroLevel = heroLevel;
            _unitConfig = unitConfig;
            _equipments.AddRange(equipments);

            OnUnitConfigChanged(null, unitConfig);
        }

        private void SharedInit()
        {
            _logger.LogInfo("SharedInit will be invoked.");

            SyncRandom();
            _logger.LogInfo("Random has been synchronized.");

            PropertiesProvider = _propertiesProviderFactory.Create(UnitConfig, _heroLevel, _equipments);

            //TODO: Надо переделать. Вью конфиг превратить в ScriptableObject с обычными параметрами.
            //Все коллайдеры держать в базовом префабе юнитов и настраивать здесь по параметрам из ВьюКонфига.
            //ViewConfig при этом стоит переименовать в UnitPhysicConfig например.
            //Сейчас меняются только зоны дамага в переключаемых вью конфигах. Поэтому так должно работать.
            if (ViewConfig == null)
            {
                ViewConfig = Instantiate(UnitConfig.ViewConfig, transform);
                ViewConfig!.Init(UnitType);
            }

            if (!_isUnitInitialized)
            {
                SetUnitInitialized(true);
                HealthState.HealthChanged += OnHealthChanged;
            }

            _logger.LogInfo("SharedInit has been invoked.");
        }

        private void SharedDeinit()
        {
            Deinitializing?.Invoke(this);

            if (ViewConfig != null)
            {
                Destroy(ViewConfig.gameObject);
                ViewConfig = null;
            }

            if (_isUnitInitialized)
                SetUnitInitialized(false);

            HealthState.HealthChanged -= OnHealthChanged;
        }

        public void SwitchUnitConfig(UnitConfig newUnitConfig)
        {
            //TODO: Тут следовало бы переинитить макимальное здоровье, но это ломает систему HealthIncreasingService
            //HealthState.MaxHealth = newUnitConfig.Health;
            _logger.LogInfo($"MaxHealth -> {HealthState.MaxHealth} HP.");

            _logger.LogInfo($"Config will be switched from '{UnitConfig.Id}' to '{newUnitConfig.Id}'.");
            UnitConfig = newUnitConfig;
            _logger.LogInfo($"Config switched to '{newUnitConfig.Id}'.");
        }

        public void SyncRandom()
        {
            Random ??= new UnitRandom(_randomConfig);

            var tick = Mathf.Clamp((int)Health, 0, Random.Config.Max);
            Random.Sync(tick);
        }

        public void SingleAttack()
        {
            _lastSingleAttackFixedTime = Time.fixedTime;
            SingleAttacked?.Invoke();

            if (isServerOnly)
                SingleAttackSync();
        }

        [ClientRpc]
        private void SingleAttackSync()
        {
            _lastSingleAttackFixedTime = Time.fixedTime;
            SingleAttacked?.Invoke();
        }

        private void SetUnitInitialized(bool initialized)
        {
            if (_isUnitInitialized == initialized) return;

            _isUnitInitialized = initialized;
            if (IsInitialized) Initialized?.Invoke(this);
        }

        private void SetNetworkInitialized(bool initialized)
        {
            if (_isNetworkInitialized == initialized) return;

            _isNetworkInitialized = initialized;
            if (IsInitialized) Initialized?.Invoke(this);
        }
    }
}