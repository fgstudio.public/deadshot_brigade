using System;
using Mirror;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States
{
    [Serializable]
    public sealed class UnitEffectState
    {
        [CanBeNull] public UnitEffectConfig Config;
        [CanBeNull] public NetworkIdentity Target;
        public float EndTime;

        //TODO: это нужно для эффекта AreaBulletEffects. Cтоит вообще выпилить и спаунить абилкой объект.
        public Vector3 Position;
        public float RotationY;

        public UnitEffectState()
        {
            Config = default;
            Target = default;
            EndTime = default;
            Position = default;
            RotationY = default;
        }

        public UnitEffectState([NotNull] UnitEffectConfig config, [NotNull] NetworkIdentity source,
            [CanBeNull] NetworkIdentity target = null, float endTime = float.MaxValue)
        {
            Config = config;
            Target = target;
            EndTime = endTime;
            Position = source.transform.position;
            RotationY = source.transform.eulerAngles.y;
        }
    }
}