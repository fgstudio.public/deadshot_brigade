using System;
using Mirror;
using Frostgate.RiftHunters.Core.Battle.Shared.Looting;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    /// <summary>
    /// Счётчик собранных лутбоксов. Синхронизируется по сети через SyncDictionary.
    /// </summary>
    public sealed class LootBoxStateCounter : ILootBoxStateCounter
    {
        private const int MinCount = 0;

        public event Action<LootBoxType> Changed;

        private readonly SyncDictionary<LootBoxType, int> _dictionary;

        public LootBoxStateCounter(SyncDictionary<LootBoxType, int> dictionary)
        {
            _dictionary = dictionary;
            _dictionary.Callback += OnCallback;
        }

        public int GetCount(LootBoxType type) => _dictionary.TryGetValue(type, out int count) ? count : MinCount;

        public void Increase(LootBoxType type)
        {
            if (!_dictionary.ContainsKey(type)) _dictionary[type] = MinCount;
            _dictionary[type]++;
        }

        public void Decrease(LootBoxType type)
        {
            if (!_dictionary.ContainsKey(type)) return;
            int value = --_dictionary[type];
            if (value <= MinCount) _dictionary.Remove(type);
        }

        private void OnCallback(SyncIDictionary<LootBoxType, int>.Operation op, LootBoxType key, int item) =>
            Changed?.Invoke(key);
    }
}
