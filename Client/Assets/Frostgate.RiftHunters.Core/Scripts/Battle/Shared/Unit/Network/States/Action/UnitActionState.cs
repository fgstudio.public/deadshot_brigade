using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States
{
    public class UnitActionState : NetworkBehaviour
    {
        public event Action<UnitActionConfig> OnBeginAction;
        public event Action OnActionTypeChanged;

        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private Transform _transform;
        [SerializeField] private UnitNetworkState _unitNetworkState;

        public bool DamageImmunity => CurrentActionType != UnitActionTypes.None && _currentAction.DamageImmunity;
        public UnitActionTypes CurrentActionType => InProcess ? _currentAction.Type : UnitActionTypes.None;

        private bool InProcess => _currentAction != null;

        private UnitActionConfig _currentAction;
        private float _time;
        private float _unitAngleY;
        private bool _needReloadingAfterAction;

        [Server]
        public void TryExecute(UnitActionConfig actionConfig, float unitAngleY)
        {
            if (actionConfig.StaminaCost > 0 && _unitNetworkState.Stamina < actionConfig.StaminaCost)
                return;

            if (CurrentActionType == actionConfig.Type)
                return;

            // В стане и смерти на юнита не действуют не смертельные экшены.
            var inStun = _unitNetworkState.BehaviourState == BehaviourState.Stun;
            if (inStun || _unitNetworkState.IsDead && !actionConfig.IsDeath)
                return;

            _unitNetworkState.Stamina -= actionConfig.StaminaCost;

            // Останавливаем перезараядку и возобнавляем её после завершения действия.
            if (_unitNetworkState.BehaviourState == BehaviourState.Reload)
            {
                _unitNetworkState.BehaviourState = BehaviourState.None;
                _needReloadingAfterAction = true;
            }

            // Угол поворота юнита точечно пробросим вместе с действием, чтобы нормально засинкалось.
            InitAction(actionConfig, unitAngleY);
            RpcOnBeginAction(actionConfig, unitAngleY);
        }

        [Client]
        public void ClientInitAction(UnitActionConfig actionConfig, float unitAngleY) =>
            InitAction(actionConfig, unitAngleY);

        [ClientRpc]
        private void RpcOnBeginAction(UnitActionConfig actionConfig, float unitAngleY)
        {
            if (!isClientOnly)
                return;

            InitAction(actionConfig, unitAngleY);
        }

        private void InitAction(UnitActionConfig actionConfig, float unitAngleY)
        {
            if (CurrentActionType == actionConfig.Type)
                return;

            _unitAngleY = unitAngleY;
            _currentAction = actionConfig;
            _time = 0;

            OnBeginAction?.Invoke(actionConfig);
            OnActionTypeChanged?.Invoke();
        }

        private void FixedUpdate()
        {
            if (!InProcess)
                return;

            _rigidbody.velocity = new Vector3(0, _rigidbody.velocity.y, 0);

            var forwardDelta =
                _currentAction.ForwardMoveCurve.Evaluate(_time + Time.fixedDeltaTime) - _currentAction.ForwardMoveCurve.Evaluate(_time);

            _rigidbody.MovePosition(_transform.position + _transform.forward * forwardDelta);
            _transform.eulerAngles = new Vector3(0, _unitAngleY, 0);

            _time += Time.fixedDeltaTime;

            if (_time > _currentAction.Duration)
                Reset();
        }

        private void Reset()
        {
            _currentAction = null;
            OnActionTypeChanged?.Invoke();

            if (_needReloadingAfterAction)
            {
                _needReloadingAfterAction = false;

                if (isServer && _unitNetworkState.BehaviourState == BehaviourState.None)
                    _unitNetworkState.BehaviourState = BehaviourState.Reload;
            }
        }
    }
}