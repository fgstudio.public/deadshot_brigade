using System;
using System.Collections;
using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public class UnitNetworkPosition : NetworkBehaviour
    {
        [SerializeField] private Transform _transform;
        [SerializeField] private Transform _viewContainer;
        [SerializeField] private float _accuracy = 0.05f;
        [SerializeField] private float _lerpDuration = 1;
        [SerializeField, Range(0, 2)] private float _clientSyncInterval = 0.03f;
        [SerializeField, Range(0, 2)] private float _serverSyncInterval = 0.3f;

        private Coroutine _lerpCoroutine;
        private Vector3 _lastPosition;

        public override void OnStartClient()
        {
            if (!isLocalPlayer)
                return;

            // На клиенте игрок сам отсылает свою позицию серверу.
            // Отсылает достаточно часто чтобы логика на сервере имела более точные данные.
            StartCoroutine(SyncPosition(_clientSyncInterval, () =>
            {
                SendTeleportToServer(_transform.position);
            }));
        }

        public override void OnStopClient()
        {
            if (!isLocalPlayer)
                return;

            StopAllCoroutines();
        }

        public override void OnStartServer()
        {
            // Сервер расылает игрокам позиции всех юнитов, кроме юнита игрока.
            // Это можно отсылать пореже.
            StartCoroutine(SyncPosition(_serverSyncInterval, () =>
            {
                SendTeleportToOtherClient(_transform.position);
            }));
        }

        public override void OnStopServer()
        {
            StopAllCoroutines();
        }

        [ClientRpc]
        public void SendTeleportToAllClient(Vector3 position) =>
            TeleportClientTo(position, false);

        [ClientRpc]
        private void SendTeleportToAllClientWithLerp(Vector3 position) =>
            TeleportClientTo(position, true);

        [ClientRpc(includeOwner = false)]
        private void SendTeleportToOtherClient(Vector3 position) =>
            TeleportClientTo(position, true);

        private void TeleportClientTo(Vector3 position, bool useLerp)
        {
            if (isServer || Vector3.Distance(position, _transform.position) < _accuracy)
                return;

            if (_lerpCoroutine != null)
                StopCoroutine(_lerpCoroutine);

            _lastPosition = position;

            if (useLerp)
            {
                _lerpCoroutine = StartCoroutine(LerpViewToCurrentPosition(position));
            }
            else
            {
                _transform.position = position;
                _viewContainer.localPosition = Vector3.zero;
            }
        }

        private IEnumerator LerpViewToCurrentPosition(Vector3 position)
        {
            var viewPosition = _viewContainer.position;
            _transform.position = position;
            _viewContainer.position = viewPosition;

            var lerpTime = 0f;

            while (lerpTime < _lerpDuration)
            {
                yield return new WaitForEndOfFrame();
                lerpTime += Time.deltaTime;
                var lerp = lerpTime / _lerpDuration;
                _viewContainer.localPosition = Vector3.Lerp(_viewContainer.localPosition, Vector3.zero, lerp);
            }

            _viewContainer.localPosition = Vector3.zero;
        }

        private IEnumerator SyncPosition(float syncInterval, Action syncAction)
        {
            while (true)
            {
                yield return new WaitForSeconds(syncInterval);

                if (Vector3.Distance(_lastPosition, _transform.position) < _accuracy)
                    continue;

                _lastPosition = _transform.position;

                syncAction?.Invoke();
            }
        }

        [Command]
        private void SendTeleportToServer(Vector3 position) =>
            _transform.position = position;
    }
}