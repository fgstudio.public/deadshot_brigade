using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public partial class UnitNetworkInput
    {
        [SyncVar] private bool _shootingEnabled;
        private bool _localShootingEnabled;
        public bool ShootingEnabled => isLocalPlayer ? _localShootingEnabled : _shootingEnabled;

        public void SetShooting(bool enabled)
        {
            if (!this.enabled || ShootingEnabled == enabled)
                return;

            _localShootingEnabled = enabled;

            if (isServer) _shootingEnabled = enabled;
            else CmdSetShooting(enabled);
        }

        [Command]
        private void CmdSetShooting(bool enabled) => _shootingEnabled = enabled;
    }
}