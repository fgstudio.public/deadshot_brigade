﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States
{
    public interface IUnitHealthState
    {
        delegate void ValueChanged(float oldValue, float newValue);

        event ValueChanged MaxHealthChanged;
        event ValueChanged HealthChanged;

        float MaxHealth { get; set; }
        float Health { get; set; }
        bool IsHealthFull { get; }
        bool IsDead { get; }
    }
}
