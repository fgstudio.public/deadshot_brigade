﻿using Mirror;
using UnityEngine;
using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States
{
    public sealed class UnitStaminaState : NetworkBehaviour
    {
        public event Action MaxStaminaChanged;
        public event Action StaminaChanged;

        public float MaxStamina
        {
            get => _maxStamina;
            set => SetMaxStamina(value);
        }

        public float Stamina
        {
            get => _stamina;
            set => SetStamina(value);
        }

        public bool IsStaminaFull => Stamina >= MaxStamina;

        [SyncVar(hook = nameof(OnMaxStaminaChangedHook))]
        private float _maxStamina;

        [SyncVar(hook = nameof(OnStaminaChangedHook))]
        private float _stamina;

        [Server]
        public void Init(float stamina)
        {
            MaxStamina = stamina;
            Stamina = stamina;
        }

        private void SetMaxStamina(float value)
        {
            float oldMaxStamina = _maxStamina;
            _maxStamina = value;

            OnValueChanged(oldMaxStamina, _maxStamina, MaxStaminaChanged);
        }

        private void SetStamina(float value)
        {
            float oldStamina = _stamina;
            float newStamina = Mathf.Clamp(value, 0, MaxStamina);
            _stamina = newStamina;

            OnValueChanged(oldStamina, _stamina, StaminaChanged);
        }

        private void OnValueChanged(float oldValue, float newValue, Action @delegate)
        {
            float diff = newValue - oldValue;
            if (!Mathf.Approximately(diff, 0f))
                @delegate?.Invoke();
        }

        private void OnMaxStaminaChangedHook(float oldValue, float newValue) =>
            OnValueChanged(oldValue, newValue, MaxStaminaChanged);

        private void OnStaminaChangedHook(float oldValue, float newValue) =>
            OnValueChanged(oldValue, newValue, StaminaChanged);
    }
}
