using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    [UsedImplicitly]
    public sealed class UnitColliderCollection : UnitBaseCollection<Collider>
    {
        public UnitColliderCollection([NotNull] ILogger<UnitColliderCollection> logger)
            : base(logger) { }
    }
}
