namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public interface IEnergy
    {
        delegate void Changed(float diff);
        event Changed EnergyChanged;

        float Energy { get; }
        float MaxEnergy { get; }
        bool IsEnergyFull { get; }
    }
}