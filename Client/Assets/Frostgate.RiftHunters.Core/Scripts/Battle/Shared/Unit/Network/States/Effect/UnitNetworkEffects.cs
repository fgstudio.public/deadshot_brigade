using Mirror;
using System;
using System.Linq;
using System.Collections.Generic;
using static Mirror.SyncList<Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States.UnitEffectState>;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States
{
    public class UnitNetworkEffects : NetworkBehaviour
    {
        public event Action<UnitEffectState> OnAdd;
        public event Action<UnitEffectState> OnRemove;

        public UnitEffectParams Params { get; } = new();

        private readonly SyncList<UnitEffectState> _states = new();
        public IEnumerable<UnitEffectState> States => _states;

        private readonly List<UnitEffectState> _removeList = new();

        public override void OnStartServer()
        {
            Initialize();
        }

        public override void OnStopServer()
        {
            RemoveAll();
            Deinitialize();
        }

        public override void OnStartClient()
        {
            if (isClientOnly) Initialize();
        }

        public override void OnStopClient()
        {
            if (isClientOnly) Deinitialize();
        }

        private void OnDestroy() =>
            _states.Callback -= OnChanged;

        private void Initialize()
        {
            _states.Callback += OnChanged;
            RecalculateParams();

            foreach (UnitEffectState state in _states)
                OnAdd?.Invoke(state);
        }

        private void Deinitialize()
        {
            _states.Callback -= OnChanged;
            RecalculateParams();
        }

        [Server]
        public void Add(UnitEffectState state)
        {
            if (TryGetState(state.Config, out var sameState))
            {
                sameState.EndTime = state.EndTime;
                UpdateEffectStateOnClient(state.Config, state.EndTime);
                return;
            }

            if (state.Config != null)
                RemoveEffectsOfType(state.Config.Type);

            _states.Add(state);
        }

        [ClientRpc]
        private void UpdateEffectStateOnClient(UnitEffectConfig config, float endTime)
        {
            if (TryGetState(config, out var state))
                state.EndTime = endTime;
        }

        [Server]
        public void Remove(UnitEffectState state)
        {
            if (_states.Contains(state))
                _states.Remove(state);
        }

        public bool TryGetState(UnitEffectConfig config, out UnitEffectState stateByType)
        {
            stateByType = _states.FirstOrDefault(s => s.Config == config);
            return stateByType != null;
        }

        [Server]
        public bool Contains(UnitEffectState state) =>
            _states.Contains(state);

        [Server]
        public void RemoveAll()
        {
            for (int i = _states.Count - 1; i >= 0; i--)
                Remove(_states[i]);
        }

        private void RemoveEffectsOfType(UnitEffectTypes effectType)
        {
            IEnumerable<UnitEffectState> statesToRemove = States
                .Where(s => s.Config != null)
                .Where(s => s.Config.Type == effectType);

            _removeList.Clear();
            _removeList.AddRange(statesToRemove);

            foreach (UnitEffectState effectState in _removeList)
                Remove(effectState);
        }

        private void OnChanged(Operation op, int index, UnitEffectState oldItem, UnitEffectState newState)
        {
            switch (op)
            {
                case Operation.OP_ADD:
                    SharedAdd(newState);
                    break;
                case Operation.OP_REMOVEAT:
                    SharedRemove(oldItem);
                    break;
            }
        }

        private void SharedAdd(UnitEffectState state)
        {
            OnAdd?.Invoke(state);
            RecalculateParams();
        }

        private void SharedRemove(UnitEffectState state)
        {
            OnRemove?.Invoke(state);
            RecalculateParams();
        }

        private void RecalculateParams() =>
            Params.Recalculate(_states);
    }
}