using Mirror;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public sealed class UnitNetworkRotation : NetworkBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _transform;
        [SerializeField, Required, ChildGameObjectsOnly] private UnitNetworkInput _unitNetworkInput;

        public UnityEvent RotationSet { get; } = new();

        public void SetRotation(float yRotation)
        {
            if (!isServer) return;
            RotateTo(yRotation);
            SendRotationToAllClient(yRotation);
        }

        [ClientRpc]
        private void SendRotationToAllClient(float yRotation)
        {
            if (isClientOnly)
                RotateTo(yRotation);
        }

        private void RotateTo(float yRotation)
        {
            Vector3 eulerAngles = _transform.eulerAngles;
            eulerAngles.y = yRotation;

            _transform.eulerAngles = eulerAngles;
            _unitNetworkInput.YRotation = yRotation;

            RotationSet?.Invoke();
        }
    }
}
