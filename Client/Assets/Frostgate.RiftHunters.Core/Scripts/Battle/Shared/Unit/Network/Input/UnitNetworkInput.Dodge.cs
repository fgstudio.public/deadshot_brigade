using System;
using Mirror;
using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public partial class UnitNetworkInput
    {
        //TODO: можно выпилить этот евент и подвязыватся на UnitActionState. Тогда можно выпилить сетевой вызов CmdExecuteDodge.
        public event Action DodgeExecuting;

        private bool CanExecuteDodge =>
            enabled && !IsStunned && !IsDead &&
            UnitState.Stamina >= DodgeCost &&
            (CurrentState == BehaviourState.None ||
             CurrentState == BehaviourState.Reload ||
             CurrentState == BehaviourState.MeleeAttack ||
             CurrentAction == UnitActionTypes.None ||
             CurrentAction == UnitActionTypes.PowerDamage);

        private float DodgeCost =>
            UnitState.UnitConfig.UnitActionRoster.TryGetAction(
                UnitActionTypes.Dodge, out UnitActionConfig dodgeAction)
                ? dodgeAction.StaminaCost : default;

        public void ExecuteDodge()
        {
            if (!CanExecuteDodge) return;

            var dirByYRotation = CalculateDodgeDirByYRotation();

            if (isServer)
            {
                ExecuteDodgeOnServer(dirByYRotation);
            }
            else
            {
                var roster = _unitNetwork.UnitNetworkState.UnitConfig.UnitActionRoster;
                //TODO: для кувырка такой финт пока сработает, так как кувырков на юните не больше одного.
                //Но если их будет больше одного или таким образом будем дёргать другой экшн, то будет расхождение.
                //Желательно это переделать так чтобы от клиента отправлялся конкретный экшн а не тип экшена.
                if (roster.TryGetAction(UnitActionTypes.Dodge, out var action))
                {
                    SendExecuteDodgeToServer(dirByYRotation);
                    _unitNetwork.UnitActionState.ClientInitAction(action, dirByYRotation);
                }
            }
        }

        private float CalculateDodgeDirByYRotation() =>
            _move != Vector2.zero
                ? YRotation - Vector2.SignedAngle(Vector2.up, Move)
                : YRotation;

        [Client]
        private void SendExecuteDodgeToServer(float dirByYRotation)
        {
            DodgeExecuting?.Invoke();
            CmdExecuteDodge(dirByYRotation);
        }

        [Command]
        private void CmdExecuteDodge(float dirByYRotation) => ExecuteDodgeOnServer(dirByYRotation);

        [Server]
        private void ExecuteDodgeOnServer(float dirByYRotation)
        {
            if (CanExecuteDodge)
            {
                DodgeExecuting?.Invoke();
                _unitNetwork.TryExecuteUnitAction(UnitActionTypes.Dodge, dirByYRotation);
            }
        }
    }
}