using Mirror;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public partial class UnitNetworkInput
    {
        private bool CanMeleeAttack =>
            enabled && !IsStunned && !IsDead &&
            CurrentState == BehaviourState.None &&
            CurrentAction == UnitActionTypes.None;

        public void ExecuteMeleeAttack()
        {
            if (!CanMeleeAttack) return;
            if (isServer) MeleeAttackOnServer();
            else CmdMeleeAttack();
        }

        [Command]
        private void CmdMeleeAttack() =>
            MeleeAttackOnServer();

        private void MeleeAttackOnServer()
        {
            if (CanMeleeAttack)
                UnitState.BehaviourState = BehaviourState.MeleeAttack;
        }
    }
}
