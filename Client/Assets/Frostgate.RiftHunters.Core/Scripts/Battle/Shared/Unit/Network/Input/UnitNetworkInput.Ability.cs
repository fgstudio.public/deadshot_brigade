using System;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public partial class UnitNetworkInput
    {
        public event Action AbilityExecuting;

        private IServerImpactModel AbilityImpactModel => _unitNetwork.ImpactRoster.Ability;

        public void ExecuteAbility()
        {
            if (!CanExecuteImpact(AbilityImpactModel)) return;
            if (isClientOnly) SendExecuteAbilityToServer();
            if (isServer) ExecuteAbilityOnServer();
        }

        [Client]
        private void SendExecuteAbilityToServer()
        {
            AbilityExecuting?.Invoke();
            CmdExecuteAbility();
        }

        [Command]
        private void CmdExecuteAbility() =>
            ExecuteAbilityOnServer();

        [Server]
        private void ExecuteAbilityOnServer()
        {
            if (CanExecuteImpact(AbilityImpactModel))
            {
                AbilityExecuting?.Invoke();
                AbilityImpactModel.Execute();
            }
        }
    }
}
