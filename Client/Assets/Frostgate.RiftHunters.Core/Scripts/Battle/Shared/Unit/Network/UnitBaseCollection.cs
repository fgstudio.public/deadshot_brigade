using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public abstract class UnitBaseCollection<TKey> : ObjectBaseCollection<TKey, UnitNetwork>
    {
        protected UnitBaseCollection([NotNull] ILogger logger) : base(logger) { }

        protected sealed override string ExtractName(UnitNetwork unit)
        {
            if (unit.UnitNetworkState?.UnitConfig != null)
                return unit.UnitNetworkState.UnitConfig.Id;

            if (unit.Identity != null)
                return unit.Identity.netId.ToString();

            return string.Empty;
        }
    }
}
