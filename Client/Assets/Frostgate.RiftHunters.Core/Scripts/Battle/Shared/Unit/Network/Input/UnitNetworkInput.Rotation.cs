using System;
using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public partial class UnitNetworkInput
    {
        public event Action<float> OnSetYRotation;

        [SyncVar, SerializeField] private float _yRotation;

        private float _localYRotation;
        public float YRotation
        {
            get => isLocalPlayer ? _localYRotation : _yRotation;
            set
            {
                if (!enabled) return;

                _localYRotation = value;

                if (isServer)
                {
                    _yRotation = value;
                    OnSetYRotation?.Invoke(_yRotation);
                }
                else
                {
                    CmdSetRotation(value);
                }
            }
        }

        [Command]
        private void CmdSetRotation(float rotation)
        {
            _yRotation = rotation;
            OnSetYRotation?.Invoke(_yRotation);
        }
    }
}
