using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States
{
    public class UnitEffectParams
    {
        public event Action Changed;

        public bool PiercingBullets { get; private set; }
        public bool DisableEnergyAccumulation { get; private set; }
        public bool IgnoreEnemyDamageZone { get; private set; }
        public bool InfiniteAmmo { get; private set; }

        public float MoveSpeedMultiplier { get; private set; } = 1;
        public float AttackMultiplier { get; private set; } = 1;
        public float AttackCooldownMultiplier { get; private set; } = 1;
        public float EnergyAccumulationMultiplier { get; private set; } = 1;
        public UnitNetwork AgroTarget { get; private set; }

        public DamageZonesConfig DamageZonesConfig { get; private set; }
        public IHpAutoRecoveryConfig HpAutoRecoveryConfig { get; private set; }
        public IEnergyAutoRecoveryConfig EnergyAutoRecoveryConfig { get; private set; }
        public IStaminaAutoRecoveryConfig StaminaAutoRecoveryConfig { get; private set; }

        public void Recalculate(IEnumerable<UnitEffectState> states)
        {
            Reset();

            foreach (var state in states)
                switch (state.Config.Type)
                {
                    case UnitEffectTypes.MoveSpeedMultiplier:
                        MoveSpeedMultiplier = state.Config.Value;
                        break;
                    case UnitEffectTypes.AttackMultiplier:
                        AttackMultiplier = state.Config.Value;
                        break;
                    case UnitEffectTypes.AttackCooldownMultiplier:
                        AttackCooldownMultiplier = state.Config.Value;
                        break;
                    case UnitEffectTypes.EnergyAccumulationMultiplier:
                        EnergyAccumulationMultiplier = state.Config.Value;
                        break;
                    case UnitEffectTypes.DamageZone:
                        DamageZonesConfig = state.Config.DamageZones;
                        break;
                    case UnitEffectTypes.PiercingBullets:
                        PiercingBullets = true;
                        break;
                    case UnitEffectTypes.DisableEnergyAccumulation:
                        DisableEnergyAccumulation = true;
                        break;
                    case UnitEffectTypes.Agro when state.Target != null:
                        AgroTarget = state.Target.GetComponent<UnitNetwork>();
                        break;
                    case UnitEffectTypes.IgnoreEnemyDamageZone:
                        IgnoreEnemyDamageZone = true;
                        break;
                    case UnitEffectTypes.HpAutoRecovery:
                        HpAutoRecoveryConfig = state.Config.HpAutoRecoveryConfig;
                        break;
                    case UnitEffectTypes.EnergyAutoRecovery:
                        EnergyAutoRecoveryConfig = state.Config.EnergyAutoRecoveryConfig;
                        break;
                    case UnitEffectTypes.StaminaAutoRecovery:
                        StaminaAutoRecoveryConfig = state.Config.StaminaAutoRecoveryConfig;
                        break;
                    case UnitEffectTypes.InfiniteAmmo:
                        InfiniteAmmo = true;
                        break;
                }

            Changed?.Invoke();
        }

        private void Reset()
        {
            PiercingBullets = false;
            DisableEnergyAccumulation = false;
            IgnoreEnemyDamageZone = false;
            InfiniteAmmo = false;
            MoveSpeedMultiplier = 1;
            AttackMultiplier = 1;
            AttackCooldownMultiplier = 1;
            EnergyAccumulationMultiplier = 1;
            DamageZonesConfig = null;
            AgroTarget = null;
            HpAutoRecoveryConfig = null;
            EnergyAutoRecoveryConfig = null;
            StaminaAutoRecoveryConfig = null;
        }
    }
}