using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public partial class UnitNetworkInput
    {
        [SyncVar, SerializeField] private Vector2 _move;

        private Vector2 _localMove;
        public Vector2 Move
        {
            get => isLocalPlayer && !UnitState.IsCaptured ? _localMove : _move;
            set
            {
                if (!enabled) return;
                if (IsStunned) value = Vector2.zero;

                _localMove = value;

                if (isServer) _move = value;
                else CmdSetMove(value);
            }
        }

        [Command]
        private void CmdSetMove(Vector2 move) => _move = move;
    }
}
