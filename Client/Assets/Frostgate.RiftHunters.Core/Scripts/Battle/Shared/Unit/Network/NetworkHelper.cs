using System;
using System.Runtime.CompilerServices;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public static class NetworkHelper
    {
        private static readonly ILogger logger = LoggerFactory.CreateLogger();

        public static void ModifySyncVar<T>(T currentValue, T newValue, Action<T> onSet, Action<T, T> onSyncHook)
            where T : IEquatable<T>
        {
            if (!NetworkServer.active)
                Log("Cant set value on Client", LogLevel.Warning);

            if (currentValue.Equals(newValue))
                return;

            onSet?.Invoke(newValue);

            if (!NetworkClient.active)
                onSyncHook?.Invoke(currentValue, newValue);
        }

        public static void ModifyObjectSyncVar<T>(T currentValue, T newValue, Action<T> onSet, Action<T, T> onSyncHook)
            where T : UnityEngine.Object
        {
            if (!NetworkServer.active)
                Log("Cant set value on Client", LogLevel.Warning);

            if (currentValue == newValue)
                return;

            onSet?.Invoke(newValue);

            if (!NetworkClient.active)
                onSyncHook?.Invoke(currentValue, newValue);
        }

        public static void ModifySyncEnum<T>(T currentValue, T newValue, Action<T> onSet, Action<T, T> onSyncHook)
            where T : Enum
        {
            if (!NetworkServer.active)
                Log("Cant set value on Client", LogLevel.Warning);

            if (currentValue.GetHashCode() == newValue.GetHashCode())
                return;

            onSet?.Invoke(newValue);

            if (!NetworkClient.active)
                onSyncHook?.Invoke(currentValue, newValue);
        }

        private static void Log(string message, LogLevel level, [CallerMemberName] string methodName = "") =>
            logger.Log($"{nameof(NetworkHelper)}.{methodName} | {message}", level);
    }
}