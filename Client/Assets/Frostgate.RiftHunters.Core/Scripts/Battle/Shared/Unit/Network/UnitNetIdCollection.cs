using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    [UsedImplicitly]
    public sealed class UnitNetIdCollection : UnitBaseCollection<uint>
    {
        public UnitNetIdCollection([NotNull] ILogger<UnitNetIdCollection> logger)
            : base(logger) { }
    }
}