using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public partial class UnitNetworkInput
    {
        public event Action<uint, UnitEmotionConfig> OnEmotion;

        public void InvokeEmotion(UnitEmotionConfig config)
        {
            UnityEngine.Debug.Log($"NetworkInput | EMOTION: local invoke - {config.name} sender |{netId}|");
            OnEmotion?.Invoke(netId, config);
        }
    }
}