using System;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public partial class UnitNetworkInput
    {
        public event Action UltimateExecuting;

        private IServerImpactModel UltimateImpactModel => _unitNetwork.ImpactRoster.Ultimate;

        public void ExecuteUltimate()
        {
            if (!CanExecuteImpact(UltimateImpactModel)) return;
            if (isClientOnly) SendExecuteUltimateToServer();
            if (isServer) ExecuteUltimateOnServer();
        }

        [Client]
        private void SendExecuteUltimateToServer()
        {
            UltimateExecuting?.Invoke();
            CmdExecuteUltimate();
        }

        [Command]
        private void CmdExecuteUltimate() =>
            ExecuteUltimateOnServer();

        [Server]
        private void ExecuteUltimateOnServer()
        {
            if (CanExecuteImpact(UltimateImpactModel))
            {
                UltimateExecuting?.Invoke();
                UltimateImpactModel.Execute();
                UnitState.Energy = default; // TODO: вынести это в отдельный сервис
            }
        }
    }
}
