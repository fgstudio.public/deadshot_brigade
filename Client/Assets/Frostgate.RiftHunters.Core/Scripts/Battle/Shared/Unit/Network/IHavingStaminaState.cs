namespace Frostgate.RiftHunters.Core.Battle.Shared.Stamina
{
    public interface IHavingStaminaState
    {
        float Stamina { get; set; }
        float MaxStamina { get; }
        bool IsStaminaFull { get; }
        bool IsDead { get; }
    }
}