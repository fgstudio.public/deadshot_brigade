namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public interface IExp
    {
        delegate void Changed(int diff);
        event Changed ExpChanged;

        int Exp { get; }
    }
}
