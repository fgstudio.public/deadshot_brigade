namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public interface IReadOnlyHealth
    {
        delegate void Changed(float diff);
        event Changed HealthChanged;

        float Health { get;}
        float MaxHealth { get;}
        bool IsDead { get; }
    }

    public interface IHealth : IReadOnlyHealth
    {
        new float Health { get; set; }
    }
}