﻿using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States
{
    public sealed class UnitHealthState : NetworkBehaviour, IUnitHealthState
    {
        public event IUnitHealthState.ValueChanged MaxHealthChanged;
        public event IUnitHealthState.ValueChanged HealthChanged;

        public float MaxHealth
        {
            get => _maxHealth;
            [Server] set => SetMaxHealth(value);
        }

        public float Health
        {
            get => _health;
            [Server] set => SetHealth(value);
        }

        public bool IsHealthFull => Health >= MaxHealth;
        public bool IsDead => Health <= 0;

        [SyncVar(hook = nameof(OnMaxHealthChangedHook))]
        private float _maxHealth;

        [SyncVar(hook = nameof(OnHealthChangedHook))]
        private float _health;

        [Server]
        public void Init(float maxHealth)
        {
            MaxHealth = maxHealth;
            Health = maxHealth;
        }

        private void SetMaxHealth(float value)
        {
            float oldMaxHealth = _maxHealth;
            _maxHealth = value;

            OnValueChanged(oldMaxHealth, _maxHealth, MaxHealthChanged);
        }

        private void SetHealth(float value)
        {
            float oldHealth = _health;
            float newHealth = Mathf.Clamp(value, 0, MaxHealth);
            _health = newHealth;

            OnValueChanged(oldHealth, _health, HealthChanged);
        }

        private void OnValueChanged(float oldValue, float newValue, IUnitHealthState.ValueChanged @delegate)
        {
            float diff = newValue - oldValue;
            if (diff != 0)
                @delegate?.Invoke(oldValue, newValue);
        }

        private void OnMaxHealthChangedHook(float oldValue, float newValue)
        {
            if (isClientOnly)
                OnValueChanged(oldValue, newValue, MaxHealthChanged);
        }

        private void OnHealthChangedHook(float oldValue, float newValue)
        {
            if (isClientOnly)
                OnValueChanged(oldValue, newValue, HealthChanged);
        }
    }
}