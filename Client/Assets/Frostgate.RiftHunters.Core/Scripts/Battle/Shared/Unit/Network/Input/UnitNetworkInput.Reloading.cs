using System;
using Mirror;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public partial class UnitNetworkInput
    {
        public event Action ReloadExecuting;

        private bool CanExecuteReloading => enabled && _unitNetwork.CanBeginReloading;

        public void ExecuteReloading()
        {
            if (!CanExecuteReloading) return;
            if (isServer) ExecuteReloadingOnServer();
            else SendExecuteReloadingToServer();
        }

        [Client]
        private void SendExecuteReloadingToServer()
        {
            ReloadExecuting?.Invoke();
            CmdExecuteReloading();
        }

        [Command]
        private void CmdExecuteReloading() =>
            ExecuteReloadingOnServer();

        [Server]
        private void ExecuteReloadingOnServer()
        {
            if (CanExecuteReloading)
            {
                ReloadExecuting?.Invoke();
                UnitState.BehaviourState = BehaviourState.Reload;
            }
        }
    }
}