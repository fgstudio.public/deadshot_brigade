using Mirror;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public partial class UnitNetworkInput : NetworkBehaviour
    {
        [SerializeField, Required] private UnitNetwork _unitNetwork;

        private UnitNetworkState UnitState => _unitNetwork.UnitNetworkState;
        private UnitActionState ActionState => _unitNetwork.UnitActionState;

        private UnitActionTypes CurrentAction => ActionState.CurrentActionType;
        private BehaviourState CurrentState => UnitState.BehaviourState;

        private bool IsStunned => CurrentState == BehaviourState.Stun;
        private bool IsDead => UnitState.IsDead;

        private bool CanExecuteImpact([NotNull] IServerImpactModel impactModel) =>
            enabled && !IsStunned && !IsDead &&
            CurrentAction == UnitActionTypes.None && impactModel.CanExecute;
    }
}