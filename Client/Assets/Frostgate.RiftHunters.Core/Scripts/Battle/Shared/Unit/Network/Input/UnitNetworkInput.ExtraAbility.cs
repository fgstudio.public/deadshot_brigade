using System;
using Mirror;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public partial class UnitNetworkInput
    {
        public event Action ExtraAbilityExecuting;

        private IServerImpactModel ExtraAbilityImpactModel => _unitNetwork.ImpactRoster.ExtraAbility;

        public void ExecuteExtraAbility()
        {
            if (!CanExecuteImpact(ExtraAbilityImpactModel)) return;
            if (isClientOnly) SendExecuteExtraAbilityToServer();
            if (isServer) ExecuteExtraAbilityOnServer();
        }

        [Client]
        private void SendExecuteExtraAbilityToServer()
        {
            ExtraAbilityExecuting?.Invoke();
            CmdExecuteExtraAbility();
        }

        [Command]
        private void CmdExecuteExtraAbility() =>
            ExecuteExtraAbilityOnServer();

        [Server]
        private void ExecuteExtraAbilityOnServer()
        {
            if (CanExecuteImpact(ExtraAbilityImpactModel))
            {
                ExtraAbilityExecuting?.Invoke();
                ExtraAbilityImpactModel.Execute();
            }
        }
    }
}
