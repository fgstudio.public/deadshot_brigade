using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public partial class UnitNetworkInput
    {
        [SyncVar] private Vector3 _aimDirection;
        private Vector3 _localAimDirection;
        public Vector3 AimDirection => isLocalPlayer ? _localAimDirection : _aimDirection;

        private bool CanSetAimDirection => enabled && !_unitNetwork.ShootingSharedLogic.NeedFreeze;

        public void SetAimDirection(Vector3 aimDirection)
        {
            if (!CanSetAimDirection || AimDirection == aimDirection)
                return;

            _localAimDirection = aimDirection;

            if (isServer)
                _aimDirection = aimDirection;
            else
                CmdSetAimDirection(aimDirection);
        }

        [Command]
        private void CmdSetAimDirection(Vector3 aimDirection)
        {
            if (CanSetAimDirection)
                _aimDirection = aimDirection;
        }
    }
}