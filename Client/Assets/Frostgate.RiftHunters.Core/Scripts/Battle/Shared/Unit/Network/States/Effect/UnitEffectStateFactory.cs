using Mirror;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States
{
    [UsedImplicitly]
    public sealed class UnitEffectStateFactory
    {
        private readonly SimulationTime _time;

        public UnitEffectStateFactory([NotNull] SimulationTime time) => _time = time;

        public UnitEffectState Create([NotNull] UnitEffectConfig effectConfig, [NotNull] NetworkIdentity identity) =>
            new
            (
                effectConfig,
                identity,
                DetectTarget(effectConfig, identity),
                CalcEndTime(effectConfig)
            );

        private NetworkIdentity DetectTarget([NotNull] UnitEffectConfig effectConfig, [NotNull] NetworkIdentity identity) =>
            effectConfig.Type == UnitEffectTypes.Agro ||
            effectConfig.Target == UnitEffectTarget.Self
                ? identity
                : null;

        private float CalcEndTime([NotNull] UnitEffectConfig effectConfig) =>
            _time.Time + effectConfig.Duration;
    }
}
