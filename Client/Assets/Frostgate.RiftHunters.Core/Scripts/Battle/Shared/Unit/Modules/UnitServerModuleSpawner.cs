using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Server.Objects.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(UnitServerModuleSpawner))]
    public sealed class UnitServerModuleSpawner : UnitModuleSpawner<UnitServerModule>
    {
        public override void OnStartServer() => Init();
        public override void OnStopServer() => Deinit();

        protected override UnitServerModule GetModulePrefab(UnitNetwork unit) =>
            unit.UnitNetworkState.UnitConfig?.ServerModule;
    }
}