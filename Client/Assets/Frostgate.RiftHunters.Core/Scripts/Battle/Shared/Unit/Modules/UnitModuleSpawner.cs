using Mirror;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Components;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    public abstract class UnitModuleSpawner<TModule> : NetworkBehaviour
        where TModule : ObjectModule<UnitNetwork>
    {
        [CanBeNull] // может стать null в Runtime
        [SerializeField, Required] private UnitNetwork _unit;

        private IPrefabFactory _prefabFactory;

        [CanBeNull] private TModule _spawnedModule;

        [Inject]
        private void MonoConstructor(IPrefabFactory prefabFactory) =>
            _prefabFactory = prefabFactory;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _unit ??= GetComponent<UnitNetwork>();

        protected void Init()
        {
            if (_unit != null)
            {
                Subscribe(_unit);
                UpdateModule(_unit);
            }
        }

        protected void Deinit()
        {
            if (_unit != null)
                Unsubscribe(_unit);

            if (_spawnedModule != null)
                UnspawnModule(_spawnedModule);
        }

        [CanBeNull]
        protected abstract TModule GetModulePrefab([NotNull] UnitNetwork unit);

        private void Subscribe([NotNull] UnitNetwork unit) =>
            unit.UnitNetworkState.UnitConfigChanged += OnUnitConfigChanged;

        private void Unsubscribe([NotNull] UnitNetwork unit) =>
            unit.UnitNetworkState.UnitConfigChanged -= OnUnitConfigChanged;

        private void OnUnitConfigChanged(UnitConfig _, UnitConfig __)
        {
            if (_unit != null)
                UpdateModule(_unit);
        }

        private void UpdateModule([NotNull] UnitNetwork unit)
        {
            if (_spawnedModule != null)
                UnspawnModule(_spawnedModule);

            TModule modulePrefab = GetModulePrefab(unit);
            _spawnedModule = SpawnModule(unit, modulePrefab);
        }

        private TModule SpawnModule([NotNull] UnitNetwork unit, [CanBeNull] TModule modulePrefab)
        {
            if (modulePrefab == null)
                return null;

            Transform moduleContainer = GetModuleContainer(unit);
            TModule spawnedModule = _prefabFactory.Instantiate<TModule>(modulePrefab, moduleContainer);
            spawnedModule.Init(unit);

            return spawnedModule;

        }

        private void UnspawnModule([NotNull] TModule module)
        {
            module.Deinit();
            Destroy(module.gameObject);
        }

        private Transform GetModuleContainer([NotNull] UnitNetwork unit) =>
            unit.transform;
    }
}