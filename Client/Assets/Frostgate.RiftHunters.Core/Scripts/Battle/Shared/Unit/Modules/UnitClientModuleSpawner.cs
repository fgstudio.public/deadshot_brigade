using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Client.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(UnitClientModuleSpawner))]
    public sealed class UnitClientModuleSpawner : UnitModuleSpawner<UnitClientModule>
    {
        public override void OnStartClient() => Init();
        public override void OnStopClient() => Deinit();

        protected override UnitClientModule GetModulePrefab(UnitNetwork unit) =>
            unit.UnitNetworkState.UnitConfig?.ClientModule;
    }
}