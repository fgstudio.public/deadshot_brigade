using System;
using System.Threading;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public class UnitReloadingState : IBehaviourState
    {
        public UniTask Task { get; private set; }

        [CanBeNull] private RangeWeaponConfig RangeWeaponConfig => _unit.RangeWeapon;

        private readonly UnitNetwork _unit;
        private CancellationTokenSource _tokenSource;

        public UnitReloadingState(UnitNetwork unit) =>
            _unit = unit;

        public void Enter()
        {
            if (!_unit.Identity.isServer)
                return;

            _tokenSource = new CancellationTokenSource();
            Task = ExecuteReloading(_tokenSource);
        }

        public void Exit() =>
            _tokenSource?.Cancel();

        private async UniTask ExecuteReloading(CancellationTokenSource cancellationTokenSource)
        {
            if (RangeWeaponConfig != null)
            {
                float reloadSeconds = RangeWeaponConfig.ReloadSeconds;
                await UniTask.Delay(TimeSpan.FromSeconds(reloadSeconds));

                if (_unit.UnitNetworkState.BehaviourState != BehaviourState.Reload) return;
                if (cancellationTokenSource.IsCancellationRequested) return;

                _unit.UnitNetworkState.PatronsCount = RangeWeaponConfig.PatronsCount;
            }

            _unit.UnitNetworkState.BehaviourState = BehaviourState.None;
        }
    }
}