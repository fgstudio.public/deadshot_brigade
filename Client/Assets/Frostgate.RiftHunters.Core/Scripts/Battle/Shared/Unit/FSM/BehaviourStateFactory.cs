using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Reanimation;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public sealed class BehaviourStateFactory
    {
        private readonly UnitNetwork _unit;
        private readonly IServiceRoster _serviceRoster;
        private readonly IAimTargetFinder _aimTargetFinder;

        public BehaviourStateFactory(
            [NotNull] UnitNetwork unit, [NotNull] IServiceRoster serviceRoster,
            [NotNull] IAimTargetFinder aimTargetFinder)
        {
            _unit = unit;
            _serviceRoster = serviceRoster;
            _aimTargetFinder = aimTargetFinder;
        }

        // TODO: Создать раздельные фабрики для ботов и юинтов, когда тип объекта будет известен сразу при спауне
        public IBehaviourState Create(BehaviourState state) =>
            state switch
            {
                BehaviourState.None => StubState.Default,
                BehaviourState.MeleeAttack => new UnitMeleeAttackState(_unit, _serviceRoster.DamageService, _aimTargetFinder),
                BehaviourState.Reload => new UnitReloadingState(_unit),
                BehaviourState.Stun => new UnitStunState(_unit),
                BehaviourState.Dead => CreateDeadState(),
                _ => throw new ArgumentException(nameof(_unit.UnitNetworkState.UnitType))
            };

        private IBehaviourState CreateDeadState()
        {
            BattleUnitType unitType = _unit.UnitNetworkState.UnitType;
            IReanimationService reanimationService = _serviceRoster.ReanimationService;

            var deadState = new DeadState(_unit);

            return unitType switch
            {
                BattleUnitType.Bot => new BotDeadState(deadState),
                BattleUnitType.Player => new UnitDeadState(_unit, deadState, reanimationService),
                _ => throw new ArgumentException(nameof(unitType))
            };
        }
    }
}
