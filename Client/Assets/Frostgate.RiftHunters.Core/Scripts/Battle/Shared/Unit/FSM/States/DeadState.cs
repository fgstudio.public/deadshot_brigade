using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public sealed class DeadState : IBehaviourState
    {
        private readonly UnitNetwork _unit;

        public DeadState([NotNull] UnitNetwork unit) =>
            _unit = unit;

        public void Enter()
        {
            _unit.Rigidbody.useGravity = false;
            _unit.Rigidbody.velocity = Vector3.zero;

            if (_unit.View != null)
                _unit.View.EnableDeadStateView(true);

            if (_unit.UnitNetworkState.ViewConfig != null)
                _unit.UnitNetworkState.ViewConfig.gameObject.SetActive(false);

            _unit.UnitCollectLootBox.Disable();
            _unit.UnitCollectEnergyCapsule.Disable();
        }

        public void Exit()
        {
            _unit.Rigidbody.useGravity = true;

            if (_unit.View != null)
                _unit.View?.EnableDeadStateView(false);

            if (_unit.UnitNetworkState.ViewConfig != null)
                _unit.UnitNetworkState.ViewConfig.gameObject.SetActive(true);

            _unit.UnitCollectLootBox.Enable();
            _unit.UnitCollectEnergyCapsule.Enable();
        }
    }
}
