using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public sealed class BehaviourStateMachineHandler : IDisposable
    {
        private readonly UnitNetwork _unit;
        private readonly BehaviourStateMachine _stateMachine;

        private bool _isDead;

        public BehaviourStateMachineHandler([NotNull] UnitNetwork unit,
            [NotNull] BehaviourStateMachine stateMachine)
        {
            _unit = unit;
            _stateMachine = stateMachine;

            Subscribe(_unit);
        }

        public void Dispose()
        {
            Unsubscribe(_unit);
            _stateMachine.Dispose();
        }

        private void Subscribe(UnitNetwork unit) => unit.UnitNetworkState.HealthChanged += OnHealthChanged;
        private void Unsubscribe(UnitNetwork unit) => unit.UnitNetworkState.HealthChanged -= OnHealthChanged;

        private void OnHealthChanged(float diff)
        {
            if (!_isDead && _unit.UnitNetworkState.IsDead)
            {
                _isDead = true;
                _unit.UnitNetworkState.BehaviourState = BehaviourState.Dead;
            }

            if (_isDead && !_unit.UnitNetworkState.IsDead)
            {
                _isDead = false;
                _unit.UnitNetworkState.BehaviourState = BehaviourState.None;
            }
        }
    }
}