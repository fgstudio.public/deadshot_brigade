using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public sealed class BotDeadState : IBehaviourState
    {
        private readonly DeadState _deadState;

        public BotDeadState([NotNull] DeadState deadState) => _deadState = deadState;
        public void Enter() => _deadState.Enter();
        public void Exit() => _deadState.Exit();
    }
}
