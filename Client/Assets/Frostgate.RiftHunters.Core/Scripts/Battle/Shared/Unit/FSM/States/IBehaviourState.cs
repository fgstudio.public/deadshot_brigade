namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public interface IBehaviourState
    {
        void Enter();
        void Exit();
    }
}
