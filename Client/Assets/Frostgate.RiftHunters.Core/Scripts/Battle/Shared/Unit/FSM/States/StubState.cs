namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public sealed class StubState : IBehaviourState
    {
        public static readonly StubState Default = new StubState();

        public void Enter() { }
        public void Exit() { }
    }
}
