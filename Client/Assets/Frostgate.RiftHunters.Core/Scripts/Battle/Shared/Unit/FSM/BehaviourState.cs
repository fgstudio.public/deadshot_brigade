namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public enum BehaviourState
    {
        None = 0,
        MeleeAttack = 1,
        Stun = 2,
        _ = 3,
        Reload = 6,
        Dead = 7,
    }
}
