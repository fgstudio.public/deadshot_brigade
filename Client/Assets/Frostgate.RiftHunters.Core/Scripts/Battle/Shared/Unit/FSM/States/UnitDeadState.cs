using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Reanimation;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public sealed class UnitDeadState : IBehaviourState
    {
        private readonly UnitNetwork _unit;
        private readonly DeadState _deadState;
        private readonly IReanimationService _reanimationService;

        public UnitDeadState([NotNull] UnitNetwork unit,
            [NotNull] DeadState deadState, [NotNull] IReanimationService reanimationService)
        {
            _unit = unit;
            _deadState = deadState;
            _reanimationService = reanimationService;
        }

        public void Enter()
        {
            _unit.ReanimationMechanic.Completed.AddListener(OnCompleted);

            _deadState.Enter();
            _unit.ReanimationMechanic.Enable();
        }

        public void Exit()
        {
            _unit.ReanimationMechanic.Completed.RemoveListener(OnCompleted);

            _unit.ReanimationMechanic.Disable();
            _deadState.Exit();
        }

        private void OnCompleted() => _reanimationService.Reanimate(_unit);
    }
}