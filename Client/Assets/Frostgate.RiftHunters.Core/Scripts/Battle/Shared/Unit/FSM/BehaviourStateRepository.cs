using JetBrains.Annotations;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public sealed class BehaviourStateRepository
    {
        private readonly BehaviourStateFactory _factory;
        private readonly Dictionary<BehaviourState, IBehaviourState> _states;

        public BehaviourStateRepository([NotNull] BehaviourStateFactory factory)
        {
            _factory = factory;
            _states = new Dictionary<BehaviourState, IBehaviourState>();
        }

        public IBehaviourState Get(BehaviourState state)
        {
            if (!_states.TryGetValue(state, out IBehaviourState behaviourState))
                _states[state] = behaviourState = _factory.Create(state);

            return behaviourState;
        }
    }
}
