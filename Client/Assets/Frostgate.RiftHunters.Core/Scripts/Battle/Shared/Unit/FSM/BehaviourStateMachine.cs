using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public sealed class BehaviourStateMachine : IDisposable
    {
        private readonly UnitNetwork _unit;
        private readonly BehaviourStateRepository _repository;

        private IBehaviourState _currentState;

        public BehaviourStateMachine([NotNull] UnitNetwork unit, [NotNull] BehaviourStateRepository repository)
        {
            _unit = unit;
            _repository = repository;

            Subscribe(_unit);

            if (unit.UnitNetworkState.IsInitialized)
                OnInitialized(unit.UnitNetworkState);
        }

        public void Dispose()
        {
            Unsubscribe(_unit);
            _currentState?.Exit();
        }

        private void Subscribe(UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized += OnInitialized;
            unit.UnitNetworkState.BehaviourStateChanged += OnBehaviourStateChanged;
        }

        private void Unsubscribe(UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized -= OnInitialized;
            unit.UnitNetworkState.BehaviourStateChanged -= OnBehaviourStateChanged;
        }

        private void OnInitialized(UnitNetworkState state) =>
            OnBehaviourStateChanged();

        private void OnBehaviourStateChanged()
        {
            if (!_unit.UnitNetworkState.IsInitialized)
                return;

            IBehaviourState newState = _repository.Get(_unit.UnitNetworkState.BehaviourState);

            if (_currentState != newState)
            {
                _currentState?.Exit();
                _currentState = newState;
                _currentState.Enter();
            }
        }
    }
}
