using System;
using System.Threading;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public class UnitMeleeAttackState : IBehaviourState
    {
        public UniTask Task { get; private set; }

        [CanBeNull] private IMeleeWeapon Weapon => _unit.MeleeWeapon;

        private readonly UnitNetwork _unit;
        private readonly IDamageService _damageService;
        private readonly IAimTargetFinder _aimTargetFinder;

        private CancellationTokenSource _tokenSource;

        public UnitMeleeAttackState(UnitNetwork unit, IDamageService damageService, IAimTargetFinder aimTargetFinder)
        {
            _unit = unit;
            _damageService = damageService;
            _aimTargetFinder = aimTargetFinder;
        }

        public void Enter()
        {
            if (!_unit.Identity.isServer)
                return;

            _tokenSource = new CancellationTokenSource();
            Task = ExecuteMeleeAttack(_tokenSource);
        }

        public void Exit() =>
            _tokenSource?.Cancel();

        private async UniTask ExecuteMeleeAttack(CancellationTokenSource cancellationTokenSource)
        {
            if (Weapon == null)
                return;

            await UniTask.Delay(TimeSpan.FromSeconds(Weapon.ShootTime));

            if (_unit.UnitNetworkState.BehaviourState != BehaviourState.MeleeAttack || cancellationTokenSource.IsCancellationRequested)
                return;

            CastMeleeAttack();

            await UniTask.Delay(TimeSpan.FromSeconds(Weapon.Cooldown - Weapon.ShootTime));

            if (_unit.UnitNetworkState.BehaviourState != BehaviourState.MeleeAttack || cancellationTokenSource.IsCancellationRequested)
                return;

            _unit.UnitNetworkState.BehaviourState = BehaviourState.None;
        }

        private void CastMeleeAttack()
        {
            var count = _aimTargetFinder.CastMeleeAttack(_unit, out var results);
            for (var i = 0; i < count; i++)
            {
                var result = results[i];

                if (Weapon != null)
                    _damageService.TakeDamage(Weapon, _unit, result.Target, result.HitPoint);
            }
        }
    }
}