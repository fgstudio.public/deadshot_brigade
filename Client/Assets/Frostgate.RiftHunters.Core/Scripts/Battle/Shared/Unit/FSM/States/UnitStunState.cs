using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM
{
    public class UnitStunState : IBehaviourState
    {
        private readonly UnitNetwork _unit;

        public UnitStunState(UnitNetwork unit) =>
            _unit = unit;

        public void Enter()
        {
            _unit.Rigidbody.useGravity = false;
            _unit.Rigidbody.velocity = Vector3.zero;
        }

        public void Exit()
        {
            _unit.Rigidbody.useGravity = true;
        }
    }
}
