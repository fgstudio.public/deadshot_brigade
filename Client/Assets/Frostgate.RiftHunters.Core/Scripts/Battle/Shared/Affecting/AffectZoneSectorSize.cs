using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Affecting
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/aa61b0f5d1a045a7aaa8dc875bb468c3")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(AffectZoneSectorSize))]
    public sealed class AffectZoneSectorSize : MonoBehaviour
    {
        private const float UnitRadiusComp = 0.4f;

        private const float ReferenceSize = 1f;
        private const float AspectMult = 1.63f;

        [Title("Component settings")]
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _model;
        [SerializeField, Range(0, 180)] private float _referenceAngle = 70f;

        [Title("Zone settings")]
        [MinValue(0), MaxValue(20), SuffixLabel("units", true)]
        [ShowInInspector] private float Radius
        {
            get => GetRadius();
            set => SetRadius(value);
        }

        [MinValue(0), MaxValue(160), SuffixLabel("deg", true)]
        [ShowInInspector] private float AngleWidth
        {
            get => GetAngleWidth();
            set => SetAngleWidth(value);
        }

        private void OnValidate()
        {
            InitReferences();
            UpdateSize();
        }

        private void Awake() => UpdateSize();

        private void InitReferences() =>
            _model ??= GetComponentInChildren<Transform>();

        [Button] private void UpdateSize()
        {
            SetRadius(GetRadius());
            SetAngleWidth(GetAngleWidth());
        }

        private float GetRadius() =>
            DecompRadius(_model.localScale.y);

        private void SetRadius(float radius) =>
            _model.localScale = _model.localScale.SetY(CompRadius(radius));

        private float GetAngleWidth()
        {
            float radius = GetRadius();
            float referenceAngle = CalcActualReferenceAngle(radius);

            float size = _model.localScale.x;
            float mult = size / ReferenceSize;

            return mult * referenceAngle;
        }

        private void SetAngleWidth(float angleWidth)
        {
            float radius = GetRadius();
            float referenceAngle = CalcActualReferenceAngle(radius);

            float mult = angleWidth / referenceAngle;
            float size = mult * ReferenceSize;

            _model.localScale = _model.localScale.SetX(size);
        }

        private float CalcActualReferenceAngle(float radius) =>
            _referenceAngle / Mathf.Pow(AspectMult, radius - 1);

        private float CompRadius(float realRadius) =>
            realRadius > 0 ? realRadius + UnitRadiusComp : 0;

        private float DecompRadius(float compRadius) =>
            Mathf.Max(0, compRadius - UnitRadiusComp);
    }
}