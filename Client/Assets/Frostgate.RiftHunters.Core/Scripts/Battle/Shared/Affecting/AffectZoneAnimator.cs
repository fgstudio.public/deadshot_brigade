using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Affecting
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/aa61b0f5d1a045a7aaa8dc875bb468c3")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(AffectZoneAnimator))]
    public sealed class AffectZoneAnimator : MonoBehaviour
    {
        [Header("References")] [SerializeField, Required, ChildGameObjectsOnly]
        private Animator _animator;

        [Header("Settings")] [Tooltip("0 — disabled"), SuffixLabel("sec")] [SerializeField, Range(0, 1)]
        private float _onAwakeUpdateDt;

        [SerializeField, Required] private string _activeParameterName;

        private void OnValidate() => _animator ??= GetComponentInChildren<Animator>();
        private void Awake()
        {
            if (_onAwakeUpdateDt > 0)
                _animator.Update(_onAwakeUpdateDt);
        }

        public void PlayActivating() => _animator.SetBool(_activeParameterName, true);
        public void PlayDeactivating() => _animator.SetBool(_activeParameterName, false);
    }
}