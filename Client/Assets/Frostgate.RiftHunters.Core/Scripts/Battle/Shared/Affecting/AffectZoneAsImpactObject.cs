using System;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Client.Unit.Impact;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Affecting
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(AffectZone))]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" +
                      nameof(AffectZoneAsImpactObject))]
    public sealed class AffectZoneAsImpactObject : MonoBehaviour, IImpactViewObject
    {
        [SerializeField, Required, ChildGameObjectsOnly] private AffectZone _affectZone;

        public void Execute(EventData.EventType eventType,
            Vector3 sourcePosition, Vector3 targetPosition, TimeSpan duration)
        {
            foreach (IDurable durable in GetComponentsInChildren<IDurable>())
                durable.Duration = (float)duration.TotalSeconds;

            _affectZone.ActivateFor(duration);
        }

        private void OnValidate() => InitReferences();
        private void Awake() => InitReferences();

        private void InitReferences() =>
            _affectZone ??= GetComponent<AffectZone>();
    }
}