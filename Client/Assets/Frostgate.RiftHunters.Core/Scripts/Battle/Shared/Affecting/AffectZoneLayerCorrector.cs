using System;
using System.Collections;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Affecting
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(AffectZoneLayerCorrector))]
    public sealed class AffectZoneLayerCorrector : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly]
        private SpriteMask _mask;

        [SerializeField, Required, ChildGameObjectsOnly]
        private SpriteRenderer _renderer;

        private static readonly IEnumerator<int> indexIterator = new IndexIterator(2, 253,
            current => current + 2);

        private void OnEnable()
        {
            if (!indexIterator.MoveNext())
            {
                indexIterator.Reset();
                indexIterator.MoveNext();
            }

            int order = indexIterator.Current;
            _mask.isCustomRangeActive = true;

            _renderer.sortingOrder = order;
            _mask.frontSortingOrder = order;
            _mask.backSortingOrder = order - 1;
        }

        private void OnDisable()
        {
            _mask.isCustomRangeActive = false;

            _renderer.sortingOrder = default;
            _mask.frontSortingOrder = default;
            _mask.backSortingOrder = default;
        }

        private sealed class IndexIterator : IEnumerator<int>
        {
            public int Current => _currentValue;

            private readonly int _startValue;
            private readonly int _endValue;
            [NotNull] private readonly Func<int, int> _changeValue;

            object IEnumerator.Current => Current;

            private int _currentValue = -1;
            private bool _isMoved;

            public IndexIterator(int startValue, int endValue, [NotNull] Func<int, int> changeValue)
            {
                _startValue = startValue;
                _endValue = endValue;
                _changeValue = changeValue;

                ResetCurrentValue();
            }

            public bool MoveNext()
            {
                if (_currentValue > _endValue)
                    return false;

                _currentValue = _isMoved ? _changeValue.Invoke(_currentValue) : _startValue;
                _isMoved = true;

                return true;
            }

            public void Reset() => ResetCurrentValue();


            public void Dispose()
            {
            }

            private void ResetCurrentValue()
            {
                _currentValue = -1;
                _isMoved = false;
            }
        }
    }
}