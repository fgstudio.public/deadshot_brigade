using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Affecting
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/aa61b0f5d1a045a7aaa8dc875bb468c3")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(AffectZoneRectSize))]
    public sealed class AffectZoneRectSize : MonoBehaviour
    {
        private const float UnitRadiusComp = 0.4f;

        [SerializeField, Required, ChildGameObjectsOnly] private Transform _model;

        [MinValue(0), MaxValue(20), SuffixLabel("units", true)]
        [ShowInInspector] private Vector2 Size
        {
            get => Get();
            set => Set(value);
        }

        private void OnValidate()
        {
            InitReferences();
            UpdateSize();
        }

        private void Awake() => UpdateSize();

        private void InitReferences() => _model ??= GetComponentInChildren<Transform>();
        [Button] private void UpdateSize() => Set(Get());

        private Vector2 Get() =>
            DecompSize(_model.localScale.CutZ());

        private void Set(Vector2 size) =>
            _model.localScale = CompSize(new Vector3(size.x, size.y, 1));

        private Vector2 CompSize(Vector2 realSize)
        {
            if (realSize.x > 0) realSize.x += UnitRadiusComp;
            if (realSize.y > 0) realSize.y += UnitRadiusComp;
            return realSize;
        }

        private Vector2 DecompSize(Vector2 compSize)
        {
            compSize.x = Mathf.Max(0, compSize.x - UnitRadiusComp);
            compSize.y = Mathf.Max(0, compSize.y - UnitRadiusComp);
            return compSize;
        }
    }
}