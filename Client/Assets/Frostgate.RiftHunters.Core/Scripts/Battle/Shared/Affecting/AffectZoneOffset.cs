using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Affecting
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/aa61b0f5d1a045a7aaa8dc875bb468c3")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(AffectZoneOffset))]
    public sealed class AffectZoneOffset : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _model;

        [MinValue(0), MaxValue(20), SuffixLabel("units", true)]
        [ShowInInspector] private Vector3 Offset
        {
            get => Get();
            set => Set(value);
        }

        private void OnValidate()
        {
            InitReferences();
            UpdateOffset();
        }

        private void Awake() => UpdateOffset();

        private void InitReferences() => _model ??= GetComponentInChildren<Transform>();
        [Button] private void UpdateOffset() => Set(Get());

        private Vector3 Get() => _model.localPosition;
        private void Set(Vector3 offset) => _model.localPosition = offset;
    }
}