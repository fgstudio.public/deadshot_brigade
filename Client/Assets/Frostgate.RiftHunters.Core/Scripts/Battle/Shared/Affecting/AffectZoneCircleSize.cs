using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Affecting
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/aa61b0f5d1a045a7aaa8dc875bb468c3")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(AffectZoneCircleSize))]
    public sealed class AffectZoneCircleSize : MonoBehaviour
    {
        private const float UnitRadiusComp = 0.4f;

        [SerializeField, Required, ChildGameObjectsOnly] private Transform _model;

        [MinValue(0), MaxValue(20), SuffixLabel("units", true)]
        [ShowInInspector] private float Radius
        {
            get => Get();
            set => Set(value);
        }

        private void OnValidate()
        {
            InitReferences();
            UpdateRadius();
        }

        private void Awake() => UpdateRadius();

        private void InitReferences() => _model ??= GetComponentInChildren<Transform>();
        [Button] private void UpdateRadius() => Set(Get());

        private float Get() => DecompRadius(_model.localScale.x);
        private void Set(float radius) => _model.localScale = Vector3.one * CompRadius(radius);

        private float CompRadius(float realRadius) =>
            realRadius > 0 ? realRadius + UnitRadiusComp : 0;

        private float DecompRadius(float compRadius) =>
            Mathf.Max(0, compRadius - UnitRadiusComp);
    }
}