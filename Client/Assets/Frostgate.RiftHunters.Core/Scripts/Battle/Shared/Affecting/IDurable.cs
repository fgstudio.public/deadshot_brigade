namespace Frostgate.RiftHunters.Core.Battle.Shared.Affecting
{
    public interface IDurable
    {
        float Duration { get; set; }
    }
}
