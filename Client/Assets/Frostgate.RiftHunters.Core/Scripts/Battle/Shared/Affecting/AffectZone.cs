using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Affecting
{
    public interface IReadOnlyAffectZone
    {
        UnityEvent Activated { get; }
        UnityEvent Completed { get; }
        UnityEvent Canceled { get; }

        bool IsActive { get; }
    }

    public interface IAffectZone : IReadOnlyAffectZone
    {
        void Activate();
        UniTask ActivateFor(TimeSpan timeSpan);
        void Complete();
        void Cancel();
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/aa61b0f5d1a045a7aaa8dc875bb468c3")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(AffectZone))]
    public sealed class AffectZone : MonoBehaviour, IAffectZone
    {
        private const string EventsFoldoutName = "Events";

        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private AffectZoneAnimator _animator;

        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Activated { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Completed { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Canceled { get; private set; }

        [CanBeNull] private CancellationTokenSource _cts;

        [Title("State")]
        [ShowInInspector, ReadOnly] public bool IsActive { get; private set; }

        private void OnValidate() =>
            _animator ??= GetComponentInChildren<AffectZoneAnimator>();

        private void OnDestroy() =>
            DisposeTokenSource(ref _cts);

        [Title("Buttons")]
        [Button]
        public void Activate()
        {
            IsActive = true;
            _animator.PlayActivating();
            DisposeTokenSource(ref _cts);

            Activated?.Invoke();
        }

        [Button]
        public void Complete()
        {
            Deactivate();
            Completed?.Invoke();
        }

        [Button]
        public void Cancel()
        {
            Deactivate();
            Canceled?.Invoke();
        }

        public UniTask ActivateFor(TimeSpan timeSpan)
        {
            Activate();

            DisposeTokenSource(ref _cts);
            _cts = new CancellationTokenSource();

            return UniTask.Delay(timeSpan, cancellationToken: _cts.Token)
                .ContinueWith(Complete);
        }

        private void Deactivate()
        {
            IsActive = false;
            _animator.PlayDeactivating();
            DisposeTokenSource(ref _cts);
        }

        private void DisposeTokenSource(ref CancellationTokenSource cts)
        {
            if (cts != null)
            {
                cts.Cancel();
                cts.Dispose();
                cts = null;
            }
        }
    }
}