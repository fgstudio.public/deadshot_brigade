﻿using DG.Tweening;
using UnityEngine;
using JetBrains.Annotations;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Affecting
{
    [DisallowMultipleComponent]
    [HelpURL("https://app.clickup.com/t/2t7ty5y")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(AffectZoneMaskTween))]
    public sealed class AffectZoneMaskTween : MonoBehaviour, IDurable
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _mask;

        [Header("Settings")]
        [SerializeField] private Ease _ease = Ease.Linear;
        [SerializeField, MinValue(0)] private float _duration;

        [CanBeNull] private Tweener _tweener;
        private Vector3? _scaleCache;

        float IDurable.Duration
        {
            get => _duration;
            set => _duration = value;
        }

        private void Awake() =>
            CacheScaleIfUncached();

        public void Play()
        {
            CacheScaleIfUncached();

            Stop();

            _tweener = _mask.transform.DOScale(Vector3.zero, _duration).SetEase(_ease);
            _tweener.SetTarget(_mask);
            _tweener.Play();
        }

        public void Stop()
        {
            _tweener?.Kill();
            ResetScale();
        }

        private void CacheScaleIfUncached() => _scaleCache ??= _mask.localScale;
        private void ResetScale() => _mask.localScale = _scaleCache!.Value;
    }
}