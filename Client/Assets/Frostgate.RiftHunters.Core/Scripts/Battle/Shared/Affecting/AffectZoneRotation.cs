using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Affecting
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/aa61b0f5d1a045a7aaa8dc875bb468c3")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(AffectZoneRotation))]
    public sealed class AffectZoneRotation : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _model;

        [PropertyRange(-360, 360), SuffixLabel("deg", true)]
        [ShowInInspector] private float Rotation
        {
            get => Get();
            set => Set(value);
        }

        private void OnValidate()
        {
            InitReferences();
            UpdateRotation();
        }

        private void Awake() => UpdateRotation();

        private void InitReferences() => _model ??= GetComponentInChildren<Transform>();
        [Button] private void UpdateRotation() => Set(Get());

        private float Get() => _model.localEulerAngles.y;
        private void Set(float rotation) => _model.localEulerAngles = _model.localEulerAngles.SetY(rotation);
    }
}