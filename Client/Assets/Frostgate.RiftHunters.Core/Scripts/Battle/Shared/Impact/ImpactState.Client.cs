using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    // здесь происходит обработка полученных Rpc, которые инициируют события на стороны клиента
    public partial class ImpactState
    {
        [ClientRpc]
        private void RpcInvokeActivatedInPositionEvent(string impactId, string effectId, Vector3 position)
        {
            if (isClientOnly)
                InvokeActivatedInPositionEvent(impactId, effectId, position);
        }

        [ClientRpc]
        private void RpcInvokeAffectingInPositionEvent(string impactId, string effectId, Vector3 position)
        {
            if (isClientOnly)
                InvokeAffectingInPositionEvent(impactId, effectId, position);
        }

        [ClientRpc]
        private void RpcInvokeAffectingTargetEvent(string impactId, string effectId, Vector3 position)
        {
            if (isClientOnly)
                InvokeAffectingTargetEvent(impactId, effectId, position);
        }

        [ClientRpc]
        private void RpcInvokeAffectedTargetEvent(string impactId, string effectId, Vector3 position)
        {
            if (isClientOnly)
                InvokeAffectedTargetEvent(impactId, effectId, position);
        }
    }
}
