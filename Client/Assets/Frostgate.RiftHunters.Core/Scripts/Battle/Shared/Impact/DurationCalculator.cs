using System;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    public sealed class DurationCalculator
    {
        public TimeSpan CalcEffectDelay([NotNull] EffectActivationData activationData, Vector3 sourcePosition, Vector3 targetPosition)
        {
            float seconds = CalcDelaySeconds(activationData, sourcePosition, targetPosition);
            return TimeSpan.FromSeconds(seconds);
        }

        public TimeSpan CalcEffectDuration([NotNull] EffectAffectDurationData durationData) =>
            durationData.Type switch
            {
                EffectAffectDurationData.DurationType.Instantly => TimeSpan.Zero,
                EffectAffectDurationData.DurationType.ContinuousFor => durationData.Duration,
                EffectAffectDurationData.DurationType.RepeatableFor => durationData.RepeatCooldown * durationData.RepeatCount,
                _ => throw new ArgumentOutOfRangeException(nameof(EffectAffectDurationData.DurationType))
            };

        private float CalcDelaySeconds([NotNull] EffectActivationData activationData, Vector3 sourcePosition, Vector3 targetPosition) =>
            activationData.Type switch
            {
                EffectActivationData.ActivationType.Instantly => 0,
                EffectActivationData.ActivationType.Delayed => activationData.Delay,
                EffectActivationData.ActivationType.DelayedByDistance =>
                        activationData.DelayByDistance.Evaluate(
                            Vector3.Distance(sourcePosition, targetPosition)),
                _ => throw new ArgumentOutOfRangeException()
            };
    }
}
