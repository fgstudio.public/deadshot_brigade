namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    public interface IImpactSource : INetworkIdentifiable, IGameObject
    {
    }
}
