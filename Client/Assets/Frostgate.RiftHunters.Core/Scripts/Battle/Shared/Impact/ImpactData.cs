using System;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    public enum ImpactStatus : byte
    {
        None,
        Started,
        Finished,
        Interrupted
    }

    [Serializable]
    public readonly struct ImpactData
    {
        public readonly string Id;
        public readonly ImpactStatus Status;
        public readonly float StartTimestamp;

        public ImpactData(string id,
            ImpactStatus status = ImpactStatus.None, float startTimestamp = default)
        {
            Id = id;
            Status = status;
            StartTimestamp = startTimestamp;
        }

        public ImpactData WithStatus(ImpactStatus status) =>
            new(Id, status, StartTimestamp);
    }
}
