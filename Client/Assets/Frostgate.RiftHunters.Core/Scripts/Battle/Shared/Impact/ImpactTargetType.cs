namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    public enum ImpactTargetType
    {
        None = 0,
        Unit = 1,
        Barrel = 2,
        Trap = 3,
        Totem = 4
    }
}
