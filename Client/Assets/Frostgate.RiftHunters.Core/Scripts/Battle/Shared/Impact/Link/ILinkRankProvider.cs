﻿namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    public interface ILinkRankProvider
    {
        int LinkRank { get; }
    }
}