using System;
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    // TODO: сделать частью UnitServerModule
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5")]
    [AddComponentMenu(BattleComponentMenus.Client.Menu + "/" + nameof(LinkSpawnerView))]
    public sealed partial class LinkSpawnerView : NetworkBehaviour, ILinkRankProvider
    {
        [Required, AssetsOnly, AssetSelector] public LinkView LinkPrefab;
        [Required, ChildGameObjectsOnly] public Transform LinkContainer;
        [Required, ChildGameObjectsOnly] public ParticleSystemEmitter StartEmitter;

        [field: SerializeField] public UnityEvent<float, TimeSpan> Started { get; private set; }
        [field: SerializeField] public UnityEvent Finished { get; private set; }

        [field: SerializeField] public UnityEvent<NetworkIdentity, NetworkIdentity> LinkSet { get; private set; }
        [field: SerializeField] public UnityEvent<NetworkIdentity, NetworkIdentity> LinkRemoved { get; private set; }

        // ReSharper disable once ConvertToAutoProperty
        public int LinkRank => _linkRank;
        public bool IsActive { get; private set; }

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            StartEmitter ??= GetComponentInChildren<ParticleSystemEmitter>();

        private void OnStartSpawner(float radius, TimeSpan duration)
        {
            IsActive = true;
            Started?.Invoke(radius, duration);
        }

        private void OnFinishSpawner()
        {
            IsActive = false;
            Finished?.Invoke();
        }

        private void OnSetLink(NetworkIdentity start, NetworkIdentity end) =>
            LinkSet?.Invoke(start, end);

        private void OnRemoveLink(NetworkIdentity start, NetworkIdentity end) =>
            LinkRemoved?.Invoke(start, end);
    }
}