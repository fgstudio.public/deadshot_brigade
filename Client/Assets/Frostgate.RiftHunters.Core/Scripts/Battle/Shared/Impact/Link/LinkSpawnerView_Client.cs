using System;
using System.Threading;
using System.Collections.Generic;
using Mirror;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    public sealed partial class LinkSpawnerView
    {
        private readonly struct LinkTargets
        {
            [NotNull] public readonly NetworkIdentity Start;
            [NotNull] public readonly NetworkIdentity End;

            public LinkTargets([NotNull] NetworkIdentity start, [NotNull] NetworkIdentity end)
            {
                Start = start;
                End = end;
            }
        }

        private readonly Dictionary<LinkTargets, LinkView> _links = new();
        [CanBeNull] private CancellationTokenSource _startCts;

        private void OnDestroy()
        {
            if (isClient)
            {
                DeactivateEmitter();
                RemoveAllLinks();
            }
        }

        [ClientRpc] private void RpcStartSpawner(float radius, float durationSeconds)
        {
            if (isClient)
            {
                TimeSpan duration = TimeSpan.FromSeconds(durationSeconds);
                ActivateEmitterFor(duration);
                OnStartSpawner(radius, duration);
            }
        }

        [ClientRpc]
        private void RpcFinishSpawner()
        {
            if (isClient)
            {
                DeactivateEmitter();
                RemoveAllLinks();
                OnFinishSpawner();
            }
        }

        [ClientRpc]
        private void RpcSetLink(NetworkIdentity start, NetworkIdentity end)
        {
            if (isClient)
            {
                SetLink(start, end);
                OnSetLink(start, end);
            }
        }

        [ClientRpc]
        private void RpcRemoveLink(NetworkIdentity start, NetworkIdentity end)
        {
            if (isClient)
            {
                RemoveLink(start, end);
                OnRemoveLink(start, end);
            }
        }

        [Client] private void ActivateEmitterFor(TimeSpan duration)
        {
            StartEmitter.Play();

            _startCts = new CancellationTokenSource();

            UniTask.Delay(duration, cancellationToken: _startCts.Token)
                .ContinueWith(OnFinishSpawner).AsAsyncUnitUniTask();
        }

        [Client] private void DeactivateEmitter()
        {
            StartEmitter.Stop();

            if (_startCts != null)
            {
                _startCts.Cancel();
                _startCts.Dispose();
                _startCts = null;
            }
        }

        [Client] private void SetLink([NotNull] NetworkIdentity start, [NotNull] NetworkIdentity end)
        {
            var linkTargets = new LinkTargets(start, end);

            if (!_links.ContainsKey(linkTargets))
            {
                LinkView link = Instantiate(LinkPrefab, LinkContainer);
                link.SetTargets(start.transform, end.transform, this);
                _links.Add(linkTargets, link);
            }
        }

        [Client] private void RemoveLink([NotNull] NetworkIdentity start, [NotNull] NetworkIdentity end)
        {
            var linkTargets = new LinkTargets(start, end);

            if (_links.TryGetValue(linkTargets, out LinkView linkView))
            {
                Destroy(linkView.gameObject);
                _links.Remove(linkTargets);
            }
        }

        [Client] private void RemoveAllLinks()
        {
            foreach (LinkView linkView in _links.Values)
                Destroy(linkView.gameObject);

            _links.Clear();
        }
    }
}