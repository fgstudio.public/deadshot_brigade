using TMPro;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/fa2f1c562cb9488fbbafe2c04b2ea6c4")]
    [AddComponentMenu(BattleComponentMenus.Client.Menu + "/" + nameof(LinkView))]
    public sealed class LinkView : MonoBehaviour
    {
        private const int PositionsCount = 2;

        [Header("References")]
        [Required, ChildGameObjectsOnly] public LineRenderer Renderer;
        [Required, ChildGameObjectsOnly] public ParticleSystemEmitter StartEmitter;
        [Required, ChildGameObjectsOnly] public ParticleSystemEmitter EndEmitter;
        [Required, ChildGameObjectsOnly] public Transform ModifierTextTransform;
        [Required, ChildGameObjectsOnly] public TMP_Text ModifierText;

        [Header("Settings")]
        [MinValue(0), SuffixLabel("units", true)] public float StartTrim;
        [MinValue(0), SuffixLabel("units", true)] public float EndTrim;

        [CanBeNull] private ILinkRankProvider _rankProvider;
        [CanBeNull] private Transform _start;
        [CanBeNull] private Transform _end;
        private bool _hasTargets;

        private readonly Vector3[] _positions = new Vector3[PositionsCount];

        private void Awake() => InitReferences();
        private void OnValidate()
        {
            InitReferences();
            Renderer.positionCount = PositionsCount;
        }

        private void InitReferences()
        {
            Renderer ??= GetComponentInChildren<LineRenderer>();
            StartEmitter ??= GetComponentInChildren<ParticleSystemEmitter>();
            EndEmitter ??= GetComponentInChildren<ParticleSystemEmitter>();
        }

        private void Update()
        {
            if (_hasTargets)
                _hasTargets = CalcHasTargets();

            Vector3 offset = transform.parent.localPosition;
            Vector3 startPoint = _hasTargets ? _start!.position + offset : Vector3.zero;
            Vector3 endPoint = _hasTargets ? _end!.position + offset : Vector3.zero;

            bool hasTrimming = StartTrim > 0 || EndTrim > 0;
            if (hasTrimming)
            {
                Vector3 distanceVector = endPoint - startPoint;
                float distance = distanceVector.magnitude;
                Vector3 direction = distanceVector.normalized;

                startPoint += Mathf.Clamp(StartTrim, 0, distance - EndTrim) * direction;
                endPoint -= Mathf.Clamp(EndTrim, 0, distance - StartTrim) * direction;
            }

            _positions[0] = startPoint;
            _positions[1] = endPoint;

            Renderer.positionCount = PositionsCount;
            Renderer.SetPositions(_positions);

            StartEmitter.transform.position = startPoint;
            EndEmitter.transform.position = endPoint;

            var middlePosition = (startPoint + endPoint) / 2f;
            ModifierTextTransform.position = middlePosition;

            if (_hasTargets && _rankProvider != null)
                ModifierText.text = _rankProvider.LinkRank.ToString();
        }

        public void RemoveTargets() =>
            SetTargets(null, null, null);

        public void SetTargets([CanBeNull] Transform start, [CanBeNull] Transform end, [CanBeNull] ILinkRankProvider rankProvider)
        {
            _rankProvider = rankProvider;
            _start = start;
            _end = end;

            if (_start != null) StartEmitter.Play();
            else StartEmitter.Stop();

            if (_end != null) EndEmitter.Play();
            else EndEmitter.Stop();

            _hasTargets = CalcHasTargets();
        }

        private bool CalcHasTargets() =>
            _start != null && _end != null;
    }
}