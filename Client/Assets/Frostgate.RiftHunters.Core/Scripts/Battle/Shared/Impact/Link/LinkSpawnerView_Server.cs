using System;
using Mirror;
using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    public interface ILinkSpawnerViewInvoker
    {
        [Server] void StartSpawner(float radius, TimeSpan duration, [NotNull] ILinkRankProvider rankProvider);
        [Server] void FinishSpawner();
        [Server] void SetLink(NetworkIdentity start, NetworkIdentity end);
        [Server] void RemoveLink(NetworkIdentity start, NetworkIdentity end);
    }

    public sealed partial class LinkSpawnerView : ILinkSpawnerViewInvoker
    {
        [SyncVar, SerializeField] private int _linkRank;

        private ILinkRankProvider _rankProvider;

        private void Update()
        {
            if (isServer && _rankProvider != null)
                _linkRank = _rankProvider.LinkRank;
        }

        [Server] void ILinkSpawnerViewInvoker.StartSpawner(float radius, TimeSpan duration, ILinkRankProvider rankProvider)
        {
            _rankProvider = rankProvider;
            _linkRank = _rankProvider.LinkRank;

            RpcStartSpawner(radius, (float)duration.TotalSeconds);
            OnStartSpawner(radius, duration);
        }

        [Server] void ILinkSpawnerViewInvoker.FinishSpawner()
        {
            _rankProvider = null;
            _linkRank = default;

            RpcFinishSpawner();
            OnFinishSpawner();
        }

        [Server] void ILinkSpawnerViewInvoker.SetLink(NetworkIdentity start, NetworkIdentity end)
        {
            RpcSetLink(start, end);
            OnSetLink(start, end);
        }

        [Server] void ILinkSpawnerViewInvoker.RemoveLink(NetworkIdentity start, NetworkIdentity end)
        {
            RpcRemoveLink(start, end);
            OnRemoveLink(start, end);
        }
    }
}