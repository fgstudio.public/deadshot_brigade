using System;
using System.Threading;
using Mirror;
using DG.Tweening;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    public interface IWaveSpawnerViewInvoker
    {
        [Server] void StartSpawner(float radius, TimeSpan duration);
        [Server] void FinishSpawner();
        [Server] void StartWave(int index, float radius, TimeSpan duration);
        [Server] void FinishWave(int index);
    }

    // TODO: сделать частью UnitServerModule
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(WaveSpawnerView))]
    public sealed class WaveSpawnerView : NetworkBehaviour, IWaveSpawnerViewInvoker
    {
        [Required, ChildGameObjectsOnly] public ParticleSystemEmitter Backlight;
        [Required, ChildGameObjectsOnly] public ParticleSystemEmitter Wave;

        [field: SerializeField] public UnityEvent<float, TimeSpan> Started { get; private set; }
        [field: SerializeField] public UnityEvent Finished { get; private set; }

        [field: SerializeField] public UnityEvent<int, float, TimeSpan> WaveStarted { get; private set; }
        [field: SerializeField] public UnityEvent<int> WaveFinished { get; private set; }

        private int _currentWaveIndex;
        [CanBeNull] private Tweener _twWave;
        [CanBeNull] private CancellationTokenSource _ctsBacklight;

        private void OnDestroy()
        {
            DeactivateBacklight();
            DeactivateWave();
        }

        [Server] void IWaveSpawnerViewInvoker.StartSpawner(float radius, TimeSpan duration)
        {
            OnStartSpawner(radius, duration);
            RpcStartSpawner(radius, (float)duration.TotalSeconds);
        }

        [Server] void IWaveSpawnerViewInvoker.FinishSpawner()
        {
            OnFinishSpawner();
            RpcFinishSpawner();
        }

        [Server] void IWaveSpawnerViewInvoker.StartWave(int index, float radius, TimeSpan duration)
        {
            OnStartWave(index, radius, duration);
            RpcStartWave(index, radius, (float)duration.TotalSeconds);
        }

        [Server] void IWaveSpawnerViewInvoker.FinishWave(int index)
        {
            OnFinishWave(index);
            RpcFinishWave(index);
        }

        [ClientRpc] private void RpcStartSpawner(float radius, float durationSeconds)
        {
            if (isClientOnly)
                OnStartSpawner(radius, TimeSpan.FromSeconds(durationSeconds));
        }

        [ClientRpc] private void RpcFinishSpawner()
        {
            if (isClientOnly)
                OnFinishSpawner();
        }

        [ClientRpc] private void RpcStartWave(int index, float radius, float durationSeconds)
        {
            if (isClientOnly)
                OnStartWave(index, radius, TimeSpan.FromSeconds(durationSeconds));
        }

        [ClientRpc] private void RpcFinishWave(int index)
        {
            if (isClientOnly)
                OnFinishWave(index);
        }

        private void OnStartSpawner(float radius, TimeSpan duration)
        {
            DeactivateBacklight();
            ActivateBacklightFor(duration, radius);
            Started?.Invoke(radius, duration);
        }

        private void OnFinishSpawner()
        {
            DeactivateBacklight();
            Finished?.Invoke();
        }

        private void OnStartWave(int index, float radius, TimeSpan duration)
        {
            DeactivateWave();
            ActivateWaveFor(duration, radius);
            _currentWaveIndex = index;

            WaveStarted?.Invoke(index, radius, duration);
        }

        private void OnFinishWave(int index)
        {
            if (_currentWaveIndex != index) return;

            DeactivateWave();
            WaveFinished?.Invoke(index);
        }

        private void ActivateBacklightFor(TimeSpan duration, float radius)
        {
            Backlight.Play();
            Backlight.transform.localScale = 0.6f * radius * Vector3.one;

            _ctsBacklight = new CancellationTokenSource();

            UniTask.Delay(duration, cancellationToken: _ctsBacklight.Token)
                .ContinueWith(DeactivateBacklight)
                .AsAsyncUnitUniTask();
        }

        private void DeactivateBacklight()
        {
            Backlight.Stop();

            if (_ctsBacklight != null)
            {
                _ctsBacklight.Cancel();
                _ctsBacklight.Dispose();
                _ctsBacklight = null;
            }
        }

        private void ActivateWaveFor(TimeSpan duration, float radius)
        {
            Wave.Play();

            Wave.transform.localScale = Vector3.zero;
            _twWave = Wave.transform
                .DOScale(radius * Vector3.one, (float)duration.TotalSeconds)
                .OnComplete(DeactivateWave);
        }

        private void DeactivateWave()
        {
            Wave.Stop();

            if (_twWave != null)
            {
                _twWave.Kill();
                _twWave = null;
            }
        }
    }
}