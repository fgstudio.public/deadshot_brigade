using System;
using System.Linq;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Battle.Server.Impact;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    // здесь происходит запуск обработки воздействий и контроль данных модели
    public partial class ImpactState
    {
        // TODO: логика должна быть в ImpactModel
        [CanBeNull] private ImpactExecutorFactory _executorFactory;
        private readonly Dictionary<ImpactConfig, ImpactExecutor> _executors = new();

        [Server]
        public void Init([NotNull] ImpactExecutorFactory executorFactory)
        {
            ThrowIfClient(nameof(Init));
            _executorFactory = executorFactory;
        }

        [Server]
        private void OnDestroyServer()
        {
            ThrowIfClient(nameof(OnDestroyServer));

            foreach (ImpactExecutor executor in _executors.Values)
            {
                Unsubscribe(executor);
                executor.Dispose();
            }

            _executors.Clear();
        }

        [Server]
        public void Reset()
        {
            ThrowIfClient(nameof(Reset));

            // чтобы вызвать события об удалении конкретных ключей
            string[] impactIds = _impactData.Keys.ToArray();
            foreach (string impactId in impactIds) _impactData.Remove(impactId);
        }

        [Server]
        public UniTask ExecuteImpactAsync([NotNull] ImpactConfig config)
        {
            ThrowIfClient(nameof(ExecuteImpactAsync));

            if (!_executors.TryGetValue(config, out ImpactExecutor executor))
                executor = _executors[config] = CreateExecutor(config);

            return executor.ExecuteAsync();
        }

        [Server]
        public void InterruptImpact([NotNull] ImpactConfig config)
        {
            ThrowIfClient(nameof(InterruptImpact));

            if (_executors.TryGetValue(config, out ImpactExecutor executor))
                executor.Interrupt();
        }

        [Server]
        private ImpactExecutor CreateExecutor([NotNull] ImpactConfig impactConfig)
        {
            ThrowIfClient(nameof(CreateExecutor));

            ImpactExecutor impactExecutor = _executorFactory!.Create(impactConfig);
            Subscribe(impactExecutor);

            return impactExecutor;
        }

        [Server]
        private void Subscribe([NotNull] ImpactExecutor impactExecutor)
        {
            ThrowIfClient(nameof(Subscribe));

            impactExecutor.Interrupted += OnInterrupted;
            impactExecutor.CastStarted += OnCastStarted;
            impactExecutor.CastFinished += OnCastFinished;
            impactExecutor.EffectActivatedInPosition += OnEffectActivatedInPosition;
            impactExecutor.EffectAffectingInPosition += OnEffectAffectingInPosition;
            impactExecutor.EffectAffectingTarget += OnEffectAffectingTarget;
            impactExecutor.EffectAffectedTarget += OnEffectAffectedTarget;
        }

        [Server]
        private void Unsubscribe([NotNull] ImpactExecutor impactExecutor)
        {
            ThrowIfClient(nameof(Unsubscribe));

            impactExecutor.Interrupted -= OnInterrupted;
            impactExecutor.CastStarted -= OnCastStarted;
            impactExecutor.CastFinished -= OnCastFinished;
            impactExecutor.EffectActivatedInPosition -= OnEffectActivatedInPosition;
            impactExecutor.EffectAffectingInPosition -= OnEffectAffectingInPosition;
            impactExecutor.EffectAffectingTarget -= OnEffectAffectingTarget;
            impactExecutor.EffectAffectedTarget -= OnEffectAffectedTarget;
        }

        [Server]
        private void OnCastStarted(string impactId)
        {
            ThrowIfClient(nameof(OnCastStarted));

            _impactData[impactId] = new ImpactData(
                impactId, ImpactStatus.Started, _simulationTime!.Time);
        }

        [Server]
        private void OnCastFinished(string impactId)
        {
            ThrowIfClient(nameof(OnCastFinished));

            if (!_impactData.TryGetValue(impactId, out ImpactData data))
                data = new ImpactData(impactId);

            _impactData[impactId] = data.WithStatus(ImpactStatus.Finished);
        }

        [Server]
        private void OnInterrupted(string impactId)
        {
            ThrowIfClient(nameof(OnCastFinished));

            if (!_impactData.TryGetValue(impactId, out ImpactData data))
                data = new ImpactData(impactId);

            _impactData[impactId] = data.WithStatus(ImpactStatus.Interrupted);
        }

        [Server]
        private void OnEffectActivatedInPosition(string impactId, string effectId, Vector3 position)
        {
            ThrowIfClient(nameof(OnEffectActivatedInPosition));

            InvokeActivatedInPositionEvent(impactId, effectId, position);
            RpcInvokeActivatedInPositionEvent(impactId, effectId, position);
        }

        [Server]
        private void OnEffectAffectingInPosition(string impactId, string effectId, Vector3 position)
        {
            ThrowIfClient(nameof(OnEffectAffectingInPosition));

            InvokeAffectingInPositionEvent(impactId, effectId, position);
            RpcInvokeAffectingInPositionEvent(impactId, effectId, position);
        }

        [Server]
        private void OnEffectAffectingTarget(string impactId, string effectId, Vector3 position)
        {
            ThrowIfClient(nameof(OnEffectAffectingTarget));

            InvokeAffectingTargetEvent(impactId, effectId, position);
            RpcInvokeAffectingTargetEvent(impactId, effectId, position);
        }

        [Server]
        private void OnEffectAffectedTarget(string impactId, string effectId, Vector3 position)
        {
            ThrowIfClient(nameof(OnEffectAffectedTarget));

            InvokeAffectedTargetEvent(impactId, effectId, position);
            RpcInvokeAffectedTargetEvent(impactId, effectId, position);
        }

        private void ThrowIfClient(string methodName)
        {
            if (isClientOnly)
                throw new InvalidOperationException($"{methodName} method is only server-side");
        }
    }
}
