using System;
using System.Linq;
using Mirror;
using Zenject;
using UnityEngine;
using UnityEngine.Events;
using JetBrains.Annotations;
using static Mirror.SyncIDictionary<string,Frostgate.RiftHunters.Core.Battle.Shared.Impact.ImpactData>;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(ImpactState))]
    // здесь происходит вызов событий модели
    public sealed partial class ImpactState : NetworkBehaviour
    {
        [field: Header("Common Events")]
        [field: SerializeField] public UnityEvent<string> Interrupted { get; private set; }
        [field: SerializeField] public UnityEvent<string> CooldownUpdated { get; private set; }

        [field: Header("Cast Events")]
        [field: SerializeField] public UnityEvent<string> CastStarted { get; private set; }
        [field: SerializeField] public UnityEvent<string> CastFinished { get; private set; }

        [field: Header("Effect Events")]
        [field: SerializeField] public UnityEvent<string, string, Vector3> EffectActivatedInPosition { get; private set; }
        [field: SerializeField] public UnityEvent<string, string, Vector3> EffectAffectingInPosition { get; private set; }
        [field: SerializeField] public UnityEvent<string, string, Vector3> EffectAffectingTarget { get; private set; }
        [field: SerializeField] public UnityEvent<string, string, Vector3> EffectAffectedTarget { get; private set; }

        private readonly SyncDictionary<string, ImpactData> _impactData = new();

        [CanBeNull] private ILogger _logger;
        [CanBeNull] private SimulationTime _simulationTime;

        public bool IsCastingAny => _impactData.Values
            .Any(v => v.Status == ImpactStatus.Started);

        [Inject]
        private void MonoConstructor(ILogger<ImpactState> logger, SimulationTime simulationTime)
        {
            _logger = logger;
            _simulationTime = simulationTime;
        }

        private void Start() =>
            _impactData.Callback += OnDataUpdated;

        private void OnDestroy()
        {
            if (isServer) OnDestroyServer();
            _impactData.Callback -= OnDataUpdated;
        }

        public override void OnStartServer()
        {
            Initialize();
        }

        public override void OnStartClient()
        {
            if (isClientOnly)
                Initialize();
        }

        public bool IsCasting(string impactId) =>
            _impactData.TryGetValue(impactId, out ImpactData data) &&
            data.Status == ImpactStatus.Started;

        public TimeSpan PassedFromLastExecution(string impactId) =>
            _impactData.TryGetValue(impactId, out ImpactData data)
                ? TimeSpan.FromSeconds(_simulationTime!.Time - data.StartTimestamp)
                : TimeSpan.MaxValue;

        private void Initialize() =>
            _impactData.Values.ForEach(d =>
                OnDataUpdated(Operation.OP_ADD, d.Id, d));

        private void OnDataUpdated(Operation op,
            string key, ImpactData impactData) =>
            GetEventToInvoke(op, impactData)?.Invoke(key);

        [CanBeNull]
        private Action<string> GetEventToInvoke(
            Operation op, ImpactData impactData)
        {
            switch (op)
            {
                case Operation.OP_ADD:
                case Operation.OP_SET:
                    return impactData.Status switch
                    {
                        ImpactStatus.None => null,
                        ImpactStatus.Started => InvokeOnCastStarted,
                        ImpactStatus.Finished => InvokeOnCastFinished,
                        ImpactStatus.Interrupted => InvokeOnInterrupted,
                        _ => throw new ArgumentOutOfRangeException(nameof(ImpactStatus))
                    };

                case Operation.OP_CLEAR:
                case Operation.OP_REMOVE:
                    return InvokeOnCooldownUpdated;

                default: throw new ArgumentOutOfRangeException(nameof(op), op, null);
            }
        }

        private void InvokeOnCastStarted(string key)
        {
            _logger!.Log($"Cast Started: {key}");
            CastStarted?.Invoke(key);

            InvokeOnCooldownUpdated(key);
        }

        private void InvokeOnCastFinished(string key)
        {
            _logger!.Log($"Cast Finished: {key}");
            CastFinished?.Invoke(key);
            CooldownUpdated?.Invoke(key);
        }

        private void InvokeOnInterrupted(string key)
        {
            _logger!.Log($"Cast Interrupted: {key}");
            Interrupted?.Invoke(key);
        }

        private void InvokeOnCooldownUpdated(string key)
        {
            _logger!.Log($"Cooldown Updated: {key}");
            CooldownUpdated?.Invoke(key);
        }

        private void InvokeActivatedInPositionEvent(string impactId, string effectId, Vector3 position)
        {
            _logger!.Log($"Effect Activated: [Impact: {impactId}], [Effect: {effectId}], [Pos: {position}]");
            EffectActivatedInPosition?.Invoke(impactId, effectId, position);
        }

        private void InvokeAffectingInPositionEvent(string impactId, string effectId, Vector3 position)
        {
            _logger!.Log($"Effect Affecting: [Impact: {impactId}], [Effect: {effectId}],[Pos: {position}]");
            EffectAffectingInPosition?.Invoke(impactId, effectId, position);
        }

        private void InvokeAffectingTargetEvent(string impactId, string effectId, Vector3 position)
        {
            _logger!.Log($"Effect Affecting: [Impact: {impactId}], [Effect: {effectId}], [Target: {position}]");
            EffectAffectingTarget?.Invoke(impactId, effectId, position);
        }

        private void InvokeAffectedTargetEvent(string impactId, string effectId, Vector3 position)
        {
            _logger!.Log($"Effect Affected: [Impact: {impactId}], [Effect: {effectId}], [Target: {position}]");
            EffectAffectedTarget?.Invoke(impactId, effectId, position);
        }
    }
}