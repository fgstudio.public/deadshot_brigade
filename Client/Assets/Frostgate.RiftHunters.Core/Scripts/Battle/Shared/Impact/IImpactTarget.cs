namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    public interface IImpactTarget : IPositioned, INetworkIdentifiable
    {
        ImpactTargetType ImpactTargetType { get; }
    }
}
