using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    public readonly struct StubImpactTarget : IImpactTarget
    {
        public Vector3 Position { get; }
        public NetworkIdentity Identity { get; }
        public ImpactTargetType ImpactTargetType { get; }

        public StubImpactTarget(Vector3 position, NetworkIdentity identity, ImpactTargetType impactTargetType)
        {
            Position = position;
            Identity = identity;
            ImpactTargetType = impactTargetType;
        }
    }
}
