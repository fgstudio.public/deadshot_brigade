using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.UI;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(TotemHealthBar))]
    public sealed class TotemHealthBar : MonoBehaviour
    {
        [Required, ChildGameObjectsOnly] public Bar HealthBar;
        [Required, ChildGameObjectsOnly] public TotemState State;

        private void OnValidate()
        {
            HealthBar ??= GetComponentInChildren<Bar>();
            State ??= GetComponentInChildren<TotemState>();
        }

        private void Start()
        {
            State.HealthChanged += UpdateHealth;
            UpdateHealth(default);
        }

        private void OnDestroy()
        {
            State.HealthChanged -= UpdateHealth;
        }

        [ContextMenu(nameof(UpdateHealth))]
        private void UpdateHealth(float diff) =>
            HealthBar.Set(State.Health, State.MaxHealth);
    }
}