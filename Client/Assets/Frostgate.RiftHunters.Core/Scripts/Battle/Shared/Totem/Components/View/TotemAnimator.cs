using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(TotemAnimator))]
    public sealed class TotemAnimator : MonoBehaviour
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Animator _animator;

        [Header("Settings")]
        [Tooltip("0 — disabled"), SuffixLabel("sec")]
        [SerializeField, Range(0, 1)] private float _updateDt;
        [SerializeField, Required] private string _activeParameterName;

        private void OnValidate() => _animator ??= GetComponentInChildren<Animator>();
        private void Awake() => UpdateAnimator();

        [Button]
        public void Reset()
        {
            PlayDeactivating();
            UpdateAnimator();
        }

        [Button] public void PlayActivating() => _animator.SetBool(_activeParameterName, true);
        [Button] public void PlayDeactivating() => _animator.SetBool(_activeParameterName, false);

        private void UpdateAnimator()
        {
            if (_updateDt > 0)
                _animator.Update(_updateDt);
        }
    }
}