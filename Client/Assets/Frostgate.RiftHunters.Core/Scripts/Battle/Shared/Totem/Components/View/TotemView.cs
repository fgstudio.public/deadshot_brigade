using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface ITotemView : IAimTargetView
    { }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(TotemView))]
    public sealed class TotemView : MonoBehaviour, ITotemView
    {
        [field: Header("References")]
        [field: SerializeField, Required, ChildGameObjectsOnly] public Transform FxContainer { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public Transform DamageFxTarget { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public Transform DamageReflectFxTarget { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public TotemAnimator Animator { get; private set; }

        public Transform WorldFxContainer => transform;
    }
}