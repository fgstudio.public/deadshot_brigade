using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// КО: Коллекция тотемов.
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    [UsedImplicitly]
    public sealed class TotemNetIdCollection : TotemBaseCollection<uint>
    {
        public TotemNetIdCollection([NotNull] ILogger<TotemNetIdCollection> logger)
            : base(logger) { }
    }
}