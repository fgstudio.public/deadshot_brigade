using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [CreateAssetMenu(
        fileName = nameof(TotemConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(TotemConfig))]
    public sealed class TotemConfig : ScriptableObject
    {
        [field: SerializeField, MinValue(0)]
        public float Health { get; private set; }

        [field: SerializeField, CanBeNull]
        public BotSpawnerConfig SpawnerConfig { get; private set; }

        [field: SerializeField, HideLabel, BoxGroup(nameof(HpAutoLoss))]
        public HpAutoLossConfig HpAutoLoss { get; private set; }

        [SerializeField, Required] private UnitEffectConfig[] _effects;
        [SerializeField, Required] private UnitEffectConfig[] _postEffects;

        public IReadOnlyList<UnitEffectConfig> Effects => _effects;
        public IReadOnlyList<UnitEffectConfig> PostEffects => _postEffects;
    }
}