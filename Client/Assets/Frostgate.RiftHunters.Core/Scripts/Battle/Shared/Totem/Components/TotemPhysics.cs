using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface ITotemPhysics
    {
        Collider Collider { get; }
        float BodyRadius { get; }
        Vector3 FocusPoint { get; }
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(TotemPhysics))]
    public sealed class TotemPhysics : Component, ITotemPhysics
    {
        [Required, ChildGameObjectsOnly] public CapsuleCollider BodyCollider;
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _focusPoint;

        [Title("Info")]
        [ShowInInspector, ReadOnly] public float BodyRadius => BodyCollider.radius;
        [ShowInInspector, ReadOnly] public Vector3 FocusPoint => _focusPoint.position;

        Collider ITotemPhysics.Collider => BodyCollider;

        private void OnValidate()
        {
            BodyCollider ??= GetComponentInChildren<CapsuleCollider>(true);
            _focusPoint ??= GetComponentInChildren<Transform>(true);
        }
        protected override void OnEnabled() => BodyCollider.enabled = true;
        protected override void OnDisabled() => BodyCollider.enabled = false;
    }
}