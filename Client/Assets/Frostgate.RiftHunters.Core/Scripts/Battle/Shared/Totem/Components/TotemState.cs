using System;
using Mirror;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface ITotemState : IDamageReceiverState
    {
        event Action Died;
        event Action Revived;
        event Action HostUnitIdChanged;
        event Action<ITotem> Initialized;

        bool IsInitialized { get; }
        ITotem Totem { get; }
        TotemConfig Config { get; }
        uint HostUnitId { get; set; }
    }

    [DisallowMultipleComponent]
    [HelpURL("https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(TotemState))]
    public sealed class TotemState : NetworkBehaviour, ITotemState
    {
        private const string EventsFoldoutGroup = "Events";

        // TODO: Это нужно сделать SyncVar и задавать в фабрике
        [field: SerializeField] public TotemConfig Config { get; private set; }
        [field: SerializeField, Required] public Totem Totem { get; private set; }
        ITotem ITotemState.Totem => Totem;

        [SyncVar(hook = nameof(OnHealthChanged))] private float _health;
        [SyncVar(hook = nameof(OnHostUnitIdChanged))] private uint _hostUnitId;

        [SerializeField, FoldoutGroup(EventsFoldoutGroup)] private UnityEvent _died;
        [SerializeField, FoldoutGroup(EventsFoldoutGroup)] private UnityEvent _revived;
        [SerializeField, FoldoutGroup(EventsFoldoutGroup)] private UnityEvent<float> _healthChanged;
        [SerializeField, FoldoutGroup(EventsFoldoutGroup)] private UnityEvent _hostUnitIdChanged;

        public event Action Died;
        public event Action Revived;
        public event Action HostUnitIdChanged;
        public event Action<ITotem> Initialized;
        public event IReadOnlyHealth.Changed HealthChanged;

        public float Health
        {
            get => _health;
            set => NetworkHelper.ModifySyncVar(_health, value, v => _health = v, OnHealthChanged);
        }

        public uint HostUnitId
        {
            get => _hostUnitId;
            set => NetworkHelper.ModifySyncVar(_hostUnitId, value, v => _hostUnitId = v, OnHostUnitIdChanged);
        }

        public bool IsDead => Health <= 0;
        public float Defense => 0;
        public float BodyRadius => 0;
        public bool InDodge => false;
        public bool IsInvincible => false;
        public Vector3 Position => transform.position;
        public float MaxHealth => Config.Health;
        public bool IsInitialized { get; private set; }

        public override void OnStartClient() => SetInitialized(true);
        public override void OnStartServer() => SetInitialized(true);
        public override void OnStopClient() => SetInitialized(false);
        public override void OnStopServer() => SetInitialized(false);
        private void SetInitialized(bool status)
        {
            if (IsInitialized != status)
            {
                IsInitialized = status;
                if (status) Initialized?.Invoke(Totem);
            }
        }

        private void OnHealthChanged(float oldValue, float newValue)
        {
            if (Mathf.Approximately(oldValue, newValue)) return;

            InvokeHealthChanged(newValue - oldValue);
            if (oldValue > 0 && newValue <= 0) InvokeDied();
            if (oldValue <= 0 && newValue > 0) InvokeRevived();
        }

        private void OnHostUnitIdChanged(uint oldValue, uint newValue)
        {
            if (oldValue != newValue)
                InvokeHostUnitIdChanged();
        }

        private void InvokeHealthChanged(float diff)
        {
            HealthChanged?.Invoke(diff);
            _healthChanged?.Invoke(diff);
        }

        private void InvokeDied()
        {
            Died?.Invoke();
            _died?.Invoke();
        }

        private void InvokeRevived()
        {
            Revived?.Invoke();
            _revived?.Invoke();
        }

        private void InvokeHostUnitIdChanged()
        {
            HostUnitIdChanged?.Invoke();
            _hostUnitIdChanged?.Invoke();
        }
    }
}