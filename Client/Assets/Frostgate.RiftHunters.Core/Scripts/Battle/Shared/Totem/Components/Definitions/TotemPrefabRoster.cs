using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface ITotemPrefabRoster
    {
        IEnumerable<ITotem> Prefabs { get; }
        ITotem FindPrefab(Guid guid);
    }

    /// <summary>
    /// Ростер префабов для тотемов.
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    [CreateAssetMenu(
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(TotemPrefabRoster),
        fileName = nameof(TotemPrefabRoster))]
    [HelpURL("https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80")]
    public sealed partial class TotemPrefabRoster : ScriptableObject, ITotemPrefabRoster
    {
        [ValidateInput(nameof(HasElements), EmptyError)]
        [ValidateInput(nameof(ArePrefabsNotNull), NullPrefabsError)]
        [ValidateInput(nameof(AreAllTypesUnique), DuplicateTypesError)]
        [SerializeField] private Totem[] _prefabs;

        IEnumerable<ITotem> ITotemPrefabRoster.Prefabs => Prefabs;
        public IEnumerable<ITotem> Prefabs => _prefabs;

        ITotem ITotemPrefabRoster.FindPrefab(Guid guid) => FindPrefab(guid);
        public Totem FindPrefab(Guid guid) => _prefabs.FirstOrDefault(l => l.Identity.assetId == guid);
    }
}