using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// КО: Коллекция тотемов.
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    [UsedImplicitly]
    public abstract class TotemBaseCollection<TKey> : ObjectBaseCollection<TKey, ITotem>
    {
        protected TotemBaseCollection([NotNull] ILogger logger) : base(logger) { }

        protected sealed override string ExtractName(ITotem totem) =>
            (totem.Identity != null ? totem.Identity.netId : default).ToString();
    }
}
