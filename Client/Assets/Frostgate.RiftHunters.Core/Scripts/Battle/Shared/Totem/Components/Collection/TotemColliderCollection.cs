using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// КО: Коллекция тотемов.
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    [UsedImplicitly]
    public sealed class TotemColliderCollection : TotemBaseCollection<Collider>
    {
        public TotemColliderCollection([NotNull] ILogger<TotemColliderCollection> logger)
            : base(logger) { }
    }
}
