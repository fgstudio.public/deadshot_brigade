using Mirror;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using Frostgate.RiftHunters.Core.Components;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface ITotem : IImpactTarget, IAimTarget, IPoolObject, IAutoReleased, IDestroyable, IHpAutoLost, ISpawnable
    {
        ITotemPhysics Physics { get; }
        new ITotemView View { get; }
        new ITotemState State { get; }
    }

    [DisallowMultipleComponent]
    [RequireComponent(typeof(NetworkIdentity))]
    [HelpURL("https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(Totem))]
    public sealed class Totem : MonoBehaviour, ITotem
    {
        private const string EventsFoldoutGroup = "Events";

        [Required, ChildGameObjectsOnly] public NetworkIdentity Identity;
        [Required, ChildGameObjectsOnly] public TotemView View;
        [Required, ChildGameObjectsOnly] public TotemState State;
        [Required, ChildGameObjectsOnly] public TotemPhysics Physics;
        [Required, ChildGameObjectsOnly] public AutoDestroyComponent AutoDestroyComponent;
        [Required] public DamageZone DamageZone;

        [field: SerializeField, FoldoutGroup(EventsFoldoutGroup)] public UnityEvent Destroyed { get; private set; }

        public EntityType Type => EntityType.Totem;
        public bool IsPriorityTarget => true;
        public bool CapturedByDirectAiming => false;
        public Collider AimTarget => Physics.BodyCollider;
        public ImpactTargetType ImpactTargetType => ImpactTargetType.Totem;

        Vector3 IPositioned.Position => transform != null ? transform.position : Vector3.zero;
        GameObject IGameObject.GameObject => this != null? gameObject : null;
        NetworkIdentity INetworkIdentifiable.Identity => Identity;
        ITotemView ITotem.View => View;
        ITotemState ITotem.State => State;
        ITotemPhysics ITotem.Physics => Physics;
        IDamageReceiverState IDamageReceiver.State => State;
        IHealth IHpAutoLost.State => State;
        IHpAutoLossConfig IHpAutoLost.Config => State.Config.HpAutoLoss;
        IAimTargetView IAimTarget.View => View;
        BattleUnitType IAimTarget.UnitType => BattleUnitType.Bot;
        bool IDamageReceiver.DamageImmunity => false;
        float IAimTarget.BodyRadius => Physics.BodyRadius;
        Vector3 IAimTarget.FocusPoint => Physics.FocusPoint;
        bool IAimTarget.IsDestroyedOrDead => this == null || State.IsDead;

        DamageZone IDamageReceiver.GetDamageZone(Vector3 hitPoint, float breakShieldChance) => DamageZone;
        DamageZone IDamageReceiver.GetDamageZone(float zoneMultiplier) => DamageZone;
        public void TryExecuteUnitAction(UnitActionTypes actionType, Vector3 casterPosition) { }
        public float GetDamageMultiplier(DamageType damageType) => 1;

        public void AddUnitEffect(UnitEffectState effect) {}
        public bool HasUnitEffect(UnitEffectState effect) => false;
        public void RemoveUnitEffect(UnitEffectState effect) {}
        public bool TryGetUnitEffect(UnitEffectConfig config, out UnitEffectState state)
        {
            state = null;
            return false;
        }

        public bool CanReceiveUnitEffect(UnitEffectConfig config) => false;
        public bool IsFriendly(UnitNetwork sourceUnit) => false;

        private UnityAction<IAutoReleased> _readyForReleaseAction;
        event UnityAction<IAutoReleased> IAutoReleased.ReadyForRelease
        {
            add => _readyForReleaseAction += value;
            remove => _readyForReleaseAction -= value;
        }

        void IPoolObject.OnGet() => InitComponents();
        void IPoolObject.OnRelease() => View.Animator.Reset();

        private void OnValidate() => Init();
        private void Awake() => Init();
        private void Start()
        {
            InitState();
            Subscribe();
        }
        private void OnDestroy()
        {
            Unsubscribe();
            Destroyed?.Invoke();
        }

        private void Init()
        {
            InitReferences();
            InitComponents();
        }

        private void InitReferences()
        {
            Identity ??= GetComponentInChildren<NetworkIdentity>();
            State ??= GetComponentInChildren<TotemState>();
            View ??= GetComponentInChildren<TotemView>();
            Physics ??= GetComponentInChildren<TotemPhysics>();
            AutoDestroyComponent ??= GetComponentInChildren<AutoDestroyComponent>();
        }

        private void InitComponents()
        {
            Physics.Enable();
            AutoDestroyComponent.Disable();
        }

        private void InitState()
        {
            if (State.IsDead) OnDied();
            else OnRevived();
        }

        private void Subscribe()
        {
            State.Died += OnDied;
            State.Revived += OnRevived;
            AutoDestroyComponent.DestroyComponent.Destroyed.AddListener(Destroy);
        }

        private void Unsubscribe()
        {
            State.Died -= OnDied;
            State.Revived -= OnRevived;
            AutoDestroyComponent.DestroyComponent.Destroyed.RemoveListener(Destroy);
        }

        private void OnDied()
        {
            Physics.Disable();
            AutoDestroyComponent.Enable();
            View.Animator.PlayDeactivating();
        }

        private void OnRevived()
        {
            InitComponents();
            View.Animator.PlayActivating();
        }

        [ContextMenu(nameof(Destroy))] public void Destroy() => InvokeReadyForRelease();
        private void InvokeReadyForRelease() => _readyForReleaseAction?.Invoke(this);
    }
}