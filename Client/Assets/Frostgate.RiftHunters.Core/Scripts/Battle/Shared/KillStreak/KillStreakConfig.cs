﻿using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Network.Shared.KillStreak
{
    [CreateAssetMenu(fileName = nameof(KillStreakConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(KillStreakConfig))]
    public class KillStreakConfig : ScriptableObject
    {
        [SerializeField, MinValue(0)] private int _killsForBonus;
        [SerializeField, MinValue(0), SuffixLabel("sec", true)] private float _streakCooldown;

        [SerializeField, Required, AssetSelector] private List<StreakBonusConfig> _bonusConfigs;

        public int KillsForBonus => _killsForBonus;
        public TimeSpan StreakCooldown => TimeSpan.FromSeconds(_streakCooldown);
        public IReadOnlyList<StreakBonusConfig> BonusConfigs => _bonusConfigs;
    }
}