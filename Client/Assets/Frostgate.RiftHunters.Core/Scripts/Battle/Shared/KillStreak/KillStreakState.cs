﻿using System;
using Mirror;
using UnityEngine;
using static Mirror.SyncList<Frostgate.RiftHunters.Core.Network.Shared.KillStreak.KillStreakData>;

namespace Frostgate.RiftHunters.Core.Network.Shared.KillStreak
{
    public interface IReadOnlyKillStreakState
    {
        event Action StreakReset;
        event Action<KillStreakData> StreakCountChanged;
        int StreakCount { get; }
    }

    [DisallowMultipleComponent]
    public class KillStreakState : NetworkBehaviour, IReadOnlyKillStreakState
    {
        public event Action StreakReset;
        public event Action<KillStreakData> StreakCountChanged;

        public int StreakCount => _streakDatas.Count;

        private SyncList<KillStreakData> _streakDatas = new();

        public override void OnStartServer()
        {
            Initialize();
        }

        public override void OnStartClient()
        {
            if (isClientOnly)
                Initialize();
        }

        public void OnDestroy()
        {
            Deinitialize();
        }

        private void Initialize()
        {
            _streakDatas.Callback += OnStreakChanged;

            for (int i = 0; i < _streakDatas.Count; i++)
            {
                KillStreakData data = _streakDatas[i];
                OnStreakChanged(Operation.OP_SET, i, data, data);
            }
        }

        private void Deinitialize()
        {
            _streakDatas.Callback -= OnStreakChanged;
        }

        private void OnStreakChanged(Operation op, int itemIndex, KillStreakData oldItem, KillStreakData newItem)
        {
            switch (op)
            {
                case Operation.OP_ADD:
                case Operation.OP_SET:
                case Operation.OP_INSERT:
                    StreakCountChanged?.Invoke(newItem);
                    break;
            }
        }

        [Server]
        public void AddCount(uint netId, float deathTime)
        {
            var killStreakData = new KillStreakData(netId, deathTime);
            _streakDatas.Add(killStreakData);
        }

        [Server]
        public void ResetStreak()
        {
            _streakDatas.Clear();
            StreakReset?.Invoke();
        }
    }
}