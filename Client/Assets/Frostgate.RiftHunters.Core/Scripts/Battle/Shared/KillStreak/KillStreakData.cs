﻿using System;

namespace Frostgate.RiftHunters.Core.Network.Shared.KillStreak
{
    [Serializable]
    public struct KillStreakData
    {
        public readonly uint NetId;
        public readonly float TimeKill;

        public KillStreakData(uint netId, float timeKill)
        {
            NetId = netId;
            TimeKill = timeKill;
        }
    }
}