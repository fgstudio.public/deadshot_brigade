using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [System.Serializable]
    public sealed class LootBoxTuple
    {
        public LootBoxType Type => _type;
        public LootBox LootBox => _lootBox;


        [SerializeField] private LootBoxType _type;
        [SerializeField, Required, AssetsOnly, AssetList] private LootBox _lootBox;
    }
}