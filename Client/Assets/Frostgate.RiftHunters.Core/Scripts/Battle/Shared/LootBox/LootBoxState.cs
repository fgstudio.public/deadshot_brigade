using System;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface IReadOnlyLootBoxState
    {
        event Action Initialized;
        bool IsInitialized { get; }

        event Action Collected;
        uint CollectorId { get; }
        bool IsCollected { get; }

        IReadOnlyCollection<UnitEffectConfig> UnitEffects { get; }
        LootBoxConfig Config { get; }
        LootBoxType Type { get; }
    }

    public interface IEditableLootBoxState : IReadOnlyLootBoxState, IResettable
    {
        new uint CollectorId { get; [Server] set; }
        new LootBoxConfig Config { get; [Server] set; }
    }

    /// <summary>
    /// Стэйт лут-бокса для синхронизации.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(LootBoxState))]
    public sealed class LootBoxState : NetworkBehaviour, IEditableLootBoxState
    {
        public event Action Initialized;
        public event Action Collected;

        [SyncVar]
        [SerializeField] private LootBoxConfig _config;

        [SyncVar]
        [SerializeField] private uint _hostId;

        [SyncVar(hook = nameof(OnCollectorIdChanged))]
        [SerializeField] private uint _collectorId;

        public LootBoxType Type => Config.Type;
        public IReadOnlyCollection<UnitEffectConfig> UnitEffects => Config.UnitEffects;
        public bool IsCollected => IsCollectedWith(_collectorId);
        public bool IsInitialized { get; private set; }

        public LootBoxConfig Config
        {
            get => _config;
            [Server] set => _config = value;
        }

        public uint CollectorId
        {
            get => _collectorId;
            [Server] set => NetworkHelper.ModifySyncVar(_collectorId, value, v => _collectorId = value, OnCollectorIdChanged);
        }

        public override void OnStartClient()
        {
            base.OnStartClient();
            if (!isServer) Initialize();
        }

        public override void OnStartServer()
        {
            base.OnStartServer();
            Initialize();
        }

        public void Reset()
        {
            if (isServer)
            {
                _config = null;
                _hostId = default;
                _collectorId = default;
            }
        }

        private void Initialize()
        {
            IsInitialized = true;
            Initialized?.Invoke();
        }

        private void OnCollectorIdChanged(uint oldValue, uint newValue)
        {
            if (oldValue != newValue && IsCollectedWith(newValue))
                Collected?.Invoke();
        }

        private bool IsCollectedWith(uint collectorId) => collectorId != default;
    }
}