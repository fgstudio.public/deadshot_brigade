using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [Serializable]
    [CreateAssetMenu(fileName = nameof(LootSetConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(LootSetConfig))]
    public sealed partial class LootSetConfig : ScriptableObject
    {
        public const float MinChance = 0;
        public const float MaxChance = 100;
        public const int MinAmount = 1;
        public const int MaxAmount = 100;

        [field: SerializeField, Range(MinChance, MaxChance)] public float DropChance { get; private set; }
        [field: SerializeField, Range(MinAmount, MaxAmount)] public int Amount { get; private set; }

        [ValidateInput(nameof(IsChancesSumValid), InvalidChancesError)]
        [field: SerializeField] public LootBoxDrop[] LootBoxes { get; private set; }
    }
}