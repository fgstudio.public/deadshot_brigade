using System;
using System.Threading;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public sealed class ColliderBlocker : MonoBehaviour
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Collider _collider;

        [Header("Settings")]
        [SerializeField] private bool _autoBlockOnEnable;
        [SerializeField, MinValue(0), SuffixLabel("sec", true)] private float _blockTime;

        [CanBeNull] private CancellationTokenSource _cts;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() => _collider ??= GetComponentInChildren<Collider>();

        private void OnEnable()
        {
            if (_autoBlockOnEnable)
                BlockForAWhile().AsAsyncUnitUniTask();
        }

        private void OnDisable() =>
            Stop();

        [Button]
        public async UniTask BlockForAWhile()
        {
            Stop();

            _cts = new CancellationTokenSource();
            CancellationToken token = _cts.Token;

            _collider.enabled = false;
            await UniTask.Delay(TimeSpan.FromSeconds(_blockTime), cancellationToken: token);
            _collider.enabled = true;
        }

        [Button]
        public void Stop()
        {
            if (_cts != null)
            {
                _cts.Cancel();
                _cts.Dispose();
                _cts = null;
            }
        }
    }
}
