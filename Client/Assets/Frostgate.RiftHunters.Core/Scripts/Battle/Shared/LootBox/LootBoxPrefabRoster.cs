using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// Ростер префабов для лутбоксов.
    /// Должен содержать префабы лутбоксов всех видов.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    [CreateAssetMenu(
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(LootBoxPrefabRoster),
        fileName = nameof(LootBoxPrefabRoster))]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    public sealed partial class LootBoxPrefabRoster : ScriptableObject
    {
        [ValidateInput(nameof(HasElements), EmptyError)]
        [ValidateInput(nameof(HasAllTypes), NotSetTypes)]
        [ValidateInput(nameof(ArePrefabsNotNull), NullPrefabsError)]
        [ValidateInput(nameof(AreAllTypesUnique), DuplicateTypesError)]
        [SerializeField] private LootBoxTuple[] _lootBoxTuples;

        public IEnumerable<LootBoxType> Types => _lootBoxTuples.Select(t => t.Type);
        public IEnumerable<LootBox> Prefabs => _lootBoxTuples.Select(t => t.LootBox);

        public LootBoxType FindType(Guid guid) => _lootBoxTuples.First(t => t.LootBox.Identity.assetId == guid).Type;
        public LootBox FindPrefab(LootBoxType type) => _lootBoxTuples.First(t => t.Type == type).LootBox;
    }
}