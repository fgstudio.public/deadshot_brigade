using System;
using System.Collections;
using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public sealed class LooterAttractionCache
    {
        private Vector3 _position;
        private Vector3 _scale;
        public bool HasCache { get; private set; }

        public void Cache([NotNull] Transform target)
        {
            _position = target.position;
            _scale = target.localScale;
            HasCache = true;
        }

        public void Restore([NotNull] Transform target)
        {
            if (!HasCache) throw new InvalidOperationException();
            target.position = _position;
            target.localScale = _scale;
            HasCache = false;
        }
    }

    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.Menu + "/" + nameof(LooterAttraction))]
    public sealed class LooterAttraction : MonoBehaviour
    {
        [SerializeField] private AnimationCurve _animationCurve;
        [SerializeField] private AnimationCurve _scaleCurve;
        [SerializeField, Range(0.1f, 5f)] private float _durationTime = 1.0f;
        [SerializeField] private float _positionHeight = 0.8f;

        [CanBeNull] private Coroutine _coroutine;
        [NotNull] private readonly LooterAttractionCache _cache = new();

        public void FollowTarget([NotNull] Transform target, [CanBeNull] Action onFinish)
        {
            Reset();

            _cache.Cache(transform);
            _coroutine = StartCoroutine(FollowRoutine(target, onFinish));
        }

        public void Reset()
        {
            if (_coroutine != null)
            {
                StopCoroutine(_coroutine);
                _coroutine = null;
            }

            if (_cache.HasCache)
                _cache.Restore(transform);
        }

        private IEnumerator FollowRoutine([CanBeNull] Transform target, [CanBeNull] Action onFinish)
        {
            float time = 0;

            Transform self = transform;
            Vector3 startPosition = self.position;

            while (time < _durationTime && self != null && target != null)
            {
                time += Time.deltaTime;

                float ratio = time / _durationTime;
                float curveValue = _animationCurve.Evaluate(ratio);
                float scaleValue = _scaleCurve.Evaluate(ratio);

                Vector3 targetPosition = target.position;
                targetPosition.y += _positionHeight;

                self.position = Vector3.Lerp(startPosition, targetPosition, curveValue);
                self.localScale = new Vector3(scaleValue, scaleValue, scaleValue);

                yield return null;
            }

            onFinish?.Invoke();
        }
    }
}