using Mirror;
using DG.Tweening;
using UnityEngine;
using System.Threading;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

using Random = UnityEngine.Random;
using Sequence = DG.Tweening.Sequence;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [RequireComponent(typeof(LootBox))]
    public sealed class LootBoxSpawnEffect : MonoBehaviour
    {
        [Header("References")]
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private LootBox _lootBox;
        [SerializeField, Required, ChildGameObjectsOnly] private Transform _visualTransform;

        [Header("Settings")]
        [SerializeField, MinValue(0)] private float _duration;
        [SerializeField] private float _startHeight;
        [SerializeField] private float _minHeight;
        [SerializeField] private float _maxHeight;
        [SerializeField] private AnimationCurve _fallCurve;

        [CanBeNull] private CancellationTokenSource _delayCts;
        [CanBeNull] private Sequence _sequence;

        private void OnValidate() =>
            _lootBox ??= GetComponent<LootBox>();

        private void OnEnable()
        {
            if (_lootBox.State.IsInitialized) OnInitialized();
            else _lootBox.State.Initialized += OnInitialized;
        }

        private void OnDisable()
        {
            _lootBox.State.Initialized -= OnInitialized;
            Stop();
        }

        private void OnInitialized() =>
            PlayWithDelay(1);

        private void PlayWithDelay(int frames)
        {
            _delayCts = new CancellationTokenSource();

            UniTask.DelayFrame(frames, cancellationToken: _delayCts.Token)
                .ContinueWith(Play)
                .AsAsyncUnitUniTask();
        }

        [Button]
        private void Play()
        {
            if (!NetworkClient.spawned.TryGetValue(_lootBox.P2PObject.HostId, out NetworkIdentity hostIdentity))
                return;

            _visualTransform.transform.localPosition =
                (hostIdentity.transform.position -_lootBox.transform.position)
                .SetY(_startHeight);

            _visualTransform.DOLocalMoveX(0f, _duration);
            _visualTransform.DOLocalMoveZ(0f, _duration);
            _visualTransform.localScale = Vector3.zero;
            _visualTransform.DOScale(1f, _duration);

            _sequence = DOTween.Sequence();
            _sequence.Append(_visualTransform.DOLocalMoveY(Random.Range(_minHeight, _maxHeight), _duration / 2))
                .Append(_visualTransform.DOLocalMoveY(0, _duration / 2).SetEase(_fallCurve));
            _sequence.Play();
        }

        [Button]
        private void Stop()
        {
            _sequence?.Kill();

            if (_delayCts != null)
            {
                _delayCts.Cancel();
                _delayCts.Dispose();
                _delayCts = null;
            }
        }
    }
}