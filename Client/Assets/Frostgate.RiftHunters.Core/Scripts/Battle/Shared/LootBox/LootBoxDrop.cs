using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// Настройка выпадения лутбокса с текущего персонажа после смерти.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    [Serializable]
    public sealed class LootBoxDrop
    {
        public const float MinChance = 0;
        public const float MaxChance = 100;

        [field: SerializeField, Range(MinChance, MaxChance)] public float DropChance { get; private set; }
        [field: SerializeField] public LootBoxConfig Config { get; private set; }
    }
}