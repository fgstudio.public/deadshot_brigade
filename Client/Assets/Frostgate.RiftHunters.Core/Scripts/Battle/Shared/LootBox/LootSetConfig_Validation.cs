﻿using System.Linq;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public sealed partial class LootSetConfig
    {
        private const string InvalidChancesError = "Sum of chances isn't valid";
        private bool IsChancesSumValid(LootBoxDrop[] lootBoxes)
        {
            float chancesSum = lootBoxes.Sum(lb => lb.DropChance);
            return chancesSum is >= LootBoxDrop.MinChance and <= LootBoxDrop.MaxChance;
        }
    }
}