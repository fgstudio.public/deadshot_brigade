using System;
using System.Linq;
using Frostgate.RiftHunters.Core.Utils;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public partial class LootBoxPrefabRoster
    {
        private const string EmptyError = "Is empty";
        private const string NotSetTypes = "Has not set types";
        private const string NullPrefabsError = "Has null prefabs";
        private const string DuplicateTypesError = "Has duplicate types";

        private bool HasElements() => _lootBoxTuples.Length > 0;
        private bool ArePrefabsNotNull() => !Validator.HasNullElements(_lootBoxTuples.Select(p => p.LootBox));
        private bool AreAllTypesUnique() => !Validator.HasDuplicateElements(
            _lootBoxTuples.Where(p => p != null).Select(p => p.Type));
        private bool HasAllTypes()
        {
            string[] setTypes = _lootBoxTuples
                .Where(p => p != null)
                .Select(p => p.Type.ToString("F"))
                .ToArray();

            string[] actualTypes = Enum.GetNames(typeof(LootBoxType));

            return actualTypes.All(t => setTypes.Contains(t));
        }
    }
}
