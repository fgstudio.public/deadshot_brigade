using System;
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Client;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface IClientLootBox : IGameObject, INetworkIdentifiable, IDestroyable, IPoolObject, IAutoReleased
    {
        event Action<IClientLootBox> Collected;
        IReadOnlyLootBoxState State { get; }
    }

    public interface IServerLootBox : IGameObject, INetworkIdentifiable, IDestroyable, IPoolObject, IAutoReleased
    {
        event Action<IServerLootBox> Collected;
        IEditableLootBoxState State { get; }
    }

    /// <summary>
    /// Ящики с наградой, выпадающий из убитых противников
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(LootBoxState))]
    [RequireComponent(typeof(NetworkIdentity))]
    [RequireComponent(typeof(CollectableMechanic))]
    [RequireComponent(typeof(P2PObject))]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(LootBox))]
    public sealed class LootBox : MonoBehaviour, IClientLootBox, IServerLootBox, IResettable
    {
        public event Action<LootBox> Collected;
        private Action<IClientLootBox> _clientCollected = delegate { };
        private Action<IServerLootBox> _serverCollected = delegate { };
        event Action<IClientLootBox> IClientLootBox.Collected
        {
            add => _clientCollected += value;
            remove => _clientCollected -= value;
        }
        event Action<IServerLootBox> IServerLootBox.Collected
        {
            add => _serverCollected += value;
            remove => _serverCollected -= value;
        }

        [field: SerializeField, FoldoutGroup("Events")] public UnityEvent Destroyed { get; private set; }

        [Header("References")]
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private P2PObject _p2PObject;
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private LootBoxState _state;
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private NetworkIdentity _identity;
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private CollectableMechanic _collectable;
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private LooterAttraction _looterAttraction;

        [Header("VFX")]
        [SerializeField] private UnitViewVFX _collectedVfx;

        public NetworkIdentity Identity => _identity;
        public bool IsCollected => _collectable.IsCollected;

        IReadOnlyLootBoxState IClientLootBox.State => _state;
        IEditableLootBoxState IServerLootBox.State => _state;
        public LootBoxState State => _state;
        public P2PObject P2PObject => _p2PObject;
        GameObject IGameObject.GameObject => this != null? gameObject : null;


        private UnityAction<IAutoReleased> _readyForReleaseAction;
        event UnityAction<IAutoReleased> IAutoReleased.ReadyForRelease
        {
            add => _readyForReleaseAction += value;
            remove => _readyForReleaseAction -= value;
        }

        void IPoolObject.OnGet()
        {
            _state.Collected += OnStateCollected;
        }

        void IPoolObject.OnRelease()
        {
            _state.Collected -= OnStateCollected;
            Reset();
        }

        private void Start()
        {
            _collectable.Destroyed.AddListener(Destroy);
        }

        private void OnDestroy()
        {
            _collectable.Destroyed.RemoveListener(Destroy);
            Destroyed?.Invoke();
        }

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _p2PObject = GetComponent<P2PObject>();
            _state ??= GetComponent<LootBoxState>();
            _identity ??= GetComponent<NetworkIdentity>();
            _collectable ??= GetComponent<CollectableMechanic>();
            _looterAttraction ??= GetComponent<LooterAttraction>();
        }

        [ContextMenu(nameof(Reset))]
        public void Reset()
        {
            _p2PObject.Reset();
            _state.Reset();
            _collectable.Reset();
            _looterAttraction.Reset();
        }

        [ContextMenu(nameof(Destroy))] public void Destroy() => InvokeReadyForRelease();
        private void InvokeReadyForRelease() => _readyForReleaseAction?.Invoke(this);

        private void OnStateCollected()
        {
            uint collectorId = _state.CollectorId;

            if (Identity.isClient && NetworkClient.spawned.TryGetValue(collectorId, out NetworkIdentity collector))
            {
                Transform collectorTransform = collector.transform;
                _looterAttraction.FollowTarget(collectorTransform, () => _collectable.Collect());
                if (_collectedVfx != null) Instantiate(_collectedVfx, collectorTransform);
            }
            else
            {
                _collectable.Collect();
            }

            InvokeCollected();
        }

        private void InvokeCollected()
        {
            Collected?.Invoke(this);
            _clientCollected?.Invoke(this);
            _serverCollected?.Invoke(this);
        }
    }
}