using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Client;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    [Serializable]
    [CreateAssetMenu(fileName = nameof(LootBoxConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(LootBoxConfig))]
    public sealed class LootBoxConfig : ScriptableObject
    {
        [field: SerializeField] public LootBoxType Type { get; private set; }
        [field: SerializeField] public float DropRadius { get; private set; }

        public IReadOnlyCollection<UnitEffectConfig> UnitEffects => _unitEffects;

        // TODO: Rewards List
        [SerializeField] private UnitEffectConfig[] _unitEffects;
    }
}