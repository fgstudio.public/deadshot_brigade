namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// Виды лутбоксов по цветам.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public enum LootBoxType
    {
        Green,
        Blue,
        Purple
    }
}
