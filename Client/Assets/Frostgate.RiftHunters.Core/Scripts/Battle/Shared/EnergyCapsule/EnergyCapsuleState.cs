using System;
using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface IReadOnlyEnergyCapsuleState
    {
        event Action Collected;
        float Energy { get; }
        uint CollectorId { get; }
        bool IsCollected { get; }
    }

    public interface IEditableEnergyCapsuleState : IReadOnlyEnergyCapsuleState
    {
        new float Energy { get; [Server] set; }
        new uint CollectorId { get; [Server] set; }
    }

    /// <summary>
    /// Стэйт энергетической капсулы для синхронизации.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(EnergyCapsuleState))]
    public sealed class EnergyCapsuleState : NetworkBehaviour, IEditableEnergyCapsuleState
    {
        public event Action Collected;

        [SyncVar] private float _energy;

        [SyncVar(hook = nameof(OnCollectorIdChanged))]
        [SerializeField] private uint _collectorId;

        public bool IsCollected => IsCollectedWith(_collectorId);

        public uint CollectorId
        {
            get => _collectorId;
            [Server] set => NetworkHelper.ModifySyncVar(_collectorId, value, v => _collectorId = value, OnCollectorIdChanged);
        }

        public float Energy
        {
            get => _energy;
            [Server] set => _energy = value;
        }

        public void Reset()
        {
            if (isServer)
            {
                _energy = default;
                _collectorId = default;
            }
        }

        private void OnCollectorIdChanged(uint oldValue, uint newValue)
        {
            if (oldValue != newValue && IsCollectedWith(newValue))
                Collected?.Invoke();
        }

        private bool IsCollectedWith(uint collectorId) => collectorId != default;
    }
}