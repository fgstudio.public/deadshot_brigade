using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Energy
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public readonly struct EnergyCapsuleSourceDTO : IEnergyCapsuleSource
    {
        public readonly Transform Transform { get; }
        public readonly NetworkIdentity Identity { get; }
        public readonly EnergyCapsuleConfig EnergyCapsuleForDeath { get; }

        public EnergyCapsuleSourceDTO(Transform transform, NetworkIdentity identity, EnergyCapsuleConfig energyCapsuleForDeath)
        {
            Transform = transform;
            Identity = identity;
            EnergyCapsuleForDeath = energyCapsuleForDeath;
        }
    }
}
