using System;
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    public interface IClientEnergyCapsule : INetworkIdentifiable, IDestroyable, IPoolObject, IGameObject, IAutoReleased
    {
        event Action<IClientEnergyCapsule> Collected;
        IReadOnlyEnergyCapsuleState State { get; }
    }

    public interface IServerEnergyCapsule : INetworkIdentifiable, IDestroyable, IPoolObject, IGameObject, IAutoReleased
    {
        event Action<IServerEnergyCapsule> Collected;
        IEditableEnergyCapsuleState State { get; }
    }

    /// <summary>
    /// Энергетическая капсула, выпадающая из убитых противников
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(NetworkIdentity))]
    [RequireComponent(typeof(EnergyCapsuleState))]
    [RequireComponent(typeof(CollectableMechanic))]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(EnergyCapsule))]
    public sealed class EnergyCapsule : MonoBehaviour, IClientEnergyCapsule, IServerEnergyCapsule, IResettable
    {
        public event Action<EnergyCapsule> Collected;
        private Action<IClientEnergyCapsule> _clientCollected = delegate { };
        private Action<IServerEnergyCapsule> _serverCollected = delegate { };
        event Action<IClientEnergyCapsule> IClientEnergyCapsule.Collected
        {
            add => _clientCollected += value;
            remove => _clientCollected -= value;
        }
        event Action<IServerEnergyCapsule> IServerEnergyCapsule.Collected
        {
            add => _serverCollected += value;
            remove => _serverCollected -= value;
        }

        [field: SerializeField] public UnityEvent Destroyed { get; private set; }

        [Header("References")]
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private NetworkIdentity _identity;
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private EnergyCapsuleState _state;
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private CollectableMechanic _collectable;
        [SerializeField, Required, ReadOnly, ChildGameObjectsOnly] private LooterAttraction _looterAttraction;

        [Header("VFX")] [SerializeField, Required] private GameObject _collectedVfx;

        public EnergyCapsuleState State => _state;
        IReadOnlyEnergyCapsuleState IClientEnergyCapsule.State => _state;
        IEditableEnergyCapsuleState IServerEnergyCapsule.State => _state;

        public NetworkIdentity Identity => _identity;
        GameObject IGameObject.GameObject => this != null? gameObject : null;


        private UnityAction<IAutoReleased> _readyForReleaseAction;
        event UnityAction<IAutoReleased> IAutoReleased.ReadyForRelease
        {
            add => _readyForReleaseAction += value;
            remove => _readyForReleaseAction -= value;
        }

        void IPoolObject.OnGet()
        {
            _state.Collected += OnStateCollected;
        }
        void IPoolObject.OnRelease()
        {
            _state.Collected -= OnStateCollected;
            Reset();
        }

        private void Start()
        {
            _collectable.Destroyed.AddListener(Destroy);
        }

        private void OnDestroy()
        {
            _collectable.Destroyed.RemoveListener(Destroy);
            Destroyed?.Invoke();
        }

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences()
        {
            _identity ??= GetComponent<NetworkIdentity>();
            _state ??= GetComponent<EnergyCapsuleState>();
            _collectable ??= GetComponent<CollectableMechanic>();
            _looterAttraction ??= GetComponent<LooterAttraction>();
        }

        [ContextMenu(nameof(Reset))]
        public void Reset()
        {
            _state.Reset();
            _collectable.Reset();
            _looterAttraction.Reset();
        }

        [ContextMenu(nameof(Destroy))] public void Destroy() => InvokeReadyForRelease();
        private void InvokeReadyForRelease() => _readyForReleaseAction?.Invoke(this);

        private void OnStateCollected()
        {
            uint collectorId = _state.CollectorId;

            if (Identity.isClient && NetworkClient.spawned.TryGetValue(collectorId, out NetworkIdentity collector))
            {
                Transform collectorTransform = collector.transform;
                _looterAttraction.FollowTarget(collectorTransform, () =>
                {
                    _collectable.Collect();
                    Instantiate(_collectedVfx, collectorTransform);
                });
            }
            else
            {
                _collectable.Collect();
            }

            InvokeCollected();
        }

        private void InvokeCollected()
        {
            Collected?.Invoke(this);
            _clientCollected?.Invoke(this);
            _serverCollected?.Invoke(this);
        }
    }
}