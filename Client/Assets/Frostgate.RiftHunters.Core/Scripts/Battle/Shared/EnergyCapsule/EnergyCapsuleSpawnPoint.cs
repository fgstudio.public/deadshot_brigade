using Mirror;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Network.Server;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Energy
{
    [RequireComponent(typeof(EnergyCapsuleSpawnPointState))]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(EnergyCapsuleSpawnPoint))]
    public sealed class EnergyCapsuleSpawnPoint : MonoBehaviour
    {
        [Header("References")]
        [Required, ChildGameObjectsOnly]
        [SerializeField] private EnergyCapsuleSpawnPointState _state;

        [Header("Settings")]
        [SerializeField] private bool _autoSpawnOnStart;

        [HideLabel, BoxGroup("Energy Capsule Drop Settings")]
        [SerializeField] private EnergyCapsuleConfig _config;

        [CanBeNull] private EnergyCapsuleMediator _mediator;
        [CanBeNull] private IEnergyCapsuleSource _capsuleSource;
        private readonly ILogger _logger = LoggerFactory.CreateLogger<EnergyCapsuleSpawnPoint>();

        public EnergyCapsuleSpawnPointState State => _state;

        [Inject]
        private void MonoConstructor([InjectOptional] EnergyCapsuleMediator mediator)
        {
            _mediator = mediator;
            _capsuleSource = new EnergyCapsuleSourceDTO(transform, _state.netIdentity, _config);
        }

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() =>
            _state ??= GetComponent<EnergyCapsuleSpawnPointState>();

        private void Start()
        {
            if (_autoSpawnOnStart && _state.isServer)
                Spawn();
        }

        [Server, Button]
        public void Spawn()
        {
            IServerEnergyCapsule energyCapsule = _mediator!.CreateCapsule(_capsuleSource!);
            _logger.Log("Spawned");

            InitCapsule(energyCapsule);
        }

        private void InitCapsule(IServerEnergyCapsule capsule)
        {
            SubscribeOnCapsule(capsule);
            _state.AddSpawnedCapsule(capsule);
            _logger.Log("Capsule Initialized");
        }

        private void DeinitCapsule(IServerEnergyCapsule capsule)
        {
            UnsubscribeFromCapsule(capsule);
            _state.RemoveSpawnedCapsule(capsule);
            _logger.Log("Capsule Deinitialized");
        }

        private void SubscribeOnCapsule(IServerEnergyCapsule energyCapsule)
        {
            energyCapsule.Collected += OnCapsuleCollected;
            energyCapsule.ReadyForRelease += OnCapsuleReleasing;
        }

        private void UnsubscribeFromCapsule(IServerEnergyCapsule energyCapsule)
        {
            energyCapsule.Collected -= OnCapsuleCollected;
            energyCapsule.ReadyForRelease -= OnCapsuleReleasing;
        }

        private void OnCapsuleReleasing(IAutoReleased autoReleased)
        {
            if (autoReleased is IServerEnergyCapsule capsule)
                DeinitCapsule(capsule);
        }

        private void OnCapsuleCollected(IServerEnergyCapsule capsule)
        {
            DeinitCapsule(capsule);
        }
    }
}