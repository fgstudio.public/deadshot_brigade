using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle.Shared
{
    /// <summary>
    /// Настройка выпадения энергетической капсулы с текущего персонажа после смерти.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    [Serializable]
    public sealed class EnergyCapsuleConfig
    {
        public const float MinChance = 0;
        public const float MaxChance = 100;

        public const float MinEnergy = 0;
        public const float MaxEnergy = 100;

        private const float MinOffset = 0;
        private const float MaxOffset = 5;

        [field: SerializeField, Range(MinEnergy, MaxEnergy)] public float Energy { get; private set; }
        [field: SerializeField, Range(MinChance, MaxChance)] public float DropChance { get; private set; }
        [field: SerializeField, Range(MinOffset, MaxOffset)] public float MaxRandomPositionOffset { get; private set; }
    }
}
