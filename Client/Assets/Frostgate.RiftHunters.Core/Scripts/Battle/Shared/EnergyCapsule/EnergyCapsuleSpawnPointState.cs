using System;
using System.Linq;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.Events;
using JetBrains.Annotations;
using static Mirror.SyncList<uint>;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Energy
{
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    [AddComponentMenu(BattleComponentMenus.Shared.Menu + "/" + nameof(EnergyCapsuleSpawnPointState))]
    public sealed class EnergyCapsuleSpawnPointState : NetworkBehaviour
    {
        [field: SerializeField] public UnityEvent<EnergyCapsule> Spawned { get; private set; }

        private readonly SyncList<uint> _spawnedCapsuleIds = new();
        private readonly ILogger _logger = LoggerFactory.CreateLogger<EnergyCapsuleSpawnPointState>();

        public IEnumerable<EnergyCapsule> SpawnedCapsules =>
            _spawnedCapsuleIds.Select(FindCapsule);

        [Client] public override void OnStartClient() { if (isClientOnly) Initialize(); }
        [Client] public override void OnStopClient() { if (isClientOnly) Deinitialize(); }

        [Server] public override void OnStartServer() => Initialize();
        [Server] public override void OnStopServer() => Deinitialize();

        private void Initialize()
        {
            _spawnedCapsuleIds.Callback += OnCallback;

            for (int i = 0; i < _spawnedCapsuleIds.Count; i++)
                OnCallback(Operation.OP_SET, i, default, _spawnedCapsuleIds[i]);
        }

        private void Deinitialize()
        {
            _spawnedCapsuleIds.Callback -= OnCallback;
            _spawnedCapsuleIds.Clear();
        }

        private void OnCallback(Operation op, int itemIndex, uint oldItem, uint newItem)
        {
            switch (op)
            {
                case Operation.OP_SET:
                case Operation.OP_ADD:
                case Operation.OP_INSERT:
                    EnergyCapsule capsule = FindCapsule(newItem);
                    _logger.Log($"Added. SceneObject is Found: {capsule != null}");
                    Spawned?.Invoke(capsule);
                    break;

                case Operation.OP_CLEAR:
                case Operation.OP_REMOVEAT:
                    _logger.Log("Removed");
                    break;

                default: throw new ArgumentOutOfRangeException(nameof(op), op, null);
            }
        }

        [Server]
        public void AddSpawnedCapsule([NotNull] IServerEnergyCapsule energyCapsule) =>
            _spawnedCapsuleIds.Add(energyCapsule.Identity.netId);

        [Server]
        public void RemoveSpawnedCapsule([NotNull] IServerEnergyCapsule energyCapsule) =>
            _spawnedCapsuleIds.Remove(energyCapsule.Identity.netId);

        [CanBeNull]
        private EnergyCapsule FindCapsule(uint id)
        {
            Dictionary<uint, NetworkIdentity> identities = isServer
                ? NetworkServer.spawned : NetworkClient.spawned;

            return identities.TryGetValue(id, out NetworkIdentity identity)
                ? identity.GetComponent<EnergyCapsule>() : null;
        }
    }
}