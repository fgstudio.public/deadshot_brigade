﻿namespace Frostgate.RiftHunters.Core.Battle
{
    public static class BattleAssetMenus
    {
            public const string Menu = AssetMenus.Menu + "/Battle";

            public static class Shared
            {
                public const string Menu = BattleAssetMenus.Menu + "/" + nameof(Shared);

                public static class Impacts { public const string Menu = Shared.Menu + "/" + nameof(Impacts); }
            }

            public static class Client
            {
                public const string Menu = BattleAssetMenus.Menu + "/" + nameof(Client);

                public static class Impacts { public const string Menu = Client.Menu + "/" + nameof(Impacts); }
            }

            public static class Server
            {
                public const string Menu = BattleAssetMenus.Menu + "/" + nameof(Server);

                public static class Impacts { public const string Menu = Server.Menu + "/" + nameof(Impacts); }
            }
    }
}