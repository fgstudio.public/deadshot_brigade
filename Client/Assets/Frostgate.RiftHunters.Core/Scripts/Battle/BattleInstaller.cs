using Frostgate.RiftHunters.Core.Battle.Client.PathPainter;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Client.Vibration;
using Frostgate.RiftHunters.Core.Battle.Server.Impact;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Core.Battle.Shared.Exp;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Energy;
using Frostgate.RiftHunters.Core.Battle.Shared.Healing;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Stamina;
using Frostgate.RiftHunters.Core.Battle.Shared.Looting;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.Reanimation;
using Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints;
using Frostgate.RiftHunters.Core.Battle.Shared.Invincibility;
using Frostgate.RiftHunters.Core.Battle.Shared.MissionScore;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;
using Frostgate.RiftHunters.Core.Battle.Shared.StreakBonus;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Network.Client.Observing;
using Frostgate.RiftHunters.Core.Network.Shared.KillStreak;

namespace Frostgate.RiftHunters.Core.Battle
{
    public sealed class BattleInstaller : MonoInstaller
    {
        [SerializeField, Required] private ServiceRoster _serviceRoster;
        [SerializeField, Required] private SimulationTime _simulationTime;
        [SerializeField, Required] private AimTargetFinder _aimTargetFinder;
        [SerializeField, Required] private PathPainter _pathPainter;
        [SerializeField, Required, AssetsOnly] private ReanimationSettings _reanimationSettings;
        [SerializeField, Required, AssetsOnly] private MissionScoresConfig _missionScoresConfig;
        [SerializeField, Required] private BotRespawnState _botRespawnState;
        [SerializeField, Required] private KillStreakState _killStreakState;
        [SerializeField, Required] private KillStreakConfig _killStreakConfig;

        public override void InstallBindings()
        {
            var vibrationConfigAccess = Container.Resolve<IConfigAccessor<BattleVibrationConfig>>();
            Container.BindInterfacesAndSelfTo<BattleVibrationConfig>()
                .FromInstance(vibrationConfigAccess.Config).AsSingle();
            Container.Bind<BattleVibrationMediator>().AsSingle();

            Container.Bind<IRandom>().To<UnityRandom>().AsSingle();
            Container.Bind<BulletProcessors>().AsSingle();
            Container.Bind<AreaRandom>().AsSingle();
            Container.BindInterfacesAndSelfTo<ActualRespawnPoint>().AsSingle();

            Container.Bind<UnitEffectStateFactory>().AsSingle();
            Container.Bind<UnitEffectModelFactory>().AsSingle();
            Container.Bind<SimulationTime>().FromInstance(_simulationTime).AsSingle();
            Container.Bind<IAimTargetFinder>().FromInstance(_aimTargetFinder).AsSingle();
            Container.Bind<PathPainter>().FromInstance(_pathPainter);
            Container.Bind<ReanimationSettings>().FromInstance(_reanimationSettings).AsSingle();
            Container.Bind<IMissionScoresConfig>().FromInstance(_missionScoresConfig).AsSingle();
            Container.BindInterfacesAndSelfTo<BotRespawnState>().FromInstance(_botRespawnState).AsSingle();
            Container.BindInterfacesAndSelfTo<KillStreakState>().FromInstance(_killStreakState).AsSingle();
            Container.Bind<KillStreakConfig>().FromInstance(_killStreakConfig).AsSingle();

            var activityObserverFactory = new ActivityObserverFactory(_pathPainter);
            Container.Bind<ActivityObserverFactory>().FromInstance(activityObserverFactory).AsSingle();

            BindServices();
            BindCommandRepository();

            BindObjectCollections<MineNetIdCollection, IMine>();
            BindObjectCollections<UnitNetIdCollection, UnitColliderCollection, UnitNetwork>();
            BindObjectCollections<TrapNetIdCollection, TrapColliderCollection, ITrap>();
            BindObjectCollections<TotemNetIdCollection, TotemColliderCollection, ITotem>();
            BindObjectCollections
                <ShootingTargetNetworkCollection, ShootingTargetColliderCollection, ShootingTargetNetwork>();

            Container.BindInterfacesAndSelfTo<AimTargetColliderCollection>().AsSingle();
            Container.BindInterfacesAndSelfTo<AimTargetNetIdCollection>().AsSingle();

            Container.Bind<PlayersDieObserver>().AsSingle();
        }

        private void BindServices()
        {
            // вынести это в отдельный Installer не вышло — проблемы с последовательностью биндингов
            Container.Bind<IServiceRoster>().FromInstance(_serviceRoster);
            Container.Bind<IBonusService>().FromInstance(_serviceRoster.BonusService);
            Container.Bind<IDamageService>().FromInstance(_serviceRoster.DamageService);
            Container.Bind<IHealingService>().FromInstance(_serviceRoster.HealingService);
            Container.Bind<IInvincibleService>().FromInstance(_serviceRoster.InvincibleService);
            Container.Bind<IReceiveExpService>().FromInstance(_serviceRoster.ReceiveExpService);
            Container.Bind<IRespawnUnitService>().FromInstance(_serviceRoster.RespawnUnitService);
            Container.Bind<IReanimationService>().FromInstance(_serviceRoster.ReanimationService);
            Container.Bind<ICaptureUnitService>().FromInstance(_serviceRoster.CaptureUnitService);
            Container.Bind<IStaminaService>().FromInstance(_serviceRoster.StaminaService);
            Container.Bind<ICollectLootBoxService>().FromInstance(_serviceRoster.CollectLootBoxService);
            Container.Bind<ICollectEnergyCapsuleService>().FromInstance(_serviceRoster.CollectEnergyCapsuleService);
        }

        private void BindCommandRepository()
        {
            var receiverRepository = new ObjectCommandReceiverRepository();
            var senderRepository = new ObjectCommandSenderRepository();

            Container.BindInterfacesAndSelfTo<ObjectCommandReceiverRepository>().FromInstance(receiverRepository);
            Container.BindInterfacesAndSelfTo<ObjectCommandSenderRepository>().FromInstance(senderRepository);

            BindCommands<ITrap>(receiverRepository, senderRepository);
            BindCommands<IMine>(receiverRepository, senderRepository);
            BindCommands<ITotem>(receiverRepository, senderRepository);
            BindCommands<IWaveSpawner>(receiverRepository, senderRepository);
            BindCommands<ILinkSpawner>(receiverRepository, senderRepository);
            BindCommands<IShield>(receiverRepository, senderRepository);
            BindCommands<BotSpawnerConfig>(receiverRepository, senderRepository);
        }

        private void BindCommands<TObject>(ObjectCommandReceiverRepository receiverRepository, ObjectCommandSenderRepository senderRepository)
            where TObject : class
        {
            var receiver = new ObjectCommandReceiver<TObject>();
            var sender = new ObjectCommandSender<TObject>(receiver);

            receiverRepository.Add(receiver);
            senderRepository.Add(sender);

            Container.BindInterfacesAndSelfTo<ObjectCommandReceiver<TObject>>().FromInstance(receiver);
            Container.BindInterfacesAndSelfTo<ObjectCommandSender<TObject>>().FromInstance(sender);
        }

        private void BindObjectCollections<TIdentityCollection, TColliderCollection, TValue>()
            where TIdentityCollection : IObjectCollection<uint, TValue>
            where TColliderCollection : IObjectCollection<Collider, TValue>
        {
            Container.BindInterfacesTo<TIdentityCollection>().AsSingle();
            Container.BindInterfacesTo<TColliderCollection>().AsSingle();

            var collections = new ObjectCollectionRepository<TValue>();
            collections.Add(Container.Resolve<IObjectCollection<uint, TValue>>());
            collections.Add(Container.Resolve<IObjectCollection<Collider, TValue>>());

            Container.BindInterfacesAndSelfTo<ObjectCollectionRepository<TValue>>().FromInstance(collections);
        }

        private void BindObjectCollections<TIdentityCollection, TValue>()
            where TIdentityCollection : IObjectCollection<uint, TValue>
        {
            Container.BindInterfacesTo<TIdentityCollection>().AsSingle();

            var collections = new ObjectCollectionRepository<TValue>();
            collections.Add(Container.Resolve<IObjectCollection<uint, TValue>>());

            Container.BindInterfacesAndSelfTo<ObjectCollectionRepository<TValue>>().FromInstance(collections);
        }
    }
}