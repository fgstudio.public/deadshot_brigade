﻿using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface IPreferences
    {
        [NotNull] BattleComponentRoster BattleComponentRoster { get; }
        bool IsDebugModeEnabled { get; }
    }
}
