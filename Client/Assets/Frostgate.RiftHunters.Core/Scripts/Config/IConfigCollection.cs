﻿using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core
{
    public interface IConfigCollection<TConfig> : IReadOnlyCollection<TConfig> where TConfig : IConfig
    {
        bool Contains(string id);
        bool TryGet(string id, out TConfig config);
        TConfig this[string id] { get; }
    }
}
