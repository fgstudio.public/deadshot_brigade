using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core
{
    public abstract class ScriptableConfigRepository<TConfig, TValue> : ScriptableObject
        where TConfig : ScriptableConfig
    {
        [Serializable]
        private sealed class KeyValue
        {
            [Required, AssetSelector] public TConfig Config;
            [Required, AssetSelector] public TValue Value;
        }

        [SerializeField] private KeyValue[] _keyValuePairs;

        public int Count => _keyValuePairs.Length;
        public IEnumerable<TConfig> Configs => _keyValuePairs.Select(kv => kv.Config);
        public IEnumerable<TValue> Values => _keyValuePairs.Select(kv => kv.Value);

        public TValue this[TConfig config] => this[config.Id];
        public TValue this[string configId] =>
            _keyValuePairs.Single(kv => kv.Config.Id == configId).Value;

        public bool ContainsConfig(TConfig config) => ContainsConfig(config.Id);
        public bool ContainsConfig(string configId) =>
            _keyValuePairs.Any(kv => kv.Config.Id == configId);

        public bool TryGetValue(TConfig config, out TValue value) =>
            TryGetValue(config.Id, out value);
        public bool TryGetValue(string configId, out TValue value)
        {
            KeyValue keyValue = _keyValuePairs.FirstOrDefault(kv => kv.Config.Id == configId);

            if (keyValue != null)
            {
                value = keyValue.Value;
                return true;
            }
            else
            {
                value = default;
                return false;
            }
        }
    }
}
