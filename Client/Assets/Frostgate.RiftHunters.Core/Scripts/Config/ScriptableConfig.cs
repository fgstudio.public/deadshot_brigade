﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public class ScriptableConfig : ScriptableObject, IConfig, ISerializationCallbackReceiver
    {
        [field: SerializeField, Required, InlineButton(nameof(GenerateId), "Generate")]
        public string Id { get; private set; }

        private void GenerateId()
        {
            Id = ConfigIdGenerator.GenerateId(GetType());
        }

        private void GenerateIdIfEmpty()
        {
            if (string.IsNullOrWhiteSpace(Id))
                GenerateId();
        }

        public void OnBeforeSerialize() => GenerateIdIfEmpty();

        public void OnAfterDeserialize()
        {
        }
    }
}