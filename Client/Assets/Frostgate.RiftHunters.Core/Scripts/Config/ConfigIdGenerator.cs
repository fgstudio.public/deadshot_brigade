﻿using System;

namespace Frostgate.RiftHunters.Core
{
    public static class ConfigIdGenerator
    {
        private const string DefaultConfigName = "Config";

        public static string GenerateId<T>() => GenerateId(typeof(T));

        public static string GenerateId(Type type)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(type);
            return CreateId(type.Name);
        }

        public static string GenerateId() => CreateId(DefaultConfigName);
        private static string CreateId(string configName) => $"{configName}_{Guid.NewGuid()}";
    }
}