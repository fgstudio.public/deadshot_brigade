﻿namespace Frostgate.RiftHunters.Core
{
    public interface ISavableParametersConfig
    {
        void SaveParameters();
    }
}