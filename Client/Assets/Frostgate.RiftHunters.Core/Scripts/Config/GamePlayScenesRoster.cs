﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    [CreateAssetMenu(fileName = nameof(GamePlayScenesRoster),
        menuName = AssetMenus.RiftHunters.Scene.Menu + "/" + nameof(GamePlayScenesRoster))]
    public sealed class GamePlayScenesRoster : ScriptableConfig
    {
        [SerializeField] private SceneLoadingData _bootScene;
        [SerializeField] private SceneLoadingData _mainScene;

        public ISceneLoadingData BootScene => _bootScene;
        public ISceneLoadingData MainScene => _mainScene;
    }
}