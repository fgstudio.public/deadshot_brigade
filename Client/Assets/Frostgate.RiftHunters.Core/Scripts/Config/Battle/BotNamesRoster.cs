﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle
{
    [CreateAssetMenu(fileName = nameof(BotNamesRoster),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(BotNamesRoster))]
    public sealed class BotNamesRoster : ScriptableConfig, INamesRoster
    {
        [SerializeField, Required] private string[] _predefinedNames = Array.Empty<string>();
        [SerializeField, Required] private string[] _firstNames = Array.Empty<string>();
        [SerializeField, Required] private string[] _secondNames = Array.Empty<string>();

        public ICollection<string> PredefinedNamesSet => _predefinedNames;
        public ICollection<string> FirstNamesSet => _firstNames;
        public ICollection<string> SecondNamesSet => _secondNames;
    }
}