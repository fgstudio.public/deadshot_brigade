using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle
{
    public abstract class EntityConfig : ScriptableObject, IConfig, ISavableParametersConfig
    {
        private const string GroupName = "Base params";

        [Indent, Title(GroupName)]
        [field: SerializeField] public string Id { get; private set; }
        [Title(GroupName)]
        [field: SerializeField] public string Name { get; private set; }
        [Title(GroupName)]
        [field: SerializeField] public Sprite Sprite { get; private set; }

        public virtual void SaveParameters()
        {
        }

        [ContextMenu(nameof(SetIdByName))]
        private void SetIdByName() => Id = name;
    }
}