using System;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle
{
    /// <summary>
    /// Настройки автоуменьшения здоровья.
    /// </summary>
    [Serializable]
    public class HpAutoLossConfig : IHpAutoLossConfig
    {
        [field: SerializeField, SuffixLabel("hp/sec", true)] public float Speed { get; private set; }
    }
}
