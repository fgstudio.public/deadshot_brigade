namespace Frostgate.RiftHunters.Core.Battle
{
    public enum UnitEffectTypes
    {
        Agro = 1,
        Stun = 2,
        MoveSpeedMultiplier = 10,
        AttackMultiplier = 11,
        AttackCooldownMultiplier = 12,
        EnergyAccumulationMultiplier = 13,
        DamageZone = 30,
        PiercingBullets = 31,
        DisableEnergyAccumulation = 32,
        IgnoreEnemyDamageZone = 33,
        HpAutoRecovery = 34,
        EnergyAutoRecovery = 35,
        StaminaAutoRecovery = 36,
        InfiniteAmmo = 37,
        FireDamage = 40,
        BulletArea = 41,
    }

    public enum UnitEffectTarget
    {
        Self = 1,
        Target = 5,
        EnemiesInRadius = 2,
        FriendsInRadius = 3,
        SelfAndFriendsInRadius = 4,
    }
}