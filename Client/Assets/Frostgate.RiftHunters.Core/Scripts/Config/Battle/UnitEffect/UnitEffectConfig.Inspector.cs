namespace Frostgate.RiftHunters.Core.Battle
{
    public partial class UnitEffectConfig
    {
        private bool ShowRadius => Target == UnitEffectTarget.FriendsInRadius ||
                                   Target == UnitEffectTarget.EnemiesInRadius ||
                                   Target == UnitEffectTarget.SelfAndFriendsInRadius;

        private bool ShowValue => Type == UnitEffectTypes.MoveSpeedMultiplier ||
                                  Type == UnitEffectTypes.AttackMultiplier ||
                                  Type == UnitEffectTypes.AttackCooldownMultiplier ||
                                  Type == UnitEffectTypes.EnergyAccumulationMultiplier ||
                                  Type == UnitEffectTypes.IgnoreEnemyDamageZone ||
                                  Type == UnitEffectTypes.FireDamage;

        private string ValueLabelName =>
            Type switch
            {
                UnitEffectTypes.FireDamage => "Damage",
                _ => "Value"
            };

        private string ValueSuffix =>
            Type switch
            {
                UnitEffectTypes.FireDamage => "max hp ratio / interval",
                _ => string.Empty
            };

        private bool ShowDamageZone => Type == UnitEffectTypes.DamageZone;

        private bool ShowAreaBulletEffects => Type == UnitEffectTypes.BulletArea;

        private bool ShowInterval => Type == UnitEffectTypes.FireDamage;

        private bool ShowHpAutoRecovery => Type == UnitEffectTypes.HpAutoRecovery;
        private bool ShowEnergyAutoRecovery => Type == UnitEffectTypes.EnergyAutoRecovery;
        private bool ShowStaminaAutoRecovery => Type == UnitEffectTypes.StaminaAutoRecovery;
    }
}