using System;
using Frostgate.RiftHunters.Core.Battle.Client;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle
{
    //TODO: это нужно для эффекта AreaBulletEffects. Возможно его стоит вообще выпилить и спаунить абилкой объект.
    [Serializable]
    public class BulletAreaData
    {
        [field: SerializeField] public Vector3 Size { get; private set; }
        [field: SerializeField] public float ForwardOffset { get; private set; }
    }

    [CreateAssetMenu(fileName = nameof(UnitEffectConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(UnitEffectConfig))]
    public partial class UnitEffectConfig : EntityConfig
    {
        [field: SerializeField] public UnitEffectTypes Type { get; private set; }
        [field: SerializeField] public bool IsBuff { get; private set; }

        // TODO: избавиться от этих свойств этим должен заведовать тот, кто накладывает бафф, а не сам бафф.
        [field: SerializeField] public UnitEffectTarget Target { get; private set; }

        [field: SuffixLabel("units", true)]
        [field: SerializeField, ShowIf(nameof(ShowRadius))] public float Radius { get; private set; }
        [field: SerializeField] public UnitViewVFX ViewVFX { get; private set; }

        [field: SuffixLabel("sec", true)]
        [field: SerializeField] public float Duration { get; private set; }

        [field: LabelText("@" + nameof(ValueLabelName)), SuffixLabel("@" + nameof(ValueSuffix), true)]
        [field: SerializeField, ShowIf(nameof(ShowValue))] public float Value { get; private set; }

        [field: SuffixLabel("sec", true)]
        [field: SerializeField, ShowIf(nameof(ShowInterval))] public float Interval { get; private set; }

        [field: SerializeField, ShowIf(nameof(ShowDamageZone))] public DamageZonesConfig DamageZones { get; private set; }
        [field: SerializeField, ShowIf(nameof(ShowHpAutoRecovery))] public HpAutoRecoveryConfig HpAutoRecoveryConfig { get; private set; }
        [field: SerializeField, ShowIf(nameof(ShowEnergyAutoRecovery))] public EnergyAutoRecoveryConfig EnergyAutoRecoveryConfig { get; private set; }
        [field: SerializeField, ShowIf(nameof(ShowStaminaAutoRecovery))] public StaminaAutoRecoveryConfig StaminaAutoRecoveryConfig { get; private set; }
        [field: SerializeField, ShowIf(nameof(ShowAreaBulletEffects))] public BulletAreaData BulletAreaData { get; private set; }
        [field: SerializeField, ShowIf(nameof(ShowAreaBulletEffects))] public UnitEffectConfig[] Effects { get; private set; }
    }
}