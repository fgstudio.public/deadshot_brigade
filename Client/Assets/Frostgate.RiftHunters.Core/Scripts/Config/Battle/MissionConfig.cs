﻿using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle
{
    public interface IMissionConfig : IConfig
    {
        string MissionName { get; }
        MissionType Type { get; }
        IReadOnlyList<ISceneLoadingData> Scenes { get; }
        int MaxPlayersNumber { get; }
        bool IsTutorial { get; }
        UnitRoster AllyBotsRoster { get; }
    }

    [CreateAssetMenu(fileName = nameof(MissionConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(MissionConfig))]
    public sealed class MissionConfig : ScriptableConfig, IMissionConfig
    {
        private const string ConfigurationGroupName = "Configuration";
        private const string GameplayGroupName = "Gameplay";
        private const string ScenesGroupName = "Scenes";

        [SerializeField, Required, TitleGroup(ConfigurationGroupName), Space(10)]
        private string _missionName;

        [SerializeField, TabGroup(GameplayGroupName)]
        private bool _isTutorial;

        [SerializeField, TabGroup(GameplayGroupName)]
        private MissionType _type = MissionType.Multiplayer;

        [SerializeField, MinValue(1), TabGroup(GameplayGroupName)]
        private int _maxPlayersNumber;

        [SerializeField, AssetSelector, TabGroup(GameplayGroupName)]
        private UnitRoster _allyBotsRoster;

        [SerializeField, TabGroup(ScenesGroupName), ListDrawerSettings(Expanded = true), HideLabel]
        private SceneLoadingData[] _scenes = Array.Empty<SceneLoadingData>();

        public string MissionName => _missionName;
        public MissionType Type => _type;
        public IReadOnlyList<ISceneLoadingData> Scenes => _scenes;
        public int MaxPlayersNumber => _maxPlayersNumber;
        public bool IsTutorial => _isTutorial;

        public UnitRoster AllyBotsRoster => _allyBotsRoster;
    }
}