using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle
{
    [CreateAssetMenu(fileName = nameof(UnitRandomConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(UnitRandomConfig))]
    public class UnitRandomConfig : ScriptableObject
    {
        [field: SerializeField] public int Max { get; private set; }

        [SerializeField] private List<int> _data;

        public int Get(int tick) => _data[tick % Max];

        [ContextMenu(nameof(Generate))]
        private void Generate()
        {
            _data.Clear();

            if (Max <= 1)
            {
                UnityEngine.Debug.LogError($"{nameof(Max)} = {Max}; increase this parameter");
                return;
            }

            // Создаем весь диапозон значений
            for (var i = 0; i < Max; i++)
                _data.Add(i);

            const int randomizeIterationsCount = 5;
            for (var i = 0; i < randomizeIterationsCount; i++)
                RandomizeData();
        }

        private void RandomizeData()
        {
            // Перемешиваем сгенереные значения в случайном порядке
            for (var i = 0; i < Max; i++)
            {
                var value = _data[i];
                _data.RemoveAt(i);

                // Коллекция сейчас на один элемент меньше,
                // поэтому берём её текущий Count в качестве верхнего диапозона возможной позиции
                var valueRandomPosition = Random.Range(0, _data.Count);
                _data.Insert(valueRandomPosition, value);
            }
        }
    }
}