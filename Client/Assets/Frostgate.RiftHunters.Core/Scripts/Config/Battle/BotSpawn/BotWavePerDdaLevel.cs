﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [HelpURL("https://www.notion.so/frostgate/59ba6d9bdc3c4504b5787e636ec174ec")]
    [Serializable]
    public sealed class BotWavePerDdaLevel : ICloneable<BotWavePerDdaLevel>
    {
        [field: SerializeField, Range(1, 20)] public int DdaLevel { get; private set; } = 1;
        public IReadOnlyList<UnitConfig> BotConfigs => _botConfigs ??= Array.Empty<UnitConfig>();

        [SerializeField, Required] private UnitConfig[] _botConfigs;

        private BotWavePerDdaLevel(int ddaLevel, IReadOnlyList<UnitConfig> botConfigs)
        {
            DdaLevel = ddaLevel;
            _botConfigs = new UnitConfig[botConfigs.Count];
            Enumerable.Range(0, botConfigs.Count).ForEach(
                i => _botConfigs[i] = botConfigs[i]);
        }

        public BotWavePerDdaLevel Clone() => new(DdaLevel, _botConfigs);
    }
}
