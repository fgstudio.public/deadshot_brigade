using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [CreateAssetMenu(
        fileName = nameof(BotArenaConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(BotArenaConfig))]
    public sealed class BotArenaConfig : ScriptableConfig
    {
        [Required, AssetsOnly]
        [SerializeField] private ActivityConfig[] _activityConfigs;

        [HideLabel, BoxGroup("Respawn Settings")]
        [SerializeField] private BotArenaRespawnData _respawnData;

        public BotArenaRespawnData RespawnData => _respawnData;
        [NotNull] public IReadOnlyList<ActivityConfig> ActivityConfigs => _activityConfigs;
    }
}