﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [Flags]
    public enum WaveEndCondition
    {
        KillAll = 1 << 0,
        WaitTime = 1 << 1
    }

    [Serializable]
    [HelpURL("https://www.notion.so/frostgate/59ba6d9bdc3c4504b5787e636ec174ec")]
    public sealed class BotWaveConfig : ICloneable<BotWaveConfig>
    {
        private const string EndSettings = "End Settings";

        [BoxGroup("Start Settings"), MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _delay;

        [BoxGroup("Repeat Settings"), MinValue(1)]
        [SerializeField] private int _repeatsCount;

        [BoxGroup(EndSettings)]
        [SerializeField] private WaveEndCondition _waveEndCondition = WaveEndCondition.KillAll;

        [BoxGroup(EndSettings), ShowIf(nameof(IsWaveEndWaiting)), SuffixLabel("sec", true)]
        [SerializeField, MinValue(0)] private int _waveEndTime;

        [SerializeField] private BotWavePerDdaLevel[] _ddaSetups;

        public TimeSpan Delay => TimeSpan.FromSeconds(_delay);
        public int RepeatsCount => _repeatsCount;
        public WaveEndCondition WaveEndCondition => _waveEndCondition;
        public TimeSpan WaveEndTime => IsWaveEndWaiting ? TimeSpan.FromSeconds(_waveEndTime) : TimeSpan.MaxValue;
        public IReadOnlyList<BotWavePerDdaLevel> DdaSetups => _ddaSetups;

        private bool IsWaveEndWaiting => _waveEndCondition.HasFlag(WaveEndCondition.WaitTime);

        private BotWaveConfig(float delay, int repeatsCount,
            WaveEndCondition waveEndCondition, int waveEndTime,
            IReadOnlyList<BotWavePerDdaLevel> botWavePerDdaLevels)
        {
            _delay = delay;
            _repeatsCount = repeatsCount;

            _waveEndCondition = waveEndCondition;
            _waveEndTime = waveEndTime;

            _ddaSetups = new BotWavePerDdaLevel[botWavePerDdaLevels.Count];
            Enumerable.Range(0, botWavePerDdaLevels.Count).ForEach(
                i => _ddaSetups[i] = botWavePerDdaLevels[i].Clone());
        }

        public BotWaveConfig Clone() =>
            new(_delay, _repeatsCount,
                _waveEndCondition, _waveEndTime, _ddaSetups);
    }
}
