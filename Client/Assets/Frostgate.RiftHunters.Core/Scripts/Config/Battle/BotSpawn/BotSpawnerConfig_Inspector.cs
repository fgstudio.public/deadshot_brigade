using System.Linq;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    public partial class BotSpawnerConfig
    {
        private int _currentWaveIndex;

        private void OnWaveDrawn(int index) =>
            _currentWaveIndex = index;

        [Button(ButtonSizes.Large), HorizontalGroup]
        private void CopyCurrentWave() =>
            _waves = _waves
                .Take(_currentWaveIndex)
                .Append(_waves[_currentWaveIndex])
                .Concat(_waves.Skip(_currentWaveIndex))
                .Select(d => d.Clone())
                .ToArray();

        [Button(ButtonSizes.Large), HorizontalGroup]
        private void MoveWaveForward()
        {
            if (_currentWaveIndex.IsContained(0, _waves.Length - 2))
                _waves.Swap(_currentWaveIndex, _currentWaveIndex + 1);
        }

        [Button(ButtonSizes.Large), HorizontalGroup]
        private void MoveWaveBack()
        {
            if (_currentWaveIndex.IsContained(1, _waves.Length - 1))
                _waves.Swap(_currentWaveIndex, _currentWaveIndex - 1);
        }
    }
}
