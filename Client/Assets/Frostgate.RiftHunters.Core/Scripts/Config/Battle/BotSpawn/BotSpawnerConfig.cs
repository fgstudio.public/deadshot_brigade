﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [HelpURL("https://www.notion.so/frostgate/59ba6d9bdc3c4504b5787e636ec174ec")]
    [CreateAssetMenu(fileName = nameof(BotSpawnerConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(BotSpawnerConfig))]
    public sealed partial class BotSpawnerConfig : ScriptableConfig
    {
        private const string ContinuationSettings = "Continuation Settings";
        private const string FrameLimitations = "Frame Limitations";
        private const string StartupSettings = "Startup Settings";
        private const string FinalizationSettings = "Finalization Settings";

        [BoxGroup(ContinuationSettings)]
        [SerializeField] private bool _isEndless;


        [BoxGroup(FrameLimitations)]
        [SerializeField] private bool _unlimitedUnits = true;

        [BoxGroup(FrameLimitations)]
        [MinValue(0), HideIf(nameof(_unlimitedUnits))]
        [SerializeField] private int _maxUnitsInFrame;


        [BoxGroup(StartupSettings)]
        [MinValue(0), SuffixLabel("sec", true)]
        [SerializeField] private float _delay;


        [BoxGroup(FinalizationSettings)]
        [SerializeField] private bool _autoUnspawnBots;

        [ListDrawerSettings(NumberOfItemsPerPage = 1, OnBeginListElementGUI = nameof(OnWaveDrawn))]
        [SerializeField] private BotWaveConfig[] _waves;

        public bool IsEndless => _isEndless;
        public bool AutoUnspawnBots => _autoUnspawnBots;
        public TimeSpan StartupDelay => TimeSpan.FromSeconds(_delay);
        public int MaxUnitsAtTheMoment => _unlimitedUnits ? int.MaxValue : _maxUnitsInFrame;
        public IList<BotWaveConfig> Waves => _waves ??= Array.Empty<BotWaveConfig>();
    }
}