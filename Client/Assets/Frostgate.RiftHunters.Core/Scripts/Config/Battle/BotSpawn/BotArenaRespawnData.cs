using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn
{
    [Flags]
    public enum RespawnType
    {
        None = 0,
        ByTimer = 1 << 0,
        WhenAllPlayersDie = 1 << 1
    }

    [Serializable]
    public sealed class BotArenaRespawnData
    {
        [SerializeField] private RespawnType _respawnTypeFlags;

        [SuffixLabel("sec", true), MinValue(0), ShowIf(nameof(IsTimerUsed))]
        [SerializeField] private float _respawnTimerInterval;

        [SuffixLabel("sec", true), MinValue(0)]
        [SerializeField] private float _invincibleDuration;

        public RespawnType RespawnTypeFlags => _respawnTypeFlags;
        public TimeSpan InvincibleDuration => TimeSpan.FromSeconds(_invincibleDuration);
        public TimeSpan RespawnTimerInterval => IsTimerUsed
            ? TimeSpan.FromSeconds(_respawnTimerInterval)
            : TimeSpan.Zero;

        private bool IsTimerUsed => _respawnTypeFlags.HasFlag(RespawnType.ByTimer);
    }
}
