﻿namespace Frostgate.RiftHunters.Core.Battle
{
    public enum MissionType
    {
        Singleplayer = 1,
        Multiplayer = 2
    }
}