using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Impact;

namespace Frostgate.RiftHunters.Core.Battle
{
    [Serializable]
    public abstract class UnitImpactData
    {
        public enum DelayType { None, Value, UntilActionEnd }

        [CanBeNull, AssetsOnly, AssetSelector]
        [SerializeField] private ImpactConfig _config;

        [LabelText(nameof(Delay)), HorizontalGroup, ShowIf(nameof(UseDelayVariant))]
        [SerializeField] private DelayType _delayVariant;

        [MinValue(0), SuffixLabel("sec", true), HideLabel, LabelWidth(20), HorizontalGroup, ShowIf(nameof(UseDelay))]
        [SerializeField] private float _delay;

        [MinValue(0), SuffixLabel("sec", true), ShowIf(nameof(UseCooldown))]
        [SerializeField] private float _cooldown;

        [CanBeNull] public ImpactConfig Config => _config;
        public DelayType DelayVariant => HasDelay ? _delayVariant : DelayType.None;
        public TimeSpan Delay => TimeSpan.FromSeconds(UseDelay ? _delay : default);
        public TimeSpan Cooldown => TimeSpan.FromSeconds(UseCooldown ? _cooldown : default);

        protected abstract bool HasDelay { get; }
        protected abstract bool HasCooldown { get; }

        private bool UseCooldown => HasCooldown && _config != null;
        private bool UseDelayVariant => HasDelay && _config != null;
        private bool UseDelay => _delayVariant == DelayType.Value && UseDelayVariant;
    }
}
