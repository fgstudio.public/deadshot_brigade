using System;

namespace Frostgate.RiftHunters.Core.Battle
{
    [Serializable]
    public sealed class UnitDeathImpactData : UnitImpactData
    {
        protected override bool HasDelay => false;
        protected override bool HasCooldown => false;
    }
}
