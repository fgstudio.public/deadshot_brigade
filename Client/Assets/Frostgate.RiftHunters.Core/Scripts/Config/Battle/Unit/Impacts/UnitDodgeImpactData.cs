using System;

namespace Frostgate.RiftHunters.Core.Battle
{
    [Serializable]
    public sealed class UnitDodgeImpactData : UnitImpactData
    {
        protected override bool HasDelay => true;
        protected override bool HasCooldown => true;
    }
}
