using System;

namespace Frostgate.RiftHunters.Core.Battle
{
    [Serializable]
    public sealed class UnitAbilityImpactData : UnitImpactData
    {
        protected override bool HasDelay => false;
        protected override bool HasCooldown => true;
    }
}
