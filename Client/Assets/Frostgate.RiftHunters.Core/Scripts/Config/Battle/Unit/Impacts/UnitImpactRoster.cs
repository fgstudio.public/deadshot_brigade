using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle
{
    [Serializable]
    public sealed class UnitImpactRoster
    {
        [SerializeField, HideLabel, BoxGroup(nameof(Death))] private UnitDeathImpactData _death;
        [SerializeField, HideLabel, BoxGroup(nameof(Ultimate))] private UnitUltimateImpactData _ultimate;
        [SerializeField, HideLabel, BoxGroup(nameof(Ability))] private UnitAbilityImpactData _ability;
        [SerializeField, HideLabel, BoxGroup(nameof(ExtraAbility))] private UnitExtraAbilityImpactData _extraAbility;
        [SerializeField, HideLabel, BoxGroup(nameof(Special))] private UnitSpecialImpactData _special;

        public UnitImpactData Death => _death;
        public UnitImpactData Ability => _ability;
        public UnitImpactData Ultimate => _ultimate;
        public UnitImpactData ExtraAbility => _extraAbility;
        public UnitImpactData Special => _special;

        [NotNull, ItemNotNull] public IEnumerable<UnitImpactData> AllImpacts
        {
            get
            {
                if (Death.Config != null) yield return Death;
                if (Ability.Config != null) yield return Ability;
                if (Ultimate.Config != null) yield return Ultimate;
                if (ExtraAbility.Config != null) yield return ExtraAbility;
                if (Special.Config != null) yield return Special;
            }
        }
    }
}
