using System;

namespace Frostgate.RiftHunters.Core.Battle
{
    [Serializable]
    public sealed class UnitReloadImpactData : UnitImpactData
    {
        protected override bool HasDelay => true;
        protected override bool HasCooldown => true;
    }
}
