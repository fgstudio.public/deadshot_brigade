using System;

namespace Frostgate.RiftHunters.Core.Battle
{
    [Serializable]
    public sealed class UnitSpecialImpactData : UnitImpactData
    {
        protected override bool HasDelay => false;
        protected override bool HasCooldown => false;
    }
}
