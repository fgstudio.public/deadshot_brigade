using System;

namespace Frostgate.RiftHunters.Core.Battle
{
    [Serializable]
    public sealed class UnitExtraAbilityImpactData : UnitImpactData
    {
        protected override bool HasDelay => false;
        protected override bool HasCooldown => true;
    }
}
