using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle
{
    [Serializable]
    // TODO: нужно ввести ID для индетификации зон. Сейчас они идентифицируются по Multiplier'у, что вызывает коллизии
    public sealed class DamageZone
    {
        public bool IsShield => Multiplier == 0;

        [field: SerializeField] public Sprite Sprite { get; private set; }
        [field: SerializeField] public Color Color { get; private set; }
        [field: SerializeField] public float Multiplier { get; private set; }
        [field: SerializeField] public float OffsetAngle { get; private set; }
        [field: SerializeField] public float HalfSizeAngle { get; private set; }
        [field: SerializeField] public bool IsBack { get; private set; }
    }
}