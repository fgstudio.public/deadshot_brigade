using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle
{
    [CreateAssetMenu(fileName = nameof(UnitPhaseConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(UnitPhaseConfig))]
    public sealed class UnitPhaseConfig : ScriptableObject
    {
        [Serializable] public sealed class Conditions
        {
            [field: SerializeField, Range(0, 1)] public float CurrentHealthRatio { get; private set; }
        }

        [Serializable] public sealed class Actions
        {
            [field: SerializeField] public bool InitEnergy { get; private set; }
            [field: SerializeField, Range(0, 1), ShowIf(nameof(InitEnergy))] public float InitialEnergyRatio { get; private set; }
        }

        [field: SerializeField] public Conditions ApplyConditions { get; private set; }
        [field: SerializeField] public Actions ApplyActions { get; private set; }
        [field: SerializeField] public UnitConfig UnitConfig { get; private set; }
    }
}