using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle
{
    [CreateAssetMenu(fileName = nameof(DamageZonesConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(DamageZonesConfig))]
    public class DamageZonesConfig : ScriptableObject
    {
        [Tooltip("Для дефолтной зоны, углы не имеют значения")]
        [SerializeField, Required] private DamageZone _defaultZone;

        [SerializeField, Required] private DamageZone[] _zones;

        public DamageZone DefaultZone => _defaultZone;
        public IEnumerable<DamageZone> AllZones => _zones;

        public DamageZone GetDamageZoneByMultiplier(float zoneMultiplier)
        {
            const float tolerance = 0.1f;
            return _zones.FirstOrDefault(x => Math.Abs(x.Multiplier - zoneMultiplier) < tolerance) ?? _defaultZone;
        }

        public DamageZone GetDamageZone(Vector3 vectorToHitPoint, Vector3 unitForward)
        {
            vectorToHitPoint.y = 0;
            var localAngle = Vector3.SignedAngle(unitForward, vectorToHitPoint, Vector3.up);
            return GetDamageZoneByAngle(localAngle);
        }

        public int GetDamageZoneIndex(Vector3 incomingAttackVector, Vector3 unitForward)
        {
            incomingAttackVector.y = 0;
            var localAngle = Vector3.SignedAngle(unitForward, -incomingAttackVector, Vector3.up);
            return GetDamageZoneIndexByAngle(localAngle);
        }

        private DamageZone GetDamageZoneByAngle(float localAngle)
        {
            foreach (var damageZone in _zones)
                if (CheckLocalAngle(localAngle, damageZone))
                    return damageZone;

            return _defaultZone;
        }

        private int GetDamageZoneIndexByAngle(float localAngle)
        {
            for (var i = 0; i < _zones.Length; i++)
            {
                var damageZone = _zones[i];
                if (CheckLocalAngle(localAngle, damageZone))
                    return i;
            }

            return -1;
        }

        private static bool CheckLocalAngle(float localAngle, DamageZone damageZone) =>
            Mathf.Abs(Mathf.DeltaAngle(localAngle, damageZone.OffsetAngle)) <= damageZone.HalfSizeAngle;
    }
}