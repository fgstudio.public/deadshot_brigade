using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Core.Battle.Client;
using Frostgate.RiftHunters.Core.Battle.Client.Unit;
using Frostgate.RiftHunters.Core.Battle.Server.UnitAI;
using Frostgate.RiftHunters.Core.Battle.Server.Objects.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle
{
    [CreateAssetMenu(fileName = nameof(UnitConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(UnitConfig))]
    public sealed class UnitConfig : ScriptableObject, IConfig
    {
        private const string MetaData = "Meta Data";
        private const string RoleData = MetaData + "/Role Data";

        private const string CoreData = "Core Data";
        private const string DefaultData = "Default Data (can be overriden)";
        private const string BotData = "Bot Data";

        private const string UpgradeableData = "Upgradeable Data";
        private const string AutoRecoveryData = UpgradeableData + "/Auto Recovery";

        [field: SerializeField] public string Id { get; private set; }

        // TODO: в конфиг Hero
        [field: SerializeField, BoxGroup(MetaData)] public string Name { get; private set; }
        [field: SerializeField, BoxGroup(MetaData)] public Sprite Sprite { get; private set; }

        // TODO: в конфиг Hero
        [field: SerializeField, BoxGroup(RoleData)] public HeroClass Class { get; private set; }
        // TODO: вместо этих использовать ClassIconsConfig
        [field: SerializeField, BoxGroup(RoleData)] public Sprite RoleIcon { get; private set; }
        [field: SerializeField, BoxGroup(RoleData)] public Sprite GoldRoleIcon { get; private set; }

        [field: SerializeField, BoxGroup(CoreData)] public bool IsBoss { get; private set; }
        [field: SerializeField, Required, BoxGroup(CoreData)] public UnitView View { get; private set; }

        [field: SerializeField, Required, BoxGroup(CoreData)] public UnitViewConfig ViewConfig { get; private set; }
        [field: SerializeField, BoxGroup(CoreData)] public UnitClientModule ClientModule { get; private set; }
        [field: SerializeField, BoxGroup(CoreData)] public UnitServerModule ServerModule { get; private set; }
        [field: SerializeField, BoxGroup(CoreData)] public UnitAIConfig UnitAIConfig { get; private set; }
        [field: SerializeField, BoxGroup(CoreData)] public UnitPhaseConfig UnitPhaseConfig { get; private set; }
        [field: SerializeField, BoxGroup(CoreData), Min(1)] public int Phase { get; private set; }
        [field: SerializeField, BoxGroup(CoreData)] public UnitActionRoster UnitActionRoster { get; private set; }
        [field: SerializeField, Required, BoxGroup(CoreData)] public HeroUpgradeTable UpgradeTable { get; private set; }
        [field: SerializeField, MinValue(1), BoxGroup(CoreData)] public float BodyMass { get; private set; } = 80;

        [SerializeField, BoxGroup(CoreData)] private DamageMultiplier[] _damageMultipliers = Array.Empty<DamageMultiplier>();


        [field: FoldoutGroup(BotData + "/Death Rewards"), HideLabel]
        [field: SerializeField] public DeathRewards DeathRewards { get; private set; }

        // TODO: эти данные должны браться из сохранений, но из-за ботов должны быть и тут
        [field: SerializeField, BoxGroup(BotData)] public RangeWeaponConfig DefaultWeapon { get; private set; }


        [field: BoxGroup(UpgradeableData)]
        [field: SerializeField] public MeleeWeaponConfig DefaultMeleeWeapon { get; private set; }

        [field: BoxGroup(UpgradeableData)]
        [field: SerializeField, Min(0)] public int GearScore { get; private set; }

        [field: SuffixLabel(UnitPropertySuffixRoster.HealthLabel, true), BoxGroup(UpgradeableData)]
        [field: SerializeField] public float Health { get; private set; }

        [field: SuffixLabel(UnitPropertySuffixRoster.StaminaLabel, true), BoxGroup(UpgradeableData)]
        [field: SerializeField] public float Stamina { get; private set; }

        [field: SuffixLabel(UnitPropertySuffixRoster.EnergyLabel, true), BoxGroup(UpgradeableData)]
        [field: SerializeField, Range(0, 100), BoxGroup(UpgradeableData)] public float Energy { get; private set; }

        [field: SuffixLabel(UnitPropertySuffixRoster.MoveSpeedLabel, true), BoxGroup(UpgradeableData)]
        [field: SerializeField] public float Speed { get; private set; } = 8;

        [field: SuffixLabel(UnitPropertySuffixRoster.TurnSpeedLabel, true), BoxGroup(UpgradeableData)]
        [field: SerializeField] public float TurnSpeed { get; private set; } = float.PositiveInfinity;


        [field: FoldoutGroup(AutoRecoveryData), HideLabel, BoxGroup(AutoRecoveryData + "/Hp Auto Recovery")]
        [field: SerializeField] public HpAutoRecoveryConfig HpAutoRecovery { get; private set; }

        [field: FoldoutGroup(AutoRecoveryData), HideLabel, BoxGroup(AutoRecoveryData + "/Energy Auto Recovery")]
        [field: SerializeField] public EnergyAutoRecoveryConfig EnergyAutoRecovery { get; private set; }

        [field: FoldoutGroup(AutoRecoveryData), HideLabel, BoxGroup(AutoRecoveryData + "/Stamina AutoRecovery")]
        [field: SerializeField] public StaminaAutoRecoveryConfig StaminaAutoRecovery { get; private set; }


        [field: HideLabel, FoldoutGroup(UpgradeableData + "/Impacts")]
        [field: SerializeField] public UnitImpactRoster Impacts { get; private set; }


        private Dictionary<DamageType, float> _damageMultipliersCache;

        public float GetDamageMultiplier(DamageType type)
        {
            if (_damageMultipliersCache == null)
            {
                _damageMultipliersCache = new Dictionary<DamageType, float>();
                foreach (var damageMultiplier in _damageMultipliers)
                    _damageMultipliersCache.Add(damageMultiplier.Type, damageMultiplier.Multiplier);
            }

            return _damageMultipliersCache.TryGetValue(type, out float multiplier) ? multiplier : 1;
        }
    }
}