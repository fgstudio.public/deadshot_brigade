using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle
{
    [Serializable]
    public class DamageMultiplier
    {
        public DamageType Type => _type;
        public float Multiplier => _multiplier;

        [SerializeField] private DamageType _type = DamageType.Fire;
        [SerializeField] private float _multiplier = 0.1f;
    }
}