using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle
{
    [HelpURL("https://www.notion.so/frostgate/e23a099eb137443b8a16bd7b023dc6d8")]
    [CreateAssetMenu(fileName = nameof(ReanimationSettings),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(ReanimationSettings))]
    public sealed class ReanimationSettings : ScriptableObject
    {
        [field: SerializeField, Min(0), SuffixLabel("units", true)] public float AreaRadius { get; private set; }
        [field: SerializeField, Min(0), SuffixLabel("ratio/unit", true)] public float IncreaseProgressSpeedPerUnit { get; private set; }
        [field: SerializeField, Min(0), SuffixLabel("ratio", true)] public float DecreaseProgressSpeed { get; private set; }
        [field: SerializeField, Range(0, 1), SuffixLabel("ratio")] public float ReanimatedHealthRatio { get; private set; }
    }
}