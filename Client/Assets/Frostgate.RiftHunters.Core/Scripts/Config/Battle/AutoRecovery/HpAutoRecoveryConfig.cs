using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle
{
    /// <summary>
    /// Настройки автовосстановления здоровья персонажа.
    /// <see cref="https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#e417470319264003b313af6970c072ce"/>
    /// </summary>
    [Serializable]
    public sealed class HpAutoRecoveryConfig : IHpAutoRecoveryConfig
    {
        [field: SuffixLabel(UnitPropertySuffixRoster.HealthRecoverySpeedLabel, true)]
        [field: SerializeField] public float PercentPerSecond { get; private set; }

        [field: MinValue(0), SuffixLabel("sec", true)]
        [field: SerializeField] public float DamageCooldown { get; private set; }

        [UsedImplicitly] public HpAutoRecoveryConfig() { }

        public HpAutoRecoveryConfig(float percentPerSecond, float damageCooldown)
        {
            PercentPerSecond = percentPerSecond;
            DamageCooldown = damageCooldown;
        }
    }
}