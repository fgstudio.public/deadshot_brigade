using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle
{
    [Serializable]
    public sealed class StaminaAutoRecoveryConfig : IStaminaAutoRecoveryConfig
    {
        [field: SuffixLabel(UnitPropertySuffixRoster.StaminaRecoverySpeedLabel, true)]
        [field: SerializeField] public float Speed { get; private set; }

        [UsedImplicitly] public StaminaAutoRecoveryConfig() { }

        public StaminaAutoRecoveryConfig(float speed)
        {
            Speed = speed;
        }
    }
}