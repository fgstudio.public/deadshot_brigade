using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems;

namespace Frostgate.RiftHunters.Core.Battle
{
    /// <summary>
    /// Настройки автовосстановления энергии персонажа.
    /// </summary>
    [Serializable]
    public sealed class EnergyAutoRecoveryConfig : IEnergyAutoRecoveryConfig
    {
        [field: SuffixLabel(UnitPropertySuffixRoster.EnergyRecoverySpeedLabel, true)]
        [field: SerializeField] public float Speed { get; private set; }

        [UsedImplicitly] public EnergyAutoRecoveryConfig() { }

        public EnergyAutoRecoveryConfig(float speed)
        {
            Speed = speed;
        }
    }
}