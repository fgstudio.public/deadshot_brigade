using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    [CreateAssetMenu(
        fileName = nameof(WaveSpawnerConfig),
        menuName = BattleAssetMenus.Shared.Impacts.Menu + "/" + nameof(WaveSpawnerConfig))]
    public sealed class WaveSpawnerConfig : ScriptableObject
    {
        [SerializeField, MinValue(0), SuffixLabel("sec", true)] private float _cooldown;
        [SerializeField, MinValue(0)] private int _count;
        [SerializeField, Required, HideLabel, BoxGroup(nameof(Wave))] private WaveConfig _wave;

        public TimeSpan Cooldown => TimeSpan.FromSeconds(_cooldown);
        public int Count => _count;
        public WaveConfig Wave => _wave;

        public TimeSpan Duration => Wave.Duration * Count + Cooldown * Mathf.Max(0, Count - 1);
    }
}