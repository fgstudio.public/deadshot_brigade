using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    [Serializable]
    public sealed class WaveConfig
    {
        [SerializeField, MinValue(0), SuffixLabel("units", true)] private float _radius;
        [SerializeField, MinValue(0), SuffixLabel("units/sec", true)] private float _speed;
        [SerializeField, MinValue(0), SuffixLabel("hp", true)] private float _damage;
        [SerializeField, Required, AssetSelector] private UnitActionDamageTrigger[] _damageUnitActions;
        [SerializeField, CanBeNull, AssetSelector] private WaveSpawnerConfig _distributingSpawnerConfig;

        public float Radius => _radius;
        public float Speed => _speed;
        public float Damage => _damage;
        [CanBeNull] public WaveSpawnerConfig DistributingSpawnerConfig => _distributingSpawnerConfig;
        [NotNull, ItemNotNull] public IReadOnlyList<UnitActionDamageTrigger> DamageUnitActions => _damageUnitActions;

        public TimeSpan Duration => Speed > 0 ? TimeSpan.FromSeconds(Radius / Speed) : TimeSpan.Zero;
    }
}