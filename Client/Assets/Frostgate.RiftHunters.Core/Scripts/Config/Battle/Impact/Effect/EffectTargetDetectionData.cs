using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    [Serializable]
    public sealed partial class EffectTargetDetectionData
    {
        public enum DetectionType
        {
            AutoAimInRadius, // поиск целей в радиусе
            AutoAimInSector, // поиск целей в секторе окружности
            AutoAimInPoint, // поиск целей в указанной точке, относительно исполнителя
            AutoAimInRect // поиск целей в прямоугольной области
        }

        [Tooltip("Где определять цели:\n" +
                 nameof(DetectionType.AutoAimInRadius) + " — в радиусе относительно исполнителя,\n" +
                 nameof(DetectionType.AutoAimInSector) + " — в указанном секторе относительно исполнителя,\n" +
                 nameof(DetectionType.AutoAimInPoint) + " — в позиции относительно исполнителя,\n" +
                 nameof(DetectionType.AutoAimInRect) + " — в прямоугольной области относительно исполнителя.")]
        [SerializeField] private DetectionType _type;

        [MinValue(0), ShowIf(nameof(IsUsedValue1)), SuffixLabel("@" + nameof(SuffixValue1), true)]
        [LabelText("@" + nameof(LabelValue1)), Tooltip("@" + nameof(TooltipValue1))]
        [SerializeField] private float _value1;

        [MinValue(0), ShowIf(nameof(IsUsedValue2)), SuffixLabel("@" + nameof(SuffixValue2), true)]
        [LabelText("@" + nameof(LabelValue2)), Tooltip("@" + nameof(TooltipValue2))]
        [SerializeField] private float _value2;

        [Range(-360, 360), ShowIf(nameof(IsRotationUsed)), SuffixLabel("deg", true)]
        [Tooltip("Угол поворота области относительно своей оси")]
        [SerializeField] private float _rotation;

        [SuffixLabel("units"), Tooltip("Смещение области относительно исполнителя")]
        [SerializeField] private Vector3 _offset;

        [MinValue(0), ShowIf(nameof(IsMaxTargetsUsed))]
        [Tooltip("Максимальное кол-во определённых целей")]
        [SerializeField] private int _maxTargets = int.MaxValue;

        public DetectionType Type => _type;
        public Vector3 Offset => _offset;
        public float Rotation => IsRotationUsed ? _rotation : default;
        public float AngleWidth => IsAngleWidthUsed ? _value2 : default;
        public float Radius => IsRadiusUsed ? _value1 : default;
        public Vector2 Size => IsSizeUsed ? new Vector2(_value1, _value2) : default;
        public int MaxTargets => IsMaxTargetsUsed ? _maxTargets : int.MaxValue;

        private bool IsRadiusUsed => _type == DetectionType.AutoAimInRadius ||
                                     _type == DetectionType.AutoAimInSector;
        private bool IsAngleWidthUsed => _type == DetectionType.AutoAimInSector;
        private bool IsSizeUsed => _type == DetectionType.AutoAimInRect;
        private bool IsRotationUsed => _type == DetectionType.AutoAimInRect ||
                                       _type == DetectionType.AutoAimInSector;
        private bool IsMaxTargetsUsed => _type == DetectionType.AutoAimInRadius ||
                                         _type == DetectionType.AutoAimInSector;
    }
}
