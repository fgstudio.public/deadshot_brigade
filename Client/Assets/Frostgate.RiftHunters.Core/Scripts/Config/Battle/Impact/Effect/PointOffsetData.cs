using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    [Serializable]
    public sealed class PointOffsetData
    {
        public enum OffsetType
        {
            None, // не изменять позицию по умолчанию,
            RandomInRadius, // случайная позиция в заданном радиусе,
            RandomOnRadius, // случайная позиция на окружности заданного радиуса,
            EvenlyOnRadius  // равномерное распределение позиций на окружности заданного радиуса
        }

        [HorizontalGroup(MinWidth = 180), LabelWidth(50)]
        [Tooltip("Какую позицию задать:\n" +
                 nameof(OffsetType.None) + " — использовать позицию префаба по умолчанию,\n" +
                 nameof(OffsetType.RandomInRadius) + " — использовать случайную позицию в окржуности заданного радиуса,\n" +
                 nameof(OffsetType.RandomOnRadius) + " — использовать случайную позицию на окружности заданного радиуса,\n" +
                 nameof(OffsetType.EvenlyOnRadius) + " — использовать равномерное распределение позиций на окружности заданного радиуса.")]
        [SerializeField] private OffsetType _type;

        [MinValue(0), ShowIf(nameof(IsRadiusUsed)), SuffixLabel("units", true), HideLabel, HorizontalGroup]
        [SerializeField] private float _radius;

        public OffsetType Type => _type;
        public float Radius => IsRadiusUsed ? _radius : default;

        private bool IsRadiusUsed => _type == OffsetType.RandomInRadius || _type == OffsetType.RandomOnRadius
                                                                        || _type == OffsetType.EvenlyOnRadius;
    }
}
