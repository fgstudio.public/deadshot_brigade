using System;
using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    [Serializable]
    public sealed class EffectTargetOrderData
    {
        [Tooltip("Приоритеты целей в порядке уменьшения приоритета:\n" +
                 nameof(ImpactTargetType.None) + " — неопределено,\n" +
                 nameof(ImpactTargetType.Unit) + " — моб/игрок,\n" +
                 nameof(ImpactTargetType.Barrel) + " — бочка,\n" +
                 nameof(ImpactTargetType.Trap) + " — клетка,\n" +
                 nameof(ImpactTargetType.Totem) + " — тотем.\n" +
                 "Сущности, для которых приоритет не указан, " +
                 "имеют по умолчанию равный и наименьший приоритет.")]
        [SerializeField] private ImpactTargetType[] _order;

        public IReadOnlyList<ImpactTargetType> Order => _order;

        public int CalcOrderIndex([NotNull] Object target) =>
            _order.IndexOf(o => DetectType(target) == o);

        public bool IsMatching([NotNull] Object target) =>
            _order.Contains(DetectType(target));

        private ImpactTargetType DetectType([NotNull] Object target) =>
            target switch
            {
                Trap => ImpactTargetType.Trap,
                Totem => ImpactTargetType.Totem,
                UnitNetwork => ImpactTargetType.Unit,
                BarrelNetwork => ImpactTargetType.Barrel,
                _ => ImpactTargetType.None
            };
    }
}
