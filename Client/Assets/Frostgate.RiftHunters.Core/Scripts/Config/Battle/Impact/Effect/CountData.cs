using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    [Serializable]
    public sealed class CountData
    {
        private const string CountGroup = "Count";
        private const string LimitGroup = "Limit";

        public enum CountType
        {
            Exact, // точное количество
            ByPlayers // по количеству активных игроков
        }

        [field: HorizontalGroup(CountGroup), LabelWidth(50),
                Tooltip("Способ задания количества:\n" +
                        nameof(CountType.Exact) + " — точное число,\n" +
                        nameof(CountType.ByPlayers) + " — по числу активных игроков.")]
        [field: SerializeField] public CountType Type { get; private set; }

        [field: ShowIf(nameof(IsExactMode)), HorizontalGroup(CountGroup), HideLabel, MinValue(1)]
        [field: SerializeField]public int ExactCount { get; private set; }

        [field: HideIf(nameof(IsExactMode)), HorizontalGroup(LimitGroup), LabelWidth(70)]
        [field: SerializeField] public bool UseLimits { get; private set; }

        [field: ShowIf(nameof(IsLimitsMode)), HorizontalGroup(LimitGroup), HideLabel, MinMaxSlider(0, 100)]
        [field: SerializeField] public Vector2Int Limits { get; private set; }

        private bool IsExactMode => Type == CountType.Exact;
        private bool IsLimitsMode => Type == CountType.ByPlayers && UseLimits;
    }

}
