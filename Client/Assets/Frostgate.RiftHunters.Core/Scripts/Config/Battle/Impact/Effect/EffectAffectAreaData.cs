using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    // TODO: как-нибудь совместить с EffectTargetDetectionData
    [Serializable]
    public sealed partial class EffectAffectAreaData
    {
        public enum AreaType
        {
            Point, // в заданной точке
            Sector, // в заданном секторе
            Radius, // в заданном радиусе
            Rect // в заданной прямоугольной области
        }

        [Tooltip("Размер области воздействия:\n" +
                 nameof(AreaType.Point) + " — точечная,\n" +
                 nameof(AreaType.Sector) + " — сектор с заданным радиусом и шириной,\n" +
                 nameof(AreaType.Radius) + " — круг с заданным радиусом,\n" +
                 nameof(AreaType.Rect) + " — заданная прямоугольная область.")]
        [SerializeField] private AreaType _type;

        [MinValue(0), ShowIf(nameof(IsUsedValue1)), SuffixLabel("@" + nameof(SuffixValue1), true)]
        [LabelText("@" + nameof(LabelValue1)), Tooltip("@" + nameof(TooltipValue1))]
        [SerializeField] private float _value1;

        [MinValue(0), ShowIf(nameof(IsUsedValue2)), SuffixLabel("@" + nameof(SuffixValue2), true)]
        [LabelText("@" + nameof(LabelValue2)), Tooltip("@" + nameof(TooltipValue2))]
        [SerializeField] private float _value2;

        [Range(-360, 360), ShowIf(nameof(IsRotationUsed)), SuffixLabel("deg", true)]
        [Tooltip("Угол поворота области относительно своей оси")]
        [SerializeField] private float _rotation;

        [SuffixLabel("units"), Tooltip("Смещение области относительно исполнителя")]
        [SerializeField] private Vector3 _offset;

        public AreaType Type => _type;
        public Vector3 Offset => _offset;
        public float Rotation => IsRotationUsed ? _rotation : default;
        public float Radius => IsRadiusUsed ? _value1 : 0.1f;
        public Vector2 Size => IsSizeUsed ? new Vector2(_value1, _value2) : default;
        public float AngleWidth => IsAngleWidthUsed ? _value2 : default;

        private bool IsRadiusUsed => _type == AreaType.Radius ||
                                     _type == AreaType.Sector;
        private bool IsRotationUsed => _type == AreaType.Rect ||
                                       _type == AreaType.Sector;
        private bool IsAngleWidthUsed => _type == AreaType.Sector;
        private bool IsSizeUsed => _type == AreaType.Rect;
    }
}
