using System;
using System.Linq;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Filters;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;
using Sirenix.OdinInspector;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    [Serializable]
    public sealed class EffectTargetFilterData
    {
        [SerializeField, HideLabel]
        private UnitFilterParams _filterParams;

        [Tooltip("Типы целевых сущностей в порядке уменьшения приоритета:\n" +
                 nameof(ImpactTargetType.None) + " — неопределено,\n" +
                 nameof(ImpactTargetType.Unit) + " — моб/игрок,\n" +
                 nameof(ImpactTargetType.Barrel) + " — бочка,\n" +
                 nameof(ImpactTargetType.Trap) + " — клетка,\n" +
                 nameof(ImpactTargetType.Totem) + " — тотем.\n" +
                 "Сущности, которые не добавлены в список, " +
                 "будут отфилттровываться при выборке.\n" +
                 "Если список пуст, то будут учитываться все типы сущностей.")]
        [SerializeField] private ImpactTargetType[] _orderedAvailableTypes;

        public int CalcOrderIndex([NotNull] Object target) =>
            _orderedAvailableTypes.IndexOf(o => DetectType(target) == o);

        public int CalcOrderIndex([NotNull] IImpactTarget impactTarget) =>
            _orderedAvailableTypes.IndexOf(o => impactTarget.ImpactTargetType == o);

        public bool IsMatching([NotNull] Object target) =>
            _filterParams.IsMatching(target) &&
            (_orderedAvailableTypes.Length == 0 ||
             _orderedAvailableTypes.Contains(DetectType(target)));

        public bool IsMatching([NotNull] IImpactTarget impactTarget) =>
            _filterParams.IsMatching(impactTarget) &&
            (_orderedAvailableTypes.Length == 0 ||
             _orderedAvailableTypes.Contains(impactTarget.ImpactTargetType));

        private ImpactTargetType DetectType([NotNull] Object target) =>
            target switch
            {
                Trap => ImpactTargetType.Trap,
                Totem => ImpactTargetType.Totem,
                UnitNetwork => ImpactTargetType.Unit,
                BarrelNetwork => ImpactTargetType.Barrel,
                _ => ImpactTargetType.None
            };
    }
}
