using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    [Serializable]
    public sealed class EffectRepeatsData
    {
        public enum RepeatType
        {
            Once, // без повторений
            Many // с повторениями
        }

        [Tooltip("Кол-во повторений:\n" +
                 nameof(RepeatType.Once) + " — без повторений,\n" +
                 nameof(RepeatType.Many) + " — с заданным кол-ом повторений.")]
        [SerializeField] private RepeatType _type;

        [MinValue(2), ShowIf(nameof(_type), RepeatType.Many)]
        [SerializeField] private int _repeatsCount;

        [MinValue(0), ShowIf(nameof(_type), RepeatType.Many), SuffixLabel("sec", true)]
        [SerializeField] private int _repeatDelay;

        public RepeatType Type => _type;
        public int RepeatsCount => _type == RepeatType.Many ? _repeatsCount : 1;
        public TimeSpan RepeatDelay => _type == RepeatType.Many
            ? TimeSpan.FromSeconds(_repeatDelay) : TimeSpan.Zero;
    }
}
