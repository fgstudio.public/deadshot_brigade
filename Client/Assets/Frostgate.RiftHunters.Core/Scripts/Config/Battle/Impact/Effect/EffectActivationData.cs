using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    [Serializable]
    public sealed class EffectActivationData
    {
        public enum ActivationType
        {
            Instantly, // моментально
            Delayed, // через заданное время
            DelayedByDistance // через заданное время, которое завист от расстояния до цели
        }

        [HorizontalGroup(MinWidth = 190), LabelWidth(50),
         Tooltip("Когда активировать эффект:\n" +
                 nameof(ActivationType.Instantly) + " — моментально,\n" +
                 nameof(ActivationType.Delayed) + " — через заданное время,\n" +
                 nameof(ActivationType.DelayedByDistance) + " — через заданное время, зависящее от расстояния до цели.")]
        [SerializeField] private ActivationType _type;

        [MinValue(0), ShowIf(nameof(IsDelayUsed)),
         SuffixLabel("sec", true), HideLabel, HorizontalGroup]
        [SerializeField] private float _delay;

        [ShowIf(nameof(IsDelayByDistanceUsed)),
         SuffixLabel("x: units, y: sec"), HideLabel, HorizontalGroup]
        [SerializeField] private AnimationCurve _delayByDistance;

        public ActivationType Type => _type;
        public float Delay => IsDelayUsed ? _delay : default;
        public AnimationCurve DelayByDistance => IsDelayByDistanceUsed ? _delayByDistance : new AnimationCurve();

        private bool IsDelayUsed => _type == ActivationType.Delayed;
        private bool IsDelayByDistanceUsed => _type == ActivationType.DelayedByDistance;
    }
}
