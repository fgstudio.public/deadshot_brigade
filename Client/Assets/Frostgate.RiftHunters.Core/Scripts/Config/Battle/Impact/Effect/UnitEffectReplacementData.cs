using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Impact
{
    [Serializable]
    public sealed class UnitEffectReplacementData
    {
        [field: SerializeField] public float Damage { get; private set; }
        [field: SerializeField, Required] public UnitEffectConfig ReplaceableEffect { get; private set; }
        [field: SerializeField] public UnitEffectConfig ReplacingEffect { get; private set; }
    }
}