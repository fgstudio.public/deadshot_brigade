using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    [Serializable]
    public sealed class EffectData
    {
        [field: Required] // TODO: тесты на уникальность
        [field: SerializeField] public string Id { get; private set; }

        [field: HideLabel, BoxGroup(nameof(RepeatsData)),
                Tooltip("Настройки кол-ва повторений эффекта")]
        [field: SerializeField] public EffectRepeatsData RepeatsData { get; private set; }

        [field: HideLabel, BoxGroup(nameof(TargetData)),
                Tooltip("Настройки определения первичных позиций для срабатывания эффекта")]
        [field: SerializeField] public EffectTargetData TargetData { get; private set; }

        [field: HideLabel, BoxGroup(nameof(ActivationData)),
                Tooltip("Настройки времени активации эффекта")]
        [field: SerializeField]public EffectActivationData ActivationData { get; private set; }

        [field: HideLabel, BoxGroup(nameof(AffectAreaData)),
                Tooltip("Настройки области, в которой будут найдены конечные цели, по которым будет применён эффект")]
        [field: SerializeField] public EffectAffectAreaData AffectAreaData { get; private set; }

        [Tooltip("Конкретные воздействия на цели в определённой области")]
        [SerializeField] private EffectAffectData[] _affectData;
        public IReadOnlyList<EffectAffectData> AffectData => _affectData;
    }
}
