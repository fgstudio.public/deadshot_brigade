using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    [Serializable]
    public sealed class EffectTargetData
    {
        public enum TargetType
        {
            Self, // Цель — исполнитель способности
            Filtered, // Цель конфигурируется в конфиге
            NoAim // Нет цели
        }

        [Tooltip("Как определять цели:\n" +
                 nameof(TargetType.Self) + " — использовать себя,\n" +
                 nameof(TargetType.Filtered) + " — найти по параметрам,\n" +
                 nameof(TargetType.NoAim) + " — нет цели.")]
        [SerializeField] private TargetType _type;

        [SerializeField, HideLabel, ShowIf(nameof(IsFilteredUsed))]
        private EffectTargetFilterData _filterData;

        [SerializeField, HideLabel, ShowIf(nameof(IsFilteredUsed)), Title(nameof(DetectionData))]
        private EffectTargetDetectionData _detectionData;

        public TargetType Type => _type;
        public EffectTargetFilterData FilterData => IsFilteredUsed ? _filterData : null;
        public EffectTargetDetectionData DetectionData => IsFilteredUsed ? _detectionData : null;

        private bool IsFilteredUsed => _type == TargetType.Filtered;
    }
}
