using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    [Serializable]
    public sealed class EffectAffectDurationData
    {
        public enum DurationType
        {
            Instantly, // моментально
            ContinuousFor, // постепенно в течение некоторого времени
            RepeatableFor // размеренными по времени порциями
        }

        [Tooltip("Продолжительность измененения характеристик:\n" +
                 nameof(DurationType.Instantly) + " — моментально,\n" +
                 nameof(DurationType.ContinuousFor) + " — постепенное изменение в течение заданного времени,\n" +
                 nameof(DurationType.RepeatableFor) + " — несколько повторений через промежутки времени.")]
        [SerializeField] private DurationType _type;

        [ShowIf(nameof(IsDurationUsed)), MinValue(0), SuffixLabel("sec", true)]
        [Tooltip("Длительность, в течение которой изменятся характеристики на указанное число")]
        [SerializeField] private float _duration;

        [ShowIf(nameof(IsRepeatableUsed)), MinValue(0), HorizontalGroup]
        [Tooltip("Сколько раз нужно будет изменить характеристики на указанное число")]
        [SerializeField] private int _repeatCount;

        [ShowIf(nameof(IsRepeatableUsed)), MinValue(0), SuffixLabel("sec", true), HorizontalGroup]
        [Tooltip("Через какие промежутки времени производить повторные изменения характеристик")]
        [SerializeField] private float _repeatCooldown;

        public DurationType Type => _type;
        public TimeSpan Duration => IsDurationUsed ? TimeSpan.FromSeconds(_duration) : default;
        public float RepeatCount => IsRepeatableUsed ? _repeatCount : default;
        public TimeSpan RepeatCooldown => IsRepeatableUsed ? TimeSpan.FromSeconds(_repeatCooldown) : default;

        private bool IsDurationUsed => _type == DurationType.ContinuousFor;
        private bool IsRepeatableUsed => _type == DurationType.RepeatableFor;
    }
}
