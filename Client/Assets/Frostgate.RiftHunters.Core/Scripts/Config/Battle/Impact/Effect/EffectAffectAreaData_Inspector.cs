using System;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    public sealed partial class EffectAffectAreaData
    {
        #region VALUE_1

        private bool IsUsedValue1 =>
            _type switch
            {
                AreaType.Radius => true,
                AreaType.Sector => true,
                AreaType.Point => false,
                AreaType.Rect => true,
                _ => throw new ArgumentOutOfRangeException()
            };

        private string LabelValue1 =>
            _type switch
            {
                AreaType.Radius => "Radius",
                AreaType.Sector => "Radius",
                AreaType.Point => string.Empty,
                AreaType.Rect => "X size",
                _ => throw new ArgumentOutOfRangeException()
            };

        private string TooltipValue1 =>
            _type switch
            {
                AreaType.Radius => "Радиус окружности",
                AreaType.Sector => "Радиус сектора",
                AreaType.Point => string.Empty,
                AreaType.Rect => "Ширина прямоугольной области",
                _ => throw new ArgumentOutOfRangeException()
            };

        private string SuffixValue1 => "units";

        #endregion // VALUE_1

        #region VALUE_2

        private bool IsUsedValue2 =>
            _type switch
            {
                AreaType.Radius => false,
                AreaType.Sector => true,
                AreaType.Point => false,
                AreaType.Rect => true,
                _ => throw new ArgumentOutOfRangeException()
            };

        private string LabelValue2 =>
            _type switch
            {
                AreaType.Radius => string.Empty,
                AreaType.Sector => "Angle Width",
                AreaType.Point => string.Empty,
                AreaType.Rect => "Z size",
                _ => throw new ArgumentOutOfRangeException()
            };

        private string TooltipValue2 =>
            _type switch
            {
                AreaType.Radius => string.Empty,
                AreaType.Sector => "Ширина сектора в градусах",
                AreaType.Point => string.Empty,
                AreaType.Rect => "Высота прямоугольной области",
                _ => throw new ArgumentOutOfRangeException()
            };

        private string SuffixValue2 =>
            _type switch
            {
                AreaType.Radius => string.Empty,
                AreaType.Sector => "deg",
                AreaType.Point => string.Empty,
                AreaType.Rect => "units",
                _ => throw new ArgumentOutOfRangeException()
            };

        #endregion VALUE_2
    }
}
