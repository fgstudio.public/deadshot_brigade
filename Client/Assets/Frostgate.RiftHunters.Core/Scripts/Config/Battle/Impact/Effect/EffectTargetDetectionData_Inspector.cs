using System;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    public sealed partial class EffectTargetDetectionData
    {
        #region VALUE_1

        private bool IsUsedValue1 =>
            _type switch
            {
                DetectionType.AutoAimInRadius => true,
                DetectionType.AutoAimInSector => true,
                DetectionType.AutoAimInPoint => false,
                DetectionType.AutoAimInRect => true,
                _ => throw new ArgumentOutOfRangeException()
            };

        private string LabelValue1 =>
            _type switch
            {
                DetectionType.AutoAimInRadius => "Radius",
                DetectionType.AutoAimInSector => "Radius",
                DetectionType.AutoAimInPoint => string.Empty,
                DetectionType.AutoAimInRect => "X size",
                _ => throw new ArgumentOutOfRangeException()
            };

        private string TooltipValue1 =>
            _type switch
            {
                DetectionType.AutoAimInRadius => "Радиус окружности",
                DetectionType.AutoAimInSector => "Радиус сектора",
                DetectionType.AutoAimInPoint => string.Empty,
                DetectionType.AutoAimInRect => "Ширина прямоугольной области",
                _ => throw new ArgumentOutOfRangeException()
            };

        private string SuffixValue1 => "units";

        #endregion // VALUE_1

        #region VALUE_2

        private bool IsUsedValue2 =>
            _type switch
            {
                DetectionType.AutoAimInRadius => false,
                DetectionType.AutoAimInSector => true,
                DetectionType.AutoAimInPoint => false,
                DetectionType.AutoAimInRect => true,
                _ => throw new ArgumentOutOfRangeException()
            };

        private string LabelValue2 =>
            _type switch
            {
                DetectionType.AutoAimInRadius => string.Empty,
                DetectionType.AutoAimInSector => "Angle Width",
                DetectionType.AutoAimInPoint => string.Empty,
                DetectionType.AutoAimInRect => "Z size",
                _ => throw new ArgumentOutOfRangeException()
            };

        private string TooltipValue2 =>
            _type switch
            {
                DetectionType.AutoAimInRadius => string.Empty,
                DetectionType.AutoAimInSector => "Ширина сектора в градусах",
                DetectionType.AutoAimInPoint => string.Empty,
                DetectionType.AutoAimInRect => "Высота прямоугольной области",
                _ => throw new ArgumentOutOfRangeException()
            };

        private string SuffixValue2 =>
            _type switch
            {
                DetectionType.AutoAimInRadius => string.Empty,
                DetectionType.AutoAimInSector => "deg",
                DetectionType.AutoAimInPoint => string.Empty,
                DetectionType.AutoAimInRect => "units",
                _ => throw new ArgumentOutOfRangeException()
            };

        #endregion VALUE_2
    }
}
