using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Server.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Effect
{
    public interface ITypeEffectAffectData { }

    public interface IEffectAffectStatData : ITypeEffectAffectData
    {
        float Health { get; }
        EffectAffectDurationData StatDurationData { get; }
    }

    public interface IEffectAffectHealPercentData : ITypeEffectAffectData
    {
        float HealPercent { get; }
    }

    public interface IEffectAffectBuffData : ITypeEffectAffectData
    {
        IReadOnlyList<UnitEffectConfig> Buffs { get; }
    }

    public interface IDetonationEffectAffectData : ITypeEffectAffectData
    {
        IReadOnlyList<UnitEffectReplacementData> UnitEffectReplacements { get; }
    }

    public interface IEffectAffectSpawnData : ITypeEffectAffectData
    {
        BotSpawnerConfig SpawnerConfig { get; }
    }

    public interface IEffectAffectGenerationData : ITypeEffectAffectData
    {
        GameObject Prefab { get; }
        IEnumerable<Type> AllowedTypes { get; }
        PointOffsetData PrefabOffsetData { get; }
    }

    public interface IEffectAffectUnitActionData : ITypeEffectAffectData
    {
        [NotNull] IReadOnlyList<UnitActionDamageTrigger> ActionTriggers { get; }
    }

    [Serializable]
    public sealed class EffectAffectData : IEffectAffectBuffData, IDetonationEffectAffectData,
        IEffectAffectStatData, IEffectAffectGenerationData, IEffectAffectUnitActionData,
        IEffectAffectHealPercentData, IEffectAffectSpawnData
    {
        private static readonly Type[] allowedTypes
            = new[] { typeof(ITotem), typeof(ITrap), typeof(IMine), typeof(IWaveSpawner), typeof(ILinkSpawner), typeof(IShield) };

        public enum AffectType
        {
            Buff, // применения бафа
            Stat, // изменение характеристики персонажа
            Generation, // генерация некоторого объекта
            Detonation, // замена эффектов
            UnitAction, // реакция на урон
            HealPercent, // лечение в процентах от максимального здоровья
            Spawn, // спаун противников из конфига
        }

        [HideLabel, Title(nameof(RepeatsPerTarget)),
         Tooltip("Настройки количества повторений воздействия на каждую цель")]
        [SerializeField] private CountData _repeatsPerTarget;

        [Title("Affect")]
        [Tooltip("Тип воздействия:\n" +
                 nameof(AffectType.Buff) + " — применение бафа,\n" +
                 nameof(AffectType.Stat) + " — изменение характеристик персонажа,\n" +
                 nameof(AffectType.Detonation) + " — замена эффектов,\n" +
                 nameof(AffectType.Generation) + " — генерация некоторого объекта,\n" +
                 nameof(AffectType.UnitAction) + " — реакция юнита на урон,\n" +
                 nameof(AffectType.HealPercent) + " — лечение в процентах от максимального здоровья,\n" +
                 nameof(AffectType.Spawn) + " — спаун противников из конфига")]
        [SerializeField] private AffectType _type;

        [ShowIf(nameof(IsBuffUsed)), Required, AssetsOnly]
        [SerializeField] private UnitEffectConfig[] _buffs;

        [ShowIf(nameof(IsDetonationUsed)), Required, AssetsOnly]
        [SerializeField] private UnitEffectReplacementData[] _unitEffectReplacements;

        // TODO: представить характеристики в виде ScriptableObject'ов или enum'а
        [ShowIf(nameof(IsStatUsed))]
        [Tooltip("Отрицательное значение — для урона.\n" +
                 "Положительное значение — для лечения.")]
        [SerializeField] private float _health;
        [ShowIf(nameof(IsHealPercentUsed))]
        [Tooltip("Лечение в доле от максимального здоровья")]
        [SerializeField] private float _healPercent;

        [ShowIf(nameof(IsStatUsed)), Indent, HideLabel, Title(nameof(StatDurationData))]
        [Tooltip("Настройки длительности процесса изменения характеристик")]
        [SerializeField] private EffectAffectDurationData _statDurationData;

        [ShowIf(nameof(IsPrefabUsed)), Required, AssetsOnly,
         AssetSelector(Paths = "Assets/Frostgate.RiftHunters.Core/Battle/"),
         ValidateInput(nameof(ValidatePrefabType), "$" + nameof(PrefabTypeError))]
        [SerializeField] private GameObject _prefab;

        [ShowIf(nameof(IsPrefabUsed)), Indent, HideLabel, Title(nameof(PrefabOffsetData))]
        [Tooltip("Настройки позиционирования сгенерированного объекта")]
        [SerializeField] private PointOffsetData _prefabOffsetData;

        [ShowIf(nameof(IsUnitActionUsed)), Required, AssetsOnly]
        [Tooltip("Реакции юнита на урон")]
        [SerializeField] private UnitActionDamageTrigger[] _unitActions;

        [ShowIf(nameof(IsSpawnerUsed)), Required, AssetsOnly]
        [Tooltip("Список противников для спауна")]
        [SerializeField] private BotSpawnerConfig _spawnerConfig;

        public CountData RepeatsPerTarget => _repeatsPerTarget;
        public AffectType Type => _type;

        public IReadOnlyList<UnitEffectConfig> Buffs => IsBuffUsed ? _buffs : Array.Empty<UnitEffectConfig>();
        public IReadOnlyList<UnitEffectReplacementData> UnitEffectReplacements =>
            IsDetonationUsed ? _unitEffectReplacements : Array.Empty<UnitEffectReplacementData>();

        public float Health => IsStatUsed ? _health : default;
        public float HealPercent => _healPercent;
        public EffectAffectDurationData StatDurationData => IsStatUsed ? _statDurationData : null;

        public GameObject Prefab => IsPrefabUsed ? _prefab : null;
        public IEnumerable<Type> AllowedTypes => IsPrefabUsed ? allowedTypes : Enumerable.Empty<Type>();
        public PointOffsetData PrefabOffsetData => IsPrefabUsed ? _prefabOffsetData : null;
        public IReadOnlyList<UnitActionDamageTrigger> ActionTriggers => IsUnitActionUsed ? _unitActions : Array.Empty<UnitActionDamageTrigger>();
        public BotSpawnerConfig SpawnerConfig => IsSpawnerUsed ? _spawnerConfig : null;

        private bool IsBuffUsed => _type == AffectType.Buff;
        private bool IsDetonationUsed => _type == AffectType.Detonation;
        private bool IsStatUsed => _type == AffectType.Stat;
        private bool IsHealPercentUsed => _type == AffectType.HealPercent;
        private bool IsPrefabUsed => _type == AffectType.Generation;
        private bool IsUnitActionUsed => _type == AffectType.UnitAction;
        private bool IsSpawnerUsed => _type == AffectType.Spawn;

        private static string PrefabTypeError => $"Supports only: {string.Join(',', allowedTypes.Select(t => t.Name.ToString()))}";
        private static bool ValidatePrefabType(GameObject prefab) => allowedTypes.Any(t => prefab != null && prefab.TryGetComponent(t, out _));
    }
}