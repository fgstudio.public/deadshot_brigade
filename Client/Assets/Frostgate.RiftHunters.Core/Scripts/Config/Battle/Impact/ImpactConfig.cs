using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Impact.Effect;
using Frostgate.RiftHunters.Core.Battle.Server.UnitAI;

namespace Frostgate.RiftHunters.Core.Battle.Impact
{
    [TypeInfoBox("Конфиг логики работы воздействия.\n" +
                 "Не конфигурирует визуальное поведение.\n" +
                 "Логика имеет ряд событий, которые может прослушивать визуальная часть.")]
    [CreateAssetMenu(
        fileName = nameof(ImpactConfig),
        menuName = BattleAssetMenus.Shared.Impacts.Menu + "/" + nameof(ImpactConfig))]
    public sealed class ImpactConfig : ScriptableObject
    {
        /////////////// Main Data ///////////////

        // TODO: тесты на проверку уникальности
        [field: Required, BoxGroup("Main Data")]
        [field: SerializeField] public string Id { get; private set; }

        /////////////// Cast Data ///////////////

        [HideLabel, BoxGroup("Cast Data")]
        [SerializeField] private CastData _castData;

        [HideLabel, BoxGroup("Ai Conditions")]
        [SerializeField] private ImpactAIConditions _aiConditions;

        public bool CanMove => _castData.CanMove;
        public TimeSpan CastDuration => TimeSpan.FromSeconds(_castData.Duration);

        /////////////// Effects ///////////////

        [Space, Required, ListDrawerSettings(NumberOfItemsPerPage = 1)]
        [SerializeField] private EffectData[] _effects;
        public IReadOnlyList<EffectData> Effects => _effects;
        public ImpactAIConditions AIConditions => _aiConditions;
    }
}