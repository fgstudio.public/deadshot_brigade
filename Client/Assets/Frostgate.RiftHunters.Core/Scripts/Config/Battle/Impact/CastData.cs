using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Impact
{
    [Serializable]
    public sealed class CastData
    {
        [field: Tooltip("Разрешение движения на время каста (" + nameof(Duration) + ")")]
        [field: SerializeField] public bool CanMove { get; private set; }

        [field: MinValue(0), SuffixLabel("sec", true),
                Tooltip("Длительность каста. Влияет только на " + nameof(CanMove))]
        [field: SerializeField] public float Duration { get; private set; }
    }
}
