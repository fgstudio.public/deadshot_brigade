using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Filters;

namespace Frostgate.RiftHunters.Core.Battle.Shared.Impact
{
    [CreateAssetMenu(
        fileName = nameof(LinkSpawnerConfig),
        menuName = BattleAssetMenus.Shared.Impacts.Menu + "/" + nameof(LinkSpawnerConfig))]
    [HelpURL("https://www.notion.so/frostgate/fa2f1c562cb9488fbbafe2c04b2ea6c4")]
    public sealed class LinkSpawnerConfig : ScriptableObject
    {
        [Serializable, InlineProperty] private sealed class EffectsByCount
        {
            [MinValue(0), HorizontalGroup(LabelWidth = 40)] public int Count;
            [Required, AssetSelector, HorizontalGroup] public UnitEffectConfig[] Effects;
        }

        [SerializeField, MinValue(0), SuffixLabel("units", true)] private float _radius;
        [SerializeField, MinValue(0), SuffixLabel("sec", true)] private float _duration;
        [SerializeField, Required, HideLabel, BoxGroup(nameof(FilterParams))] private UnitFilterParams _filterParams;
        [ValidateInput(nameof(ValidateEffectCounts), "Elements have to be ordered by unique count")]
        [SerializeField, Required] private EffectsByCount[] _effects;
        [SerializeField, CanBeNull] private LinkSpawnerConfig _distributingConfig;

        public float Radius => _radius;
        public TimeSpan Duration => TimeSpan.FromSeconds(_duration);
        public UnitFilterParams FilterParams => _filterParams;
        [CanBeNull] public LinkSpawnerConfig DistributingConfig => _distributingConfig;
        [NotNull] public IReadOnlyDictionary<int, IReadOnlyList<UnitEffectConfig>> Effects =>
            _effectsRuntimeCache ??= CreateCache(_effects);

        private Dictionary<int, IReadOnlyList<UnitEffectConfig>> _effectsRuntimeCache;

        private void OnValidate()
        {
            _effectsRuntimeCache = null;
            _effects = _effects.OrderBy(e => e.Count).ToArray();
        }

        private Dictionary<int, IReadOnlyList<UnitEffectConfig>> CreateCache(EffectsByCount[] effects) =>
            effects.ToDictionary(e => e.Count, e => e.Effects as IReadOnlyList<UnitEffectConfig>);

        private bool ValidateEffectCounts()
        {
            for (int i = 1; i < _effects.Length; i++)
            {
                EffectsByCount prev = _effects[i - 1];
                EffectsByCount cur = _effects[i];

                if (prev.Count >= cur.Count) return false;
            }

            return true;
        }
    }
}