using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Battle.Impact.Client
{
    [TypeInfoBox("Конфиг данных воздействия для UI.\n")]
    [CreateAssetMenu(fileName = nameof(ImpactUIConfig),
                     menuName = BattleAssetMenus.Client.Impacts.Menu + "/" + nameof(ImpactUIConfig))]
    public sealed class ImpactUIConfig : ScriptableObject, IConfig
    {
        [SerializeField, Required, AssetsOnly, AssetSelector]
        private ImpactConfig _config;

        // TODO: удалить после появления локализации
        [field: Required, Tooltip("Отображаемое имя способности")]
        [field: SerializeField] public string Name { get; private set; }

        [field: Tooltip("Отображаемое описание способности")]
        [field: SerializeField, TextArea] public string Description { get; private set; }

        [field: Required, AssetsOnly,
                AssetSelector(Paths = "Assets/Frostgate.RiftHunters.Core/UI/Impacts/Sprites")]
        [field: SerializeField] public Sprite Icon { get; private set; }

        public string Id => _config.Id;
    }
}