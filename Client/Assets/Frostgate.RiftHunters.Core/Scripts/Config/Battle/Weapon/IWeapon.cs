﻿using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle
{
    public interface IWeapon : IDamageSource, IIdentifiable
    {
        string AnimationKey { get; }
        bool SyncEveryShoot { get; } // TODO: вероятно неактуальный параметр
        float InAttackMoveSpeedMultiplier { get; }
        public float CriticalDamageChance { get; }
        public float CriticalDamageMultiplier { get; }
        public float BreakShieldChance { get; }
        UnitEffectConfig[] UnitEffects { get; }
        float MaxDistance { get; }
        float AttackAngle { get; }
        float AttackVerticalAngle { get; }
        float AttackOriginHalfSize { get; }
        float ShootTime { get; }
        float FreezeBeforeShootDuration { get; }
        float Cooldown { get; }
        IReadOnlyList<UnitActionDamageTrigger> UnitActionDamageTriggers { get; }
        float MaxCastDistance { get; }

        float GetDamage(float distance = 0);
        float GetDistanceMultiplier(float distance = 0);
    }
}