using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle
{
    public abstract class WeaponConfig : Equipment, IWeapon
    {
        protected const string WeaponBoxName = "Core Weapon Data";
        protected const string WeaponViewBoxName = "View Weapon Data";

        private const float ShootDistanceExpander = 0.5f + 0.5f;
        private const float MaxAttackAngle = 180;
        private const float MaxAttackDistance = 50;
        private const float MaxAttackOriginHalfSize = 3;
        private const int MinDamageRank = 1;
        private const int MaxDamageRank = 6;
        private const int MinReloadRank = 1;
        private const int MaxReloadRank = 6;

        [field: SerializeField, BoxGroup(WeaponViewBoxName)] public string AnimationKey { get; private set; }

        // TODO: вероятно неактуальный параметр
        bool IWeapon.SyncEveryShoot => SyncEveryShoot;
        [field: SerializeField, BoxGroup(WeaponBoxName)] public bool SyncEveryShoot;
        [field: SerializeField, BoxGroup(WeaponBoxName), Range(0, 1)] public float InAttackMoveSpeedMultiplier { get; private set; } = 1;
        [field: SerializeField, BoxGroup(WeaponBoxName), Min(0)] public float CriticalDamageChance { get; private set; }
        [field: SerializeField, BoxGroup(WeaponBoxName), Min(0)] public float CriticalDamageMultiplier { get; private set; }
        [field: SerializeField, BoxGroup(WeaponBoxName), Range(0, 1)] public float BreakShieldChance { get; private set; }
        [field: SerializeField, BoxGroup(WeaponBoxName)] public UnitEffectConfig[] UnitEffects { get; private set; }
        [field: SerializeField, BoxGroup(WeaponBoxName)] private AnimationCurve DamageByDistanceMultiplier { get; set; }
        [field: SerializeField, BoxGroup(WeaponBoxName)] private float Damage { get; set; }
        [field: SerializeField, BoxGroup(WeaponBoxName), Range(0, MaxAttackDistance)] public float MaxDistance { get; private set; }
        [field: SerializeField, BoxGroup(WeaponBoxName), Range(0, MaxAttackAngle), SuffixLabel("degree")] public float AttackAngle { get; private set; }
        [field: SerializeField, BoxGroup(WeaponBoxName), Range(0, MaxAttackAngle), SuffixLabel("degree")] public float AttackVerticalAngle { get; private set; } = 45;
        [field: SerializeField, BoxGroup(WeaponBoxName), Range(0, MaxAttackOriginHalfSize)] public float AttackOriginHalfSize { get; private set; } = 0.3f;
        [field: SerializeField, BoxGroup(WeaponBoxName), Min(0), SuffixLabel("sec", true)] public float ShootTime { get; private set; }

        [field: Tooltip("интервал времени перед выстрелом, когда юнит когда у югнита замораживается вектор прицеливания и ходьба")]
        [field: SerializeField, BoxGroup(WeaponBoxName), Min(0), SuffixLabel("sec", true)] public float FreezeBeforeShootDuration { get; private set; }
        [field: SerializeField, BoxGroup(WeaponBoxName), Min(0), SuffixLabel("sec", true)] public float Cooldown { get; private set; }
        [field: SerializeField, BoxGroup(WeaponBoxName), Range(MinDamageRank, MaxDamageRank)] public int DamageRank { get; private set; }
        [field: SerializeField, BoxGroup(WeaponBoxName), Range(MinReloadRank, MaxReloadRank)] public int ReloadRank { get; private set; }
        [SerializeField, BoxGroup(WeaponBoxName)] private UnitActionDamageTrigger[] _unitActionDamageTriggers;

        public IReadOnlyList<UnitActionDamageTrigger> UnitActionDamageTriggers => _unitActionDamageTriggers;
        public float MaxCastDistance => MaxDistance + ShootDistanceExpander;
        protected abstract DamageType DamageType { get; }
        DamageType IDamageSource.Type => DamageType;

        public float GetDamage(float distance = 0) => Damage * GetDistanceMultiplier(distance);
        public float GetDistanceMultiplier(float distance = 0) => DamageByDistanceMultiplier.Evaluate(distance / MaxDistance);

#if UNITY_EDITOR
        public void SetShootingArea(float attackOriginHalfSize, float minAttackDistance, float maxAttackDistance, float attackAngle, float attackVerticalAngle)
        {
            if (Application.isPlaying)
                UnityEngine.Debug.LogError("Cam be execute only in Edit Mode");

            AttackOriginHalfSize = Mathf.Clamp(attackOriginHalfSize, 0, MaxAttackOriginHalfSize);

            AttackAngle = Mathf.Clamp(attackAngle, 0, MaxAttackAngle);
            AttackVerticalAngle = Mathf.Clamp(attackVerticalAngle, 0, MaxAttackAngle);

            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }
}