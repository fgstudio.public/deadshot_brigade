using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle
{
    public enum UnitActionTriggerTypes
    {
        Damage = 0,
        CriticalDamage = 1,
        FatalDamage = 2
    }

    [CreateAssetMenu(fileName = nameof(UnitActionDamageTrigger),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(UnitActionDamageTrigger))]
    public class UnitActionDamageTrigger : ScriptableObject
    {
        [Tooltip("Тип триггера определяет на каком игровом собитии он сработает")]
        [SerializeField] private UnitActionTriggerTypes _triggerType;

        [field: Tooltip("Тип действия определяет какое действие этот триггер будет вызывать при срабатывании")]
        [field: SerializeField] public UnitActionTypes ActionType { get; private set; }

        public bool IsFatalDamage => _triggerType == UnitActionTriggerTypes.FatalDamage;
        public bool IsDamage => _triggerType == UnitActionTriggerTypes.Damage;
        public bool IsCriticalDamage => _triggerType == UnitActionTriggerTypes.CriticalDamage;
    }
}