using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle
{
    [CreateAssetMenu(fileName = nameof(RangeWeaponConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(RangeWeaponConfig))]
    public class RangeWeaponConfig : WeaponConfig
    {
        protected const string RangeBoxName = "Core Range Weapon Data";

        [field: SerializeField, BoxGroup(WeaponViewBoxName)] public string AttackAnimation { get; private set; }

        [field: SerializeField, BoxGroup(RangeBoxName), Min(0), SuffixLabel("sec", true)] public float ReloadSeconds { get; private set; }
        [field: SerializeField, BoxGroup(RangeBoxName), Range(0, 1)] public float InReloadMoveSpeedMultiplier { get; private set; } = 1;
        [field: SerializeField, BoxGroup(RangeBoxName), Min(0)] public int PatronsCount { get; private set; }
        [field: SerializeField, BoxGroup(RangeBoxName), Range(1, 100)] public int BulletsPerShoot { get; private set; } = 1;
        [field: SerializeField, BoxGroup(RangeBoxName), Range(0, 360), SuffixLabel("degree")] public float ScatterAngle { get; private set; }
        [field: SerializeField, BoxGroup(RangeBoxName), Range(1, 5000), SuffixLabel("m/s")] public float BulletSpeed { get; private set; } = 2000;

        protected override DamageType DamageType => DamageType.Range;
        public bool UsePatrons => PatronsCount > 0;

        protected override bool IsTypePropertyEnabled => false;

        private void OnValidate()
        {
            _type = EquipmentType.RangeWeapon;
        }
    }
}