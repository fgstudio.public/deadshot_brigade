using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle
{
    [CreateAssetMenu(fileName = nameof(MeleeWeaponConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(MeleeWeaponConfig))]
    public class MeleeWeaponConfig : WeaponConfig, IMeleeWeapon
    {
        protected override DamageType DamageType => DamageType.Melee;
    }
}