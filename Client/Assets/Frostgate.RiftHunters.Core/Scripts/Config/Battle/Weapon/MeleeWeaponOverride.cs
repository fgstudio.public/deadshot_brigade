﻿using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Battle
{
    public sealed class MeleeWeaponOverride : IMeleeWeapon
    {
        [NotNull] private IMeleeWeapon _originalWeapon;
        private float? _damageOverride;
        private float? _attackTimeOverride;
        private float? _attackCooldownOverride;

        public string Id => _originalWeapon.Id;
        public DamageType Type => _originalWeapon.Type;
        public float BreakShieldChance => _originalWeapon.BreakShieldChance;
        public UnitEffectConfig[] UnitEffects => _originalWeapon.UnitEffects;
        public float MaxDistance => _originalWeapon.MaxDistance;
        public float AttackAngle => _originalWeapon.AttackAngle;
        public float AttackVerticalAngle => _originalWeapon.AttackVerticalAngle;
        public float AttackOriginHalfSize => _originalWeapon.AttackOriginHalfSize;
        public float ShootTime => _attackTimeOverride ?? _originalWeapon.ShootTime;
        public float FreezeBeforeShootDuration => _originalWeapon.FreezeBeforeShootDuration;
        public float Cooldown => _attackCooldownOverride ?? _originalWeapon.Cooldown;
        public string AnimationKey => _originalWeapon.AnimationKey;
        public bool SyncEveryShoot => _originalWeapon.SyncEveryShoot;
        public float InAttackMoveSpeedMultiplier => _originalWeapon.InAttackMoveSpeedMultiplier;
        public float CriticalDamageChance => _originalWeapon.CriticalDamageChance;
        public float CriticalDamageMultiplier => _originalWeapon.CriticalDamageMultiplier;
        public IReadOnlyList<UnitActionDamageTrigger> UnitActionDamageTriggers => _originalWeapon.UnitActionDamageTriggers;
        public float MaxCastDistance => _originalWeapon.MaxCastDistance;

        public MeleeWeaponOverride([NotNull] IMeleeWeapon meleeWeapon)
        {
            _originalWeapon = meleeWeapon;
        }

        public void OverrideWeapon([NotNull] IMeleeWeapon meleeWeapon) => _originalWeapon = meleeWeapon;
        public void OverrideDamage(float damage) => _damageOverride = damage;
        public void OverrideAttackTime(float attackSpeed) => _attackTimeOverride = attackSpeed;
        public void OverrideAttackCooldown(float attackCooldown) => _attackCooldownOverride = attackCooldown;

        public float GetDamage(float distance = 0) =>
            _damageOverride.HasValue
                ? _damageOverride.Value * GetDistanceMultiplier(distance)
                : _originalWeapon.GetDamage(distance);

        public float GetDistanceMultiplier(float distance) =>
            _originalWeapon.GetDistanceMultiplier(distance);
    }
}