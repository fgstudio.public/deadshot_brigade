using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle
{
    public enum UnitActionTypes
    {
        None = 0,
        LowDamage = 10,
        MiddleDamage = 11,
        PowerDamage = 12,
        UltraDamage = 13,
        LowFatalDamage = 20,
        MiddleFatalDamage = 21,
        PowerFatalDamage = 22,
        Dodge = 50,
    }

    [CreateAssetMenu(fileName = nameof(UnitActionConfig),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(UnitActionConfig))]
    public class UnitActionConfig : EntityConfig
    {
        [field: SerializeField] public UnitActionTypes Type { get; private set; }
        [field: SerializeField] public string AnimationTrigger { get; private set; }
        [field: SerializeField] public AnimationCurve ForwardMoveCurve { get; private set; }
        [field: SerializeField] public float StaminaCost { get; private set; }

        public float Duration => ForwardMoveCurve[ForwardMoveCurve.length - 1].time;

        public bool IsDeath =>
            Type == UnitActionTypes.PowerFatalDamage || Type == UnitActionTypes.LowFatalDamage || Type == UnitActionTypes.MiddleFatalDamage;

        public bool IsNotFatalDamage =>
            Type == UnitActionTypes.PowerDamage || Type == UnitActionTypes.LowDamage || Type == UnitActionTypes.MiddleDamage;

        public bool DamageImmunity => _damageImmunity;

        [SerializeField] private bool _damageImmunity;
    }
}