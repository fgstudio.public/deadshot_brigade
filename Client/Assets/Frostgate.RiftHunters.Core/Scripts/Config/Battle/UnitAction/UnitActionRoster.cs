using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Battle
{
    [CreateAssetMenu(fileName = nameof(UnitActionRoster),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(UnitActionRoster))]
    public class UnitActionRoster : ScriptableObject
    {
        [field: SerializeField] public UnitActionConfig[] Actions { get; private set; }

        private Dictionary<UnitActionTypes, List<UnitActionConfig>> _cache;

        public bool TryGetAction(UnitActionTypes actionType, out UnitActionConfig unitActionConfig)
        {
            InitCache();

            unitActionConfig = null;

            if (!_cache.ContainsKey(actionType))
                return false;

            unitActionConfig = _cache[actionType].First();
            return true;
        }

        public bool TryGetRandomAction(UnitActionTypes actionType, out UnitActionConfig unitActionConfig)
        {
            InitCache();

            unitActionConfig = null;

            if (!_cache.ContainsKey(actionType))
                return false;

            unitActionConfig = _cache[actionType].GetRandom();
            return true;
        }

        private void InitCache()
        {
            if (_cache != null)
                return;

            _cache = new Dictionary<UnitActionTypes, List<UnitActionConfig>>();

            foreach (var unitActionConfig in Actions)
            {
                if (!_cache.ContainsKey(unitActionConfig.Type))
                    _cache.Add(unitActionConfig.Type, new List<UnitActionConfig>());

                _cache[unitActionConfig.Type].Add(unitActionConfig);
            }
        }
    }
}