using System;
using UnityEngine;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Battle
{
    /// <summary>
    /// Награды, остающиеся после погибшего юнита.
    /// </summary>
    [Serializable]
    public sealed class DeathRewards
    {
        private const int MinExp = 0;
        private const int MaxExp = 1000;

        [field: SerializeField, Range(MinExp, MaxExp)] public int Exp { get; set; }
        [field: SerializeField] public EnergyCapsuleConfig EnergyCapsule { get; set; }

        [SerializeField] private LootSetConfig _lootSet;

        public LootSetConfig LootSet => _lootSet;
    }
}
