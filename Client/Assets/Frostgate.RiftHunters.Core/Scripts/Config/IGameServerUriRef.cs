using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public interface IGameServerUriRef
    {
        [NotNull] Uri Value { get; }
    }
}
