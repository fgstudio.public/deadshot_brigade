using System;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Core.Definitions
{
    [Serializable, InlineProperty]
    public class ViewWithPath<T> where T: Object
    {
        [OnValueChanged(nameof(OnViewSetup))]
        [SerializeField, JsonIgnore, HorizontalGroup("G1"), HideLabel]
        public T View;

        private void OnViewSetup()
        {
#if UNITY_EDITOR
            Path = UnityEditor.AssetDatabase.GetAssetPath(View);
#endif
        }

        [field: SerializeField, HorizontalGroup("G1"), HideLabel, ReadOnly]
        public string Path { get; private set; }
    }
}