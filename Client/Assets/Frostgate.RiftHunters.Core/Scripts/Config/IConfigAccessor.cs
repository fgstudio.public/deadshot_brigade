﻿namespace Frostgate.RiftHunters.Core
{
    public interface IConfigAccessor<out TConfig> where TConfig : IConfig
    {
        TConfig Config { get; }
    }
}
