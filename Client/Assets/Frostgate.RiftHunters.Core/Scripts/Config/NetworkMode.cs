﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core
{
    public enum StageType { Dev, Prod }

    [CreateAssetMenu(menuName = AssetMenus.RiftHunters.Menu + "/" + nameof(NetworkMode),
        fileName = nameof(NetworkMode))]
    public sealed class NetworkMode : ScriptableObject
    {
        [SerializeField] private StageType _stage;

        [SerializeField, HorizontalGroup("ENV"), LabelText("Environment"),
         Tooltip("Environment of Unity Gaming Services")]
        private bool _hasEnvironment = true;

        [SerializeField, HorizontalGroup("ENV"), Required, HideLabel, EnableIf(nameof(HasEnvironment))]
        private string _environment;

        [SerializeField, HorizontalGroup("MK"), PropertyTooltip("Matchmaker IP or hostname")]
        private string _matchmakerAddr;

        [SerializeField, HorizontalGroup("MK", MinWidth = 50, MaxWidth = 75), LabelText(":"), LabelWidth(6)]
        [PropertyTooltip("Matchmaker port")]
        private int _matchmakerPort;


        public Uri MatchmakerUri => _matchmakerUriCache ??= new Uri($"{_matchmakerAddr}:{_matchmakerPort}");
        public bool HasEnvironment => _hasEnvironment;
        public string Environment => _environment;
        public StageType Stage => _stage;

        private Uri _matchmakerUriCache;

        private void OnValidate()
        {
            if (!_hasEnvironment)
                _environment = "Empty";
        }
    }
}