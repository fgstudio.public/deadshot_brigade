﻿using Zenject;

namespace Frostgate.RiftHunters.Core
{
    public sealed class CoreMonoInstaller : MonoInstaller
    {
        public override void InstallBindings() => Container.Install<CoreInstaller>();
    }
}