﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core
{
    public interface IGameStateMachine
    {
        UniTask EnterAsync<TState>() where TState : IState;
        UniTask EnterAsync<TState, TArg>(TArg arg) where TState : IState<TArg>;
        UniTask EnterAsync<TState>(params object[] scopeDependencies) where TState : IState;

        UniTask EnterAsync<TState, TArg>(TArg arg, params object[] scopeDependencies)
            where TState : IState<TArg>;
    }
}