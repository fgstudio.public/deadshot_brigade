﻿using System.Linq;
using System.Collections.Generic;
using System.Threading;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Voice;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.UI.Screens;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Meta.Player;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Core
{
    [UsedImplicitly]
    public sealed class GameMainClientState : IState<ClientConnectionError>
    {
        [NotNull] private readonly IGameStateMachine _stateMachine;
        [NotNull] private readonly PlayerDataService _playerDataService;
        [NotNull] private readonly EquipmentService _equipmentService;
        [NotNull] private readonly HeroUpgradeService _heroUpgradeService;
        [NotNull] private readonly IUIService _uiService;
        [NotNull] private readonly VoiceService _voiceService;
        [NotNull] private readonly InventoryService _inventoryService;
        [NotNull] private readonly IConfigCollection<IMissionConfig> _missionConfigs;
        [NotNull] private readonly CampaignService _campaignService;

        [NotNull] private readonly MissionConfig _firstTutorialConfig;
        [NotNull] private readonly TutorialProgressService _tutorialProgressService;

        private readonly CancellationTokenSource _pollingCts = new();
        private UIMainScreen _mainScreen;

        public GameMainClientState(
            [NotNull] IGameStateMachine stateMachine, [NotNull] PlayerDataService playerDataService,
            [NotNull] EquipmentService equipmentService, [NotNull] HeroUpgradeService heroUpgradeService,
            [NotNull] IUIService uiService,
            [NotNull] VoiceService voiceService,
            [NotNull] InventoryService inventoryService,
            [NotNull] IConfigCollection<IMissionConfig> missionConfigs,
            [NotNull] IFirstTutorial firstTutorialConfig,
            [NotNull] CampaignService campaignService)
        {
            _stateMachine = stateMachine;
            _playerDataService = playerDataService;
            _equipmentService = equipmentService;
            _heroUpgradeService = heroUpgradeService;
            _uiService = uiService;
            _voiceService = voiceService;
            _inventoryService = inventoryService;
            _missionConfigs = missionConfigs;
            _firstTutorialConfig = firstTutorialConfig.FirstTutorialConfig;
            _tutorialProgressService = new TutorialProgressService();
            _campaignService = campaignService;
        }

        public async UniTask EnterAsync(ClientConnectionError error)
        {
            _voiceService.LeaveChannel();

            _uiService.Hide();
            _mainScreen = await _uiService.ShowAsync<UIMainScreen>(true);

            IEnumerable<Hero> heroes = await _inventoryService.GetAllHeroes();
            await _mainScreen.InitializeAsync(heroes, _missionConfigs.ToArray(), _uiService);
            SubscribeOnView();

            // if (!CheckReadyFirstTutorial())
            // {
            //     StartTutorial(_firstTutorialConfig);
            //     return;
            // }

            if (error != ClientConnectionError.None &&
                StaticObject<TimedContainer<ClientConnectionArgs>>.HasValue())
            {
                _mainScreen.ShowErrorModalView(OnReconnectRequested);
            }
        }

        private bool CheckReadyFirstTutorial()
        {
            return _tutorialProgressService.CheckReadyTutorialMission(_firstTutorialConfig.Id);
        }

        private async void StartTutorial(MissionConfig missionConfig)
        {
            await _uiService.ShowAsync<UILoadingScreen>(true);
            UIMainScreen.ScreenData screenData = _mainScreen.GetScreenData();

            //todo: возможно следует доработать получение ClientArg из Меты
            var args = new SelfHostArgs
            {
                ServerArgs = new ServerStartArgs(missionConfig),
                ClientArgs = CreateClientArgs(screenData),
            };

            await _stateMachine.EnterAsync<SelfHostBattleState, SelfHostArgs>(args);
        }

        public UniTask ExitAsync()
        {
            _pollingCts.Cancel();

            UnsubscribeFromView();
            _uiService.Hide();
            _mainScreen = null;

            return UniTask.CompletedTask;
        }

        private void SubscribeOnView()
        {
            _mainScreen.StartServerRequested += OnStartServerRequested;
            _mainScreen.StartSingleplayerRequested += OnStartSingleplayerRequested;
            _mainScreen.StartMultiplayerRequested += OnStartMultiplayerRequested;
            _mainScreen.ReconnectRequested += OnReconnectRequested;
        }

        private void UnsubscribeFromView()
        {
            _mainScreen.StartServerRequested -= OnStartServerRequested;
            _mainScreen.StartSingleplayerRequested -= OnStartSingleplayerRequested;
            _mainScreen.StartMultiplayerRequested -= OnStartMultiplayerRequested;
            _mainScreen.ReconnectRequested -= OnReconnectRequested;
        }

        private async void OnStartServerRequested()
        {
            UIMainScreen.ScreenData data = _mainScreen.GetScreenData();

            var serverStartArgs = new ServerStartArgs(data.MissionConfig);
            await _stateMachine.EnterAsync<ServerBattleState, ServerStartArgs>(serverStartArgs);
        }

        private async void OnStartSingleplayerRequested()
        {
            UIMainScreen.ScreenData data = _mainScreen.GetScreenData();
            await HandleScreenDataAsync(data, false, true);
        }

        private async void OnStartMultiplayerRequested()
        {
            UIMainScreen.ScreenData data = _mainScreen.GetScreenData();
            await HandleScreenDataAsync(data, false);
        }

        private async void OnReconnectRequested()
        {
            var args = StaticObject<TimedContainer<ClientConnectionArgs>>.Instance.Data;
            await _stateMachine.EnterAsync<GameConnectingToMatchState, ClientConnectionArgs>(args);
        }

        private UniTask HandleScreenDataAsync(UIMainScreen.ScreenData screenData, bool isReconnect,
            bool forceSingleplayer = false)
        {
            if (string.IsNullOrWhiteSpace(screenData.ServerAddress) || isReconnect)
            {
                var lobbyData = CreateLobbyData(screenData.Player);
                return StartConnectingAsync(lobbyData, forceSingleplayer);
            }

            return ConnectToDirectServerAsync(screenData);
        }

        private async UniTask StartConnectingAsync(LobbyData lobbyData, bool forceSingleplayer = false)
        {
            var missionConfig = await _campaignService.TakeMission();
            ConnectingToMatchStateArgs args = new(lobbyData, missionConfig) { ForceSingleplayer = forceSingleplayer };

            await _stateMachine.EnterAsync<GameConnectingToMatchState, ConnectingToMatchStateArgs>(args);
        }

        private UniTask ConnectToDirectServerAsync(UIMainScreen.ScreenData screenData)
        {
            ClientConnectionArgs args = CreateClientArgs(screenData);

            return _stateMachine.EnterAsync<GameConnectingToMatchState, ClientConnectionArgs>(args);
        }

        private ClientConnectionArgs CreateClientArgs(UIMainScreen.ScreenData screenData)
        {
            string[] connectionStrings = screenData.ServerAddress.Split(":");

            var lobbyData = CreateLobbyData(screenData.Player);
            IMissionConfig mission = screenData.MissionConfig;

            return new ClientConnectionArgs
            {
                ServerAddress = connectionStrings[0],
                ServerPort = connectionStrings.Length == 2 ? int.Parse(connectionStrings[1]) : null,
                SelectedUnit = lobbyData.SelectedUnit,
                Equipments = lobbyData.Equipments,
                Player = lobbyData.Player,
                HeroLevel = lobbyData.HeroLevel,
                MissionConfig = mission,
            };
        }

        private LobbyData CreateLobbyData(PlayerDto player)
        {
            Hero hero = _playerDataService.GetSelectedHero();
            int heroLevel = _heroUpgradeService.GetHeroLevel(hero.Id);
            Equipment[] equipments = _equipmentService.FindEquippedItems(hero.Id).ToArray();

            return new LobbyData(player, hero.Config, equipments, heroLevel);
        }
    }
}