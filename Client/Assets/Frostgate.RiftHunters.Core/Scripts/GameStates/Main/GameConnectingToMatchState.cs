﻿using System;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.UI.Screens;
using Frostgate.RiftHunters.Core.Voice;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class GameConnectingToMatchState : IState<ConnectingToMatchStateArgs>, IState<ClientConnectionArgs>
    {
        private const string SearchForPlayers = "Search for Players";
        private const string ConnectingToServers = "Connecting to Servers";

        private const int RetryAttemptsLimit = 0;

        [NotNull] private readonly IGameStateMachine _stateMachine;
        [NotNull] private readonly IMatchmakingService _matchmaker;
        [NotNull] private readonly IUIService _uiService;
        [NotNull] private readonly VoiceService _voiceService;
        [NotNull] private readonly ILogger<GameConnectingToMatchState> _logger;

        [CanBeNull] private UILoadingScreen _screen;

        public GameConnectingToMatchState(
            [NotNull] IGameStateMachine stateMachine,
            [NotNull] IMatchmakingService matchmaker,
            [NotNull] IUIService uiService,
            [NotNull] VoiceService voiceService,
            [NotNull] ILogger<GameConnectingToMatchState> logger)
        {
            _stateMachine = stateMachine;
            _matchmaker = matchmaker;
            _uiService = uiService;
            _voiceService = voiceService;
            _logger = logger;
        }

        public async UniTask EnterAsync(ConnectingToMatchStateArgs args)
        {
            await ShowUiAsync().ContinueWith(() => SetUiText(SearchForPlayers));

            SetUiText(SearchForPlayers);

            IMissionConfig missionConfig = args.MissionConfig;
            bool forceSingleplayer = args.ForceSingleplayer;
            if (missionConfig.Type == MissionType.Singleplayer || forceSingleplayer)
                await ConnectToSelfHostBattleAsync(args);
            else
                await ConnectToMatchmakingBattleAsync(args);
        }

        public UniTask EnterAsync(ClientConnectionArgs args) =>
            _stateMachine.EnterAsync<ClientBattleState, ClientConnectionArgs>(args);

        public UniTask ExitAsync() => UniTask.CompletedTask;

        private UniTask ConnectToSelfHostBattleAsync(ConnectingToMatchStateArgs args)
        {
            var selfHostArgs = CreateSelfHostConnectionArgs(args);

            SetUiText(ConnectingToServers);

            return _stateMachine.EnterAsync<SelfHostBattleState, SelfHostArgs>(selfHostArgs);
        }

        private async UniTask ConnectToMatchmakingBattleAsync(ConnectingToMatchStateArgs args)
        {
            IMissionConfig missionConfig = args.MissionConfig;
            UnitConfig unitConfig = args.LobbyData.SelectedUnit;

            (TicketStatus status, MatchAssignment assignment) = await FindMatchAsync(missionConfig, unitConfig);

            if (status != TicketStatus.Found)
            {
                _logger.LogInfo("Matchmaking failed. Creating self host battle.");
                await ConnectToSelfHostBattleAsync(args);
            }
            else
            {
                SetUiText(ConnectingToServers);

                ClientConnectionArgs clientConnArgs = CreateMatchmakingConnectionArgs(args, assignment);

                // TODO: Перенести логин вивокса в AuthState или PostAuthState,
                // Использовать только Id игрока, убрать аргумент из метода LoginAsync
                _voiceService.LoginAsync(args.LobbyData.Player.Name);
                await EnterAsync(clientConnArgs);
            }
        }

        private async UniTask<(TicketStatus, MatchAssignment)> FindMatchAsync(IMissionConfig missionConfig,
            UnitConfig unitConfig)
        {
            ITicketOptions ticketOptions = new TicketOptions(missionConfig, unitConfig);

            int retryAttemptsCounter = 1;
            ITicket ticket = await _matchmaker.CreateTicketAsync(ticketOptions);
            ITicketData ticketData = await _matchmaker.GetTickedDataWithResultingStatusAsync(ticket, 1500);

            while (retryAttemptsCounter <= RetryAttemptsLimit &&
                   (string.IsNullOrEmpty(ticket.Id) || ticketData.Status != TicketStatus.Found))
            {
                _logger.LogInfo($"Matchmaking attempt '{retryAttemptsCounter}' failed. Retrying.");
                
                retryAttemptsCounter++;

                ticket = await _matchmaker.CreateTicketAsync(ticketOptions);
                ticketData = await _matchmaker.GetTickedDataWithResultingStatusAsync(ticket, 1500);
            }

            return (ticketData.Status, ticketData.Assignment);
        }

        private async UniTask ShowUiAsync()
        {
            _screen = await _uiService.ShowAsync<UILoadingScreen>(true);
        }

        private void SetUiText(string text) => _screen!.Text = text;

        private SelfHostArgs CreateSelfHostConnectionArgs(ConnectingToMatchStateArgs args)
        {
            LobbyData lobbyData = args.LobbyData;

            var clientArgs = new ClientConnectionArgs
            {
                SelectedUnit = lobbyData.SelectedUnit,
                Equipments = lobbyData.Equipments,
                Player = lobbyData.Player,
                HeroLevel = lobbyData.HeroLevel,
                MatchMakerMode = false,
                MatchId = Guid.NewGuid().ToString(),
                MissionConfig = args.MissionConfig
            };

            var matchPlayer = new MatchPlayer(lobbyData.Player.Id, lobbyData.SelectedUnit);
            var serverArgs = new ServerStartArgs(args.MissionConfig, new[] { matchPlayer });

            return new SelfHostArgs
            {
                ClientArgs = clientArgs,
                ServerArgs = serverArgs,
            };
        }

        private ClientConnectionArgs CreateMatchmakingConnectionArgs(ConnectingToMatchStateArgs args,
            MatchAssignment assignment)
        {
            var clientArgs = new ClientConnectionArgs
            {
                ServerAddress = assignment.ServerIp,
                ServerPort = assignment.ServerPort,
                SelectedUnit = args.LobbyData.SelectedUnit,
                Equipments = args.LobbyData.Equipments,
                Player = args.LobbyData.Player,
                HeroLevel = args.LobbyData.HeroLevel,
                MatchMakerMode = true,
                MatchId = assignment.MatchId,
                MissionConfig = args.MissionConfig
            };

            return clientArgs;
        }
    }

    public sealed class ConnectingToMatchStateArgs
    {
        public LobbyData LobbyData { get; }
        public IMissionConfig MissionConfig { get; }
        public bool ForceSingleplayer { get; set; }

        public ConnectingToMatchStateArgs(LobbyData lobbyData, IMissionConfig missionConfig)
        {
            LobbyData = lobbyData;
            MissionConfig = missionConfig;
        }
    }
}