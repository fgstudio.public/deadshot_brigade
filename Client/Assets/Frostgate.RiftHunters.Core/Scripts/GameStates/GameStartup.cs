﻿using System;
using JetBrains.Annotations;
using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Core
{
    [UsedImplicitly]
    public static class GameStartup
    {
        private const int TargetFrameRate = 60; // TODO: Тут этому не место, имеет место быть сделать in-game settings

        private static readonly Lazy<IGameStateMachine> StateMachineHandle =
            new(ProjectContext.Instance.Container.Resolve<IGameStateMachine>);

        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            Application.targetFrameRate = TargetFrameRate;

            Application.quitting += Shutdown;
            Boot();
        }

        private static async void Boot() => await StateMachineHandle.Value.EnterAsync<GameBootState>();
        private static async void Shutdown() => await StateMachineHandle.Value.EnterAsync<GameShutdownState>();
    }
}