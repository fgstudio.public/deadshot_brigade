﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Voice;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class GameClientShutdownState : IState
    {
        [NotNull] private readonly VoiceService _voiceService;

        [UsedImplicitly]
        public GameClientShutdownState([NotNull] VoiceService voiceService)
        {
            _voiceService = voiceService;
        }

        public UniTask EnterAsync() => DeinitializeAsync();

        public UniTask ExitAsync() => UniTask.CompletedTask;

        // Деинициализация  компонентов/сервисов для клиента
        private UniTask DeinitializeAsync() => _voiceService.DeinitializeAsync();
    }
}