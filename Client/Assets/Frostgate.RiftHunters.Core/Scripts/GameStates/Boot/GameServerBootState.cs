﻿using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class GameServerBootState : IState
    {
        [NotNull] private readonly IGameStateMachine _stateMachine;
        [NotNull] private readonly ILogger<GameServerBootState> _logger;

        [UsedImplicitly]
        public GameServerBootState([NotNull] IGameStateMachine stateMachine,
            [NotNull] ILogger<GameServerBootState> logger)
        {
            _stateMachine = stateMachine;
            _logger = logger;
        }

        public async UniTask EnterAsync()
        {
            _logger.LogInfo("Server starting up.");

            await InitializeAsync();

            await _stateMachine.EnterAsync<ServerBattleState>();
        }

        public UniTask ExitAsync() => UniTask.CompletedTask;

        // Инициализация  компонентов/сервисов для сервера
        private UniTask InitializeAsync() => UniTask.CompletedTask;
    }
}