﻿using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    [UsedImplicitly]
    public sealed class GameServerShutdownState : IState
    {
        public UniTask EnterAsync() => DeinitializeAsync();
        public UniTask ExitAsync() => UniTask.CompletedTask;

        // Деинициализация компонентов/сервисов для сервера
        private UniTask DeinitializeAsync() => UniTask.CompletedTask;
    }
}