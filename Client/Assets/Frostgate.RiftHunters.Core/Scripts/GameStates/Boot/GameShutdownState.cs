﻿using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class GameShutdownState : IState
    {
        [NotNull] private readonly IGameStateMachine _stateMachine;

        [UsedImplicitly]
        public GameShutdownState([NotNull] IGameStateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        public UniTask EnterAsync()
        {
#if UNITY_SERVER && !UNITY_EDITOR
            return _stateMachine.EnterAsync<GameServerShutdownState>()
                .ContinueWith(DeinitializeAsync);
#else
            return _stateMachine.EnterAsync<GameClientShutdownState>()
                .ContinueWith(DeinitializeAsync);
#endif
        }

        // Деинициализация общих компонентов/сервисов для клиента и сервера
        private UniTask DeinitializeAsync() => UniTask.CompletedTask;

        public UniTask ExitAsync() => UniTask.CompletedTask;
    }
}