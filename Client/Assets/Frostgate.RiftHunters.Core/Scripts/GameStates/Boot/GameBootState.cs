﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Infrastructure.Definitions;
using JetBrains.Annotations;


namespace Frostgate.RiftHunters.Core
{
    public sealed class GameBootState : IState
    {
        [NotNull] private readonly IGameStateMachine _stateMachine;
        [NotNull] private readonly IConfigRepository _configRepository;
        [NotNull] private readonly IGameServicesCore _gameServices;

        [UsedImplicitly]
        public GameBootState(
            [NotNull] IGameStateMachine stateMachine,
            [NotNull] IConfigRepository configRepository,
            [NotNull] IGameServicesCore gameServices)
        {
            _stateMachine = stateMachine;
            _configRepository = configRepository;
            _gameServices = gameServices;
        }

        public async UniTask EnterAsync()
        {
            await InitializeAsync();

#if UNITY_SERVER && !UNITY_EDITOR
            await _stateMachine.EnterAsync<GameServerBootState>();
#else
            await _stateMachine.EnterAsync<GameClientBootState>();
#endif
        }

        // Инициализация общих компонентов/сервисов для клиента и сервера
        private async UniTask InitializeAsync()
        {
            await _configRepository.InitializeAsync();
            await _gameServices.InitializeAsync();
        }

        public UniTask ExitAsync() => UniTask.CompletedTask;
    }
}