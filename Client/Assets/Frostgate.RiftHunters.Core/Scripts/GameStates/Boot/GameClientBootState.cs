﻿using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Infrastructure.Services.Facebook;
using Frostgate.RiftHunters.Infrastructure.Services.Analytics.AppsFlyer;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public sealed class GameClientBootState : IState
    {
        [NotNull] private readonly IGameStateMachine _stateMachine;
        [NotNull] private readonly FacebookService _facebookService;
        [NotNull] private readonly AppsFlyerService _appsFlyerService;

        [UsedImplicitly]
        public GameClientBootState([NotNull] IGameStateMachine stateMachine, [NotNull] FacebookService facebookService,
            [NotNull] AppsFlyerService appsFlyerService)
        {
            _stateMachine = stateMachine;
            _facebookService = facebookService;
            _appsFlyerService = appsFlyerService;
        }

        public async UniTask EnterAsync()
        {
            await InitializeAsync();
            await _stateMachine.EnterAsync<GameClientAuthState>();
        }

        // Инициализация  компонентов/сервисов для клиента
        private async UniTask InitializeAsync()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            await _facebookService.InitializeAsync();
            await _appsFlyerService.InitializeAsync();
        }

        public UniTask ExitAsync() => UniTask.CompletedTask;
    }
}