﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Services.Auth;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.UI.Screens.Auth;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public class GameClientAuthInProgressState : IState<AuthType>
    {
        [NotNull] private readonly IUIService _uiService;
        [NotNull] private readonly IGameStateMachine _gameStateMachine;
        [NotNull] private readonly IAuthServiceFacade _authServiceFacade;

        public GameClientAuthInProgressState(
            [NotNull] IUIService uiService,
            [NotNull] IGameStateMachine gameStateMachine,
            [NotNull] IAuthServiceFacade authServiceFacade)
        {
            _uiService = uiService;
            _gameStateMachine = gameStateMachine;
            _authServiceFacade = authServiceFacade;
        }

        public async UniTask EnterAsync(AuthType authType)
        {
            await _uiService.ShowAsync<UIAuthInProgressScreen>();

            var authResult = await _authServiceFacade.SignInAsync(authType);

            await ProcessAuthResult(authResult);
        }

        public UniTask ExitAsync()
        {
            _uiService.Hide();
            return UniTask.CompletedTask;
        }

        private async UniTask ProcessAuthResult(AuthResult authResult)
        {
            if (authResult.IsSuccess)
                await _gameStateMachine.EnterAsync<GamePostAuthInitializationClientState>();
            else
                await _gameStateMachine.EnterAsync<GameClientAuthFailedState>();
        }
    }
}