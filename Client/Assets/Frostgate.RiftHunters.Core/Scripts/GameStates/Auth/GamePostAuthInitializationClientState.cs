﻿using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Voice;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Player;
using Frostgate.RiftHunters.Meta.Processing;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Core
{
    public sealed class GamePostAuthInitializationClientState : IState
    {
        [NotNull] private readonly IGameStateMachine _stateMachine;

        [NotNull] private readonly IEconomyApi _economyApi;
        [NotNull] private readonly ISaveService _saveService;
        [NotNull] private readonly VoiceService _voiceService;

        [NotNull] private readonly EquipmentService _equipmentService;
        [NotNull] private readonly PlayerDataService _playerDataService;
        [NotNull] private readonly CampaignService _campaignService;
        [NotNull] private readonly HeroUpgradeService _heroUpgradeService;

        [NotNull] private readonly PlayerDataInitProcessor _playerDataInitProcessor;
        [NotNull] private readonly CampaignDataInitProcessor _campaignDataInitProcessor;
        [NotNull] private readonly HeroesUpgradeDataInitProcessor _heroesUpgradeDataInitProcessor;
        [NotNull] private readonly HeroesEquipmentDataInitProcessor _heroesEquipmentDataInitProcessor;

        [NotNull] private readonly DefaultInventoryProcessor _defaultInventoryPreprocessor;

        [UsedImplicitly]
        public GamePostAuthInitializationClientState(
            [NotNull] IGameStateMachine stateMachine,

            [NotNull] IEconomyApi economyApi,
            [NotNull] ISaveService saveService,
            [NotNull] VoiceService voiceService,

            [NotNull] EquipmentService equipmentService,
            [NotNull] PlayerDataService playerDataService,
            [NotNull] CampaignService campaignService,
            [NotNull] HeroUpgradeService heroUpgradeService,

            [NotNull] PlayerDataInitProcessor playerDataInitProcessor,
            [NotNull] CampaignDataInitProcessor campaignDataInitProcessor,
            [NotNull] HeroesUpgradeDataInitProcessor heroesUpgradeDataInitProcessor,
            [NotNull] HeroesEquipmentDataInitProcessor heroesEquipmentDataInitProcessor,

            [NotNull] DefaultInventoryProcessor defaultInventoryPreprocessor)
        {
            _stateMachine = stateMachine;

            _economyApi = economyApi;
            _saveService = saveService;
            _voiceService = voiceService;

            _equipmentService = equipmentService;
            _playerDataService = playerDataService;
            _campaignService = campaignService;
            _heroUpgradeService = heroUpgradeService;

            _playerDataInitProcessor = playerDataInitProcessor;
            _campaignDataInitProcessor = campaignDataInitProcessor;
            _heroesUpgradeDataInitProcessor = heroesUpgradeDataInitProcessor;
            _heroesEquipmentDataInitProcessor = heroesEquipmentDataInitProcessor;

            _defaultInventoryPreprocessor = defaultInventoryPreprocessor;
        }

        public async UniTask EnterAsync()
        {
            // порядок важен
            await InitializeLowLevelServices();

            await ExecuteDataInitProcessors();
            await ExecuteDefaultEconomyProcessors();

            await InitializeHighLevelServices();

            await _stateMachine.EnterAsync<GameLoadMainSceneState, ClientConnectionError>(ClientConnectionError.None);
        }

        public UniTask ExitAsync() => UniTask.CompletedTask;

        private async UniTask InitializeLowLevelServices()
        {
            await _economyApi.InitializeAsync();
            await _saveService.InitializeAsync();
            await _voiceService.InitializeAsync();
        }

        private async UniTask ExecuteDataInitProcessors()
        {
            await _playerDataInitProcessor.ExecuteAsync();
            await _campaignDataInitProcessor.ExecuteAsync();
            await _heroesUpgradeDataInitProcessor.ExecuteAsync();
            await _heroesEquipmentDataInitProcessor.ExecuteAsync();
        }

        private async UniTask ExecuteDefaultEconomyProcessors()
        {
            await _defaultInventoryPreprocessor.ExecuteAsync();
        }

        private async UniTask InitializeHighLevelServices()
        {
            await _equipmentService.InitializeAsync();
            await _playerDataService.InitializeAsync();
            await _campaignService.InitializeAsync();
            await _heroUpgradeService.InitializeAsync();
        }
    }
}