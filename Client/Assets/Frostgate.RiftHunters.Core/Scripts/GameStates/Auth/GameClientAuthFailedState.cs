﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.UI.Screens.Auth;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public class GameClientAuthFailedState : IState
    {
        [NotNull] private readonly IUIService _uiService;
        [NotNull] private readonly IGameStateMachine _gameStateMachine;

        public GameClientAuthFailedState([NotNull] IUIService uiService, [NotNull] IGameStateMachine gameStateMachine)
        {
            _uiService = uiService;
            _gameStateMachine = gameStateMachine;
        }

        public async UniTask EnterAsync()
        {
            var screen = await _uiService.ShowAsync<UIAuthFailedScreen>();

            await screen.WaitForConfirm();

            _gameStateMachine.EnterAsync<GameClientAuthState>();
        }

        public UniTask ExitAsync()
        {
            _uiService.Hide();
            return UniTask.CompletedTask;
        }
    }
}