﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Services.Auth;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.UI.Screens.Auth;
using Frostgate.RiftHunters.Infrastructure;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class GameClientAuthState : IState
    {
        [NotNull] private readonly IGameStateMachine _stateMachine;
        [NotNull] private readonly IUIService _uiService;
        [NotNull] private readonly Preferences _preferences;

        [UsedImplicitly]
        public GameClientAuthState(
            [NotNull] IGameStateMachine stateMachine,
            [NotNull] IUIService uiService,
            [NotNull] Preferences preferences)
        {
            _stateMachine = stateMachine;
            _uiService = uiService;
            _preferences = preferences;
        }

        public async UniTask EnterAsync()
        {
            if (!_preferences.NetworkMode.HasEnvironment)
            {
                await _stateMachine.EnterAsync<GamePostAuthInitializationClientState>();
                return;
            }

            var authScreen = await _uiService.ShowAsync<UIAuthScreen>();
            var authType = await authScreen.GetAuthTypeAsync();

            await _stateMachine.EnterAsync<GameClientAuthInProgressState, AuthType>(authType);
        }

        public UniTask ExitAsync() => UniTask.CompletedTask;
    }
}