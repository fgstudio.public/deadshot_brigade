﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core
{
    public interface IState : IExitableState
    {
        UniTask EnterAsync();
    }

    public interface IState<in TArg> : IExitableState
    {
        UniTask EnterAsync(TArg arg);
    }
}
