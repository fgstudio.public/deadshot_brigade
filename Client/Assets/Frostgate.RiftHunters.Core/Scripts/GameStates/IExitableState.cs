﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core
{
    public interface IExitableState
    {
        UniTask ExitAsync();
    }
}
