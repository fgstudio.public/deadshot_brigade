﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.UI.Screens;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class GameLoadMainSceneState : IState<ClientConnectionError>
    {
        private readonly IGameStateMachine _stateMachine;
        private readonly IUIService _uiService;
        private readonly ISceneLoader _sceneLoader;
        [NotNull] private readonly IConfigAccessor<GamePlayScenesRoster> _scenesRoster;

        public GameLoadMainSceneState([NotNull] IGameStateMachine stateMachine, [NotNull] IUIService uiService,
            [NotNull] ISceneLoader sceneLoader,
            [NotNull] IConfigAccessor<GamePlayScenesRoster> scenesRoster)
        {
            _stateMachine = stateMachine;
            _uiService = uiService;
            _sceneLoader = sceneLoader;
            _scenesRoster = scenesRoster;
        }

        public async UniTask EnterAsync(ClientConnectionError error)
        {
            await _uiService.ShowAsync<UILoadingScreen>(true);
            await _sceneLoader.LoadSceneAsync(_scenesRoster.Config.MainScene);
            
            await _stateMachine.EnterAsync<GameMainClientState, ClientConnectionError>(error);
        }

        public UniTask ExitAsync()
        {
            _uiService.Hide();
            return UniTask.CompletedTask;
        }
    }
}