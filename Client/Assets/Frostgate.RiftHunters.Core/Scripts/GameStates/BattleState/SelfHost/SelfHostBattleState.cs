using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Client;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.UI.Screens;
using Frostgate.RiftHunters.Meta;
using UnityEngine.SceneManagement;

namespace Frostgate.RiftHunters.Core
{
    public class SelfHostBattleState : BattleState, IState<SelfHostArgs>
    {
        private ServerBattleLogic _server;
        private ClientBattleLogic _client;
        private readonly IUIService _uiService;
        private readonly ISceneLoader _sceneLoader;
        private SelfHostAuthenticator _authenticator;
         private readonly CampaignService _campaignService;

        public SelfHostBattleState(
            IUIService uiService,
            IPreferences preferences,
            ISceneLoader sceneLoader,
            CampaignService campaignService
        ) : base(preferences.BattleComponentRoster)
        {
            _uiService = uiService;
            _sceneLoader = sceneLoader;
            _campaignService = campaignService;
        }

        public async UniTask EnterAsync(SelfHostArgs args)
        {
            await _uiService.ShowAsync<UILoadingScreen>(true);

            IMissionConfig missionConfig = args.ServerArgs.MissionConfig;
            
            NetworkEnter();

            await _sceneLoader.LoadMissionAsync(missionConfig);

            
            
            _client = new ClientBattleLogic(args.ClientArgs, NetworkController, BattleContainer, BattleComponentRoster, 
                missionConfig, _campaignService);

            //TODO: в этот момент активны 2 камеры до того как загрузится следующая сцена
            //TODO: камера нужна объектам которые сейчас спаунятся общим списком со сценой. Поэтому камеру раньше сцены инитим так как я её со сцены убрал. Далее и остальные системы надо переносить и делать свои инсталлеры завизимостей.
            BattleCamera battleCamera = _client.InitCamera();
            
            InstallBindings();
            BattleContainer.Inject(battleCamera);

            ServerStartArgs serverArgs = args.ServerArgs;
            _server = new ServerBattleLogic(NetworkController, BattleContainer, missionConfig, serverArgs.MatchPlayers);
            _server.Init();

            _authenticator = NetworkController.ReplaceAuthenticator<SelfHostAuthenticator>();
            _authenticator.Init(args.ServerArgs.MissionConfig);
            _authenticator.OnClientAuthenticated.AddListener(() => { _client.Init(); });

            NetworkController.StartHost();

            _uiService.Hide();
        }

        public UniTask ExitAsync()
        {
            _client.Dispose();
            _server.Dispose();

            NetworkController.StopHost();

            NetworkExit();
            return UniTask.CompletedTask;
        }
    }
}