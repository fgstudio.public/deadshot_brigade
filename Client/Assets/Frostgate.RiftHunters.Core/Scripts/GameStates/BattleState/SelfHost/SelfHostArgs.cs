namespace Frostgate.RiftHunters.Core
{
    public class SelfHostArgs
    {
        public ClientConnectionArgs ClientArgs;
        public ServerStartArgs ServerArgs;
    }
}