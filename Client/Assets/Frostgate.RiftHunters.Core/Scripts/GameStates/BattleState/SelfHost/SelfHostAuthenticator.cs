using System;
using Mirror;

namespace Frostgate.RiftHunters.Core
{
    public class SelfHostAuthenticator : ServerAuthenticator
    {
        public override void OnStartClient() =>
            NetworkClient.RegisterHandler<AuthResponse>(AuthResponseHandler, false);

        public override void OnStopClient() =>
            NetworkClient.UnregisterHandler<AuthResponse>();

        private void AuthResponseHandler(AuthResponse message)
        {
            UnityEngine.Debug.Log($"{nameof(AuthResponseHandler)} result:");

            switch (message.Result)
            {
                case ClientConnectionResult.Successful:
                    UnityEngine.Debug.Log(message.Result);
                    ClientAccept();
                    break;

                case ClientConnectionResult.DeclinedByPlayersCount:
                    UnityEngine.Debug.LogError(message.Result);
                    ClientReject();
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}