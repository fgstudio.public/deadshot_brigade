using System;
using Frostgate.RiftHunters.Core.Battle.Client.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Systems.Pool;
using kcp2k;
using Mirror;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public class NetworkController : NetworkManager
    {
        private const string TransportScheme = "kcp";

        public event Action OnStartServerEvent;
        public event Action OnStopServerEvent;
        public event Action<NetworkConnectionToClient> OnServerConnectEvent;
        public event Action<NetworkConnectionToClient> OnServerDisconnectEvent;
        public event Action<NetworkConnectionToClient> OnServerReadyEvent;

        public event Action OnClientConnectEvent;
        public event Action OnClientDisconnectEvent;

        private KcpTransport Transport => (KcpTransport)transport;

        [SerializeField, Min(0)] public float _playerWinFreeMoveTime = 10;
        [SerializeField, Required] public UnitNetwork _unitNetworkPrefab;
        [SerializeField, Required] public PrefabPoolConfig<LootBoxPrefabRoster, PathPoolConfig> _lootBox;
        [SerializeField, Required] public PrefabPoolConfig<TrapPrefabRoster, PathPoolConfig> _trap;
        [SerializeField, Required] public PrefabPoolConfig<MinePrefabRoster, PathPoolConfig> _mine;
        [SerializeField, Required] public PrefabPoolConfig<TotemPrefabRoster, PathPoolConfig> _totem;
        [SerializeField] public NetworkIdentity[] _spawnablePrefabs;

        [SerializeField, Required, AssetSelector]
        public ActivityRelatedViewConfigRoster ActivityViewConfigRoster;

        public void StartClient(string serverAddr, int? serverPort)
        {
            var uriBuilder = new UriBuilder(TransportScheme, serverAddr);
            if (serverPort.HasValue)
                uriBuilder.Port = serverPort.Value;
            
            StartClient(uriBuilder.Uri);
        }

        public void StartServer(int? port)
        {
            if (port.HasValue)
                Transport.Port = (ushort)port.Value;
            
            StartServer();
        }
        
        public override void OnStartServer()
        {
            OnStartServerEvent?.Invoke();
            UnityEngine.Debug.Log($"{nameof(OnStartServer)} {Application.version}");
            base.OnStartServer();
        }

        public override void OnStopServer()
        {
            OnStopServerEvent?.Invoke();
            UnityEngine.Debug.Log($"{nameof(OnStopServer)}");
            base.OnStopServer();
        }

        public override void OnServerConnect(NetworkConnectionToClient client)
        {
            OnServerConnectEvent?.Invoke(client);
            UnityEngine.Debug.Log($"{nameof(OnServerConnect)}");
            base.OnServerConnect(client);
        }

        // WARNING: Вызывается без OnServerConnect/OnServerReady при Authenticator.ServerReject
        public override void OnServerDisconnect(NetworkConnectionToClient client)
        {
            OnServerDisconnectEvent?.Invoke(client);
            UnityEngine.Debug.Log($"{nameof(OnServerDisconnect)}");
        }

        /// <summary>Called on the server when a client is ready (= loaded the scene)</summary>
        public override void OnServerReady(NetworkConnectionToClient client)
        {
            OnServerReadyEvent?.Invoke(client);
            UnityEngine.Debug.Log($"{nameof(OnServerReady)}");
            base.OnServerReady(client);
        }

        public override void OnClientConnect()
        {
            OnClientConnectEvent?.Invoke();
            UnityEngine.Debug.Log($"{nameof(OnClientConnect)}");
            base.OnClientConnect();
        }

        public override void OnClientDisconnect()
        {
            OnClientDisconnectEvent?.Invoke();
            UnityEngine.Debug.Log($"{nameof(OnClientDisconnect)}");
            base.OnClientDisconnect();
        }

        public override void OnClientError(Exception exception)
        {
            UnityEngine.Debug.Log($"{nameof(OnClientError)} {exception}");
        }

        public override void OnClientNotReady()
        {
            UnityEngine.Debug.Log($"{nameof(OnClientNotReady)}");
        }

        public override void OnStartClient()
        {
            UnityEngine.Debug.Log($"{nameof(OnStartClient)}");
        }

        public T ReplaceAuthenticator<T>() where T : NetworkAuthenticator
        {
            var newAuthenticator = gameObject.AddComponent<T>();

            if (authenticator != null)
                Destroy(authenticator);

            authenticator = newAuthenticator;
            return newAuthenticator;
        }
    }
}