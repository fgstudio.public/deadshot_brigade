using System;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Systems.Pool;
using JetBrains.Annotations;
using Mirror;
using Zenject;

namespace Frostgate.RiftHunters.Core
{
    public abstract class BattleLogic : IDisposable
    {
        protected readonly NetworkController NetworkController;
        [NotNull] protected readonly IMissionConfig MissionConfig;

        // TODO: спаун всех объектов надо переделывать. Очень много кода дублируется на каждый тип объекта.
        protected UnitNetwork _unitNetworkPrefab => NetworkController._unitNetworkPrefab;
        protected PrefabPoolConfig<LootBoxPrefabRoster, PathPoolConfig> _lootBox => NetworkController._lootBox;
        protected PrefabPoolConfig<TrapPrefabRoster, PathPoolConfig> _trap => NetworkController._trap;
        protected PrefabPoolConfig<MinePrefabRoster, PathPoolConfig> _mine => NetworkController._mine;
        protected PrefabPoolConfig<TotemPrefabRoster, PathPoolConfig> _totem => NetworkController._totem;

        protected NetworkIdentity[] _spawnablePrefabs => NetworkController._spawnablePrefabs;

        protected NetworkDependencies NetworkDependencies;
        protected DiContainer DiContainer;

        protected BattleLogic(NetworkController networkController, DiContainer container,
            [NotNull] IMissionConfig missionConfig)
        {
            NetworkController = networkController;
            DiContainer = container;
            MissionConfig = missionConfig;
        }

        protected void Resolve()
        {
            NetworkDependencies = DiContainer.Resolve<NetworkDependencies>();
            InitConfigSynchronization();
        }

        private void InitConfigSynchronization()
        {
            NetworkConfigSync.Init(
                NetworkDependencies.Shared.Equipments,
                NetworkDependencies.Shared.UnitConfigs,
                NetworkDependencies.Shared.UnitEffectConfigs,
                NetworkDependencies.Shared.UnitActionConfigs,
                NetworkDependencies.Shared.UnitEmotionConfigs);

            ActivityConfigSynchronization.Init(NetworkDependencies.Shared.ActivityConfigs);
        }

        public abstract void Dispose();
    }
}