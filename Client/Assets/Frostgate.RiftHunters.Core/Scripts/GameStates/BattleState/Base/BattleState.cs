using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using Zenject;
using Zenject.Internal;

namespace Frostgate.RiftHunters.Core
{
    public abstract class BattleState
    {
        protected readonly BattleComponentRoster BattleComponentRoster;
        protected NetworkController NetworkController;
        protected DiContainer BattleContainer { get; }

        protected BattleState(BattleComponentRoster battleComponentRoster)
        {
            BattleComponentRoster = battleComponentRoster;
            BattleContainer = ProjectContext.Instance.Container.CreateSubContainer();
        }

        protected void NetworkEnter()
        {
            NetworkController = Object.Instantiate(BattleComponentRoster.NetworkControllerPrefab);
            Object.DontDestroyOnLoad(NetworkController.gameObject);
        }

        protected void NetworkExit()
        {
            Object.Destroy(NetworkController.gameObject);
        }

        protected void InstallBindings()
        {
            var parent = new GameObject(nameof(BattleContainer));
            BattleContainer.DefaultParent = parent.transform;

            // TODO: создание контейнера убрано из SceneContext компонента. Теперь это просто набр ссылок на инсталлеры. Надо из него потихоньку вытаскивать элементы и спаунить по отдельности.
            var sceneContext = Object.FindObjectOfType<SceneContext>();
            // TODO: Попробовать Run и AutoRun

            foreach (var installer in sceneContext.Installers)
            {
                BattleContainer.Inject(installer);
                installer.InstallBindings();
            }

            foreach (var installer in sceneContext.ScriptableObjectInstallers)
            {
                BattleContainer.Inject(installer);
                installer.InstallBindings();
            }

            var sceneInjectableList = new List<MonoBehaviour>();
            var battleScene = sceneContext.gameObject.scene;
            ZenUtilInternal.GetInjectableMonoBehavioursInScene(battleScene, sceneInjectableList);

            foreach (var injectable in sceneInjectableList)
                BattleContainer.Inject(injectable);
        }
    }
}