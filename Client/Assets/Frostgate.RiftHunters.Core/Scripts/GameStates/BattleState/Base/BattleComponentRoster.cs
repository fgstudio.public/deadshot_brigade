using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Client;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    [CreateAssetMenu(
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(BattleComponentRoster),
        fileName = nameof(BattleComponentRoster))]
    public class BattleComponentRoster : ScriptableObject
    {
        [field: SerializeField] public NetworkController NetworkControllerPrefab { get; private set; }

        //TODO: для клиентских компонентов стоит завести отдельный ростер
        [field: SerializeField] public BattleCamera BattleCamera { get; private set; }

        [field: SerializeField] public UnitViewVFX SelectedUnitViewVFX { get; private set; }
    }
}