using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle;
using JetBrains.Annotations;
using Mirror;

namespace Frostgate.RiftHunters.Core
{
    public class ServerBattleState : BattleState, IState, IState<ServerStartArgs>
    {
        [NotNull] private readonly ISceneLoader _sceneLoader;
        [NotNull] private readonly IServerDataQueryService _queryService;
        [NotNull] private readonly IServerAllocationService _allocationService;
        [NotNull] private readonly IServerConfigurationProvider _configurationProvider;
        [NotNull] private readonly ILogger<ServerBattleState> _logger;

        private ServerBattleLogic _server;
        private ServerAuthenticator _authenticator;

        public ServerBattleState(
            [NotNull] IPreferences preferences,
            [NotNull] ISceneLoader sceneLoader,
            [NotNull] IServerDataQueryService queryService,
            [NotNull] IServerAllocationService allocationService,
            [NotNull] IServerConfigurationProvider configurationProvider,
            [NotNull] ILogger<ServerBattleState> logger
        ) : base(preferences.BattleComponentRoster)
        {
            _sceneLoader = sceneLoader;
            _queryService = queryService;
            _allocationService = allocationService;
            _configurationProvider = configurationProvider;
            _logger = logger;
        }

        public async UniTask EnterAsync()
        {
            _logger.LogInfo("Starting as Dedicated Game Server");

            await _queryService.StartQueryingAsync();

            // TODO: Желательно реализовать механизм, подобный в Gung.
            // Сервер запускается и слушает подключения игроков, после набора целевого количества или по таймауту,
            // снять плашку загрузки на клиенте и заспавнить юнита.
            IAllocationData allocationData = await _allocationService.AwaitAllocationAsync();
            IMissionConfig mission = allocationData.Mission;
            IReadOnlyList<MatchPlayer> players = allocationData.Players;

            var startArgs = new ServerStartArgs(mission, players, _configurationProvider.Port);
            await EnterAsync(startArgs);

            NetworkController.OnServerConnectEvent += OnPlayersNumberChanged;
            NetworkController.OnServerDisconnectEvent += OnPlayersNumberChanged;

            _queryService.CurrentMission = mission;
        }

        public async UniTask EnterAsync(ServerStartArgs args)
        {
            NetworkEnter();

            IMissionConfig missionConfig = args.MissionConfig;
            IReadOnlyList<MatchPlayer> matchPlayers = args.MatchPlayers;
            
            await _sceneLoader.LoadMissionAsync(missionConfig);

            InstallBindings();

            _server = new ServerBattleLogic(NetworkController, BattleContainer, missionConfig, matchPlayers);
            _server.Init();

            _authenticator = NetworkController.ReplaceAuthenticator<ServerAuthenticator>();
            _authenticator.Init(missionConfig);

            NetworkController.StartServer(args.Port);
        }

        public UniTask ExitAsync()
        {
            _logger.LogInfo($"Exit from {nameof(ServerBattleState)}.");

            _server.Dispose();

            NetworkController.OnServerConnectEvent -= OnPlayersNumberChanged;
            NetworkController.OnServerDisconnectEvent -= OnPlayersNumberChanged;

            NetworkController.StopServer();
            NetworkExit();

            _queryService.StopQuerying();

            return UniTask.CompletedTask;
        }

        private void OnPlayersNumberChanged(NetworkConnectionToClient _)
        {
            _queryService.CurrentPlayersNumber = NetworkServer.connections.Count;
        }
    }
}