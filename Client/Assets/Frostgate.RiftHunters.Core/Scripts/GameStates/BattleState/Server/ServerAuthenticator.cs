using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle;
using Mirror;

namespace Frostgate.RiftHunters.Core
{
    public readonly struct AuthResponse : NetworkMessage
    {
        public readonly ClientConnectionResult Result;
        public readonly string MissionId;

        public AuthResponse(ClientConnectionResult result, string missionId)
        {
            Result = result;
            MissionId = missionId;
        }
    }

    public enum ClientConnectionResult
    {
        Successful = 0,
        DeclinedByPlayersCount = 1,
        DeclinedByClientTimeout = 2
    }

    public class ServerAuthenticator : NetworkAuthenticator
    {
        private int MaxPlayers => _missionConfig.MaxPlayersNumber;
        
        private IMissionConfig _missionConfig;

        private readonly HashSet<NetworkConnection> _connectionsPendingDisconnect = new();
        private readonly HashSet<NetworkConnection> _successfulConnections = new();
        private NetworkController _networkController;

        public void Init(IMissionConfig missionConfig)
        {
            _missionConfig = missionConfig;
        }

        private void Awake() 
        {
            _networkController = GetComponent<NetworkController>();

            if (_networkController != null)
                _networkController.OnServerDisconnectEvent += OnDisconnected;
            else
                UnityEngine.Debug.LogError($"{nameof(_networkController)} == null");
        }

        private void OnDestroy()
        {
            if (_networkController != null)
                _networkController.OnServerDisconnectEvent -= OnDisconnected;
        }

        /// <summary>
        /// Called on server from OnServerAuthenticateInternal when a client needs to authenticate
        /// </summary>
        /// <param name="conn">Connection to client.</param>
        public override async void OnServerAuthenticate(NetworkConnectionToClient conn)
        {
            if (_connectionsPendingDisconnect.Contains(conn))
                return;

            if (_successfulConnections.Count < MaxPlayers)
            {
                UnityEngine.Debug.Log($"{nameof(OnServerAuthenticate)} Successful");
                _successfulConnections.Add(conn);

                var successfulMessage = new AuthResponse(ClientConnectionResult.Successful, _missionConfig.Id);
                conn.Send(successfulMessage);
                ServerAccept(conn);
            }
            else
            {
                UnityEngine.Debug.LogWarning($"{nameof(OnServerAuthenticate)} DeclinedByPlayersCount");
                _connectionsPendingDisconnect.Add(conn);

                var declinedMessage = new AuthResponse(ClientConnectionResult.DeclinedByPlayersCount, _missionConfig.Id);
                conn.Send(declinedMessage);

                // must set NetworkConnection isAuthenticated = false
                conn.isAuthenticated = false;

                // Чтобы клиенту успело оотправится сообщение о неудачном подключении, ставим задержку перед дисконектом.
                const int delay = 1000;
                await UniTask.Delay(delay);
                ServerReject(conn);
                await UniTask.Yield();
                _connectionsPendingDisconnect.Remove(conn);
            }
        }

        private async void OnDisconnected(NetworkConnectionToClient conn)
        {
            if (!_successfulConnections.Contains(conn))
                return;

            UnityEngine.Debug.Log($"{nameof(ServerAuthenticator)} OnDisconnected");

            _successfulConnections.Remove(conn);
        }
    }
}