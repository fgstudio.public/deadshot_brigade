using System;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public sealed class ServerStartArgs
    {
        [NotNull] public IMissionConfig MissionConfig { get; }
        [NotNull] public IReadOnlyList<MatchPlayer> MatchPlayers { get; }
        public int? Port { get; }


        public ServerStartArgs([NotNull] IMissionConfig missionConfig, int? port = null) : this(missionConfig,
            Array.Empty<MatchPlayer>(), port)
        {
        }

        public ServerStartArgs([NotNull] IMissionConfig missionConfig,
            [NotNull] IReadOnlyList<MatchPlayer> matchPlayers,
            int? port = null)
        {
            MissionConfig = missionConfig;
            MatchPlayers = matchPlayers;
            Port = port;
        }
    }
}