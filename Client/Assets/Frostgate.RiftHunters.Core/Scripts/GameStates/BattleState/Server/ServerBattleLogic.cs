using Mirror;
using Zenject;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Battle.Server.Activity;
using Frostgate.RiftHunters.Core.Battle.Server.Systems;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.Respawn;
using Frostgate.RiftHunters.Core.Battle.Server.UnitAI;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Network.Server;
using Frostgate.RiftHunters.Core.Network.Server.Retraining;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Network.Shared.Capturing;
using Frostgate.RiftHunters.Core.Network.Shared.KillStreak;
using Frostgate.RiftHunters.Core.Scripts.Network.Server.KillStreak;

namespace Frostgate.RiftHunters.Core
{
    public class ServerBattleLogic : BattleLogic
    {
        [NotNull] private readonly IReadOnlyList<MatchPlayer> _matchPlayers;

        [NotNull] private readonly List<IDisposable> _serverDisposables = new();
        [NotNull] private readonly CancellationTokenSource _cts;

        [CanBeNull] private IRestoreWatchdog _restoreWatchdog;
        [CanBeNull] private ServerMediator _serverMediator;


        public ServerBattleLogic(NetworkController networkController, DiContainer battleContainer,
            [NotNull] IMissionConfig missionConfig, [NotNull] IReadOnlyList<MatchPlayer> matchPlayers) :
            base(networkController, battleContainer, missionConfig)
        {
            _matchPlayers = matchPlayers;
            _cts = new CancellationTokenSource();
        }

        public void Init()
        {
            Resolve();

            NetworkServer.RegisterHandler<LobbyData>(LobbyResultServerHandler);

            NetworkDependencies.Shared.BotArenaState.AreAllArenasCompletedChanged
                .AddListener(OnAreAllArenasCompletedChanged);

            NetworkController.OnStartServerEvent += OnStartServer;
            NetworkController.OnStopServerEvent += OnStopServer;
            NetworkController.OnServerDisconnectEvent += OnServerDisconnect;
            NetworkController.OnServerReadyEvent += OnServerReady;
        }

        public override void Dispose()
        {
            NetworkClient.UnregisterHandler<LobbyMessage>();

            NetworkDependencies.Shared.BotArenaState.AreAllArenasCompletedChanged
                .RemoveListener(OnAreAllArenasCompletedChanged);

            NetworkController.OnStartServerEvent -= OnStartServer;
            NetworkController.OnStopServerEvent -= OnStopServer;
            NetworkController.OnServerDisconnectEvent -= OnServerDisconnect;
            NetworkController.OnServerReadyEvent -= OnServerReady;

            _cts.Cancel();
            _cts.Dispose();

            DisposeDependencies();
        }

        // TODO: в di-контейнер
        private async void OnStartServer()
        {
            // TODO: перенести на уровень стейта ServerBattleState
            _restoreWatchdog = CreateServerWatchdog();

            SharedDependencies shared = NetworkDependencies.Shared;
            ServerDependencies server = NetworkDependencies.Server;

            IPrefabFactory prefabFactory = shared.PrefabFactory;
            IServiceRoster serviceRoster = shared.ServiceRoster;
            IDamageService damageService = serviceRoster.DamageService;

            ObjectCollectionRepository<UnitNetwork> unitCollections = shared.UnitCollections;
            IObjectCollection<uint, UnitNetwork> unitNetworkCollection = unitCollections.Get<uint>();
            IObjectCollection<Collider, UnitNetwork> unitColliderCollection = unitCollections.Get<Collider>();

            IReadOnlyObjectCollection<uint, IAimTarget> aimTargetNetIdCollection = shared.AimTargetNetIdCollection;
            IReadOnlyObjectCollection<Collider, IAimTarget> aimTargetColliderCollection =
                shared.AimTargetColliderCollection;

            UnitAIGroupVision unitAIGroupVision = new(
                unitColliderCollection, aimTargetNetIdCollection, damageService);
            _serverDisposables.Add(unitAIGroupVision);

            UnitAIGroupVision playerAIGroupVision = new(
                unitColliderCollection, aimTargetNetIdCollection, damageService);
            _serverDisposables.Add(playerAIGroupVision);

            ImpactStateInitializer impactStateInitializer = new(
                shared.AreaRandom, serviceRoster,
                server.UnitEffectStateFactory, server.CommandSenderRepository, unitNetworkCollection);

            UnitSpawnFacade unitSpawnFacade = CreateUnitSpawnFacade(
                impactStateInitializer, unitAIGroupVision, playerAIGroupVision);
            UnitHandleFacade unitHandleFacade = new();

            BotSpawnerFactory botSpawnerFactory = CreateBotSpawnerFactory(unitSpawnFacade);
            BotCustomSpawnerMediator botCustomSpawnerMediator = new(botSpawnerFactory, shared.SpawnAreaFinder);
            _serverDisposables.Add(botCustomSpawnerMediator);

            LootBoxHandleFacade lootBoxFacade = CreateLootBoxFacade();
            TrapHandleFacade trapFacade = CreateTrapFacade();
            MineHandleFacade mineFacade = CreateMineFacade(impactStateInitializer);
            EnergyCapsuleMediator energyCapsuleMediator = DiContainer.Resolve<EnergyCapsuleMediator>();
            _serverDisposables.Add(energyCapsuleMediator);
            TotemHandleFacade totemFacade = CreateTotemFacade();
            _serverDisposables.Add(totemFacade);

            ShieldHandleFacade shieldFacade = CreateShieldFacade();
            WaveFacade waveFacade = new(prefabFactory, unitNetworkCollection);
            LinkFacade linkFacade = new(prefabFactory, unitNetworkCollection);
            ExpFacade scoresFacade = new(serviceRoster.ReceiveExpService);

            BotArenaMediator botArenaMediator = CreateBotArenaMediator(botSpawnerFactory);
            _serverDisposables.Add(botArenaMediator);

            botArenaMediator.CreateArenas(shared.BotArenaSceneConfiguration.ArenaData);

            // TODO: костыль, т.к. UI инициализируется позже и не учитывает начального состояния системы
            UniTask.Delay(TimeSpan.FromSeconds(1), cancellationToken: _cts.Token)
                .ContinueWith(() => botArenaMediator.ActivateNextArena())
                .AsAsyncUnitUniTask();

            BarrelFacade barrelFacade = new(shared.ShootingTargetCollections.Get<uint>());
            _serverDisposables.Add(barrelFacade);

            HpAutoRecoverySystem hpAutoRecoverySystem = new(damageService,
                serviceRoster.ReanimationService, serviceRoster.HealingService);
            _serverDisposables.Add(hpAutoRecoverySystem);

            EnergyAutoRecoverySystem energyAutoRecoverySystem = new(unitNetworkCollection);
            _serverDisposables.Add(energyAutoRecoverySystem);

            StaminaAutoRecoverySystem staminaAutoRecoverySystem = new(
                serviceRoster.StaminaService, unitNetworkCollection);
            _serverDisposables.Add(staminaAutoRecoverySystem);

            HpAutoLossSystem hpAutoLossSystem = new(aimTargetColliderCollection);
            _serverDisposables.Add(hpAutoLossSystem);

            PlayersDieObserver playersDieObserver = server.PlayersDieObserver;

            RespawnSystem respawnSystem = CreateRespawnSystem();
            _serverDisposables.Add(respawnSystem);

            StreakBonusMediator bonusMediator = DiContainer.Resolve<StreakBonusMediator>();
            _serverDisposables.Add(bonusMediator);

            IConnectionInitializationStrategy fallbackConnInitStrategy = new SpawnUnitStrategy(unitSpawnFacade);
            IConnectionInitializationStrategy connInitStrategy =
                new TransferControlToPlayerStrategy(unitNetworkCollection, fallbackConnInitStrategy);
            IConnectionDeinitializationStrategy connDeinitStrategy =
                new TransferControlToAIStrategy(playerAIGroupVision, unitNetworkCollection);

            _serverMediator = new ServerMediator(
                unitHandleFacade, lootBoxFacade, energyCapsuleMediator,
                scoresFacade, waveFacade, linkFacade, trapFacade, mineFacade,
                totemFacade, shieldFacade, serviceRoster, botArenaMediator, botCustomSpawnerMediator,
                bonusMediator, playersDieObserver, server.CommandReceiverRepository,
                unitNetworkCollection, barrelFacade, connInitStrategy, connDeinitStrategy);

            _serverDisposables.Add(_serverMediator);


            OldUnitMineDestroyHandler oldUnitMineDestroyHandler = CreateOldMineDestroyHandler();
            _serverDisposables.Add(oldUnitMineDestroyHandler);

            KillStreakServerHandler killStreakServerHandler = new(
                shared.ServiceRoster.DamageService,
                shared.SimulationTime,
                LoggerFactory.CreateLogger<KillStreakServerHandler>(),
                server.KillStreakState,
                DiContainer.Resolve<KillStreakConfig>(),
                bonusMediator, unitNetworkCollection);
            _serverDisposables.Add(killStreakServerHandler);

            // Фикс https://app.clickup.com/t/85zt69hhr
            // У чекпоинтов на спавне еще не успели просчитаться bounds, поэтому боты в self-host спавнятся друг на друге.
            // Не придумал, как сделать лучше
            await UniTask.Yield();

            // Спавним союзных ботов после того как сервер закончит подбор игроков
            var nameGenerator = new UniqueNameGenerator(server.BotNamesRoster);
            AllyBotsSpawner allyBotsSpawner = new(unitSpawnFacade, MissionConfig, _matchPlayers, nameGenerator);
            allyBotsSpawner.Spawn();
        }

        /// <summary>This is called when a server is stopped - including when a host is stopped.</summary>
        private void OnStopServer() => DisposeDependencies();

        /// <summary>Called on the server when a client is ready (= loaded the scene)</summary>
        private void OnServerReady(NetworkConnectionToClient client) => _restoreWatchdog!.OnServerConnect(client);

        // WARNING: Вызывается без OnServerConnect/OnServerReady при Authenticator.ServerReject

        /// <summary>Called on the server when a client disconnects.</summary>
        private void OnServerDisconnect(NetworkConnectionToClient client)
        {
            _serverMediator!.DeinitializeConnection(client);
            _restoreWatchdog!.OnServerDisconnect(client);
        }

        private IRestoreWatchdog CreateServerWatchdog()
        {
#if !UNITY_EDITOR && UNITY_SERVER
            var watchdogLogger = LoggerFactory.CreateLogger<ServerRestoreWatchdog>();
            return new ServerRestoreWatchdog(Dispose, watchdogLogger);
#endif
            return new HostRestoreWatchdog();
        }

        private RespawnSystem CreateRespawnSystem()
        {
            SharedDependencies shared = NetworkDependencies.Shared;
            ServerDependencies server = NetworkDependencies.Server;

            IServiceRoster serviceRoster = shared.ServiceRoster;
            ObjectCollectionRepository<UnitNetwork> unitCollections = shared.UnitCollections;
            IObjectCollection<uint, UnitNetwork> unitNetworkCollection = unitCollections.Get<uint>();

            var pointCalculator = new UnitPlayerPointCalculator(shared.AreaRandom, server.ActualRespawnPoint);
            var strategyFactory = new RespawnStrategyFactory(serviceRoster.InvincibleService,
                serviceRoster.RespawnUnitService, pointCalculator);
            var awaiterFactory = new AwaiterFactory(server.PlayersDieObserver);
            var arenaConfigProvider = new ArenaConfigProvider(
                shared.BotArenaState, server.BotArenaCollection);
            var handlerFactory = new UnitDeadHandlerFactory(
                awaiterFactory, strategyFactory, arenaConfigProvider,
                shared.SimulationTime, server.BotRespawnState);

            return new RespawnSystem(handlerFactory, unitNetworkCollection);
        }

        private BotArenaMediator CreateBotArenaMediator(BotSpawnerFactory botSpawnerFactory)
        {
            SharedDependencies shared = NetworkDependencies.Shared;
            ServerDependencies server = NetworkDependencies.Server;

            BotSpawnState spawnState = shared.BotSpawnState;
            IEditableBotArenaState arenaState = server.BotArenaState;

            ObjectCollectionRepository<UnitNetwork> unitCollections = shared.UnitCollections;
            IObjectCollection<uint, UnitNetwork> unitNetworkCollection = unitCollections.Get<uint>();
            IObjectCollection<BotArenaConfig, BotArena> botArenaCollection = server.BotArenaCollection;

            var activityFactory = new ActivityFactory(server.DdaSystem,
                spawnState, unitNetworkCollection);

            var botArenaFactory = new BotArenaFactory(
                arenaState, spawnState, botSpawnerFactory, activityFactory, server.ActualRespawnPoint);
            var botArenaCreationMediator =
                new BotArenaCreationMediator(botArenaFactory, botArenaCollection);

            BotArenaConfig[] botArenaConfigs = shared.BotArenaSceneConfiguration.ArenaData
                .Select(d => d.Config).ToArray();

            var botArenaIterator =
                new BotArenaOrderedIterator(botArenaConfigs, botArenaCollection);
            _serverDisposables.Add(botArenaIterator);

            var botArenaActivationMediator = new BotArenaActivationMediator(arenaState, botArenaIterator);
            _serverDisposables.Add(botArenaActivationMediator);

            return new BotArenaMediator(
                botArenaCreationMediator, botArenaActivationMediator);
        }

        private UnitSpawnFacade CreateUnitSpawnFacade(ImpactStateInitializer impactStateInitializer,
            UnitAIGroupVision botGroupVision, UnitAIGroupVision playerGroupVision)
        {
            SharedDependencies shared = NetworkDependencies.Shared;
            ServerDependencies server = NetworkDependencies.Server;

            var smHandlerFactory = new DefaultStateMachineHandlerFactory(shared.ServiceRoster, shared.AimTargetFinder);
            var pawnFactory = new PawnFactory(_unitNetworkPrefab, shared.PrefabFactory, smHandlerFactory,
                impactStateInitializer);

            ObjectCollectionRepository<UnitNetwork> unitCollections = shared.UnitCollections;
            AreaRandom areaRandom = shared.AreaRandom;
            IActualRespawnPoint respawnPoint = server.ActualRespawnPoint;

            var pointCalculator = new UnitPlayerPointCalculator(areaRandom, respawnPoint);
            var unitFactory = new UnitFactory(pawnFactory, pointCalculator);
            var botFactory = new BotFactory(pawnFactory, botGroupVision, playerGroupVision, pointCalculator, unitCollections.Get<uint>());

            return new UnitSpawnFacade(unitFactory, botFactory, unitCollections);
        }

        private BotSpawnerFactory CreateBotSpawnerFactory(IBotSpawnFacade botSpawnFacade)
        {
            SharedDependencies shared = NetworkDependencies.Shared;
            ServerDependencies server = NetworkDependencies.Server;

            SimulationTime simTime = shared.SimulationTime;
            IEditableBotSpawnState botSpawnState = shared.BotSpawnState;
            IDdaSystem ddaSystem = server.DdaSystem;

            var botWaveSourceFacade = new BotWaveSourceFacade(ddaSystem);
            _serverDisposables.Add(botWaveSourceFacade);

            return new BotSpawnerFactory(simTime, botSpawnFacade, botSpawnState, botWaveSourceFacade);
        }

        private LootBoxHandleFacade CreateLootBoxFacade()
        {
            SharedDependencies shared = NetworkDependencies.Shared;

            var poolRepository = new LootBoxPoolRepository(_lootBox.PrefabSource, _lootBox.PoolConfig);
            var factory = new LootBoxFactory(shared.AreaRandom, poolRepository);
            var chancePicker = new LootBoxChancePicker(shared.Random);
            var unitCollection = shared.UnitCollections.Get<uint>();

            return new LootBoxHandleFacade(factory, chancePicker, unitCollection);
        }

        private TrapHandleFacade CreateTrapFacade()
        {
            SharedDependencies shared = NetworkDependencies.Shared;

            var poolRepository = new TrapPoolRepository(_trap.PrefabSource, _trap.PoolConfig, shared.PrefabFactory);
            var trapFactory = new TrapFactory(poolRepository);

            var logger = DiContainer.Resolve<ILogger<TrapHandler>>();
            var unitCollection = shared.UnitCollections.Get<uint>();
            var handlerFactory = new TrapHandlerFactory(logger, shared.ServiceRoster, unitCollection);

            return new TrapHandleFacade(unitCollection, shared.TrapCollections, handlerFactory, trapFactory);
        }

        private MineHandleFacade CreateMineFacade(ImpactStateInitializer impactStateInitializer)
        {
            SharedDependencies shared = NetworkDependencies.Shared;

            var poolRepository = new MinePoolRepository(_mine.PrefabSource, _mine.PoolConfig, shared.PrefabFactory);
            var factory = new MineFactory(shared.Random, poolRepository, impactStateInitializer);

            return new MineHandleFacade(shared.MineCollections, factory);
        }

        private TotemHandleFacade CreateTotemFacade()
        {
            SharedDependencies shared = NetworkDependencies.Shared;
            ServerDependencies server = NetworkDependencies.Server;

            var poolRepository = new TotemPoolRepository(_totem.PrefabSource, _totem.PoolConfig, shared.PrefabFactory);
            var unitCollection = shared.UnitCollections.Get<uint>();
            var totemFactory = new TotemFactory(shared.Random, poolRepository);

            var dataFactory = DiContainer.Instantiate<TotemEffectDataFactory>();
            var facadeLogger = DiContainer.Resolve<ILogger<TotemHostUniHandleFacade>>();
            var facadeFactory =
                new TotemHostUniHandleFacadeFactory(dataFactory, server.UnitEffectStateFactory, facadeLogger);

            return new TotemHandleFacade(unitCollection, server.CommandSenderRepository.Get<BotSpawnerConfig>(),
                shared.TotemCollections, facadeFactory, totemFactory);
        }

        private ShieldHandleFacade CreateShieldFacade()
        {
            SharedDependencies shared = NetworkDependencies.Shared;

            var unitCollection = shared.UnitCollections.Get<uint>();
            var shieldFactory = new ShieldFactory(shared.PrefabFactory);

            return new ShieldHandleFacade(shieldFactory, unitCollection);
        }

        private OldUnitMineDestroyHandler CreateOldMineDestroyHandler()
        {
            SharedDependencies shared = NetworkDependencies.Shared;

            IObjectCollection<uint, IMine> mineCollection =
                shared.MineCollections.Get<uint>();

            var destroyerLogger = DiContainer.Resolve<ILogger<OldUnitMineDestroyer>>();
            var oldUnitMineDestroyerFactory = new OldUnitMineDestroyerFactory(
                shared.SimulationTime, destroyerLogger);

            return new OldUnitMineDestroyHandler(oldUnitMineDestroyerFactory, mineCollection);
        }

        private void LobbyResultServerHandler(NetworkConnectionToClient sender, LobbyData message)
        {
            if (!sender.isAuthenticated)
            {
                UnityEngine.Debug.LogWarning(
                    $"{nameof(LobbyResultServerHandler)}: not authenticated sender try send message");
                sender.Disconnect();
                return;
            }

            if (_serverMediator == null)
            {
                UnityEngine.Debug.LogWarning(
                    $"{nameof(LobbyResultServerHandler)}: {nameof(ServerMediator)} isn't instantiated");
                sender.Disconnect();
                return;
            }

            _serverMediator.InitializeConnection(sender, message.SelectedUnit,
                message.Equipments, message.Player, message.HeroLevel);
        }

        private void OnAreAllArenasCompletedChanged(bool areAllArenasCompleted)
        {
            if (areAllArenasCompleted)
            {
                NetworkDependencies.Shared.BotArenaState.AreAllArenasCompletedChanged
                    .RemoveListener(OnAreAllArenasCompletedChanged);

                TimeSpan playerFreeMovingTime = TimeSpan.FromSeconds(NetworkController._playerWinFreeMoveTime);
                UniTask.Delay(playerFreeMovingTime, cancellationToken: _cts.Token)
                    .ContinueWith(() =>
                    {
                        NetworkDependencies.Shared.PlayerInput.Disable();
                        DisableUnitsInput();
                    }).AsAsyncUnitUniTask();
            }
        }

        private void DisableUnitsInput()
        {
            var units = NetworkDependencies.Shared
                .UnitCollections.Get<uint>().Values;

            foreach (UnitNetwork unitNetwork in units)
                unitNetwork.UnitNetworkInput.enabled = false;
        }

        private void DisposeDependencies()
        {
            _serverDisposables.ForEach(d => d.Dispose());
            _serverDisposables.Clear();
        }
    }
}