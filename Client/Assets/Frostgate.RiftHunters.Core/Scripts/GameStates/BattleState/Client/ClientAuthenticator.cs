using System;
using System.Collections;
using Cysharp.Threading.Tasks;
using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public class ClientAuthenticator : NetworkAuthenticator
    {
        public event Action<ClientConnectionResult> OnResult;

        private Func<string, UniTask> _levelLoadingTask;
        private bool _isAuthenticated;

        public void Init(Func<string, UniTask> levelLoadingTask)
        {
            _levelLoadingTask = levelLoadingTask;
        }

        /// <summary>Called when client starts, used to register message handlers if needed.</summary>
        public override void OnStartClient()
        {
            NetworkClient.RegisterHandler<AuthResponse>(AuthResponseHandler, false);
            StartCoroutine(TimeoutConnection());
        }

        private IEnumerator TimeoutConnection()
        {
            const float duration = 20;
            yield return new WaitForSecondsRealtime(duration);

            if (NetworkClient.connection != null)
                NetworkClient.connection.Disconnect();

            UnityEngine.Debug.LogError($"{nameof(TimeoutConnection)} Disconnect by timer {duration}s");
            OnResult?.Invoke(ClientConnectionResult.DeclinedByClientTimeout);
        }

        /// <summary>Called when client stops, used to unregister message handlers if needed.</summary>
        public override void OnStopClient()
        {
            StopAllCoroutines();
            NetworkClient.UnregisterHandler<AuthResponse>();
            
            if (!_isAuthenticated)
                OnResult?.Invoke(ClientConnectionResult.DeclinedByClientTimeout);
        }

        private async void AuthResponseHandler(AuthResponse message)
        {
            UnityEngine.Debug.Log($"{nameof(AuthResponseHandler)} result:");
            StopAllCoroutines();

            switch (message.Result)
            {
                case ClientConnectionResult.Successful:
                    UnityEngine.Debug.Log(message.Result);
                    await _levelLoadingTask(message.MissionId);
                    ClientAccept();
                    _isAuthenticated = true;
                    break;

                case ClientConnectionResult.DeclinedByPlayersCount:
                    UnityEngine.Debug.LogError(message.Result);
                    ClientReject();
                    _isAuthenticated = false;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            OnResult?.Invoke(message.Result);
        }
    }
}