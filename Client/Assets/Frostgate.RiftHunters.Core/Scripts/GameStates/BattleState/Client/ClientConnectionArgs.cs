using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Core
{
    // TODO: Под рефакторинг, явно выделить обязательные параметры.
    public class ClientConnectionArgs
    {
        public string ServerAddress;
        public int? ServerPort;
        public PlayerDto Player;
        public int HeroLevel;
        public UnitConfig SelectedUnit;
        public Equipment[] Equipments;
        public bool MatchMakerMode;
        public IMissionConfig MissionConfig;
        public string MatchId;
    }
}