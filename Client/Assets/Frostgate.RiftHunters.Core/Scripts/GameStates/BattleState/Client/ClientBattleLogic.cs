using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Client;
using Frostgate.RiftHunters.Core.Battle.Client.Activity;
using Frostgate.RiftHunters.Core.Battle.Client.FX;
using Frostgate.RiftHunters.Core.Battle.Client.Vibration;
using Frostgate.RiftHunters.Core.Battle.Impact.Client;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Network.Client;
using Frostgate.RiftHunters.Core.Network.Client.Observing;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Network.Shared.Capturing;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.UI;
using Mirror;
using Zenject;
using UnityEngine;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle.Client.KillStreak;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.MissionScore;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Network.Shared.KillStreak;
using Frostgate.RiftHunters.Core.UI.Hud;
using Frostgate.RiftHunters.Core.Voice;
using Frostgate.RiftHunters.Meta;
using JetBrains.Annotations;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Core
{
    public class ClientBattleLogic : BattleLogic
    {
        private readonly ClientConnectionArgs _args;
        private readonly List<IDisposable> _clientDisposables = new();
        private readonly ILogger _logger = LoggerFactory.CreateLogger<ClientBattleLogic>();
        private readonly BattleComponentRoster _componentRoster;
        [NotNull] private readonly CancellationTokenSource _cts;
        private readonly CampaignService _campaignService;

        private BattleCamera _camera;

        public ClientBattleLogic(ClientConnectionArgs args,
            NetworkController networkController,
            DiContainer battleContainer,
            BattleComponentRoster componentRoster,
            [NotNull] IMissionConfig missionConfig, CampaignService campaignService) : base(networkController, battleContainer, missionConfig)
        {
            _args = args;
            _componentRoster = componentRoster;
            _cts = new CancellationTokenSource();
            _campaignService = campaignService;
        }

        //TODO: пока отдельно. Взялся переносить камеру со сцены, но от неё зависит сцена, так что камеру пока надо проинитить раньше. Потом от этого надо будет избавится и возможно оставить один метод инициализации.
        public BattleCamera InitCamera()
        {
            _logger.Log($"{nameof(InitCamera)}");

            _camera = Object.Instantiate(_componentRoster.BattleCamera);
            Object.DontDestroyOnLoad(_camera.gameObject);
            DiContainer.Bind<BattleCamera>().FromInstance(_camera).AsSingle();

            return _camera;
        }

        public void Init()
        {
            Resolve();

            CreateObserverRepository(NetworkDependencies.Shared, NetworkDependencies.Client);
            CreateSpawners(NetworkDependencies.Shared);

            NetworkDependencies.Shared.BotArenaState
                .AreAllArenasCompletedChanged.AddListener(OnAreAllArenasCompletedChanged);

            foreach (NetworkIdentity spawnablePrefab in _spawnablePrefabs)
                NetworkClient.RegisterPrefab(spawnablePrefab.gameObject, Spawn, UnSpawn);

            var killStreakClientHandler = new KillStreakClientHandler(NetworkDependencies.Shared.SimulationTime,
                LoggerFactory.CreateLogger<KillStreakClientHandler>(), NetworkDependencies.Client.KillStreakState,
                DiContainer.Resolve<KillStreakConfig>(), NetworkDependencies.Client.UIBattleMediator.Hud.KillStreak);
            _clientDisposables.Add(killStreakClientHandler);

            var activityFactory = new ClientActivityHandlingFactory(
                NetworkController.ActivityViewConfigRoster,
                NetworkDependencies.Shared.BotSpawnState,
                NetworkDependencies.Client.UIBattleMediator,
                NetworkDependencies.Shared.UnitCollections.Get<uint>());

            IEnumerable<ActivityTaskSceneData> activityTaskData =
                NetworkDependencies.Shared.BotArenaSceneConfiguration
                    .ArenaData.SelectMany(d => d.Activities.SelectMany(c => c.TasksData))
                    .Where(o => o != null);

            var service = new ClientActivityService(activityTaskData, activityFactory);
            _clientDisposables.Add(service);

            if (MissionConfig.IsTutorial)
            {
                var tutorialMediator = DiContainer.Instantiate<TutorialMediator>();
                tutorialMediator.InitMissionConfig(MissionConfig);
                _clientDisposables.Add(tutorialMediator);
            }

            NetworkClient.Send(new LobbyData(_args.Player, _args.SelectedUnit, _args.Equipments, _args.HeroLevel));

            NetworkController.OnClientConnectEvent += OnClientConnected;
            NetworkController.OnClientDisconnectEvent += OnClientDisconnected;
        }

        public override void Dispose()
        {
            NetworkDependencies.Shared.BotArenaState
                .AreAllArenasCompletedChanged.RemoveListener(OnAreAllArenasCompletedChanged);

            NetworkController.OnClientDisconnectEvent -= OnClientDisconnected;

            _cts.Cancel();
            _cts.Dispose();

            ClearClientDisposables();

            Object.Destroy(_camera.gameObject);
        }

        private void CreateObserverRepository(
            SharedDependencies shared, ClientDependencies client)
        {
            DiContainer diContainer = shared.DiContainer;
            IUIBattleMediator uiBattleMediator = client.UIBattleMediator;
            BattleVibrationMediator vibrationMediator = client.VibrationMediator;
            AimTargetNetIdCollection aimTargetCollection = shared.AimTargetNetIdCollection;
            IObjectCollection<uint,UnitNetwork> unitNetworks = shared.UnitCollections.Get<uint>();

            var unitCollectionObserver = CreateUnitCollectionObserver(shared, client);

            var teamPanel = DiContainer.Resolve<ITeamPanel>();
            var allyPanel = DiContainer.Resolve<IAllyPanel>();
            var emotionObserver = new UnitEmotionObserver(teamPanel, allyPanel, LoggerFactory.CreateLogger<UnitEmotionObserver>());
            emotionObserver.SetObservable(unitNetworks);
            _clientDisposables.Add(emotionObserver);

            var fxRoster = diContainer.Resolve<IFxRoster>();
            var serviceRosterObserver = new ServiceRosterObserver(fxRoster, vibrationMediator, uiBattleMediator, unitNetworks, aimTargetCollection);
            serviceRosterObserver.SetObservable(shared.ServiceRoster);

            var trapCollectionObserver = new TrapCollectionObserver(uiBattleMediator.Hud.SpecialProgressBars);
            trapCollectionObserver.SetObservable(shared.TrapCollections.Get<uint>());

            var totemCollectionObserver = new TotemCollectionObserver(uiBattleMediator.TotemsPanel, uiBattleMediator.Hud.PointerPanels.TotemPanel);
            totemCollectionObserver.SetObservable(shared.TotemCollections.Get<uint>());

            var arenaStateObserver = new BotArenaStateObserver(
                client.UIBattleMediator, client.ActivityObserverFactory, shared.BotArenaSceneConfiguration);
            arenaStateObserver.SetObservable(shared.BotArenaState);

            var respawnStateObserver =
                new RespawnStateObserver(shared.SimulationTime, client.UIBattleMediator,
                    shared.UnitCollections.Get<uint>());

            respawnStateObserver.SetObservable(shared.BotRespawnState);

            var arenaProgressObserver = new ArenaProgressObserver(shared.SimulationTime,
                client.UIBattleMediator.Hud.MissionTimer, shared.MissionScoreConfig.StartArenaId);
            arenaProgressObserver.SetObservable(shared.BotArenaState);

            var bonusCollection =
                DiContainer.Resolve<IObjectCollection<uint, ISharedStreakBonus>>();
            var bonusInitializeObservable =
                DiContainer.Instantiate<StreakBonusInitializeObservable>();
            bonusInitializeObservable.SetObservable(bonusCollection);

            VoiceService voiceService = client.VoiceService;
            var voiceInputObserver = new VoiceInputObserver(voiceService, voiceService, client.UIBattleMediator.VoiceChatMenu);
            voiceInputObserver.SetObservable(shared.PlayerInput);

            var selfVoiceObserver = new SelfVoiceObserver(client.UIBattleMediator.InputMediator.MuteSelfButton);
            selfVoiceObserver.SetObservable(voiceService);

            var participantsVoiceObserver =
                new ParticipantsVoiceObserver(client.UIBattleMediator.InputMediator.MuteParticipantsButton);
            participantsVoiceObserver.SetObservable(voiceService);

            var observerRepository = new ObjectObserverRepository();
            observerRepository.Add(unitCollectionObserver);
            observerRepository.Add(serviceRosterObserver);
            observerRepository.Add(trapCollectionObserver);
            observerRepository.Add(totemCollectionObserver);
            observerRepository.Add(arenaStateObserver);
            observerRepository.Add(respawnStateObserver);
            observerRepository.Add(arenaProgressObserver);
            observerRepository.Add(bonusInitializeObservable);
            observerRepository.Add(voiceInputObserver);
            observerRepository.Add(selfVoiceObserver);
            observerRepository.Add(participantsVoiceObserver);

            _clientDisposables.Add(observerRepository);
        }

        private UnitCollectionObserver CreateUnitCollectionObserver(
            SharedDependencies shared, ClientDependencies client)
        {
            IUIBattleMediator uiBattleMediator = client.UIBattleMediator;
            BattleVibrationMediator vibrationMediator = client.VibrationMediator;
            IObjectCollection<uint, UnitNetwork> unitNetworks =
                shared.UnitCollections.Get<uint>();

            var stateMachineFactory = new UnitStateMachineFactory(
                shared.ServiceRoster, shared.AimTargetFinder);
            var unitObserverFactory = new UnitObserverFactory(
                uiBattleMediator.Hud.TeamPanel, uiBattleMediator.Hud.PointerPanels.AllyPanel);

            LocalPlayerCompositeObserver localPlayerObserver =
                CreateLocalPlayerObserver(shared, client, vibrationMediator);

            var unitCollectionObserver = new UnitCollectionObserver(
                client.UIBattleMediator, client.InputAttacher,
                unitObserverFactory, stateMachineFactory, localPlayerObserver);
            unitCollectionObserver.SetObservable(unitNetworks);

            return unitCollectionObserver;
        }

        private LocalPlayerCompositeObserver CreateLocalPlayerObserver(
            SharedDependencies shared, ClientDependencies client, BattleVibrationMediator vibrationMediator)
        {
            IUIBattleMediator uiBattleMediator = client.UIBattleMediator;
            IUIPlayerInputMediator inputMediator = uiBattleMediator.InputMediator;
            IConfigCollection<ImpactUIConfig> impactUiConfigs = client.ImpactUIConfigs;
            ObjectCollectionRepository<UnitNetwork> unitCollections = shared.UnitCollections;

            var observer = new LocalPlayerCompositeObserver(
                new LocalPlayerDodgingObserver(inputMediator),
                new LocalPlayerReloadingObserver(inputMediator),
                new LocalPlayerAbilityObserver(inputMediator, vibrationMediator, impactUiConfigs),
                new LocalPlayerInputButtonObserver(vibrationMediator),
                new LocalPlayerEnergyObserver(uiBattleMediator, impactUiConfigs),
                new LocalPlayerStateObserver(uiBattleMediator, unitCollections)
            );

            _clientDisposables.Add(observer);
            return observer;
        }

        private void CreateSpawners(SharedDependencies shared)
        {
            IPrefabFactory prefabFactory = shared.PrefabFactory;
            ObjectCollectionRepository<UnitNetwork> unitCollections = shared.UnitCollections;

            var unitSpawner = new UnitSpawner(_unitNetworkPrefab, unitCollections, prefabFactory);
            _clientDisposables.Add(unitSpawner);

            var lootBoxSpawner = new LootBoxSpawner(_lootBox.PrefabSource, _lootBox.PoolConfig);
            _clientDisposables.Add(lootBoxSpawner);

            var energyCapsuleSpawner = DiContainer.Resolve<EnergyCapsuleSpawner>();
            _clientDisposables.Add(energyCapsuleSpawner);

            var trapPoolRepository = new TrapPoolRepository(_trap.PrefabSource, _trap.PoolConfig, shared.PrefabFactory);
            _clientDisposables.Add(trapPoolRepository);

            var trapFactory = new TrapFactory(trapPoolRepository);
            var trapSpawner = new TrapSpawner(_trap.PrefabSource, trapFactory, shared.TrapCollections);
            _clientDisposables.Add(trapSpawner);

            var totemPoolRepository = new TotemPoolRepository(_totem.PrefabSource, _totem.PoolConfig, shared.PrefabFactory);
            _clientDisposables.Add(totemPoolRepository);

            var totemFactory = new TotemFactory(totemPoolRepository);
            var totemSpawner = new TotemSpawner(totemFactory, _totem.PrefabSource, shared.TotemCollections);
            _clientDisposables.Add(totemSpawner);

            var minePoolRepository = new MinePoolRepository(_mine.PrefabSource, _mine.PoolConfig, shared.PrefabFactory);
            _clientDisposables.Add(minePoolRepository);

            var mineFactory = new MineFactory(minePoolRepository);
            var mineSpawner = new MineSpawner(mineFactory, _mine.PrefabSource, shared.MineCollections);
            _clientDisposables.Add(mineSpawner);

            var bonusSpawner = DiContainer.Instantiate<StreakBonusSpawner>();
            _clientDisposables.Add(bonusSpawner);
        }

        private GameObject Spawn(SpawnMessage spawnMessage)
        {
            var prefab = _spawnablePrefabs.First(x => x.assetId == spawnMessage.assetId);
            var instance = NetworkDependencies.Shared.PrefabFactory.Instantiate(prefab);
            return instance;
        }

        private void UnSpawn(GameObject gameObject) => Object.Destroy(gameObject);

        private void OnClientConnected() => ConnectToVoiceChannel();

        /// <summary>Called on clients when disconnected from a server.</summary>
        private void OnClientDisconnected()
        {
            DisconnectFromVoiceChannel();
            ClearClientDisposables();

            foreach (var spawnablePrefab in _spawnablePrefabs)
                NetworkClient.UnregisterPrefab(spawnablePrefab.gameObject);
        }

        private async void ConnectToVoiceChannel()
        {
            VoiceService voiceService = NetworkDependencies.Client.VoiceService;
            MissionType missionType = _args.MissionConfig.Type;
            string matchId = _args.MatchId;

            if (missionType == MissionType.Multiplayer && !string.IsNullOrWhiteSpace(matchId))
                await voiceService.JoinChannelAsync(matchId);
        }

        private void DisconnectFromVoiceChannel()
        {
            VoiceService voiceService = NetworkDependencies.Client.VoiceService;
            voiceService.LeaveChannel();
        }

        private void OnAreAllArenasCompletedChanged(bool areAllArenasCompleted)
        {
            if (areAllArenasCompleted)
            {
                NetworkDependencies.Shared.BotArenaState.AreAllArenasCompletedChanged
                    .RemoveListener(OnAreAllArenasCompletedChanged);

                NetworkDependencies.Client.UIBattleMediator.Hud.MissionEndTimer.RunTimer(NetworkController._playerWinFreeMoveTime);

                TimeSpan playerFreeMovingTime = TimeSpan.FromSeconds(NetworkController._playerWinFreeMoveTime);
                UniTask.Delay(playerFreeMovingTime, cancellationToken: _cts.Token)
                    .ContinueWith(() =>
                    {
                        NetworkDependencies.Shared.PlayerInput.Disable();
                        ShowMissionWinView();
                    }).AsAsyncUnitUniTask();
            }
        }

        private void ShowMissionWinView()
        {
            IUIBattleMediator uiBattleMediator = NetworkDependencies.Client.UIBattleMediator;
            IEnumerable<WinPlayerInfo> winPlayerInfos = GenerateWinPlayersInfo();
            TimeSpan sessionDuration = CalcSessionDuration();
            int starsCount = CalcStarsCount();

            var phase = _campaignService.GetCurrentPhase();
            _campaignService.AddScore(phase, starsCount);

            uiBattleMediator.EnableFinishedGameplayMode();
            uiBattleMediator.WinPanelMediator.SetScoresTable(
                winPlayerInfos, sessionDuration, starsCount, OnExitClicked);
        }

        private IEnumerable<WinPlayerInfo> GenerateWinPlayersInfo()
        {
            return NetworkDependencies.Shared.UnitCollections.Get<uint>()
                .Where(unit => unit.UnitNetworkState.UnitType == BattleUnitType.Player)
                .Select(player => player.UnitNetworkState)
                .Select(state => new WinPlayerInfo()
                {
                    HeroId = state.Id,
                    IsLocalPlayer = state.isLocalPlayer,
                    UserName = state.PlayerName,
                    PlayerScore = state.Exp
                });
        }

        private TimeSpan CalcSessionDuration()
        {
            SharedDependencies shared = NetworkDependencies.Shared;
            IReadOnlyDictionary<string,BotArenaData> arenaData = shared.BotArenaState.Data;
            IMissionScoresConfig scoresConfig = shared.MissionScoreConfig;

            string startArenaId = scoresConfig.StartArenaId; // TODO: Сделать выделенный механизм, который рассчитывает длительность выполнения миссии и переиспользовать
            BotArenaData startArena = arenaData[startArenaId];
            BotArenaData lastArena = arenaData.Values.OrderBy(v => v.Index).Last();
            float sessionSeconds = lastArena.CompletionTime - startArena.StartTime;

            return TimeSpan.FromSeconds(sessionSeconds);
        }

        private int CalcStarsCount()
        {
            SharedDependencies shared = NetworkDependencies.Shared;
            IMissionScoresConfig scoresConfig = shared.MissionScoreConfig;
            IObjectCollection<uint, UnitNetwork> units = shared.UnitCollections.Get<uint>();

            ScoreConditionValidatorFactory validatorFactory = new(shared.BotArenaState); // TODO: Перенести в контейнер в Shared-область
            MissionScoreFactory scoreFactory = new(validatorFactory);
            MissionScoresCalculator scoresCalculator = new(scoresConfig, scoreFactory);

            UnitNetwork localUnit = units.First(u => u.NetworkIdentity.isLocalPlayer);

            return scoresCalculator.Calculate(localUnit);
        }

        private async void OnExitClicked()
        {
            await NetworkDependencies.Shared.StateMachine
                .EnterAsync<GameLoadMainSceneState, ClientConnectionError>(ClientConnectionError.None);
        }

        private void ClearClientDisposables()
        {
            _clientDisposables.ForEach(d => d.Dispose());
            _clientDisposables.Clear();
        }
    }
}