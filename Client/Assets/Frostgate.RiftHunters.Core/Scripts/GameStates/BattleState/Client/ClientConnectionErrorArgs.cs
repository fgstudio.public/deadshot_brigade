namespace Frostgate.RiftHunters.Core
{
    public enum ClientConnectionError
    {
        None,
        ServerUnreachable,
        AuthFailed,
        Unknown
    }
}
