using System;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Client;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.UI.Screens;
using Frostgate.RiftHunters.Meta;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public class ClientBattleState : BattleState, IState<ClientConnectionArgs>
    {
        private ClientBattleLogic _client;
        private readonly IUIService _uiService;
        private readonly ISceneLoader _sceneLoader;
        private ClientConnectionArgs _args;
        private ClientAuthenticator _authenticator;
        private readonly IGameStateMachine _stateMachine;
        [NotNull] private readonly IConfigCollection<IMissionConfig> _missionConfigs;
        [NotNull] private readonly ILogger<ClientBattleState> _logger;
        [NotNull] private readonly CampaignService _campaignService;

        public ClientBattleState(IUIService uiService, IPreferences preferences,
            ISceneLoader sceneLoader,
            IGameStateMachine stateMachine, [NotNull] IConfigCollection<IMissionConfig> missionConfigs,
            [NotNull] ILogger<ClientBattleState> logger, [NotNull] CampaignService campaignService) :
            base(preferences.BattleComponentRoster)
        {
            _uiService = uiService;
            _sceneLoader = sceneLoader;
            _stateMachine = stateMachine;
            _missionConfigs = missionConfigs;
            _logger = logger;
            _campaignService = campaignService;
        }

        public async UniTask EnterAsync(ClientConnectionArgs args)
        {
            _args = args;

            // Сохраняем данные о подключении для реконнекта
            StaticObject<TimedContainer<ClientConnectionArgs>>.Instance =
                new TimedContainer<ClientConnectionArgs>(args, TimeSpan.FromSeconds(600));

            await _uiService.ShowAsync<UILoadingScreen>(true);

            NetworkEnter();

            _authenticator = NetworkController.ReplaceAuthenticator<ClientAuthenticator>();
            _authenticator.Init(OnReadyLoadScene);
            _authenticator.OnResult += OnAuthResult;

            NetworkController.StartClient(args.ServerAddress, args.ServerPort);
        }

        private async void OnAuthResult(ClientConnectionResult result)
        {
            _authenticator.OnResult -= OnAuthResult;

            switch (result)
            {
                case ClientConnectionResult.Successful:
                    NetworkController.OnClientDisconnectEvent += OnClientDisconnect;
                    break;
                case ClientConnectionResult.DeclinedByPlayersCount:
                case ClientConnectionResult.DeclinedByClientTimeout:
                    // Если сервер не пускает, то очищаем данные для потенциального реконнекта.
                    StaticObject<TimedContainer<ClientConnectionArgs>>.Instance = null;
                    //TODO: нужна система диалогов. А пока приходится пробрасывать ошибку в следующий стейт.
                    await _stateMachine.EnterAsync<GameLoadMainSceneState, ClientConnectionError>(ClientConnectionError.AuthFailed);
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException(nameof(result), result, null);
            }
        }

        private async UniTask OnReadyLoadScene(string missionId)
        {
            if (!_missionConfigs.TryGet(missionId, out IMissionConfig mission))
            {
                _logger.LogCritical($"{nameof(IMissionConfig)} with {nameof(mission.Id)} '{missionId}' not found.");

                return;
            }

            await _sceneLoader.LoadMissionAsync(mission);

            _client = new ClientBattleLogic(_args, NetworkController, BattleContainer, BattleComponentRoster, mission, _campaignService);

            //TODO: в этот момент активны 2 камеры до того как загрузится следующая сцена
            //TODO: камера нужна объектам которые сейчас спаунятся общим списком со сценой. Поэтому камеру раньше сцены инитим так как я её со сцены убрал. Далее и остальные системы надо переносить и делать свои инсталлеры завизимостей.
            BattleCamera battleCamera = _client.InitCamera();

            InstallBindings();
            BattleContainer.Inject(battleCamera);

            _client.Init();
        }

        private async void OnClientDisconnect()
        {
            UnityEngine.Debug.LogError($"{nameof(ClientBattleState)} {nameof(OnClientDisconnect)}");

            //TODO: нужна система диалогов. А пока приходится пробрасывать ошибку в следующий стейт.
            await _stateMachine.EnterAsync<GameLoadMainSceneState, ClientConnectionError>(ClientConnectionError.Unknown);
        }

        public UniTask ExitAsync()
        {
            if (_authenticator != null)
                _authenticator.OnResult -= OnAuthResult;

            _client?.Dispose();

            NetworkController.OnClientDisconnectEvent -= OnClientDisconnect;
            NetworkController.StopClient();

            NetworkExit();
            return UniTask.CompletedTask;
        }
    }
}