﻿using Frostgate.RiftHunters.Core.Voice;
using Zenject;

namespace Frostgate.RiftHunters.Core
{
    public sealed class CoreInstaller : Installer
    {
        public override void InstallBindings()
        {
            Container.Install<LoggingInstaller>();
            Container.Bind<VoiceService>().AsSingle();
        }
    }
}