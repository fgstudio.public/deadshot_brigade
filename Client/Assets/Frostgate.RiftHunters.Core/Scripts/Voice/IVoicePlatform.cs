﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.Voice
{
    public interface IVoicePlatform : ISelfVoiceController, IParticipantsVoiceController
    {
        UniTask InitializeAsync();
        UniTask LoginAsync(string userName);
        UniTask JoinChannelAsync(string channelId);
        void LeaveChannel();
        UniTask DeinitializeAsync();
    }
}