﻿using System;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Core.Voice
{
    public interface ISelfVoiceController
    {
        event Action<bool> SelfSpeechDetected;
        event Action<bool> MuteSelfChanged;

        bool MuteSelf { get; set; }
    }

    public interface IParticipantsVoiceController
    {
        event Action<string, bool> ParticipantSpeechDetected;
        event Action<bool> MuteParticipantsChanged;
        event Action<string, bool> MuteParticipantChanged;
        public bool ParticipantIsMuted(string userName);

        public bool IsOtherParticipantsMuted();

        bool MuteParticipants { get; set; }

        void MuteParticipant(string userName, bool mute);
    }

    public sealed class VoiceService : ISelfVoiceController, IParticipantsVoiceController
    {
        public event Action<bool> SelfSpeechDetected
        {
            add => _platform.SelfSpeechDetected += value;
            remove => _platform.SelfSpeechDetected -= value;
        }

        public event Action<bool> MuteSelfChanged
        {
            add => _platform.MuteSelfChanged += value;
            remove => _platform.MuteSelfChanged -= value;
        }

        public event Action<string, bool> ParticipantSpeechDetected
        {
            add => _platform.ParticipantSpeechDetected += value;
            remove => _platform.ParticipantSpeechDetected -= value;
        }

        public event Action<bool> MuteParticipantsChanged
        {
            add => _platform.MuteParticipantsChanged += value;
            remove => _platform.MuteParticipantsChanged -= value;
        }

        public event Action<string, bool> MuteParticipantChanged
        {
            add => _platform.MuteParticipantChanged += value;
            remove => _platform.MuteParticipantChanged -= value;
        }

        public bool MuteSelf
        {
            get => _platform.MuteSelf;
            set => _platform.MuteSelf = value;
        }

        public bool MuteParticipants
        {
            get => _platform.MuteParticipants;
            set => _platform.MuteParticipants = value;
        }

        [NotNull] private readonly IVoicePlatform _platform;


        public VoiceService([NotNull] IVoicePlatform platform)
        {
            _platform = platform;
        }

        public UniTask InitializeAsync() => _platform.InitializeAsync();
        public UniTask LoginAsync(string userName) => _platform.LoginAsync(userName);

        public UniTask JoinChannelAsync(string channelId) => _platform.JoinChannelAsync(channelId);

        public void LeaveChannel() => _platform.LeaveChannel();
        public bool ParticipantIsMuted(string userName) => _platform.ParticipantIsMuted(userName);

        public bool IsOtherParticipantsMuted() => _platform.IsOtherParticipantsMuted();

        public void MuteParticipant(string userName, bool mute) => _platform.MuteParticipant(userName, mute);

        public UniTask DeinitializeAsync() => _platform.DeinitializeAsync();
    }
}