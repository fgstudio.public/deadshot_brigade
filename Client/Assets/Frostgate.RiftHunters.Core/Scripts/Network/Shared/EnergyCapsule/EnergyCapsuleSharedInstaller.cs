using Frostgate.RiftHunters.Core.Battle;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    [CreateAssetMenu(fileName = nameof(EnergyCapsuleSharedInstaller),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(EnergyCapsuleSharedInstaller))]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    public sealed class EnergyCapsuleSharedInstaller : ScriptableObjectInstaller
    {
        [Required, HideLabel, BoxGroup("Pool")]
        [SerializeField] private PrefabPoolConfig<EnergyCapsule, PathPoolConfig> _pool;

        public override void InstallBindings()
        {
            var instanceProvider = new PrefabInstanceProvider<EnergyCapsule>(_pool.PrefabSource);
            var pool = new EnergyCapsulePool(instanceProvider, _pool.PoolConfig);

            Container.Bind(typeof(EnergyCapsule), typeof(IClientEnergyCapsule), typeof(IServerEnergyCapsule))
                .FromInstance(_pool.PrefabSource).AsSingle();
            Container.BindInterfacesAndSelfTo<EnergyCapsulePool>().FromInstance(pool).AsSingle();
        }
    }
}