using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    /// <summary>
    /// Пул энергетических капсул с авто-упаковкой в пул после сбора.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class EnergyCapsulePool : AutoReleasingMonoPool<EnergyCapsule>
    {
        public EnergyCapsulePool([NotNull] PrefabInstanceProvider<EnergyCapsule> instanceProvider, [NotNull] IPoolConfig poolConfig)
            : base(instanceProvider, poolConfig) { }
    }
}
