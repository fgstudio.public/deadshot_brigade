using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    /// <summary>
    /// Пул тотемов с авто-упаковкой в пул после уничтожения.
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    public sealed class TotemPool : AutoReleasingMonoPool<Totem>
    {
        public TotemPool([NotNull] IPrefabInstanceProvider<Totem> instanceProvider, [NotNull] IPoolConfig poolConfig)
            : base(instanceProvider, poolConfig)
        { }
    }
}
