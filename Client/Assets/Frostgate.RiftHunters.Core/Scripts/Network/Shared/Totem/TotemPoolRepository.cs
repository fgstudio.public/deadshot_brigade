using System;
using System.Linq;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Network.Shared.Capturing
{
    public interface IReadOnlyTotemPoolRepository
    {
        IPool<Totem> this[Guid assetId] { get; }
        IPool<Totem> Get(Guid assetId);
    }

    /// <summary>
    /// Репозиторий пулов тотемов для получения для каждого типа ловушки своего пула.
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    public sealed class TotemPoolRepository : PoolRepository<Guid, Totem>, IReadOnlyTotemPoolRepository
    {
        public TotemPoolRepository([NotNull] ITotemPrefabRoster prefabRoster,
            [NotNull] IPoolConfig poolConfig, [NotNull] IPrefabFactory prefabFactory)
            : base(CreateDictionary(prefabRoster, poolConfig, prefabFactory)) { }

        private static Dictionary<Guid, IPool<Totem>> CreateDictionary(
            ITotemPrefabRoster prefabRoster, IPoolConfig poolConfig, IPrefabFactory prefabFactory) =>
            prefabRoster.Prefabs
                .ToDictionary
                (
                    keySelector: SelectKey,
                    elementSelector: x => SelectElement(x, poolConfig, prefabFactory)
                );

        private static Guid SelectKey(ITotem totem) => totem.Identity.assetId;

        private static IPool<Totem> SelectElement(ITotem totem, IPoolConfig poolConfig, IPrefabFactory prefabFactory) =>
            new TotemPool(SelectProvider(totem, prefabFactory), poolConfig);

        private static IPrefabInstanceProvider<Totem> SelectProvider(ITotem totem, IPrefabFactory prefabFactory) =>
            new DiPrefabInstanceProvider<Totem>(totem.GameObject!, prefabFactory);
    }
}
