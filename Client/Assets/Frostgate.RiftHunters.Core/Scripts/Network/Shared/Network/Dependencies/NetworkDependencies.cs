using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    [UsedImplicitly]
    public sealed class NetworkDependencies
    {
        [NotNull] public readonly ClientDependencies Client;
        [NotNull] public readonly SharedDependencies Shared;
        [NotNull] public readonly ServerDependencies Server;

        public NetworkDependencies([NotNull] ClientDependencies client,
            [NotNull] SharedDependencies shared, [NotNull] ServerDependencies server)
        {
            Client = client;
            Shared = shared;
            Server = server;
        }
    }
}
