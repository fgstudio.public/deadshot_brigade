using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.UnitEffect;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Network.Shared.KillStreak;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    [UsedImplicitly]
    public sealed class ServerDependencies
    {
        [NotNull] public readonly IDdaSystem DdaSystem;
        [NotNull] public readonly ObjectCommandSenderRepository CommandSenderRepository;
        [NotNull] public readonly ObjectCommandReceiverRepository CommandReceiverRepository;
        [NotNull] public readonly PlayersDieObserver PlayersDieObserver;
        [NotNull] public readonly UnitEffectStateFactory UnitEffectStateFactory;
        [NotNull] public readonly UnitEffectModelFactory UnitEffectModelFactory;
        [NotNull] public readonly IObjectCollection<BotArenaConfig, BotArena> BotArenaCollection;
        [NotNull] public readonly IEditableBotArenaState BotArenaState;
        [NotNull] public readonly BotRespawnState BotRespawnState;
        [NotNull] public readonly KillStreakState KillStreakState;
        [NotNull] public readonly IActualRespawnPoint ActualRespawnPoint;
        [NotNull] public readonly BotNamesRoster BotNamesRoster;

        public ServerDependencies(
            [NotNull] IDdaSystem ddaSystem,
            [NotNull] ObjectCommandSenderRepository commandSenderRepository,
            [NotNull] ObjectCommandReceiverRepository commandReceiverRepository,
            [NotNull] PlayersDieObserver playersDieObserver,
            [NotNull] UnitEffectStateFactory unitEffectStateFactory,
            [NotNull] UnitEffectModelFactory unitEffectModelFactory,
            [NotNull] BotArenaSceneConfiguration botArenaSceneConfiguration,
            [NotNull] IObjectCollection<BotArenaConfig, BotArena> botArenaCollection,
            [NotNull] IEditableBotArenaState botArenaState,
            [NotNull] BotRespawnState botRespawnState,
            [NotNull] KillStreakState killStreakState,
            [NotNull] ActualRespawnPoint actualRespawnPoint,
            [NotNull] IConfigAccessor<BotNamesRoster> namesRoster)
        {
            DdaSystem = ddaSystem;
            CommandSenderRepository = commandSenderRepository;
            CommandReceiverRepository = commandReceiverRepository;
            PlayersDieObserver = playersDieObserver;
            UnitEffectStateFactory = unitEffectStateFactory;
            UnitEffectModelFactory = unitEffectModelFactory;
            BotArenaCollection = botArenaCollection;
            BotArenaState = botArenaState;
            BotRespawnState = botRespawnState;
            KillStreakState = killStreakState;
            ActualRespawnPoint = actualRespawnPoint;
            BotNamesRoster = namesRoster.Config;
        }
    }
}