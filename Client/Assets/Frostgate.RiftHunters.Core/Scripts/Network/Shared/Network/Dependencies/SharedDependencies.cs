using Zenject;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Systems.Input;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.MissionScore;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    [UsedImplicitly]
    public sealed class SharedDependencies
    {
        [NotNull] public readonly DiContainer DiContainer;
        [NotNull] public readonly IRandom Random;
        [NotNull] public readonly AreaRandom AreaRandom;
        [NotNull] public readonly PlayerInput PlayerInput;
        [NotNull] public readonly IPrefabFactory PrefabFactory;
        [NotNull] public readonly SimulationTime SimulationTime;
        [NotNull] public readonly IGameStateMachine StateMachine;
        [NotNull] public readonly IAimTargetFinder AimTargetFinder;
        [NotNull] public readonly IMissionScoresConfig MissionScoreConfig;
        [NotNull] public readonly IConfigCollection<UnitConfig> UnitConfigs;
        [NotNull] public readonly IConfigCollection<ActivityConfig> ActivityConfigs;
        [NotNull] public readonly IConfigCollection<UnitEffectConfig> UnitEffectConfigs;
        [NotNull] public readonly IConfigCollection<UnitActionConfig> UnitActionConfigs;
        [NotNull] public readonly IConfigCollection<UnitEmotionConfig> UnitEmotionConfigs;
        [NotNull] public readonly IConfigCollection<Equipment> Equipments;

        [NotNull] public readonly BotArenaSceneConfiguration BotArenaSceneConfiguration;
        [NotNull] public readonly BotSpawnState BotSpawnState;
        [NotNull] public readonly SpawnAreaFinder SpawnAreaFinder;
        [NotNull] public readonly IReadOnlyBotRespawnState BotRespawnState;
        [NotNull] public readonly IReadOnlyBotArenaState BotArenaState;
        [NotNull] public readonly ObjectCollectionRepository<UnitNetwork> UnitCollections;
        [NotNull] public readonly ObjectCollectionRepository<ShootingTargetNetwork> ShootingTargetCollections;
        [NotNull] public readonly ObjectCollectionRepository<ITotem> TotemCollections;
        [NotNull] public readonly ObjectCollectionRepository<ITrap> TrapCollections;
        [NotNull] public readonly ObjectCollectionRepository<IMine> MineCollections;
        [NotNull] public readonly AimTargetNetIdCollection AimTargetNetIdCollection;
        [NotNull] public readonly AimTargetColliderCollection AimTargetColliderCollection;
        [NotNull] public readonly BulletProcessors BulletProcessors;
        [NotNull] public readonly IServiceRoster ServiceRoster;

        public SharedDependencies([NotNull] DiContainer diContainer, [NotNull] PlayerInput playerInput,
            [NotNull] IRandom random, [NotNull] AreaRandom areaRandom,
            [NotNull] IPrefabFactory prefabFactory, [NotNull] SimulationTime simulationTime,
            [NotNull] IGameStateMachine stateMachine, [NotNull] IAimTargetFinder aimTargetFinder,
            [NotNull] IMissionScoresConfig missionScoreConfig,
            [NotNull] IConfigCollection<UnitConfig> unitConfigs,
            [NotNull] IConfigCollection<UnitEffectConfig> unitEffectConfigs,
            [NotNull] IConfigCollection<UnitActionConfig> unitActionConfigs,
            [NotNull] IConfigCollection<UnitEmotionConfig> unitEmotionConfigs,
            [NotNull] IConfigCollection<Equipment> equipments,
            [NotNull] BotArenaSceneConfiguration botArenaSceneConfiguration,
            [NotNull] BotSpawnState botSpawnState,
            [NotNull] SpawnAreaFinder spawnAreaFinder,
            [NotNull] IReadOnlyBotRespawnState botRespawnState,
            [NotNull] IReadOnlyBotArenaState botArenaState,
            [NotNull] ObjectCollectionRepository<UnitNetwork> unitCollections,
            [NotNull] IConfigCollection<ActivityConfig> activityConfigs,
            [NotNull] ObjectCollectionRepository<ShootingTargetNetwork> shootingTargetCollections,
            [NotNull] ObjectCollectionRepository<ITotem> totemCollections,
            [NotNull] ObjectCollectionRepository<ITrap> trapCollections,
            [NotNull] AimTargetNetIdCollection aimTargetNetIdCollection,
            [NotNull] AimTargetColliderCollection aimTargetColliderCollection,
            [NotNull] ObjectCollectionRepository<IMine> mineCollections,
            [NotNull] IServiceRoster serviceRoster,
            [NotNull] BulletProcessors bulletProcessors,
            [NotNull] IObjectCollection<uint, UnitNetwork> networkPlayers)
        {
            DiContainer = diContainer;
            Random = random;
            AreaRandom = areaRandom;
            PlayerInput = playerInput;
            PrefabFactory = prefabFactory;
            SimulationTime = simulationTime;
            StateMachine = stateMachine;
            AimTargetFinder = aimTargetFinder;
            MissionScoreConfig = missionScoreConfig;
            UnitConfigs = unitConfigs;
            ActivityConfigs = activityConfigs;
            UnitEffectConfigs = unitEffectConfigs;
            UnitActionConfigs = unitActionConfigs;
            UnitEmotionConfigs = unitEmotionConfigs;
            Equipments = equipments;
            BotArenaSceneConfiguration = botArenaSceneConfiguration;
            BotSpawnState = botSpawnState;
            SpawnAreaFinder = spawnAreaFinder;
            BotRespawnState = botRespawnState;
            BotArenaState = botArenaState;
            UnitCollections = unitCollections;
            ShootingTargetCollections = shootingTargetCollections;
            TotemCollections = totemCollections;
            TrapCollections = trapCollections;
            MineCollections = mineCollections;
            AimTargetNetIdCollection = aimTargetNetIdCollection;
            AimTargetColliderCollection = aimTargetColliderCollection;
            ServiceRoster = serviceRoster;
            BulletProcessors = bulletProcessors;
        }
    }
}