using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Battle.Impact.Client;
using Frostgate.RiftHunters.Core.Battle.Client.Unit;
using Frostgate.RiftHunters.Core.Battle.Client.Vibration;
using Frostgate.RiftHunters.Core.Network.Client.Observing;
using Frostgate.RiftHunters.Core.Battle.Client.PathPainter;
using Frostgate.RiftHunters.Core.Network.Shared.KillStreak;
using Frostgate.RiftHunters.Core.Voice;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    [UsedImplicitly]
    public sealed class ClientDependencies
    {
        [NotNull] public readonly IUIBattleMediator UIBattleMediator;
        [NotNull] public readonly IConfigCollection<ImpactUIConfig> ImpactUIConfigs;
        [NotNull] public readonly BattleVibrationMediator VibrationMediator;
        [NotNull] public readonly PlayerInputAttacher InputAttacher;
        [NotNull] public readonly PathPainter PathPainter;
        [NotNull] public readonly IReadOnlyKillStreakState KillStreakState;
        [NotNull] public readonly ActivityObserverFactory ActivityObserverFactory;
        [NotNull] public readonly VoiceService VoiceService;

        public ClientDependencies(
            [NotNull] IUIBattleMediator uiBattleMediator,
            [NotNull] IConfigCollection<ImpactUIConfig> impactUIConfigs,
            [NotNull] PlayerInputAttacher inputAttacher,
            [NotNull] BattleVibrationMediator vibrationMediator,
            [NotNull] PathPainter pathPainter,
            [NotNull] KillStreakState killStreakState,
            [NotNull] ActivityObserverFactory activityObserverFactory, 
            [NotNull] VoiceService voiceService)
        {
            UIBattleMediator = uiBattleMediator;
            ImpactUIConfigs = impactUIConfigs;
            InputAttacher = inputAttacher;
            VibrationMediator = vibrationMediator;
            PathPainter = pathPainter;
            KillStreakState = killStreakState;
            ActivityObserverFactory = activityObserverFactory;
            VoiceService = voiceService;
        }
    }
}