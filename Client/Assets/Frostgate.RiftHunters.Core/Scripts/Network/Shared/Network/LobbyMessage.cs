using Mirror;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    public struct LobbyMessage : NetworkMessage
    {
        public ClientConnectionError ClientConnectionError;
    }
}