using Mirror;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    public readonly struct LobbyData : NetworkMessage
    {
        public readonly int HeroLevel;
        public readonly PlayerDto Player;
        public readonly UnitConfig SelectedUnit;
        public readonly Equipment[] Equipments;

        public LobbyData(PlayerDto player, UnitConfig selectedUnit, Equipment[] equipments, int heroLevel)
        {
            Player = player;
            SelectedUnit = selectedUnit;
            Equipments = equipments;
            HeroLevel = heroLevel;
        }

    }
}