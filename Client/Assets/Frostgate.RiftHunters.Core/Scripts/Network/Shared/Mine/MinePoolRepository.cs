using System;
using System.Linq;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    public interface IReadOnlyMinePoolRepository
    {
        IPool<Mine> this[Guid assetId] { get; }
        IPool<Mine> Get(Guid assetId);
    }

    /// <summary>
    /// Репозиторий пулов vby для получения для каждого типа ловушки своего пула.
    /// <see cref="https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5"/>
    /// </summary>
    public sealed class MinePoolRepository : PoolRepository<Guid, Mine>, IReadOnlyMinePoolRepository
    {
        public MinePoolRepository([NotNull] IMinePrefabRoster prefabRoster,
            [NotNull] IPoolConfig poolConfig, [NotNull] IPrefabFactory prefabFactory)
            : base(CreateDictionary(prefabRoster, poolConfig, prefabFactory)) { }

        private static Dictionary<Guid, IPool<Mine>> CreateDictionary(
            IMinePrefabRoster prefabRoster, IPoolConfig poolConfig, IPrefabFactory prefabFactory) =>
            prefabRoster.Prefabs
                .ToDictionary
                (
                    keySelector: SelectKey,
                    elementSelector: x => SelectElement(x, poolConfig, prefabFactory)
                );

        private static Guid SelectKey(IMine mine) => mine.Identity.assetId;

        private static IPool<Mine> SelectElement(IMine mine, IPoolConfig poolConfig, IPrefabFactory prefabFactory) =>
            new MinePool(SelectProvider(mine, prefabFactory), poolConfig);

        private static IPrefabInstanceProvider<Mine> SelectProvider(IMine mine, IPrefabFactory prefabFactory) =>
            new DiPrefabInstanceProvider<Mine>(mine.GameObject!, prefabFactory);
    }
}
