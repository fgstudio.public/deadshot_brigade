using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    /// <summary>
    /// Пул мин с авто-упаковкой в пул после уничтожения.
    /// <see cref="https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5"/>
    /// </summary>
    public sealed class MinePool : AutoReleasingMonoPool<Mine>
    {
        public MinePool([NotNull] IPrefabInstanceProvider<Mine> instanceProvider, [NotNull] IPoolConfig poolConfig)
            : base(instanceProvider, poolConfig)
        { }
    }
}
