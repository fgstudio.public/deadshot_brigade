using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;

namespace Frostgate.RiftHunters.Core.Network.Shared.Capturing
{
    /// <summary>
    /// Пул ловушек с авто-упаковкой в пул после уничтожения.
    /// <see cref="https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80"/>
    /// </summary>
    public sealed class TrapPool : AutoReleasingMonoPool<Trap>
    {
        public TrapPool([NotNull] IPrefabInstanceProvider<Trap> instanceProvider, [NotNull] IPoolConfig poolConfig)
            : base(instanceProvider, poolConfig)
        { }
    }
}
