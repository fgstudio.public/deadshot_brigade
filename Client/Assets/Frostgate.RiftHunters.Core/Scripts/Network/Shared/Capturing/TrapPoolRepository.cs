using System;
using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;

namespace Frostgate.RiftHunters.Core.Network.Shared.Capturing
{
    public interface IReadOnlyTrapPoolRepository
    {
        IPool<Trap> this[Guid assetId] { get; }
        IPool<Trap> Get(Guid assetId);
    }

    /// <summary>
    /// Репозиторий пулов ловушек для получения для каждого типа ловушки своего пула.
    /// <see cref="https://www.notion.so/c8c4c349ab554487b1ea1c50622a6e80"/>
    /// </summary>
    public sealed class TrapPoolRepository : PoolRepository<Guid, Trap>, IReadOnlyTrapPoolRepository
    {
        public TrapPoolRepository([NotNull] ITrapPrefabRoster prefabRoster,
            [NotNull] IPoolConfig poolConfig, [NotNull] IPrefabFactory prefabFactory)
            : base(CreateDictionary(prefabRoster, poolConfig, prefabFactory)) { }

        private static Dictionary<Guid, IPool<Trap>> CreateDictionary(
            ITrapPrefabRoster prefabRoster, IPoolConfig poolConfig, IPrefabFactory prefabFactory) =>
            prefabRoster.Prefabs
                .ToDictionary
                (
                    keySelector: SelectKey,
                    elementSelector: x => SelectElement(x, poolConfig, prefabFactory)
                );

        private static Guid SelectKey(ITrap trap) => trap.Identity.assetId;

        private static IPool<Trap> SelectElement(ITrap trap, IPoolConfig poolConfig, IPrefabFactory prefabFactory) =>
            new TrapPool(SelectProvider(trap, prefabFactory), poolConfig);

        private static IPrefabInstanceProvider<Trap> SelectProvider(ITrap trap, IPrefabFactory prefabFactory) =>
            new DiPrefabInstanceProvider<Trap>(trap.GameObject!, prefabFactory);
    }
}
