using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    /// <summary>
    /// Пул лут-боксов с авто-упаковкой в пул после сбора.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class LootBoxPool : AutoReleasingMonoPool<LootBox>
    {
        public LootBoxPool([NotNull] PrefabInstanceProvider<LootBox> instanceProvider, [NotNull] IPoolConfig poolConfig)
            : base(instanceProvider, poolConfig) { }
    }
}
