using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    public interface IReadOnlyLootBoxPoolRepository
    {
        IPool<LootBox> this[LootBoxType type] { get; }
        IPool<LootBox> Get(LootBoxType type);
    }

    /// <summary>
    /// Репозиторий пулов лут-боксов для получения для каждого типа лут-бокса своего пула.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class LootBoxPoolRepository : PoolRepository<LootBoxType, LootBox>, IReadOnlyLootBoxPoolRepository
    {
        public LootBoxPoolRepository([NotNull] LootBoxPrefabRoster prefabRoster, [NotNull] IPoolConfig poolConfig)
            : base(CreateDictionary(prefabRoster, poolConfig)) { }

        private static Dictionary<LootBoxType, IPool<LootBox>> CreateDictionary(LootBoxPrefabRoster prefabRoster, IPoolConfig poolConfig) =>
            prefabRoster.Types.ToDictionary
                (
                    keySelector: key => key,
                    elementSelector: x => SelectElement(x, prefabRoster, poolConfig)
                );

        private static IPool<LootBox> SelectElement(LootBoxType type, LootBoxPrefabRoster prefabRoster, IPoolConfig poolConfig) =>
            new LootBoxPool(SelectProvider(type, prefabRoster), poolConfig);

        private static PrefabInstanceProvider<LootBox> SelectProvider(LootBoxType type, LootBoxPrefabRoster prefabRoster) =>
            new(prefabRoster.FindPrefab(type).gameObject);
    }
}