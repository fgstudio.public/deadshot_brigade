using System.Collections.Generic;
using System.Linq;
using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Core.Network.Shared
{
    public static class NetworkConfigSync
    {
        private static IConfigCollection<Equipment> _equipments;
        private static IConfigCollection<UnitConfig> _unitConfigs;
        private static IConfigCollection<UnitEffectConfig> _unitEffectConfigs;
        private static IConfigCollection<UnitActionConfig> _unitActionConfigs;
        private static IConfigCollection<UnitEmotionConfig> _unitEmotionConfigs;

        public static void Init(
            IConfigCollection<Equipment> equipments,
            IConfigCollection<UnitConfig> unitConfigs,
            IConfigCollection<UnitEffectConfig> unitEffectConfigs,
            IConfigCollection<UnitActionConfig> unitActionConfigs,
            IConfigCollection<UnitEmotionConfig> unitEmotionConfigs)
        {
            _equipments = equipments;
            _unitConfigs = unitConfigs;
            _unitEffectConfigs = unitEffectConfigs;
            _unitActionConfigs = unitActionConfigs;
            _unitEmotionConfigs = unitEmotionConfigs;
        }

        [UsedImplicitly]
        public static void WriteUnitConfig(this NetworkWriter writer, UnitConfig unitConfig) =>
            writer.WriteString(unitConfig != null ? unitConfig.Id : null);

        [UsedImplicitly]
        public static UnitConfig ReadUnitConfig(this NetworkReader reader) =>
            _unitConfigs[reader.ReadString()];

        [UsedImplicitly]
        public static void WriteUnitEffectConfig(this NetworkWriter writer, UnitEffectConfig unitEffectConfigs) =>
            writer.WriteString(unitEffectConfigs != null ? unitEffectConfigs.Id : null);

        [UsedImplicitly]
        public static UnitEffectConfig ReadUnitEffectConfig(this NetworkReader reader) =>
            _unitEffectConfigs[reader.ReadString()];

        [UsedImplicitly]
        public static void WriteUnitActionConfig(this NetworkWriter writer, UnitActionConfig unitActionConfigs) =>
            writer.WriteString(unitActionConfigs != null ? unitActionConfigs.Id : null);

        [UsedImplicitly]
        public static UnitActionConfig ReadUnitActionConfig(this NetworkReader reader) =>
            _unitActionConfigs[reader.ReadString()];

        [UsedImplicitly]
        public static void WriteUnitEmotionConfig(this NetworkWriter writer, UnitEmotionConfig unitActionConfigs) =>
            writer.WriteString(unitActionConfigs != null ? unitActionConfigs.Id : null);

        [UsedImplicitly]
        public static UnitEmotionConfig ReadUnitEmotionConfig(this NetworkReader reader) =>
            _unitEmotionConfigs[reader.ReadString()];

        [UsedImplicitly]
        public static void WriteEquipment(this NetworkWriter writer, Equipment equipment) =>
            writer.WriteString(equipment != null ? equipment.Id : null);

        [UsedImplicitly]
        public static Equipment ReadEquipment(this NetworkReader reader) =>
            _equipments[reader.ReadString()];
    }
}