using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// Client Only. Doesn't work in host-mode.
    /// Компонент для спауна на клиенте сгенерированных сервером энергетических капсул.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class EnergyCapsuleSpawner : ISpawner
    {
        private readonly IClientEnergyCapsule _prefab;
        private readonly EnergyCapsuleFactory _factory;

        private bool _isDisposed;

        public EnergyCapsuleSpawner([NotNull] IClientEnergyCapsule prefab, [NotNull] EnergyCapsuleFactory factory)
        {
            _prefab = prefab;
            _factory = factory;

            RegisterPrefab(_prefab);
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            UnregisterPrefab(_prefab);
            _isDisposed = true;
        }

        private void RegisterPrefab(IClientEnergyCapsule prefab) =>
            NetworkClient.RegisterPrefab(prefab.GameObject, prefab.Identity.assetId, Spawn, Unspawn);

        private void UnregisterPrefab(IClientEnergyCapsule prefab) =>
            NetworkClient.UnregisterPrefab(prefab.GameObject);

        private GameObject Spawn(SpawnMessage spawnMessage)
        {
            IClientEnergyCapsule energyCapsule = _factory.Create(spawnMessage);
            return energyCapsule.GameObject;
        }

        private void Unspawn(GameObject gameObject)
        {
            var energyCapsule = gameObject.GetComponent<IClientEnergyCapsule>();
            energyCapsule.Destroy();
        }
    }
}