using Frostgate.RiftHunters.Core.Battle;
using Zenject;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    [CreateAssetMenu(fileName = nameof(EnergyCapsuleClientInstaller),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(EnergyCapsuleClientInstaller))]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    public sealed class EnergyCapsuleClientInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<EnergyCapsuleFactory>().AsSingle();
            Container.Bind<EnergyCapsuleSpawner>().AsSingle();
        }
    }
}