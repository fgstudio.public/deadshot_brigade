using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Network.Shared;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// Фабрика энергетических капсул. Проводит настройку выданных из пула объектов
    /// и инкапсулирует их данные, отдавай объекты по интерфейсу.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class EnergyCapsuleFactory
    {
        private readonly EnergyCapsulePool _pool;

        public EnergyCapsuleFactory([NotNull] EnergyCapsulePool pool) =>
            _pool = pool;

        public IClientEnergyCapsule Create(SpawnMessage spawnMessage)
        {
            EnergyCapsule energyCapsule = _pool.Get();

            energyCapsule.gameObject.transform.position = spawnMessage.position;
            energyCapsule.gameObject.transform.rotation = spawnMessage.rotation;
            energyCapsule.gameObject.transform.localScale = spawnMessage.scale;

            return energyCapsule;
        }
    }
}