using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// Client Only. Doesn't work in host-mode.
    /// Компонент для спауна на клиенте сгенерированных сервером мин
    /// <see cref="https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5"/>
    /// </summary>
    public sealed class MineSpawner : ISpawner
    {
        private readonly MineFactory _factory;
        private readonly IMinePrefabRoster _prefabRoster;
        private readonly ObjectCollectionRepository<IMine> _mineCollections;

        public MineSpawner(
            [NotNull] MineFactory factory,
            [NotNull] IMinePrefabRoster prefabRoster,
            [NotNull] ObjectCollectionRepository<IMine> mineCollections)
        {
            _factory = factory;
            _prefabRoster = prefabRoster;
            _mineCollections = mineCollections;

            RegisterPrefabs(_prefabRoster.Prefabs);
        }

        public void Dispose()
        {
            UnregisterPrefabs(_prefabRoster.Prefabs);
        }

        private void RegisterPrefabs(IEnumerable<IMine> prefabs)
        {
            foreach (IMine mine in prefabs)
                NetworkClient.RegisterPrefab(
                    mine.GameObject, mine.Identity.assetId, Spawn, Unspawn);
        }

        private void UnregisterPrefabs(IEnumerable<IMine> prefabs)
        {
            foreach (IMine mine in prefabs)
                NetworkClient.UnregisterPrefab(mine.GameObject);
        }

        private GameObject Spawn(SpawnMessage spawnMessage)
        {
            IMine mine = _factory.Create(spawnMessage.assetId, spawnMessage);

            if (mine.State.IsInitialized) AddToCollection(mine);
            else mine.State.Initialized += AddToCollection;

            return mine.GameObject;
        }


        private void Unspawn(GameObject gameObject)
        {
            var mine = gameObject.GetComponent<IMine>();
            mine.State.Initialized -= AddToCollection;
            RemoveFromCollection(mine);
            mine.Destroy();
        }

        private void AddToCollection(IMine mine) => _mineCollections.Get<uint>().Add(mine.Identity.netId, mine);
        private void RemoveFromCollection(IMine mine) => _mineCollections.Get<uint>().Remove(mine.Identity.netId);
    }
}