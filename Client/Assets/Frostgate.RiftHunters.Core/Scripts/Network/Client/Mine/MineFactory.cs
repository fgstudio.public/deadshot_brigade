using Mirror;
using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// Фабрика мин. Проводит настройку выданных из пула объектов
    /// и инкапсулирует их данные, отдавай объекты по интерфейсу.
    /// <see cref="https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5"/>
    /// </summary>
    public sealed class MineFactory
    {
        private readonly MinePoolRepository _poolRepository;

        public MineFactory([NotNull] MinePoolRepository poolRepository) =>
            _poolRepository = poolRepository;

        public IMine Create([NotNull] IMine prefab, SpawnMessage spawnMessage) =>
            Create(prefab.Identity.assetId, spawnMessage);

        public IMine Create(Guid assetId, SpawnMessage spawnMessage)
        {
            IPool<Mine> pool = _poolRepository[assetId];
            Mine mine = pool.Get();

            mine.gameObject.transform.position = spawnMessage.position;
            mine.gameObject.transform.rotation = spawnMessage.rotation;
            mine.gameObject.transform.localScale = spawnMessage.scale;

            return mine;
        }

    }
}
