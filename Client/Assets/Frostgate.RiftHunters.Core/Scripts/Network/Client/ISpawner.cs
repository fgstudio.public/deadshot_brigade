using System;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    public interface ISpawner : IDisposable { }
}
