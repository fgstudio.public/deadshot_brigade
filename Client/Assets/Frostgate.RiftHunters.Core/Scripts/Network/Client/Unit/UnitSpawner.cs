using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// Client Only. Doesn't work in host-mode.
    /// </summary>
    public class UnitSpawner : ISpawner
    {
        [NotNull] private readonly UnitNetwork _prefab;

        [NotNull] private readonly IPool<UnitNetwork> _unitPool;
        [NotNull] private readonly AsyncUnitCollectionsWrapper _collections;

        public UnitSpawner(
            [NotNull] UnitNetwork prefab,
            [NotNull] ObjectCollectionRepository<UnitNetwork> collections,
            [NotNull] IPrefabFactory prefabFactory)
        {
            _prefab = prefab;

            _collections = new(collections);
            _unitPool = CreatePool(prefab, prefabFactory);

            NetworkClient.RegisterPrefab(
                prefab.gameObject,
                prefab.NetworkIdentity.assetId, Spawn, Unspawn
            );
        }

        public void Dispose()
        {
            _unitPool.Dispose();
            _collections.Dispose();
            NetworkClient.UnregisterPrefab(_prefab.gameObject);
        }

        private IPool<UnitNetwork> CreatePool(UnitNetwork prefab, IPrefabFactory prefabFactory)
        {
            // TODO: организовать конфиг
            const int maxCount = 80;
            const int initialCount = 50;
            const string containerName = "Unit";

            var instanceProvider =
                new DiPrefabInstanceProvider<UnitNetwork>(prefab, prefabFactory);

            return NetworkClient.isHostClient
                ? new FakePool<UnitNetwork>(instanceProvider)
                : new MonoPool<UnitNetwork>(instanceProvider,
                    new NativePoolConfig(initialCount, maxCount, containerName));
        }

        private GameObject Spawn(SpawnMessage spawnMessage)
        {
            UnitNetwork unit = _unitPool.Get();

            var transform = unit.transform;
            transform.position = spawnMessage.position;
            transform.rotation = spawnMessage.rotation;
            transform.localScale = spawnMessage.scale;

            UnitNetworkState state = unit.UnitNetworkState;
            if (state.IsInitialized) OnInitialized(state);
            else state.Initialized += OnInitialized;

            state.Deinitializing += OnDeinitializing;

            return unit.gameObject;
        }

        private void Unspawn(GameObject gameObject)
        {
            var unit = gameObject.GetComponent<UnitNetwork>();
            unit.UnitNetworkState.Initialized -= OnInitialized;
            unit.UnitNetworkState.Deinitializing -= OnDeinitializing;

            _unitPool.Release(unit);
        }

        private void OnInitialized(UnitNetworkState state)
        {
            state.Initialized -= OnInitialized;
            _collections.Add(state.UnitNetwork);
        }

        // ReSharper disable once IdentifierTypo
        private void OnDeinitializing(UnitNetworkState state)
        {
            state.Deinitializing -= OnDeinitializing;
            _collections.Remove(state.UnitNetwork);
        }
    }
}
