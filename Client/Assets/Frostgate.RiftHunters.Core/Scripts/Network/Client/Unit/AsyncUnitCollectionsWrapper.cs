using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using System;
using System.Threading;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    public sealed class AsyncUnitCollectionsWrapper : IDisposable
    {
        [NotNull] private readonly ObjectCollectionRepository<UnitNetwork> _collections;

        [NotNull] private readonly Queue<Action> _actionsQueue = new();
        [NotNull] private readonly CancellationTokenSource _cts = new();

        public AsyncUnitCollectionsWrapper(
            [NotNull] ObjectCollectionRepository<UnitNetwork> collections)
        {
            _collections = collections;
            UpdateRoutine(_cts.Token).SuppressCancellationThrow();
        }

        public void Dispose()
        {
            _cts.Cancel();
            _cts.Dispose();
        }

        private async UniTask UpdateRoutine(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                if (_actionsQueue.TryDequeue(out Action action))
                    action();

                await UniTask.Yield();
            }
        }

        public void Add([NotNull] UnitNetwork unit) =>
            _actionsQueue.Enqueue(() =>
            {
                if (unit == null || unit.Identity == null) return;
                _collections.Get<uint>().Add(unit.Identity.netId, unit);

                if (unit.UnitNetworkState == null || unit.UnitNetworkState.ViewConfig == null) return;
                _collections.Get<Collider>().Add(unit.UnitNetworkState.ViewConfig.Body, unit);
                _collections.Get<Collider>().Add(unit.UnitNetworkState.ViewConfig.AimTarget, unit);
            });

        public void Remove([NotNull] UnitNetwork unit)
        {
            if (unit == null || unit.Identity == null) return;
            _collections.Get<uint>().Remove(unit.Identity.netId);

            if (unit.UnitNetworkState == null || unit.UnitNetworkState.ViewConfig == null) return;
            _collections.Get<Collider>().Remove(unit.UnitNetworkState.ViewConfig.Body);
            _collections.Get<Collider>().Remove(unit.UnitNetworkState.ViewConfig.AimTarget);
        }
    }
}