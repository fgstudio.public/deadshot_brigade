using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    public sealed class UnitStateMachineFactory
    {
        private readonly IServiceRoster _serviceRoster;
        private readonly IAimTargetFinder _aimTargetFinder;

        public UnitStateMachineFactory(
            [NotNull] IServiceRoster serviceRoster,
            [NotNull] IAimTargetFinder aimTargetFinder)
        {
            _serviceRoster = serviceRoster;
            _aimTargetFinder = aimTargetFinder;
        }

        public BehaviourStateMachineHandler Create([NotNull] UnitNetwork unit)
        {
            var stateFactory = new BehaviourStateFactory(unit, _serviceRoster, _aimTargetFinder);
            var stateRepository = new BehaviourStateRepository(stateFactory);
            var stateMachine = new BehaviourStateMachine(unit, stateRepository);

            return new BehaviourStateMachineHandler(unit, stateMachine);
        }
    }
}
