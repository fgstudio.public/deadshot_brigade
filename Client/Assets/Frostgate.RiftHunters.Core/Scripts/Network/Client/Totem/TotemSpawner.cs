using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// Client Only. Doesn't work in host-mode.
    /// Компонент для спауна на клиенте сгенерированных сервером тотемов
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    public sealed class TotemSpawner : ISpawner
    {
        private readonly TotemFactory _factory;
        private readonly ITotemPrefabRoster _prefabRoster;
        private readonly ObjectCollectionRepository<ITotem> _totemCollections;

        public TotemSpawner(
            [NotNull] TotemFactory factory,
            [NotNull] ITotemPrefabRoster prefabRoster,
            [NotNull] ObjectCollectionRepository<ITotem> totemCollections)
        {
            _factory = factory;
            _prefabRoster = prefabRoster;
            _totemCollections = totemCollections;

            RegisterPrefabs(_prefabRoster.Prefabs);
        }

        public void Dispose()
        {
            UnregisterPrefabs(_prefabRoster.Prefabs);
        }

        private void RegisterPrefabs(IEnumerable<ITotem> prefabs)
        {
            foreach (ITotem totem in prefabs)
                NetworkClient.RegisterPrefab(
                    totem.GameObject, totem.Identity.assetId, Spawn, Unspawn);
        }

        private void UnregisterPrefabs(IEnumerable<ITotem> prefabs)
        {
            foreach (ITotem totem in prefabs)
                NetworkClient.UnregisterPrefab(totem.GameObject);
        }

        private GameObject Spawn(SpawnMessage spawnMessage)
        {
            ITotem totem = _factory.Create(spawnMessage.assetId, spawnMessage);

            if (totem.State.IsInitialized) AddToCollections(totem);
            else totem.State.Initialized += AddToCollections;

            return totem.GameObject;
        }

        private void Unspawn(GameObject gameObject)
        {
            var totem = gameObject.GetComponent<ITotem>();
            totem.State.Initialized -= AddToCollections;
            RemoveFromCollections(totem);
            totem.Destroy();
        }

        private void AddToCollections(ITotem totem)
        {
            _totemCollections.Get<uint>().Add(totem.Identity.netId, totem);
            _totemCollections.Get<Collider>().Add(totem.Physics.Collider, totem);
        }

        private void RemoveFromCollections(ITotem totem)
        {
            _totemCollections.Get<uint>().Remove(totem.Identity.netId);
            _totemCollections.Get<Collider>().Remove(totem.Physics.Collider);
        }
    }
}