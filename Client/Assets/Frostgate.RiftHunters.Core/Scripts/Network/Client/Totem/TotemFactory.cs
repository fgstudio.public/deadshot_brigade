using System;
using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Network.Shared.Capturing;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// Фабрика тотемов. Проводит настройку выданных из пула объектов
    /// и инкапсулирует их данные, отдавай объекты по интерфейсу.
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    public sealed class TotemFactory
    {
        private readonly TotemPoolRepository _poolRepository;

        public TotemFactory([NotNull] TotemPoolRepository poolRepository) =>
            _poolRepository = poolRepository;

        public ITotem Create([NotNull] ITotem prefab, SpawnMessage spawnMessage) =>
            Create(prefab.Identity.assetId, spawnMessage);

        public ITotem Create(Guid assetId, SpawnMessage spawnMessage)
        {
            IPool<Totem> pool = _poolRepository[assetId];
            Totem totem = pool.Get();

            totem.gameObject.transform.position = spawnMessage.position;
            totem.gameObject.transform.rotation = spawnMessage.rotation;
            totem.gameObject.transform.localScale = spawnMessage.scale;

            return totem;
        }
    }
}
