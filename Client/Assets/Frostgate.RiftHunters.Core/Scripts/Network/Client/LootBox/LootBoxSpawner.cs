using System;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Systems.Pool;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// Client Only. Doesn't work in host-mode.
    /// Компонент для спауна на клиенте сгенерированных сервером лут-боксов
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class LootBoxSpawner : ISpawner
    {
        private readonly LootBoxPrefabRoster _prefabRoster;
        private readonly LootBoxFactory _lootBoxFactory;

        private bool _isDisposed;

        public LootBoxSpawner([NotNull] LootBoxPrefabRoster prefabRoster, [NotNull] IPoolConfig poolConfig)
        {
            _prefabRoster = prefabRoster;

            var poolRepository = new LootBoxPoolRepository(prefabRoster, poolConfig);
            _lootBoxFactory = new LootBoxFactory(poolRepository);

            RegisterPrefabs(_prefabRoster.Prefabs);
        }

        public void Dispose()
        {
            if (_isDisposed) return;

            UnregisterPrefabs(_prefabRoster.Prefabs);

            _isDisposed = true;
        }

        private void RegisterPrefabs(IEnumerable<IClientLootBox> prefabs)
        {
            foreach (IClientLootBox lootBox in prefabs)
                NetworkClient.RegisterPrefab(
                    lootBox.GameObject, lootBox.Identity.assetId, Spawn, Unspawn);
        }

        private void UnregisterPrefabs(IEnumerable<IClientLootBox> prefabs)
        {
            foreach (IClientLootBox lootBox in prefabs)
                NetworkClient.UnregisterPrefab(lootBox.GameObject);
        }

        private GameObject Spawn(SpawnMessage spawnMessage)
        {
            LootBoxType type = FindType(spawnMessage.assetId);
            IClientLootBox lootBox = _lootBoxFactory.Create(type, spawnMessage);

            return lootBox.GameObject;
        }

        private void Unspawn(GameObject gameObject)
        {
            var lootBox = gameObject.GetComponent<IClientLootBox>();
            lootBox.Destroy();
        }

        private LootBoxType FindType(Guid assetId) => _prefabRoster.FindType(assetId);
    }
}