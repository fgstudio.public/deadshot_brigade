using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Network.Shared;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// Фабрика лут-боксов. Проводит настройку выданных из пула объектов
    /// и инкапсулирует их данные, отдавай объекты по интерфейсу.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class LootBoxFactory
    {
        private readonly IReadOnlyLootBoxPoolRepository _poolRepository;

        public LootBoxFactory([NotNull] IReadOnlyLootBoxPoolRepository poolRepository) =>
            _poolRepository = poolRepository;

        public IClientLootBox Create(LootBoxType type, SpawnMessage spawnMessage)
        {
            LootBox lootBox = _poolRepository[type].Get();

            lootBox.gameObject.transform.position = spawnMessage.position;
            lootBox.gameObject.transform.rotation = spawnMessage.rotation;
            lootBox.gameObject.transform.localScale = spawnMessage.scale;

            return lootBox;
        }
    }
}