using Mirror;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.UI.Hud;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class TrapCollectionObserver : ObjectObserver<IReadOnlyObjectCollection<uint, ITrap>>
    {
        private readonly Dictionary<ITrap, ITrapState.UnitAction> _unitCapturedActions = new();
        private readonly Dictionary<ITrap, ITrapState.UnitAction> _unitLostActions = new();

        private readonly ISpecialProgressBarsPanel _panel;

        public TrapCollectionObserver([NotNull] ISpecialProgressBarsPanel panel) =>
            _panel = panel;

        protected override void Subscribe(IReadOnlyObjectCollection<uint, ITrap> observable)
        {
            observable.Added += OnAdded;
            observable.Removed += OnRemoved;

            foreach (ITrap trap in observable)
                OnAdded(trap);
        }

        protected override void Unsubscribe(IReadOnlyObjectCollection<uint, ITrap> observable)
        {
            observable.Added -= OnAdded;
            observable.Removed -= OnRemoved;

            foreach (ITrap trap in observable)
                OnRemoved(trap);
        }

        private void OnAdded(ITrap trap)
        {
            SubscribeTrap(trap);

            uint capturedNetId = trap.State.CapturedUnitId;
            if (capturedNetId != default)
                ShowProgressIfCapturedLocal(trap, capturedNetId);
        }

        private void OnRemoved(ITrap trap)
        {
            UnsubscribeTrap(trap);

            uint lostNetId = trap.State.CapturedUnitId;
            if (lostNetId != default)
                HideProgressIfLostLocal(trap, lostNetId);
        }

        private void SubscribeTrap(ITrap trap)
        {
            void CapturedAction(uint netId) => ShowProgressIfCapturedLocal(trap, netId);
            void LostAction(uint netId) => HideProgressIfLostLocal(trap, netId);

            trap.State.UnitCaptured += CapturedAction;
            trap.State.UnitLost += LostAction;

            _unitCapturedActions[trap] = CapturedAction;
            _unitLostActions[trap] = LostAction;
        }

        private void UnsubscribeTrap(ITrap trap)
        {
            ITrapState.UnitAction capturedAction = _unitCapturedActions[trap];
            ITrapState.UnitAction lostAction = _unitLostActions[trap];

            trap.State.UnitCaptured -= capturedAction;
            trap.State.UnitLost -= lostAction;

            _unitCapturedActions.Remove(trap);
            _unitLostActions.Remove(trap);
        }

        private void ShowProgressIfCapturedLocal(ITrap trap, uint capturedNetId)
        {
            if (IsLocalPlayer(capturedNetId))
                _panel.ShowProgressFor(trap);
        }

        private void HideProgressIfLostLocal(ITrap trap, uint lostNetId)
        {
            if (IsLocalPlayer(lostNetId))
                _panel.HideProgressFor(trap);
        }

        private bool IsLocalPlayer(uint netId) =>
            NetworkClient.localPlayer?.netId == netId;
    }
}