﻿using System;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Reanimation;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    // TODO: плохо прослеживается логика сущности. некорректное определение Done статуса. может быть утечка.
    public class RespawnTimerHandler : IDisposable
    {
        public event Action<RespawnTimerHandler> Done;

        public readonly IReanimationMechanic ReanimationMechanic;

        private readonly ITimer _timer;
        public IReadOnlyTimer Timer => _timer;

        public RespawnTimerHandler(IReanimationMechanic reanimationMechanic, Timer timer)
        {
            _timer = timer;
            ReanimationMechanic = reanimationMechanic;

            _timer.Stopped += OnTimerStopped;
            ReanimationMechanic.Activated.AddListener(OnReanimationActivated);
        }

        public void Dispose()
        {
            _timer.Stopped -= OnTimerStopped;
            ReanimationMechanic.Activated.RemoveListener(OnReanimationActivated);

            _timer?.Dispose();
        }

        public void Start() => _timer.Start();

        // TODO: не понятно, почему Done только при остановке таймера. а когда таймер завершается?
        private void OnTimerStopped() => Done?.Invoke(this);
        private void OnReanimationActivated() => _timer.Stop();
    }
}