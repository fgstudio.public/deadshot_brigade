﻿using Mirror;
using System;
using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.UI.Hud;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    // TODO: много похожих ответственностей. требуется докомпозиция
    public sealed class RespawnStateObserver : ObjectObserver<IReadOnlyBotRespawnState>
    {
        private readonly SimulationTime _simulationTime;
        private readonly IUIBattleMediator _uiBattleMediator;
        private readonly PlayersCache _playersCache; //  TODO: удалить, когда UnitCollection будет на uint

        private readonly HashSet<RespawnTimerHandler> _handlers = new();

        public RespawnStateObserver(
            [NotNull] SimulationTime simulationTime,
            [NotNull] IUIBattleMediator uiBattleMediator,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> units)
        {
            _simulationTime = simulationTime;
            _uiBattleMediator = uiBattleMediator;
            _playersCache = new PlayersCache(units);
        }

        protected override void OnDisposed() =>
            _handlers.ToArray().ForEach(RemoveHandler);

        protected override void OnSet(IReadOnlyBotRespawnState observable)
        {
            if (observable.IsInitialized)
                OnInitialized();
        }

        protected override void Subscribe(IReadOnlyBotRespawnState observable)
        {
            observable.Changed.AddListener(OnChanged);
            observable.Initialized.AddListener(OnInitialized);

        }

        protected override void Unsubscribe(IReadOnlyBotRespawnState observable)
        {
            observable.Changed.RemoveListener(OnChanged);
            observable.Initialized.RemoveListener(OnInitialized);
        }

        private void OnInitialized() =>
            Observable.RespawnUnitsData.ForEach(kv => OnChanged(kv.Key, kv.Value));

        private void OnChanged(uint netId, RespawnData respawnData)
        {
            if (!_playersCache.IsCached(netId))
                _playersCache.Cache(netId);

            bool isPlayerLocal = NetworkClient.localPlayer.netId == netId;
            if (isPlayerLocal) InitializeRespawnUi(netId, respawnData);
            else CreateRespawnMarkerForAlly(netId, respawnData);
        }

        private void CreateRespawnMarkerForAlly(uint netId, RespawnData respawnData)
        {
            if (!_playersCache.TryGet(netId, out UnitNetwork player))
                return;

            IReanimationPanel reanimationPointers = _uiBattleMediator.Hud.PointerPanels.ReanimationPanel;

            switch (respawnData.Status)
            {
                case RespawnStatus.Respawning:
                    double elapsedSecs = _simulationTime.Time - respawnData.ServerEventTime;
                    double actualIntervalSecs = respawnData.IntervalTime - elapsedSecs;
                    bool hasTimer = actualIntervalSecs > 0;

                    if (hasTimer)
                    {
                        RespawnTimerHandler handler = CreateHandler(player, actualIntervalSecs, OnHandlerMarkerDone);
                        reanimationPointers.SetTarget(player.ReanimationMechanic, handler.Timer);
                    }
                    else
                    {
                        reanimationPointers.SetTarget(player.ReanimationMechanic);
                    }

                    break;

                case RespawnStatus.Done:
                case RespawnStatus.Paused:
                    reanimationPointers.RemoveTarget(player.ReanimationMechanic);
                    break;
            }
        }

        private void InitializeRespawnUi(uint netId, RespawnData respawnData)
        {
            if (!_playersCache.TryGet(netId, out UnitNetwork player))
                return;

            UIReanimationMediator reanimationMediator = _uiBattleMediator.ReanimationMediator;

            switch (respawnData.Status)
            {
                case RespawnStatus.Respawning:
                    double elapsedSecs = _simulationTime.Time - respawnData.ServerEventTime;
                    double actualIntervalSecs = respawnData.IntervalTime - elapsedSecs;
                    bool hasTimer = actualIntervalSecs > 0;

                    IReadOnlyTimer timer = hasTimer ? CreateHandler(player, actualIntervalSecs).Timer : null;
                    reanimationMediator.SetReanimationTimer(timer);

                    break;

                case RespawnStatus.Paused:
                    reanimationMediator.EnableReanimationMode();
                    break;
            }
        }

        private RespawnTimerHandler CreateHandler(UnitNetwork player, double actualIntervalSecs,
            Action<RespawnTimerHandler> doneCallback = null)
        {
            var handler = new RespawnTimerHandler(
                player.ReanimationMechanic,
                new Timer(TimeSpan.FromSeconds(actualIntervalSecs)));

            handler.Done += doneCallback;
            _handlers.Add(handler);

            handler.Start();
            return handler;
        }

        private void RemoveHandler(RespawnTimerHandler handler)
        {
            handler.Done -= OnHandlerMarkerDone;
            handler.Dispose();

            _handlers.Remove(handler);
        }

        private void OnHandlerMarkerDone(RespawnTimerHandler handler)
        {
            _uiBattleMediator.Hud.PointerPanels
                .ReanimationPanel.RemoveTarget(handler.ReanimationMechanic);

            RemoveHandler(handler);
        }
    }
}