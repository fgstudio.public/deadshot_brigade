using Mirror;
using JetBrains.Annotations;
using System.Linq;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    //  TODO: удалить, когда UnitCollection будет на uint
    public sealed class PlayersCache
    {
        private readonly Dictionary<uint, UnitNetwork> _players = new();
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _units;

        public PlayersCache([NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> units) =>
            _units = units;

        public bool TryGet(uint netId, out UnitNetwork player) =>
            _players.TryGetValue(netId, out player);

        public bool IsCached(uint netId) =>
            _players.ContainsKey(netId);

        public void Cache(uint netId)
        {
            UnitNetwork player = _units.FirstOrDefault(u => u.netId == netId);

            if (player != null)
                _players[netId] = player;
        }
    }
}