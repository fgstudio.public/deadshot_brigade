﻿using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Systems.Input;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Voice;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class VoiceInputObserver : ObjectObserver<PlayerInput>
    {
        [NotNull] private readonly ISelfVoiceController _selfController;
        [NotNull] private readonly IParticipantsVoiceController _participantsController;
        [NotNull] private readonly UIVoiceChatMenu _voiceChatMenu;

        public VoiceInputObserver([NotNull] ISelfVoiceController selfController,
            [NotNull] IParticipantsVoiceController participantsController, [NotNull] UIVoiceChatMenu voiceChatMenu)
        {
            _selfController = selfController;
            _participantsController = participantsController;
            _voiceChatMenu = voiceChatMenu;
        }

        protected override void Subscribe(PlayerInput observable)
        {
            observable.MuteSelfVoice.AddListener(OnMuteSelfVoice);
            observable.MuteParticipantsVoice.AddListener(OnMuteParticipantsVoice);
        }

        protected override void Unsubscribe(PlayerInput observable)
        {
            observable.MuteSelfVoice.RemoveListener(OnMuteSelfVoice);
            observable.MuteParticipantsVoice.RemoveListener(OnMuteParticipantsVoice);
        }

        private void OnMuteSelfVoice()
        {
            _selfController.MuteSelf = !_selfController.MuteSelf;
        }

        private void OnMuteParticipantsVoice()
        {
            _voiceChatMenu.gameObject.SetActive(true);
        }
    }
}