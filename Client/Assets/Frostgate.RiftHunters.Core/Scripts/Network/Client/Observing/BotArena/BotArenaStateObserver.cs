﻿using Mirror;
using UnityEngine;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class BotArenaStateObserver : ObjectObserver<IReadOnlyBotArenaState>
    {
        private readonly IUIBattleMediator _uiBattleMediator;
        private readonly ActivityObserverFactory _activityObserverFactory;
        private readonly BotArenaSceneConfiguration _botArenaSceneConfiguration;

        private readonly CancellationTokenSource _cts = new();
        private readonly List<ActivityObserver> _activityObservers = new();

        private Transform LocalPlayer => NetworkClient.localPlayer?.transform;

        public BotArenaStateObserver(
            [NotNull] IUIBattleMediator uiBattleMediator,
            [NotNull] ActivityObserverFactory activityObserverFactory,
            [NotNull] BotArenaSceneConfiguration botArenaSceneConfiguration)
        {
            _uiBattleMediator = uiBattleMediator;
            _activityObserverFactory = activityObserverFactory;
            _botArenaSceneConfiguration = botArenaSceneConfiguration;
        }

        protected override void OnDisposed()
        {
            _cts.Cancel();
            _cts.Dispose();
            DisposeActivityObservers();
        }

        protected override async void Subscribe(IReadOnlyBotArenaState botArenaState)
        {
            if (LocalPlayer == null)
            {
                CancellationToken token = _cts.Token;
                await UniTask.WaitWhile(() => LocalPlayer == null, cancellationToken: token);
                if (token.IsCancellationRequested) return;
            }

            botArenaState.DataUpdated.AddListener(OnDataUpdated);

            if (botArenaState.IsInitialized) OnInitialized();
            else botArenaState.Initialized.AddListener(OnInitialized);
        }

        protected override void Unsubscribe(IReadOnlyBotArenaState botArenaState)
        {
            botArenaState.DataUpdated.RemoveListener(OnDataUpdated);
            botArenaState.Initialized.RemoveListener(OnInitialized);
        }

        private void OnInitialized()
        {
            SetAutoRespawnModeEnabled(true);
            OnDataUpdated(Observable.CurrentArenaId);
        }

        private void OnDataUpdated(string arenaId)
        {
            if (Observable.AreAllArenasCompleted)
                SetAutoRespawnModeEnabled(true);

            if (arenaId != Observable.CurrentArenaId) 
                return;

            if (TryGetArenaConfig(arenaId, out BotArenaConfig arenaConfig))
            {
                bool isAutoRespawn = arenaConfig.RespawnData
                    .RespawnTypeFlags.HasFlag(RespawnType.ByTimer);
                SetAutoRespawnModeEnabled(isAutoRespawn);
            }

            DisposeActivityObservers();

            if (TryGetArenaActivities(arenaId, out IReadOnlyList<ActivityTaskSceneData> sceneData))
                CreateActivityObservers(sceneData);
        }

        private bool TryGetArenaConfig(string id, out BotArenaConfig arenaConfig)
        {
            arenaConfig = _botArenaSceneConfiguration.ArenaData
                .FirstOrDefault(data => data.Config.Id == id)?.Config;
            return arenaConfig != null;
        }

        private bool TryGetArenaActivities(string id, out IReadOnlyList<ActivityTaskSceneData> sceneData)
        {
            sceneData = _botArenaSceneConfiguration.ArenaData
                .FirstOrDefault(data => data.Config.Id == id)?.Activities.SelectMany(a => a.TasksData).ToArray();
            return sceneData != null;
        }
        
        /// <param name="isEnabled">
        /// Если true, то UI не показывается.
        /// </param>
        private void SetAutoRespawnModeEnabled(bool isEnabled)
        {
            if (isEnabled)
                _uiBattleMediator.EnableAutoRespawn();
            else
                _uiBattleMediator.DisableAutoRespawn();
        }

        private void DisposeActivityObservers()
        {
            _activityObservers.ForEach(t => t.Dispose());
            _activityObservers.Clear();
        }

        private void CreateActivityObservers(IReadOnlyList<ActivityTaskSceneData> sceneData)
        {
            foreach (ActivityTaskSceneData data in sceneData)
            {
                if (data.State == null)
                    continue;

                ActivityObserver activityObserver =
                    _activityObserverFactory.Create(data, LocalPlayer);

                _activityObservers.Add(activityObserver);
            }
        }
    }
}