﻿using System;
using System.Collections.Generic;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Client.PathPainter;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class ActivityObserver : IDisposable
    {
        [NotNull] private readonly ILogger _logger;
        [NotNull] private readonly Transform _localPlayer;
        [NotNull] private readonly PathPainter _pathPainter;
        [NotNull] private readonly IReadOnlyActivityTaskState _model;
        [NotNull, ItemNotNull] private readonly IReadOnlyList<Transform> _staticTargets;

        public ActivityObserver(
            [NotNull] ILogger logger,
            [NotNull] Transform localPlayer,
            [NotNull] PathPainter pathPainter,
            [NotNull] IReadOnlyActivityTaskState model,
            [NotNull, ItemNotNull] IReadOnlyList<Transform> staticTargets)
        {
            _model = model;
            _logger = logger;
            _pathPainter = pathPainter;
            _localPlayer = localPlayer;
            _staticTargets = staticTargets;

            _model.Activated.AddListener(OnActivated);
            _model.Completed.AddListener(OnCompleted);
            _model.Deactivated.AddListener(OnDeactivated);

            if (_model.Status == ActivityStatus.Active)
                OnActivated();
        }

        public void Dispose()
        {
            _model.Activated.RemoveListener(OnActivated);
            _model.Completed.RemoveListener(OnCompleted);
            _model.Deactivated.RemoveListener(OnDeactivated);

            _staticTargets.ForEach(_pathPainter.RemoveTarget);
        }

        private void OnActivated()
        {
            _staticTargets.ForEach(t => _pathPainter.SetTarget(_localPlayer.transform, t));
            _logger.Log($"Activity Activated: |{_model}|");
        }

        private void OnDeactivated()
        {
            _staticTargets.ForEach(_pathPainter.RemoveTarget);
            _logger.Log($"Activity Deactivated: |{_model}|");
        }

        private void OnCompleted()
        {
            _staticTargets.ForEach(_pathPainter.RemoveTarget);
            _logger.Log($"Activity Completed: |{_model}|");
        }
    }
}