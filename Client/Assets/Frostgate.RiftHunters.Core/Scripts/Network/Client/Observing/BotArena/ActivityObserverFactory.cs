﻿using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Client.PathPainter;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class ActivityObserverFactory
    {
        [NotNull] private readonly PathPainter _pathPainter;

        public ActivityObserverFactory([NotNull] PathPainter pathPainter) =>
            _pathPainter = pathPainter;

        public ActivityObserver Create([NotNull] ActivityTaskSceneData taskSceneData, [NotNull] Transform localPlayer)
        {
            ILogger logger = LoggerFactory.CreateLogger<ActivityObserver>();

            return new ActivityObserver(logger, localPlayer, _pathPainter,
                (IReadOnlyActivityTaskState)taskSceneData.State, taskSceneData.StaticTargets);
        }
    }
}