using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.UI.Hud.Pointing;
using Frostgate.RiftHunters.Core.UI.Hud.TotemPanel;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class TotemCollectionObserver : ObjectObserver<IReadOnlyObjectCollection<uint, ITotem>>
    {
        private readonly ITotemsPanel _hpPanel;
        private readonly ITotemPanel _pointerPanel;

        public TotemCollectionObserver([NotNull] ITotemsPanel hpPanel, [NotNull] ITotemPanel pointerPanel)
        {
            _hpPanel = hpPanel;
            _pointerPanel = pointerPanel;
        }

        protected override void Subscribe(IReadOnlyObjectCollection<uint, ITotem> observable)
        {
            observable.Added += OnAdded;
            observable.Removed += OnRemoved;

            foreach (ITotem totem in observable)
                OnAdded(totem);
        }

        protected override void Unsubscribe(IReadOnlyObjectCollection<uint, ITotem> observable)
        {
            observable.Added -= OnAdded;
            observable.Removed -= OnRemoved;

            foreach (ITotem totem in observable)
                OnRemoved(totem);
        }

        private void OnAdded(ITotem totem)
        {
            _hpPanel.TrackTotemHp(totem);
            _pointerPanel.SetTarget(totem.GameObject!.transform);
        }

        private void OnRemoved(ITotem totem)
        {
            _hpPanel.UntrackTotemHp(totem);
            _pointerPanel.RemoveTarget(totem.GameObject!.transform);
        }
    }
}