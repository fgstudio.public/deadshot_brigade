﻿using System.Collections;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.UI.Hud;
using Frostgate.RiftHunters.Core.Utils;
using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public class UnitEmotionObserver : ObjectObserver<IReadOnlyObjectCollection<uint, UnitNetwork>>
    {
        private readonly ITeamPanel _teamPanel;
        private readonly IAllyPanel _allyPanel;

        private readonly Dictionary<uint, UnitNetwork> _players = new();
        private readonly Dictionary<uint, Coroutine> _coroutines = new();

        private ILogger _logger;

        public UnitEmotionObserver(ITeamPanel teamPanel, IAllyPanel allyPanel, ILogger logger)
        {
            _teamPanel = teamPanel;
            _allyPanel = allyPanel;
            _logger = logger;
        }

        protected override void Subscribe(IReadOnlyObjectCollection<uint, UnitNetwork> collection)
        {
            collection.Added += OnAdded;
            collection.Removed += OnRemoved;
            collection.Values.ForEach(OnAdded);
        }

        protected override void Unsubscribe(IReadOnlyObjectCollection<uint, UnitNetwork> collection)
        {
            collection.Added -= OnAdded;
            collection.Removed -= OnRemoved;
        }

        private void OnAdded(UnitNetwork unit)
        {
            if (!_players.ContainsKey(unit.netId) && unit.UnitType == BattleUnitType.Player)
            {
                _players[unit.netId] = unit;
                unit.Emotion.ApplyEmotion += OnApplyEmotion;
                unit.Emotion.ClearEmotion += OnClearEmotion;

                _logger.Log($"Add player |{unit.name}| id |{unit.netId}|");
            }
        }

        private void OnRemoved(UnitNetwork unit)
        {
            if (_players.ContainsKey(unit.netId))
            {
                ClearEmotion(unit.netId);

                unit.Emotion.ApplyEmotion -= OnApplyEmotion;
                unit.Emotion.ClearEmotion -= OnClearEmotion;
                _players.Remove(unit.netId);

                _allyPanel.RemoveTarget(unit.Transform);

                _logger.Log($"Remove player |{unit.name}|");
            }
        }

        private void OnApplyEmotion(uint netId, UnitEmotionConfig config)
        {
            ClearEmotion(netId);

            if (_players.TryGetValue(netId, out UnitNetwork targetUnit))
            {
                if (targetUnit == null)
                    return;

                _coroutines[netId] = CoroutineStarter.I.Execute(ShowEmotion(targetUnit, config));
                _logger.Log($"Apply emotion |{config.Name}| |{targetUnit.name}|");
            }
        }

        private void OnClearEmotion(uint netId)
        {
            ClearEmotion(netId);
        }

        private void ClearEmotion(uint netId)
        {
            if (_coroutines.ContainsKey(netId))
            {
                if (_coroutines[netId] != null)
                    CoroutineStarter.I.Break(_coroutines[netId]);
                _coroutines[netId] = null;
            }

            if (_players.TryGetValue(netId, out UnitNetwork targetUnit))
            {
                if (targetUnit == null)
                    return;

                _allyPanel.SetTarget(targetUnit.Transform, targetUnit.UnitNetworkState.UnitConfig.RoleIcon);
                _teamPanel.UpdateTeammatePanelEmotion(targetUnit, null);
                targetUnit.View.EmotionUnitView.ClearEmotion();

                _logger.Log($"Clear emotions |{targetUnit.name}|");
            }
        }

        private IEnumerator ShowEmotion(UnitNetwork target, UnitEmotionConfig config)
        {
            target.View.EmotionUnitView.ShowEmotion(config);
            _teamPanel.UpdateTeammatePanelEmotion(target, config);
            if (IsAllyUnit(target))
                _allyPanel.SetTarget(target.Transform, config.Sprite, false, true);

            yield return new WaitForSeconds(config.ShowDuration);

            ClearEmotion(target.netId);
        }

        private bool IsAllyUnit(UnitNetwork unit) =>
            !unit.Identity.isLocalPlayer &&
            unit.UnitNetworkState.UnitType == BattleUnitType.Player;
    }
}