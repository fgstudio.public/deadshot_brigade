﻿using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.UI.Input;
using Frostgate.RiftHunters.Core.Voice;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class ParticipantsVoiceObserver : ObjectObserver<IParticipantsVoiceController>
    {
        [NotNull] private readonly IVoiceChatButton _muteParticipantsButton;

        private IParticipantsVoiceController _observable;

        public ParticipantsVoiceObserver([NotNull] IVoiceChatButton muteParticipantsButton)
        {
            _muteParticipantsButton = muteParticipantsButton;
        }

        protected override void Subscribe(IParticipantsVoiceController observable)
        {
            _observable = observable;

            observable.MuteParticipantsChanged += OnMuteParticipantsChanged;
            observable.MuteParticipantChanged += OnMuteParticipantChanged;
        }

        protected override void Unsubscribe(IParticipantsVoiceController observable)
        {
            _observable = null;

            observable.MuteParticipantsChanged -= OnMuteParticipantsChanged;
            observable.MuteParticipantChanged -= OnMuteParticipantChanged;
        }

        private void OnMuteParticipantChanged(string name, bool mute) =>
            OnMuteParticipantsChanged(_observable.IsOtherParticipantsMuted());

        private void OnMuteParticipantsChanged(bool mute) =>
            _muteParticipantsButton.SetActive(!mute);
    }
}