using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI.Hud;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class UnitObserverFactory
    {
        private readonly ITeamPanel _teamPanel;
        private readonly IAllyPanel _allyPanel;

        public UnitObserverFactory([NotNull] ITeamPanel _teamPanel, [NotNull] IAllyPanel allyPanel)
        {
            this._teamPanel = _teamPanel;
            _allyPanel = allyPanel;
        }

        public UnitObserver Create() =>
            new UnitObserver(_teamPanel, _allyPanel);
    }
}