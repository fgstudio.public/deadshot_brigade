using Frostgate.RiftHunters.Core.Battle.Client.Vibration;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Observing;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class LocalPlayerInputButtonObserver :  ObjectObserver<UnitNetwork>
    {
        [NotNull] private readonly BattleVibrationMediator _vibrationMediator;

        public LocalPlayerInputButtonObserver([NotNull] BattleVibrationMediator vibrationMediator)
        {
            _vibrationMediator = vibrationMediator;
        }

        protected override void Subscribe(UnitNetwork observable)
        {
            observable.UnitNetworkInput.DodgeExecuting += OnInputButtonClick;
            observable.UnitNetworkInput.ReloadExecuting += OnInputButtonClick;
            observable.UnitNetworkInput.AbilityExecuting += OnInputButtonClick;
            observable.UnitNetworkInput.ExtraAbilityExecuting += OnInputButtonClick;
            observable.UnitNetworkInput.UltimateExecuting += OnInputButtonClick;
        }

        protected override void Unsubscribe(UnitNetwork observable)
        {
            observable.UnitNetworkInput.DodgeExecuting -= OnInputButtonClick;
            observable.UnitNetworkInput.ReloadExecuting -= OnInputButtonClick;
            observable.UnitNetworkInput.AbilityExecuting -= OnInputButtonClick;
            observable.UnitNetworkInput.ExtraAbilityExecuting -= OnInputButtonClick;
            observable.UnitNetworkInput.UltimateExecuting -= OnInputButtonClick;
        }

        private void OnInputButtonClick() => _vibrationMediator.HandleEvent(new InputButtonClickedEvent());
    }
}
