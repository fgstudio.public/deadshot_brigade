using Mirror;
using System.Linq;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Reanimation;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class LocalPlayerStateObserver : ObjectObserver<UnitNetwork>
    {
        private readonly IUIBattleMediator _uiBattleMediator;
        private readonly ObjectCollectionRepository<UnitNetwork> _unitCollection;

        private BehaviourState _stateCache;

        public LocalPlayerStateObserver([NotNull] IUIBattleMediator uiBattleMediator,
            [NotNull] ObjectCollectionRepository<UnitNetwork> unitCollection)
        {
            _unitCollection = unitCollection;
            _uiBattleMediator = uiBattleMediator;
        }

        protected override void OnSet(UnitNetwork observable) { }

        protected override void Subscribe(UnitNetwork unit) =>
            unit.UnitNetworkState.BehaviourStateChanged += OnStateChanged;

        protected override void Unsubscribe(UnitNetwork unit) =>
            unit.UnitNetworkState.BehaviourStateChanged -= OnStateChanged;

        private void OnStateChanged()
        {
            BehaviourState state = Observable.UnitNetworkState.BehaviourState;
            if (state == _stateCache) return;

            if (state == BehaviourState.Dead)
            {
                ReanimationMechanicComponent reanimationMechanic = Observable.ReanimationMechanic;

                ReanimationModeDTO dto = CreateReanimationModeDTO(reanimationMechanic);
                _uiBattleMediator.EnableReanimationMode(dto);

                Subscribe(reanimationMechanic);
            }
            else if (_stateCache == BehaviourState.Dead)
            {
                _uiBattleMediator.DisableReanimationMode();
                Unsubscribe(Observable.ReanimationMechanic);
            }

            _stateCache = state;
        }

        private ReanimationModeDTO CreateReanimationModeDTO(
            [NotNull] ReanimationMechanicComponent reanimationMechanic)
        {
            // TODO: должна быть коллекция чисто подконтрольных юнитов. Боты должны храниться отдельно
            UnitNetwork playerUnit = Observable;
            UnitNetwork[] nonPlayerUnits = _unitCollection.Get<uint>()
                .Where(u => u.UnitType == BattleUnitType.Player)
                .Where(u => u != playerUnit)
                .ToArray();

            return new ReanimationModeDTO(playerUnit, nonPlayerUnits, reanimationMechanic.Progress);
        }

        private void Subscribe([NotNull] ReanimationMechanicComponent reanimationMechanic)
        {
            reanimationMechanic.Activated.AddListener(_uiBattleMediator.ActivateReanimationProcess);
            reanimationMechanic.Deactivated.AddListener(_uiBattleMediator.StopReanimationProcess);
            reanimationMechanic.Restored.AddListener(_uiBattleMediator.ReanimationMediator.EnableWaitingMode);
        }

        private void Unsubscribe([NotNull] ReanimationMechanicComponent reanimationMechanic)
        {
            reanimationMechanic.Activated.RemoveListener(_uiBattleMediator.ActivateReanimationProcess);
            reanimationMechanic.Deactivated.RemoveListener(_uiBattleMediator.StopReanimationProcess);
            reanimationMechanic.Restored.RemoveListener(_uiBattleMediator.ReanimationMediator.EnableWaitingMode);
        }
    }
}