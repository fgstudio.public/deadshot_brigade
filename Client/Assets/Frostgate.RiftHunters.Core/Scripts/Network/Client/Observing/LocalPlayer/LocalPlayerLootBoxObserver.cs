using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI.Hud;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Looting;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class LocalPlayerLootBoxObserver : ObjectObserver<UnitNetwork>
    {
        private readonly ILootBoxesPanel _lootBoxesPanel;

        public LocalPlayerLootBoxObserver([NotNull] ILootBoxesPanel lootBoxesPanel) =>
            _lootBoxesPanel = lootBoxesPanel;

        protected override void OnSet(UnitNetwork unit)
        {
            if (unit.UnitNetworkState.IsInitialized)
                OnInitialized(unit.UnitNetworkState);
        }

        protected override void Subscribe(UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized += OnInitialized;
            unit.UnitNetworkState.LootBoxCounter.Changed += OnLootBoxChanged;
        }

        protected override void Unsubscribe(UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized -= OnInitialized;
            unit.UnitNetworkState.LootBoxCounter.Changed -= OnLootBoxChanged;
        }

        private void OnInitialized(UnitNetworkState state) =>
            _lootBoxesPanel.UpdateCounts(state.LootBoxCounter);

        private void OnLootBoxChanged(LootBoxType lootBoxType)
        {
            ILootBoxStateCounter lootBoxCounter = Observable.UnitNetworkState.LootBoxCounter;
            _lootBoxesPanel.SetCount(lootBoxType, lootBoxCounter.GetCount(lootBoxType));
        }

    }
}
