using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class LocalPlayerDodgingObserver : ObjectObserver<UnitNetwork>
    {
        private readonly IUIPlayerInputMediator _inputMediator;

        public LocalPlayerDodgingObserver([NotNull] IUIPlayerInputMediator inputMediator) =>
            _inputMediator = inputMediator;

        protected override void OnSet(UnitNetwork unit)
        {
            if (unit.UnitNetworkState.IsInitialized)
                OnInitialized(unit.UnitNetworkState);
        }

        protected override void Subscribe(UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized += OnInitialized;
            unit.UnitNetworkState.StaminaChanged += OnStaminaChanged;
            unit.UnitNetworkState.BehaviourStateChanged += OnBehaviourStateChanged;
        }

        protected override void Unsubscribe(UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized -= OnInitialized;
            unit.UnitNetworkState.StaminaChanged -= OnStaminaChanged;
            unit.UnitNetworkState.BehaviourStateChanged -= OnBehaviourStateChanged;
        }

        private void OnInitialized(UnitNetworkState unitState)
        {
            bool canDodge = unitState.UnitConfig.UnitActionRoster
                .TryGetAction(UnitActionTypes.Dodge, out UnitActionConfig dodgeAction);

            if (canDodge)
            {
                int segmentCount = (int)(unitState.MaxStamina / dodgeAction.StaminaCost);
                _inputMediator.DodgeButton.SetSegments(segmentCount);
                UpdateButtonState();
            }
            else
            {
                _inputMediator.DodgeButton.SetInteractable(false);
                Unsubscribe(Observable);
            }
        }

        private void OnBehaviourStateChanged() => UpdateButtonState();
        private void OnStaminaChanged() => UpdateButtonState();

        private void UpdateButtonState()
        {
            UnitNetworkState unitState = Observable.UnitNetworkState;
            UnitActionState actionState = Observable.UnitActionState;

            bool canDodge = unitState.UnitConfig.UnitActionRoster
                .TryGetAction(UnitActionTypes.Dodge, out UnitActionConfig dodgeAction);

            if (canDodge)
            {
                float dodgeCost = dodgeAction.StaminaCost;
                float maxStamina = unitState.MaxStamina;
                float stamina = unitState.Stamina;

                bool hasStaminaToDodge = stamina >= dodgeCost;
                bool isDodging = actionState.CurrentActionType == UnitActionTypes.Dodge;

                _inputMediator.DodgeButton.SetCurrentStamina(stamina, maxStamina);
                _inputMediator.DodgeButton.SetInteractable(hasStaminaToDodge && !isDodging);
            }
        }
    }
}
