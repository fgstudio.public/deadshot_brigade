using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class LocalPlayerReloadingObserver : ObjectObserver<UnitNetwork>
    {
        private readonly IUIPlayerInputMediator _inputMediator;

        private UnitNetworkState _unitState;

        public LocalPlayerReloadingObserver([NotNull] IUIPlayerInputMediator inputMediator) =>
            _inputMediator = inputMediator;

        protected override void OnSet(UnitNetwork unit)
        {
            if (unit.UnitNetworkState.IsInitialized)
                OnInitialized(unit.UnitNetworkState);
        }

        protected override void Subscribe(UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized += OnInitialized;
            unit.UnitNetworkState.PatronsCountChanged += OnPatronsCountChanged;
            unit.UnitNetworkState.BehaviourStateChanged += OnBehaviourStateChanged;
        }

        protected override void Unsubscribe(UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized -= OnInitialized;
            unit.UnitNetworkState.PatronsCountChanged -= OnPatronsCountChanged;
            unit.UnitNetworkState.BehaviourStateChanged -= OnBehaviourStateChanged;
        }

        private void OnInitialized(UnitNetworkState unitState)
        {
            _unitState = unitState;
            OnPatronsCountChanged();
        }

        private void OnBehaviourStateChanged()
        {
            if (_unitState.BehaviourState == BehaviourState.Reload)
            {
                float reloadSecs = Observable.RangeWeapon?.ReloadSeconds ?? 0;
                TimeSpan reloadTime = TimeSpan.FromSeconds(reloadSecs);
                _inputMediator.ReloadButton.StartReload(reloadTime);
            }
            else
            {
                _inputMediator.ReloadButton.StopReload();
            }
        }

        private void OnPatronsCountChanged()
        {
            UnitNetworkState state = Observable.UnitNetworkState;
            bool isPatronsFull = state.PatronsCount == state.MaxPatrons;
            _inputMediator.ReloadButton.SetInteractable(!isPatronsFull);
        }
    }
}