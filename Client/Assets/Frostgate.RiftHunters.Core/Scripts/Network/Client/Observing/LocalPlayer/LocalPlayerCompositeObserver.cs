using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    /// <summary>
    /// Наблюдатель и обработчик изменения данных в UnitNetwork, которые являются LocalPlayer.
    /// </summary>
    public sealed class LocalPlayerCompositeObserver : ObjectObserver<UnitNetwork>
    {
        private readonly ObjectObserver<UnitNetwork>[] _subObservers;

        public LocalPlayerCompositeObserver([NotNull] params ObjectObserver<UnitNetwork>[] subObservers) =>
            _subObservers = subObservers;

        protected override void OnDisposed()
        {
            foreach (ObjectObserver<UnitNetwork> observer in _subObservers)
                observer.Dispose();
        }

        protected override void OnSet(UnitNetwork unit)
        {
            foreach (ObjectObserver<UnitNetwork> observer in _subObservers)
                observer.SetObservable(unit);
        }

        protected override void OnRemove(UnitNetwork unit)
        {
            foreach (ObjectObserver<UnitNetwork> observer in _subObservers)
                observer.RemoveObservable();
        }

        protected override void Subscribe(UnitNetwork observable) { }
        protected override void Unsubscribe(UnitNetwork observable) { }
    }
}