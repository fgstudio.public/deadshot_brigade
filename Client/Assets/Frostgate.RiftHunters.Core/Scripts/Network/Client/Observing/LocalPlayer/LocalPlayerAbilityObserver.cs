using System;
using System.Threading;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.UI.Input;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Impact.Client;
using Frostgate.RiftHunters.Core.Battle.Client.Vibration;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class LocalPlayerAbilityObserver : ObjectObserver<UnitNetwork>
    {
        [NotNull] private readonly IUIPlayerInputMediator _inputMediator;
        [NotNull] private readonly BattleVibrationMediator _vibrationMediator;
        [NotNull] private readonly IConfigCollection<ImpactUIConfig> _uiConfigs;

        [NotNull] private readonly CancellationTokenSource _cts = new();

        public LocalPlayerAbilityObserver(
            [NotNull] IUIPlayerInputMediator inputMediator,
            [NotNull] BattleVibrationMediator vibrationMediator,
            [NotNull] IConfigCollection<ImpactUIConfig> uiConfigs)
        {
            _uiConfigs = uiConfigs;
            _inputMediator = inputMediator;
            _vibrationMediator = vibrationMediator;
        }

        protected override void OnDisposed()
        {
            _cts.Cancel();
            _cts.Dispose();
        }

        protected override void OnSet(UnitNetwork unit)
        {
            if (unit.UnitNetworkState.IsInitialized)
                OnInitialized(unit.UnitNetworkState);
        }

        protected override void Subscribe(UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized += OnInitialized;
            unit.UnitNetworkInput.AbilityExecuting += OnAbilityExecuting;
            unit.UnitNetworkInput.ExtraAbilityExecuting += OnExtraAbilityExecuting;

            unit.ImpactState.CastStarted.AddListener(OnCastStarted);
            unit.ImpactRoster.Ability.CooldownUpdated += UpdateAbilityButtonState;
            unit.ImpactRoster.ExtraAbility.CooldownUpdated += UpdateExtraAbilityButtonState;
        }

        protected override void Unsubscribe(UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized -= OnInitialized;
            unit.UnitNetworkInput.AbilityExecuting -= OnAbilityExecuting;
            unit.UnitNetworkInput.ExtraAbilityExecuting -= OnExtraAbilityExecuting;

            unit.ImpactState.CastStarted.RemoveListener(OnCastStarted);
            unit.ImpactRoster.Ability.CooldownUpdated -= UpdateAbilityButtonState;
            unit.ImpactRoster.ExtraAbility.CooldownUpdated -= UpdateExtraAbilityButtonState;
        }

        private void OnInitialized(UnitNetworkState state)
        {
            ImpactConfig abilityConfig = state.PropertiesProvider?.Impacts.Ability.Config;
            IAbilityButton abilityButton = _inputMediator.AbilityButton;
            InitButton(abilityButton, abilityConfig, Observable.ImpactRoster.Ability);

            ImpactConfig extraAbilityConfig = state.PropertiesProvider?.Impacts.ExtraAbility.Config;
            IAbilityButton extraAbilityButton = _inputMediator.ExtraAbilityButton;
            InitButton(extraAbilityButton, extraAbilityConfig, Observable.ImpactRoster.ExtraAbility);
        }

        private void OnCastStarted(string impactId) =>
            _vibrationMediator.HandleEvent(new ImpactCastStartedEvent(impactId));

        private void OnAbilityExecuting() => BlockButtonForSomeTime(
                _inputMediator.AbilityButton, Observable.ImpactRoster.Ability);

        private void OnExtraAbilityExecuting() => BlockButtonForSomeTime(
            _inputMediator.ExtraAbilityButton, Observable.ImpactRoster.ExtraAbility);

        private void BlockButtonForSomeTime(
            [NotNull] IAbilityButton button, [NotNull] ImpactModel impactModel)
        {
            button.BlockButton();

            TimeSpan serverResponseComp = TimeSpan.FromSeconds(3);
            UniTask.Delay(serverResponseComp, cancellationToken: _cts.Token)
                .ContinueWith(() =>
                {
                    bool noResponse = button.IsBlocked;
                    if (noResponse) UpdateButtonState(button, impactModel);
                })
                .SuppressCancellationThrow();
        }

        private void InitButton([NotNull] IAbilityButton button, [CanBeNull] ImpactConfig impactConfig, ImpactModel abilityImpactModel)
        {
            if (impactConfig)
            {
                button.Enable();
                UpdateButtonIcon(button, impactConfig);
                UpdateButtonState(button, abilityImpactModel);

                // В абилке джека хотят особое поведение инпута. Возможно в будущем это на все абилки распрстранится или просто отменется.
                // Пока просто захардкодил айдишик конкретного конфига.
                // В этом режиме инпута доступно вращение камерой с зажатой кнопкой, и активация абилки после долгого зажатия и возюканья.
                button.JackSpecial?.EnableDrag(impactConfig.Id == "hero_jack_ability");
            }
            else
            {
                button.Disable();
            }
        }

        private void UpdateAbilityButtonState() =>
            UpdateButtonState(_inputMediator.AbilityButton, Observable.ImpactRoster.Ability);

        private void UpdateExtraAbilityButtonState() =>
            UpdateButtonState(_inputMediator.ExtraAbilityButton, Observable.ImpactRoster.ExtraAbility);

        private void UpdateButtonIcon([NotNull] IAbilityButton button, [NotNull] ImpactConfig impactConfig)
        {
            if (_uiConfigs.TryGet(impactConfig.Id, out ImpactUIConfig uiConfig))
                button.SetIcon(uiConfig.Icon);
        }

        private void UpdateButtonState([NotNull] IAbilityButton button, [NotNull] ImpactModel impactModel)
        {
            TimeSpan cooldown = impactModel.RemainingCooldown;

            if (cooldown.Ticks == 0) button.StopCooldown();
            else button.StartCooldown(cooldown);
        }
    }
}