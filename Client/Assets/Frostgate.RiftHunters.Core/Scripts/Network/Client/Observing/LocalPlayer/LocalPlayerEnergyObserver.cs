using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.UI.Input;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Impact.Client;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class LocalPlayerEnergyObserver : ObjectObserver<UnitNetwork>
    {
        [NotNull] private readonly IUIBattleMediator _battleMediator;
        [NotNull] private readonly IConfigCollection<ImpactUIConfig> _uiConfigs;

        public LocalPlayerEnergyObserver(
            [NotNull] IUIBattleMediator battleMediator,
            [NotNull] IConfigCollection<ImpactUIConfig> uiConfigs)
        {
            _battleMediator = battleMediator;
            _uiConfigs = uiConfigs;
        }

        protected override void OnSet(UnitNetwork unit)
        {
            if (unit.UnitNetworkState.IsInitialized)
                OnInitialized(unit.UnitNetworkState);
        }

        protected override void Subscribe(UnitNetwork unit)
        {
            unit.UnitNetworkInput.UltimateExecuting += OnUltimateExecuting;
            unit.UnitNetworkState.EnergyChanged += OnEnergyChanged;
            unit.UnitNetworkState.Initialized += OnInitialized;
            unit.UnitNetworkState.Raised += OnRaised;
        }

        protected override void Unsubscribe(UnitNetwork unit)
        {
            unit.UnitNetworkInput.UltimateExecuting -= OnUltimateExecuting;
            unit.UnitNetworkState.EnergyChanged -= OnEnergyChanged;
            unit.UnitNetworkState.Initialized -= OnInitialized;
            unit.UnitNetworkState.Raised -= OnRaised;
        }

        private void OnUltimateExecuting() => BlockButton();

        private void OnRaised() => UpdateButtonState(Observable.UnitNetworkState);

        private void OnEnergyChanged(float diff)
        {
            UnitNetworkState state = Observable.UnitNetworkState;
            UpdateEnergyProgress(state);
            UpdateButtonState(state);
        }

        private void OnInitialized(UnitNetworkState state)
        {
            InitButton(state);
            InitEnergyProgress(state);
        }

        private void InitButton([NotNull] UnitNetworkState state)
        {
            ImpactConfig ultimateConfig = state.PropertiesProvider?.Impacts.Ultimate.Config;
            IUltimateButton ultimateButton = _battleMediator.InputMediator.UltimateButton;

            if (ultimateConfig != null)
            {
                ultimateButton.Enable();
                UpdateButtonIcon(ultimateConfig);
                UpdateButtonState(state);
            }
            else
            {
                ultimateButton.Disable();
            }
        }

        private void UpdateButtonIcon([NotNull] ImpactConfig impactConfig)
        {
            if (_uiConfigs.TryGet(impactConfig.Id, out ImpactUIConfig uiConfig))
                _battleMediator.InputMediator.UltimateButton.SetIcon(uiConfig.Icon);
        }

        private void UpdateButtonState([NotNull] UnitNetworkState state) =>
            _battleMediator.InputMediator.UltimateButton.SetActive(state.IsEnergyFull);

        private void BlockButton() =>
            _battleMediator.InputMediator.UltimateButton.SetActive(false);

        private void InitEnergyProgress([NotNull] UnitNetworkState state)
        {
            float ratio = CalcEnergyRatio(state);
            _battleMediator.InputMediator.UltimateButton.EnergyProgress.InitProgress(ratio);
        }

        private void UpdateEnergyProgress([NotNull] UnitNetworkState state)
        {
            float ratio = CalcEnergyRatio(state);
            _battleMediator.InputMediator.UltimateButton.EnergyProgress.SetProgress(ratio);
        }

        private static float CalcEnergyRatio([NotNull] UnitNetworkState state) =>
            state.MaxEnergy > 0 ? state.Energy / state.MaxEnergy : default;
    }
}