using Mirror;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Client.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class UnitCollectionObserver : ObjectObserver<IReadOnlyObjectCollection<uint, UnitNetwork>>
    {
        [NotNull] private readonly IUIBattleMediator _battleMediator;
        [NotNull] private readonly PlayerInputAttacher _inputAttacher;
        [NotNull] private readonly UnitObserverFactory _unitObserverFactory;
        [NotNull] private readonly UnitStateMachineFactory _stateMachineFactory;
        [NotNull] private readonly LocalPlayerCompositeObserver _localPlayerObserver;

        private readonly Dictionary<UnitNetwork, UnitObserver> _unitObservers;

        public UnitCollectionObserver(
            [NotNull] IUIBattleMediator battleMediator,
            [NotNull] PlayerInputAttacher inputAttacher,
            [NotNull] UnitObserverFactory unitObserverFactory,
            [NotNull] UnitStateMachineFactory stateMachineFactory,
            [NotNull] LocalPlayerCompositeObserver localPlayerObserver)
        {
            _inputAttacher = inputAttacher;
            _battleMediator = battleMediator;
            _unitObserverFactory = unitObserverFactory;
            _stateMachineFactory = stateMachineFactory;
            _localPlayerObserver = localPlayerObserver;

            _unitObservers = new Dictionary<UnitNetwork, UnitObserver>();
        }

        protected override void OnDisposed()
        {
            foreach (UnitObserver observer in _unitObservers.Values)
                observer.Dispose();

            _unitObservers.Clear();
        }

        protected override void Subscribe(IReadOnlyObjectCollection<uint, UnitNetwork> collection)
        {
            collection.Added += OnAdded;
            collection.Removed += OnRemoved;
            collection.Values.ForEach(OnAdded);
        }

        protected override void Unsubscribe(IReadOnlyObjectCollection<uint, UnitNetwork> collection)
        {
            collection.Added -= OnAdded;
            collection.Removed -= OnRemoved;
        }

        private void OnAdded(UnitNetwork unit)
        {
            if (unit.UnitNetworkState.IsInitialized)
                OnInitialized(unit.UnitNetworkState);
            else
                unit.UnitNetworkState.Initialized += OnInitialized;
        }

        private void OnInitialized(UnitNetworkState state)
        {
            state.Initialized -= OnInitialized;

            UnitNetwork unit = state.UnitNetwork;
            NetworkIdentity identity = unit.Identity;

            if (identity.isClientOnly)
            {
                BehaviourStateMachineHandler stateMachineHandler = _stateMachineFactory.Create(unit);
                unit.SetStateMachineHandler(stateMachineHandler);
            }

            if (identity.isLocalPlayer)
            {
                _inputAttacher.AttachToUnit(unit);
                _localPlayerObserver.SetObservable(unit);
            }

            if (IsBoss(state))
                _battleMediator.BossPhasesMediator.Init(unit.UnitNetworkState);

            unit.InitClient();
            unit.ViewSpawner.Init();

            UnitObserver observer = _unitObserverFactory.Create();
            observer.SetObservable(unit);

            _unitObservers[unit] = observer;
        }

        private void OnRemoved([NotNull] UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized -= OnInitialized;

            if (unit.Identity.isClientOnly)
                unit.SetStateMachineHandler(null);

            if (unit.Identity.isLocalPlayer)
            {
                _inputAttacher.DeattachUnit(unit);
                _localPlayerObserver.RemoveObservable();
            }

            if (IsBoss(unit.UnitNetworkState))
                _battleMediator.BossPhasesMediator.Deinit();

            unit.DeinitClient();
            unit.ViewSpawner.Deinit();

            if (_unitObservers.TryGetValue(unit, out UnitObserver observer))
            {
                observer.Dispose();
                _unitObservers.Remove(unit);
            }
        }

        private bool IsBoss([NotNull] UnitNetworkState state) =>
            state.UnitConfig.UnitPhaseConfig != null;
    }
}