﻿using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.UI.Input;
using Frostgate.RiftHunters.Core.Voice;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class SelfVoiceObserver : ObjectObserver<ISelfVoiceController>
    {
        [NotNull] private readonly IVoiceChatButton _muteSelfButton;

        public SelfVoiceObserver([NotNull] IVoiceChatButton muteSelfButton)
        {
            _muteSelfButton = muteSelfButton;
        }

        protected override void Subscribe(ISelfVoiceController observable)
        {
            observable.MuteSelfChanged += OnMuteSelfChanged;
        }

        protected override void Unsubscribe(ISelfVoiceController observable)
        {
            observable.MuteSelfChanged -= OnMuteSelfChanged;
        }

        private void OnMuteSelfChanged(bool mute)
        {
            _muteSelfButton.SetActive(!mute);
        }
    }
}