using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI.Hud;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class ArenaProgressObserver : ObjectObserver<IReadOnlyBotArenaState>
    {
        private readonly UIMissionTimer _missionTimer;
        private readonly SimulationTime _simulationTime;
        private readonly string _startArenaId;

        private readonly ILogger _logger;

        private BotArenaData _startArenaData;

        public ArenaProgressObserver([NotNull] SimulationTime simulationTime, [NotNull] UIMissionTimer missionTimer,
            [NotNull] string startArenaId)
        {
            _missionTimer = missionTimer;
            _simulationTime = simulationTime;
            _startArenaId = startArenaId;

            _logger = LoggerFactory.CreateLogger<ArenaProgressObserver>();
        }

        protected override void OnDisposed() =>
            _simulationTime.OnUpdate -= OnTimeUpdated;

        protected override void OnSet(IReadOnlyBotArenaState observable)
        {
            if (observable.IsInitialized) OnArenaStateInitialized();
            else observable.Initialized.AddListener(OnArenaStateInitialized);
        }

        protected override void Subscribe(IReadOnlyBotArenaState observable)
        {
            observable.DataUpdated.AddListener(OnDataUpdated);
            observable.AreAllArenasCompletedChanged.AddListener(OnAllArenasCompletedChanged);
        }

        protected override void Unsubscribe(IReadOnlyBotArenaState observable)
        {
            observable.Initialized.RemoveListener(OnArenaStateInitialized);

            observable.DataUpdated.RemoveListener(OnDataUpdated);
            observable.AreAllArenasCompletedChanged.RemoveListener(OnAllArenasCompletedChanged);
        }

        private void OnArenaStateInitialized()
        {
            _logger.Log("Initialized");

            if (!Observable.AreAllArenasCompleted)
                OnDataUpdated(_startArenaId);
        }

        private void OnDataUpdated(string arenaId)
        {
            if (arenaId != _startArenaId) return;
            if (!Observable.Data.TryGetValue(arenaId, out BotArenaData arenaData)) return;
            if (arenaData.Status == BotArenaStatus.Deactivated) return;

            _startArenaData = arenaData;
            _logger.Log("Start " + nameof(BotArenaData) + " Cached");

            ActivateTimer();
        }

        private void OnAllArenasCompletedChanged(bool isCompleted)
        {
            if (isCompleted)
                DeactivateTimer();
        }

        private void ActivateTimer()
        {
            if (!_missionTimer.IsActive)
            {
                _missionTimer.SetActiveState();

                _simulationTime.OnUpdate += OnTimeUpdated;
                UpdateTimer(_simulationTime.TimeSpan, true);

                _logger.Log("Timer Activated");
            }
        }

        private void DeactivateTimer()
        {
            if (_missionTimer.IsActive)
            {
                _missionTimer.StartBumpAnimation();
                _simulationTime.OnUpdate -= OnTimeUpdated;
                UpdateTimer(_simulationTime.TimeSpan, true);

                _logger.Log("Timer Deactivated");
            }
        }

        private void OnTimeUpdated(float _) =>
            UpdateTimer(_simulationTime.TimeSpan, false);

        private void UpdateTimer(TimeSpan currentTime, bool forced)
        {
            TimeSpan startTime = TimeSpan.FromSeconds(_startArenaData.StartTime);
            TimeSpan interval = currentTime - startTime;

            _missionTimer.UpdateTimer(interval, forced);
        }
    }
}