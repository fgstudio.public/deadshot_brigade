using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Client.FX;
using Frostgate.RiftHunters.Core.Battle.Client.Vibration;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Exp;
using Frostgate.RiftHunters.Core.Battle.Shared.Healing;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    /// <summary>
    /// Наблюдатель и обработчик изменения данных в serviceRoster-объектах.
    /// </summary>
    public sealed class ServiceRosterObserver : ObjectObserver<IServiceRoster>
    {
        private readonly IFxRoster _fxRoster;
        private readonly BattleVibrationMediator _vibration;
        private readonly IUIBattleMediator _uiBattleMediator;
        private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _units;
        private readonly IReadOnlyObjectCollection<uint, IAimTarget> _aimTargets;

        public ServiceRosterObserver([NotNull] IFxRoster fxRoster,
            [NotNull] BattleVibrationMediator vibration, [NotNull] IUIBattleMediator uiBattleMediator,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> units,
            [NotNull] IReadOnlyObjectCollection<uint, IAimTarget> aimTargets)
        {
            _units = units;
            _fxRoster = fxRoster;
            _vibration = vibration;
            _aimTargets = aimTargets;
            _uiBattleMediator = uiBattleMediator;
        }

        protected override void OnSet(IServiceRoster serviceRoster)
        { }

        protected override void Subscribe(IServiceRoster serviceRoster)
        {
            serviceRoster.HealingService.Healed.AddListener(OnHealed);
            serviceRoster.DamageService.Damaged.AddListener(OnDamaged);
            serviceRoster.DamageService.Blocked.AddListener(OnDamageBlocked);
            serviceRoster.RespawnUnitService.Respawned.AddListener(OnUnitRespawned);
            serviceRoster.ReceiveExpService.Received.AddListener(OnExpReceived);
        }

        protected override void Unsubscribe(IServiceRoster serviceRoster)
        {
            serviceRoster.HealingService.Healed.RemoveListener(OnHealed);
            serviceRoster.DamageService.Damaged.RemoveListener(OnDamaged);
            serviceRoster.DamageService.Blocked.RemoveListener(OnDamageBlocked);
            serviceRoster.RespawnUnitService.Respawned.RemoveListener(OnUnitRespawned);
            serviceRoster.ReceiveExpService.Received.RemoveListener(OnExpReceived);
        }

        private void OnHealed(HealEventData data)
        {
            if (data.Heal > 0 && data.Type != HealingType.AutoRecovery &&
                _units.TryGet(data.ReceiverId, out UnitNetwork receiver))
            {
                if (receiver?.View?.FxContainer != null)
                    _fxRoster.UiVfx.Heal.Play(data.Heal, receiver.View.FxContainer);

                if (receiver?.View?.WorldFxContainer != null)
                    _fxRoster.WorldFx.Heal.Play(receiver.View.WorldFxContainer);
            }
        }

        private void OnDamaged(DamageEventData data)
        {
            float damage = data.Damage;

            bool hasCaster = _units.TryGet(data.CasterNetId, out UnitNetwork casterUnit);
            bool hasReceiver = _aimTargets.TryGet(data.ReceiverNetId, out IAimTarget receiverTarget) &&
                               receiverTarget.GameObject != null && receiverTarget.View?.FxContainer != null;

            if (hasReceiver)
            {
                _fxRoster.WorldFx.Damage.Play(
                    receiverTarget.View.DamageFxTarget, damage, receiverTarget.State.BodyRadius);

                bool isReceiverBot = !receiverTarget.Identity.isLocalPlayer &&
                                     receiverTarget.UnitType == BattleUnitType.Bot;

                if (damage > 0 && isReceiverBot)
                {
                    DamageZone damageZone = receiverTarget.GetDamageZone(data.ZoneMultiplier);

                    Vector3 position = _uiBattleMediator.FloatingTextsMediator
                        .GetScreenPositionWithRandomOffset(receiverTarget.View.FxContainer.position);

                    Transform container = _uiBattleMediator.FloatingTextsMediator.Container;

                    if(damageZone.IsBack)
                        _fxRoster.UiVfx.Indicator.PlayInTheBackEffect(position, container);
                }
            }

            if (data.Type != DamageType.Ability &&
                hasCaster && casterUnit.isLocalPlayer && casterUnit.CurrentWeapon != null)
            {
                // Костыль всех костылей. Придумать как передавать оружие через DamageEventData
                _vibration.HandleEvent(new OutcomingDamageEvent(casterUnit.CurrentWeapon));
            }

            if (hasReceiver && hasCaster && receiverTarget.Identity.isLocalPlayer)
            {
                // Не очень удобное и красивое решение.
                // Посмотреть в сторону IObserver<T> и IObservable<T>
                _vibration.HandleEvent(new IncomingDamageEvent(data.Type, damage));

                _uiBattleMediator.Hud.DamageDirectionIndicator.Indicate(
                    casterUnit.transform, receiverTarget.GameObject.transform);
            }
        }

        private void OnDamageBlocked(BlockEventData data)
        {
            if (!_aimTargets.TryGet(data.ReceiverId, out IAimTarget receiver)) return;
            if (receiver.View == null) return;

            _fxRoster.WorldFx.ShieldImpact.Play(receiver.View.DamageReflectFxTarget);

            if (!receiver.Identity.isLocalPlayer && receiver.UnitType == BattleUnitType.Bot)
            {
                _fxRoster.UiVfx.Invul.Handle(
                    new DamageEventData(data.CasterId, data.ReceiverId, DamageType.Melee, 0, false, data.ZoneMultiplier, 1f),
                    _uiBattleMediator.FloatingTextsMediator.GetScreenPosition(receiver.View.FxContainer.position),
                    receiver.GetDamageZone(data.ZoneMultiplier),
                    _uiBattleMediator.FloatingTextsMediator.Container);
            }
        }

        private void OnUnitRespawned(uint respawnableId)
        {
            if (NetworkClient.spawned.TryGetValue(respawnableId, out NetworkIdentity respawnableIdentity))
                _fxRoster.WorldFx.Respawn.Play(respawnableIdentity.transform);
        }

        private void OnExpReceived(ExpEventData data)
        {
            if (data.Exp <= 0) return;
            if (!_units.TryGet(data.ReceiverId, out UnitNetwork receiver)) return;
            if (receiver.UnitType != BattleUnitType.Player) return;

            bool isLocalPlayer = receiver.Identity.isLocalPlayer;
            UnitNetworkState state = receiver.UnitNetworkState;

            if (isLocalPlayer) _fxRoster.UiVfx.Exp.Play(data.Exp, receiver.View?.FxContainer);
            _uiBattleMediator.Hud.TeamPanel.UpdateTeammatePanelAnimated(data.ReceiverId, state.PlayerName, state.Exp, isLocalPlayer);
        }
    }
}