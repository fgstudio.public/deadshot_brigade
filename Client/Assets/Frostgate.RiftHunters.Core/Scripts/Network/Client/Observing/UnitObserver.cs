using System.Collections;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.UI.Hud;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Utils;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Network.Client.Observing
{
    public sealed class UnitObserver : ObjectObserver<UnitNetwork>
    {
        private readonly ITeamPanel _teamPanel;
        private readonly IAllyPanel _allyPanel;
        private Coroutine _coroutine;

        public UnitObserver([NotNull] ITeamPanel teamPanel, [NotNull] IAllyPanel allyPanel)
        {
            _teamPanel = teamPanel;
            _allyPanel = allyPanel;
        }

        protected override void OnSet(UnitNetwork unit)
        {
            if (unit.UnitNetworkState.IsInitialized)
                OnInitialized(unit.UnitNetworkState);
        }

        protected override void OnRemove(UnitNetwork unit)
        {
            RemoveScoreForPlayer(unit);
        }

        protected override void Subscribe(UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized += OnInitialized;
            unit.UnitNetworkState.ExpChanged += OnExpChanged;
            unit.ReanimationMechanic.Disabled.AddListener(OnUnitReanimationDisabled);
        }

        protected override void Unsubscribe(UnitNetwork unit)
        {
            unit.UnitNetworkState.Initialized -= OnInitialized;
            unit.UnitNetworkState.ExpChanged -= OnExpChanged;
            unit.ReanimationMechanic.Disabled.RemoveListener(OnUnitReanimationDisabled);
        }

        private void OnInitialized(UnitNetworkState state)
        {
            UpdateScoreForPlayer(state.UnitNetwork);
            UpdateAllyPointer(state.UnitNetwork);
        }

        private void OnExpChanged(int diff) =>
            UpdateScoreForPlayer(Observable);

        private void OnUnitReanimationDisabled()
        {
            UpdateAllyPointer(Observable);
        }

        private void UpdateScoreForPlayer(UnitNetwork unit)
        {
            if (HasPlayerScore(unit))
                _teamPanel.UpdateTeammatePanelDiscrete(unit);
        }

        private void RemoveScoreForPlayer(UnitNetwork unit)
        {
            if (HasPlayerScore(unit))
                _teamPanel.RemoveTeammatePanel(unit.Identity);
        }

        private bool HasPlayerScore(UnitNetwork unit) =>
            unit.UnitNetworkState.UnitType == BattleUnitType.Player;

        private void UpdateAllyPointer(UnitNetwork unit)
        {
            //todo: для поинтеров возможно следует создать отдельный обсервер (сейчас поинтеры для разных
            //систем вызываются в разных местах
            if (IsAllyUnit(unit))
                _allyPanel.SetTarget(unit.transform, unit.UnitNetworkState.UnitConfig.RoleIcon);
        }

        private bool IsAllyUnit(UnitNetwork unit) =>
            !unit.Identity.isLocalPlayer &&
            unit.UnitNetworkState.UnitType == BattleUnitType.Player;

    }
}