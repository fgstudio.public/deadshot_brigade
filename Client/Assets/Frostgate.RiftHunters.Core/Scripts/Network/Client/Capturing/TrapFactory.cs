using System;
using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Network.Shared.Capturing;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// Фабрика ловушек. Проводит настройку выданных из пула объектов
    /// и инкапсулирует их данные, отдавай объекты по интерфейсу.
    /// <see cref="https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80"/>
    /// </summary>
    public sealed class TrapFactory
    {
        private readonly TrapPoolRepository _poolRepository;

        public TrapFactory([NotNull] TrapPoolRepository poolRepository) =>
            _poolRepository = poolRepository;

        public ITrap Create([NotNull] ITrap prefab, SpawnMessage spawnMessage) =>
            Create(prefab.Identity.assetId, spawnMessage);

        public ITrap Create(Guid assetId, SpawnMessage spawnMessage)
        {
            IPool<Trap> pool = _poolRepository[assetId];
            Trap trap = pool.Get();

            trap.gameObject.transform.position = spawnMessage.position;
            trap.gameObject.transform.rotation = spawnMessage.rotation;
            trap.gameObject.transform.localScale = spawnMessage.scale;

            return trap;
        }
    }
}
