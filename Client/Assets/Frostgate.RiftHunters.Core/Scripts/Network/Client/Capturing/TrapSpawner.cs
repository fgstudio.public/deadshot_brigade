using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// Client Only. Doesn't work in host-mode.
    /// Компонент для спауна на клиенте сгенерированных сервером ловушек
    /// <see cref="https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80"/>
    /// </summary>
    public sealed class TrapSpawner : ISpawner
    {
        private readonly ObjectCollectionRepository<ITrap> _collections;
        private readonly ITrapPrefabRoster _prefabRoster;
        private readonly TrapFactory _factory;


        public TrapSpawner(
            [NotNull] ITrapPrefabRoster prefabRoster,
            [NotNull] TrapFactory factory,
            [NotNull] ObjectCollectionRepository<ITrap> collections)

        {
            _prefabRoster = prefabRoster;
            _collections = collections;
            _factory = factory;

            RegisterPrefabs(_prefabRoster.Prefabs);
        }

        public void Dispose()
        {
            UnregisterPrefabs(_prefabRoster.Prefabs);
        }

        private void RegisterPrefabs(IEnumerable<ITrap> prefabs)
        {
            foreach (ITrap trap in prefabs)
                NetworkClient.RegisterPrefab(
                    trap.GameObject, trap.Identity.assetId, Spawn, Unspawn);
        }

        private void UnregisterPrefabs(IEnumerable<ITrap> prefabs)
        {
            foreach (ITrap trap in prefabs)
                NetworkClient.UnregisterPrefab(trap.GameObject);
        }

        private GameObject Spawn(SpawnMessage spawnMessage)
        {
            ITrap trap = _factory.Create(spawnMessage.assetId, spawnMessage);

            if (trap.State.IsInitialized) AddToCollections(trap);
            else trap.State.Initialized += AddToCollections;

            return trap.GameObject;
        }

        private void Unspawn(GameObject gameObject)
        {
            var trap = gameObject.GetComponent<ITrap>();
            trap.State.Initialized -= AddToCollections;
            RemoveFromCollections(trap);
            trap.Destroy();
        }

        private void AddToCollections(ITrap trap)
        {
            _collections.Get<uint>().Add(trap.Identity.netId, trap);
            _collections.Get<Collider>().Add(trap.Physics.Collider, trap);
        }

        private void RemoveFromCollections(ITrap trap)
        {
            _collections.Get<uint>().Remove(trap.Identity.netId);
            _collections.Get<Collider>().Remove(trap.Physics.Collider);
        }
    }
}