using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Observing;
using Frostgate.RiftHunters.Core.Battle.Client.Objects.StreakBonus;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0"/>
    /// </summary>
    public sealed class StreakBonusInitializeObservable : ObjectObserver<IReadOnlyObjectCollection<uint, ISharedStreakBonus>>
    {
        private readonly ILogger _logger;
        private readonly IPrefabFactory _prefabFactory;
        private readonly StreakBonusViewSettings _viewSettings;
        private readonly StreakBonusMechanicSettings _mechanicSettings;
        private readonly ClientModulePrefabRepository _prefabRepository;

        public StreakBonusInitializeObservable(
            [NotNull] IPrefabFactory prefabFactory,
            [NotNull] StreakBonusViewSettings viewSettings,
            [NotNull] StreakBonusMechanicSettings mechanicSettings,
            [NotNull] ClientModulePrefabRepository prefabRepository,
            [NotNull] ILogger<StreakBonusInitializeObservable> logger)
        {
            _logger = logger;
            _viewSettings = viewSettings;
            _prefabFactory = prefabFactory;
            _mechanicSettings = mechanicSettings;
            _prefabRepository = prefabRepository;
        }

        protected override void OnSet(IReadOnlyObjectCollection<uint, ISharedStreakBonus> observable) =>
            observable.ForEach(OnAdded);

        protected override void Subscribe(IReadOnlyObjectCollection<uint, ISharedStreakBonus> observable) =>
            observable.Added += OnAdded;

        protected override void Unsubscribe(IReadOnlyObjectCollection<uint, ISharedStreakBonus> observable) =>
            observable.Added -= OnAdded;

        private void OnAdded(ISharedStreakBonus bonus)
        {
            IReadOnlyStreakBonusState state = bonus.State;

            StreakBonusClientModule modulePrefab = _prefabRepository[state.ConfigId];
            Transform parent = bonus.GameObject!.transform;

            var module = _prefabFactory.Instantiate<StreakBonusClientModule>(modulePrefab, parent);
            module.Init(state, _viewSettings, _mechanicSettings);

            _logger.Log($"Initialize [netId:{bonus.Identity.netId}]");
        }
    }
}
