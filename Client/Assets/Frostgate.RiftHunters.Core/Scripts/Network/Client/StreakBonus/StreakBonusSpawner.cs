using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// Client Only. Doesn't work in host-mode
    /// <see cref="https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0"/>
    /// </summary>
    public sealed class StreakBonusSpawner : ISpawner
    {
        private readonly ILogger _logger;
        private readonly StreakBonusFactory _factory;
        private readonly IObjectCollection<uint, ISharedStreakBonus> _bonusCollection;

        public StreakBonusSpawner(
            [NotNull] StreakBonusFactory factory,
            [NotNull] ILogger<StreakBonusSpawner> logger,
            [NotNull] IObjectCollection<uint, ISharedStreakBonus> bonusCollection)
        {
            _logger = logger;
            _factory = factory;
            _bonusCollection = bonusCollection;
            RegisterPrefab(factory.CorePrefab);
        }

        public void Dispose() =>
            UnregisterPrefab(_factory.CorePrefab);

        private void RegisterPrefab(ISharedStreakBonus prefab) =>
            NetworkClient.RegisterPrefab(prefab.GameObject, prefab.Identity.assetId,
                Spawn, Unspawn);

        private void UnregisterPrefab(ISharedStreakBonus prefab) =>
            NetworkClient.UnregisterPrefab(prefab.GameObject);

        private GameObject Spawn(SpawnMessage spawnMessage)
        {
            ISharedStreakBonus bonus = _factory.CreateCoreObject(spawnMessage);

            if (bonus.State.IsInitialized) OnInitialized(bonus.State);
            else bonus.State.Initialized.AddListener(OnInitialized);
            _logger.Log("Spawn");

            return bonus.GameObject;
        }

        private void Unspawn(GameObject gameObject)
        {
            var bonus = gameObject.GetComponent<ISharedStreakBonus>();
            bonus.State.Initialized.RemoveListener(OnInitialized);
            RemoveFromCollection(bonus);
            _logger.Log("Unspawn");

            Object.Destroy(gameObject);
        }

        private void OnInitialized(IReadOnlyStreakBonusState state)
        {
            state.Initialized.RemoveListener(OnInitialized);
            AddToCollection(state.CoreObject);
        }

        private void AddToCollection(ISharedStreakBonus bonus) =>
            _bonusCollection.Add(bonus.Identity.netId, bonus);

        private void RemoveFromCollection(ISharedStreakBonus bonus) =>
            _bonusCollection.Remove(bonus.Identity.netId);
    }
}
