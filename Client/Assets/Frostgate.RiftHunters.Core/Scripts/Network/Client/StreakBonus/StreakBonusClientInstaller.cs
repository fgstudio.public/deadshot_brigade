using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Client.Objects.StreakBonus;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [CreateAssetMenu(fileName = nameof(StreakBonusClientInstaller),
        menuName = StreakBonusAssetMenu.Client + "/" + nameof(StreakBonusClientInstaller))]
    public sealed class StreakBonusClientInstaller : ScriptableObjectInstaller
    {
        [Required, AssetSelector]
        [SerializeField] private StreakBonusViewSettings _viewSettings;

        [Required, AssetSelector]
        [SerializeField] private ClientModulePrefabRepository _modulePrefabRepository;

        public override void InstallBindings()
        {
            Container.Bind<StreakBonusViewSettings>()
                .FromInstance(_viewSettings).AsSingle();

            Container.Bind<ClientModulePrefabRepository>()
                .FromInstance(_modulePrefabRepository).AsSingle();

            Container.Bind<StreakBonusFactory>().AsSingle();
            Container.Bind<StreakBonusInitializeObservable>().AsSingle();
        }
    }
}
