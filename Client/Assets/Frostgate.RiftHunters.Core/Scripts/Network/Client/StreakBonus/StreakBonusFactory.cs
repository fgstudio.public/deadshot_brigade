using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Network.Client
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0"/>
    /// </summary>
    public sealed class StreakBonusFactory
    {
        private readonly IPrefabFactory _prefabFactory;
        private readonly StreakBonusCoreObject _corePrefab;

        public ISharedStreakBonus CorePrefab => _corePrefab;

        public StreakBonusFactory(
            [NotNull] IPrefabFactory prefabFactory,
            [NotNull] StreakBonusCoreObject corePrefab)
        {
            _corePrefab = corePrefab;
            _prefabFactory = prefabFactory;
        }

        public ISharedStreakBonus CreateCoreObject(SpawnMessage spawnMessage)
        {
            var coreObject = _prefabFactory.Instantiate<StreakBonusCoreObject>(_corePrefab);

            coreObject.gameObject.transform.position = spawnMessage.position;
            coreObject.gameObject.transform.rotation = spawnMessage.rotation;
            coreObject.gameObject.transform.localScale = spawnMessage.scale;

            return coreObject;
        }
    }
}
