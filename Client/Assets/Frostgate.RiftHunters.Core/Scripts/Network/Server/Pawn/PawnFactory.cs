﻿using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Impact;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class PawnFactory
    {
        [NotNull] private readonly UnitNetwork _pawnPrefab;
        [NotNull] private readonly IPrefabFactory _prefabFactory;
        [NotNull] private readonly IPawnStateMachineHandlerFactory _smHandlerFactory;
        [NotNull] private readonly ImpactStateInitializer _impactStateInitializer;

        public PawnFactory([NotNull] UnitNetwork pawnPrefab, [NotNull] IPrefabFactory prefabFactory,
            [NotNull] IPawnStateMachineHandlerFactory smHandlerFactory,
            [NotNull] ImpactStateInitializer impactStateInitializer)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(pawnPrefab, nameof(pawnPrefab));
            ThrowHelper.ThrowIfArgumentNullOrDefault(prefabFactory, nameof(prefabFactory));
            ThrowHelper.ThrowIfArgumentNullOrDefault(smHandlerFactory, nameof(smHandlerFactory));
            ThrowHelper.ThrowIfArgumentNullOrDefault(impactStateInitializer, nameof(impactStateInitializer));

            _pawnPrefab = pawnPrefab;
            _prefabFactory = prefabFactory;
            _smHandlerFactory = smHandlerFactory;
            _impactStateInitializer = impactStateInitializer;
        }

        public UnitNetwork Create(PawnCreationData creationData)
        {
            UnitNetwork pawnInstance = _prefabFactory.Instantiate<UnitNetwork>(_pawnPrefab, creationData.Position,
                creationData.Rotation, creationData.Container);

            pawnInstance.InitServer(creationData.Type, creationData.Config, creationData.Equipments,
                creationData.PlayerId, creationData.Name, creationData.HeroLevel);
            BehaviourStateMachineHandler smHandler = _smHandlerFactory.Create(pawnInstance);
            pawnInstance.SetStateMachineHandler(smHandler);
            _impactStateInitializer.Init(pawnInstance.ImpactState, pawnInstance);

            return pawnInstance;
        }
    }
}