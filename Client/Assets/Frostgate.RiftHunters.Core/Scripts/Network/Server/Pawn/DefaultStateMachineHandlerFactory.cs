using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;
using Frostgate.RiftHunters.Core.Battle.Shared.Shooting;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class DefaultStateMachineHandlerFactory : IPawnStateMachineHandlerFactory
    {
        private readonly IServiceRoster _serviceRoster;
        private readonly IAimTargetFinder _aimTargetFinder;

        public DefaultStateMachineHandlerFactory([NotNull] IServiceRoster serviceRoster,
            [NotNull] IAimTargetFinder aimTargetFinder)
        {
            _serviceRoster = serviceRoster;
            _aimTargetFinder = aimTargetFinder;
        }

        public BehaviourStateMachineHandler Create([NotNull] UnitNetwork pawn)
        {
            var stateFactory = new BehaviourStateFactory(pawn, _serviceRoster, _aimTargetFinder);
            var stateRepository = new BehaviourStateRepository(stateFactory);
            var stateMachine = new BehaviourStateMachine(pawn, stateRepository);

            return new BehaviourStateMachineHandler(pawn, stateMachine);
        }
    }
}
