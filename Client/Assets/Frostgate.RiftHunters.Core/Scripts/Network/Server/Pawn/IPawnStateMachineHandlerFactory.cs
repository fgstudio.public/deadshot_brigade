﻿using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.FSM;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public interface IPawnStateMachineHandlerFactory
    {
        BehaviourStateMachineHandler Create(UnitNetwork pawn);
    }
}