﻿using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public struct PawnCreationData
    {
        [NotNull] public UnitConfig Config { get; }
        [NotNull] public Equipment[] Equipments { get; set; }

        [CanBeNull] public Transform Container { get; set; }
        public Vector3 Position { get; set; }
        public Quaternion Rotation { get; set; }


        // TEMP SECTION
        public BattleUnitType Type { get; set; }
        [CanBeNull] public string PlayerId { get; set; }
        [CanBeNull] public string Name { get; set; }
        public int HeroLevel { get; set; }


        public PawnCreationData([NotNull] UnitConfig config, [NotNull] Equipment[] equipments, int heroLevel)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(config);

            Config = config;
            Equipments = equipments;

            Container = null;
            Position = default;
            Rotation = default;
            Type = BattleUnitType.Player;
            PlayerId = null;
            Name = null;
            HeroLevel = heroLevel;
        }
    }
}