using System;
using System.Linq;
using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Energy;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Обработка серверной логики работы энергетических капсул.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class EnergyCapsuleMediator : IDisposable
    {
        private readonly EnergyCapsuleCollection _energyCapsuleCollection;

        private readonly EnergyCapsuleFactory _factory;
        private readonly EnergyCapsuleChanceDropper _chanceDropper;

        public EnergyCapsuleMediator(
            [NotNull] EnergyCapsuleFactory factory, [NotNull] EnergyCapsuleChanceDropper chanceDropper)
        {
            _energyCapsuleCollection = new EnergyCapsuleCollection();

            _factory = factory;
            _chanceDropper = chanceDropper;
        }

        public void Dispose()
        {
            RemoveAllCapsules();
        }

        public IServerEnergyCapsule CreateCapsule([NotNull] IEnergyCapsuleSource capsuleSource)
        {
            if (!_chanceDropper.CanDrop(capsuleSource))
                return null;

            EnergyCapsuleConfig capsuleConfig = capsuleSource.EnergyCapsuleForDeath;
            IServerEnergyCapsule energyCapsule = _factory.Create(capsuleConfig, capsuleSource);

            SubscribeOnCapsule(energyCapsule);

            _energyCapsuleCollection.Add(energyCapsule);
            NetworkServer.Spawn(energyCapsule.GameObject);

            return energyCapsule;
        }

        public void RemoveAllCapsules() =>
            _energyCapsuleCollection.Values.ToArray()
                .ForEach(c => c.Destroy());

        private void SubscribeOnCapsule(IServerEnergyCapsule energyCapsule)
        {
            energyCapsule.Collected += OnCollected;
            energyCapsule.ReadyForRelease += OnReadyForRelease;
        }

        private void UnsubscribeFromCapsule(IServerEnergyCapsule energyCapsule)
        {
            energyCapsule.Collected -= OnCollected;
            energyCapsule.ReadyForRelease -= OnReadyForRelease;
        }

        private void OnCollected(IServerEnergyCapsule energyCapsule)
        {
            _energyCapsuleCollection.Remove(energyCapsule);
        }

        private void OnReadyForRelease(IAutoReleased autoReleased)
        {
            if (autoReleased is IServerEnergyCapsule capsule)
            {
                UnsubscribeFromCapsule(capsule);

                _energyCapsuleCollection.Remove(capsule);
                NetworkServer.UnSpawn(capsule.GameObject);
            }
        }
    }
}