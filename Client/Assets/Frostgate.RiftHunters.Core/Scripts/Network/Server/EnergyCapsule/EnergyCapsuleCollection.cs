using Mirror;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// КО: Коллекция энергетических капсул.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class EnergyCapsuleCollection
    {
        private readonly Dictionary<NetworkIdentity, IServerEnergyCapsule> _energyCapsules = new();

        public IEnumerable<NetworkIdentity> Keys => _energyCapsules.Keys;
        public IEnumerable<IServerEnergyCapsule> Values => _energyCapsules.Values;

        public void Add([NotNull] IServerEnergyCapsule energyCapsule) =>
            _energyCapsules[energyCapsule.Identity] = energyCapsule;

        public void Remove([NotNull] IServerEnergyCapsule energyCapsule) =>
            Remove(energyCapsule.Identity);

        public void Remove([NotNull] NetworkIdentity identity) =>
            _energyCapsules.Remove(identity);

        public bool TryGet([NotNull] NetworkIdentity identity, out IServerEnergyCapsule energyCapsule) =>
            _energyCapsules.TryGetValue(identity, out energyCapsule);

    }
}