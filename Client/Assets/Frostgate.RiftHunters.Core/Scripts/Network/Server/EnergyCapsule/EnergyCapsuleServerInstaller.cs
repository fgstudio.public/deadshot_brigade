using Frostgate.RiftHunters.Core.Battle;
using Zenject;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    [CreateAssetMenu(fileName = nameof(EnergyCapsuleServerInstaller),
        menuName = BattleAssetMenus.Shared.Menu + "/" + nameof(EnergyCapsuleServerInstaller))]
    [HelpURL("https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b")]
    public sealed class EnergyCapsuleServerInstaller : ScriptableObjectInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<EnergyCapsulePositionCalculator>().AsSingle();
            Container.Bind<EnergyCapsuleFactory>().AsSingle();
            Container.Bind<EnergyCapsuleChanceDropper>().AsSingle();
            Container.Bind<EnergyCapsuleMediator>().AsSingle();
        }
    }
}