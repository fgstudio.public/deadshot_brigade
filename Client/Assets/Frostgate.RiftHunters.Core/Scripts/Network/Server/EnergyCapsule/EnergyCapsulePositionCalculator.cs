using UnityEngine;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Класс для вычисления позиции энерегитической капсулы.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class EnergyCapsulePositionCalculator
    {
        private readonly AreaRandom _random;

        public EnergyCapsulePositionCalculator(AreaRandom random) =>
            _random = random;

        public Vector3 Calc(EnergyCapsuleConfig capsuleConfig, Vector3 targetPosition) =>
            targetPosition + CalcPositionOffset(capsuleConfig);

        private Vector3 CalcPositionOffset(EnergyCapsuleConfig capsuleConfig) =>
            _random.SquareY(Vector3.zero, capsuleConfig.MaxRandomPositionOffset * 2);
    }
}
