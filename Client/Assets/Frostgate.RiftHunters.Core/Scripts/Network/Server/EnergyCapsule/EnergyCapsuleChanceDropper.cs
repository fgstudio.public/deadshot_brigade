using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Energy;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Определение шанса оставить после смерти энергетическую капсулу.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class EnergyCapsuleChanceDropper
    {
        private readonly IRandom _random;

        public EnergyCapsuleChanceDropper(IRandom random) =>
            _random = random;

        public bool CanDrop([NotNull] IEnergyCapsuleSource energyCapsuleSource)
        {
            float chance = _random.Range(EnergyCapsuleConfig.MinChance, EnergyCapsuleConfig.MaxChance);
            return chance <= energyCapsuleSource.EnergyCapsuleForDeath.DropChance;
        }
    }
}
