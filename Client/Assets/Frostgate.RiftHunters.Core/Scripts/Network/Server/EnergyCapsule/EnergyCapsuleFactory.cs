using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Energy;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Фабрика энергетических капсул. Проводит настройку выданных из пула объектов
    /// и инкапсулирует их данные, отдавай объекты по интерфейсу.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class EnergyCapsuleFactory
    {
        private readonly EnergyCapsulePool _pool;
        private readonly EnergyCapsulePositionCalculator _positionCalculator;

        public EnergyCapsuleFactory([NotNull] EnergyCapsulePool pool, [NotNull] EnergyCapsulePositionCalculator positionCalculator)
        {
            _pool = pool;
            _positionCalculator = positionCalculator;
        }

        public IServerEnergyCapsule Create([NotNull] EnergyCapsuleConfig capsuleConfig,
            [NotNull] IEnergyCapsuleSource capsuleHost)
        {
            var energyCapsule = _pool.Get();
            energyCapsule.State.Energy = capsuleConfig.Energy;
            energyCapsule.transform.position = _positionCalculator.Calc(capsuleConfig, capsuleHost.Transform.position.SetY(default));
            return energyCapsule;
        }
    }
}