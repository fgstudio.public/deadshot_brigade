﻿using JetBrains.Annotations;
using Mirror;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public interface IRestoreWatchdog
    {
        void OnServerConnect([NotNull] NetworkConnectionToClient client);
        void OnServerDisconnect([NotNull] NetworkConnectionToClient client);
    }
}