﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    // TODO: Переименовать во что-то адекватное
    public sealed class ServerRestoreWatchdog : IRestoreWatchdog
    {
        private const int ShutdownDelayAfterStartSeconds = 45;
        private const int ShutdownDelaySeconds = 5;

        [NotNull] private readonly Action _onBeforeRestoring;
        [NotNull] private readonly ILogger<ServerRestoreWatchdog> _logger;

        [NotNull] private readonly ICollection<NetworkConnectionToClient> _connections =
            new HashSet<NetworkConnectionToClient>();

        private CancellationTokenSource _shutdownCts;
        private bool _isShutdownScheduled;

        public ServerRestoreWatchdog([NotNull] Action onBeforeRestoring,
            [NotNull] ILogger<ServerRestoreWatchdog> logger)
        {
            _onBeforeRestoring = onBeforeRestoring;
            _logger = logger;

            // Если сервер запустился и в течение 45 секунд никто не подключится, то выключаемся
            ScheduleShutdown(ShutdownDelayAfterStartSeconds);
        }

        public void OnServerConnect(NetworkConnectionToClient client)
        {
            if (_isShutdownScheduled)
                CancelShutdown();

            _connections.Add(client);
            _logger.LogInfo($"Connection handled. Total connections = {_connections.Count}.");
        }

        public void OnServerDisconnect(NetworkConnectionToClient client)
        {
            if (!_connections.Remove(client))
            {
                _logger.LogWarning(
                    $"Client connection '{client.connectionId}' '{client.address}' not registered. Ignoring.");
                return;
            }

            _logger.LogInfo($"Disconnection handled. Total connections = {_connections.Count}.");

            if (_connections.Count == 0)
                ScheduleShutdown(ShutdownDelaySeconds);
        }

        private async void ScheduleShutdown(int seconds)
        {
            _shutdownCts = new CancellationTokenSource();
            _isShutdownScheduled = true;

            int shutdownAfterMs = seconds * 1000;
            UniTask shutdownTask = UniTask
                .Delay(shutdownAfterMs, DelayType.Realtime, cancellationToken: _shutdownCts.Token)
                .ContinueWith(Shutdown).SuppressCancellationThrow();

            _logger.LogInfo($"Shutdown scheduled after {shutdownAfterMs} milliseconds.");

            await shutdownTask;
        }

        private void Shutdown()
        {
            _onBeforeRestoring.Invoke();
            Application.Quit();
        }

        private void CancelShutdown()
        {
            _shutdownCts.Cancel();
            _shutdownCts.Dispose();
            _isShutdownScheduled = false;

            _logger.LogInfo("Reboot cancelled.");
        }
    }
}