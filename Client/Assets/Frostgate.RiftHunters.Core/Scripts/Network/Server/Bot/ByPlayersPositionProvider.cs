﻿using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Battle.Server
{
    public sealed class ByPlayersPositionProvider : IPositionProvider
    {
        public Vector3 Position => CalculateHomePosition();

        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _units;

        public ByPlayersPositionProvider(
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> units)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(units, nameof(units));

            _units = units;
        }

        private Vector3 CalculateHomePosition()
        {
            int playersCounter = 0;
            Vector2 posSum = Vector2.zero;

            // TODO: Не очень оптимальный вариант
            foreach (var unit in _units)
            {
                if (unit.UnitType != BattleUnitType.Player || !unit.NetworkIdentity.hasAuthority)
                    continue;

                Vector3 unitPos = unit.Transform.position;
                Vector2 unitPos2D = new Vector2(unitPos.x, unitPos.z);
                posSum += unitPos2D;

                playersCounter++;
            }

            Vector2 avgPos = posSum / playersCounter;

            return new Vector3(avgPos.x, 0f, avgPos.y);
        }
    }
}