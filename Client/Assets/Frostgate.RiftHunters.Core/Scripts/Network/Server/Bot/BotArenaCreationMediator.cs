using System.Linq;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class BotArenaCreationMediator
    {
        [NotNull] private readonly BotArenaFactory _factory;
        [NotNull] private readonly IObjectCollection<BotArenaConfig, BotArena> _arenaCollection;

        public BotArenaCreationMediator(
            [NotNull] BotArenaFactory factory,
            [NotNull] IObjectCollection<BotArenaConfig, BotArena> arenaCollection)
        {
            _factory = factory;
            _arenaCollection = arenaCollection;
        }

        public BotArena CreateArena([NotNull] BotArenaSceneData sceneData, int arenaIndex)
        {
            BotArena botArena = _factory.Create(sceneData, arenaIndex);
            _arenaCollection.Add(botArena.Config, botArena);

            return botArena;
        }

        public void RemoveAllArenas() =>
            _arenaCollection.Keys.ToArray()
                .ForEach(RemoveArena);

        private void RemoveArena([NotNull] BotArenaConfig config)
        {
            _arenaCollection[config].Dispose();
            _arenaCollection.Remove(config);
        }
    }
}
