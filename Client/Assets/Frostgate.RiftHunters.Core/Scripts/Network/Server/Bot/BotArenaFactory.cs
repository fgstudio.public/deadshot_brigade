using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Server.Activity;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Activity;
using Frostgate.RiftHunters.Core.Battle.Shared.Checkpoints;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class BotArenaFactory
    {
        [NotNull] private readonly IEditableBotArenaState _arenaState;
        [NotNull] private readonly IReadOnlyBotSpawnState _spawnState;
        [NotNull] private readonly BotSpawnerFactory _spawnerFactory;
        [NotNull] private readonly IActivityFactory _activityFactory;
        [NotNull] private readonly IActualRespawnPoint _respawnPoint;

        public BotArenaFactory(
            [NotNull] IEditableBotArenaState arenaState,
            [NotNull] IReadOnlyBotSpawnState spawnState,
            [NotNull] BotSpawnerFactory spawnerFactory,
            [NotNull] IActivityFactory activityFactory,
            [NotNull] IActualRespawnPoint respawnPoint)
        {
            _arenaState = arenaState;
            _spawnState = spawnState;
            _spawnerFactory = spawnerFactory;
            _activityFactory = activityFactory;
            _respawnPoint = respawnPoint;
        }

        public BotArena Create([NotNull] BotArenaSceneData sceneData, int arenaIndex)
        {
            EnumerableEnumerator<IActivity> activities =
                CreateActivities(sceneData);

            IReadOnlyList<BotSpawnInArenaHandler> spawnHandlers =
                CreateSpawnHandlers(sceneData);

            return new(arenaIndex, sceneData.Config, _arenaState,
                sceneData.EffectsArea, sceneData.ExitPassage?.State,
                activities, sceneData.ActiveObjects, spawnHandlers, sceneData.RespawnPoint, _respawnPoint);
        }

        private EnumerableEnumerator<IActivity> CreateActivities(
            [NotNull] BotArenaSceneData sceneData)
        {
            IEnumerable<IActivity> tasks =
                Enumerable.Range(0, sceneData.Activities.Count)
                .Select(i => CreateActivity(sceneData, i));

            return new EnumerableEnumerator<IActivity>(tasks);
        }

        private IActivity CreateActivity(
            [NotNull] BotArenaSceneData sceneData, int index)
        {
            ActivityConfig config = sceneData.Config.ActivityConfigs[index];
            ActivitySceneData aData = sceneData.Activities[index];

            return _activityFactory.Create(config, aData);
        }

        private IReadOnlyList<BotSpawnInArenaHandler> CreateSpawnHandlers(
            [NotNull] BotArenaSceneData sceneData)
        {
            return sceneData.SpawnerData.Select(CreateSpawnHandler).ToArray();
        }

        private BotSpawnInArenaHandler CreateSpawnHandler([NotNull] BotSpawnerSceneData sceneData)
        {
            BotSpawner spawner = _spawnerFactory
                .Create(sceneData.Config, sceneData.SpawnAreas);

            ILogger<BotSpawnInArenaHandler> logger =
                LoggerFactory.CreateLogger<BotSpawnInArenaHandler>();

            return new BotSpawnInArenaHandler(
                logger, spawner, sceneData.Activation, sceneData.Deactivation, _spawnState);
        }
    }
}