﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class BotCustomSpawnerMediator : IDisposable
    {
        [NotNull] private readonly SpawnAreaFinder _spawnAreaFinder;
        [NotNull] private readonly BotSpawnerFactory _spawnerFactory;

        [NotNull, ItemNotNull] private readonly HashSet<BotSpawner> _spawners = new();

        public BotCustomSpawnerMediator(
            [NotNull] BotSpawnerFactory spawnerFactory,
            [NotNull] SpawnAreaFinder spawnAreaFinder)
        {
            _spawnerFactory = spawnerFactory;
            _spawnAreaFinder = spawnAreaFinder;
        }

        public void Dispose() =>
            StopAllSpawners();

        public void SpawnOnNearestArea([NotNull] BotSpawnerConfig spawnerConfig, [NotNull] IPositionProvider positionProvider)
        {
            const bool isCustom = true;

            Vector3 nearestPosition = positionProvider.Position;
            BotSpawnArea nearestArea = _spawnAreaFinder.FindNearestOnCurrentArena(nearestPosition, isCustom);
            BotSpawner spawner = _spawnerFactory.Create(spawnerConfig, nearestArea, positionProvider);
            _spawners.Add(spawner);

            spawner.StartSpawn()
                .ContinueWith(() => RemoveSpawner(spawner))
                .AsAsyncUnitUniTask();
        }

        public void StopAllSpawners()
        {
            if (_spawners.Count == 0)
                return;

            BotSpawner[] spawnersCopy = _spawners.ToArray();
            _spawners.Clear();

            spawnersCopy.ForEach(s => s.Dispose());
        }

        private void RemoveSpawner([NotNull] BotSpawner spawner)
        {
            if (_spawners.Contains(spawner))
            {
                _spawners.Remove(spawner);
                spawner.Dispose();
            }
        }

    }
}