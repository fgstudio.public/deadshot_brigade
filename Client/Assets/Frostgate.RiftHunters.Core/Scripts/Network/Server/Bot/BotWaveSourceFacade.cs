using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.DDA;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class BotWaveSourceFacade : IDisposable
    {
        [NotNull] private readonly IDdaSystem _ddaSystem;
        [NotNull] private readonly Dictionary<BotSpawnerConfig, BotWaveSource> _waveSources = new();

        public BotWaveSourceFacade([NotNull] IDdaSystem ddaSystem) =>
            _ddaSystem = ddaSystem;

        public void Dispose()
        {
            foreach (BotWaveSource waveSource in _waveSources.Values)
            {
                waveSource.Dispose();
                _ddaSystem.UnregisterService(waveSource);
            }

            _waveSources.Clear();
        }

        [NotNull]
        public BotWaveSource CreateWaveSource([NotNull] BotSpawnerConfig spawnerConfig)
        {
            var waveSource = new BotWaveSource(spawnerConfig);
            _waveSources[spawnerConfig] = waveSource;
            _ddaSystem.RegisterService(waveSource);
            // TODO: регистрация и обратная регистрация должна происходит при вкл/выкл ReferenceRoster

            return waveSource;
        }
    }
}
