using System;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class BotArenaMediator : IDisposable
    {
        [NotNull] private readonly BotArenaCreationMediator _creationMediator;
        [NotNull] private readonly BotArenaActivationMediator _activationMediator;

        public bool CanRestartCurrentArena =>
            _activationMediator.Current is { Status: BotArenaStatus.Activated };

        public BotArenaMediator(
            [NotNull] BotArenaCreationMediator creationMediator,
            [NotNull] BotArenaActivationMediator activationMediator)
        {
            _creationMediator = creationMediator;
            _activationMediator = activationMediator;
        }

        public void Dispose()
        {
            _activationMediator.DeactivateAllArenas();
            _creationMediator.RemoveAllArenas();
        }

        public void ActivateNextArena() =>
            _activationMediator.TryActivateNextArena();

        public void CreateArenas([NotNull, ItemNotNull] IReadOnlyList<BotArenaSceneData> sceneData)
        {
            for (int i = 0; i < sceneData.Count; i++)
                _creationMediator.CreateArena(sceneData[i], i);
        }

        public void RestartCurrentArena() =>
            _activationMediator.Current?.Restart()
                .AsAsyncUnitUniTask();
    }
}