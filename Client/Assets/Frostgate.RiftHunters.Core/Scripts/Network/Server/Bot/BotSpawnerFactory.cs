using System;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class BotSpawnerFactory
    {
        [NotNull] private readonly SimulationTime _time;
        [NotNull] private readonly IBotSpawnFacade _botSpawnFacade;
        [NotNull] private readonly IEditableBotSpawnState _botSpawnState;
        [NotNull] private readonly BotWaveSourceFacade _waveSourceFacade;

        public BotSpawnerFactory(
            [NotNull] SimulationTime time,
            [NotNull] IBotSpawnFacade botSpawnFacade,
            [NotNull] IEditableBotSpawnState botSpawnState,
            [NotNull] BotWaveSourceFacade waveSourceFacade)
        {
            _time = time;
            _botSpawnFacade = botSpawnFacade;
            _botSpawnState = botSpawnState;
            _waveSourceFacade = waveSourceFacade;
        }

        public BotSpawner Create(
            [NotNull] BotSpawnerConfig spawnerConfig,
            [NotNull, ItemNotNull] IReadOnlyList<BotSpawnArea> spawnAreas)
        {
            ISpawnPositionProvider positionProvider = CreatePositionProvider(spawnAreas);
            BotWaveSource waveSource = _waveSourceFacade.CreateWaveSource(spawnerConfig);

            return new BotSpawner(spawnerConfig.Id, _time, waveSource,
                _botSpawnState, _botSpawnFacade, positionProvider);
        }

        public BotSpawner Create([NotNull] BotSpawnerConfig spawnerConfig, [NotNull] BotSpawnArea spawnArea,
            [NotNull] IPositionProvider homePositionProvider)
        {
            BotWaveSource waveSource = _waveSourceFacade.CreateWaveSource(spawnerConfig);
            return new BotSpawner(spawnerConfig.Id, _time, waveSource,
                _botSpawnState, _botSpawnFacade, spawnArea, homePositionProvider);
        }

        private ISpawnPositionProvider CreatePositionProvider([NotNull, ItemNotNull] IReadOnlyList<BotSpawnArea> spawnAreas) =>
            spawnAreas.Count switch
            {
                0 => new StubSpawnPositionProvider(),
                1 => spawnAreas[0],
                > 1 => new CompositeSpawnPositionProvider(spawnAreas),
                _ => throw new ArgumentOutOfRangeException(nameof(spawnAreas.Count))
            };
    }
}