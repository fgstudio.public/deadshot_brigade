using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class BotArenaActivationMediator : IDisposable
    {
        [NotNull] private readonly IEditableBotArenaState _state;
        [NotNull] private readonly IEnumerator<BotArena> _arenaIterator;

        [CanBeNull] public BotArena Current => _arenaIterator.Current;

        public BotArenaActivationMediator([NotNull] IEditableBotArenaState state, 
            [NotNull] IEnumerator<BotArena> arenaIterator)
        {
            _state = state;
            _arenaIterator = arenaIterator;

            _state.DataUpdated.AddListener(OnDataUpdated);
        }

        public void Dispose()
        {
            _arenaIterator.Reset();
        }

        public bool TryActivateNextArena()
        {
            bool hasCurrent = _arenaIterator.MoveNext() &&
                              _arenaIterator.Current != null;

            if (hasCurrent)
                _arenaIterator.Current.Activate();

            return hasCurrent;
        }

        public void DeactivateAllArenas()
        {
            _arenaIterator.Reset();

            while (_arenaIterator.MoveNext())
                _arenaIterator.Current?.Deactivate();

            _arenaIterator.Reset();
        }

        private void OnDataUpdated(string arenaId)
        {
            if (_state.GetStatus(arenaId) != BotArenaStatus.Completed) return;
            if (_arenaIterator.Current == null) return;
            if (_arenaIterator.Current.Config.Id != arenaId) return;

            if (!TryActivateNextArena())
                _state.AreAllArenasCompleted = true;
        }
    }
}