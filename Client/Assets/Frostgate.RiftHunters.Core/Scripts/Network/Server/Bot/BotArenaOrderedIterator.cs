using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Server.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class BotArenaOrderedIterator : IEnumerator<BotArena>
    {
        private const int InitialIndex = -1;
        private const int DisposedIndex = int.MaxValue;

        [NotNull, ItemNotNull] private readonly IReadOnlyList<BotArenaConfig> _orderedConfigs;
        [NotNull] private readonly IReadOnlyObjectCollection<BotArenaConfig, BotArena> _arenas;

        private int _currentIndex;

        public BotArena Current => _arenas[_orderedConfigs[_currentIndex]];
        object IEnumerator.Current => Current;

        private bool IsDisposed => _currentIndex == DisposedIndex;

        public BotArenaOrderedIterator([
                NotNull] [ItemNotNull] IReadOnlyList<BotArenaConfig> orderedConfigs,
            [NotNull] IReadOnlyObjectCollection<BotArenaConfig, BotArena> arenas)
        {
            _orderedConfigs = orderedConfigs;
            _arenas = arenas;
            _currentIndex = InitialIndex;
        }

        public void Dispose() =>
            _currentIndex = DisposedIndex;

        public void Reset()
        {
            if (!IsDisposed)
                _currentIndex = InitialIndex;
        }

        public bool MoveNext()
        {
            bool canIncreaseIndex = _currentIndex < _orderedConfigs.Count - 1;
            if (canIncreaseIndex)
            {
                _currentIndex++;
                return true;
            }

            return false;
        }
    }
}
