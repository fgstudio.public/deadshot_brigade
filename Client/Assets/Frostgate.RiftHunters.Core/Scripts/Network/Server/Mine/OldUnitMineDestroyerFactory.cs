using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class OldUnitMineDestroyerFactory
    {
        [NotNull] private readonly SimulationTime _time;
        [NotNull] private readonly ILogger<OldUnitMineDestroyer> _logger;

        public OldUnitMineDestroyerFactory([NotNull] SimulationTime time,
            [NotNull] ILogger<OldUnitMineDestroyer> logger)
        {
            _time = time;
            _logger = logger;
        }

        [NotNull]
        public OldUnitMineDestroyer Create(uint hostId)
        {
            _logger.Log($"Created handler for host {hostId}");
            return new OldUnitMineDestroyer(hostId, _time, _logger);
        }
    }
}
