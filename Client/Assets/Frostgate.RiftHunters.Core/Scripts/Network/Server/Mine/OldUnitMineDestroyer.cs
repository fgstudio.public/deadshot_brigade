using System;
using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class OldUnitMineDestroyer : IDisposable
    {
        private const float ExpiryTime = 1;

        public event Action<IMine> HostIdChanged;

        private readonly uint _hostId;
        [NotNull] private readonly SimulationTime _time;
        [NotNull] private readonly Dictionary<IMine, float> _plantTimes;
        [NotNull] private readonly ILogger<OldUnitMineDestroyer> _logger;

        public OldUnitMineDestroyer(uint hostId,
            [NotNull] SimulationTime time,
            [NotNull] ILogger<OldUnitMineDestroyer> logger)
        {
            _time = time;
            _hostId = hostId;
            _logger = logger;

            _plantTimes = new Dictionary<IMine, float>();
        }

        public void Dispose() =>
            RemoveAllMines();

        public void Add([NotNull] IMine mine)
        {
            _plantTimes[mine] = _time.Time;
            mine.State.HostUnitIdChanged += RemoveAlienMines;
            _logger.Log($"Added mine {mine.Identity.netId} of host {_hostId}");

            DestroyOldMines();
        }

        public void Remove([NotNull] IMine mine)
        {
            _plantTimes.Remove(mine);
            mine.State.HostUnitIdChanged -= RemoveAlienMines;
            _logger.Log($"Removed mine {mine.Identity.netId} of host {_hostId}");
        }

        private void RemoveAllMines()
        {
            IMine[] mines = _plantTimes.Keys.ToArray();
            foreach (IMine mine in mines) Remove(mine);
        }

        private void RemoveAlienMines()
        {
            IMine[] alienMines = _plantTimes
                .Select(kv => kv.Key)
                .Where(IsAlien)
                .ToArray();

            foreach (IMine alienMine in alienMines)
            {
                _logger.Log($"Changed host {_hostId} for {alienMine.State.HostUnitId} " +
                            $"by mine {alienMine.Identity.netId}");

                HostIdChanged?.Invoke(alienMine);
                Remove(alienMine);
            }
        }

        private void DestroyOldMines()
        {
            IMine[] oldMines = _plantTimes
                .Select(kv => kv.Key)
                .Where(IsOld)
                .ToArray();

            foreach (IMine oldMine in oldMines)
            {
                _logger.Log($"Destroyed as old mine {oldMine.Identity.netId} of host {_hostId}");
                oldMine.Destroy();
                Remove(oldMine);
            }
        }

        private bool IsAlien([NotNull] IMine mine) =>
            mine.State.HostUnitId != _hostId;

        private bool IsOld([NotNull] IMine mine) =>
            IsTimeExpired(_plantTimes[mine]);

        private bool IsTimeExpired(float time) =>
            _time.Time - time > ExpiryTime;
    }
}
