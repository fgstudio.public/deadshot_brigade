using System;
using System.Collections.Generic;
using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class OldUnitMineDestroyHandler : IDisposable
    {
        [NotNull] private readonly OldUnitMineDestroyerFactory _destroyerFactory;
        [NotNull] private readonly Dictionary<uint, OldUnitMineDestroyer> _destroyers;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, IMine> _mines;

        public OldUnitMineDestroyHandler(
            [NotNull] OldUnitMineDestroyerFactory destroyerFactory,
            [NotNull] IReadOnlyObjectCollection<uint, IMine> mines)
        {
            _mines = mines;
            _destroyerFactory = destroyerFactory;
            _destroyers = new Dictionary<uint, OldUnitMineDestroyer>();

            Subscribe(mines);
        }

        public void Dispose()
        {
            foreach (OldUnitMineDestroyer destroyer in _destroyers.Values)
                DisposeDestroyer(destroyer);
            _destroyers.Clear();

            Unsubscribe(_mines);
        }

        private void Subscribe([NotNull] IReadOnlyObjectCollection<uint, IMine> mines)
        {
            mines.Added += Add;
            mines.Removed += Remove;
        }

        private void Unsubscribe([NotNull] IReadOnlyObjectCollection<uint, IMine> mines)
        {
            mines.Added -= Add;
            mines.Removed -= Remove;
        }

        private void Add([NotNull] IMine mine)
        {
            uint hostId = mine.State.HostUnitId;

            if (!_destroyers.TryGetValue(hostId, out OldUnitMineDestroyer destroyer))
                destroyer = _destroyers[hostId] = CreateDestroyer(hostId);

            destroyer.Add(mine);
        }

        private void Remove([NotNull] IMine mine)
        {
            if (_destroyers.TryGetValue(mine.State.HostUnitId, out OldUnitMineDestroyer destroyer))
                destroyer.Remove(mine);
        }

        private OldUnitMineDestroyer CreateDestroyer(uint hostId)
        {
            OldUnitMineDestroyer destroyer = _destroyerFactory.Create(hostId);
            destroyer.HostIdChanged += Add;

            return destroyer;
        }

        private void DisposeDestroyer([NotNull] OldUnitMineDestroyer destroyer)
        {
            destroyer.Dispose();
            destroyer.HostIdChanged -= Add;
        }
    }
}