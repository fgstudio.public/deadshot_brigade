using System.Linq;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Обработка серверной логики работы мин
    /// <see cref="https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5"/>
    /// </summary>
    public sealed class MineHandleFacade
    {
        private readonly ObjectCollectionRepository<IMine> _mineCollections;
        private readonly MineFactory _factory;

        public MineHandleFacade(
            [NotNull] ObjectCollectionRepository<IMine> mineCollections,
            [NotNull] MineFactory factory)
        {
            _mineCollections = mineCollections;
            _factory = factory;
        }

        public void CreateMine([NotNull] IMine prefab, [CanBeNull] NetworkIdentity unitIdentity, Vector3 position)
        {
            uint hostUnitId = unitIdentity?.netId ?? default;

            IMine mine = _factory.Create(prefab, position);
            mine.State.HostUnitId = hostUnitId;

            mine.Exploded.AddListener(OnExploded);
            mine.Destroyed.AddListener(OnFinalized);
            mine.ReadyForRelease += OnReadyForRelease;

            NetworkServer.Spawn(mine.GameObject);

            if (mine.State.IsInitialized) AddToCollection(mine);
            else mine.State.Initialized += AddToCollection;

            void OnExploded() => RemoveFromCollection(mine);
            void OnReadyForRelease(IAutoReleased _) => OnFinalized();
            void OnFinalized()
            {
                mine.Exploded.RemoveListener(OnExploded);
                mine.Destroyed.RemoveListener(OnFinalized);
                mine.ReadyForRelease -= OnReadyForRelease;
                mine.State.Initialized -= AddToCollection;

                RemoveFromCollection(mine);
                NetworkServer.UnSpawn(mine.GameObject);
            }
        }

        public void RemoveAllMines()
        {
            List<IMine> mines = _mineCollections.Get<uint>().Values.ToList();
            foreach (IMine mine in mines) mine.Destroy();
        }

        private void AddToCollection(IMine mine) => _mineCollections.Get<uint>().Add(mine.Identity.netId, mine);
        private void RemoveFromCollection(IMine mine) => _mineCollections.Get<uint>().Remove(mine.Identity.netId);
    }
}