using System;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Impact;
using Frostgate.RiftHunters.Core.Network.Shared;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Фабрика мин. Проводит настройку выданных из пула объектов
    /// и инкапсулирует их данные, отдавай объекты по интерфейсу.
    /// <see cref="https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5"/>
    /// </summary>
    public sealed class MineFactory
    {
        private readonly IRandom _random;
        private readonly MinePoolRepository _poolRepository;
        private readonly ImpactStateInitializer _impactStateInitializer;

        public MineFactory(
            [NotNull] IRandom random,
            [NotNull] MinePoolRepository poolRepository,
            [NotNull] ImpactStateInitializer impactStateInitializer)
        {
            _random = random;
            _poolRepository = poolRepository;
            _impactStateInitializer = impactStateInitializer;
        }

        public IMine Create([NotNull] IMine prefab, Vector3 position) =>
            Create(prefab.Identity.assetId, position);

        public IMine Create(Guid assetId, Vector3 position)
        {
            IPool<Mine> pool = _poolRepository[assetId];
            Mine mine = pool.Get();

            Transform mineTransform = mine.transform;
            mineTransform.position = position;
            mineTransform.eulerAngles = mineTransform.eulerAngles.SetY(_random.Range(-360, 360));

            _impactStateInitializer.Init(mine.ImpactState, mine);

            return mine;
        }
    }
}