﻿using Frostgate.RiftHunters.Core.Battle;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class AllyBotsSpawner
    {
        [NotNull] private readonly IBotSpawnFacade _botSpawnFacade;
        [NotNull] private readonly IMissionConfig _missionConfig;
        [NotNull] private readonly IReadOnlyList<MatchPlayer> _matchPlayers;
        [NotNull] private readonly INameGenerator _nameGenerator;

        public AllyBotsSpawner([NotNull] IBotSpawnFacade botSpawnFacade,
            [NotNull] IMissionConfig missionConfig, [NotNull] IReadOnlyList<MatchPlayer> matchPlayers,
            [NotNull] INameGenerator nameGenerator)
        {
            _botSpawnFacade = botSpawnFacade;
            _missionConfig = missionConfig;
            _matchPlayers = matchPlayers;
            _nameGenerator = nameGenerator;
        }

        public void Spawn()
        {
            int botsRequiredCount = _missionConfig.MaxPlayersNumber - _matchPlayers.Count;
            int botsSpawnedCount = 0;

            foreach (UnitConfig botConfig in _missionConfig.AllyBotsRoster.Objects)
            {
                if (IsBotAllowedToSpawn(botConfig))
                {
                    SpawnBot(botConfig);
                    botsSpawnedCount++;
                }

                if (botsSpawnedCount >= botsRequiredCount)
                    break;
            }
        }

        private bool IsBotAllowedToSpawn(UnitConfig config)
        {
            foreach (MatchPlayer player in _matchPlayers)
            {
                if (player.SelectedUnit.Id == config.Id)
                    return false;
            }

            return true;
        }

        private void SpawnBot(UnitConfig config) => _botSpawnFacade.SpawnAllyBot(config, _nameGenerator.Generate());
    }
}