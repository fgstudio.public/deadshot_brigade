﻿using System.Linq;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using Mirror;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public interface IUnitSpawnFacade
    {
        void SpawnUnitForConnection([NotNull] NetworkConnectionToClient conn, [NotNull] IReadOnlyPlayer player,
            [NotNull] UnitConfig unitConfig, [NotNull] Equipment[] equipments, int heroLevel);

        void UnspawnUnitForConnection([NotNull] NetworkConnectionToClient conn);
    }

    public interface IBotSpawnFacade
    {
        UnitNetwork SpawnEnemyBot([NotNull] UnitConfig botConfig, Vector3 position, [NotNull] IPositionProvider homePositionProvider);
        UnitNetwork SpawnAllyBot([NotNull] UnitConfig botConfig, string name);
        void DestroyBot([NotNull] UnitNetwork bot);
    }

    public sealed class UnitSpawnFacade : IUnitSpawnFacade, IBotSpawnFacade
    {
        [NotNull] private readonly UnitFactory _unitFactory;
        [NotNull] private readonly BotFactory _botFactory;
        [NotNull] private readonly ObjectCollectionRepository<UnitNetwork> _unitCollections;

        public UnitSpawnFacade([NotNull] UnitFactory unitFactory, [NotNull] BotFactory botFactory,
            [NotNull] ObjectCollectionRepository<UnitNetwork> unitCollections)
        {
            _unitFactory = unitFactory;
            _botFactory = botFactory;
            _unitCollections = unitCollections;
        }

        #region Player Unit

        public void SpawnUnitForConnection(NetworkConnectionToClient conn, IReadOnlyPlayer player,
            UnitConfig unitConfig,
            Equipment[] equipments, int heroLevel)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(conn, nameof(conn));
            ThrowHelper.ThrowIfArgumentNullOrDefault(unitConfig, nameof(unitConfig));
            ThrowHelper.ThrowIfArgumentNullOrDefault(player, nameof(player));

            UnitNetwork unit = _unitFactory.Create(unitConfig, equipments, player.Id, player.Name, heroLevel);

            NetworkServer.AddPlayerForConnection(conn, unit.gameObject);
            NetworkServer.Spawn(unit.gameObject, conn);

            RegisterUnit(unit);
        }

        public void UnspawnUnitForConnection(NetworkConnectionToClient conn)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(conn, nameof(conn));

            IObjectCollection<uint, UnitNetwork>
                identityCollection = _unitCollections.Get<uint>();
            NetworkIdentity[] unitIdentities =
                conn.clientOwnedObjects.Where(id => identityCollection.Contains(id.netId)).ToArray();

            unitIdentities.ForEach(id => UnregisterUnit(identityCollection[id.netId]));
        }

        #endregion


        #region Server Bot

        public UnitNetwork SpawnEnemyBot(UnitConfig botConfig, Vector3 position, IPositionProvider homePositionProvider)
        {
            UnitNetwork bot = _botFactory.CreateEnemy(botConfig, position, homePositionProvider);
            NetworkServer.Spawn(bot.gameObject);
            RegisterUnit(bot);

            return bot;
        }

        public UnitNetwork SpawnAllyBot(UnitConfig botConfig, string name)
        {
            UnitNetwork bot = _botFactory.CreateAlly(botConfig, name);
            NetworkServer.Spawn(bot.gameObject);
            RegisterUnit(bot);

            return bot;
        }

        public void DestroyBot(UnitNetwork bot)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(bot, nameof(bot));

            UnregisterUnit(bot);
            NetworkServer.Destroy(bot.gameObject);
        }

        #endregion

        private void RegisterUnit(UnitNetwork unit)
        {
            _unitCollections.Get<Collider>().Add(unit.UnitNetworkState.ViewConfig.Body, unit);
            _unitCollections.Get<Collider>().Add(unit.UnitNetworkState.ViewConfig.AimTarget, unit);
            _unitCollections.Get<uint>().Add(unit.Identity.netId, unit);
        }

        private void UnregisterUnit(UnitNetwork unit)
        {
            _unitCollections.Get<Collider>().Remove(unit.UnitNetworkState.ViewConfig.Body);
            _unitCollections.Get<Collider>().Remove(unit.UnitNetworkState.ViewConfig.AimTarget);
            _unitCollections.Get<uint>().Remove(unit.Identity.netId);
        }
    }
}