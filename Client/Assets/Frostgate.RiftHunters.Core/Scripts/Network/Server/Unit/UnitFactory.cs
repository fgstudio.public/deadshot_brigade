using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public class UnitFactory
    {
        [NotNull] private readonly PawnFactory _pawnFactory;
        [NotNull] private readonly UnitPlayerPointCalculator _pointCalculator;

        public UnitFactory([NotNull] PawnFactory pawnFactory, [NotNull] UnitPlayerPointCalculator pointCalculator)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(pointCalculator, nameof(pointCalculator));

            _pawnFactory = pawnFactory;
            _pointCalculator = pointCalculator;
        }

        public UnitNetwork Create(UnitConfig unitConfig, Equipment[] equipments, string playerId, string playerName, int heroLevel)
        {
            PointData pointData = _pointCalculator.CalcForCheckpoint();

            var creationData = new PawnCreationData(unitConfig, equipments, heroLevel)
            {
                Position = pointData.Position,
                Rotation = Quaternion.Euler(pointData.Rotation),
                PlayerId = playerId,
                Name = playerName,
                Type = BattleUnitType.Player,
                HeroLevel = heroLevel
            };

            return _pawnFactory.Create(creationData);
        }
    }
}