using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Server.Impact;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5"/>
    /// </summary>
    public sealed class LinkFacade
    {
        [NotNull] private readonly IPrefabFactory _prefabFactory;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;

        public LinkFacade(
            [NotNull] IPrefabFactory prefabFactory,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _prefabFactory = prefabFactory;
            _unitCollection = unitCollection;
        }

        public void CreateLink([NotNull] ILinkSpawner prefab,
            [CanBeNull] NetworkIdentity targetIdentity, Vector3 position)
        {
            if (targetIdentity == null) return;
            if (!_unitCollection.TryGet(targetIdentity.netId, out UnitNetwork unit)) return;

            var masterLinkSpawner = _prefabFactory.Instantiate<IMasterLinkSpawner>(prefab.GameObject, unit.transform);
            masterLinkSpawner.Finished.AddListener(FinalizeSpawner);
            masterLinkSpawner.StartEffect(unit, masterLinkSpawner);

            void FinalizeSpawner()
            {
                masterLinkSpawner.Finished.RemoveListener(FinalizeSpawner);
                Object.Destroy(masterLinkSpawner.GameObject);
            }
        }
    }
}