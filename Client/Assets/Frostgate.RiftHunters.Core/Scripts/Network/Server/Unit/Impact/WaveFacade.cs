using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Server.Impact;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/38de45f7ac374202872d1c775f0b6aa5"/>
    /// </summary>
    public sealed class WaveFacade
    {
        [NotNull] private readonly IPrefabFactory _prefabFactory;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;

        public WaveFacade(
            [NotNull] IPrefabFactory prefabFactory,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _prefabFactory = prefabFactory;
            _unitCollection = unitCollection;
        }

        public void CreateWave([NotNull] IWaveSpawner prefab,
            [CanBeNull] NetworkIdentity targetIdentity, Vector3 position)
        {
            if (targetIdentity == null) return;
            if (!_unitCollection.TryGet(targetIdentity.netId, out UnitNetwork unit)) return;

            var waveSpawner = _prefabFactory.Instantiate<IWaveSpawner>(prefab.GameObject, unit.transform);
            waveSpawner.Finished.AddListener(FinalizeSpawner);
            waveSpawner.StartEffect(unit);

            void FinalizeSpawner()
            {
                waveSpawner.Finished.RemoveListener(FinalizeSpawner);
                Object.Destroy(waveSpawner.GameObject);
            }
        }
    }
}