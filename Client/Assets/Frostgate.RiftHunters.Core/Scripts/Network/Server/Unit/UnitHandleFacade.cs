using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public class UnitHandleFacade
    {
        public void HandleDead([NotNull] UnitNetwork deadUnit)
        {
            InterruptAllCastingImpacts(deadUnit.ImpactRoster);
            TryExecuteImpactAfterDeath(deadUnit.ImpactRoster.Death);

            deadUnit.UnitNetworkEffects.RemoveAll();
        }

        private void InterruptAllCastingImpacts(IServerImpactModelRoster impactModelRoster)
        {
            foreach (IServerImpactModel impactModel in impactModelRoster.AllImpacts)
                if (impactModel.IsCasting)
                    impactModel.Interrupt();
        }

        private bool TryExecuteImpactAfterDeath(IServerImpactModel deathImpactModel)
        {
            bool canExecute = deathImpactModel.CanExecute;
            if (canExecute) deathImpactModel.Execute();
            return canExecute;
        }
    }
}