using System;
using Frostgate.RiftHunters.Core.Battle.Server.UnitAI;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Server;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public class BotFactory
    {
        [NotNull] private readonly PawnFactory _pawnFactory;
        [NotNull] private readonly UnitAIGroupVision _botGroupVision;
        [NotNull] private readonly UnitAIGroupVision _playerGroupVision;
        [NotNull] private readonly UnitPlayerPointCalculator _pointCalculator;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _networkPlayers;

        public BotFactory([NotNull] PawnFactory pawnFactory, [NotNull] UnitAIGroupVision botGroupVision, [NotNull] UnitAIGroupVision playerGroupVision,
            [NotNull] UnitPlayerPointCalculator pointCalculator,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> networkPlayers)
        {
            ThrowHelper.ThrowIfArgumentNullOrDefault(pawnFactory, nameof(pawnFactory));

            _pawnFactory = pawnFactory;
            _botGroupVision = botGroupVision;
            _playerGroupVision = playerGroupVision;
            _pointCalculator = pointCalculator;
            _networkPlayers = networkPlayers;
        }

        public UnitNetwork CreateEnemy([NotNull] UnitConfig botConfig, Vector3 position, IPositionProvider homePositionProvider)
        {
            var creationData = new PawnCreationData(botConfig, Array.Empty<Equipment>(), 1)
            {
                Position = position,
                Rotation = Quaternion.identity,
                Name = string.Empty,
                Type = BattleUnitType.Bot
            };

            return Create(creationData, homePositionProvider, _botGroupVision);
        }

        public UnitNetwork CreateAlly([NotNull] UnitConfig botConfig, string name)
        {
            PointData pointData = _pointCalculator.CalcForCheckpoint();

            var creationData = new PawnCreationData(botConfig, Array.Empty<Equipment>(), 1)
            {
                Position = pointData.Position,
                Rotation = Quaternion.Euler(pointData.Rotation),
                Name = name,
                Type = BattleUnitType.Player
            };
            var homePosProvider = new ByPlayersPositionProvider(_networkPlayers);

            return Create(creationData, homePosProvider, _playerGroupVision);
        }

        private UnitNetwork Create(PawnCreationData creationData, IPositionProvider homePositionProvider, UnitAIGroupVision unitAIGroupVision)
        {
            UnitNetwork pawn = _pawnFactory.Create(creationData);
            pawn.InitAI(unitAIGroupVision, homePositionProvider);

            return pawn;
        }
    }
}