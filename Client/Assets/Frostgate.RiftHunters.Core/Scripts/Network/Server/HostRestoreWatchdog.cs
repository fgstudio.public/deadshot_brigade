﻿using Mirror;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class HostRestoreWatchdog : IRestoreWatchdog
    {
        public void OnServerConnect(NetworkConnectionToClient client)
        {
        }

        public void OnServerDisconnect(NetworkConnectionToClient client)
        {
        }
    }
}