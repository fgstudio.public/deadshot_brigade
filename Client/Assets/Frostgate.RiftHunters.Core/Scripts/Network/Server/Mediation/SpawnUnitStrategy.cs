﻿using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using Mirror;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class SpawnUnitStrategy : IConnectionInitializationStrategy
    {
        [NotNull] private readonly IUnitSpawnFacade _spawnFacade;

        public SpawnUnitStrategy([NotNull] IUnitSpawnFacade spawnFacade)
        {
            _spawnFacade = spawnFacade;
        }

        public void Execute(NetworkConnectionToClient connection, IReadOnlyPlayer player, UnitConfig unitConfig,
            Equipment[] equipments, int heroLevel) =>
            _spawnFacade.SpawnUnitForConnection(connection, player, unitConfig, equipments, heroLevel);
    }
}