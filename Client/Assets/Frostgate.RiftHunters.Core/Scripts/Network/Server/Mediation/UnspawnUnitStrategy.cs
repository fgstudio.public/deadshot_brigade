﻿using JetBrains.Annotations;
using Mirror;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class UnspawnUnitStrategy : IConnectionDeinitializationStrategy
    {
        [NotNull] private readonly IUnitSpawnFacade _spawnFacade;

        public UnspawnUnitStrategy([NotNull] IUnitSpawnFacade spawnFacade)
        {
            _spawnFacade = spawnFacade;
        }

        public void Execute(NetworkConnectionToClient connection) => _spawnFacade.UnspawnUnitForConnection(connection);
    }
}