using Mirror;
using System;
using System.Threading;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Server.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Energy;
using Frostgate.RiftHunters.Core.Battle.Shared.ShootingTarget;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public class ServerMediator : IDisposable
    {
        private readonly UnitHandleFacade _unitHandleFacade;
        private readonly LootBoxHandleFacade _lootBoxFacade;
        private readonly EnergyCapsuleMediator _energyCapsuleMediator;
        private readonly TrapHandleFacade _trapFacade;
        private readonly MineHandleFacade _mineFacade;
        private readonly TotemHandleFacade _totemFacade;
        private readonly ExpFacade _expFacade;
        private readonly WaveFacade _waveFacade;
        private readonly LinkFacade _linkFacade;
        private readonly IServiceRoster _serviceRoster;
        private readonly ShieldHandleFacade _shieldFacade;
        private readonly BotArenaMediator _botArenaMediator;
        private readonly BotCustomSpawnerMediator _customSpawnerMediator;
        private readonly StreakBonusMediator _bonusMediator;
        private readonly PlayersDieObserver _playersDieObserver;
        private readonly IReadOnlyObjectCommandReceiverRepository _commandReceiverRepository;
        private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;
        private readonly BarrelFacade _barrelFacade;
        private readonly IConnectionInitializationStrategy _connInitStrategy;
        private readonly IConnectionDeinitializationStrategy _connDeinitStrategy;

        [CanBeNull] private CancellationTokenSource _cts;
        private bool _isDisposed;

        public ServerMediator(UnitHandleFacade unitHandleFacade, LootBoxHandleFacade lootBoxFacade,
            EnergyCapsuleMediator energyCapsuleMediator,
            ExpFacade expFacade, WaveFacade waveFacade, LinkFacade linkFacade,
            TrapHandleFacade trapFacade, MineHandleFacade mineFacade,
            TotemHandleFacade totemFacade, ShieldHandleFacade shieldFacade,
            IServiceRoster serviceRoster, BotArenaMediator botArenaMediator,
            BotCustomSpawnerMediator customSpawnerMediator, StreakBonusMediator bonusMediator,
            PlayersDieObserver playersDieObserver, IReadOnlyObjectCommandReceiverRepository commandReceiverRepository,
            IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection, BarrelFacade barrelFacade,
            IConnectionInitializationStrategy connInitStrategy,
            IConnectionDeinitializationStrategy connDeinitStrategy)
        {
            _unitHandleFacade = unitHandleFacade;
            _lootBoxFacade = lootBoxFacade;
            _energyCapsuleMediator = energyCapsuleMediator;
            _trapFacade = trapFacade;
            _mineFacade = mineFacade;
            _totemFacade = totemFacade;
            _expFacade = expFacade;
            _waveFacade = waveFacade;
            _linkFacade = linkFacade;
            _shieldFacade = shieldFacade;
            _botArenaMediator = botArenaMediator;
            _customSpawnerMediator = customSpawnerMediator;
            _bonusMediator = bonusMediator;
            _serviceRoster = serviceRoster;
            _commandReceiverRepository = commandReceiverRepository;
            _unitCollection = unitCollection;
            _playersDieObserver = playersDieObserver;
            _barrelFacade = barrelFacade;
            _connInitStrategy = connInitStrategy;
            _connDeinitStrategy = connDeinitStrategy;

            Subscribe();
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            _barrelFacade.Dispose();
            CancelCts(ref _cts);
            Unsubscribe();

            _isDisposed = true;
        }

        public void InitializeConnection(NetworkConnectionToClient connection, UnitConfig unitConfig,
            Equipment[] equipments, IReadOnlyPlayer player, int heroLevel) =>
            _connInitStrategy.Execute(connection, player, unitConfig, equipments, heroLevel);

        public void DeinitializeConnection(NetworkConnectionToClient connection) =>
            _connDeinitStrategy.Execute(connection);

        private void Subscribe()
        {
            _playersDieObserver.AllPlayersDied += OnAllPlayersDied;

            _serviceRoster.DamageService.Died.AddListener(OnDied);

            _commandReceiverRepository.Get<ITrap>().InstantiateForUnitReceived += _trapFacade.CreateTrap;
            _commandReceiverRepository.Get<IMine>().InstantiateForUnitReceived += _mineFacade.CreateMine;
            _commandReceiverRepository.Get<ITotem>().InstantiateForUnitReceived += _totemFacade.CreateTotem;
            _commandReceiverRepository.Get<IWaveSpawner>().InstantiateForUnitReceived += _waveFacade.CreateWave;
            _commandReceiverRepository.Get<ILinkSpawner>().InstantiateForUnitReceived += _linkFacade.CreateLink;
            _commandReceiverRepository.Get<IShield>().InstantiateForUnitReceived += _shieldFacade.CreateShield;

            _commandReceiverRepository.Get<BotSpawnerConfig>().SpawnOnNearestSpawnerReceived += _customSpawnerMediator.SpawnOnNearestArea;
        }

        private void Unsubscribe()
        {
            _playersDieObserver.AllPlayersDied -= OnAllPlayersDied;

            // changing order may harm stability
            _serviceRoster.DamageService.Died.RemoveListener(OnDied);

            _commandReceiverRepository.Get<ITrap>().InstantiateForUnitReceived -= _trapFacade.CreateTrap;
            _commandReceiverRepository.Get<IMine>().InstantiateForUnitReceived -= _mineFacade.CreateMine;
            _commandReceiverRepository.Get<ITotem>().InstantiateForUnitReceived -= _totemFacade.CreateTotem;
            _commandReceiverRepository.Get<IWaveSpawner>().InstantiateForUnitReceived -= _waveFacade.CreateWave;
            _commandReceiverRepository.Get<ILinkSpawner>().InstantiateForUnitReceived -= _linkFacade.CreateLink;
            _commandReceiverRepository.Get<IShield>().InstantiateForUnitReceived -= _shieldFacade.CreateShield;

            _commandReceiverRepository.Get<BotSpawnerConfig>().SpawnOnNearestSpawnerReceived -= _customSpawnerMediator.SpawnOnNearestArea;
        }

        // TODO: подвязать на рестарт арены, а не смерть всех игроков
        private void OnAllPlayersDied()
        {
            CancelCts(ref _cts);
            _cts = new CancellationTokenSource();

            // TODO: хот-фикс проблем респауна
            UniTask.Delay(100, cancellationToken: _cts.Token)
                .ContinueWith(() =>
                {
                    _mineFacade.RemoveAllMines();
                    _lootBoxFacade.RemoveAllLootBoxes();
                    _energyCapsuleMediator.RemoveAllCapsules();
                    _totemFacade.RemoveAllTotems();
                    _bonusMediator.RemoveAllBonuses();
                    _barrelFacade.ResetBarrels();
                    _customSpawnerMediator.StopAllSpawners();

                    if (_botArenaMediator.CanRestartCurrentArena)
                        _botArenaMediator.RestartCurrentArena();
                })
                .AsAsyncUnitUniTask();
        }

        private void OnDied(DiedEventData eventData)
        {
            if (_unitCollection.TryGet(eventData.DiedId, out UnitNetwork diedUnit) && diedUnit != null)
            {
                _unitHandleFacade.HandleDead(diedUnit);
                _lootBoxFacade.CreateLootBoxForDeadUnit(diedUnit);

                if (_unitCollection.TryGet(eventData.KillerId, out UnitNetwork killerUnit) && killerUnit != null)
                    _expFacade.HandleKill(killerUnit, diedUnit);
            }

            if (NetworkServer.spawned.TryGetValue(eventData.DiedId, out NetworkIdentity diedIdentity) &&
                diedIdentity.TryGetComponent(out IEnergyCapsuleSource capsuleSource))
                _energyCapsuleMediator.CreateCapsule(capsuleSource);
        }

        private void CancelCts(ref CancellationTokenSource cts)
        {
            if (cts != null)
            {
                cts.Cancel();
                cts.Dispose();
                cts = null;
            }
        }
    }
}