﻿using JetBrains.Annotations;
using Mirror;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public interface IConnectionDeinitializationStrategy
    {
        void Execute([NotNull] NetworkConnectionToClient connection);
    }
}