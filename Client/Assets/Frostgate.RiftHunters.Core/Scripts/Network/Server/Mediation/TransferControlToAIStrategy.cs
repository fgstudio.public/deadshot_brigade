﻿using Frostgate.RiftHunters.Core.Battle.Server;
using Frostgate.RiftHunters.Core.Battle.Server.UnitAI;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using JetBrains.Annotations;
using Mirror;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class TransferControlToAIStrategy : IConnectionDeinitializationStrategy
    {
        [NotNull] private readonly UnitAIGroupVision _playerGroupVision;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _units;
        [NotNull] private readonly ByPlayersPositionProvider _aiPosProvider;

        public TransferControlToAIStrategy([NotNull] UnitAIGroupVision playerGroupVision,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> units)
        {
            _playerGroupVision = playerGroupVision;
            _units = units;
            _aiPosProvider = new ByPlayersPositionProvider(units);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("ReSharper", "Unity.NoNullPropagation")]
        public void Execute(NetworkConnectionToClient connection)
        {
            UnitNetwork unit = FindUnit(connection);

            unit?.InitAI(_playerGroupVision, _aiPosProvider);
        }

        private UnitNetwork FindUnit(NetworkConnectionToClient connection)
        {
            // На всякий случай TryGet, если юнит ещё не появился в коллекции
            _units.TryGet(connection.identity.netId, out UnitNetwork unit);

            return unit;
        }
    }
}