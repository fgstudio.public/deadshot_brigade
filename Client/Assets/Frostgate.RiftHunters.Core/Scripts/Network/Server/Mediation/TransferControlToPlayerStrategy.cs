﻿using System.Linq;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using Mirror;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class TransferControlToPlayerStrategy : IConnectionInitializationStrategy
    {
        [NotNull] private readonly IObjectCollection<uint, UnitNetwork> _units;
        [NotNull] private readonly IConnectionInitializationStrategy _fallbackStrategy;

        public TransferControlToPlayerStrategy([NotNull] IObjectCollection<uint, UnitNetwork> units,
            [NotNull] IConnectionInitializationStrategy fallbackStrategy)
        {
            _units = units;
            _fallbackStrategy = fallbackStrategy;
        }

        public void Execute(NetworkConnectionToClient connection, IReadOnlyPlayer player, UnitConfig unitConfig,
            Equipment[] equipments, int heroLevel)
        {
            UnitNetwork unit = FindUnit(player);

            if (unit != null)
            {
                unit.DeinitAI();

                NetworkServer.AddPlayerForConnection(connection, unit.gameObject);
                NetworkServer.Spawn(unit.gameObject, connection);
            }
            else
            {
                _fallbackStrategy.Execute(connection, player, unitConfig, equipments, heroLevel);
            }
        }

        private UnitNetwork FindUnit(IReadOnlyPlayer player) =>
            _units.FirstOrDefault(u => u.UnitNetworkState.PlayerId == player.Id);
    }
}