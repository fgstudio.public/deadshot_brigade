﻿using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Meta.Economy.Inventory;
using JetBrains.Annotations;
using Mirror;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public interface IConnectionInitializationStrategy
    {
        void Execute([NotNull] NetworkConnectionToClient connection, [NotNull] IReadOnlyPlayer player,
            [NotNull] UnitConfig unitConfig, [NotNull] Equipment[] equipments, int heroLevel);
    }
}