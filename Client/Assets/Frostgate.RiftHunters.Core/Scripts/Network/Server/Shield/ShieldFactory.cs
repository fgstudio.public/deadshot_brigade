using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class ShieldFactory
    {
        [NotNull] private readonly IPrefabFactory _prefabFactory;

        public ShieldFactory([NotNull] IPrefabFactory prefabFactory) =>
            _prefabFactory = prefabFactory;

        public IShield Create([NotNull] GameObject prefab, [NotNull] UnitNetwork host, Vector3 position)
        {
            Shield shield = _prefabFactory
                .Instantiate<Shield>(prefab, position, Quaternion.identity);

            shield.ServerInit(host.UnitType);

            return shield;
        }
    }
}
