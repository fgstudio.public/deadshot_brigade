using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class ShieldHandleFacade
    {
        [NotNull] private readonly ShieldFactory _factory;
        [NotNull] private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;

        [NotNull] private readonly Dictionary<IShield, HostedShieldHandler> _handlers = new();

        public ShieldHandleFacade(
            [NotNull] ShieldFactory factory,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _factory = factory;
            _unitCollection = unitCollection;
        }

        public void CreateShield([NotNull] IShield shieldPrefab, [NotNull] NetworkIdentity host, Vector3 position)
        {
            if (_unitCollection.TryGet(host.netId, out UnitNetwork unitHost))
            {
                IShield shield = _factory.Create(shieldPrefab.GameObject!, unitHost, position);

                var handler = new HostedShieldHandler(shield, unitHost);
                handler.LifecycleFinished += RemoveShield;

                _handlers[shield] = handler;
                NetworkServer.Spawn(shield.GameObject);
            }
        }

        public void RemoveShield([NotNull] IShield shield)
        {
            HostedShieldHandler handler = _handlers[shield];
            handler.LifecycleFinished -= RemoveShield;
            handler.Dispose();

            _handlers.Remove(shield);
            NetworkServer.Destroy(shield.GameObject);
        }
    }
}