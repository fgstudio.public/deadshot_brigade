using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Impact;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class HostedShieldHandler : IDisposable
    {
        public event Action<IShield> LifecycleFinished;

        [CanBeNull] private readonly UnitNetwork _host;
        [NotNull] private readonly IShield _shield;
        [NotNull] private readonly CancellationTokenSource _cts;

        private bool _isLifecycleFinished;

        public HostedShieldHandler([NotNull] IShield shield, [NotNull] UnitNetwork host)
        {
            _shield = shield;
            _host = host;
            _cts = new CancellationTokenSource();

            SubscribeHost(host);

            UniTask.Delay(shield.LifeTime, cancellationToken: _cts.Token)
                .ContinueWith(OnShieldLifeCycleFinished)
                .AsAsyncUnitUniTask();
        }

        public void Dispose()
        {
            if (_host != null)
                UnsubscribeHost(_host);

            InterruptShieldLifecycle();
            _cts.Dispose();
        }

        private void SubscribeHost([NotNull] UnitNetwork host)
        {
            host.Destroyed.AddListener(InterruptShieldLifecycle);
            host.UnitNetworkState.HealthChanged += OnHostHealthChanged;
        }

        private void UnsubscribeHost([NotNull] UnitNetwork host)
        {
            host.Destroyed.RemoveListener(InterruptShieldLifecycle);
            host.UnitNetworkState.HealthChanged -= OnHostHealthChanged;
        }

        private void InterruptShieldLifecycle()
        {
            _cts.Cancel();

            if (!_isLifecycleFinished)
                OnShieldLifeCycleFinished();
        }

        private void OnShieldLifeCycleFinished()
        {
            _isLifecycleFinished = true;
            LifecycleFinished?.Invoke(_shield);
        }

        private void OnHostHealthChanged(float diff)
        {
            if (_host != null && _host.UnitNetworkState.IsDead)
                InterruptShieldLifecycle();
        }
    }
}
