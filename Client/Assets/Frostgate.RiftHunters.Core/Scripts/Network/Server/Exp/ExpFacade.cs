using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Exp;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Обработка серверной логики получения опыта
    /// <see cref="https://www.notion.so/frostgate/8b6ab79b447546ee927012827976abc6#f6918c33089f4e14b33e639b0ad8b2c0"/>
    /// </summary>
    public sealed class ExpFacade
    {
        private readonly IReceiveExpService _receiveExpService;

        public ExpFacade(IReceiveExpService receiveExpService)
        {
            _receiveExpService = receiveExpService;
        }

        public void HandleKill([NotNull] UnitNetwork expReceiver, [NotNull] UnitNetwork expSource)
        {
            if (expReceiver.UnitType == BattleUnitType.Player)
                _receiveExpService.Receive(expReceiver, expSource);
        }
    }
}