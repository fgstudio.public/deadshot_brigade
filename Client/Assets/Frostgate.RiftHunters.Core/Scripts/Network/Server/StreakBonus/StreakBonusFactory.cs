using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;
using Frostgate.RiftHunters.Core.Battle.Server.Objects.StreakBonus;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.StreakBonus;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// <see cref="https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0"/>
    /// </summary>
    public sealed class StreakBonusFactory
    {
        private readonly AreaRandom _areaRandom;
        private readonly IBonusService _bonusService;
        private readonly IPrefabFactory _prefabFactory;
        private readonly StreakBonusCoreObject _corePrefab;
        private readonly StreakBonusMechanicSettings _mechanicSettings;
        private readonly ServerModulePrefabRepository _prefabRepository;

        public StreakBonusFactory(
            [NotNull] AreaRandom areaRandom,
            [NotNull] IBonusService bonusService,
            [NotNull] IPrefabFactory prefabFactory,
            [NotNull] StreakBonusCoreObject corePrefab,
            [NotNull] StreakBonusMechanicSettings mechanicSettings,
            [NotNull] ServerModulePrefabRepository prefabRepository)
        {
            _areaRandom = areaRandom;
            _corePrefab = corePrefab;
            _bonusService = bonusService;
            _prefabFactory = prefabFactory;
            _mechanicSettings = mechanicSettings;
            _prefabRepository = prefabRepository;
        }

        public ISharedStreakBonus Create(
            [NotNull] StreakBonusConfig bonusConfig, [NotNull] NetworkIdentity source)
        {
            // Position Generation
            float dropRadius = _mechanicSettings.DropRadius;
            Vector3 bonusPosition = GeneratePosition(source.transform.position, dropRadius);

            // Instantiation
            var coreObject = _prefabFactory.Instantiate<StreakBonusCoreObject>(_corePrefab);
            coreObject.transform.position = bonusPosition;

            // State Initialization
            IEditableStreakBonusState state = coreObject.State;
            state.Init(source.netId, bonusConfig.Id);

            // Server Module Initialization
            StreakBonusServerModule modulePrefab = _prefabRepository[bonusConfig];
            var module = _prefabFactory.Instantiate<StreakBonusServerModule>(modulePrefab, coreObject.transform);
            module.Init(state, bonusConfig, _bonusService, _mechanicSettings);

            return coreObject;
        }

        private Vector3 GeneratePosition(Vector3 basePosition, float dropRadius)
        {
            const int maxTries = 10;
            int triesCount = 0;

            Vector3 position = basePosition;
            while (triesCount++ < maxTries)
            {
                position = _areaRandom.SphereY(basePosition, dropRadius);
                if (PositionHelper.IsValidToSpawn(position))
                    return position;
            }

            return position;
        }
    }
}