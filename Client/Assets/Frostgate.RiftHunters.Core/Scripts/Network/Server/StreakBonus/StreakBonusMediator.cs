using System;
using System.Linq;
using System.Threading;
using Cysharp.Threading.Tasks;
using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Обработка серверной логики работы бонусов
    /// <see cref="https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0"/>
    /// </summary>
    public sealed class StreakBonusMediator : IDisposable
    {
        private readonly ILogger _logger;
        private readonly StreakBonusFactory _factory;
        private readonly IObjectCollection<uint, ISharedStreakBonus> _bonusCollection;

        private readonly CancellationTokenSource _cts = new();

        public StreakBonusMediator(
            [NotNull] StreakBonusFactory factory,
            [NotNull] ILogger<StreakBonusMediator> logger,
            [NotNull] IObjectCollection<uint, ISharedStreakBonus> bonusCollection)
        {
            _logger = logger;
            _factory = factory;
            _bonusCollection = bonusCollection;
        }

        public void Dispose()
        {
            _cts.Cancel();
            _cts.Dispose();
            RemoveAllBonuses();
        }

        public void CreateBonus(
            [NotNull] StreakBonusConfig bonusConfig, [NotNull] NetworkIdentity source)
        {
            ISharedStreakBonus bonus = _factory.Create(bonusConfig, source);
            IReadOnlyStreakBonusState state = bonus.State;

            NetworkServer.Spawn(bonus.GameObject); // инициализация netId
            _bonusCollection.Add(bonus.Identity.netId, bonus);

            state.StatusChanged.AddListener(OnStatusChanged);
            OnStatusChanged(state);

            _logger.Log($"Create bonus [netId:{bonus.Identity.netId}] " +
                        $"with [configId:{bonusConfig.Id}] " +
                        $"from source [netId:{source.netId}]");
        }

        public void RemoveAllBonuses() =>
            _bonusCollection.Values.ToArray().ForEach(RemoveBonus);

        private void RemoveBonus([CanBeNull] ISharedStreakBonus bonus)
        {
            if (bonus?.GameObject == null) return;

            uint netId = bonus.Identity.netId;
            bonus.State.StatusChanged.RemoveListener(OnStatusChanged);

            _bonusCollection.Remove(bonus.Identity.netId);
            NetworkServer.Destroy(bonus.GameObject);

            _logger.Log($"Remove bonus [netId:{netId}]");
        }

        private void OnStatusChanged(IReadOnlyStreakBonusState state)
        {
            switch (state.Status)
            {
                case StreakBonusStatus.Collected:
                    TimeSpan syncVarCompensation = TimeSpan.FromSeconds(5);
                    UniTask.Delay(syncVarCompensation, cancellationToken: _cts.Token)
                        .ContinueWith(() => RemoveBonus(state.CoreObject))
                        .SuppressCancellationThrow();
                    break;

                case StreakBonusStatus.Expired:
                    RemoveBonus(state.CoreObject);
                    break;
            }
        }
    }
}