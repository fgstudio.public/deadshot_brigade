using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Shared.Objects.StreakBonus;
using Frostgate.RiftHunters.Core.Battle.Server.Objects.StreakBonus;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    [HelpURL("https://www.notion.so/frostgate/83a8edd6805249aaa63c321f161f72b0")]
    [CreateAssetMenu(fileName = nameof(StreakBonusServerInstaller),
        menuName = StreakBonusAssetMenu.Server + "/" + nameof(StreakBonusServerInstaller))]
    public sealed class StreakBonusServerInstaller : ScriptableObjectInstaller
    {
        [Required, AssetSelector]
        [SerializeField] private ServerModulePrefabRepository _modulePrefabRepository;

        public override void InstallBindings()
        {
            Container.Bind<ServerModulePrefabRepository>().
                FromInstance(_modulePrefabRepository).AsSingle();

            Container.Bind<StreakBonusFactory>().AsSingle();
            Container.Bind<StreakBonusMediator>().AsSingle();
        }
    }
}
