using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Компонент для выбора случайного лут-бокса для генерации из весового списка
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class LootBoxChancePicker
    {
        private readonly IRandom _random;

        public LootBoxChancePicker(IRandom random) =>
            _random = random;

        [NotNull, ItemNotNull]
        public IEnumerable<LootBoxConfig> Pick([NotNull] LootSetConfig setConfig)
        {
            bool hasChanceToDrop = GenerateSetDropChance() > setConfig.DropChance;
            if (hasChanceToDrop)
                yield break;

            for (int i = 0; i < setConfig.Amount; i++)
            {
                LootBoxConfig boxConfig = Pick(setConfig.LootBoxes);
                if (boxConfig != null)
                    yield return boxConfig;
            }
        }

        [CanBeNull]
        private LootBoxConfig Pick([NotNull] IReadOnlyList<LootBoxDrop> lootBoxDrops) =>
            _random.WeightedList
            (
                list: lootBoxDrops,
                weight: GenerateLootBoxPickChance(),
                minWeight: LootBoxDrop.MinChance,
                weightGetter: c => c.DropChance
            )?.Config;

        private float GenerateSetDropChance() =>
            _random.Range(LootSetConfig.MinChance, LootSetConfig.MaxChance);

        private float GenerateLootBoxPickChance() =>
            _random.Range(LootBoxDrop.MinChance, LootBoxDrop.MaxChance);
    }
}
