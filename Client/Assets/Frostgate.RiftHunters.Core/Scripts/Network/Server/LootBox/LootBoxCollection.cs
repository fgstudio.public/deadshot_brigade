using Mirror;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle.Shared;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// КО: Коллекция лут-боксов.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class LootBoxCollection
    {
        private readonly Dictionary<NetworkIdentity, IServerLootBox> _lootBoxes = new();

        public IEnumerable<NetworkIdentity> Keys => _lootBoxes.Keys;
        public IEnumerable<IServerLootBox> Values => _lootBoxes.Values;

        public void Add([NotNull] IServerLootBox lootBox) =>
            _lootBoxes[lootBox.Identity] = lootBox;

        public void Remove([NotNull] IServerLootBox lootBox) =>
            Remove(lootBox.Identity);

        public void Remove([NotNull] NetworkIdentity identity) =>
            _lootBoxes.Remove(identity);

        public bool TryGet([NotNull] NetworkIdentity identity, out IServerLootBox lootBox) =>
            _lootBoxes.TryGetValue(identity, out lootBox);
    }
}