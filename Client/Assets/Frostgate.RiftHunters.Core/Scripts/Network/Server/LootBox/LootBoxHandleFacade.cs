using System.Linq;
using System.Collections.Generic;
using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Looting;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Обработка серверной логики работы лут-боксов
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class LootBoxHandleFacade
    {
        private readonly LootBoxCollection _lootBoxCollection;

        private readonly LootBoxFactory _factory;
        private readonly LootBoxChancePicker _chancePicker;
        private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _units;

        // TODO: враппер над коллекцией юнитов для накопления только игроков
        private readonly HashSet<ILootBoxCollector> _playersCollectors = new();

        public LootBoxHandleFacade(
            [NotNull] LootBoxFactory factory,
            [NotNull] LootBoxChancePicker chancePicker,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> units)
        {
            _factory = factory;
            _chancePicker = chancePicker;
            _units = units;
            _lootBoxCollection = new LootBoxCollection();

            _units.Added += OnCollectorAdded;
            _units.Removed += OnCollectorRemoved;
            _units.Values.ForEach(OnCollectorAdded);
        }

        private void OnCollectorAdded(UnitNetwork unit)
        {
            if (unit.UnitNetworkState.UnitType != BattleUnitType.Player)
                return;

            _playersCollectors.Add(unit);
        }

        private void OnCollectorRemoved(UnitNetwork unit)
        {
            if (unit.UnitNetworkState.UnitType != BattleUnitType.Player)
                return;

            if (_playersCollectors.Contains(unit))
                _playersCollectors.Remove(unit);
        }

        public void CreateLootBoxForDeadUnit(UnitNetwork deadUnit)
        {
            foreach (var lootBoxCollector in _playersCollectors)
            {
                IEnumerable<IServerLootBox> lootBoxes = CreateLootBoxes(deadUnit, lootBoxCollector);
                foreach (IServerLootBox lootBox in lootBoxes)
                    SetupCreatedLootBox(lootBox);
            }
        }

        public void RemoveAllLootBoxes()
        {
            List<IServerLootBox> lootBoxes = _lootBoxCollection.Values.ToList();
            foreach (IServerLootBox lootBox in lootBoxes) lootBox.Destroy();
        }

        private IEnumerable<IServerLootBox> CreateLootBoxes(ILootBoxSource lootBoxSource, ILootBoxCollector targetCollector)
        {
            return _chancePicker.Pick(lootBoxSource.LootSetForDeath)
                .Select(lootBoxConfig => _factory.Create(lootBoxConfig, lootBoxSource, targetCollector));
        }

        private void SetupCreatedLootBox(IServerLootBox lootBox)
        {
            SubscribeOnLootBox(lootBox);
            _lootBoxCollection.Add(lootBox);

            NetworkServer.Spawn(lootBox.GameObject);
        }

        private void SubscribeOnLootBox(IServerLootBox lootBox)
        {
            lootBox.Collected += OnCollected;
            lootBox.ReadyForRelease += OnReadyForRelease;
        }

        private void UnsubscribeFromLootBox(IServerLootBox lootBox)
        {
            lootBox.Collected -= OnCollected;
            lootBox.ReadyForRelease -= OnReadyForRelease;
        }

        private void OnCollected(IServerLootBox lootBox) =>
            _lootBoxCollection.Remove(lootBox);

        private void OnReadyForRelease(IAutoReleased autoReleased)
        {
            if (autoReleased is IServerLootBox lootBox)
            {
                UnsubscribeFromLootBox(lootBox);
                _lootBoxCollection.Remove(lootBox);
                NetworkServer.UnSpawn(lootBox.GameObject);
            }
        }
    }
}