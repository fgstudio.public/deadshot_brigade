using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Network.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Helpers;
using Frostgate.RiftHunters.Core.Battle.Shared.Looting;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Фабрика лут-боксов. Проводит настройку выданных из пула объектов
    /// и инкапсулирует их данные, отдавай объекты по интерфейсу.
    /// <see cref="https://www.notion.so/frostgate/19543b2fc0344775b163382f9f73f53b"/>
    /// </summary>
    public sealed class LootBoxFactory
    {
        private readonly AreaRandom _areaRandom;
        private readonly IReadOnlyLootBoxPoolRepository _poolRepository;

        public LootBoxFactory(
            [NotNull] AreaRandom areaRandom,
            [NotNull] IReadOnlyLootBoxPoolRepository poolRepository)
        {
            _areaRandom = areaRandom;
            _poolRepository = poolRepository;
        }

        public IServerLootBox Create(
            [NotNull] LootBoxConfig lootBoxConfig,
            [NotNull] ILootBoxSource lootBoxHost,
            [NotNull] ILootBoxCollector targetClient)
        {
            Vector3 hostPosition = lootBoxHost.Transform.position.SetY(default);
            Vector3 boxPosition = GeneratePosition(hostPosition, lootBoxConfig.DropRadius);

            LootBox lootBox = _poolRepository[lootBoxConfig.Type].Get();

            // здесь можно передать список наград в объект
            lootBox.State.Config = lootBoxConfig;
            lootBox.transform.position = boxPosition;
            lootBox.P2PObject.SetData(lootBoxHost.Identity.netId, targetClient.Identity.netId);

            return lootBox;
        }

        private Vector3 GeneratePosition(Vector3 hostPosition, float dropRadius)
        {
            const int maxTries = 10;
            int triesCount = 0;

            Vector3 boxPosition = hostPosition;
            while (triesCount++ < maxTries)
            {
                boxPosition = _areaRandom.SphereY(hostPosition, dropRadius);
                if (PositionHelper.IsValidToSpawn(boxPosition))
                    return boxPosition;
            }

            return boxPosition;
        }
    }
}