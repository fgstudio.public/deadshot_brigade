using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Respawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;

namespace Frostgate.RiftHunters.Core.Network.Server.Retraining
{
    public sealed class TrapHandler
    {
        public event Action<ITrap> HandlingStarted;
        public event Action<ITrap> HandlingStopped;

        private readonly ITrap _trap;
        private readonly ILogger _logger;
        private readonly IRespawnUnitService _respawnUnitService;
        private readonly ICaptureUnitService _captureUnitService;
        private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;

        [CanBeNull] private UnitNetwork _unit;

        public TrapHandler(
            [NotNull] IRespawnUnitService respawnUnitService,
            [NotNull] ICaptureUnitService captureUnitService,
            [NotNull] ILogger<TrapHandler> logger, [NotNull] ITrap trap,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _trap = trap;
            _logger = logger;
            _unitCollection = unitCollection;
            _respawnUnitService = respawnUnitService;
            _captureUnitService = captureUnitService;
        }

        public void StartHandling([NotNull] UnitNetwork unit)
        {
            uint unitNetId = unit.Identity.netId;
            _trap.State.CapturedUnitId = unitNetId;
            _unit = unit;

            CaptureUnit(unitNetId);
            SubscribeTrap(_trap);
            SubscribeUnit(unit);

            HandlingStarted?.Invoke(_trap);
        }

        public void StopHandling()
        {
            UnsubscribeTrap(_trap);
            if (_unit != null) UnsubscribeUnit(_unit);

            if (_trap.State.CapturedUnitId != default)
                ReleaseUnit(_trap.State.CapturedUnitId);

            _trap.State.CapturedUnitId = default;

            HandlingStopped?.Invoke(_trap);
        }

        private void SubscribeTrap([NotNull] ITrap trap)
        {
            trap.ReadyForRelease += OnReadyForRelease;
            trap.Destroyed.AddListener(StopHandling);
            trap.State.Died += ReleaseTrap;
            trap.State.UnitLost += ReleaseUnit;
            trap.State.UnitCaptured += CaptureUnit;
        }

        private void UnsubscribeTrap([NotNull] ITrap trap)
        {
            trap.ReadyForRelease -= OnReadyForRelease;
            trap.Destroyed.RemoveListener(StopHandling);
            trap.State.Died -= ReleaseTrap;
            trap.State.UnitLost -= ReleaseUnit;
            trap.State.UnitCaptured -= CaptureUnit;
        }

        private void SubscribeUnit([NotNull] UnitNetwork unit)
        {
            unit.Destroyed.AddListener(KillTrap);
            _respawnUnitService.Respawned.AddListener(OnUnitRespawned);
        }

        private void UnsubscribeUnit([NotNull] UnitNetwork unit)
        {
            unit.Destroyed.RemoveListener(KillTrap);
            _respawnUnitService.Respawned.RemoveListener(OnUnitRespawned);
        }

        private void OnReadyForRelease(IAutoReleased _) => StopHandling();
        private void OnUnitRespawned(uint respawnableId)
        {
            if (respawnableId == _trap.State.CapturedUnitId)
                KillTrap();
        }

        private void KillTrap() => _trap.State.Health = default;
        private void ReleaseTrap() => _trap.State.CapturedUnitId = default;

        private void ReleaseUnit(uint netId)
        {
            bool found = _unitCollection.TryGet(netId, out UnitNetwork unit);
            if (found) _captureUnitService.Release(unit);

            if (found) _logger.Log($"Release Unit with netId {netId}");
            else _logger.LogWarning($"Can't find {nameof(UnitNetwork)} with netId {netId}");
        }

        private void CaptureUnit(uint netId)
        {
            bool found = _unitCollection.TryGet(netId, out UnitNetwork unit);
            if (found) _captureUnitService.Capture(unit);

            if (found) _logger.Log($"Capture Unit with netId {netId}");
            else _logger.LogWarning($"Can't find {nameof(UnitNetwork)} with netId {netId}");
        }
    }
}