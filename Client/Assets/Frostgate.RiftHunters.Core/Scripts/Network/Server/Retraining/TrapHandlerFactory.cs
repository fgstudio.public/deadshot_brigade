using Mirror;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;

namespace Frostgate.RiftHunters.Core.Network.Server.Retraining
{
    public sealed class TrapHandlerFactory
    {
        private readonly ILogger<TrapHandler> _logger;
        private readonly IServiceRoster _serviceRoster;
        private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _unitCollection;

        public TrapHandlerFactory(
            [NotNull] ILogger<TrapHandler> logger, [NotNull] IServiceRoster serviceRoster,
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> unitCollection)
        {
            _logger = logger;
            _serviceRoster = serviceRoster;
            _unitCollection = unitCollection;
        }

        public TrapHandler Create([NotNull] ITrap trap) =>
            new(_serviceRoster.RespawnUnitService, _serviceRoster.CaptureUnitService,
                _logger, trap, _unitCollection);
    }
}