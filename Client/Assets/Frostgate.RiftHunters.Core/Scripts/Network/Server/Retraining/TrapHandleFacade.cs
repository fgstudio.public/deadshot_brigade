using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Capturing;
using Frostgate.RiftHunters.Core.Network.Server.Retraining;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Обработка серверной логики работы ловушки
    /// <see cref="https://www.notion.so/frostgate/c8c4c349ab554487b1ea1c50622a6e80"/>
    /// </summary>
    public sealed class TrapHandleFacade
    {
        private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _units;
        private readonly ObjectCollectionRepository<ITrap> _trapCollections;
        private readonly TrapHandlerFactory _handlerFactory;
        private readonly TrapFactory _factory;

        public TrapHandleFacade(
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> units,
            [NotNull] ObjectCollectionRepository<ITrap> trapCollections,
            [NotNull] TrapHandlerFactory handlerFactory, [NotNull] TrapFactory factory)
        {
            _units = units;
            _trapCollections = trapCollections;
            _handlerFactory = handlerFactory;
            _factory = factory;
        }

        public void CreateTrap([NotNull] ITrap prefab, [CanBeNull] NetworkIdentity unitIdentity, Vector3 position)
        {
            ITrap trap = _factory.Create(prefab, position);

            var handler = _handlerFactory.Create(trap);
            handler.HandlingStarted += OnHandlingStarted;
            handler.HandlingStopped += OnHandlingStopped;
            // TODO: тут может быть утечка, т.к. нет отписки

            if (unitIdentity != null && _units.TryGet(unitIdentity.netId, out UnitNetwork unit))
                handler.StartHandling(unit);
        }

        private void OnHandlingStarted(ITrap trap)
        {
            NetworkServer.Spawn(trap.GameObject);

            if (trap.State.IsInitialized) AddToCollections(trap);
            else trap.State.Initialized += AddToCollections;
        }

        private void OnHandlingStopped(ITrap trap)
        {
            trap.State.Initialized -= AddToCollections;
            RemoveFromCollections(trap);
            NetworkServer.UnSpawn(trap.GameObject);
        }

        private void AddToCollections(ITrap trap)
        {
            _trapCollections.Get<uint>().Add(trap.Identity.netId, trap);
            _trapCollections.Get<Collider>().Add(trap.Physics.Collider, trap);
        }

        private void RemoveFromCollections(ITrap trap)
        {
            _trapCollections.Get<uint>().Remove(trap.Identity.netId);
            _trapCollections.Get<Collider>().Remove(trap.Physics.Collider);
        }
    }
}