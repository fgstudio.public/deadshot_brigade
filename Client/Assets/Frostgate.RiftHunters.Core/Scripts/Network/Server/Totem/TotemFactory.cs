using System;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Network.Shared.Capturing;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Фабрика тотемов. Проводит настройку выданных из пула объектов
    /// и инкапсулирует их данные, отдавай объекты по интерфейсу.
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    public sealed class TotemFactory
    {
        private readonly IRandom _random;
        private readonly TotemPoolRepository _poolRepository;

        public TotemFactory(
            [NotNull] IRandom random,
            [NotNull] TotemPoolRepository poolRepository)
        {
            _random = random;
            _poolRepository = poolRepository;
        }

        public ITotem Create([NotNull] ITotem prefab, Vector3 position) =>
            Create(prefab.Identity.assetId, position);

        public ITotem Create(Guid assetId, Vector3 position)
        {
            IPool<Totem> pool = _poolRepository[assetId];
            Totem totem = pool.Get();


            Transform totemTransform = totem.transform;
            totemTransform.position = position;
            totemTransform.eulerAngles = totemTransform.eulerAngles.SetY(_random.Range(-360, 360));

            totem.State.Health = totem.State.Config.Health;

            return totem;
        }
    }
}