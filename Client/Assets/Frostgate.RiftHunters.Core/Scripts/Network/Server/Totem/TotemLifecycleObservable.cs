using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Systems.Pool;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Отслеживание событий жизненного цикла тотема.
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    public sealed class TotemLifecycleObservable
    {
        public event Action<ITotem> Died;
        public event Action<ITotem> Released;
        public event Action<ITotem> Destroyed;
        public event Action<ITotem> Unhosted;

        [NotNull] private readonly ITotem _totem;
        [CanBeNull] private readonly UnitNetwork _hostUnit;

        public TotemLifecycleObservable([NotNull] ITotem totem, [NotNull] UnitNetwork hostUnit)
        {
            _totem = totem;
            _hostUnit = hostUnit;

            SubscribeTotem(totem);
            SubscribeUnit(hostUnit);
        }

        public void Dispose()
        {
            UnsubscribeTotem(_totem);
            if (_hostUnit != null) UnsubscribeUnit(_hostUnit);
        }

        public void SubscribeUnit([NotNull] UnitNetwork hostUnit) =>
            hostUnit.Destroyed.AddListener(OnUnitDestroyed);

        public void UnsubscribeUnit([NotNull] UnitNetwork hostUnit) =>
            hostUnit.Destroyed.RemoveListener(OnUnitDestroyed);

        private void SubscribeTotem([NotNull] ITotem totem)
        {
            totem.State.Died += OnTotemDied;
            totem.ReadyForRelease += OnTotemReleased;
            totem.Destroyed.AddListener(OnTotemDestroyed);
        }

        private void UnsubscribeTotem([NotNull] ITotem totem)
        {
            totem.State.Died -= OnTotemDied;
            totem.ReadyForRelease -= OnTotemReleased;
            totem.Destroyed.RemoveListener(OnTotemDestroyed);
        }

        private void OnTotemDied() => Died?.Invoke(_totem);
        private void OnTotemReleased(IAutoReleased _) => Released?.Invoke(_totem);
        private void OnTotemDestroyed() => Destroyed?.Invoke(_totem);
        private void OnUnitDestroyed() => Unhosted?.Invoke(_totem);
    }
}
