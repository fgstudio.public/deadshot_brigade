using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class TotemHostUniHandleFacadeFactory
    {
        [NotNull] private readonly TotemEffectDataFactory _dataFactory;
        [NotNull] private readonly UnitEffectStateFactory _effectStateFactory;
        [NotNull] private readonly ILogger<TotemHostUniHandleFacade> _logger;

        public TotemHostUniHandleFacadeFactory(
            [NotNull] TotemEffectDataFactory dataFactory,
            [NotNull] UnitEffectStateFactory effectStateFactory,
            [NotNull] ILogger<TotemHostUniHandleFacade> logger)
        {
            _logger = logger;
            _dataFactory = dataFactory;
            _effectStateFactory = effectStateFactory;
        }

        public TotemHostUniHandleFacade Create([NotNull] UnitNetwork hostUnit) =>
            new(hostUnit, _effectStateFactory, _dataFactory, _logger);
    }
}
