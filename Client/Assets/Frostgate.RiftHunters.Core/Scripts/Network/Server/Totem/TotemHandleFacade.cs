using System;
using System.Linq;
using Mirror;
using UnityEngine;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Systems.BotSpawn;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Обработка серверной логики работы тотемов
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    public sealed class TotemHandleFacade : IDisposable
    {
        private readonly IReadOnlyObjectCollection<uint, UnitNetwork> _units;
        private readonly ObjectCommandSender<BotSpawnerConfig> _commandSender;
        private readonly ObjectCollectionRepository<ITotem> _totemCollections;
        private readonly TotemHostUniHandleFacadeFactory _facadeFactory;
        private readonly TotemFactory _totemFactory;

        private readonly Dictionary<uint, TotemHostUniHandleFacade> _handlers = new();
        private readonly Dictionary<ITotem, TotemLifecycleObservable> _totemObservables = new();

        public TotemHandleFacade(
            [NotNull] IReadOnlyObjectCollection<uint, UnitNetwork> units,
            [NotNull] ObjectCommandSender<BotSpawnerConfig> commandSender,
            [NotNull] ObjectCollectionRepository<ITotem> totemCollections,
            [NotNull] TotemHostUniHandleFacadeFactory facadeFactory,
            [NotNull] TotemFactory totemFactory)
        {
            _totemCollections = totemCollections;
            _commandSender = commandSender;
            _facadeFactory = facadeFactory;
            _totemFactory = totemFactory;
            _units = units;
        }

        public void Dispose() =>
            RemoveAllTotems();

        public void CreateTotem([NotNull] ITotem prefab, [CanBeNull] NetworkIdentity unitIdentity, Vector3 position)
        {
            if (unitIdentity == null) return;
            if (!_units.TryGet(unitIdentity.netId, out UnitNetwork unit)) return;

            uint hostUnitId = unitIdentity.netId;

            ITotem totem = _totemFactory.Create(prefab, position);
            totem.State.HostUnitId = hostUnitId;

            CreateTotemObservable(totem, unit);
            if (unitIdentity != null) StartHandlingTotemForUnit(totem, unit);

            NetworkServer.Spawn(totem.GameObject);

            if (totem.State.IsInitialized) AddToCollections(totem);
            else totem.State.Initialized += AddToCollections;
        }

        public void RemoveAllTotems() =>
            _totemObservables.Keys.ToArray().ForEach(t => t.Destroy());

        private void CreateTotemObservable([NotNull] ITotem totem,
            [NotNull] UnitNetwork hostUnit)
        {
            var observable = new TotemLifecycleObservable(totem, hostUnit);

            observable.Died += OnTotemDied;
            observable.Released += OnReleased;
            observable.Destroyed += OnReleased;
            observable.Unhosted += OnUnhosted;

            _totemObservables[totem] = observable;
        }

        private void RemoveTotemObservable([NotNull] ITotem totem)
        {
            TotemLifecycleObservable observable = _totemObservables[totem];

            observable.Died -= OnTotemDied;
            observable.Released -= OnReleased;
            observable.Destroyed -= OnReleased;
            observable.Unhosted -= OnUnhosted;

            observable.Dispose();
            _totemObservables.Remove(totem);
        }

        private void OnTotemDied([NotNull] ITotem totem)
        {
            StopHandlingTotem(totem);
            StaticPositionProvider positionProvider = new(totem.Position);
            _commandSender.SendSpawnOnNearestSpawner(totem.State.Config.SpawnerConfig, positionProvider);
        }

        private void OnReleased(ITotem totem)
        {
            RemoveTotemObservable(totem);
            StopHandlingTotem(totem);
            RemoveFromCollections(totem);
            totem.State.Initialized -= AddToCollections;

            NetworkServer.UnSpawn(totem.GameObject);
        }

        private void AddToCollections(ITotem totem)
        {
            _totemCollections.Get<uint>().Add(totem.Identity.netId, totem);
            _totemCollections.Get<Collider>().Add(totem.Physics.Collider, totem);
        }

        private void RemoveFromCollections(ITotem totem)
        {
            _totemCollections.Get<uint>().Remove(totem.Identity.netId);
            _totemCollections.Get<Collider>().Remove(totem.Physics.Collider);
        }

        private void OnUnhosted(ITotem totem) =>
            totem.Destroy();

        private void StartHandlingTotemForUnit([NotNull] ITotem totem, [NotNull] UnitNetwork unit)
        {
            uint hostUnitId = unit.Identity.netId;
            if (!_handlers.TryGetValue(hostUnitId, out TotemHostUniHandleFacade handler))
                handler = _handlers[hostUnitId] = _facadeFactory.Create(unit);

            handler.AddTotem(totem);
        }

        private void StopHandlingTotem([NotNull] ITotem totem)
        {
            uint hostUnitId = totem.State.HostUnitId;

            if (_handlers.TryGetValue(hostUnitId, out TotemHostUniHandleFacade handler))
            {
                handler.RemoveTotem(totem);

                if (!handler.IsActive)
                    _handlers.Remove(hostUnitId);
            }
        }
    }
}