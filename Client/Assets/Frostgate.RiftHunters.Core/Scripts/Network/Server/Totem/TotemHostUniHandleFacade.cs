using Mirror;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Обработка серверной логики работы тотемов для конкретного юнита.
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    public sealed class TotemHostUniHandleFacade
    {
        [CanBeNull] private readonly UnitNetwork _hostUnit; // can be disconnected and etc
        [NotNull] private readonly TotemEffectDataFactory _dataFactory;
        [NotNull] private readonly UnitEffectStateFactory _effectStateFactory;
        [NotNull] private readonly ILogger<TotemHostUniHandleFacade> _logger;

        private readonly Dictionary<UnitEffectConfig, TotemEffectData> _effects = new();
        private readonly HashSet<UnitEffectConfig> _postEffects = new();

        public bool IsActive => _effects.Count > 0;

        public TotemHostUniHandleFacade([NotNull] UnitNetwork hostUnit,
            [NotNull] UnitEffectStateFactory effectStateFactory,
            [NotNull] TotemEffectDataFactory dataFactory,
            [NotNull] ILogger<TotemHostUniHandleFacade> logger)
        {
            _logger = logger;
            _dataFactory = dataFactory;
            _hostUnit = hostUnit;
            _effectStateFactory = effectStateFactory;
        }

        public void AddTotem([NotNull] ITotem totem)
        {
            if (_hostUnit == null) return;
            if (_hostUnit.Identity == null) return;

            foreach (UnitEffectConfig effect in totem.State.Config.Effects)
            {
                if (!_effects.TryGetValue(effect, out TotemEffectData data))
                    data = CreateEffectData(effect, _hostUnit.Identity);

                data.AddTotem(totem);
            }

            foreach (UnitEffectConfig postEffect in totem.State.Config.PostEffects)
                _postEffects.Add(postEffect);
        }

        public void RemoveTotem([NotNull] ITotem totem)
        {
            if (_hostUnit == null) return;

            foreach (UnitEffectConfig effect in totem.State.Config.Effects)
                if (_effects.TryGetValue(effect, out TotemEffectData data))
                    data.RemoveTotem(totem);
        }

        private TotemEffectData CreateEffectData([NotNull] UnitEffectConfig effectConfig,
            [NotNull] NetworkIdentity effectReceiver)
        {
            // TODO: Перенести создание стэйта в DataFactory, когда _effectStateFactory будет внутри UnitNetwork
            UnitEffectState effectState = _effectStateFactory.Create(effectConfig, effectReceiver);
            TotemEffectData data = _dataFactory.Create(effectState);
            _effects[effectConfig] = data;
            OnDataCreated(data);

            return data;
        }

        private void OnDataCreated([NotNull] TotemEffectData data)
        {
            data.Emptied += OnDataEmptied;
            _hostUnit?.AddUnitEffect(data.State);
            Log($"Effect {data.State.Config!.name} Applied");
        }

        private void OnDataEmptied([NotNull] TotemEffectData data)
        {
            data.Emptied -= OnDataEmptied;
            _effects.Remove(data.State.Config!);
            _hostUnit?.RemoveUnitEffect(data.State);
            Log($"Effect {data.State.Config.name} Canceled");

            if (_effects.Count == 0 && _hostUnit != null)
                ApplyPostEffects(_postEffects, _hostUnit);
        }

        private void ApplyPostEffects(
            [NotNull] IEnumerable<UnitEffectConfig> postEffects, [CanBeNull] IUnitEffectReceiver effectReceiver)
        {
                foreach (UnitEffectConfig postEffect in postEffects)
                    if (effectReceiver?.Identity != null)
                    {
                        ApplyEffect(postEffect, effectReceiver.Identity, effectReceiver);
                        Log($"Post Effect {postEffect.name} Applied");
                    }

                _postEffects.Clear();
        }

        private void ApplyEffect([NotNull] UnitEffectConfig effect,
            [NotNull] NetworkIdentity source, [NotNull] IUnitEffectReceiver target)
        {
            UnitEffectState effectState = _effectStateFactory.Create(effect, source);
            target.AddUnitEffect(effectState);
        }

        private void Log(string message) =>
            _logger.Log($"Unit {_hostUnit?.Identity.netId} | {message}");
    }
}
