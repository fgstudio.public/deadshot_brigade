using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    public sealed class TotemEffectDataFactory
    {
        [NotNull] private readonly ILogger<TotemEffectData> _dataLogger;

        public TotemEffectDataFactory([NotNull] ILogger<TotemEffectData> dataLogger) =>
            _dataLogger = dataLogger;

        public TotemEffectData Create([NotNull] UnitEffectState effectState) =>
            new(effectState, _dataLogger);
    }
}
