using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit.Network.States;

namespace Frostgate.RiftHunters.Core.Network.Server
{
    /// <summary>
    /// Данные по тотемному эффекту для юнита.
    /// <see cref="https://www.notion.so/frostgate/5a336157f1024b298b9ef8c42f129b61"/>
    /// </summary>
    public sealed class TotemEffectData
    {
        public event Action<TotemEffectData> Emptied;

        public readonly UnitEffectState State;

        [NotNull] private readonly HashSet<ITotem> _totems;
        [NotNull] private readonly ILogger<TotemEffectData> _logger;

        public TotemEffectData([NotNull] UnitEffectState effectState,
            [NotNull] ILogger<TotemEffectData> logger)
        {
            State = effectState;
            _logger = logger;
            _totems = new HashSet<ITotem>();
        }

        public void AddTotem([NotNull] ITotem totem)
        {
            _totems.Add(totem);
            _logger.Log($"Totem {totem.Identity.netId} Added. Count: {_totems.Count}");
        }

        public void RemoveTotem([NotNull] ITotem totem)
        {
            _totems.Remove(totem);
            _logger.Log($"Totem {totem.Identity.netId} Removed. Count: {_totems.Count}");

            if (_totems.Count == 0)
                Emptied?.Invoke(this);
        }
    }
}
