using Zenject;
using Frostgate.RiftHunters.Core.Network.Shared;

namespace Frostgate.RiftHunters.Core.Network
{
    public sealed class NetworkInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<ClientDependencies>().AsSingle();
            Container.Bind<SharedDependencies>().AsSingle();
            Container.Bind<ServerDependencies>().AsSingle();
            Container.Bind<NetworkDependencies>().AsSingle();
        }
    }
}
