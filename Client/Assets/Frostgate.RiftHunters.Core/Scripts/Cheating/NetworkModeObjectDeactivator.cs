﻿using Zenject;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Infrastructure;

namespace Frostgate.RiftHunters.Core.Cheating
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Cheats.Menu + "/" + nameof(NetworkModeObjectDeactivator))]
    public sealed class NetworkModeObjectDeactivator : MonoBehaviour
    {
        [SerializeField, Required] private NetworkMode[] _deactivatingModes;

        [CanBeNull] private Preferences _preferences;

        [Inject]
        private void MonoConstructor(Preferences preferences)
        {
            _preferences = preferences;
            UpdateActivity();
        }

        private void OnEnable()
        {
            UpdateActivity();
        }

        private void UpdateActivity()
        {
            if (_preferences != null && _deactivatingModes.Contains(_preferences.NetworkMode))
                gameObject.SetActive(false);
        }
    }
}