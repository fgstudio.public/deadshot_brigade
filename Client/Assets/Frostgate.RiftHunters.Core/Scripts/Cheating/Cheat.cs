﻿using System;
using Mirror;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Cheating
{
    public abstract class Cheat : NetworkBehaviour
    {
        [SerializeField] private KeyCode[] _keysToCheat;

        private ILogger _logger;

        private void Awake() =>
            _logger = LoggerFactory.CreateLogger(GetType().Name);

        private void Update()
        {
            if (_keysToCheat.Length == 0)
                return;

            // TODO: перевести на новый InputSystem
            for (int i = 0; i < _keysToCheat.Length; i++)
                if (!Input.GetKeyDown(_keysToCheat[i]))
                    return;

            ExecuteCheat();
        }

        [Button, ContextMenu("Execute")]
        private void ExecuteCheat()
        {
            if (isClientOnly)
            {
                CmdExecuteCheat();
                _logger.Log("Sent Cmd");
            }
            else
            {
                CheatExecuting();
                _logger.Log("Executed");
            }
        }

        [Command]
        private void CmdExecuteCheat()
        {
            CheatExecuting();
            _logger.Log("Executed");
        }

        protected abstract void CheatExecuting();
    }
}