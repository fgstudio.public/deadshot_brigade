﻿using Zenject;
using UnityEngine;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle.Shared;
using Frostgate.RiftHunters.Core.Battle.Shared.Unit;
using Frostgate.RiftHunters.Core.Battle.Shared.Damaging;

namespace Frostgate.RiftHunters.Core.Cheating
{
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Shared.Cheats.Menu + "/" + nameof(KillPlayersCheat))]
    public sealed class KillPlayersCheat : Cheat
    {
        [CanBeNull] private IDamageService _damageService;
        [CanBeNull] private IReadOnlyObjectCollection<uint, UnitNetwork> _units;

        [Inject]
        private void MonoConstructor(IDamageService damageService, IReadOnlyObjectCollection<uint, UnitNetwork> units)
        {
            _units = units;
            _damageService = damageService;
        }

        protected override void CheatExecuting()
        {
            foreach (UnitNetwork unitNetwork in _units!.Values)
                if (unitNetwork.UnitType == BattleUnitType.Player)
                    _damageService!.KillInstantly(unitNetwork, unitNetwork);
        }
    }
}