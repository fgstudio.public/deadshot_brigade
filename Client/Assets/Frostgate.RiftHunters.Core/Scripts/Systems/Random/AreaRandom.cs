using UnityEngine;

namespace Frostgate.RiftHunters.Core.Systems
{
    public sealed class AreaRandom
    {
        private readonly IRandom _random;

        public AreaRandom(IRandom random) =>
            _random = random;

        public Vector3 SquareY(Vector3 center, float size) =>
            RectY(center, size, size);

        public Vector3 SquareZ(Vector3 center, float size) =>
            RectZ(center, size, size);

        public Vector3 RectY(Vector3 center, Vector2 size) =>
            RectY(center, size.x, size.y);

        public Vector3 RectZ(Vector3 center, Vector2 size) =>
            RectZ(center, size.x, size.y);

        public Vector3 RectY(Vector3 center, float width, float height)
        {
            Vector3 radiusVector = new Vector3(width / 2f, default, height / 2f);
            Vector3 minPoint = center - radiusVector;
            Vector3 maxPoint = center + radiusVector;

            return new Vector3
            {
                x = _random.Range(minPoint.x, maxPoint.x),
                y = center.y,
                z = _random.Range(minPoint.z, maxPoint.z)
            };
        }

        public Vector3 RectZ(Vector3 center, float width, float height)
        {
            Vector3 radiusVector = new Vector3(width / 2f, height / 2f, default);
            Vector3 minPoint = center - radiusVector;
            Vector3 maxPoint = center + radiusVector;

            return new Vector3
            {
                x = _random.Range(minPoint.x, maxPoint.x),
                y = _random.Range(minPoint.y, maxPoint.y),
                z = center.z
            };
        }

        public Vector3 CircleBorderY(Vector3 center, float radius)
        {
            float angle = _random.Range(0, 360);

            return CreateVector(center, angle, radius);
        }

        public Vector3 SphereY(Vector3 center, float radius)
        {
            float angle = _random.Range(0, 360);
            float magnitude = _random.Range(0, radius);

            return CreateVector(center, angle, magnitude);
        }

        private Vector3 CreateVector(Vector3 center, float angle, float magnitude) =>
            Area.CreateVector(center, angle, magnitude);
    }
}
