using System;
using System.Linq;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Systems
{
    public sealed class UnityRandom : IRandom
    {
        public int Range(int minInclusive, int maxExclusive) => UnityEngine.Random.Range(minInclusive, maxExclusive);

        public float Range(float minInclusive, float maxInclusive) => UnityEngine.Random.Range(minInclusive, maxInclusive);

        public T List<T>(IReadOnlyList<T> list)
        {
            int randomIndex = Range(0, list.Count);
            return list[randomIndex];
        }

        public T WeightedList<T>(IReadOnlyList<T> list, float weight, Func<T, float> weightGetter)
        {
            float minWeight = FindMinWeight(list, weightGetter);
            return WeightedList(list, weight, minWeight, weightGetter);
        }

        public T WeightedList<T>(IReadOnlyList<T> list, float weight, float minWeight, Func<T, float> weightGetter)
        {
            float accumulatedChance = minWeight;

            for (int i = 0; i < list.Count; i++)
            {
                T element = list[i];
                float elementWeight = weightGetter(element);

                var range = new MinMaxFloat(accumulatedChance, accumulatedChance + elementWeight);
                if (range.Contains(weight))
                    return element;

                accumulatedChance += elementWeight;
            }

            return default;
        }

        private float FindMinWeight<T>(IReadOnlyList<T> list, Func<T, float> weightGetter) =>
            list.Select(weightGetter).Min();
    }
}
