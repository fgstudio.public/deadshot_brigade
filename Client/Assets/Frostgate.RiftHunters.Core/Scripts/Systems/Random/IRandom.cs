using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Systems
{
    public interface IRandom
    {
        int Range(int minInclusive, int maxExclusive);
        float Range(float minInclusive, float maxInclusive);
        T List<T>([NotNull] IReadOnlyList<T> list);
        T WeightedList<T>([NotNull] IReadOnlyList<T> list, float weight, [NotNull] Func<T, float> weightGetter);
        T WeightedList<T>([NotNull] IReadOnlyList<T> list, float weight, float minWeight, [NotNull] Func<T, float> weightGetter);
    }
}
