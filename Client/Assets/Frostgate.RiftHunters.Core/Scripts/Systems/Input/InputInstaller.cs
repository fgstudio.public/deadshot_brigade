﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle.Client;
using Frostgate.RiftHunters.Core.Battle.Client.Unit;

namespace Frostgate.RiftHunters.Core.Systems.Input
{
    public sealed class InputInstaller : MonoInstaller
    {
        [SerializeField, Required] private PlayerInput _playerInput;
        [SerializeField, Required] private Transform _ui;

        public override void InstallBindings()
        {
            Devices.Mobile.Initialize();
            Container.Bind<CoreInput>().AsSingle();
            Container.Resolve<CoreInput>().Enable();

            Container.Bind<PlayerInput>().FromInstance(_playerInput);
            Container.Bind<PlayerInputAttacher>().FromMethod(CreatePlayerInputAttacher).AsSingle();
        }

        private PlayerInputAttacher CreatePlayerInputAttacher() =>
            new(Container.TryResolve<BattleCamera>(), _playerInput, _ui);
    }
}
