using System.Linq;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.InputSystem.Utilities;

namespace Frostgate.RiftHunters.Core.Systems.Input.Devices
{
    public enum MobileButton
    {
        Reload = 0,
        Ability = 1,
        Ultimate = 2,
        Dodge = 3,
        ExtraAbility = 4,
        MuteSelfVoice = 5,
        MuteParticipantsVoice = 6,
    }

    public struct MobileState : IInputStateTypeInfo
    {
        [InputControl(displayName = nameof(Mobile.Stick), layout = nameof(Stick), usage = "Primary2DMotion", processors = "stickDeadzone")]
        public Vector2 Stick;

        [InputControl(displayName = nameof(Mobile.Delta), layout = "Vector2", usage = "Secondary2DMotion")]
        public Vector2 Delta;

        [InputControl(name = nameof(Mobile.Reload), layout = "Button", bit = (uint)MobileButton.Reload, displayName = "Button Reload")]
        [InputControl(name = nameof(Mobile.Ability), layout = "Button", bit = (uint)MobileButton.Ability, displayName = "Button Ability")]
        [InputControl(name = nameof(Mobile.Ultimate), layout = "Button", bit = (uint)MobileButton.Ultimate, displayName = "Button Ultimate")]
        [InputControl(name = nameof(Mobile.Dodge), layout = "Button", bit = (uint)MobileButton.Dodge, displayName = "Button Dodge")]
        [InputControl(name = nameof(Mobile.ExtraAbility), layout = "Button", bit = (uint)MobileButton.ExtraAbility, displayName = "Button Extra Ability")]
        [InputControl(name = nameof(Mobile.MuteSelfVoice), layout = "Button", bit = (uint)MobileButton.MuteSelfVoice, displayName = "Button Mute Self Voice")]
        [InputControl(name = nameof(Mobile.MuteParticipantsVoice), layout = "Button", bit = (uint)MobileButton.MuteParticipantsVoice, displayName = "Button Mute Participants Voice")]
        public uint Buttons;

        private static readonly FourCC sFormat = new FourCC('M', 'O', 'B', 'I');
        public FourCC format => sFormat;
    }

    /// <summary>
    /// Кастомный InputDevice для взаимодействия с компонентами в UI.
    /// <see href="https://www.notion.so/frostgate/UI-44e0621d108743c28cd8e6e80ab5158e">Документация</see>
    /// </summary>
#if UNITY_EDITOR
    [UnityEditor.InitializeOnLoad]
#endif
    [InputControlLayout(stateType = typeof(MobileState), isGenericTypeOfDevice = true)]
    public sealed class Mobile : InputDevice
    {
        public static Mobile Current { get; private set; }

        [InputControl] public Vector2Control Delta { get; private set; }
        [InputControl] public StickControl Stick { get; private set; }
        [InputControl] public ButtonControl Reload { get; private set; }
        [InputControl] public ButtonControl Ability { get; private set; }
        [InputControl] public ButtonControl Ultimate { get; private set; }
        [InputControl] public ButtonControl Dodge { get; private set; }
        [InputControl] public ButtonControl ExtraAbility { get; private set; }
        [InputControl] public ButtonControl MuteSelfVoice { get; private set; }
        [InputControl] public ButtonControl MuteParticipantsVoice { get; private set; }

        public ButtonControl this[MobileButton button] => button switch
        {
            MobileButton.Reload => Reload,
            MobileButton.Ability => Ability,
            MobileButton.Ultimate => Ultimate,
            MobileButton.Dodge => Dodge,
            MobileButton.ExtraAbility => ExtraAbility,
            MobileButton.MuteSelfVoice => MuteSelfVoice,
            MobileButton.MuteParticipantsVoice => MuteParticipantsVoice,
            _ => throw new InvalidEnumArgumentException(nameof(button), (int)button, typeof(MobileButton))
        };

        public override void MakeCurrent()
        {
            base.MakeCurrent();
            Current = this;
        }

        protected override void OnRemoved()
        {
            base.OnRemoved();
            if (Current == this)
                Current = null;
        }

        protected override void FinishSetup()
        {
            Stick = GetChildControl<StickControl>(nameof(Stick));
            Delta = GetChildControl<Vector2Control>(nameof(Delta));
            Reload = GetChildControl<ButtonControl>(nameof(Reload));
            Ability = GetChildControl<ButtonControl>(nameof(Ability));
            Ultimate = GetChildControl<ButtonControl>(nameof(Ultimate));
            Dodge = GetChildControl<ButtonControl>(nameof(Dodge));
            ExtraAbility = GetChildControl<ButtonControl>(nameof(ExtraAbility));
            MuteSelfVoice = GetChildControl<ButtonControl>(nameof(MuteSelfVoice));
            MuteParticipantsVoice = GetChildControl<ButtonControl>(nameof(MuteParticipantsVoice));
            base.FinishSetup();
        }

        static Mobile()
        {
            InputSystem.RegisterLayout<Mobile>();

            if (!InputSystem.devices.Any(x => x is Mobile))
                InputSystem.AddDevice<Mobile>(nameof(Mobile));
        }

        [RuntimeInitializeOnLoadMethod]
        public static void Initialize()
        { }
    }
}
