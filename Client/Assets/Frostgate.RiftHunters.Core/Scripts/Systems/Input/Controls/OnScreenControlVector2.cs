using UnityEngine;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Systems.Input.Controls
{
    /// <summary>
    /// Кастомный компонент, который позволяет отправлять Vector2-данные в Input System.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.Input.Menu + "/" + "On-Screen Control Vector2")]
    [HelpURL("https://www.notion.so/frostgate/UI-44e0621d108743c28cd8e6e80ab5158e")]
    public sealed class OnScreenControlVector2 : OnScreenControl
    {
        [InputControl(layout = nameof(Vector2))]
        [SerializeField, Required] private string _controlPath;

        protected override string controlPathInternal
        {
            get => _controlPath;
            set => _controlPath = value;
        }

        public void SendVector2(Vector2 vector) => SendValueToControl(vector);
    }
}