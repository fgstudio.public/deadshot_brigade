using Zenject;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace Frostgate.RiftHunters.Core.Systems.Input
{
    /// <summary>
    /// Обёртка для Player Action Map в Input System.
    /// <see cref="CoreInput"/>
    /// </summary>
    [AddComponentMenu(BattleComponentMenus.Client.Input.Menu + "/" + nameof(PlayerInput))]
    [HelpURL("https://www.notion.so/frostgate/Input-System-05b4ba6855854644858df2a84bfa4815")]
    public sealed class PlayerInput : Component
    {
        private const string EventsFoldoutName = "Events";
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent<Vector2> Move { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent<Vector2> Look { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Reload { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent ExtraAbility { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Ultimate { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent Dodge { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent<int> Emotion { get; private set; }

        private const string AbilityFoldoutName = "Ability";
        [field: SerializeField, FoldoutGroup(AbilityFoldoutName)] public UnityEvent Ability { get; private set; }
        [field: SerializeField, FoldoutGroup(AbilityFoldoutName)] public UnityEvent AbilityPressed { get; private set; }
        [field: SerializeField, FoldoutGroup(AbilityFoldoutName)] public UnityEvent AbilityCanceled { get; private set; }

        private const string VoiceFoldoutName = "Voice";
        [field: SerializeField, FoldoutGroup(VoiceFoldoutName)] public UnityEvent MuteSelfVoice { get; private set; }
        [field: SerializeField, FoldoutGroup(VoiceFoldoutName)] public UnityEvent MuteParticipantsVoice { get; private set; }

        public bool IsAbilityPressed { get; private set; }

        [CanBeNull] private CoreInput _input;
        private bool _isSubscribed;

        [Inject]
        private void MonoConstructor(CoreInput coreInput)
        {
            _input = coreInput;

            if (!_isSubscribed)
                OnEnabled();
        }

        protected override void OnEnabled()
        {
            if (!_isSubscribed && _input != null)
                Subscribe(_input);
        }

        protected override void OnDisabled()
        {
            if (_isSubscribed && _input != null)
                Unsubscribe(_input);

            CancelInputInProgress();
        }

        private void CancelInputInProgress()
        {
            Move?.Invoke(Vector2.zero);
            Look?.Invoke(Vector2.zero);
        }

        private void Subscribe([NotNull] CoreInput input)
        {
            _isSubscribed = true;

            input.Player.Move.canceled += OnMoveCancelled;
            input.Player.Move.performed += OnMovePerformed;
            input.Player.Look.performed += OnLookPerformed;;
            input.Player.Reload.performed += OnReloadPerformed;
            input.Player.Ultimate.performed += OnUltimatePerformed;
            input.Player.Dodge.performed += OnDodgePerformed;
            input.Player.Emotion.performed += OnEmotionPerformed;

            input.Player.Ability.performed += OnAbilityPerformed;
            input.Player.Ability.started += OnAbilityStarted;
            input.Player.Ability.canceled += OnAbilityCanceled;

            input.Player.ExtraAbility.performed += OnExtraAbilityPerformed;

            input.Player.MuteSelfVoice.performed += OnMuteSelfPerformed;
            input.Player.MuteParticipantsVoice.performed += OnMuteParticipantsPerformed;
        }

        private void Unsubscribe([NotNull] CoreInput input)
        {
            _isSubscribed = false;

            input.Player.Move.canceled -= OnMoveCancelled;
            input.Player.Move.performed -= OnMovePerformed;
            input.Player.Look.performed -= OnLookPerformed;
            input.Player.Reload.performed -= OnReloadPerformed;
            input.Player.Ultimate.performed -= OnUltimatePerformed;
            input.Player.Dodge.performed -= OnDodgePerformed;
            input.Player.Emotion.performed -= OnEmotionPerformed;

            input.Player.Ability.performed -= OnAbilityPerformed;
            input.Player.Ability.started -= OnAbilityStarted;
            input.Player.Ability.canceled -= OnAbilityCanceled;

            input.Player.ExtraAbility.performed -= OnExtraAbilityPerformed;

            input.Player.MuteSelfVoice.performed -= OnMuteSelfPerformed;
            input.Player.MuteParticipantsVoice.performed -= OnMuteParticipantsPerformed;
        }

        private void OnMoveCancelled(InputAction.CallbackContext context) => Move?.Invoke(context.ReadValue<Vector2>());
        private void OnMovePerformed(InputAction.CallbackContext context) => Move?.Invoke(context.ReadValue<Vector2>());
        private void OnLookPerformed(InputAction.CallbackContext context) => Look?.Invoke(context.ReadValue<Vector2>());
        private void OnReloadPerformed(InputAction.CallbackContext context) => Reload?.Invoke();

        private void OnAbilityPerformed(InputAction.CallbackContext context)
        {
            IsAbilityPressed = false;
            Ability?.Invoke();
        }

        private void OnAbilityStarted(InputAction.CallbackContext context)
        {
            IsAbilityPressed = true;
            AbilityPressed?.Invoke();
        }

        private void OnAbilityCanceled(InputAction.CallbackContext context)
        {
            IsAbilityPressed = false;
            AbilityCanceled?.Invoke();
        }

        private void OnExtraAbilityPerformed(InputAction.CallbackContext context) => ExtraAbility?.Invoke();
        private void OnUltimatePerformed(InputAction.CallbackContext context) => Ultimate?.Invoke();
        private void OnDodgePerformed(InputAction.CallbackContext context) => Dodge?.Invoke();
        private void OnEmotionPerformed(InputAction.CallbackContext context) => Emotion?.Invoke(context.ReadValue<int>());

        // TODO: я хз. По-хорошему ничего инвокать не нужно и все события должны прийти через систему инпута юнити. Не разобрался как там сконфигать нажатия.
        public void InvokeEmotion(int index) => Emotion?.Invoke(index);

        private void OnMuteSelfPerformed(InputAction.CallbackContext _) => MuteSelfVoice?.Invoke();
        private void OnMuteParticipantsPerformed(InputAction.CallbackContext _) => MuteParticipantsVoice?.Invoke();
    }
}