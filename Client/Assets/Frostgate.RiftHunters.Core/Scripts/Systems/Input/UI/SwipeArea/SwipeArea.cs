using UnityEngine;
using UnityEngine.Events;
using Frostgate.RiftHunters.Core.Systems.Input.UI;

namespace Frostgate.RiftHunters.Core.UI
{
    public class SwipeArea : MonoBehaviour
    {
        public UnityEvent SwipedToRight;
        public UnityEvent SwipedToLeft;

        [SerializeField] private DragArea _dragArea;
        [SerializeField] private float _dragSensitive = 0.02f;

        private Vector2 _dragVector;

        public void OnEnable()
        {
            _dragArea.PointerDragEvent.AddListener(OnPointerDrag);
            _dragArea.PointerUpEvent.AddListener(OnPointerUp);
        }

        public void OnDisable()
        {
            _dragArea.PointerDragEvent.RemoveAllListeners();
            _dragArea.PointerUpEvent.RemoveAllListeners();
        }

        private void OnPointerDrag(Vector2 vector) => _dragVector = vector;

        private void OnPointerUp()
        {
            if (_dragVector.x > _dragSensitive)
                SwipedToRight?.Invoke();
            if(_dragVector.x < -_dragSensitive)
                SwipedToLeft?.Invoke();

            _dragVector = Vector2.zero;
        }
    }
}
