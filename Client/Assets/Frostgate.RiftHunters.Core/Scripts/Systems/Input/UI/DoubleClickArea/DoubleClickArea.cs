using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Frostgate.RiftHunters.Core.Systems.Input.UI
{
    public sealed class DoubleClickArea : Component, IPointerDownHandler
    {
        private const string EventsFoldoutName = "DoubleClickEvent";
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent DoubleClickEvent { get; private set; }

        [SerializeField] private float _doubleClickTime = 0.3f;
        private float _lastClickTime;

        public void OnPointerDown(PointerEventData eventData)
        {
            var currentTime = Time.time;

            if (currentTime - _lastClickTime < _doubleClickTime)
                DoubleClickEvent.Invoke();
            else
                _lastClickTime = currentTime;
        }
    }
}
