using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem.OnScreen;

namespace Frostgate.RiftHunters.Core.Components.UI
{
    public sealed class ButtonClicker : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private OnScreenButton _button;

        private void OnValidate() =>
            _button ??= GetComponentInChildren<OnScreenButton>(true);

        [UsedImplicitly] public void Click()
        {
            _button.OnPointerDown(null);
            _button.OnPointerUp(null);
        }
    }
}
