using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Systems.Input.UI
{
    public interface IDragArea
    {
        UnityEvent<Vector2> PointerDragEvent { get; }
        UnityEvent PointerDownEvent { get; }
        UnityEvent PointerUpEvent { get; }
    }

    /// <summary>
    /// Область для считывания касаний, которые приводят к повороту подконтрольного персонажа.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu(BattleComponentMenus.Client.Input.Menu + "/" + nameof(DragArea))]
    [HelpURL("https://www.notion.so/frostgate/DragArea-ec799004db8742deaf7290d814ab2ebc")]
    public sealed class DragArea: Component, IDragHandler, IPointerDownHandler, IPointerUpHandler, IDragArea
    {
        private const string EventsFoldoutName = "Events";
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent<Vector2> PointerDragEvent { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent PointerDownEvent { get; private set; }
        [field: SerializeField, FoldoutGroup(EventsFoldoutName)] public UnityEvent PointerUpEvent { get; private set; }

        public void OnPointerDown(PointerEventData eventData) => PointerDownEvent?.Invoke();
        public void OnPointerUp(PointerEventData eventData) => PointerUpEvent?.Invoke();
        public void OnDrag(PointerEventData eventData)
        {
            Vector2 inchesDelta = eventData.delta / Screen.dpi;
            PointerDragEvent?.Invoke(inchesDelta);
        }
    }
}