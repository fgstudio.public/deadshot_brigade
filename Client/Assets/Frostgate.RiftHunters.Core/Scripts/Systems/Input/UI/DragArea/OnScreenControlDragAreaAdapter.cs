using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Input.Controls;

namespace Frostgate.RiftHunters.Core.Systems.Input.UI
{
    /// <summary>
    /// Адаптер, который передаёт данные от DragArea к OnScreenControlVector2.
    /// <see cref="DragArea"/>
    /// <see cref="OnScreenControlVector2"/>
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu(BattleComponentMenus.Client.Input.Menu + "/" + "On-Screen Control DragAreaAdapter")]
    [HelpURL("https://www.notion.so/frostgate/Joystick-6c40479a203b413394ff29ed63bb4813")]
    public sealed class OnScreenControlDragAreaAdapter : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private DragArea _dragArea;
        [SerializeField, Required, ChildGameObjectsOnly] private OnScreenControlVector2 _control;

        private void OnEnable()
        {
            _dragArea.PointerDragEvent.AddListener(SendVector2);
            _dragArea.PointerDownEvent.AddListener(SendZeroVector2);
            _dragArea.PointerUpEvent.AddListener(SendZeroVector2);
        }

        private void OnDisable()
        {
            _dragArea.PointerDragEvent.RemoveListener(SendVector2);
            _dragArea.PointerDownEvent.RemoveListener(SendZeroVector2);
            _dragArea.PointerUpEvent.RemoveListener(SendZeroVector2);
        }

        private void SendZeroVector2() => SendVector2(Vector2.zero);
        private void SendVector2(Vector2 vector2) => _control.SendVector2(vector2);
    }
}