using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Frostgate.RiftHunters.Core.Systems.Input.UI
{
    /// <summary>
    /// Элемент тач-управления, представляющий собой аналог стика на геймпаде.
    /// Учитывает отклонение центрального элемента от центрального положения.
    /// Копипаст из ThirdPersonTemplate от Unity с авторскими модификациями.
    /// Находится в фиксированной части экрана на заданной заранее позиции.
    /// Отклонение стика происходит в пределах указанной области джойстика.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu(BattleComponentMenus.Client.Input.Menu + "/" + nameof(FixedJoystick))]
    [HelpURL("https://www.notion.so/frostgate/Joystick-6c40479a203b413394ff29ed63bb4813")]
    public sealed class FixedJoystick : Joystick
    {
        private const string SettingsFoldoutName = "Settings";
        private const string EventsFoldoutName = "Events";
        private const float UnityMagicConst = 2.5f;

        [field: Header("Rect References")]
        [field: SerializeField, Required, ChildGameObjectsOnly] public RectTransform Container { get; private set; }
        [field: SerializeField, Required, ChildGameObjectsOnly] public RectTransform Handle { get; private set; }

        [FoldoutGroup(SettingsFoldoutName)] public bool DragEnabled = true;
        [FoldoutGroup(SettingsFoldoutName), SerializeField, Min(0)] private float _magnitudeClamp = 1f;
        [FoldoutGroup(SettingsFoldoutName), SerializeField, Range(1, 45)] private float _sectorAngleSize = 5;

        [FoldoutGroup(EventsFoldoutName), SerializeField] private UnityEvent<Vector2> _joystickDragEvent;
        [FoldoutGroup(EventsFoldoutName), SerializeField] private UnityEvent _joystickDownEvent;
        [FoldoutGroup(EventsFoldoutName), SerializeField] private UnityEvent _joystickUpEvent;

        public override UnityEvent<Vector2> JoystickDragEvent => _joystickDragEvent;
        public override UnityEvent JoystickDownEvent => _joystickDownEvent;
        public override UnityEvent JoystickUpEvent => _joystickUpEvent;

        protected override void OnEnabled() => ResetHandleRectPosition();
        protected override void OnDisabled() => ResetHandleRectPosition();

        public override void OnPointerDown(PointerEventData eventData)
        {
            _joystickDownEvent?.Invoke();
            UpdateControlPosition(eventData);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            _joystickUpEvent?.Invoke();
            ResetHandleRectPosition();
        }

        public override void OnDrag(PointerEventData eventData)
        {
            if (!DragEnabled)
                return;

            UpdateControlPosition(eventData);
        }

        private void UpdateControlPosition(PointerEventData eventData)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                Container, eventData.position, eventData.pressEventCamera, out var position);

            UpdateHandleRectPosition(eventData.position);

            position = ApplySizeDelta(position);
            position = ClampValuesToMagnitude(position);

            var sectorPosition = ClampPositionBySector(position);

            //TODO: Я хз. Но после прохода через инпут систему события теряются. Такой вариант помог но надо разобраться
            _joystickDragEvent?.Invoke(Vector2.zero);
            _joystickDragEvent?.Invoke(sectorPosition);
        }

        private Vector2 ClampPositionBySector(Vector2 position)
        {
            var angle = Vector2.SignedAngle(Vector2.up, position);
            var sector = angle / _sectorAngleSize;
            var roundedSector = Mathf.RoundToInt(sector);
            var targetAngle = roundedSector * _sectorAngleSize;
            var sectorPosition = Vector2.up.Rotate(targetAngle) * position.magnitude;
            return sectorPosition;
        }

        private void ResetHandleRectPosition()
        {
            UpdateHandleRectPosition(Container.position);
            _joystickDragEvent?.Invoke(Vector2.zero);
        }

        private void UpdateHandleRectPosition(Vector2 newPosition)
        {
            if (Handle)
                Handle.position = newPosition;
        }

        private Vector2 ApplySizeDelta(Vector2 position) => new
        (
            position.x / Container.sizeDelta.x * UnityMagicConst,
            position.y / Container.sizeDelta.y * UnityMagicConst
        );

        private Vector2 ClampValuesToMagnitude(Vector2 position) =>
            Vector2.ClampMagnitude(position, _magnitudeClamp);
    }
}