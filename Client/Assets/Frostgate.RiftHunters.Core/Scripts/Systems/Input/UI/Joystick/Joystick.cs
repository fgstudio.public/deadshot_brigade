using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Frostgate.RiftHunters.Core.Systems.Input.UI
{
    public interface IJoystick
    {
        UnityEvent<Vector2> JoystickDragEvent { get; }
        UnityEvent JoystickDownEvent { get; }
        UnityEvent JoystickUpEvent { get; }
    }

    public abstract class Joystick : Component, IJoystick, IPointerDownHandler, IDragHandler, IPointerUpHandler
    {
        public abstract UnityEvent<Vector2> JoystickDragEvent { get; }
        public abstract UnityEvent JoystickDownEvent { get; }
        public abstract UnityEvent JoystickUpEvent { get; }

        public abstract void OnPointerDown(PointerEventData eventData);
        public abstract void OnDrag(PointerEventData eventData);
        public abstract void OnPointerUp(PointerEventData eventData);
    }
}
