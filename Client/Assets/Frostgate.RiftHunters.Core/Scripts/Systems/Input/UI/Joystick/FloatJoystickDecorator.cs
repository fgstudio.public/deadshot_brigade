using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Frostgate.RiftHunters.Core.Systems.Input.UI
{
    /// <summary>
    /// При воздействии от игрока появляется в месте касания с установленным стиком в центральном положении
    /// и далее работает как фиксированный джойстик.
    /// При отсутствии взаимодействия возвращается в первоначальное положение по умолчанию.
    /// <see cref="FixedJoystick"/>
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu(BattleComponentMenus.Client.Input.Menu + "/" + nameof(FloatJoystickDecorator))]
    [HelpURL("https://www.notion.so/frostgate/Joystick-6c40479a203b413394ff29ed63bb4813")]
    public sealed class FloatJoystickDecorator : Joystick
    {
        [SerializeField, Required, ChildGameObjectsOnly] private FixedJoystick _joystick;

        private IDragHandler[] _pointerDragHandlers;
        private IPointerUpHandler[] _pointerUpHandlers;
        private IPointerDownHandler[] _pointerDownHandlers;

        public override UnityEvent<Vector2> JoystickDragEvent => _joystick.JoystickDragEvent;
        public override UnityEvent JoystickDownEvent => _joystick.JoystickDownEvent;
        public override UnityEvent JoystickUpEvent => _joystick.JoystickUpEvent;

        private void OnValidate() => InitComponents();
        private void Awake() => InitComponents();

        private void InitComponents()
        {
            _joystick ??= GetComponent<FixedJoystick>();

            _pointerDragHandlers = _joystick.GetComponentsInChildren<IDragHandler>();
            _pointerUpHandlers = _joystick.GetComponentsInChildren<IPointerUpHandler>();
            _pointerDownHandlers = _joystick.GetComponentsInChildren<IPointerDownHandler>();
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            _joystick.Container.position = eventData.position;

            foreach (IPointerDownHandler handler in _pointerDownHandlers)
                handler.OnPointerDown(eventData);
        }

        public override void OnDrag(PointerEventData eventData)
        {
            MoveCenterToTouch(eventData);

            foreach (IDragHandler handler in _pointerDragHandlers)
                handler.OnDrag(eventData);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            foreach (IPointerUpHandler handler in _pointerUpHandlers)
                handler.OnPointerUp(eventData);
        }

        private void MoveCenterToTouch(PointerEventData eventData)
        {
            var sizeDelta = _joystick.Container.TransformVector(_joystick.Container.sizeDelta);
            var maxDistanceCenterToTouch = sizeDelta.x / 2;
            var deltaPosition = eventData.position - (Vector2)_joystick.Container.position;

            if (deltaPosition.magnitude > maxDistanceCenterToTouch)
                _joystick.Container.position = eventData.position - deltaPosition.normalized * maxDistanceCenterToTouch;
        }
    }
}