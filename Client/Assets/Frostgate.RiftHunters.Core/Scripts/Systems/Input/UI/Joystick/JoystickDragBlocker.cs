using UnityEngine;
using UnityEngine.EventSystems;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Systems.Input.UI
{
    /// <summary>
    /// Компонент, который позволяет блокировать движение джойстика в начальной точке.
    /// Был использован в Outfire для того, чтобы реализовывать логику при нажатии на джойстик.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(FixedJoystick))]
    [AddComponentMenu(BattleComponentMenus.Client.Input.Menu + "/" + nameof(JoystickDragBlocker))]
    [HelpURL("https://www.notion.so/frostgate/Joystick-6c40479a203b413394ff29ed63bb4813")]
    public sealed class JoystickDragBlocker : Component, IPointerDownHandler, IDragHandler
    {
        [Header("References")]
        [SerializeField, Required] private FixedJoystick _joystick;

        [Header("Settings")]
        [SerializeField, Min(0)] private float _blockRadiusInches = 0.1f;

        private bool _canBlock;
        private Vector2 _onDownPosition;

        private void Awake() => InitJoystick();
        private void OnValidate() => InitJoystick();
        private void InitJoystick()
        {
            _joystick ??= GetComponent<FixedJoystick>();
            _canBlock = _joystick.DragEnabled;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!_canBlock)
                return;

            _joystick.DragEnabled = false;
            _onDownPosition = eventData.position;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (_canBlock && !_joystick.DragEnabled)
                _joystick.DragEnabled = CalcDragEnabling(eventData);
        }

        private bool CalcDragEnabling(PointerEventData eventData)
        {
            float delta = Vector2.Distance(eventData.position, _onDownPosition);
            float inches = delta / Screen.dpi;
            return inches > _blockRadiusInches;
        }
    }
}