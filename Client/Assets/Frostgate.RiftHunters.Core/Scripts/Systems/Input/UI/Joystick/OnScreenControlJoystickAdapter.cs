using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Systems.Input.Controls;

namespace Frostgate.RiftHunters.Core.Systems.Input.UI
{
    /// <summary>
    /// Адаптер, который передаёт данные от Joystick к OnScreenControlVector2.
    /// <see cref="FixedJoystick"/>
    /// <see cref="OnScreenControlVector2"/>
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu(BattleComponentMenus.Client.Input.Menu + "/" + "On-Screen Control JoystickAdapter")]
    [HelpURL("https://www.notion.so/frostgate/Joystick-6c40479a203b413394ff29ed63bb4813")]
    public sealed class OnScreenControlJoystickAdapter : MonoBehaviour
    {
        [SerializeField, Required, ChildGameObjectsOnly] private Joystick _joystick;
        [SerializeField, Required, ChildGameObjectsOnly] private OnScreenControlVector2 _control;

        private void Start()
        {
            _joystick.JoystickDragEvent.AddListener(SendVector2);
            _joystick.JoystickUpEvent.AddListener(SendZeroVector2);
            _joystick.JoystickDownEvent.AddListener(SendZeroVector2);
        }

        private void OnDestroy()
        {
            _joystick.JoystickDragEvent.RemoveListener(SendVector2);
            _joystick.JoystickUpEvent.RemoveListener(SendZeroVector2);
            _joystick.JoystickDownEvent.RemoveListener(SendZeroVector2);
        }

        private void SendZeroVector2() => SendVector2(Vector2.zero);
        private void SendVector2(Vector2 vector2) => _control.SendVector2(vector2);
    }
}