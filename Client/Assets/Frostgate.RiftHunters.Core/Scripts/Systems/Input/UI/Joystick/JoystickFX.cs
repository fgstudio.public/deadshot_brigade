using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Systems.Input.UI
{
    /// <summary>
    /// Компонент, отвечающий за логику работы подсветки.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu(BattleComponentMenus.Client.Input.Menu + "/" + nameof(JoystickFX))]
    [HelpURL("https://www.notion.so/frostgate/Joystick-6c40479a203b413394ff29ed63bb4813")]
    public sealed class JoystickFX : Component
    {
        private const string ReferencesFoldoutName = "References";
        private const string SettingsFoldoutName = "Settings";
        private const int MaxAngle = 360;

        [FoldoutGroup(ReferencesFoldoutName), SerializeField, Required, ChildGameObjectsOnly] private RectTransform _fxRect;
        [FoldoutGroup(ReferencesFoldoutName), SerializeField, Required, ChildGameObjectsOnly] private CanvasGroup _fxGroup;
        [FoldoutGroup(ReferencesFoldoutName), SerializeField, Required, ChildGameObjectsOnly] private Joystick _joystick;
        [FoldoutGroup(ReferencesFoldoutName), SerializeField, Required, ChildGameObjectsOnly] private RectTransform _handle;

        [FoldoutGroup(SettingsFoldoutName), SerializeField, Min(0)] private float _minAlpha;
        [FoldoutGroup(SettingsFoldoutName), SerializeField, Min(0)] private float _maxAlpha;
        [FoldoutGroup(SettingsFoldoutName), SerializeField, Range(-MaxAngle, MaxAngle)] private float _angleOffset;
        [FoldoutGroup(SettingsFoldoutName), SerializeField, Min(0)] private float _minDistance;
        [FoldoutGroup(SettingsFoldoutName), SerializeField, Min(0)] private float _maxDistance;

        protected override void OnEnabled()
        {
            Subscribe();
            ResetAlpha();
        }

        protected override void OnDisabled()
        {
            Unsubscribe();
        }

        private void Subscribe()
        {
            _joystick.JoystickDragEvent.AddListener(OnJoystickDrag);
            _joystick.JoystickUpEvent.AddListener(OnJoystickUp);
        }

        private void Unsubscribe()
        {
            _joystick.JoystickDragEvent.RemoveListener(OnJoystickDrag);
            _joystick.JoystickUpEvent.RemoveListener(OnJoystickUp);
        }

        private void OnJoystickDrag(Vector2 _)
        {
            UpdateAlpha();
            UpdateRotation();
        }

        private void OnJoystickUp()
        {
            ResetAlpha();
        }

        private void UpdateAlpha() => SetAlpha(CalcActualAlpha());
        private void ResetAlpha() => SetAlpha(_minAlpha);
        private void SetAlpha(float alpha) => _fxGroup.alpha = alpha;
        private float CalcActualAlpha()
        {
            float sqrDistance = _handle.localPosition.sqrMagnitude;
            float sqrDistanceRange = Mathf.Pow(_maxDistance - _minDistance, 2);
            return Mathf.Lerp(_minAlpha, _maxAlpha, sqrDistance / sqrDistanceRange);
        }

        private void UpdateRotation() => SetRotation(CalcActualRotation());
        private void SetRotation(float angle) => _fxRect.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        private float CalcActualRotation() => Vector2.SignedAngle(Vector2.up, _handle.localPosition) + _angleOffset;
    }
}