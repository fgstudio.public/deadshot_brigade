using UnityEngine;
using UnityEngine.InputSystem.EnhancedTouch;

namespace Frostgate.RiftHunters.Core.Systems.Input.UI
{
    /// <summary>
    /// Компонент, который отключает GameObject, если нет поддержки Touchscreen,
    /// и наоборот включает, если есть.
    /// </summary>
    [DisallowMultipleComponent]
    [AddComponentMenu(BattleComponentMenus.Client.Input.Menu + "/" + nameof(OnlyForTouchscreenGameObject))]
    [HelpURL("https://www.notion.so/frostgate/UI-44e0621d108743c28cd8e6e80ab5158e")]
    public class OnlyForTouchscreenGameObject : MonoBehaviour
    {
        [SerializeField] private bool _ignoreInEditor;
        private bool TouchSupported => _ignoreInEditor || EnhancedTouchSupport.enabled || TouchSimulation.instance?.enabled == true;

        private void Awake()
        {
            if (TouchSupported)
                gameObject.SetActive(true);
        }

        private void OnEnable()
        {
            if (!TouchSupported)
                gameObject.SetActive(false);
        }
    }
}