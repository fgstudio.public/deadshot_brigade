using System;
using JetBrains.Annotations;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public interface IReadOnlyPoolRepository<in TKey, TValue> where TValue : IPoolObject
    {
        IPool<TValue> this[[NotNull] TKey key] { get; }
        IPool<TValue> Get([NotNull] TKey key);
        bool Contains([NotNull] TKey key);
    }

    public interface IWriteOnlyPoolRepository<in TKey, TValue> where TValue : IPoolObject
    {
        void Add([NotNull] TKey key, [NotNull] IPool<TValue> pool);
    }

    public interface IPoolRepository<in TKey, TValue> : IDisposable,
        IReadOnlyPoolRepository<TKey, TValue>, IWriteOnlyPoolRepository<TKey, TValue>
        where TValue : IPoolObject
    { }

    public class PoolRepository<TKey, TValue> : IPoolRepository<TKey, TValue> where TValue : IPoolObject
    {
        private readonly Dictionary<TKey, IPool<TValue>> _pools;

        private bool _isDisposed;

        public PoolRepository() => _pools = new Dictionary<TKey, IPool<TValue>>();
        public PoolRepository([NotNull] Dictionary<TKey, IPool<TValue>> pools) => _pools = pools;

        public void Dispose()
        {
            if (_isDisposed) return;

            foreach (IPool<TValue> pool in _pools.Values)
                pool.Dispose();

            _pools.Clear();
            _isDisposed = true;
        }

        public IPool<TValue> this[TKey key] => Get(key);

        public IPool<TValue> Get(TKey key) => _pools[key];
        public bool Contains(TKey key) => _pools.ContainsKey(key);
        public void Add(TKey key, IPool<TValue> pool) => _pools[key] = pool;
    }
}
