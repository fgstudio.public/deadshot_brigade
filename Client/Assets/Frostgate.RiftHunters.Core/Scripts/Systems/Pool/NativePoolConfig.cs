﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public class NativePoolConfig : IPoolConfig, IDisposable
    {
        public Transform PoolContainer { get; }
        public Transform ObjectContainer { get; }
        public int InitialCount { get; }
        public int MaxCount { get; }

        private readonly GameObject _rootContainer;
        private bool _isDisposed;

        public NativePoolConfig(int initialCount, int maxCount, string containerName = "Root")
        {
            InitialCount = initialCount;
            MaxCount = maxCount;

            _rootContainer = new GameObject(containerName);
            Transform rootTransform = _rootContainer.transform;

            var poolContainer = new GameObject("Pool");
            PoolContainer = poolContainer.transform;
            PoolContainer.SetParent(rootTransform);

            var objectsContainer = new GameObject("Objects");
            ObjectContainer = objectsContainer.transform;
            ObjectContainer.SetParent(rootTransform);
        }

        public void Dispose()
        {
            ThrowIfDisposed();

            Object.Destroy(_rootContainer);
            _isDisposed = true;
        }

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException($"{nameof(NativePoolConfig)} already disposed.");
        }
    }
}