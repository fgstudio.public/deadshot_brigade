using System;
using UnityEngine;
using UnityEngine.Pool;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public class MonoPool<TPoolObject> : IPool<TPoolObject>
        where TPoolObject : MonoBehaviour, IPoolObject
    {
        private readonly Transform _poolContainer;
        private readonly Transform _objectContainer;

        private readonly ObjectPool<TPoolObject> _unityPool;

        public MonoPool([NotNull] IPrefabInstanceProvider<TPoolObject> instanceProvider, [NotNull] IPoolConfig config)
            : this(instanceProvider.Instantiate, instanceProvider.Destroy, config)
        { }

        public MonoPool([NotNull] Func<TPoolObject> createObject, [NotNull] Action<TPoolObject> destroyObject, [NotNull] IPoolConfig config)
            : this(createObject, destroyObject,
                config.PoolContainer, config.ObjectContainer,
                config.InitialCount, config.MaxCount)
        { }

        public MonoPool(
            [NotNull] Func<TPoolObject> createObject, [NotNull] Action<TPoolObject> destroyObject,
            [CanBeNull] Transform poolContainer, [CanBeNull] Transform objectContainer,
            int initialCount = 5, int maxCount = 20)
        {
            if (createObject == null) throw new ArgumentNullException(nameof(createObject));
            if (destroyObject == null) throw new ArgumentNullException(nameof(destroyObject));

            _poolContainer = poolContainer;
            _objectContainer = objectContainer;

            _unityPool = new ObjectPool<TPoolObject>
            (
                createFunc: createObject, actionOnDestroy: destroyObject,
                actionOnGet: OnGet, actionOnRelease: OnRelease,
                collectionCheck: true,
                defaultCapacity: initialCount, maxSize: maxCount
            );

            for (int i = 0; i < initialCount; i++)
                Release(createObject());
        }

        public void Dispose() => _unityPool.Dispose();

        public TPoolObject Get()
        {
            TPoolObject poolObject = GetNotNullPoolObject();
            OnGetting(poolObject);
            return poolObject;
        }

        public void Release(TPoolObject poolObject)
        {
            if (poolObject == null)
                throw new ArgumentNullException(GetType().Name, "Release Null Object");

            OnReleasing(poolObject);
            _unityPool.Release(poolObject);
        }

        protected virtual void OnGetting([NotNull] TPoolObject poolObject) { }
        protected virtual void OnReleasing([NotNull] TPoolObject poolObject) { }

        [NotNull] private TPoolObject GetNotNullPoolObject()
        {
            TPoolObject poolObject;
            int poolCapacity = _unityPool.CountInactive;

            // очистка от null-объектов внутри пула
            while (!TryGet(out poolObject))
                if (--poolCapacity < 0)
                    throw new ArgumentNullException(GetType().Name, "Null Prefab");

            return poolObject!;
        }

        private bool TryGet([CanBeNull] out TPoolObject poolObject) =>
            poolObject = _unityPool.Get();

        private void OnGet([CanBeNull] TPoolObject poolObject)
        {
            if (poolObject == null) return;

            SetParent(poolObject, _objectContainer);
            poolObject.OnGet();

            poolObject.gameObject.SetActive(true);
        }

        private void OnRelease([NotNull] TPoolObject poolObject)
        {
            poolObject.OnRelease();
            poolObject.gameObject.SetActive(false);
            SetParent(poolObject, _poolContainer);
        }

        private void SetParent([NotNull] TPoolObject poolObject, [NotNull] Transform parent)
        {
            Transform transform = poolObject.gameObject.transform;
            transform.SetParent(parent, false);
        }
    }
}
