using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    [Serializable]
    public sealed class ReferencePoolConfig : IPoolConfig
    {
        [field: SerializeField] public Transform PoolContainer { get; private set; }
        [field: SerializeField] public Transform ObjectContainer { get; private set; }
        [field: SerializeField, MinValue(0), MaxValue(nameof(MaxCount)), Delayed] public int InitialCount { get; private set; }
        [field: SerializeField, MinValue(nameof(MinMaxCount)), Delayed] public int MaxCount { get; private set; }

        private int MinMaxCount => Mathf.Max(1, InitialCount);
    }
}
