using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public abstract class MonoPoolBaseComponent<TBasePool, TPoolObject> : MonoBehaviour, IPool<TPoolObject>
        where TBasePool : IPool<TPoolObject>
        where TPoolObject : MonoBehaviour, IPoolObject
    {
        [SerializeField, HideLabel] protected ReferencePoolConfig Config;

        private TBasePool _poolCache;
        protected TBasePool Pool => _poolCache ??= CreatePool();

        private void OnDestroy() => _poolCache?.Dispose();

        [ContextMenu(nameof(Dispose))] public void Dispose() => Destroy(gameObject);

        [ContextMenu(nameof(Get))]
        public TPoolObject Get()
        {
            TPoolObject poolObject = Pool.Get();
            OnGetting(poolObject);
            return poolObject;
        }

        public void Release(TPoolObject poolObject)
        {
            OnReleasing(poolObject);
            Pool.Release(poolObject);
        }

        protected abstract TBasePool CreatePool();
        protected virtual void OnGetting(TPoolObject poolObject) { }
        protected virtual void OnReleasing(TPoolObject poolObject) { }
    }
}
