using UnityEngine;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public abstract class MonoPoolComponent<TPoolObject> : MonoPoolBaseComponent<MonoPool<TPoolObject>, TPoolObject>
        where TPoolObject : MonoBehaviour, IPoolObject
    {
        protected sealed override MonoPool<TPoolObject> CreatePool() =>
            new
            (
                createObject: () => CreateObject(GetPrefab()),
                destroyObject: DestroyObject,
                config: Config
            );

        protected abstract GameObject GetPrefab();
        protected abstract TPoolObject CreateObject(GameObject prefab);
        protected abstract void DestroyObject(TPoolObject poolObject);
    }
}
