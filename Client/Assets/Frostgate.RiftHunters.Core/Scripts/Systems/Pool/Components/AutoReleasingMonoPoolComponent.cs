using UnityEngine;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public abstract class AutoReleasingMonoPoolComponent<TPoolObject> : MonoPoolBaseComponent<AutoReleasingMonoPool<TPoolObject>, TPoolObject>
        where TPoolObject : MonoBehaviour, IPoolObject, IAutoReleased
    {
        protected sealed override AutoReleasingMonoPool<TPoolObject> CreatePool() =>
            new
            (
                createObject: () => CreateObject(GetPrefab()),
                destroyObject: DestroyObject,
                config: Config
            );

        protected abstract GameObject GetPrefab();
        protected abstract TPoolObject CreateObject(GameObject prefab);
        protected abstract void DestroyObject(TPoolObject poolObject);
    }
}
