using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    [Serializable]
    public sealed class PathPoolConfig : IPoolConfig
    {
        private const char Divider = '/';
        private const string PathTooltip = "\"/\" — разделитель";

        [SerializeField, Tooltip(PathTooltip)] private string _poolContainerPath;
        [SerializeField, Tooltip(PathTooltip)] public string _objectContainerPath;

        [field: SerializeField, MinValue(0), MaxValue(nameof(MaxCount)), Delayed] public int InitialCount { get; private set; }
        [field: SerializeField, MinValue(nameof(MinMaxCount)), Delayed] public int MaxCount { get; private set; }

        public Transform PoolContainer => _poolContainerCache ??= ResolvePath(_poolContainerPath);
        public Transform ObjectContainer => _objectContainerCache ??= ResolvePath(_objectContainerPath);

        private int MinMaxCount => Mathf.Max(1, InitialCount);

        private Transform _poolContainerCache;
        private Transform _objectContainerCache;

        [CanBeNull]
        private Transform ResolvePath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return null;

            bool createMode = false;
            string[] objectNames = path.Split(Divider);

            GameObject root = GameObject.Find(objectNames[0]);
            if (root == null)
            {
                createMode = true;
                root = new GameObject(objectNames[0]);
            }

            Transform parent = root.transform;
            for (int i = 1; i < objectNames.Length; i++)
            {
                string childName = objectNames[i];

                Transform child = createMode
                    ? CreateChildObject(childName, parent)
                    : parent.Find(childName);

                if (child == null)
                {
                    createMode = true;
                    child = CreateChildObject(childName, parent);
                }

                parent = child;
            }

            return parent;
        }

        private Transform CreateChildObject(string name, Transform parent)
        {
            var go = new GameObject(name);
            go.transform.SetParent(parent, false);
            return go.transform;
        }
    }
}
