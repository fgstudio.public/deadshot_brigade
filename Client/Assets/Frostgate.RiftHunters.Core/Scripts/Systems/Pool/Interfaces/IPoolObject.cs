namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public interface IPoolObject
    {
        void OnGet();
        void OnRelease();
    }
}
