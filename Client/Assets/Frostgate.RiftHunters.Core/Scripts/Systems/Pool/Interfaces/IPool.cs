using System;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public interface IPool<TPoolObject> : IDisposable where TPoolObject : IPoolObject
    {
        [NotNull] TPoolObject Get();
        void Release([NotNull] TPoolObject poolObject);
    }
}
