using UnityEngine.Events;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public interface IAutoReleased
    {
        event UnityAction<IAutoReleased> ReadyForRelease;
    }
}
