using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public sealed class FakePool<T> : IPool<T> where T : MonoBehaviour, IPoolObject
    {
        private readonly IPrefabInstanceProvider<T> _instanceProvider;

        public FakePool([NotNull] IPrefabInstanceProvider<T> instanceProvider) =>
            _instanceProvider = instanceProvider;

        public void Dispose() { }
        public T Get() => _instanceProvider.Instantiate();
        public void Release(T poolObject) => _instanceProvider.Destroy(poolObject);
    }
}
