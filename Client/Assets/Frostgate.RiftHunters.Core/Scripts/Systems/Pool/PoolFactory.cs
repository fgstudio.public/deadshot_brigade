﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public static class PoolFactory
    {
        public static IPool<TPrefab> CreateDefaultAutoReleasingPool<TPrefab, TConfig>(PrefabPoolConfig<TPrefab, TConfig> config)
            where TPrefab : MonoBehaviour, IPoolObject, IAutoReleased
            where TConfig : IPoolConfig
        {
            var prefabProvider = new PrefabInstanceProvider<TPrefab>(config.PrefabSource);
            return new AutoReleasingMonoPool<TPrefab>(prefabProvider, config.PoolConfig);
        }
    }
}
