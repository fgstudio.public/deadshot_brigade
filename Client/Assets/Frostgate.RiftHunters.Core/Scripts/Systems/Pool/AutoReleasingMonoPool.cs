using System;
using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public class AutoReleasingMonoPool<TPoolObject> : MonoPool<TPoolObject>
        where TPoolObject : MonoBehaviour, IPoolObject, IAutoReleased
    {
        public AutoReleasingMonoPool([NotNull] IPrefabInstanceProvider<TPoolObject> instanceProvider, [NotNull] IPoolConfig config)
            : base(instanceProvider.Instantiate, instanceProvider.Destroy, config) { }

        public AutoReleasingMonoPool(
            [NotNull] Func<TPoolObject> createObject, [NotNull] Action<TPoolObject> destroyObject, [NotNull] IPoolConfig config)
            : base(createObject, destroyObject, config) { }

        public AutoReleasingMonoPool(
            [NotNull] Func<TPoolObject> createObject, [NotNull] Action<TPoolObject> destroyObject,
            [CanBeNull] Transform poolContainer, [CanBeNull] Transform objectContainer,
            int initialCount = 5, int maxCount = 20)
            : base(createObject, destroyObject, poolContainer, objectContainer, initialCount, maxCount) { }

        protected sealed override void OnGetting(TPoolObject poolObject)
        {
            poolObject.ReadyForRelease += Release;
            OnGettingAutoReleased(poolObject);
        }

        protected sealed override void OnReleasing(TPoolObject poolObject)
        {
            poolObject.ReadyForRelease -= Release;
            OnReleasingAutoReleased(poolObject);
        }

        protected virtual void OnGettingAutoReleased(TPoolObject poolObject) { }
        protected virtual void OnReleasingAutoReleased(TPoolObject poolObject) { }

        private void Release(IAutoReleased autoReleased)
        {
            if (autoReleased is TPoolObject poolObject)
                base.Release(poolObject);
        }
    }
}
