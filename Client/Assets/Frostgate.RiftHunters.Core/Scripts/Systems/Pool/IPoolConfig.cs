﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public interface IPoolConfig
    {
        Transform PoolContainer { get; }
        Transform ObjectContainer { get; }
        int InitialCount { get; }
        int MaxCount { get; }
    }
}