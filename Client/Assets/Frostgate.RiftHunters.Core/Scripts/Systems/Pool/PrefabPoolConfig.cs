using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    [Serializable]
    public sealed class PrefabPoolConfig<TPrefabSource, TPoolConfig>
        where TPoolConfig : IPoolConfig
    {
        [field: SerializeField, Required] public TPrefabSource PrefabSource { get; private set; }
        [field: SerializeField, Required, HideLabel] public TPoolConfig PoolConfig { get; private set; }
    }
}
