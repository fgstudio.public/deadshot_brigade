using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public sealed class PrefabInstanceProvider<TInstance> : IPrefabInstanceProvider<TInstance> where TInstance : MonoBehaviour
    {
        private readonly TInstance _prefab;

        public PrefabInstanceProvider([NotNull] GameObject prefab) : this(prefab.GetComponent<TInstance>()) { }
        public PrefabInstanceProvider([NotNull] TInstance prefab) => _prefab = prefab;

        public TInstance Instantiate() => Object.Instantiate(_prefab).GetComponent<TInstance>();
        public void Destroy(TInstance instance) => Object.Destroy(instance.gameObject);
    }
}
