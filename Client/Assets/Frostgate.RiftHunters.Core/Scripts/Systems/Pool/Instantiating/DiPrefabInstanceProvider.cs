using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public sealed class DiPrefabInstanceProvider<TInstance> : IPrefabInstanceProvider<TInstance> where TInstance : MonoBehaviour
    {
        private readonly TInstance _prefab;
        private readonly IPrefabFactory _prefabFactory;

        public DiPrefabInstanceProvider([NotNull] GameObject prefab, [NotNull] IPrefabFactory prefabFactory)
            : this(prefab.GetComponent<TInstance>(), prefabFactory) { }

        public DiPrefabInstanceProvider([NotNull] TInstance prefab, [NotNull] IPrefabFactory prefabFactory)
        {
            _prefab = prefab;
            _prefabFactory = prefabFactory;
        }

        public TInstance Instantiate() => _prefabFactory.Instantiate<TInstance>(_prefab);
        public void Destroy(TInstance instance) => Object.Destroy(instance.gameObject);
    }
}
