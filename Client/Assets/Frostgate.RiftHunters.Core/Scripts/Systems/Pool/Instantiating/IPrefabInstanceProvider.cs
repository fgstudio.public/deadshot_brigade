using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core.Systems.Pool
{
    public interface IPrefabInstanceProvider<TInstance> where TInstance : MonoBehaviour
    {
        TInstance Instantiate();
        void Destroy([NotNull] TInstance instance);
    }
}
