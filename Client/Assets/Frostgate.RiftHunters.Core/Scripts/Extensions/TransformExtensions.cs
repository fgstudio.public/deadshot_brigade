using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public static class TransformExtensions
    {
        public static Transform GetRecursiveParent(this Transform transform, int recursiveLevel)
        {
            if (recursiveLevel < 0)
                throw new ArgumentOutOfRangeException();

            int index = 0;
            Transform result = transform;

            while (index < recursiveLevel && result != null)
            {
                index++;
                result = result.parent;
            }

            return result;
        }
    }
}
