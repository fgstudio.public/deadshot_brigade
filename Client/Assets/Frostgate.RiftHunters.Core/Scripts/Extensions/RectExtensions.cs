using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public static class RectExtensions
    {
        public static Vector2 ClampPosition(this Rect rect, Vector2 position)
        {
            Vector2 halfSize = rect.size / 2f;

            position.x = Mathf.Clamp(position.x, -halfSize.x, halfSize.x);
            position.y = Mathf.Clamp(position.y, -halfSize.y, halfSize.y);

            return position;
        }
    }
}
