﻿using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public static class VectorExtensions
    {
        public static Vector3 RotateY(this Vector3 vec, float angle)
        {
            float sin = Mathf.Sin(Mathf.Deg2Rad * angle);
            float cos = Mathf.Cos(Mathf.Deg2Rad * angle);

            return new Vector3
            (
                x: cos * vec.x + sin * vec.z,
                y: vec.y,
                z: -sin * vec.x + cos * vec.z
            );
        }

        public static Vector2 Rotate(this Vector2 vec, float angle)
        {
            float sin = Mathf.Sin(Mathf.Deg2Rad * angle);
            float cos = Mathf.Cos(Mathf.Deg2Rad * angle);

            return new Vector2(
                vec.x * cos - vec.y * sin,
                vec.x * sin + vec.y * cos
            );
        }

        public static Vector3 RotateAroundPivot(this Vector3 point, Vector3 pivot, Quaternion angle) =>
            angle * (point - pivot) + pivot;

        public static Vector3 RotateAroundPivot(this Vector3 point, Vector3 pivot, Vector3 euler) =>
            RotateAroundPivot(point, pivot, Quaternion.Euler(euler));
    }
}