using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public static class Vector2Extensions
    {
        public static Vector2 SetX(this Vector2 vector, float xValue)
        {
            vector.x = xValue;
            return vector;
        }

        public static Vector2 SetY(this Vector2 vector, float yValue)
        {
            vector.y = yValue;
            return vector;
        }

        public static Vector3 AsVector3WithY(this Vector2 vector, float yValue) =>
            new Vector3(vector.x, yValue, vector.y);
    }
}