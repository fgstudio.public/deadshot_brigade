using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public static class LayerMaskExtensions
    {
        public static bool HasLayer(this LayerMask layerMask, int layer) =>
            layerMask == (layerMask | (1 << layer));
    }
}
