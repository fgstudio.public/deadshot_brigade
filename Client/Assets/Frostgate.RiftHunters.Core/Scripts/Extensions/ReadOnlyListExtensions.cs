using System;
using JetBrains.Annotations;
using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public static class ReadOnlyListExtensions
    {
        public static bool HasIndex<T>([NotNull] this IReadOnlyList<T> list, int index) =>
            index >= 0 && index < list.Count;

        public static bool TryGet<T>([NotNull] this IReadOnlyList<T> list, int index, out T element)
        {
            bool hasIndex = list.HasIndex(index);
            element = hasIndex ? list[index] : default;
            return hasIndex;
        }

        [CanBeNull]
        public static T GetSafe<T>([NotNull] this IReadOnlyList<T> list, int index)
        {
            list.TryGet(index, out T element);
            return element;
        }

        public static bool TryGetNearest<TItem>([NotNull] this IReadOnlyList<TItem> list, int key,
            [NotNull] Func<TItem, int> getKey, out TItem item)
        {
            item = default;
            if (list.Count == 0)
                return false;

            if (list.Count == 1)
            {
                item = list[0];
                return true;
            }

            int minKeyDelta = int.MaxValue;
            foreach (TItem it in list)
            {
                int keyDelta = Mathf.Abs(key - getKey.Invoke(it));
                if (keyDelta < minKeyDelta)
                {
                    minKeyDelta = keyDelta;
                    item = it;
                }
            }

            return true;
        }

        public static bool TryGetNearest<TItem>([NotNull] this IReadOnlyList<TItem> list, float key,
            [NotNull] Func<TItem, float> getKey, out TItem item)
        {
            item = default;
            if (list.Count == 0)
                return false;

            if (list.Count == 1)
            {
                item = list[0];
                return true;
            }

            float minKeyDelta = float.MaxValue;
            foreach (TItem it in list)
            {
                float keyDelta = Mathf.Abs(key - getKey.Invoke(it));
                if (keyDelta < minKeyDelta)
                {
                    minKeyDelta = keyDelta;
                    item = it;
                }
            }

            return true;
        }
    }
}