using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public static class Vector3Extensions
    {
        public static Vector3 SetX(this Vector3 vector, float value)
        {
            vector.x = value;
            return vector;
        }

        public static Vector3 SetY(this Vector3 vector, float value)
        {
            vector.y = value;
            return vector;
        }

        public static Vector3 SetZ(this Vector3 vector, float value)
        {
            vector.z = value;
            return vector;
        }

        public static Vector2 CutY(this Vector3 vector) => new(vector.x, vector.z);
        public static Vector2 CutZ(this Vector3 vector) => new(vector.x, vector.y);
    }
}
