using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public static class CollectionExtensions
    {
        public static T GetRandom<T>(this IReadOnlyList<T> list)
        {
            if (list == null || list.Count == 0)
                return default;

            var index = Random.Range(0, list.Count);
            return list[index];
        }
    }
}