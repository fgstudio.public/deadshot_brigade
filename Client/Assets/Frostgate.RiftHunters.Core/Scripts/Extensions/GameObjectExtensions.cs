using System;
using UnityEngine;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public static class GameObjectExtensions
    {
        public static bool IsPrefab(this GameObject go)
        {
#if UNITY_EDITOR
            return UnityEditor.PrefabUtility.GetPrefabAssetType(go)
                   != UnityEditor.PrefabAssetType.NotAPrefab;
#else
            return go.gameObject.scene.buildIndex < 0;
#endif
        }

        public static GameObject Find([NotNull] this GameObject go, string name) =>
            go.transform.Find(name)?.gameObject;

        public static GameObject FindRecursively([NotNull] this GameObject go, ReadOnlySpan<string> names)
        {
            if (names.Length == 0)
                return go;

            GameObject child = go.Find(names[0]);
            if (child == null || names.Length == 1)
                return child;

            return child.FindRecursively(names.Slice(1));
        }
    }
}
