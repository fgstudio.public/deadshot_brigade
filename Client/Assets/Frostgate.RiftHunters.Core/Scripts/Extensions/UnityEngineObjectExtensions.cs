using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Frostgate.RiftHunters.Core
{
    public static class UnityEngineObjectExtensions
    {
        public static IEnumerable<Transform> GetChildren(this Transform transform, bool onlyActive = false)
        {
            foreach (Transform child in transform)
                if (!onlyActive || child.gameObject.activeSelf)
                    yield return child.transform;
        }

        public static IEnumerable<Transform> GetAllChildren(this Transform transform, bool onlyActive = false)
        {
            foreach (var child in transform.GetChildren(onlyActive))
            {
                // сначала сложим чайлдов, потом положим себя
                // важно, например, если потом захотим удалять объекты по этому списку
                foreach (var item in child.transform.GetAllChildren(onlyActive))
                    yield return item;

                yield return child.transform;
            }
        }

        public static void DestroyImmediateAllChildren(this GameObject obj, bool onlyActive = false)=>
            obj.transform.GetAllChildren(onlyActive).ToList().DestroyImmediate();

        public static void DestroyImmediate(this IEnumerable<UnityEngine.Component> objs)
        {
            foreach (var obj in objs)
                obj.DestroyImmediate();
        }

        public static void DestroyImmediate(this UnityEngine.Component obj)
        {
            if (obj != null)
                Object.DestroyImmediate(obj.gameObject);
        }
    }
}