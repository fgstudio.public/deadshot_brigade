﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Zenject;
using Object = UnityEngine.Object;

namespace Frostgate.RiftHunters.Core
{
    public static class DiContainerExtensions
    {
        public static async UniTask DisposeAllServicesAsync(this DiContainer container)
        {
            container.DisposeDisposableServices();
            await container.DisposeAsyncDisposablesServicesAsync();
        }

        public static void DisposeDisposableServices(this DiContainer container)
        {
            IEnumerable<IDisposable> disposables = container.ResolveAll<IDisposable>();
            foreach (var disposable in disposables)
                disposable.Dispose();
        }

        public static async UniTask DisposeAsyncDisposablesServicesAsync(this DiContainer container)
        {
            IEnumerable<IAsyncDisposable> asyncDisposables = container.ResolveAll<IAsyncDisposable>();
            foreach (var asyncDisposable in asyncDisposables)
                await asyncDisposable.DisposeAsync();
        }

        public static T InstantiateObject<T>(this DiContainer container, T obj) where T : Object
        {
            T instance = Object.Instantiate(obj);
            container.Inject(instance);

            return instance;
        }

        public static void BindObjectCollection(this DiContainer container, IEnumerable<object> objects,
            Action<ScopeConcreteIdArgConditionCopyNonLazyBinder> objectLifetime)
        {
            foreach (var o in objects)
            {
                ScopeConcreteIdArgConditionCopyNonLazyBinder binding = container.Bind(o.GetType()).FromInstance(o);
                objectLifetime.Invoke(binding);
            }
        }
    }
}
