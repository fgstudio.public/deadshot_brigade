using System;

namespace Frostgate.RiftHunters.Core
{
    public static class ComparableExtensions
    {
        public static bool IsContained<T>(this IComparable<T> value, T lessInclusive, T moreInclusive) =>
            value.CompareTo(lessInclusive) >= 0 && value.CompareTo(moreInclusive) <= 0;
    }
}
