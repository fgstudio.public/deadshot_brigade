using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public static class UnityComponentExtensions
    {
        public static bool TryGetComponentInHierarchy<TComponent>([NotNull] this UnityEngine.Component original, out TComponent target)
        {
            target = original.GetComponentInHierarchy<TComponent>();
            return target != null;
        }

        public static TComponent GetComponentInHierarchy<TComponent>([NotNull] this UnityEngine.Component original) =>
            original.GetComponent<TComponent>() ??
            original.GetComponentInChildren<TComponent>(true) ??
            original.GetComponentInParent<TComponent>(true);
    }
}
