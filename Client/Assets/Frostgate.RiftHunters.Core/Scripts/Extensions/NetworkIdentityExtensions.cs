using Mirror;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public static class NetworkIdentityExtensions
    {
        public static bool TrySafeGetComponent<TComponent>(
            [CanBeNull] this NetworkIdentity identity, out TComponent component)
        {
            if (identity != null && identity.gameObject != null)
                return identity.TryGetComponent(out component);

            component = default;
            return false;
        }
    }
}
