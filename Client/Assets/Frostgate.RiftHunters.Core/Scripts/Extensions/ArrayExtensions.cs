namespace Frostgate.RiftHunters.Core
{
    public static class ArrayExtensions
    {
        public static void Swap<T>(this T[] array, int index1, int index2)
        {
            T element1 = array[index1];
            T element2 = array[index2];

            array[index1] = element2;
            array[index2] = element1;
        }
    }
}
