using System.Collections.Generic;

namespace Frostgate.RiftHunters.Core
{
    public static class ObjectExtensions
    {
        public static IEnumerable<T> AsEnumerable<T>(this T obj)
        {
            yield return obj;
        }
    }
}
