﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public static class EnumerableExtensions
    {
        [NotNull]
        public static IEnumerable<T> EmptyIfNull<T>([CanBeNull] this IEnumerable<T> enumerable) =>
            enumerable ?? Array.Empty<T>();

        public static void ForEach<T>([NotNull] this IEnumerable<T> enumerable, [NotNull] Action<T> each)
        {
            foreach (T e in enumerable)
                each.Invoke(e);
        }

        public static int IndexOf<T>(this IEnumerable<T> source, T element)
            where T : IEquatable<T>
        {
            int index = 0;

            foreach (T item in source)
                if (item.Equals(element))
                    return index;
                else
                    index++;

            return -1;
        }

        public static int IndexOf<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            int index = 0;

            foreach (T item in source)
                if (predicate.Invoke(item))
                    return index;
                else
                    index++;

            return -1;
        }
    }
}
