﻿using System;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Frostgate.RiftHunters.Core
{
    [Serializable]
    public sealed class SceneLoadingData : ISceneLoadingData
    {
#if UNITY_EDITOR
        [SerializeField, OnValueChanged(nameof(OnSceneValueChanged))]
        private SceneAsset _scene;
#endif

        [SerializeField, ReadOnly] private string _name;
        [SerializeField] private LoadSceneMode _mode;
        [SerializeField] private bool _setActive = true;

        public string Name => _name;
        public LoadSceneMode Mode => _mode;
        public bool SetActive => _setActive;

#if UNITY_EDITOR
        private void OnSceneValueChanged()
        {
            _name = _scene.name;
        }
#endif
    }
}