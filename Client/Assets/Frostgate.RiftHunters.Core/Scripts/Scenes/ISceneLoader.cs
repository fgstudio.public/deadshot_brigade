﻿using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine.SceneManagement;

namespace Frostgate.RiftHunters.Core
{
    public interface ISceneLoader
    {
        UniTask LoadSceneAsync([NotNull] ISceneLoadingData data);
        UniTask LoadSceneAsync(string name, LoadSceneMode mode, bool isActive = true);
        UniTask UnloadSceneAsync([NotNull] ISceneLoadingData data);
        UniTask UnloadSceneAsync(string name);
    }
}