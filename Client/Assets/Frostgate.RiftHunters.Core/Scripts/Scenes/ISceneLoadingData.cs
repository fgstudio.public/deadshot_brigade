﻿using UnityEngine.SceneManagement;

namespace Frostgate.RiftHunters.Core
{
    public interface ISceneLoadingData
    {
        string Name { get; }
        LoadSceneMode Mode { get; }
        bool SetActive { get; }
    }
}