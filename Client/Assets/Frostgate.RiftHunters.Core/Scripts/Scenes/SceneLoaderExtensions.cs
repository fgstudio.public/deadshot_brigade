﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Battle;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Core
{
    public static class SceneLoaderExtensions
    {
        public static async UniTask LoadMissionAsync([NotNull] this ISceneLoader loader,
            [NotNull] IMissionConfig missionConfig)
        {
            foreach (ISceneLoadingData sceneData in missionConfig.Scenes)
                await loader.LoadSceneAsync(sceneData);
        }
    }
}