﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine.AddressableAssets;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Definitions
{
    public static class AddressableConfigLoader
    {
        public static async UniTask<IList<IConfig>> LoadConfigsAsync([NotNull] AssetLabelReference configsLabel,
            [NotNull] Action<IConfig> onLoadCallback)
        {
            var keys = (IEnumerable<object>)new object[] { configsLabel.labelString };
            IList<IConfig> configs = await Addressables
                .LoadAssetsAsync(keys, onLoadCallback, Addressables.MergeMode.None).ToUniTask();

            return configs ?? Array.Empty<IConfig>();
        }
    }
}