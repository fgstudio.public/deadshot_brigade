﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Definitions
{
    public interface IConfigRepository
    {
        UniTask InitializeAsync();
        void Add([NotNull] IConfig config);
        IReadOnlyDictionary<string, T> GetByType<T>() where T : IConfig;
    }
}