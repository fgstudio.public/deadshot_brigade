﻿using UnityEngine.AddressableAssets;

namespace Frostgate.RiftHunters.Infrastructure.Definitions
{
    public interface IConfigLabelProvider
    {
        AssetLabelReference GetLabel();
    }
}