﻿using Frostgate.RiftHunters.Core;
using System.Linq;

namespace Frostgate.RiftHunters.Infrastructure.Definitions
{
    public class ConfigAccessor<TConfig> : IConfigAccessor<TConfig> where TConfig : IConfig
    {
        public TConfig Config { get; }

        public ConfigAccessor(IConfigCollection<TConfig> configs)
        {
            Config = configs.First();
        }
    }
}
