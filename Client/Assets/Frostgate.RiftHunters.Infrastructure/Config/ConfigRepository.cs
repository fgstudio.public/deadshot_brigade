﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using JetBrains.Annotations;
using Zenject;

namespace Frostgate.RiftHunters.Infrastructure.Definitions
{
    public sealed class ConfigRepository : IConfigRepository
    {
        [NotNull] private readonly IConfigLabelProvider _defsLabelProvider;
        [NotNull] private readonly DiContainer _container = new();

        public ConfigRepository([NotNull] IConfigLabelProvider defsLabelProvider)
        {
            _defsLabelProvider = defsLabelProvider;
        }

        public UniTask InitializeAsync() =>
            AddressableConfigLoader.LoadConfigsAsync(_defsLabelProvider.GetLabel(), Add);

        public void Add(IConfig config)
        {
            Type configType = config.GetType();
            _container.BindInterfacesAndSelfTo(configType).FromInstance(config);

            configType = configType.BaseType;
            while (configType != null && configType.GetInterfaces().Contains(typeof(IConfig)))
            {
                _container.Bind(configType).FromInstance(config);
                configType = configType.BaseType;
            }
        }

        public IReadOnlyDictionary<string, T> GetByType<T>() where T : IConfig =>
            _container.ResolveAll<T>().ToDictionary(c => c.Id);
    }
}