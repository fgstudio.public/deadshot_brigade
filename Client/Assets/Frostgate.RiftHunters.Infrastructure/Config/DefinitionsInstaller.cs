﻿using Zenject;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Definitions
{
    public sealed class DefinitionsInstaller : Installer
    {
        public override void InstallBindings()
        {
            Container.Bind<IConfigRepository>().To<ConfigRepository>().AsSingle();
            Container.Bind(typeof(IConfigAccessor<>)).To(typeof(ConfigAccessor<>)).AsSingle();
            Container.Bind(typeof(IConfigCollection<>)).To(typeof(ConfigCollection<>)).AsSingle();
        }
    }
}