﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Definitions
{
    public class ConfigCollection<TConfig> : IConfigCollection<TConfig> where TConfig : IConfig
    {
        public int Count => _configs.Count;

        private readonly IReadOnlyDictionary<string, TConfig> _configs;

        public ConfigCollection([NotNull] IConfigRepository configRepository)
        {
            _configs = configRepository.GetByType<TConfig>();
        }

        public bool Contains(string id) => _configs.ContainsKey(id);

        public bool TryGet(string id, out TConfig config) => _configs.TryGetValue(id, out config);

        public TConfig this[string id] => _configs[id];

        public IEnumerator<TConfig> GetEnumerator() => _configs.Values.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}