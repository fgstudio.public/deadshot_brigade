﻿using Zenject;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Infrastructure.Http;
using Frostgate.RiftHunters.Infrastructure.Scenes;
using Frostgate.RiftHunters.Infrastructure.Services;
using Frostgate.RiftHunters.Infrastructure.Vibration;
using Frostgate.RiftHunters.Infrastructure.Definitions;
using Frostgate.RiftHunters.Infrastructure.ObjectFactory;
using Frostgate.RiftHunters.Infrastructure.GameStateMachine;

namespace Frostgate.RiftHunters.Infrastructure
{
    public sealed class InfrastructureInstaller : Installer
    {
        [NotNull] private readonly Preferences _preferences;

        public InfrastructureInstaller([NotNull] Preferences preferences)
        {
            _preferences = preferences;
        }

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<Preferences>().FromInstance(_preferences);

            Container.Install<DefinitionsInstaller>();
            Container.Install<GameStateMachineInstaller>();
            Container.Install<ObjectFactoryInstaller>();
            Container.Install<ScenesInstaller>();
            Container.Install<HttpInstaller>();
            Container.Install<VibrationInstaller>();
            Container.Install<ServicesInstaller>();
        }
    }
}