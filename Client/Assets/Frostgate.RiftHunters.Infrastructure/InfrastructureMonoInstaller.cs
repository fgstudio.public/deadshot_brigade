﻿using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Infrastructure
{
    public sealed class InfrastructureMonoInstaller : MonoInstaller
    {
        [SerializeField, AssetSelector, AssetsOnly]
        private Preferences _preferences;

        public override void InstallBindings() =>
            Container.Install<InfrastructureInstaller>(new object[] { _preferences });
    }
}