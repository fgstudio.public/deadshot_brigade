﻿using Frostgate.RiftHunters.Core.UI;
using Frostgate.RiftHunters.Core.UI.Toasters;
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Infrastructure.UI
{
    [DisallowMultipleComponent]
    public sealed class UIInstaller : MonoInstaller
    {
        [SerializeField, Required] private RectTransform _canvasTransform;
        [SerializeField, Required] private UITooltipService _tooltipService;
        [SerializeField, Required] private UIToastService toastService;

        public override void InstallBindings()
        {
            var loader = new AddressableScreenPrefabLoader();
            Container.Bind<IUIService>().To<UIService>().AsSingle().WithArguments(_canvasTransform, loader);
            Container.Bind<ITooltipService>().To<UITooltipService>().FromInstance(_tooltipService);
            Container.Bind<IToastService>().To<UIToastService>().FromInstance(toastService);
        }
    }
}
