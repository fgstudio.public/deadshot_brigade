﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.UI;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Frostgate.RiftHunters.Infrastructure.UI
{
    public sealed class AddressableScreenPrefabLoader : IScreenPrefabLoader
    {
        public UniTask<GameObject> LoadPrefabAsync(string address) =>
            Addressables.LoadAssetAsync<GameObject>(address).ToUniTask();
    }
}