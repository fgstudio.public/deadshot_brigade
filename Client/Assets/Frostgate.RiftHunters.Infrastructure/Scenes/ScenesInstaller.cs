﻿using Zenject;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Scenes
{
    public sealed class ScenesInstaller : Installer
    {
        public override void InstallBindings()
        {
            Container.Bind<ISceneLoader>().To<AddressableSceneLoader>().AsSingle();
        }
    }
}
