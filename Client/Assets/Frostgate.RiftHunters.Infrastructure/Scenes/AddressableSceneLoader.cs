﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using Frostgate.RiftHunters.Core;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Infrastructure.Scenes
{
    public sealed class AddressableSceneLoader : ISceneLoader
    {
        [NotNull] private readonly ILogger<AddressableSceneLoader> _logger;

        [NotNull] private readonly IDictionary<string, SceneInstance> _loadedScenes =
            new Dictionary<string, SceneInstance>();

        public AddressableSceneLoader([NotNull] ILogger<AddressableSceneLoader> logger)
        {
            _logger = logger;
        }

        /*public AsyncOperationHandle<SceneInstance> LoadSceneAsync(string name,
            LoadSceneMode mode = LoadSceneMode.Additive, bool activateOnLoad = true) =>
            Addressables.LoadSceneAsync(name, mode, activateOnLoad);

        public AsyncOperationHandle<SceneInstance> LoadSceneAsync(AssetReference sceneRef,
            LoadSceneMode mode = LoadSceneMode.Additive,
            bool activateOnLoad = true)
        {
            AsyncOperationHandle<SceneInstance> handle = sceneRef.LoadSceneAsync(mode, activateOnLoad);
            handle.Completed += OnLoadingHandleCompleted;

            return handle;
        }

        public AsyncOperationHandle UnloadSceneAsync(SceneInstance scene) => Addressables.UnloadSceneAsync(scene);

        private void OnLoadingHandleCompleted(AsyncOperationHandle<SceneInstance> handle)
        {
            Scene loadedScene = handle.Result.Scene;
            SceneManager.SetActiveScene(loadedScene);
        }*/


        public UniTask LoadSceneAsync(ISceneLoadingData data) =>
            LoadSceneAsync(data.Name, data.Mode, data.SetActive);

        public async UniTask LoadSceneAsync(string name, LoadSceneMode mode, bool isActive = true)
        {
            if (mode == LoadSceneMode.Single)
                _loadedScenes.Clear();

            AsyncOperationHandle<SceneInstance> handle = Addressables.LoadSceneAsync(name, mode);
            await WaitHandle(handle);

            SceneInstance instance = handle.Result;
            _loadedScenes[name] = instance;

            if (isActive)
                SetActive(instance);
        }

        public UniTask UnloadSceneAsync(ISceneLoadingData data) => UnloadSceneAsync(data.Name);

        public UniTask UnloadSceneAsync(string name)
        {
            if (!_loadedScenes.TryGetValue(name, out SceneInstance instance))
                _logger.LogWarning($"Scene instance for '{name}' not found.");

            return UnloadSceneAsync(instance);
        }

        public UniTask UnloadSceneAsync(SceneInstance instance)
        {
            AsyncOperationHandle handle = Addressables.UnloadSceneAsync(instance);

            return WaitHandle(handle);
        }

        private UniTask WaitHandle(AsyncOperationHandle handle) => UniTask.WaitWhile(() => handle.IsDone == false);

        private void SetActive(SceneInstance instance)
        {
            Scene scene = instance.Scene;
            SceneManager.SetActiveScene(scene);
        }
    }
}