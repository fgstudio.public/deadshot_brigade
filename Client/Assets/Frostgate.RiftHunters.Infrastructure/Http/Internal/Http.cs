﻿using System.Net.Http;
using System.Threading;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Web;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Http
{
    public sealed class Http : IHttp
    {
        private readonly HttpClient _client = new();

        public async UniTask<IHttpResponse<TResponse>> SendAsync<TRequest, TResponse>(string requestUri,
            TRequest request,
            HttpMethod method,
            CancellationToken token = default)
        {
            HttpResponseMessage responseMessage = await SendInternalAsync(requestUri, request, method, token);

            return await CreateResponseAsync<TResponse>(responseMessage);
        }

        public async UniTask<IHttpResponse> SendAsync<TRequest>(string requestUri, TRequest request, HttpMethod method,
            CancellationToken token = default)
        {
            HttpResponseMessage responseMessage = await SendInternalAsync(requestUri, request, method, token);

            return CreateResponse(responseMessage);
        }

        private async UniTask<HttpResponseMessage> SendInternalAsync<TRequest>(string requestUri, TRequest request,
            HttpMethod method,
            CancellationToken token)
        {
            HttpRequestMessage requestMessage;
            if (method == HttpMethod.Get)
            {
                string query = CreateUrlQuery(request);
                requestMessage = new HttpRequestMessage(method, requestUri + "?" + query);
            }
            else
            {
                requestMessage = new HttpRequestMessage(method, requestUri);
                string messageBody = JsonConvert.SerializeObject(request);
                requestMessage.Content = new StringContent(messageBody, Encoding.UTF8, MediaTypeNames.Application.Json);
            }

            HttpResponseMessage responseMessage = await _client.SendAsync(requestMessage, token);

            return responseMessage;
        }

        private HttpResponse CreateResponse(HttpResponseMessage message)
        {
            var response = new HttpResponse
            {
                Status = message.StatusCode,
                IsSuccess = message.IsSuccessStatusCode
            };

            return response;
        }

        private async UniTask<HttpResponse<TResponse>> CreateResponseAsync<TResponse>(HttpResponseMessage message)
        {
            var response = CreateResponse(message);
            var responseBody = await message.Content.ReadAsStringAsync();

            return new HttpResponse<TResponse>(response)
            {
                Response = response.IsSuccess ? JsonConvert.DeserializeObject<TResponse>(responseBody) : default
            };
        }

        private string CreateUrlQuery<TRequest>(TRequest request)
        {
            var props = from prop in typeof(TRequest).GetProperties(BindingFlags.Public)
                where prop.GetValue(request, null) != null
                select prop.Name + "=" + HttpUtility.UrlEncode(prop.GetValue(request, null).ToString());

            var fields = from field in typeof(TRequest).GetFields(BindingFlags.Public)
                where field.GetValue(request) != null
                select field.Name + "=" + HttpUtility.UrlEncode(field.GetValue(request).ToString());

            string query = string.Join("&", props.Concat(fields).ToArray());

            return query;
        }

        public void Dispose() => _client.Dispose();
    }
}
