﻿using System.Net;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Http
{
    public class HttpResponse : IHttpResponse
    {
        public virtual bool IsSuccess { get; set; }
        public virtual HttpStatusCode Status { get; set; }
    }

    public sealed class HttpResponse<TResponse> : HttpResponse, IHttpResponse<TResponse>
    {
        public override bool IsSuccess => _response.IsSuccess;
        public override HttpStatusCode Status => _response.Status;
        public TResponse Response { get; set; }

        [NotNull] readonly HttpResponse _response;

        public HttpResponse([NotNull] HttpResponse response)
        {
            _response = response;
        }
    }
}
