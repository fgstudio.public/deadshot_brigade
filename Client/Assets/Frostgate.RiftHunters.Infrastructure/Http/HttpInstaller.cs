﻿using Frostgate.RiftHunters.Core;
using Zenject;

namespace Frostgate.RiftHunters.Infrastructure.Http
{
    public sealed class HttpInstaller : Installer
    {
        public override void InstallBindings() => Container.Bind<IHttp>().To<Http>().AsSingle();
    }
}
