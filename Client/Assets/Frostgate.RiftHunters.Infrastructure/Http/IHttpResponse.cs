﻿using System.Net;

namespace Frostgate.RiftHunters.Infrastructure.Http
{
    public interface IHttpResponse
    {
        bool IsSuccess { get; }
        HttpStatusCode Status { get; }
    }

    public interface IHttpResponse<out TResponse> : IHttpResponse
    {
        TResponse Response { get; }
    }
}
