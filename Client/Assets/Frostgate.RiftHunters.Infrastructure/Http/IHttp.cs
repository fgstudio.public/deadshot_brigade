﻿using System;
using System.Net.Http;
using System.Threading;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Infrastructure.Http
{
    public interface IHttp : IDisposable
    {
        UniTask<IHttpResponse<TResponse>> SendAsync<TRequest, TResponse>(string requestUri, TRequest request,
            HttpMethod method,
            CancellationToken token = default);

        UniTask<IHttpResponse> SendAsync<TRequest>(string requestUri, TRequest request, HttpMethod method,
            CancellationToken token = default);
    }
}
