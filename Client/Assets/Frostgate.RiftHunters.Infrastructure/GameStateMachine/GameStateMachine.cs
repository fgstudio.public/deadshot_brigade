﻿using Frostgate.RiftHunters.Core;
using System;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Infrastructure.GameStateMachine
{
    public sealed class GameStateMachine : IGameStateMachine
    {
        [CanBeNull] private IExitableState _currentState;
        private readonly IGameStateProvider _stateProvider;

        public GameStateMachine(IGameStateProvider stateProvider)
        {
            _stateProvider = stateProvider;
        }

        public async UniTask EnterAsync<TState>() where TState : IState
        {
            TState state = await ChangeStateAsync<TState>();
            await state.EnterAsync();
        }

        public async UniTask EnterAsync<TState, TArg>(TArg arg) where TState : IState<TArg>
        {
            TState state = await ChangeStateAsync<TState>();
            await state.EnterAsync(arg);
        }

        public async UniTask EnterAsync<TState>(params object[] scopeDependencies) where TState : IState
        {
            TState state = await ChangeStateAsync<TState>(scopeDependencies);
            await state.EnterAsync();
        }

        public async UniTask EnterAsync<TState, TArg>(TArg arg, params object[] scopeDependencies)
            where TState : IState<TArg>
        {
            TState state = await ChangeStateAsync<TState>(scopeDependencies);
            await state.EnterAsync(arg);
        }

        private UniTask<TState> ChangeStateAsync<TState>() where TState : IExitableState =>
            ChangeStateAsync(_stateProvider.GetState<TState>);

        private UniTask<TState> ChangeStateAsync<TState>(params object[] scopeDependencies)
            where TState : IExitableState =>
            ChangeStateAsync(() => _stateProvider.GetScopedState<TState>(scopeDependencies));

        private async UniTask<TState> ChangeStateAsync<TState>(Func<TState> getState) where TState : IExitableState
        {
            await ExitCurrentStateAsync();

            TState nextState = getState.Invoke();
            _currentState = nextState;

            return nextState;
        }

        private UniTask ExitCurrentStateAsync() => _currentState?.ExitAsync() ?? UniTask.CompletedTask;
    }
}
