﻿using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Infrastructure.GameStateMachine.Internal;
using Zenject;

namespace Frostgate.RiftHunters.Infrastructure.GameStateMachine
{
    public sealed class GameStateMachineInstaller : Installer
    {
        public override void InstallBindings()
        {
            Container.Bind<IGameStateProvider>().To<ZenjectGameStateProvider>().AsSingle();
            Container.Bind<IGameStateMachine>().To<GameStateMachine>().AsSingle();
        }
    }
}
