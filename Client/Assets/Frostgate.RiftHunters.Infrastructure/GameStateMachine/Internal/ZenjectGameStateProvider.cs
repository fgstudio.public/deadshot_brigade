﻿using Frostgate.RiftHunters.Core;
using Zenject;

namespace Frostgate.RiftHunters.Infrastructure.GameStateMachine.Internal
{
    public sealed class ZenjectGameStateProvider : IGameStateProvider
    {
        private readonly DiContainer _container;

        public ZenjectGameStateProvider(DiContainer container)
        {
            _container = container;
        }

        public TState GetState<TState>() where TState : IExitableState => GetState<TState>(_container);

        public TState GetScopedState<TState>(params object[] scopedDependencies) where TState : IExitableState
        {
            var diScope = new DiContainer(_container);
            diScope.BindObjectCollection(scopedDependencies, binder => binder.AsSingle());

            return GetState<TState>(diScope);
        }

        private TState GetState<TState>(DiContainer container) where TState : IExitableState =>
            container.Instantiate<TState>();
    }
}
