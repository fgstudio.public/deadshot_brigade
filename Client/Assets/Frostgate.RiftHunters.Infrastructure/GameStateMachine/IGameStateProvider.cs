﻿using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.GameStateMachine
{
    public interface IGameStateProvider
    {
        public TState GetState<TState>() where TState : IExitableState;
        public TState GetScopedState<TState>(params object[] scopedDependencies) where TState : IExitableState;
    }
}
