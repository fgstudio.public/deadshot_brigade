﻿using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration
{
    public sealed class StubVibrator : IVibrator
    {
        private readonly IVibration _stubVibration = new StubVibration();

        public IVibration Vibrate(int durationMs) => _stubVibration;
        public IVibration Vibrate(int durationMs, byte intensity) => _stubVibration;
        public IVibration Vibrate(IVibrationPattern pattern) => _stubVibration;

        public void Cancel()
        {
        }
    }
}