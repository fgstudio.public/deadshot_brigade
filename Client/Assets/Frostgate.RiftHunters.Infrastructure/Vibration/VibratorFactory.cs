﻿using JetBrains.Annotations;
using UnityEngine;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration
{
    public static class VibratorFactory
    {
        public static IVibrator Create() =>
            Application.isMobilePlatform ? CreateMobileVibrator() : new StubVibrator();

        public static IVibrator Create([NotNull] ILogger<IVibrator> logger)
        {
            IVibrator vibrator = Create();

            return new LoggingVibratorDecorator(vibrator, logger);
        }

        public static IVibrator Create([NotNull] IVibratorIntensityDivider intensityDivider)
        {
            IVibrator vibrator = Create();

            return new IntensityDividingVibratorDecorator(vibrator, intensityDivider);
        }

        public static IVibrator Create([NotNull] IVibratorIntensityDivider intensityDivider, ILogger<IVibrator> logger)
        {
            IVibrator vibrator = Create(intensityDivider);

            return new LoggingVibratorDecorator(vibrator, logger);
        }

        private static IVibrator CreateMobileVibrator()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            return Infrastructure.Vibration.Android.AndroidVibratorFactory.Create();

#elif (UNITY_IOS || UNITY_IPHONE) && !UNITY_EDITOR
            return new Infrastructure.Vibration.iOS.iOSVibrator();

#else
            return new Infrastructure.Vibration.StubVibrator();

#endif
        }
    }
}