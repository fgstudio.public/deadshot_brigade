﻿#if (UNITY_ANDROID || UNITY_IOS || UNITY_IPHONE) && !UNITY_EDITOR

using UnityEngine;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration
{
    public sealed class UnityVibrator : IVibrator
    {
        private readonly IVibration _stubVibration = new StubVibration();

        public IVibration Vibrate(int durationMs) => VibrateInternal();
        public IVibration Vibrate(int durationMs, byte intensity) => VibrateInternal();
        public IVibration Vibrate(IVibrationPattern pattern) => VibrateInternal();

        public void Cancel()
        {
        }

        private IVibration VibrateInternal()
        {
            Handheld.Vibrate();

            return _stubVibration;
        }
    }
}

#endif