﻿using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration
{
    public sealed class StubVibration : IVibration
    {
        public void Stop()
        {
        }
    }
}