﻿using Zenject;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration
{
    public sealed class VibrationInstaller : Installer
    {
        public override void InstallBindings() => Container.Bind<IVibrator>().FromMethod(CreateVibrator);

        private IVibrator CreateVibrator(InjectContext context)
        {
            var logger = context.Container.Resolve<ILogger<IVibrator>>();
            return VibratorFactory.Create(logger);
        }
    }
}