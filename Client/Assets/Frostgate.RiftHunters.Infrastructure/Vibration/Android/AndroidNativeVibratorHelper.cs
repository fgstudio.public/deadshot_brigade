﻿#if UNITY_ANDROID && !UNITY_EDITOR

using UnityEngine;

namespace Frostgate.RiftHunters.Infrastructure.Vibration.Android
{
    public static class AndroidNativeVibratorHelper
    {
        private const string GetSystemServiceMethodName = "getSystemService";
        private const string ActivityVibratorFieldName = "vibrator";

        private const string HasVibratorMethodName = "hasVibrator";
        private const string HasAmplitudeControlMethodName = "hasAmplitudeControl";

        public static AndroidJavaObject Vibrator => _vibratorCache ??= GetVibrator();

        /// <summary>
        /// API lvl 11+
        /// </summary>
        public static bool HasVibrator
        {
            get
            {
                _hasVibratorCache ??= Vibrator.Call<bool>(HasVibratorMethodName);
                return _hasVibratorCache.Value;
            }
        }

        /// <summary>
        /// API lvl 26+
        /// </summary>
        public static bool HasAmplitudeControl
        {
            get
            {
                _hasAmplitudeControlCache ??= Vibrator.Call<bool>(HasAmplitudeControlMethodName);
                return _hasAmplitudeControlCache.Value;
            }
        }

        private static AndroidJavaObject _vibratorCache;
        private static bool? _hasVibratorCache;
        private static bool? _hasAmplitudeControlCache;

        private static AndroidJavaObject GetVibrator()
        {
            AndroidJavaObject currentActivity = AndroidHelper.CurrentActivity;

            return currentActivity.Call<AndroidJavaObject>(GetSystemServiceMethodName, ActivityVibratorFieldName);
        }
    }
}

#endif