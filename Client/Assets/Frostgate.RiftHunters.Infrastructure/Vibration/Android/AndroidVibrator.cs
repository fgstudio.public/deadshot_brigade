﻿#if UNITY_ANDROID && !UNITY_EDITOR

using UnityEngine;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration.Android
{
    public abstract class AndroidVibrator : IVibrator
    {
        protected const string VibrateMethodName = "vibrate";
        protected const string CancelMethodName = "cancel";

        protected readonly AndroidJavaObject Vibrator = AndroidNativeVibratorHelper.Vibrator;

        public abstract IVibration Vibrate(int durationMs);
        public abstract IVibration Vibrate(int durationMs, byte intensity);
        public abstract IVibration Vibrate(IVibrationPattern pattern);

        public virtual void Cancel() => Vibrator.Call(CancelMethodName);

        protected void CallVibrate(params object[] args) => Vibrator.Call(VibrateMethodName, args);

        protected IVibration CreateDefaultVibration() => new AndroidVibration(Cancel);
    }
}

#endif