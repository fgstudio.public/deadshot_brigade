﻿#if UNITY_ANDROID && !UNITY_EDITOR

using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration.Android
{
    public sealed class AndroidVibration : IVibration
    {
        [NotNull] private readonly Action _cancelDelegate;

        public AndroidVibration([NotNull] Action cancelDelegate)
        {
            _cancelDelegate = cancelDelegate;
        }

        public void Stop() => _cancelDelegate.Invoke();
    }
}

#endif