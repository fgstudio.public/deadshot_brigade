﻿#if UNITY_ANDROID && !UNITY_EDITOR

using JetBrains.Annotations;
using UnityEngine;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration.Android
{
    public interface IAndroidVibrationEffectFactory
    {
        AndroidJavaObject CreateOneShot(int durationMs, byte intensity);
        AndroidJavaObject CreateWaveform([NotNull] IVibrationPattern pattern);
    }
}

#endif