﻿#if UNITY_ANDROID && !UNITY_EDITOR

using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration.Android
{
    /// <summary>
    /// Для API lvl < 26
    /// </summary>
    public sealed class AndroidVibratorSDK26Less : AndroidVibrator
    {
        public override IVibration Vibrate(int durationMs)
        {
            CallVibrate((long)durationMs);
            return CreateDefaultVibration();
        }

        public override IVibration Vibrate(int durationMs, byte intensity) => Vibrate(durationMs);

        public override IVibration Vibrate(IVibrationPattern pattern)
        {
            int totalDuration = 0;
            IVibration vibration = null;

            foreach (VibrationInterval interval in pattern.Intervals)
                totalDuration += interval.DurationMs;

            if (totalDuration > 0)
                vibration = Vibrate(totalDuration);

            return vibration ?? CreateDefaultVibration();
        }
    }
}

#endif