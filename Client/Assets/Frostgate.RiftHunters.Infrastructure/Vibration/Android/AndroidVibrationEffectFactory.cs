﻿#if UNITY_ANDROID && !UNITY_EDITOR

using UnityEngine;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration.Android
{
    public class AndroidVibrationEffectFactory : IAndroidVibrationEffectFactory
    {
        private const string VibrationEffectClassName = "android.os.VibrationEffect";

        private const string CreateOneShotMethodName = "createOneShot";
        private const string CreateWaveformMethodName = "createWaveform";

        private AndroidJavaObject _nativeVibrationEffect = new AndroidJavaClass(VibrationEffectClassName);

        public AndroidJavaObject CreateOneShot(int durationMs, byte intensity) =>
            CreateEffect(CreateOneShotMethodName, (long)durationMs, (int)intensity);

        public AndroidJavaObject CreateWaveform(IVibrationPattern pattern)
        {
            long[] timings = new long[pattern.Intervals.Count];
            int[] amplitudes = new int[pattern.Intervals.Count];

            for (var i = 0; i < pattern.Intervals.Count; i++)
            {
                timings[i] = pattern.Intervals[i].DurationMs;
                amplitudes[i] = pattern.Intervals[i].Intensity;
            }

            return CreateEffect(CreateWaveformMethodName, timings, amplitudes, -1);
        }

        private AndroidJavaObject CreateEffect(string methodName, params object[] args) =>
            _nativeVibrationEffect.CallStatic<AndroidJavaObject>(methodName, args);
    }
}

#endif