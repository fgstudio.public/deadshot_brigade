﻿#if UNITY_ANDROID && !UNITY_EDITOR

using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration.Android
{
    public static class AndroidVibratorFactory
    {
        public static IVibrator Create()
        {
            return AndroidHelper.SdkVersion switch
            {
                AndroidHelper.BadSdkVersion => new UnityVibrator(),
                < 26 => new AndroidVibratorSDK26Less(),
                >= 26 => new AndroidVibratorSDK26Greater(new AndroidVibrationEffectFactory())
            };
        }
    }
}

#endif