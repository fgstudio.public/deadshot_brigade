﻿#if UNITY_ANDROID && !UNITY_EDITOR

using JetBrains.Annotations;
using UnityEngine;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration.Android
{
    /// <summary>
    /// Для API lvl >= 26
    /// </summary>
    public sealed class AndroidVibratorSDK26Greater : AndroidVibrator
    {
        [NotNull] private readonly IAndroidVibrationEffectFactory _effectFactory;

        public AndroidVibratorSDK26Greater([NotNull] IAndroidVibrationEffectFactory effectFactory)
        {
            _effectFactory = effectFactory;
        }

        public override IVibration Vibrate(int durationMs) => Vibrate(durationMs, VibrationConstants.DefaultIntensity);

        public override IVibration Vibrate(int durationMs, byte intensity)
        {
            AndroidJavaObject effect = _effectFactory.CreateOneShot(durationMs, intensity);
            CallVibrate(effect);

            return CreateDefaultVibration();
        }

        public override IVibration Vibrate(IVibrationPattern pattern)
        {
            AndroidJavaObject effect = _effectFactory.CreateWaveform(pattern);
            CallVibrate(effect);

            return CreateDefaultVibration();
        }
    }
}

#endif