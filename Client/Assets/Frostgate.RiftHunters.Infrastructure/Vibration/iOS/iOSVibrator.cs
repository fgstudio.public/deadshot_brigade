﻿#if (UNITY_IOS || UNITY_IPHONE) && !UNITY_EDITOR

using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Vibration.iOS
{
    public sealed class iOSVibrator : IVibrator
    {
        public IVibration Vibrate(int durationMs)
        {
            throw new System.NotImplementedException();
        }

        public IVibration Vibrate(int durationMs, byte intensity)
        {
            throw new System.NotImplementedException();
        }

        public IVibration Vibrate(IVibrationPattern pattern)
        {
            throw new System.NotImplementedException();
        }

        public void Cancel()
        {
            throw new System.NotImplementedException();
        }
    }
}

#endif