﻿#if UNITY_SERVER || UNITY_EDITOR

using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Infrastructure.Services.Matchmaking;
using JetBrains.Annotations;
using Unity.Services.Matchmaker.Models;
using Unity.Services.Multiplay;

namespace Frostgate.RiftHunters.Infrastructure.Services.DGSServices
{
    public sealed class MultiplayServerAllocationService : IServerAllocationService
    {
        private IMultiplayService MultiplayService => Unity.Services.Multiplay.MultiplayService.Instance;

        [NotNull] private readonly IConfigCollection<IMissionConfig> _missions;
        [NotNull] private readonly IConfigCollection<UnitConfig> _units;
        [NotNull] private readonly UnityServerBackfillService _backfillService;

        public MultiplayServerAllocationService([NotNull] IConfigCollection<IMissionConfig> missions,
            [NotNull] IConfigCollection<UnitConfig> units, [NotNull] UnityServerBackfillService backfillService)
        {
            _missions = missions;
            _units = units;
            _backfillService = backfillService;
        }

        public async UniTask<IAllocationData> AwaitAllocationAsync()
        {
            MatchmakingResults allocationPayload =
                await MultiplayService.GetPayloadAllocationFromJsonAs<MatchmakingResults>();

            _backfillService.MatchmakingResults = allocationPayload;

            return CreateAllocationData(allocationPayload);
        }

        private IAllocationData CreateAllocationData(MatchmakingResults results)
        {
            MatchProperties props = results.MatchProperties;
            List<Player> players = props.Players;

            // Можно взять первого игрока и вытащить ID миссии, т.к. миссия у всех должна быть одинаковой 
            var playerData = players[0].CustomData.GetAs<PlayerCustomData>();
            IMissionConfig mission = _missions[playerData.SelectedMissionId];

            var matchPlayers = players.Select(p =>
            {
                string playerId = p.Id;
                // ReSharper disable once VariableHidesOuterVariable
                PlayerCustomData playerData = p.CustomData.GetAs<PlayerCustomData>();
                UnitConfig selectedUnit = _units[playerData.SelectedUnitId];

                return new MatchPlayer(playerId, selectedUnit);
            }).ToList();

            return new MultiplayAllocationData(mission, matchPlayers);
        }
    }
}

#endif