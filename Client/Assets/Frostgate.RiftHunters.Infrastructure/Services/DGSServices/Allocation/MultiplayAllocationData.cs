﻿using System.Collections.Generic;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Core.Battle;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Infrastructure.Services.DGSServices
{
    public class MultiplayAllocationData : IAllocationData
    {
        public IMissionConfig Mission { get; }
        public IReadOnlyList<MatchPlayer> Players { get; }

        public MultiplayAllocationData([NotNull] IMissionConfig mission, [NotNull] IReadOnlyList<MatchPlayer> players)
        {
            Mission = mission;
            Players = players;
        }
    }
}