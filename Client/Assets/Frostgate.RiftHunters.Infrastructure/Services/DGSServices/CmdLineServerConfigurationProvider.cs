﻿using System;
using System.Collections.Generic;
using System.Text;
using Frostgate.RiftHunters.Core;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Infrastructure.Services.DGSServices
{
    /// <summary>
    /// Провайдер конфигурации сервера, переданной через аргументы командной строки.
    /// </summary>
    public sealed class CmdLineServerConfigurationProvider : IServerConfigurationProvider
    {
        private const string OptionPrefix = "-";
        private const string PortOption = OptionPrefix + "port";
        private const string QueryPortOption = OptionPrefix + "queryPort";

        private const int DefaultPort = 7777;

        public int Port { get; private set; }
        public int QueryPort { get; private set; }

        [NotNull] private readonly IDictionary<string, Action<string>> _optionActions =
            new Dictionary<string, Action<string>>();

        [NotNull] private readonly ILogger<CmdLineServerConfigurationProvider> _logger;

        public CmdLineServerConfigurationProvider([NotNull] ILogger<CmdLineServerConfigurationProvider> logger)
        {
            _logger = logger;

            _optionActions[PortOption] = SetPort;
            _optionActions[QueryPortOption] = SetQueryPort;

            ProcessCommanLineArguments(Environment.GetCommandLineArgs());
        }

        private void ProcessCommanLineArguments(string[] args)
        {
            StringBuilder builder = new();
            builder.Append("Command line arguments: ");

            for (var i = 0; i < args.Length; i++)
            {
                string arg = args[i];
                string nextArg = string.Empty;
                if (i + 1 < args.Length)
                    nextArg = args[i + 1];

                if (EvaluateArgument(arg, nextArg))
                {
                    builder.Append(arg);
                    builder.Append("=");
                    builder.AppendLine(nextArg);

                    i++;
                }
            }

            _logger.LogInfo(builder.ToString());
        }

        private bool EvaluateArgument(string arg, string nextArg)
        {
            if (!IsOption(arg) || IsOption(nextArg))
                return false;

            _optionActions[arg].Invoke(nextArg);

            return true;
        }

        private void SetPort(string portArg)
        {
            if (int.TryParse(portArg, out int port))
            {
                Port = port;
            }
            else
            {
                _logger.LogError("Command line does not contain a parseable port.");
                _logger.LogInfo($"Fallback to default port '{DefaultPort}'");
                Port = DefaultPort;
            }
        }

        private void SetQueryPort(string queryPortArg)
        {
            if (int.TryParse(queryPortArg, out int queryPort))
                QueryPort = queryPort;
            else
                _logger.LogError("Command line does not contain a parseable port.");
        }

        private bool IsOption(string arg) =>
            !string.IsNullOrEmpty(arg) &&
            _optionActions.ContainsKey(arg) &&
            arg.StartsWith(OptionPrefix);
    }
}