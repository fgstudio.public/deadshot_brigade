﻿#if UNITY_SERVER || UNITY_EDITOR

using System;
using System.Linq;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using JetBrains.Annotations;
using Unity.Services.Matchmaker;
using Unity.Services.Matchmaker.Models;
using Unity.Services.Multiplay;

namespace Frostgate.RiftHunters.Infrastructure.Services.DGSServices
{
    public sealed class UnityServerBackfillService : IServerBackfillService, IDisposable
    {
        private const int BackfillingIntervalMilliseconds = 1000;

        public int MaxPlayersNumber { get; set; }
        public MatchmakingResults MatchmakingResults { get; set; }

        private IMatchmakerService MatchmakerService => Unity.Services.Matchmaker.MatchmakerService.Instance;
        private int PlayersNumber => _backfillTicket?.Properties.MatchProperties.Players.Count ?? 0;

        [NotNull] private readonly ILogger<UnityServerBackfillService> _logger;

        private CreateBackfillTicketOptions _createBackfillTicketOptions;
        private BackfillTicket _backfillTicket;

        private bool _isBackfillingStarted;
        private bool _isBackfillDataDirty;

        public UnityServerBackfillService([NotNull] ILogger<UnityServerBackfillService> logger)
        {
            _logger = logger;
        }

        public async UniTask StartBackfillingAsync()
        {
            if (_isBackfillingStarted)
            {
                _logger.LogWarning("Backfilling already started.");
                return;
            }

            _logger.LogInfo("Starting backfill.");

            CreateBackfillTicketIfNotExist();

            if (string.IsNullOrEmpty(_backfillTicket.Id))
            {
                _backfillTicket.Id =
                    await MatchmakerService.CreateBackfillTicketAsync(_createBackfillTicketOptions);
            }

            _isBackfillingStarted = true;
            BackfillUpdateLoop();
        }

        public async UniTask StopBackfillingAsync()
        {
            if (!_isBackfillingStarted)
            {
                _logger.LogWarning("Can't stop backfilling before start.");
                return;
            }

            _logger.LogInfo("Backfill stopping.");

            await MatchmakerService.DeleteBackfillTicketAsync(_backfillTicket.Id);
            _isBackfillingStarted = false;

            _backfillTicket.Id = null;
        }

        public void AddPlayerToMatch(string playerId)
        {
            if (!_isBackfillingStarted)
            {
                _logger.LogWarning("Can't add players to the ticket before backfilling start.");
                return;
            }

            if (TryGetPlayerFromTicket(playerId, out _))
            {
                _logger.LogWarning($"User with id '{playerId}' already in match, ignoring");
                return;
            }

            var playerSource = MatchmakingResults.MatchProperties.Players.FirstOrDefault();
            var player = new Player(playerId, playerSource?.CustomData);

            _backfillTicket.Properties.MatchProperties.Players.Add(player);
            _backfillTicket.Properties.MatchProperties.Teams[0].PlayerIds.Add(playerId);

            _isBackfillDataDirty = true;
        }

        public void RemovePlayerFromMatch(string playerId)
        {
            if (!TryGetPlayerFromTicket(playerId, out Player player))
            {
                _logger.LogWarning($"User with id '{playerId}' not found in backfill ticket.");
                return;
            }

            _backfillTicket.Properties.MatchProperties.Players.Remove(player);
            _backfillTicket.Properties.MatchProperties.Teams[0].PlayerIds.Remove(playerId);

            _isBackfillDataDirty = true;
        }

        public async void Dispose() => await StopBackfillingAsync();

        private void CreateBackfillTicketIfNotExist()
        {
            if (_backfillTicket != null)
                return;

            MatchProperties matchProperties = MatchmakingResults.MatchProperties;
            var backFillProps = new BackfillTicketProperties(matchProperties);
            _backfillTicket = new BackfillTicket
            {
                Id = matchProperties.BackfillTicketId,
                Properties = backFillProps
            };

            ServerConfig serverConfig = MultiplayService.Instance.ServerConfig;

            _createBackfillTicketOptions = new CreateBackfillTicketOptions
            {
                Connection = $"{serverConfig.IpAddress}:{serverConfig.Port}",
                QueueName = MatchmakingResults.QueueName,
                Properties = backFillProps,
            };
        }

        private async void BackfillUpdateLoop()
        {
            while (_isBackfillingStarted)
            {
                if (_isBackfillDataDirty)
                {
                    await MatchmakerService.UpdateBackfillTicketAsync(_backfillTicket.Id, _backfillTicket);
                    _isBackfillDataDirty = false;
                }
                else
                {
                    _backfillTicket = await MatchmakerService.ApproveBackfillTicketAsync(_backfillTicket.Id);
                }

                if (!NeedsPlayer())
                {
                    await StopBackfillingAsync();
                    break;
                }

                await UniTask.Delay(BackfillingIntervalMilliseconds, DelayType.Realtime);
            }
        }

        private bool NeedsPlayer() => PlayersNumber < MaxPlayersNumber;

        private bool TryGetPlayerFromTicket(string playerId, out Player player)
        {
            player = _backfillTicket.Properties.MatchProperties.Players.FirstOrDefault(p => p.Id == playerId);

            return player != null;
        }
    }
}

#endif