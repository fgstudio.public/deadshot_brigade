﻿#if UNITY_SERVER || UNITY_EDITOR

using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Core.Battle;
using JetBrains.Annotations;
using Unity.Services.Multiplay;
using UnityEngine;

namespace Frostgate.RiftHunters.Infrastructure.Services.DGSServices
{
    public sealed class MultiplayServerDataQueryService : IServerDataQueryService, IDisposable
    {
        private const string ServerName = "DSB Server";
        private const string Undefined = nameof(Undefined);
        
        // Рекоменднованный интервал отправки данных SQP должен быть меньше 1 секунды.
        // В противном случае Multiplay посчитает, что сервер находится в состоянии ANR и деаллоцирует инстанс.
        private const int QueryUpdateIntervalMilliseconds = 100; 

        public IMissionConfig CurrentMission
        {
            set
            {
                _currentMission = value;
                TrySetQueryData();
            }
        }

        public int CurrentPlayersNumber
        {
            set
            {
                _currentPlayersNumber = value;
                TrySetQueryData();
            }
        }


        private IMultiplayService MultiplayService => Unity.Services.Multiplay.MultiplayService.Instance;

        [NotNull] private readonly ILogger<MultiplayServerDataQueryService> _logger;

        [CanBeNull] private IMissionConfig _currentMission;
        private int _currentPlayersNumber;

        private CancellationTokenSource _queryLoopCts;
        private IServerQueryHandler _queryHandler;

        public MultiplayServerDataQueryService([NotNull] ILogger<MultiplayServerDataQueryService> logger)
        {
            _logger = logger;
        }

        public async UniTask StartQueryingAsync()
        {
            StopQuerying();

            _logger.LogInfo("Starting SQP query loop.");

            _queryHandler =
                await MultiplayService.StartServerQueryHandlerAsync(
                    3,
                    ServerName,
                    _currentMission?.Id ?? Undefined,
                    Application.version,
                    _currentMission?.MissionName ?? Undefined);

            _queryLoopCts = new CancellationTokenSource();
            QueryUpdateLoop(_queryLoopCts.Token);
        }

        public void StopQuerying()
        {
            _logger.LogInfo("Stopping SQP query loop.");

            _queryLoopCts?.Cancel();
            _queryLoopCts?.Dispose();
            _queryLoopCts = null;
        }

        public void Dispose() => StopQuerying();

        private async void QueryUpdateLoop(CancellationToken cancellationToken)
        {
            _logger.LogInfo("SQP query loop started.");

            while (!cancellationToken.IsCancellationRequested)
            {
                _queryHandler.UpdateServerCheck();

                // ReSharper disable once MethodSupportsCancellation
                await UniTask.Delay(QueryUpdateIntervalMilliseconds, true);
            }

            _logger.LogInfo("SQP query loop stopped.");
        }

        private void TrySetQueryData()
        {
            if (_queryHandler == null)
                return;

            _queryHandler.BuildId = Application.version;
            _queryHandler.ServerName = ServerName;

            _queryHandler.MaxPlayers = (ushort)(_currentMission?.MaxPlayersNumber ?? 0);
            _queryHandler.CurrentPlayers = (ushort)_currentPlayersNumber;

            _queryHandler.GameType = _currentMission?.Id ?? Undefined;
            _queryHandler.Map = _currentMission?.MissionName ?? Undefined;

            _logger.LogInfo("SQP query data updated. " +
                            $"BuildId '{_queryHandler.BuildId}', " +
                            $"ServerName '{_queryHandler.ServerName}', " +
                            $"MaxPlayers '{_queryHandler.MaxPlayers}, " +
                            $"CurrentPlayers '{_queryHandler.CurrentPlayers}', " +
                            $"GameType '{_queryHandler.GameType}', " +
                            $"Map '{_queryHandler.Map}'.");
        }
    }
}

#endif