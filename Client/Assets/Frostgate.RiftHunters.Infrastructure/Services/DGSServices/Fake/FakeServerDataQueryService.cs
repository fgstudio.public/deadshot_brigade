﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Core.Battle;

namespace Frostgate.RiftHunters.Infrastructure.Services.DGSServices
{
    public sealed class FakeServerDataQueryService : IServerDataQueryService
    {
        public IMissionConfig CurrentMission
        {
            set { }
        }

        public int CurrentPlayersNumber
        {
            set { }
        }

        public UniTask StartQueryingAsync() => UniTask.CompletedTask;

        public void StopQuerying()
        {
        }
    }
}