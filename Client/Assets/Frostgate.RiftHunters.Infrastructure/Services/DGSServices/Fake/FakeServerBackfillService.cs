﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Services.DGSServices
{
    public sealed class FakeServerBackfillService : IServerBackfillService
    {
        public int MaxPlayersNumber
        {
            set { }
        }

        public UniTask StartBackfillingAsync() => UniTask.CompletedTask;

        public UniTask StopBackfillingAsync() => UniTask.CompletedTask;

        public void AddPlayerToMatch(string playerId)
        {
        }

        public void RemovePlayerFromMatch(string playerId)
        {
        }
    }
}