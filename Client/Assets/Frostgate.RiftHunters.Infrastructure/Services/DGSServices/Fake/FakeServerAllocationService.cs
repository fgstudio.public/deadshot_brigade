﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Services.DGSServices
{
    public sealed class FakeServerAllocationService : IServerAllocationService
    {
        public UniTask<IAllocationData> AwaitAllocationAsync() => UniTask.FromResult<IAllocationData>(null);
    }
}