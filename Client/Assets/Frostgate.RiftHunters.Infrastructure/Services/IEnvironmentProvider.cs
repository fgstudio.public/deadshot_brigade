﻿namespace Frostgate.RiftHunters.Infrastructure.Services
{
    public interface IEnvironmentProvider
    {
        string GetEnvironment();
    }
}