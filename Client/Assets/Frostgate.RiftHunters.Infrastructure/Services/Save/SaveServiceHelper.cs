﻿using System;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta;
using Frostgate.RiftHunters.Meta.Player;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Infrastructure.Services.Save
{
    public sealed class SaveServiceHelper
    {
        public static IReadOnlyBipolarDictionary<Type, string> TypedDataKeys { get; } = new BipolarDictionary<Type, string>()
        {
            [typeof(PlayerData)] = "PlayerData",
            [typeof(CampaignData)] = "CampaignData",
            [typeof(HeroesEquipmentData)] = "HeroEquipmentData",
            [typeof(HeroesUpgradeData)] = "HeroesUpgradeData",
        };
    }
}