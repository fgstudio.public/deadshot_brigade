﻿using System.Linq;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Infrastructure.Services.Save
{
    public sealed class DataCache
    {
        private readonly ILogger _logger = LoggerFactory.CreateLogger<DataCache>();
        private readonly Dictionary<string, object> _dataCache = new();

        public void Initialize(Dictionary<string, object> data)
        {
            foreach (KeyValuePair<string,object> kv in data)
                _dataCache[kv.Key] = kv.Value;
            _logger.Log("Initialized");
        }

        public void Set(Dictionary<string, object> data)
        {
            foreach (KeyValuePair<string,object> kv in data)
                Set(kv.Key, kv.Value);
        }

        public void Set(string key, object value)
        {
            _dataCache[key] = value;
            _logger.Log($"Set [{key}]: {value}");
        }

        [CanBeNull]
        public object Get(string key) =>
            _dataCache.TryGetValue(key, out object obj) ? obj : default;

        public Dictionary<string, object> Get(IEnumerable<string> keys) =>
            keys.ToDictionary(k => k, Get);

        public Dictionary<string, object> GetAll() =>
            _dataCache;
    }
}