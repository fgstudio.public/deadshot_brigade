﻿using System;
using Frostgate.RiftHunters.Core;
using UnityEngine;
using JetBrains.Annotations;
using ILogger = Frostgate.RiftHunters.Core.ILogger;

namespace Frostgate.RiftHunters.Infrastructure.Services.Save
{
    public sealed class SyncTimeTracker
    {
        public const int SyncThresholdSec = 30;
        private const string PrefsKey = "SyncTimestamp";

        private readonly ILogger _logger = LoggerFactory.CreateLogger<SyncTimeTracker>();
        private DateTime? _trackedSyncTime;

        public bool IsSyncThresholdReached =>
            !_trackedSyncTime.HasValue ||
            (DateTime.UtcNow - _trackedSyncTime.Value).TotalSeconds >= SyncThresholdSec;

        public SyncTimeTracker() =>
            _trackedSyncTime = LoadTrackedSyncTime();

        public void TrackNow()
        {
            _trackedSyncTime = DateTime.UtcNow;
            SaveTrackedSyncTime(_trackedSyncTime.Value);
        }

        [CanBeNull]
        private DateTime? LoadTrackedSyncTime()
        {
            string timestampStr = PlayerPrefs.GetString(PrefsKey, string.Empty);
            if (string.IsNullOrEmpty(timestampStr))
                return null;

            _logger.Log($"Loaded Timestamp: {timestampStr}");
            long timestamp = long.Parse(timestampStr);

            return new DateTime(timestamp);
        }

        private void SaveTrackedSyncTime(DateTime trackedSyncTime)
        {
            long timestamp = trackedSyncTime.Ticks;
            string timestampStr = timestamp.ToString();
            PlayerPrefs.SetString(PrefsKey, timestampStr);
            _logger.Log($"Saved Timestamp: {timestampStr}");
        }
    }
}