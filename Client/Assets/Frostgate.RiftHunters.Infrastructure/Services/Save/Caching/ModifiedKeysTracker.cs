﻿using UnityEngine;
using Newtonsoft.Json;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core;
using ILogger = Frostgate.RiftHunters.Core.ILogger;

namespace Frostgate.RiftHunters.Infrastructure.Services.Save
{
    public sealed class ModifiedKeysTracker
    {
        private const string PrefsKey = "TrackedKeys";

        private readonly HashSet<string> _trackedKeys;
        private readonly List<string> _tempList = new();
        private readonly ILogger _logger = LoggerFactory.CreateLogger<ModifiedKeysTracker>();

        public bool HasTrackedKeys => _trackedKeys.Count > 0;
        public IEnumerable<string> TrackedKeys => _trackedKeys;

        public ModifiedKeysTracker() =>_trackedKeys = LoadTrackedKeys();

        public void Track(IEnumerable<string> keys)
        {
            _tempList.Clear();

            foreach (string key in keys)
                if (!_trackedKeys.Contains(key))
                {
                    _trackedKeys.Add(key);
                    _tempList.Add(key);
                }

            if (_tempList.Count > 0)
            {
                _logger.Log("Tracked: " + string.Join(", ", _tempList));
                SaveTrackedKeys(_trackedKeys);
            }
        }

        public void Track(string key)
        {
            if (!_trackedKeys.Contains(key))
            {
                _trackedKeys.Add(key);
                _logger.Log("Tracked: " + key);
                SaveTrackedKeys(_trackedKeys);
            }
        }

        public void Clear()
        {
            DeleteTrackedKeys();
            _trackedKeys.Clear();
            _logger.Log("Cleared Keys");
        }

        [NotNull]
        private HashSet<string> LoadTrackedKeys()
        {
            string trackedKeysJson = PlayerPrefs.GetString(PrefsKey, string.Empty);
            if (string.IsNullOrEmpty(trackedKeysJson))
                return new HashSet<string>();

            _logger.Log($"Loaded Keys: {trackedKeysJson}");
            return JsonConvert.DeserializeObject<HashSet<string>>(trackedKeysJson) ?? new();

        }

        private void SaveTrackedKeys(HashSet<string> trackedKeys)
        {
            string trackedKeysJson = JsonConvert.SerializeObject(trackedKeys);
            PlayerPrefs.SetString(PrefsKey, trackedKeysJson);
            _logger.Log($"Saved Keys: {trackedKeysJson}");
        }

        private void DeleteTrackedKeys()
        {
            if (PlayerPrefs.HasKey(PrefsKey))
            {
                PlayerPrefs.DeleteKey(PrefsKey);
                _logger.Log("Deleted All Keys");
            }
        }
    }
}