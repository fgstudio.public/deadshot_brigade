﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
using Newtonsoft.Json;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Infrastructure.Services.Auth;
using Zenject;

namespace Frostgate.RiftHunters.Infrastructure.Services.Save
{
    public sealed class SaveServiceMediator : ISaveService, IDisposable
    {
        [NotNull] private readonly DataCache _dataCache;

        [NotNull] private readonly IDataStorage _remoteSave; // работать с remote только при isInitialized

        [NotNull] private readonly ILocalDataStorage _localSave;

        private readonly CancellationTokenSource _cts = new();

        private readonly SyncTimeTracker _syncTimeTracker = new();

        private readonly ModifiedKeysTracker _modifiedKeysTracker = new();

        private readonly ILogger<SaveServiceMediator> _logger;

        private readonly DiContainer _container;

        private bool _isInitialized;

        public SaveServiceMediator(DiContainer container, [NotNull] DataCache dataCache, [NotNull] ILocalDataStorage localSave,
            [NotNull] IDataStorage remoteSave, [NotNull] ILogger<SaveServiceMediator> logger)
        {
            _container = container;
            _dataCache = dataCache;
            _localSave = localSave;
            _remoteSave = remoteSave;
            _logger = logger;
        }

        public async UniTask InitializeAsync()
        {
            _logger.Log("Initializing...");

            CancellationToken token = _cts.Token;

            Dictionary<string, string> rawData = await _localSave.LoadAllAsync().AttachExternalCancellation(token);
            if (token.IsCancellationRequested) return;

            if (rawData.Count > 0)
            {
                Dictionary<string, object> data = ConvertDataFromRaw(rawData);
                _dataCache.Initialize(data);
                _logger.Log("Cached from local");
                SetInitialized();
                return;
            }

            rawData = await _remoteSave.LoadAllAsync().AttachExternalCancellation(token);
            if (token.IsCancellationRequested) return;

            if (rawData.Count > 0)
            {
                Dictionary<string, object> data = ConvertDataFromRaw(rawData);
                _localSave.SaveAsync(data);
                _dataCache.Initialize(data);
                _logger.Log("Cached from remote");
            }

            SetInitialized();
        }

        public void Dispose()
        {
            _cts.Cancel();
            _cts.Dispose();

            if (_isInitialized && _modifiedKeysTracker.HasTrackedKeys)
                UpdateRemoteFromCache(_modifiedKeysTracker.TrackedKeys)
                    .SuppressCancellationThrow();

            _isInitialized = false;
            _logger.Log("Disposed");
        }

        private void SetInitialized()
        {
            _isInitialized = true;
            _logger.Log("Initialized");
        }

        private Dictionary<string, object> ConvertDataFromRaw(Dictionary<string, string> rawData)
        {
            return rawData.ToDictionary(
                kv => kv.Key,
                kv =>
                {
                    Type type = SaveServiceHelper.TypedDataKeys[kv.Key];
                    return type != null && !string.IsNullOrEmpty(kv.Value)
                        ? JsonConvert.DeserializeObject(kv.Value, type)
                        : null;
                }
            );
        }

        public async UniTask SaveData<T>(T data)
        {
            string dataKey = SaveServiceHelper.TypedDataKeys[typeof(T)];

            _dataCache.Set(dataKey, data);
            _modifiedKeysTracker.Track(dataKey);

            await _localSave.SaveAsync(dataKey, data);
            await OnDataSaved();
        }

        public async UniTask<T> GetData<T>()
        {
            string dataKey = SaveServiceHelper.TypedDataKeys[typeof(T)];
            object rawData = _dataCache.Get(dataKey);
            if (rawData != default)
                return (T)rawData;

            Dictionary<string, string> dictionary = await _localSave.LoadAsync(new HashSet<string>() { dataKey })
                .AttachExternalCancellation(_cts.Token);

            if (_cts.Token.IsCancellationRequested) return default;
            if (!dictionary.TryGetValue(dataKey, out string json)) return default;
            if (string.IsNullOrEmpty(json)) return default;

            T data = JsonConvert.DeserializeObject<T>(json);
            _dataCache.Set(dataKey, data);

            _container.Inject(data);

            return data;
        }

        private async UniTask OnDataSaved()
        {
            if (_isInitialized && _modifiedKeysTracker.HasTrackedKeys && _syncTimeTracker.IsSyncThresholdReached)
            {
                await UpdateRemoteFromCache(_modifiedKeysTracker.TrackedKeys);
                _modifiedKeysTracker.Clear();
                _syncTimeTracker.TrackNow();
            }
        }

        private async UniTask UpdateRemoteFromCache(IEnumerable<string> keysToUpdate)
        {
            if (!_isInitialized)
                throw new InvalidOperationException("Service is not initialized to make remote saving");

            Dictionary<string, object> dataToRemote = _dataCache.Get(keysToUpdate);

            _logger.Log($"Updating remote with keys: {string.Join(",", dataToRemote.Keys)}");
            await _remoteSave.SaveAsync(dataToRemote);
            _logger.Log("Updated remote");
        }
    }
}