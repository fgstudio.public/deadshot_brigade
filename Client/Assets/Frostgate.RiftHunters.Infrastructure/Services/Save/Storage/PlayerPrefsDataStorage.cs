﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using ILogger = Frostgate.RiftHunters.Core.ILogger;

namespace Frostgate.RiftHunters.Infrastructure.Services.Save
{
    public sealed class PlayerPrefsDataStorage : ILocalDataStorage
    {
        private const string StoredKeysKey = "StoredKeys";

        private readonly HashSet<string> _storedKeys = new();
        private readonly ILogger _logger = LoggerFactory.CreateLogger<PlayerPrefsDataStorage>();

        public PlayerPrefsDataStorage()
        {
            _storedKeys = LoadStoredKeys();
        }

        public void ClearData()
        {
            _storedKeys.ForEach(RemoveData);
            ClearStoredKeys();
            _logger.Log("Cleared Data");
        }

        public UniTask SaveAsync(string key, object value)
        {
            SetData(key, value);
            return UniTask.CompletedTask;
        }

        public UniTask SaveAsync(Dictionary<string, object> data)
        {
            foreach (KeyValuePair<string,object> kv in data)
                SetData(kv.Key, kv.Value);

            return UniTask.CompletedTask;
        }

        public UniTask<Dictionary<string, string>> LoadAllAsync()
        {
            return LoadAsync(_storedKeys);
        }

        public UniTask<Dictionary<string, string>> LoadAsync(IEnumerable<string> keys)
        {
            return UniTask.FromResult(keys.ToDictionary(k => k, LoadData));
        }

        private void SetData(string key, object value)
        {
            string json = JsonConvert.SerializeObject(value);
            PlayerPrefs.SetString(key, json);
            _logger.Log($"Set [{key}] = {json}");

            if (!_storedKeys.Contains(key))
                AddStoredKeys(key);
        }

        private string LoadData(string key)
        {
            string json = PlayerPrefs.GetString(key, string.Empty);
            _logger.Log($"Loaded [{key}] = {json}");
            return json;
        }

        private void RemoveData(string key)
        {
            PlayerPrefs.DeleteKey(key);
            _logger.Log($"Deleted [{key}]");
        }

        private HashSet<string> LoadStoredKeys()
        {
            string json = PlayerPrefs.GetString(StoredKeysKey, string.Empty);
            _logger.Log($"Loaded stored keys: [{json}]");

            return string.IsNullOrEmpty(json) ? new() : JsonConvert.DeserializeObject<HashSet<string>>(json);
        }

        private void AddStoredKeys(string newKey)
        {
            _storedKeys.Add(newKey);

            string json = JsonConvert.SerializeObject(_storedKeys);
            PlayerPrefs.SetString(StoredKeysKey, json);
            _logger.Log($"Updated stored keys: [{json}]");
        }

        private void ClearStoredKeys()
        {
            _storedKeys.Clear();
            PlayerPrefs.DeleteKey(StoredKeysKey);
            _logger.Log("Cleared all stored keys");
        }
    }
}