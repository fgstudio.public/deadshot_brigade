﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using Unity.Services.CloudSave;

namespace Frostgate.RiftHunters.Infrastructure.Services.Save
{
    public sealed class UnityDataStorage : IDataStorage
    {
        private readonly ILogger _logger = LoggerFactory.CreateLogger<UnityDataStorage>();

        public UniTask SaveAsync(string key, object value) =>
            SaveAsync(new Dictionary<string, object>() { [key] = value });

        public async UniTask SaveAsync(Dictionary<string, object> data)
        {
            foreach (KeyValuePair<string,object> kv in data) _logger.Log($"Saving [{kv.Key}]: [{kv.Value}]");

            try
            {
                await CloudSaveService.Instance.Data.ForceSaveAsync(data);
                foreach (KeyValuePair<string,object> kv in data) _logger.Log($"Saved [{kv.Key}]: [{kv.Value}]");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Saving Failed with {nameof(Exception)}: {ex.Message}");
            }
        }

        public async UniTask<Dictionary<string, string>> LoadAllAsync()
        {
            _logger.Log("Loading All...");
            try
            {
                Dictionary<string,string> data = await CloudSaveService.Instance.Data.LoadAllAsync();
                foreach (KeyValuePair<string,string> kv in data) _logger.Log($"Loaded [{kv.Key}]: [{kv.Value}]");
                return data;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Loading All Failed with {nameof(Exception)}: {ex.Message}");
            }

            return new Dictionary<string, string>();
        }

        public async UniTask<Dictionary<string, string>> LoadAsync(IEnumerable<string> keys)
        {
            HashSet<string> keysSet = new(keys);
            foreach (string key in keysSet) _logger.Log($"Loading [{key}]...");

            try
            {
                Dictionary<string,string> data = await CloudSaveService.Instance.Data.LoadAsync(keysSet);
                foreach (KeyValuePair<string,string> kv in data) _logger.Log($"Loaded [{kv.Key}]: [{kv.Value}]");

            }
            catch (Exception ex)
            {
                _logger.LogError($"Loading Failed with {nameof(Exception)}: {ex.Message}");
            }

            return new Dictionary<string, string>();
        }
    }
}