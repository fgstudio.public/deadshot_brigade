﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Infrastructure.Services.Save
{
    public interface ILocalDataStorage : IDataStorage
    {
        void ClearData();
    }

    public interface IDataStorage
    {
        UniTask SaveAsync(string key, object value);
        UniTask SaveAsync(Dictionary<string, object> data);

        UniTask<Dictionary<string, string>> LoadAllAsync();
        UniTask<Dictionary<string, string>> LoadAsync(IEnumerable<string> keys);
    }
}