using Frostgate.RiftHunters.Core;
using UnityEngine;

namespace Frostgate.RiftHunters.Infrastructure.Services.Analytics.AppsFlyer
{
    [CreateAssetMenu(fileName = nameof(AppsFlyerConfig),
        menuName = AssetMenus.RiftHunters.Menu + "/" + nameof(AppsFlyerConfig))]
    public class AppsFlyerConfig : ScriptableConfig
    {
        [SerializeField] private AppsFlyerCredentials _androidCredentials;
        [SerializeField] private AppsFlyerCredentials _iosCredentials;

        private readonly AppsFlyerCredentials _defaultCredentials = new();
        [field: SerializeField] public bool IsDebug { get; private set; }

        public AppsFlyerCredentials GetCredentials()
        {
#if UNITY_ANDROID
            return _androidCredentials;
#elif UNITY_IOS
            return _iosCredentials;
#endif
            return _defaultCredentials;
        }
    }
}