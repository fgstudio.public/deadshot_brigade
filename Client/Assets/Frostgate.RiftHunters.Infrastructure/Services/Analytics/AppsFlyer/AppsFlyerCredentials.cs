using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Infrastructure.Services.Analytics.AppsFlyer
{
    [Serializable]
    public class AppsFlyerCredentials
    {
        [field: SerializeField] public string DevKey;
        [field: SerializeField] public string AppId;
    }
}