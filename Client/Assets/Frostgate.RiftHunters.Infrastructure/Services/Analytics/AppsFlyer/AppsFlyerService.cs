using System;
using AppsFlyerSDK;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using JetBrains.Annotations;
using UnityEngine;

namespace Frostgate.RiftHunters.Infrastructure.Services.Analytics.AppsFlyer
{
    public class AppsFlyerService
    {
        private readonly AppsFlyerConfig _config;

        public AppsFlyerService([NotNull] IConfigAccessor<AppsFlyerConfig> configAccessor)
        {
            _config = configAccessor.Config;
        }

        public UniTask InitializeAsync()
        {
            AppsFlyerSDK.AppsFlyer.OnRequestResponse += AppsFlyerOnRequestResponse;
            
            AppsFlyerSDK.AppsFlyer.setIsDebug(_config.IsDebug);

            var credentials = _config.GetCredentials();
            AppsFlyerSDK.AppsFlyer.initSDK(credentials.DevKey, credentials.AppId);
            AppsFlyerSDK.AppsFlyer.startSDK();

            return UniTask.CompletedTask;
        }

        private void AppsFlyerOnRequestResponse(object sender, EventArgs e)
        {
            var args = e as AppsFlyerRequestEventArgs;
            AppsFlyerSDK.AppsFlyer.AFLog("AppsFlyerOnRequestResponse", $"status code - {args?.statusCode}");
        }
    }
}