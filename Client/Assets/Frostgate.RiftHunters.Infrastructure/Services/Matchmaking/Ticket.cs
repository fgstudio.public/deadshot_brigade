﻿using Frostgate.RiftHunters.Core;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Infrastructure.Services.Matchmaking
{
    public sealed class Ticket : ITicket
    {
        public string Id { get; }
        public ITicketOptions Options { get; }

        public Ticket([NotNull] string id, [NotNull] ITicketOptions options)
        {
            Id = id;
            Options = options;
        }
    }
}