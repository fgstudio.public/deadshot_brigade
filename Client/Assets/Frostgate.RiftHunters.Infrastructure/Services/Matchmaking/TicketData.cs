﻿using Frostgate.RiftHunters.Core;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Infrastructure.Services.Matchmaking
{
    public sealed class TicketData : ITicketData
    {
        public ITicket Ticket { get; }
        public TicketStatus Status { get; set; }
        public MatchAssignment Assignment { get; set; }

        public TicketData([NotNull] ITicket ticket)
        {
            Ticket = ticket;
        }
    }
}