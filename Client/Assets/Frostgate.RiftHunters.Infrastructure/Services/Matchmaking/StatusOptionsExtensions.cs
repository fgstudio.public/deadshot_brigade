﻿using System;
using Frostgate.RiftHunters.Core;
using Unity.Services.Matchmaker.Models;

namespace Frostgate.RiftHunters.Infrastructure.Services.Matchmaking
{
    public static class StatusOptionsExtensions
    {
        public static TicketStatus ToTicketStatus(this MultiplayAssignment.StatusOptions options)
        {
            return options switch
            {
                MultiplayAssignment.StatusOptions.Failed => TicketStatus.Failed,
                MultiplayAssignment.StatusOptions.Found => TicketStatus.Found,
                MultiplayAssignment.StatusOptions.Timeout => TicketStatus.Timeout,
                MultiplayAssignment.StatusOptions.InProgress => TicketStatus.InProgress,
                
                _ => throw new ArgumentOutOfRangeException(nameof(options), options,
                    $"Mapping from {nameof(MultiplayAssignment.StatusOptions)} '{options}' " +
                    $"to {nameof(TicketStatus)} not implemented.")
            };
        }
    }
}