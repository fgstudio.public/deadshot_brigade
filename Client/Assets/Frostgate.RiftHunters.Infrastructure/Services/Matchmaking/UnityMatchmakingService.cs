﻿using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Services.Auth;
using Frostgate.RiftHunters.Core.Services.PlayerData;
using JetBrains.Annotations;
using Unity.Services.Matchmaker;
using Unity.Services.Matchmaker.Models;
using IMatchmakingService = Frostgate.RiftHunters.Core.IMatchmakingService;

namespace Frostgate.RiftHunters.Infrastructure.Services.Matchmaking
{
    public sealed class UnityMatchmakingService : IMatchmakingService, IDisposable
    {
        private const int AutoCleaningIntervalMilliseconds = 60000; // TODO-SG: Вынести в Preferences/MatchmakingConfig
        private const int PlayerRank = 1; // TODO: Завести сущность игрока, хранить в нём связанные данные
        private const string QueueName = "Queue-1"; // TODO-SG: Вынести в Preferences/MatchmakingConfig/MissionConfig

        [NotNull] private IMatchmakerService UgsMatchmaker => MatchmakerService.Instance;

        [NotNull] private readonly IPlayerIdStorage _playerIdStorage;
        [NotNull] private readonly ILogger<UnityMatchmakingService> _logger;
        [NotNull] private readonly Dictionary<string, TicketData> _ticketDatas = new();

        [CanBeNull] private CancellationTokenSource _autoCleaningCts;

        [UsedImplicitly]
        public UnityMatchmakingService([NotNull] IPlayerIdStorage playerIdStorage,
            [NotNull] ILogger<UnityMatchmakingService> logger)
        {
            _playerIdStorage = playerIdStorage;
            _logger = logger;
        }

        public async UniTask<ITicket> CreateTicketAsync(ITicketOptions options)
        {
            PlayerCustomData playerData = CreatePlayerCustomData(options);
            CreateTicketOptions ugsOptions = CreateUgsTicketOptions(options);
            var players = new List<Player> { new(_playerIdStorage.PlayerId, playerData) };

            string ticketId;

            try
            {
                CreateTicketResponse response =
                    await UgsMatchmaker.CreateTicketAsync(players, ugsOptions);
                ticketId = response.Id;
            }
            catch (Exception e)
            {
                ticketId = string.Empty;

                _logger.Log("An exception occured when trying to create ticket.", LogLevel.Warning);
                _logger.LogException(e, LogLevel.Warning);
            }

            return new Ticket(ticketId, options);
        }

        public async UniTask<ITicketData> GetTicketDataAsync(ITicket ticket)
        {
            TicketData ticketData = GetOrCreateTicketData(ticket);

            try
            {
                TicketStatusResponse response = await UgsMatchmaker.GetTicketAsync(ticket.Id);
                FillTicketDataByStatusResponse(ticketData, response);
            }
            catch (Exception e)
            {
                ticketData.Status = TicketStatus.Failed;

                _logger.Log(
                    $"An exception occured when trying to get ticket data with {nameof(ticket.Id)} '{ticket.Id}'.",
                    LogLevel.Warning);
                _logger.LogException(e, LogLevel.Warning);
            }

            StartAutoCleaning();

            return ticketData;
        }

        public async UniTask DeleteTicketAsync(ITicket ticket)
        {
            try
            {
                await UgsMatchmaker.DeleteTicketAsync(ticket.Id);
            }
            catch (Exception e)
            {
                _logger.Log(
                    $"An exception occured when trying to delete ticket with {nameof(ticket.Id)} '{ticket.Id}'.",
                    LogLevel.Warning);
                _logger.LogException(e, LogLevel.Warning);
            }
        }

        public void Dispose() => StopAutoCleaning();

        private PlayerCustomData CreatePlayerCustomData(ITicketOptions options)
        {
            IMissionConfig mission = options.SelectedMission;
            UnitConfig unit = options.SelectedUnit;

            return new PlayerCustomData
            {
                Rank = PlayerRank,
                SelectedMissionId = mission.Id,
                SelectedUnitId = unit.Id
            };
        }

        private CreateTicketOptions CreateUgsTicketOptions(ITicketOptions options)
        {
            Dictionary<string, object> attributes = CreateUgsTicketAttributes(options);

            return new CreateTicketOptions(QueueName, attributes);
        }

        // Пока не требуется, но метод для механизма оставлю
        private Dictionary<string, object> CreateUgsTicketAttributes(ITicketOptions options)
        {
            Dictionary<string, object> attributes = new();

            return attributes;
        }


        private TicketData GetOrCreateTicketData(ITicket ticket)
        {
            string ticketId = ticket.Id;
            if (!_ticketDatas.TryGetValue(ticketId, out TicketData data))
            {
                data = new TicketData(ticket) { Status = TicketStatus.InProgress };
                _ticketDatas[ticketId] = data;
            }

            return data;
        }

        private void FillTicketDataByStatusResponse(TicketData data, TicketStatusResponse response)
        {
            if (response.Value is not MultiplayAssignment assignment)
            {
                data.Status = TicketStatus.InProgress;
                return;
            }

            TicketStatus status = assignment.Status.ToTicketStatus();
            data.Status = status;

            if (status != TicketStatus.Found)
                return;

            string matchId = assignment.MatchId;
            string serverIp = assignment.Ip;
            int? serverPort = assignment.Port;

            data.Assignment = new MatchAssignment(matchId, serverIp, serverPort);
        }


        private async void StartAutoCleaning()
        {
            if (_autoCleaningCts != null)
                return;

            _autoCleaningCts = new CancellationTokenSource();
            await AutoCleaningRoutine(_autoCleaningCts.Token);
        }

        private void StopAutoCleaning()
        {
            if (_autoCleaningCts == null)
                return;

            _autoCleaningCts.Cancel();
            _autoCleaningCts.Dispose();
            _autoCleaningCts = null;
        }

        private async UniTask AutoCleaningRoutine(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested || _ticketDatas.Count > 0)
            {
                await UniTask.Delay(AutoCleaningIntervalMilliseconds, true, cancellationToken: cancellationToken)
                    .SuppressCancellationThrow();
                _ticketDatas.Clear();
            }
        }
    }
}