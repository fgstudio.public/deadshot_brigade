﻿namespace Frostgate.RiftHunters.Infrastructure.Services.Matchmaking
{
    // Не инкапсулировать, не создавать конструкторы
    // Сериализуется/десериализуется сервисами Multiplay, в internal используется NewtonsoftJson
    public sealed class PlayerCustomData
    {
        public int Rank { get; set; }
        public string SelectedMissionId { get; set; }
        public string SelectedUnitId { get; set; }
    }
}