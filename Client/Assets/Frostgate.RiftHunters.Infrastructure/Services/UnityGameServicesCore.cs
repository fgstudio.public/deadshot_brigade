﻿using System;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Unity.Services.Core;
using Unity.Services.Core.Environments;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Infrastructure.Services.Save;

namespace Frostgate.RiftHunters.Infrastructure.Services
{
    public sealed class UnityGameServicesCore : IGameServicesCore
    {
        [NotNull] private readonly EnvironmentDataStorage _envStorage;
        [NotNull] private readonly IEnvironmentProvider _envProvider;
        [NotNull] private readonly ILocalDataStorage _localStorage;
        [NotNull] private readonly ILogger<UnityGameServicesCore> _logger;

        public UnityGameServicesCore([NotNull] IEnvironmentProvider envProvider, [NotNull] ILocalDataStorage localStorage,
            [NotNull] ILogger<UnityGameServicesCore> logger)
        {
            _envStorage = new EnvironmentDataStorage();
            _envProvider = envProvider;
            _localStorage = localStorage;
            _logger = logger;
        }

        public async UniTask InitializeAsync()
        {
            _logger.Log("Starting services initialization.");

            string env = _envProvider.GetEnvironment();
            _logger.Log($"Selected environment '{env}'.");

            try
            {
                bool envChanged = _envStorage.UpdateEnvironment(env);
                if (envChanged)
                    _localStorage.ClearData();

                InitializationOptions options = new();
                options.SetEnvironmentName(env);

                await UnityServices.InitializeAsync(options);

                _logger.Log("Services initialized.");
            }
            catch (Exception e)
            {
                _logger.LogError($"Initialization failed with error '{e.Message}'");
            }
        }
    }
}