﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Unity.Services.Economy;
using Unity.Services.Economy.Model;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Infrastructure.Services.Economy.UGS;
using Frostgate.RiftHunters.Infrastructure.Services.Economy.Fake;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.Mediation
{
    public sealed class EconomyPlayerBalancesMediator : IEconomyPlayerBalances
    {
        public event Action<string> CurrencyChanged
        {
            add => _fakeBalances.CurrencyChanged += value;
            remove => _fakeBalances.CurrencyChanged -= value;
        }

        private readonly UGSPlayerBalances _ugsBalances = new();
        private readonly FakePlayerBalances _fakeBalances = new();
        private readonly ILogger _logger = LoggerFactory.CreateLogger<EconomyPlayerBalancesMediator>();

        public async UniTask InitializeAsync()
        {
            _logger.Log("Initializing...");
            await RefreshCaches();
            _logger.Log("Initialized");
        }

        public UniTask<long> GetBalanceAsync(string currencyId) =>
            _fakeBalances.GetBalanceAsync(currencyId);

        public async UniTask SetBalanceAsync(string currencyId, long balance)
        {
            await _fakeBalances.SetBalanceAsync(currencyId, balance);
            await _ugsBalances.SetBalanceAsync(currencyId, balance);
        }

        public async UniTask IncrementBalanceAsync(string currencyId, int amount)
        {
            await _fakeBalances.IncrementBalanceAsync(currencyId, amount);
            await _ugsBalances.IncrementBalanceAsync(currencyId, amount);
        }

        public async UniTask DecrementBalanceAsync(string currencyId, int amount)
        {
            await _fakeBalances.DecrementBalanceAsync(currencyId, amount);
            await _ugsBalances.DecrementBalanceAsync(currencyId, amount);
        }

        private async UniTask RefreshCaches()
        {
            _logger.Log("Refreshing Caches...");
            List<PlayerBalance> balances;

            try
            {
                balances = await _ugsBalances.GetBalancesDataAsync();
            }
            catch (EconomyRateLimitedException e)
            {
                _logger.Log($"Waiting new trying after {e.RetryAfter}sec...");
                await UniTask.Delay(TimeSpan.FromSeconds(e.RetryAfter));

                balances = await _ugsBalances.GetBalancesDataAsync();
            }
            catch (Exception e)
            {
                var message = $"Refreshing Error: {e.Message}";
                _logger.LogError(message);
                return;
            }

            UpdateCache(balances);
            _logger.Log("Refreshed Caches");
        }

        private void UpdateCache([CanBeNull] List<PlayerBalance> balances)
        {
            _fakeBalances.Clear();
            _logger.Log("Cleared Caches");

            if (balances is { Count: > 0 })
                foreach (PlayerBalance balance in balances)
                    _fakeBalances.SetBalanceAsync(balance.CurrencyId, balance.Balance);
        }
    }
}