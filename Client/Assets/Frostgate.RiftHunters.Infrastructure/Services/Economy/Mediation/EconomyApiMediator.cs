﻿using System;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Infrastructure.Services.Economy.UGS;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.Mediation
{
    public sealed class EconomyApiMediator : IEconomyApi
    {
        private readonly UGSPurchases _purchases = new();
        private readonly UGSConfiguration _configuration = new();
        private readonly EconomyPlayerBalancesMediator _playerBalances = new();
        private readonly EconomyPlayerInventoryMediator _playerInventory = new();
        private readonly ILogger _logger = LoggerFactory.CreateLogger<EconomyApiMediator>();

        private bool _isInitialized;

        public IEconomyPurchases Purchases => _purchases;
        public IEconomyConfiguration Configuration => _configuration;
        public IEconomyPlayerBalances PlayerBalances => _playerBalances;
        public IEconomyPlayerInventory PlayerInventory => _playerInventory;

        public async UniTask InitializeAsync()
        {
            if (_isInitialized)
                throw new InvalidOperationException($"{nameof(EconomyApiMediator)} is already initialized!");

            _logger.Log("Initializing...");
            await _configuration.InitializeAsync();
            await _playerBalances.InitializeAsync();
            await _playerInventory.InitializeAsync();

            _isInitialized = true;
            _logger.Log("Initialized");
        }
    }
}