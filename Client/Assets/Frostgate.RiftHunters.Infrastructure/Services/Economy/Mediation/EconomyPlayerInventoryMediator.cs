﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Unity.Services.Economy;
using Unity.Services.Economy.Model;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Infrastructure.Services.Economy.UGS;
using Frostgate.RiftHunters.Infrastructure.Services.Economy.Fake;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.Mediation
{
    public sealed class EconomyPlayerInventoryMediator : IEconomyPlayerInventory
    {
        private readonly UGSPlayerInventory _ugsInventory = new();
        private readonly FakePlayerInventory _fakeInventory = new();
        private readonly ILogger _logger = LoggerFactory.CreateLogger<EconomyPlayerInventoryMediator>();

        public async UniTask InitializeAsync()
        {
            _logger.Log("Initializing...");
            await RefreshCaches();
            _logger.Log("Initialized");
        }

        public UniTask<bool> HasItemAsync(string inventoryItemId) =>
            _fakeInventory.HasItemAsync(inventoryItemId);

        public UniTask<int> GetItemsCountAsync(string inventoryItemId) =>
            _fakeInventory.GetItemsCountAsync(inventoryItemId);

        public UniTask<IEnumerable<string>> GetAllItemsAsync() =>
            _fakeInventory.GetAllItemsAsync();

        public UniTask<IEnumerable<string>> GetAllPlayerInventoryItemIdsAsync(string inventoryItemId) =>
            _fakeInventory.GetAllPlayerInventoryItemIdsAsync(inventoryItemId);

        public async UniTask AddInventoryItemAsync(string inventoryItemId)
        {
            PlayersInventoryItem inventoryItem = await _ugsInventory.AddInventoryItemAsync(inventoryItemId);
            _fakeInventory.AddInventoryItem(inventoryItem.InventoryItemId, inventoryItem.PlayersInventoryItemId);
        }

        public async UniTask DeleteInventoryItemAsync(string inventoryItemId, string playerInventoryItemId)
        {
            await _ugsInventory.DeleteInventoryItemAsync(inventoryItemId, playerInventoryItemId);
            await _fakeInventory.DeleteInventoryItemAsync(inventoryItemId, playerInventoryItemId);
        }

        private async UniTask RefreshCaches()
        {
            _logger.Log("Refreshing Caches...");
            List<PlayersInventoryItem> itemsData;

            try
            {
                itemsData = await _ugsInventory.GetAllItemsDataAsync();
            }
            catch (EconomyRateLimitedException e)
            {
                _logger.Log($"Waiting new pull trying after {e.RetryAfter}sec...");
                await UniTask.Delay(TimeSpan.FromSeconds(e.RetryAfter));

                itemsData = await _ugsInventory.GetAllItemsDataAsync();
            }
            catch (Exception e)
            {
                var message = $"Refreshing Error: {e.Message}";
                _logger.LogError(message);
                return;
            }

            UpdateCache(itemsData);
        }

        private void UpdateCache([CanBeNull] List<PlayersInventoryItem> itemsData)
        {
            _fakeInventory.Clear();
            _logger.Log("Cleared Caches");

            if (itemsData is { Count: > 0 })
                foreach (PlayersInventoryItem item in itemsData)
                    _fakeInventory.AddInventoryItem(item.InventoryItemId, item.PlayersInventoryItemId);
        }
    }
}