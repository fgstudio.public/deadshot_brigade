﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.Fake
{
    public sealed class FakeEconomyApi : IEconomyApi
    {
        public UniTask InitializeAsync() => UniTask.CompletedTask;
        public IEconomyConfiguration Configuration { get; } = new FakeConfiguration();
        public IEconomyPlayerBalances PlayerBalances { get; } = new FakePlayerBalances();
        public IEconomyPlayerInventory PlayerInventory { get; } = new FakePlayerInventory();
        public IEconomyPurchases Purchases { get; } = new FakePurchases();
    }
}