﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.Fake
{
    public sealed class FakePlayerBalances : IEconomyPlayerBalances
    {
        public event Action<string> CurrencyChanged;

        private readonly Dictionary<string, long> _balance = new();
        private readonly ILogger _logger = LoggerFactory.CreateLogger<FakePlayerBalances>();

        public void Clear() =>
            _balance.Clear();

        public async UniTask IncrementBalanceAsync(string currencyId, int amount)
        {
            long prevBalance = await GetBalanceAsync(currencyId);
            long newBalance = prevBalance + amount;
            _logger.Log($"Increasing Balance by {amount}: [\"{currencyId}\": {prevBalance} -> {newBalance}]...");
            await SetBalanceAsync(currencyId, newBalance);
        }

        public async UniTask DecrementBalanceAsync(string currencyId, int amount)
        {
            long prevBalance = await GetBalanceAsync(currencyId);
            long newBalance = prevBalance - amount;

            if (newBalance < 0)
                throw new ArgumentOutOfRangeException(nameof(newBalance), "Can't decrement balance to negative value");

            _logger.Log($"Decreasing Balance by {amount}: [\"{currencyId}\": {prevBalance} -> {newBalance}]...");
            await SetBalanceAsync(currencyId, newBalance);
        }

        public UniTask<long> GetBalanceAsync(string currencyId)
        {
            _balance.TryGetValue(currencyId, out long balance);
            _logger.Log($"Get Balance [{currencyId}: {balance}]");
            return UniTask.FromResult(balance);
        }

        public UniTask SetBalanceAsync(string currencyId, long balance)
        {
            if (balance < 0)
                throw new ArgumentOutOfRangeException(nameof(balance), "Balance can't be negative");

            _balance[currencyId] = balance;
            _logger.Log($"Set Balance [\"{currencyId}\": {balance}]");
            CurrencyChanged?.Invoke(currencyId);

            return UniTask.CompletedTask;
        }
    }
}