﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Unity.Services.Economy.Model;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.Fake
{
    public sealed class FakeConfiguration : IEconomyConfiguration
    {
        public string GetConfigAssignmentHash() => string.Empty;

        public IReadOnlyList<CurrencyDefinition> GetCurrencies() => Array.Empty<CurrencyDefinition>();
        public IReadOnlyList<InventoryItemDefinition> GetInventoryItems() => Array.Empty<InventoryItemDefinition>();
        public IReadOnlyList<VirtualPurchaseDefinition> GetVirtualPurchases() => Array.Empty<VirtualPurchaseDefinition>();
        public IReadOnlyList<RealMoneyPurchaseDefinition> GetRealMoneyPurchases() => Array.Empty<RealMoneyPurchaseDefinition>();

        public CurrencyDefinition GetCurrency(string id) =>
            JsonConvert.DeserializeObject<CurrencyDefinition>(
                $"{{ \"id\": \"{id}\", \"name\": \"\", \"type\": \"Currency\", \"initial\": 0, \"max\": 100 }}");

        public InventoryItemDefinition GetInventoryItem(string id) =>
            JsonConvert.DeserializeObject<InventoryItemDefinition>(
                $"{{ \"id\": \"{id}\", \"name\": \"\", \"type\": \"InventoryItem\" }}");

        public VirtualPurchaseDefinition GetVirtualPurchase(string id) =>
            JsonConvert.DeserializeObject<VirtualPurchaseDefinition>(
                $"{{ \"id\": \"{id}\", \"name\": \"\", \"type\": \"VirtualPurchase\", \"costs\": {{}}, \"rewards\": {{}} }}");

        public RealMoneyPurchaseDefinition GetRealMoneyPurchase(string id) =>
            JsonConvert.DeserializeObject<RealMoneyPurchaseDefinition>(
                $"{{ \"id\": \"{id}\", \"name\": \"\", \"type\": \"RealMoneyPurchase\", \"storeIdentifiers\": \"\", \"rewards\": {{}} }}");
    }
}