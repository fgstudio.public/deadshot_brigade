﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Unity.Services.Economy;
using Unity.Services.Economy.Model;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.Fake
{
    public sealed class FakePurchases : IEconomyPurchases
    {
        private readonly ILogger _logger = LoggerFactory.CreateLogger<FakePurchases>();

        public UniTask<MakeVirtualPurchaseResult> MakeVirtualPurchaseAsync(string virtualPurchaseId, MakeVirtualPurchaseOptions options = null)
        {
            var currencyExchangeItems = new List<CurrencyExchangeItem>();
            var inventoryExchangeItems = new List<InventoryExchangeItem>();
            var costs = new Costs(currencyExchangeItems, inventoryExchangeItems);
            var rewards = new Rewards(currencyExchangeItems, inventoryExchangeItems);
            var makeVirtualPurchaseResult = new MakeVirtualPurchaseResult(costs, rewards);

            _logger.Log($"Virtual Purchase: \"{virtualPurchaseId}\"");
            return UniTask.FromResult(makeVirtualPurchaseResult);
        }

        public UniTask<RedeemAppleAppStorePurchaseResult> RedeemAppleAppStorePurchaseAsync(RedeemAppleAppStorePurchaseArgs args)
        {
            var appleStore = new AppleStore("", "", args.Receipt);
            var appleVerification = new AppleVerification(AppleVerification.StatusOptions.INVALIDVERIFICATIONFAILED, appleStore);

            var currencyExchangeItems = new List<CurrencyExchangeItem>();
            var inventoryExchangeItems = new List<InventoryExchangeItem>();
            var rewards = new Rewards(currencyExchangeItems, inventoryExchangeItems);

            var redeemAppleAppStorePurchaseResult = new RedeemAppleAppStorePurchaseResult(appleVerification, rewards);

            _logger.Log($"Apple Store Purchase: \"{args.RealMoneyPurchaseId}\"");
            return UniTask.FromResult(redeemAppleAppStorePurchaseResult);
        }

        public UniTask<RedeemGooglePlayPurchaseResult> RedeemGooglePlayPurchaseAsync(RedeemGooglePlayStorePurchaseArgs args)
        {
            var googleStore = new GoogleStore(args.PurchaseData);
            var googleVerification = new GoogleVerification(GoogleVerification.StatusOptions.INVALIDVERIFICATIONFAILED, googleStore);

            var currencyExchangeItems = new List<CurrencyExchangeItem>();
            var inventoryExchangeItems = new List<InventoryExchangeItem>();
            var rewards = new Rewards(currencyExchangeItems, inventoryExchangeItems);

            var redeemGooglePlayPurchaseResult = new RedeemGooglePlayPurchaseResult(googleVerification, rewards);

            _logger.Log($"Google Play Purchase:\"{args.RealMoneyPurchaseId}\"");
            return UniTask.FromResult(redeemGooglePlayPurchaseResult);
        }
    }
}