﻿using System;
using System.Linq;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.Fake
{
    public sealed class FakePlayerInventory : IEconomyPlayerInventory
    {
        private readonly Dictionary<string, List<string>> _inventory = new();
        private readonly ILogger _logger = LoggerFactory.CreateLogger<FakePlayerInventory>();

        public void Clear() =>
            _inventory.Clear();

        public async UniTask<bool> HasItemAsync(string inventoryItemId) =>
            await GetItemsCountAsync(inventoryItemId) > 0;

        public UniTask<int> GetItemsCountAsync(string inventoryItemId) =>
            UniTask.FromResult(_inventory.TryGetValue(inventoryItemId, out List<string> list) ? list.Count : default);

        public UniTask<IEnumerable<string>> GetAllItemsAsync()
        {
            var items = new List<string>();
            foreach (var value in _inventory)
                for (int i = 0; i < value.Value.Count; i++)
                    items.Add(value.Key);

            return UniTask.FromResult((IEnumerable<string>)items);
        }

        public UniTask<IEnumerable<string>> GetAllPlayerInventoryItemIdsAsync(string inventoryItemId) =>
            UniTask.FromResult(_inventory.TryGetValue(inventoryItemId, out List<string> playerItemIds)
                ? playerItemIds : Enumerable.Empty<string>());

        UniTask IEconomyPlayerInventory.AddInventoryItemAsync(string inventoryItemId)
        {
            AddInventoryItem(inventoryItemId, Guid.NewGuid().ToString());
            return UniTask.CompletedTask;
        }

        public UniTask DeleteInventoryItemAsync(string inventoryItemId, string playerInventoryItemId)
        {
            if (_inventory.TryGetValue(inventoryItemId, out List<string> playerItemIds))
            {
                playerItemIds.Remove(playerInventoryItemId);
                _logger.Log($"Removed [\"{inventoryItemId}\": \"{playerInventoryItemId}\"]");

                if (playerItemIds.Count == 0)
                {
                    _inventory.Remove(inventoryItemId);
                    _logger.Log($"Removed list for \"{inventoryItemId}\"");
                }
            }

            return UniTask.CompletedTask;
        }

        public void AddInventoryItem(string inventoryItemId, string playerInventoryItemId)
        {
            if (!_inventory.ContainsKey(inventoryItemId))
            {
                _inventory[inventoryItemId] = new List<string>();
                _logger.Log($"Added List for \"{inventoryItemId}\"");
            }

            _inventory[inventoryItemId].Add(playerInventoryItemId);
            _logger.Log($"Added [\"{inventoryItemId}\": \"{playerInventoryItemId}\"]");
        }
    }
}