﻿using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.UGS
{
    public sealed class UgsEconomyApi : IEconomyApi
    {
        public UniTask InitializeAsync() => UniTask.CompletedTask;
        public IEconomyConfiguration Configuration { get; } = new UGSConfiguration();
        public IEconomyPlayerBalances PlayerBalances { get; } = new UGSPlayerBalances();
        public IEconomyPlayerInventory PlayerInventory { get; } = new UGSPlayerInventory();
        public IEconomyPurchases Purchases { get; } = new UGSPurchases();
    }
}