﻿using System.Linq;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Unity.Services.Economy;
using Unity.Services.Economy.Model;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.UGS
{
    public sealed class UGSPlayerInventory : IEconomyPlayerInventory
    {
        private static IEconomyPlayerInventoryApiClient PlayerInventory => EconomyService.Instance.PlayerInventory;
        private static ILogger Logger { get; } = LoggerFactory.CreateLogger<UGSPlayerInventory>();

        public async UniTask<bool> HasItemAsync(string inventoryItemId)
        {
            return await GetItemsCountAsync(inventoryItemId) > 0;
        }

        public async UniTask<int> GetItemsCountAsync(string inventoryItemId)
        {
            GetInventoryResult inventoryResult = await GetInventoryItemData(inventoryItemId);
            return inventoryResult.PlayersInventoryItems.Count;
        }

        public async UniTask<IEnumerable<string>> GetAllPlayerInventoryItemIdsAsync(string inventoryItemId)
        {
            GetInventoryResult inventoryResult = await GetInventoryItemData(inventoryItemId);
            return inventoryResult.PlayersInventoryItems.Select(i => i.PlayersInventoryItemId);
        }

        public async UniTask<IEnumerable<string>> GetAllItemsAsync()
        {
            List<PlayersInventoryItem> playersInventoryItems = await GetAllItemsDataAsync();
            return playersInventoryItems.Select(i => i.InventoryItemId);
        }

        public async UniTask<List<PlayersInventoryItem>> GetAllItemsDataAsync()
        {
            GetInventoryOptions options = new(){ ItemsPerFetch = 100 };

            Logger.Log($"Getting All Items with {options.ItemsPerFetch} items per fetch...");
            GetInventoryResult inventoryResult = await PlayerInventory.GetInventoryAsync(options);
            Logger.Log("Gotten All Items");

            return inventoryResult.PlayersInventoryItems;
        }

        UniTask IEconomyPlayerInventory.AddInventoryItemAsync(string inventoryItemId) =>
            AddInventoryItemAsync(inventoryItemId);

        public async UniTask<PlayersInventoryItem> AddInventoryItemAsync(string inventoryItemId)
        {
            Logger.Log($"Adding \"{inventoryItemId}\"...");
            PlayersInventoryItem inventoryItem = await PlayerInventory.AddInventoryItemAsync(inventoryItemId);
            Logger.Log($"Added [\"{inventoryItem.InventoryItemId}\": \"{inventoryItem.PlayersInventoryItemId}\"]");

            return inventoryItem;
        }

        public async UniTask DeleteInventoryItemAsync(string inventoryItemId, string playerInventoryItemId)
        {
            Logger.Log($"Deleting [\"{inventoryItemId}\": \"{playerInventoryItemId}\"]...");
            await PlayerInventory.DeletePlayersInventoryItemAsync(playerInventoryItemId);
            Logger.Log($"Deleted [\"{inventoryItemId}\": \"{playerInventoryItemId}\"]");
        }

        private async UniTask<GetInventoryResult> GetInventoryItemData(string inventoryItemId)
        {
            GetInventoryOptions options = new() { InventoryItemIds = new List<string>() { inventoryItemId}};

            Logger.Log($"Getting data for \"{inventoryItemId}\"...");
            GetInventoryResult inventoryResult = await PlayerInventory.GetInventoryAsync(options);
            Logger.Log($"Gotten data for \"{inventoryItemId}\"...");

            return inventoryResult;
        }
    }
}