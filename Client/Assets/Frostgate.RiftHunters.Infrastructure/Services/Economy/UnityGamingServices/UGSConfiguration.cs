﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Unity.Services.Economy;
using Unity.Services.Economy.Model;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.UGS
{
    [UsedImplicitly]
    public sealed class UGSConfiguration : IEconomyConfiguration
    {
        private static IEconomyConfigurationApiClient Configuration => EconomyService.Instance.Configuration;
        private static ILogger Logger => LoggerFactory.CreateLogger<UGSConfiguration>();

        public async UniTask InitializeAsync()
        {
            try
            {
                Logger.Log("Syncing...");
                await Configuration.SyncConfigurationAsync();
                Logger.Log("Synced");
            }
            catch(Exception ex)
            {
                Logger.LogError($"Syncing Error: {ex.Message}");
            }
        }

        public string GetConfigAssignmentHash() => Configuration.GetConfigAssignmentHash();
        public IReadOnlyList<CurrencyDefinition> GetCurrencies() => Configuration.GetCurrencies();
        public IReadOnlyList<InventoryItemDefinition> GetInventoryItems() => Configuration.GetInventoryItems();
        public IReadOnlyList<VirtualPurchaseDefinition> GetVirtualPurchases() => Configuration.GetVirtualPurchases();
        public IReadOnlyList<RealMoneyPurchaseDefinition> GetRealMoneyPurchases() => Configuration.GetRealMoneyPurchases();
        public CurrencyDefinition GetCurrency(string id) => Configuration.GetCurrency(id);
        public InventoryItemDefinition GetInventoryItem(string id) => Configuration.GetInventoryItem(id);
        public VirtualPurchaseDefinition GetVirtualPurchase(string id) => Configuration.GetVirtualPurchase(id);
        public RealMoneyPurchaseDefinition GetRealMoneyPurchase(string id) => Configuration.GetRealMoneyPurchase(id);
    }
}