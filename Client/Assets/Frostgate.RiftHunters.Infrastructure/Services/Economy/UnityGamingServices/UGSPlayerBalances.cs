﻿using System;
using System.Linq;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Unity.Services.Economy;
using Unity.Services.Economy.Model;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.UGS
{
    public sealed class UGSPlayerBalances : IEconomyPlayerBalances
    {
        private static IEconomyPlayerBalancesApiClient PlayerBalances => EconomyService.Instance.PlayerBalances;
        private static ILogger Logger { get; } = LoggerFactory.CreateLogger<UGSPlayerBalances>();

        public event Action<string> CurrencyChanged
        {
            add => PlayerBalances.BalanceUpdated += value;
            remove => PlayerBalances.BalanceUpdated -= value;
        }

        public async UniTask<long> GetBalanceAsync(string currencyId)
        {
            List<PlayerBalance> balances = await GetBalancesDataAsync();
            long balance = balances
                .FirstOrDefault(b => b.CurrencyId == currencyId)?.Balance ?? default;
            Logger.Log($"Gotten Balance: [\"{currencyId}\": {balance}]");

            return balance;
        }

        public async UniTask<List<PlayerBalance>> GetBalancesDataAsync()
        {
            GetBalancesOptions options = new(){ ItemsPerFetch = 100 };

            Logger.Log($"Getting Balances with {options.ItemsPerFetch} items per fetch...");
            GetBalancesResult balanceResult = await PlayerBalances.GetBalancesAsync(options);
            Logger.Log("Gotten Balances");

            return balanceResult.Balances;
        }

        public async UniTask SetBalanceAsync(string currencyId, long balance)
        {
            if (balance < 0)
                throw new ArgumentOutOfRangeException(nameof(balance), "Balance can't be negative");

            Logger.Log($"Setting Balance [\"{currencyId}\": {balance}]...");
            PlayerBalance playerBalance = await PlayerBalances.SetBalanceAsync(currencyId, balance);
            Logger.Log($"Set Balance [\"{playerBalance.CurrencyId}\": {playerBalance.Balance}]");
        }

        public async UniTask IncrementBalanceAsync(string currencyId, int amount)
        {
            Logger.Log($"Increasing Balance \"{currencyId}\" by {amount}...");
            PlayerBalance playerBalance = await PlayerBalances.IncrementBalanceAsync(currencyId, amount);
            Logger.Log($"Increased Balance by {amount}: [\"{playerBalance.CurrencyId}\": {playerBalance.Balance}]");
        }

        public async UniTask DecrementBalanceAsync(string currencyId, int amount)
        {
            Logger.Log($"Decreasing Balance \"{currencyId}\" by {amount}...");
            PlayerBalance playerBalance = await PlayerBalances.DecrementBalanceAsync(currencyId, amount);
            Logger.Log($"Decreased Balance by {amount}: [\"{playerBalance.CurrencyId}\": {playerBalance.Balance}]");
        }
    }
}