﻿using System;
using Cysharp.Threading.Tasks;
using Unity.Services.Economy;
using Unity.Services.Economy.Model;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Infrastructure.Services.Economy.UGS
{
    // TODO: после очередной покупки нужно обновлять кэши баланса и инвентаря
    public sealed class UGSPurchases : IEconomyPurchases
    {
        private static IEconomyPurchasesApiClientApi Purchases => EconomyService.Instance.Purchases;
        private static ILogger Logger { get; } = LoggerFactory.CreateLogger<UGSPurchases>();

        public async UniTask<MakeVirtualPurchaseResult> MakeVirtualPurchaseAsync(string virtualPurchaseId, MakeVirtualPurchaseOptions options = null)
        {
            MakeVirtualPurchaseResult purchaseResult = null;

            try
            {
                Logger.Log($"Making Virtual Purchase \"{virtualPurchaseId}\"...");
                purchaseResult = await Purchases.MakeVirtualPurchaseAsync(virtualPurchaseId, options);
                Logger.Log($"Made Virtual Purchase \"{virtualPurchaseId}\"");
            }
            catch (Exception ex)
            {
                Logger.LogError($"Virtual Purchase Error: {ex.Message}");
            }

            return purchaseResult;
        }

        public async UniTask<RedeemAppleAppStorePurchaseResult> RedeemAppleAppStorePurchaseAsync(RedeemAppleAppStorePurchaseArgs args)
        {
            RedeemAppleAppStorePurchaseResult purchaseResult = null;

            try
            {
                Logger.Log($"Making App Store Purchase \"{args.RealMoneyPurchaseId}\"...");
                purchaseResult = await Purchases.RedeemAppleAppStorePurchaseAsync(args);
                Logger.Log($"Made App Store Purchase \"{args.RealMoneyPurchaseId}\"");
            }
            catch (Exception ex)
            {
                Logger.LogError($"App Store Purchase Error: {ex.Message}");
            }

            return purchaseResult;
        }

        public async UniTask<RedeemGooglePlayPurchaseResult> RedeemGooglePlayPurchaseAsync(RedeemGooglePlayStorePurchaseArgs args)
        {
            RedeemGooglePlayPurchaseResult purchaseResult = null;

            try
            {
                Logger.Log($"Making Google Play Purchase \"{args.RealMoneyPurchaseId}\"...");
                purchaseResult = await Purchases.RedeemGooglePlayPurchaseAsync(args);
                Logger.Log($"Made Google Play Purchase \"{args.RealMoneyPurchaseId}\"");
            }
            catch (Exception ex)
            {
                Logger.LogError($"Google Play Purchase Error: {ex.Message}");
            }

            return purchaseResult;
        }
    }
}