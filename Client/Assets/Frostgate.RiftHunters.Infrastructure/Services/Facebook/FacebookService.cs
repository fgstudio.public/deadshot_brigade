using Cysharp.Threading.Tasks;
using Facebook.Unity;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Services.Facebook
{
    public class FacebookService
    {
        private readonly ILogger<FacebookService> _logger;
        private UniTaskCompletionSource _tcs;

        public FacebookService(ILogger<FacebookService> logger)
        {
            _logger = logger;
        }

        public UniTask InitializeAsync()
        {
            _logger.Log("Start initializing Facebook SDK.");

            _tcs = new UniTaskCompletionSource();

            if (!FB.IsInitialized)
                FB.Init(OnInitialize);
            else
                FB.ActivateApp();

            return _tcs.Task;
        }

        private void OnInitialize()
        {
            if (FB.IsInitialized)
                FB.ActivateApp();

            _logger.Log($"Facebook SDK initialized - {FB.IsInitialized}");

            _tcs.TrySetResult();
        }
    }
}