﻿using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Core.Scripts.Services.Auth;
using Frostgate.RiftHunters.Core.Services.Auth;
using Frostgate.RiftHunters.Core.Services.PlayerData;
using Frostgate.RiftHunters.Infrastructure.Services.DGSServices;
using Frostgate.RiftHunters.Infrastructure.Services.Analytics.AppsFlyer;
using Frostgate.RiftHunters.Infrastructure.Services.Economy.Fake;
using Frostgate.RiftHunters.Infrastructure.Services.Economy.Mediation;
using Frostgate.RiftHunters.Infrastructure.Services.Matchmaking;
using Frostgate.RiftHunters.Infrastructure.Services.Facebook;
using Frostgate.RiftHunters.Infrastructure.Services.Analytics.AppsFlyer;
using Frostgate.RiftHunters.Infrastructure.Services.Save;
using Frostgate.RiftHunters.Meta.Economy;
using Zenject;

namespace Frostgate.RiftHunters.Infrastructure.Services
{
    public sealed class ServicesInstaller : Installer
    {
        public override void InstallBindings()
        {
            var prefsStorage = new PlayerPrefsDataStorage();

            Container.BindInterfacesAndSelfTo<UnityGameServicesCore>().AsSingle().WithArguments(prefsStorage);
            Container.BindInterfacesAndSelfTo<PlayerIdStorage>().AsSingle();

            Container.Bind<IAuthServiceFactory>().To<AuthServiceFactory>().AsSingle();
            Container.Bind<IAuthServiceProvider>().To<AuthServiceProvider>().AsSingle();
            Container.Bind<IAuthServiceFacade>().To<AuthServiceFacade>().AsSingle();

            Container.Bind<IEconomyApi>().FromMethod(CreateEconomyApi).AsSingle();

            Container.BindInterfacesAndSelfTo<SaveServiceMediator>().AsSingle()
                .WithArguments(new DataCache(), prefsStorage, new UnityDataStorage());

            Container.Bind<AppsFlyerService>().AsSingle();
            Container.Bind<FacebookService>().AsSingle();

            Container.BindInterfacesAndSelfTo<UnityMatchmakingService>().AsSingle();

            BindDGSServices();
        }

        private void BindDGSServices()
        {
            // DLL Multiplay может быть использована только в редакторе или UNITY_SERVER
#if UNITY_SERVER || UNITY_EDITOR
            Container.BindInterfacesAndSelfTo<MultiplayServerDataQueryService>().AsSingle();
            Container.Bind<IServerAllocationService>().To<MultiplayServerAllocationService>().AsSingle();
            Container.BindInterfacesAndSelfTo<UnityServerBackfillService>().AsSingle();
#else
            Container.Bind<IServerDataQueryService>().To<FakeServerDataQueryService>().AsSingle();
            Container.Bind<IServerAllocationService>().To<FakeServerAllocationService>().AsSingle();
            Container.Bind<IServerBackfillService>().To<FakeServerBackfillService>().AsSingle();
#endif

            Container.Bind<IServerConfigurationProvider>().To<CmdLineServerConfigurationProvider>().AsSingle();
        }

        private IEconomyApi CreateEconomyApi()
        {
            var preferences = Container.Resolve<Preferences>();
            NetworkMode networkMode = preferences.NetworkMode;

            return networkMode.HasEnvironment
                ? new EconomyApiMediator()
                : new FakeEconomyApi();
        }
    }
}