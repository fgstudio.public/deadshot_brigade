﻿using System;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Services.Auth;
using Unity.Services.Authentication;
using Unity.Services.Core;

#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#endif

namespace Frostgate.RiftHunters.Infrastructure.Services.Auth
{
    public class GooglePlayAuthService : IAuthService
    {
        public async UniTask<AuthResult> SignInAsync()
        {
#if UNITY_ANDROID

            PlayGamesPlatform.Activate();

            SignInStatus signInStatus = await AuthenticateOnGooglePlayGamesAsync();
            if (signInStatus != SignInStatus.Success)
                return AuthResult.FromError(
                    $"Failed to authenticate on GooglePlayGames. Sign in status - {signInStatus}");

            string authCode = await RequestGooglePlayGamesAuthCodeAsync();
            if (string.IsNullOrEmpty(authCode))
                return AuthResult.FromError("Empty auth code");

            Exception exception = await SignInWithGooglePlayGamesAsync(authCode);
            if (exception != null)
                return AuthResult.FromError(exception.Message);

            return AuthResult.FromResult(AuthenticationService.Instance.PlayerId);
#endif

            return AuthResult.FromError("Invalid platform");
        }


#if UNITY_ANDROID

        private UniTask<SignInStatus> AuthenticateOnGooglePlayGamesAsync()
        {
            var tcs = new UniTaskCompletionSource<SignInStatus>();
            PlayGamesPlatform.Instance.Authenticate(OnAuthComplete);

            return tcs.Task;

            void OnAuthComplete(SignInStatus signInStatus) => tcs.TrySetResult(signInStatus);
        }

        private UniTask<string> RequestGooglePlayGamesAuthCodeAsync()
        {
            var tcs = new UniTaskCompletionSource<string>();

            PlayGamesPlatform.Instance.RequestServerSideAccess(true, OnResponse);

            return tcs.Task;

            void OnResponse(string code) => tcs.TrySetResult(code);
        }

        private async UniTask<Exception> SignInWithGooglePlayGamesAsync(string authCode)
        {
            try
            {
                await AuthenticationService.Instance.SignInWithGooglePlayGamesAsync(authCode);
            }
            catch (AuthenticationException ex)
            {
                return ex;
            }
            catch (RequestFailedException ex)
            {
                return ex;
            }

            return null;
        }

#endif

        public void ClearSessionToken()
        {
        }
    }
}