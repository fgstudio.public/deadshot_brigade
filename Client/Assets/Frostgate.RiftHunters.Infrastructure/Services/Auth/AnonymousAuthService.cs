﻿using System;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Core.Services.Auth;
using JetBrains.Annotations;
using Unity.Services.Core;
using Unity.Services.Authentication;

namespace Frostgate.RiftHunters.Infrastructure.Services.Auth
{
    public sealed class AnonymousAuthService : IAuthService
    {
        [NotNull] private readonly ILogger _logger;
        
        public AnonymousAuthService([NotNull] ILogger<AnonymousAuthService> logger)
        {
            _logger = logger;
        }

        public async UniTask<AuthResult> SignInAsync()
        {
            bool isSuccess;
            string error = "";

            try
            {
                await AuthenticationService.Instance.SignInAnonymouslyAsync();
                isSuccess = true;
            }
            catch (AuthenticationException authEx)
            {
                error = $"{nameof(AuthenticationException)} Code: {authEx.ErrorCode}. Message: {authEx.Message}.";
                isSuccess = false;
            }
            catch (RequestFailedException requestEx)
            {
                error = $"{nameof(AuthenticationException)} Code: {requestEx.ErrorCode}. Message: {requestEx.Message}.";
                isSuccess = false;
            }
            catch (Exception ex)
            {
                error = $"{nameof(AuthenticationException)} Message: {ex.Message}";
                isSuccess = false;
            }

            return isSuccess ? 
                AuthResult.FromResult(AuthenticationService.Instance.PlayerId) : 
                AuthResult.FromError(error);
        }

        public void ClearSessionToken()
        {
            try
            {
                AuthenticationService.Instance.ClearSessionToken();
                _logger.Log("Session Token was Cleared");
            }
            catch (AuthenticationException authEx)
            {
                _logger.LogError($"Session Token Clearing Failed with {nameof(AuthenticationException)}: " +
                                 $"Code: {authEx.ErrorCode}. Message: {authEx.Message}.");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Session Token Clearing Failed with {nameof(Exception)}: {ex.Message}");
            }
        }
    }
}