﻿using Frostgate.RiftHunters.Core;
using UnityEngine;
using ILogger = Frostgate.RiftHunters.Core.ILogger;

namespace Frostgate.RiftHunters.Infrastructure.Services
{
    public sealed class EnvironmentDataStorage
    {
        private const string PrefsKey = "Environment";

        private readonly ILogger _logger = LoggerFactory.CreateLogger<EnvironmentDataStorage>();

        public bool UpdateEnvironment(string environment)
        {
            string loadedEnvironment = LoadEnvironment();

            bool changed = environment != loadedEnvironment;
            if (changed) SetEnvironment(environment);

            return changed;
        }

        public string LoadEnvironment()
        {
            string key = PrefsKey;
            string json = PlayerPrefs.GetString(key, string.Empty);
            _logger.Log($"Loaded [{key}: {json}]");
            return json;
        }

        private void SetEnvironment(string environment)
        {
            string key = PrefsKey;
            PlayerPrefs.SetString(key, environment);
            _logger.Log($"Set [{key}: \"{environment}\"");
        }
    }
}