﻿using Zenject;

namespace Frostgate.RiftHunters.Infrastructure.ObjectFactory
{
    public sealed class ObjectFactoryInstaller : Installer
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesTo<ZenjectObjectFactory>().AsTransient().CopyIntoAllSubContainers();
        }
    }
}
