﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.ObjectFactory
{
    public sealed class ZenjectObjectFactory : IObjectFactory, IComponentFactory, IPrefabFactory
    {
        private readonly DiContainer _container;

        public ZenjectObjectFactory(DiContainer container)
        {
            _container = container;
        }

        T IObjectFactory.Instantiate<T>() => _container.Instantiate<T>();

        T IObjectFactory.Instantiate<T>(IEnumerable<object> ctorArgs) => _container.Instantiate<T>(ctorArgs);


        T IComponentFactory.Instantiate<T>(GameObject target) => _container.InstantiateComponent<T>(target);

        T IComponentFactory.Instantiate<T>(GameObject target, IEnumerable<object> ctorArgs) =>
            _container.InstantiateComponent<T>(target, ctorArgs);


        GameObject IPrefabFactory.Instantiate(Object prefab) => _container.InstantiatePrefab(prefab);

        GameObject IPrefabFactory.Instantiate(Object prefab, Transform parent) =>
            _container.InstantiatePrefab(prefab, parent);

        GameObject IPrefabFactory.Instantiate(Object prefab, Vector3 position, Quaternion rotation, Transform parent) =>
            _container.InstantiatePrefab(prefab, position, rotation, parent);

        T IPrefabFactory.Instantiate<T>(Object prefab) => _container.InstantiatePrefabForComponent<T>(prefab);

        T IPrefabFactory.Instantiate<T>(Object prefab, Transform parent) =>
            _container.InstantiatePrefabForComponent<T>(prefab, parent);

        T IPrefabFactory.Instantiate<T>(Object prefab, Vector3 position, Quaternion rotation, Transform parent) =>
            _container.InstantiatePrefabForComponent<T>(prefab, position, rotation, parent);
    }
}
