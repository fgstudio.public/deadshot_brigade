﻿using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Core.Battle.Client.Tutorial.Services;
using Frostgate.RiftHunters.Infrastructure.Definitions;
using Frostgate.RiftHunters.Infrastructure.Services;
using Frostgate.RiftHunters.Meta;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Frostgate.RiftHunters.Infrastructure
{
    [CreateAssetMenu(menuName = AssetMenus.RiftHunters.Menu + "/" + nameof(Preferences),
        fileName = nameof(Preferences))]
    public sealed class Preferences : ScriptableObject, IPreferences, IEnvironmentProvider, IConfigLabelProvider,
        IFirstTutorial
    {
        [SerializeField, AssetSelector, AssetsOnly]
        private NetworkMode _networkMode;

        [SerializeField, AssetSelector, AssetsOnly]
        private BattleComponentRoster _battleComponentRoster;

        [SerializeField] private CampaignRoster _campaignRoster;
        [SerializeField, Required] private MissionConfig _firstTutorialMissionConfig;
        [SerializeField] private AssetLabelReference _configsLabel;

        public NetworkMode NetworkMode
        {
            get => _networkMode;
            set => _networkMode = value;
        }

        public BattleComponentRoster BattleComponentRoster => _battleComponentRoster;

        public bool IsDebugModeEnabled =>
            _networkMode.Stage == StageType.Dev || Application.isEditor || Debug.isDebugBuild;

        public CampaignRoster CampaignRoster => _campaignRoster;
        public MissionConfig FirstTutorialConfig => _firstTutorialMissionConfig;
        string IEnvironmentProvider.GetEnvironment() => _networkMode.Environment;
        AssetLabelReference IConfigLabelProvider.GetLabel() => _configsLabel;
    }
}