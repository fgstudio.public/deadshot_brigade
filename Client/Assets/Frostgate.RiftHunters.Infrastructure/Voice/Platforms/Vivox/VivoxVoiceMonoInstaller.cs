﻿using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

namespace Frostgate.RiftHunters.Infrastructure.Voice.Vivox
{
    public sealed class VivoxVoiceMonoInstaller : MonoInstaller
    {
        [SerializeField] private bool _useVivoxConfig;

        [SerializeField, BoxGroup("Vivox Config", VisibleIf = nameof(_useVivoxConfig)), HideLabel]
        private VivoxConfigBuilder _configBuilder;


        public override void InstallBindings() =>
            Container.Install<VivoxVoiceInstaller>(new object[] { _useVivoxConfig, _configBuilder });
    }
}