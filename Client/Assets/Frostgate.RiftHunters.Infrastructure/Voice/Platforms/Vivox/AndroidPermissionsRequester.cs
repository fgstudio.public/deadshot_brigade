﻿#if !UNITY_EDITOR && UNITY_ANDROID

using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine.Android;

namespace Frostgate.RiftHunters.Infrastructure.Voice.Vivox
{
    internal sealed class AndroidPermissionsRequester : IPermissionsRequester
    {
        private const int Android12SDKLevel = 31; // https://apilevels.com/

        private const string BluetoothPermission = "android.permission.BLUETOOTH_CONNECT";

        private readonly string[] _requiredPermissions = AndroidHelper.SdkVersion >= Android12SDKLevel
            ? new[]
            {
                Permission.Microphone,
                BluetoothPermission
            }
            : new[]
            {
                Permission.Microphone,
            };

        private readonly PermissionCallbacks _permissionCallbacks = new();

        public UniTask RequestIfNeededAsync()
        {
            var permissions = GetNotGrantedPermissions();
            if (permissions.Length == 0)
                return UniTask.CompletedTask;

            var tcs = new UniTaskCompletionSource();

            _permissionCallbacks.PermissionDenied += OnPermissionsGrantedOrDenied;
            _permissionCallbacks.PermissionGranted += OnPermissionsGrantedOrDenied;
            _permissionCallbacks.PermissionDeniedAndDontAskAgain += OnPermissionsGrantedOrDenied;

            Permission.RequestUserPermissions(permissions, _permissionCallbacks);

            void OnPermissionsGrantedOrDenied(string _)
            {
                _permissionCallbacks.PermissionDenied -= OnPermissionsGrantedOrDenied;
                _permissionCallbacks.PermissionGranted -= OnPermissionsGrantedOrDenied;
                _permissionCallbacks.PermissionDeniedAndDontAskAgain -= OnPermissionsGrantedOrDenied;

                tcs.TrySetResult();
            }

            return tcs.Task;
        }

        private string[] GetNotGrantedPermissions()
        {
            var permissions = new List<string>();

            foreach (string permission in _requiredPermissions)
            {
                if (!Permission.HasUserAuthorizedPermission(permission))
                    permissions.Add(permission);
            }

            return permissions.ToArray();
        }
    }
}
#endif