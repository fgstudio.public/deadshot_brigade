﻿#if !UNITY_EDITOR && UNITY_ANDROID

using System;
using UnityEngine;

namespace Frostgate.RiftHunters.Infrastructure.Voice.Vivox
{
    
    // TODO: Это копия хелпера из модуля вибрации, т.к. применил asmdef, нет возможности получить доступ к Assembly-CSharp
    public static class AndroidHelper
    {
        public const int BadSdkVersion = -0xFFFFFFF;

        private const string UnityPlayerClassName = "com.unity3d.player.UnityPlayer";
        private const string CurrentActivityFieldName = "currentActivity";

        public static int SdkVersion
        {
            get
            {
                sdkVersionCache ??= GetSdkVersion();
                return sdkVersionCache.Value;
            }
        }

        public static AndroidJavaObject CurrentActivity => _currentActivityCache ??= GetCurrentActivity();

        private static int? sdkVersionCache;

        private static AndroidJavaObject _currentActivityCache;

        private static int GetSdkVersion()
        {
            const string apiNeedle = "API-";

            int sdkVer = BadSdkVersion;

            if (Application.platform != RuntimePlatform.Android)
                return sdkVer;

            string os = SystemInfo.operatingSystem;
            int sdkPos = os.IndexOf(apiNeedle, StringComparison.OrdinalIgnoreCase);

            if (!int.TryParse(os.Substring(sdkPos + apiNeedle.Length, 2), out sdkVer))
                sdkVer = BadSdkVersion;

            return sdkVer;
        }

        private static AndroidJavaObject GetCurrentActivity()
        {
            var unityPlayer = new AndroidJavaClass(UnityPlayerClassName);

            return unityPlayer.GetStatic<AndroidJavaObject>(CurrentActivityFieldName);
        }
    }
}

#endif