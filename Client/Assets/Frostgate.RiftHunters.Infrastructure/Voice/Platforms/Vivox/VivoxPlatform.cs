﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core.Voice;
using JetBrains.Annotations;
using Unity.Services.Vivox;
using VivoxUnity;
using Channel = Unity.Services.Vivox.Channel;

namespace Frostgate.RiftHunters.Infrastructure.Voice.Vivox
{
    internal sealed class VivoxPlatform : IVoicePlatform
    {
        public event Action<bool> SelfSpeechDetected = delegate { };

        public event Action<bool> MuteSelfChanged = delegate { };

        public event Action<string, bool> ParticipantSpeechDetected = delegate { };

        public event Action<bool> MuteParticipantsChanged = delegate { };

        public event Action<string, bool> MuteParticipantChanged = delegate { };


        public bool MuteSelf
        {
            get => _muteSelf;
            set => MuteSelfInternal(value);
        }

        public bool MuteParticipants
        {
            get => _muteParticipants;
            set => MuteParticipantsInternal(value);
        }

        [NotNull] private readonly IPermissionsRequester _permissionsRequester;

        [CanBeNull] private readonly VivoxConfig _config;

        private ILoginSession _loginSession;
        private UniTask? _loginTask;

        private IChannelSession _channelSession;
        private UniTask? _channelJoinTask;

        private bool _muteSelf;
        private bool _muteParticipants;

        // Временное решение для того, чтобы хранить статусы мута ботов,
        // т.к. сейчас невозможно отличить бота от игрока и ботов нет в списке участников голосовой сессии
        private readonly IDictionary<string, bool> _fakeUsersMuteStatuses = new Dictionary<string, bool>();

        public VivoxPlatform([NotNull] IPermissionsRequester permissionsRequester,
            [CanBeNull] VivoxConfig config = null)
        {
            _permissionsRequester = permissionsRequester;
            _config = config;
        }

        public UniTask InitializeAsync()
        {
            VivoxService.Instance.Initialize(_config);

            return _permissionsRequester.RequestIfNeededAsync();
        }

        public UniTask DeinitializeAsync()
        {
            LeaveChannel();
            _loginTask = null;
            _channelJoinTask = null;
            VivoxService.Instance.Client.Uninitialize();

            return UniTask.CompletedTask;
        }

        public UniTask LoginAsync(string userName)
        {
            if (_loginTask.HasValue)
                return _loginTask.Value;

            var account = new Account(userName);

            _loginSession = VivoxService.Instance.Client.GetLoginSession(account);

            var tcs = new UniTaskCompletionSource();
            _loginTask = tcs.Task;

            string loginToken = _loginSession.GetLoginToken();
            SubscriptionMode mode = SubscriptionMode.Accept;
            IReadOnlyHashSet<AccountId> presenceSubscription = null;
            IReadOnlyHashSet<AccountId> blockedPresenceSubscription = null;
            IReadOnlyHashSet<AccountId> allowedPresenceSubscription = null;

            _loginSession.PropertyChanged += OnPropertyChanged;

            // ReSharper disable ExpressionIsAlwaysNull
            _loginSession.BeginLogin(loginToken, mode, presenceSubscription,
                blockedPresenceSubscription, allowedPresenceSubscription, ar =>
                {
                    try
                    {
                        _loginSession.EndLogin(ar);
                    }
                    catch (Exception e)
                    {
                        tcs.TrySetException(e);
                    }
                });

            void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
            {
                if (e.PropertyName != nameof(_loginSession.State))
                    return;

                if (_loginSession.State == LoginState.LoggedIn)
                {
                    _loginSession.PropertyChanged -= OnPropertyChanged;
                    tcs.TrySetResult();
                }
            }

            return _loginTask.Value;
        }

        public UniTask JoinChannelAsync(string channelId)
        {
            if (_channelJoinTask.HasValue)
                return _channelJoinTask.Value;

            if (_loginSession == null)
                return UniTask.CompletedTask;

            Channel channel = new Channel(channelId);
            _channelSession = _loginSession.GetChannelSession(channel);

            var tcs = new UniTaskCompletionSource();
            _channelJoinTask = tcs.Task;

            bool connectAudio = true;
            bool connectText = false;
            bool switchTransmission = true;
            string connectToken = _channelSession.GetConnectToken();

            _channelSession.PropertyChanged += OnPropertyChanged;
            _channelSession.Participants.AfterKeyAdded += OnAfterParticipantsKeyAdded;
            _channelSession.Participants.BeforeKeyRemoved += OnBeforeParticipantsKeyRemoved;

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            _channelSession.BeginConnect(connectAudio, connectText, switchTransmission, connectToken, ar =>
            {
                try
                {
                    _channelSession.EndConnect(ar);
                }
                catch (Exception e)
                {
                    tcs.TrySetException(e);
                }
            });

            void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
            {
                if (e.PropertyName != nameof(_channelSession.ChannelState))
                    return;

                if (_channelSession.ChannelState == ConnectionState.Connected)
                {
                    _channelSession.PropertyChanged -= OnPropertyChanged;
                    tcs.TrySetResult();
                }
            }

            return _channelJoinTask.Value;
        }

        public void LeaveChannel()
        {
            if (_channelSession == null)
                return;

            _channelSession.Participants.AfterKeyAdded -= OnAfterParticipantsKeyAdded;
            _channelSession.Participants.BeforeKeyRemoved -= OnBeforeParticipantsKeyRemoved;

            _loginSession.DeleteChannelSession(_channelSession.Channel);
            _channelJoinTask = null;
        }

        public bool ParticipantIsMuted(string userName)
        {
            VivoxUnity.IReadOnlyDictionary<string, IParticipant> participants = _channelSession?.Participants;
            IParticipant participant = participants?.FirstOrDefault(p => p.Account.DisplayName == userName);

            if (participant != null)
                return participant.LocalMute;

            if (_fakeUsersMuteStatuses.TryGetValue(userName, out var status))
                return status;

            _fakeUsersMuteStatuses[userName] = false;

            return false;
        }

        public void MuteParticipant(string userName, bool mute)
        {
            VivoxUnity.IReadOnlyDictionary<string, IParticipant> participants = _channelSession?.Participants;
            IParticipant participant = participants?.FirstOrDefault(p => p.Account.DisplayName == userName);

            if (participant == null)
                _fakeUsersMuteStatuses[userName] = mute;
            else
                participant.LocalMute = mute;

            MuteParticipantChanged.Invoke(userName, mute);
        }

        public bool IsOtherParticipantsMuted()
        {
            bool fakeUsersIsMuted = _fakeUsersMuteStatuses.Count == 0 ||
                                    _fakeUsersMuteStatuses.All(kvp => kvp.Value);

            VivoxUnity.IReadOnlyDictionary<string, IParticipant> participants = _channelSession?.Participants;
            if (participants == null)
                return fakeUsersIsMuted;

            bool participantsIsMuted = participants.Where(p => !p.IsSelf).All(p => p.LocalMute);

            return participantsIsMuted && fakeUsersIsMuted;
        }

        private void MuteSelfInternal(bool mute)
        {
            if (_muteSelf == mute)
                return;

            VivoxService.Instance.Client.AudioInputDevices.Muted = mute;
            _muteSelf = mute;

            MuteSelfChanged.Invoke(mute);
        }

        private void MuteParticipantsInternal(bool mute)
        {
            if (_muteParticipants == mute)
                return;

            VivoxService.Instance.Client.AudioOutputDevices.Muted = mute;
            _muteParticipants = mute;

            MuteParticipantsChanged.Invoke(mute);
        }

        private void OnAfterParticipantsKeyAdded(object sender, KeyEventArg<string> arg)
        {
            IParticipant participant = _channelSession.Participants[arg.Key];
            participant.PropertyChanged += OnParticipantPropertyChanged;
        }

        private void OnBeforeParticipantsKeyRemoved(object sender, KeyEventArg<string> arg)
        {
            IParticipant participant = _channelSession.Participants[arg.Key];
            participant.PropertyChanged -= OnParticipantPropertyChanged;
        }

        private void OnParticipantPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            IParticipant participant = (IParticipant)sender;
            switch (e.PropertyName)
            {
                case nameof(participant.SpeechDetected):
                    OnParticipantSpeechDetected(participant);
                    break;

                default:
                    break;
            }
        }

        private void OnParticipantSpeechDetected(IParticipant participant)
        {
            string userName = participant.Account.DisplayName;
            bool newValue = participant.SpeechDetected;

            ParticipantSpeechDetected.Invoke(userName, newValue);

            if (_loginSession.LoginSessionId.DisplayName == userName)
                SelfSpeechDetected.Invoke(newValue);
        }
    }
}