﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Infrastructure.Voice.Vivox
{
    internal interface IPermissionsRequester
    {
        UniTask RequestIfNeededAsync();
    }
}