﻿using VivoxUnity;
using Zenject;

namespace Frostgate.RiftHunters.Infrastructure.Voice.Vivox
{
    public sealed class VivoxVoiceInstaller : Installer
    {
        private readonly bool _useConfig;
        private readonly VivoxConfigBuilder? _configBuilder;

        public VivoxVoiceInstaller(bool useConfig = false, VivoxConfigBuilder? configBuilder = null)
        {
            _useConfig = useConfig;
            _configBuilder = configBuilder;
        }

        public override void InstallBindings()
        {
            BindPermissionRequester();
            BindPlatform();
        }

        private void BindPermissionRequester()
        {
#if !UNITY_EDITOR && UNITY_ANDROID
            Container.BindInterfacesAndSelfTo<AndroidPermissionsRequester>().AsSingle();
#else
            Container.BindInterfacesAndSelfTo<EmptyPermissionsRequester>().AsSingle();
#endif
        }

        private void BindPlatform()
        {
            VivoxConfig config = _useConfig && _configBuilder.HasValue ? _configBuilder.Value.Build() : null;

            Container.BindInterfacesAndSelfTo<VivoxPlatform>().AsSingle().WithArguments(config);
        }
    }
}