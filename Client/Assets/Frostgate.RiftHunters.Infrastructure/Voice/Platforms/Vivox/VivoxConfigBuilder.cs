﻿using System;
using UnityEngine;
using VivoxUnity;

namespace Frostgate.RiftHunters.Infrastructure.Voice.Vivox
{
    [Serializable]
    public struct VivoxConfigBuilder
    {
        [SerializeField] private int _codecThreads;
        [SerializeField] private int _voiceThreads;
        [SerializeField] private int _webThreads;
        [SerializeField] private int _renderSourceQueueDepthMax;
        [SerializeField] private int _renderSourceInitialBufferCount;
        [SerializeField] private int _upstreamJitterFrameCount;
        [SerializeField] private int _maxLoginPerUser;
        [SerializeField] private vx_log_level _initialLogLevel;
        [SerializeField] private bool _disableDevicePolling;
        [SerializeField] private bool _forceCaptureSilence;
        [SerializeField] private bool _enableAdvancedAutoLevels;
        [SerializeField] private int _captureDeviceBufferSizeIntervals;
        [SerializeField] private int _renderDeviceBufferSizeIntervals;
        [SerializeField] private bool _disableAudioDucking;
        [SerializeField] private bool _enableDtx;
        [SerializeField] private MediaCodecType _defaultCodecsMask;
        [SerializeField] private bool _enableFastNetworkChangeDetection;
        [SerializeField] private int _useOsProxySettings;
        [SerializeField] private bool _dynamicVoiceProcessingSwitching;
        [SerializeField] private int _neverRtpTimeoutMs;
        [SerializeField] private int _lostRtpTimeoutMs;
        [SerializeField] private bool _skipPrepareForVivox;

        public VivoxConfig Build()
        {
            return new VivoxConfig()
            {
                CodecThreads = _codecThreads,
                VoiceThreads = _voiceThreads,
                WebThreads = _webThreads,
                RenderSourceQueueDepthMax = _renderSourceQueueDepthMax,
                RenderSourceInitialBufferCount = _renderSourceInitialBufferCount,
                UpstreamJitterFrameCount = _upstreamJitterFrameCount,
                MaxLoginsPerUser = _maxLoginPerUser,
                InitialLogLevel = _initialLogLevel,
                DisableDevicePolling = _disableDevicePolling,
                ForceCaptureSilence = _forceCaptureSilence,
                EnableAdvancedAutoLevels = _enableAdvancedAutoLevels,
                CaptureDeviceBufferSizeIntervals = _captureDeviceBufferSizeIntervals,
                RenderDeviceBufferSizeIntervals = _renderDeviceBufferSizeIntervals,
                DisableAudioDucking = _disableAudioDucking,
                EnableDtx = _enableDtx,
                DefaultCodecsMask = _defaultCodecsMask,
                EnableFastNetworkChangeDetection = _enableFastNetworkChangeDetection,
                UseOsProxySettings = _useOsProxySettings,
                DynamicVoiceProcessingSwitching = _dynamicVoiceProcessingSwitching,
                NeverRtpTimeoutMs = _neverRtpTimeoutMs,
                LostRtpTimeoutMs = _lostRtpTimeoutMs,
                SkipPrepareForVivox = _skipPrepareForVivox
            };
        }
    }
}