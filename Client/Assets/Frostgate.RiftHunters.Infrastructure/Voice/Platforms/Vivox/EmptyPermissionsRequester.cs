﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Infrastructure.Voice.Vivox
{
    internal sealed class EmptyPermissionsRequester : IPermissionsRequester
    {
        public UniTask RequestIfNeededAsync() => UniTask.CompletedTask;
    }
}