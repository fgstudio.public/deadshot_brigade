﻿namespace Frostgate.RiftHunters.Infrastructure.Voice
{
    public sealed class AssetMenu
    {
        public const string Menu = AssetMenus.RiftHunters.Menu + "/" + nameof(Voice);

        public sealed class Platforms
        {
            // ReSharper disable once MemberHidesStaticFromOuterClass
            public const string Menu = AssetMenu.Menu + "/Platforms";
        }
    }
}