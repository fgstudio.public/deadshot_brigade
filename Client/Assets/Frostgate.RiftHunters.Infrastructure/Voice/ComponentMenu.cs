﻿using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Infrastructure.Voice
{
    public sealed class ComponentMenu
    {
        public const string Menu = ComponentMenus.RiftHunters.Menu + "/" + nameof(Voice);
    }
}