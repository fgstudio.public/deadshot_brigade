using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Infrastructure;

namespace Frostgate.RiftHunters.Meta
{
    public class CampaignService
    {
        private readonly ISaveService _saveService;
        private readonly CampaignRoster _campaignRoster;
        private readonly ILogger _logger = LoggerFactory.CreateLogger<CampaignService>();

        private List<RegionPhaseConfig> _allPhases;
        private Dictionary<string, RegionPhaseConfig> _phaseByIds;

        private CampaignData _data;

        public CampaignService(ISaveService saveService, Preferences preferences)
        {
            _saveService = saveService;
            _campaignRoster = preferences.CampaignRoster;
        }

        public async UniTask InitializeAsync()
        {
            _logger.Log("Initializing...");

            _data = await _saveService.GetData<CampaignData>();

            _allPhases = new List<RegionPhaseConfig>();
            _phaseByIds = new Dictionary<string, RegionPhaseConfig>();

            foreach (var regionConfig in _campaignRoster.Regions)
            foreach (var phaseConfig in regionConfig.Phases)
            {
                _allPhases.Add(phaseConfig);
                _phaseByIds.Add(phaseConfig.Id, phaseConfig);
            }

            _logger.Log("Initialized");
        }

        public RegionPhaseConfig GetCurrentPhase() => _phaseByIds[_data.CurrentPhaseId];

        public async UniTask<bool> TrySetCurrentPhase(RegionPhaseConfig phase, int currentHeroRank)
        {
            if (!IsPhaseAvailable(phase, currentHeroRank))
                return false;

            await _saveService.SaveData(_data);
            return true;
        }

        public bool IsRegionUnlocked(RegionConfig config)
        {
            if (config.Phases.Length <= 0)
                return false;

            var firstPhase = config.Phases.First();
            var firstPhaseIndex = _allPhases.IndexOf(firstPhase);
            var lastOpenedPhase = GetLastOpenedPhase();
            var lastOpenedPhaseIndex = _allPhases.IndexOf(lastOpenedPhase);

            return lastOpenedPhaseIndex >= firstPhaseIndex;
        }

        public bool IsPhaseUnlocked(RegionPhaseConfig config)
        {
            var phaseIndex = _allPhases.IndexOf(config);
            var lastOpenedPhase = GetLastOpenedPhase();
            var lastOpenedPhaseIndex = _allPhases.IndexOf(lastOpenedPhase);

            return lastOpenedPhaseIndex >= phaseIndex;
        }

        public bool IsPhaseAvailable(RegionPhaseConfig config, int currentHeroRank)
        {
            if (!IsPhaseUnlocked(config))
                return false;

            if (currentHeroRank > config.MaxHeroRank)
                return false;

            return true;
        }

        public async UniTask<MissionConfig> TakeMission()
        {
            var phase = GetCurrentPhase();
            var counter = _data.Phases[phase.Id].MissionCounter;

            // TODO: временная механика. Потмо планируется оставить только рандомные миссии.
            if (counter < phase.FirstMissionConfigs.Length)
                return phase.FirstMissionConfigs[counter];

            var prevIndex = _data.Phases[phase.Id].MissionCounter - phase.FirstMissionConfigs.Length;

            if (prevIndex >= phase.MissionConfigs.Length)
                prevIndex = 0;

            // Исключаем из рандома индекс предыдущей миссии
            var indexes = new List<int>();
            for (var i = 0; i < phase.MissionConfigs.Length; i++)
                if (i != prevIndex)
                    indexes.Add(i);

            var index = 0;
            if (indexes.Count > 0)
                index = indexes.GetRandom();

            var mission = phase.MissionConfigs[index];
            _data.Phases[phase.Id].MissionCounter = phase.FirstMissionConfigs.Length + index;

            await _saveService.SaveData(_data);
            return mission;
        }

        public UniTask AddScore(RegionPhaseConfig config, int score)
        {
            if (score <= 0)
                return UniTask.CompletedTask;

            if (!_data.Phases.ContainsKey(config.Id))
                _data.Phases.Add(config.Id, new CampaignPhaseData());

            var phase = _data.Phases[config.Id];
            phase.Score += score;

            if (phase.Score > config.MaxScore)
                phase.Score = config.MaxScore;

            if (GetLastOpenedPhase() == config && phase.Score >= config.MaxScore)
                TryOpenNextPhase();

            // TODO: временная механика. Потмо планируется оставить только рандомные миссии счётчик которых крутится до начисления очков.
            if (phase.MissionCounter < config.FirstMissionConfigs.Length)
                phase.MissionCounter++;

            return _saveService.SaveData(_data);
        }
        public int GetScore(RegionPhaseConfig phase)
        {
            if (_data.Phases.TryGetValue(phase.Id, out var data))
                return data.Score;

            return 0;
        }

        private void TryOpenNextPhase()
        {
            var last = GetLastOpenedPhase();
            var index = _allPhases.IndexOf(last);
            index++;

            if (index >= _allPhases.Count)
                return;

            var phase = _allPhases[index];
            _data.LastOpenedPhaseId = phase.Id;

            // Стейт фазы гипотетически может существовать, например если мы поиграли с другом в не доступную у нас фазу
            if (!_data.Phases.ContainsKey(phase.Id))
                _data.Phases.Add(phase.Id, new CampaignPhaseData());
        }

        private RegionPhaseConfig GetLastOpenedPhase() => _phaseByIds[_data.LastOpenedPhaseId];
    }
}