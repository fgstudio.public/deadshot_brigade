using System;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Meta
{
    [Serializable]
    public sealed class CampaignPhaseData
    {
        public int Score;
        public int MissionCounter;
    }

    [Serializable]
    public sealed class CampaignData
    {
        public string CurrentPhaseId;
        public string LastOpenedPhaseId;

        public Dictionary<string, CampaignPhaseData> Phases;
    }
}