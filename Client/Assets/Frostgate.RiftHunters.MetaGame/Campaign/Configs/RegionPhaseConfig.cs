using Frostgate.RiftHunters.Core.Battle;
using UnityEngine;

namespace Frostgate.RiftHunters.Meta
{
    [CreateAssetMenu(menuName = MetaAssetMenus.Campaign.Menu + "/" + nameof(RegionPhaseConfig), fileName = nameof(RegionPhaseConfig))]
    public class RegionPhaseConfig : EntityConfig
    {
        [SerializeField] private int _maxScore;
        [SerializeField] private int _recommendedGearscore;
        [SerializeField] private int _maxHeroRank;
        // TODO: Временная фича. Потом должна по плану остатся только механика рандомных миссий. Список последовательных миссий можно будет выпилить по отмашке от Гд.
        [SerializeField] private MissionConfig[] _firstMissionConfigs;
        [SerializeField] private MissionConfig[] _missionConfigs;

        public int MaxScore => _maxScore;
        public int RecommendedGearScore => _recommendedGearscore;
        public int MaxHeroRank => _maxHeroRank;
        public MissionConfig[] FirstMissionConfigs => _firstMissionConfigs;
        public MissionConfig[] MissionConfigs => _missionConfigs;
    }
}