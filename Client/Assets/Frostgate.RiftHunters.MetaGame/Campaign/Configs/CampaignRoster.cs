using System.Collections.Generic;
using UnityEngine;

namespace Frostgate.RiftHunters.Meta
{
    [CreateAssetMenu(menuName = MetaAssetMenus.Campaign.Menu + "/" + nameof(CampaignRoster), fileName = nameof(CampaignRoster))]
    public class CampaignRoster : ScriptableObject
    {
        [SerializeField] private RegionConfig[] _regions;

        public RegionConfig[] Regions => _regions;

        private static Dictionary<RegionPhaseConfig, RegionConfig> _regionByPhases;

        public RegionConfig GetRegion(RegionPhaseConfig targetPhase)
        {
            if (_regionByPhases == null)
            {
                _regionByPhases = new Dictionary<RegionPhaseConfig, RegionConfig>();
                foreach (var region in _regions)
                foreach (var phase in region.Phases)
                    if (phase == targetPhase)
                        _regionByPhases.Add(phase, region);;
            }

            if (_regionByPhases.TryGetValue(targetPhase, out var regionConfig))
                return regionConfig;

            return null;
        }
    }
}