using Frostgate.RiftHunters.Core.Battle;
using UnityEngine;

namespace Frostgate.RiftHunters.Meta
{
    [CreateAssetMenu(menuName = MetaAssetMenus.Campaign.Menu + "/" + nameof(RegionConfig), fileName = nameof(RegionConfig))]
    public class RegionConfig : EntityConfig
    {
        [SerializeField] private RegionPhaseConfig[] _phases;

        public RegionPhaseConfig[] Phases => _phases;
    }
}