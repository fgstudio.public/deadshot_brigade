﻿namespace Frostgate.RiftHunters.Meta
{
    public static class MetaComponentMenus
    {
        public const string Menu = ComponentMenus.RiftHunters.Menu + "/Meta";

        public static class UI
        {
            public const string Menu = MetaAssetMenus.Menu + "/" + nameof(UI);
        }

        public static class Services
        {
            public const string Menu = MetaAssetMenus.Menu + "/" + nameof(Services);
        }

        public static class Examples
        {
            public const string Menu = MetaAssetMenus.Menu + "/" + nameof(Examples);
        }
    }
}