﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Meta
{
    [CreateAssetMenu(
        fileName = nameof(DefaultMetaData),
        menuName = MetaAssetMenus.Menu + "/" + nameof(DefaultMetaData))]
    public sealed class DefaultMetaData : ScriptableObject
    {
        [InfoBox("Количество предметов должно соответствовать данным в " + nameof(EquipmentLimitations))]
        [SerializeField] private HeroData[] _heroesData;

        public IReadOnlyList<HeroData> HeroesData => _heroesData;
    }
}