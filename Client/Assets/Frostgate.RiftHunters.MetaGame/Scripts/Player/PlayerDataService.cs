using System;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta.Player
{
    public sealed class PlayerDataService
    {
        private readonly ISaveService _saveService;
        private readonly IConfigCollection<Hero> _heroCollection;
        private readonly ILogger _logger = LoggerFactory.CreateLogger<PlayerDataService>();

        private PlayerData _data;

        public PlayerDataService(
            [NotNull] ISaveService saveService,
            [NotNull] IConfigCollection<Hero> heroCollection)
        {
            _saveService = saveService;
            _heroCollection = heroCollection;
        }

        public async UniTask InitializeAsync()
        {
            if (_data != null)
                throw new InvalidOperationException("Service is already initialized");

            _logger.Log("Initializing...");

            _data = await _saveService.GetData<PlayerData>();

            if (_data == null)
                throw new ArgumentNullException(
                    nameof(PlayerData), "Isn't initialized with processor");

            _logger.Log("Initialized");
        }

        public string GetName() => _data.Name;
        public Hero GetSelectedHero() => _heroCollection[_data.SelectedHeroId];

        public UniTask SetNameAsync(string name)
        {
            _data.Name = name;
            _logger.Log($"Set Player Name: {name}");

            return _saveService.SaveData(_data);
        }

        public UniTask SetSelectedHeroAsync(string heroId)
        {
            _data.SelectedHeroId = heroId;
            _logger.Log($"Set Selected Hero: {heroId}");

            return _saveService.SaveData(_data);
        }
    }
}