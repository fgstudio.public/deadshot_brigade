﻿using System;

namespace Frostgate.RiftHunters.Meta.Player
{
    [Serializable]
    public sealed class PlayerData
    {
        public string Name;
        public string SelectedHeroId;

        public override string ToString() =>
            $"{nameof(Name)}: {Name}, {nameof(SelectedHeroId)}: {SelectedHeroId}";
    }
}