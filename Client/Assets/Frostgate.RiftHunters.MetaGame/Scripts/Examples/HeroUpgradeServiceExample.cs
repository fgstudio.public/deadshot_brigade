﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta.Examples
{
    [DisallowMultipleComponent]
    [AddComponentMenu(MetaComponentMenus.Examples.Menu + "/" + nameof(HeroUpgradeServiceExample))]
    public sealed class HeroUpgradeServiceExample : BaseServiceExample
    {
        [Title("Settings")]
        [SerializeField, Required, AssetSelector] private Hero _hero;

        [CanBeNull] private HeroUpgradeService _heroUpgradeService;

        [Inject]
        private void MonoConstructor([NotNull] HeroUpgradeService heroUpgradeService) =>
            _heroUpgradeService = heroUpgradeService;

        [Title("Buttons")]
        [Button(ButtonSizes.Medium)]
        public void GetHeroLevel() =>
            Output($"Level of \"{_hero.Id}\": " +
                   $"{_heroUpgradeService!.GetHeroLevel(_hero.Id).ToString()}");

        [Button(ButtonSizes.Medium)]
        public void GetHeroRank() =>
            Output($"Rank of \"{_hero.Id}\": " +
                   $"{_heroUpgradeService!.GetHeroRank(_hero.Id).ToString()}");

        [Button(ButtonSizes.Medium)]
        public void UpgradeAsync() =>
            AsyncOperation(() => _heroUpgradeService!.UpgradeAsync(_hero.Id)
                .ContinueWith(() => Output(
                    $"Hero \"{_hero.Id}\" upgraded to Level {_heroUpgradeService!.GetHeroLevel(_hero.Id).ToString()}")));

        [Button(ButtonSizes.Medium)]
        public void CanUpgrade() =>
            AsyncOperation(() => _heroUpgradeService!.CanUpgradeAsync(_hero.Id)
                .ContinueWith(canUpgrade => Output(
                    $"Can Upgrade \"{_hero.Id}\": {canUpgrade}")));

        [Button(ButtonSizes.Medium)]
        public void GetNextLevelData() =>
            Output($"Got Next Level Data for Level " +
                   $"{_heroUpgradeService!.GetNextLevelData(_hero.Id).Level}");
    }
}