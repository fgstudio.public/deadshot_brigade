﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Meta.Player;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta.Examples
{
    [DisallowMultipleComponent]
    [AddComponentMenu(MetaComponentMenus.Examples.Menu + "/" + nameof(PlayerDataServiceExample))]
    public sealed class PlayerDataServiceExample : BaseServiceExample
    {
        [Title("Settings")]
        [SerializeField, Required, AssetSelector] private string _playerName;
        [SerializeField, Required, AssetSelector] private Hero _selectedHero;

        [CanBeNull] private PlayerDataService _playerDataService;

        [Inject]
        private void MonoConstructor([NotNull] PlayerDataService playerDataService) =>
            _playerDataService = playerDataService;

        [Title("Buttons")]
        [Button(ButtonSizes.Medium)]
        public void GetPlayerName() =>
            Output($"Player Name: {_playerDataService!.GetName()}");

        [Button(ButtonSizes.Medium)]
        public void GetSelectedHero() =>
            Output($"Selected Hero Id: {_playerDataService!.GetSelectedHero().Id}");

        [Button(ButtonSizes.Medium)]
        public void SetPlayerName() =>
            AsyncOperation(() => _playerDataService!.SetNameAsync(_playerName)
                .ContinueWith(() => Output($"Set Player Name: {_playerName}")));

        [Button(ButtonSizes.Medium)]
        public void SetSelectedHero() =>
            AsyncOperation(() => _playerDataService!.SetSelectedHeroAsync(_selectedHero.Id)
                .ContinueWith(() => Output($"Set Selected Hero: {_selectedHero.Id}")));
    }
}