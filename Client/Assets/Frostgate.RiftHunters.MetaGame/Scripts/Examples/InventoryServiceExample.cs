﻿using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using System.Linq;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta.Examples
{
    [DisallowMultipleComponent]
    [AddComponentMenu(MetaComponentMenus.Examples.Menu + "/" + nameof(InventoryServiceExample))]
    public sealed class InventoryServiceExample : BaseServiceExample
    {
        [Title("Settings")]
        [SerializeField, Required, AssetSelector] private InventoryItem _item;

        [CanBeNull] private InventoryService _inventoryService;

        [Inject]
        private void MonoConstructor([NotNull] InventoryService inventoryService) =>
            _inventoryService = inventoryService;

        [Title("Buttons")]
        [Button(ButtonSizes.Medium)]
        public void AddInventoryItem() =>
            AsyncOperation(() => _inventoryService!.AddInventoryItem(_item.Id)
                .ContinueWith(() => Output($"Added {_item.Id}")));

        [Button(ButtonSizes.Medium)]
        public void DeleteInventoryItem() =>
            AsyncOperation(() => _inventoryService!.GetAllPlayerInventoryItemIds(_item.Id)
                .ContinueWith(playerItemIds =>
                {
                    string playerItemId = playerItemIds.First();
                    return _inventoryService!.DeleteInventoryItem(_item.Id, playerItemId)
                        .ContinueWith(() => Output($"Deleted {_item.Id} with playerItemId {playerItemId}"));
                }));


        [Button(ButtonSizes.Medium)]
        public void HasItem() =>
            AsyncOperation(() => _inventoryService!.HasItem(_item.Id)
                .ContinueWith(hasItem => Output($"{nameof(_inventoryService.HasItem)}: {hasItem}")));

        [Button(ButtonSizes.Medium)]
        public void GetItemsCount() =>
            AsyncOperation(() => _inventoryService!.GetItemsCount(_item.Id)
                .ContinueWith(count => Output($"Items {_item.Id} count: {count}")));

        [Button(ButtonSizes.Medium)]
        public void GetAllPlayerInventoryItemIds() =>
            AsyncOperation(() => _inventoryService!.GetAllPlayerInventoryItemIds(_item.Id)
                .ContinueWith(ids => Output(
                    $"Player Inventory Items Ids for {_item.Id}: {string.Join(", ", ids)}")));

        [Button(ButtonSizes.Medium)]
        public void GetAllItems() =>
            AsyncOperation(() => _inventoryService!.GetAllItems()
                .ContinueWith(items => Output(
                    $"All Items: {string.Join(", ", items.Select(i => i.Id))}")));

        [Button(ButtonSizes.Medium)]
        public void GetAllEquipment() =>
            AsyncOperation(() => _inventoryService!.GetAllEquipment()
                .ContinueWith(equipments => Output(
                    $"All Equipment: {string.Join(", ", equipments.Select(e => e.Id))}")));

        [Button(ButtonSizes.Medium)]
        public void GetAllHeroes() =>
            AsyncOperation(() => _inventoryService!.GetAllHeroes()
                .ContinueWith(heroes => Output(
                    $"All Heroes: {string.Join(", ", heroes.Select(h => h.Id))}")));
    }
}