﻿using System.Linq;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Meta.Examples
{
    [DisallowMultipleComponent]
    [AddComponentMenu(MetaComponentMenus.Examples.Menu + "/" + nameof(WalletServiceExample))]
    public sealed class WalletServiceExample : BaseServiceExample
    {
        [Title("Settings")]
        [SerializeField] private CurrencyQuantity _currencyQuantity;

        [CanBeNull] private WalletService _walletService;

        [Inject]
        private void MonoConstructor([NotNull] WalletService walletService) =>
            _walletService = walletService;

        [Title("Buttons")]
        [Button(ButtonSizes.Medium)]
        public void SetBalance() =>
            AsyncOperation(() => _walletService!.SetBalanceAsync(_currencyQuantity)
                .ContinueWith(() => Output(
                    $"Set Balance {_currencyQuantity.ToString()}")));

        [Button(ButtonSizes.Medium)]
        public void IncrementBalance() =>
            AsyncOperation(() => _walletService!.IncrementBalanceAsync(_currencyQuantity)
                .ContinueWith(() => Output(
                    $"Incremented Balance of \"{_currencyQuantity.Currency.Id}\" by {_currencyQuantity.Quantity}")));

        [Button(ButtonSizes.Medium)]
        public void DecrementBalance() =>
            AsyncOperation(() => _walletService!.DecrementBalanceAsync(_currencyQuantity)
                .ContinueWith(() => Output(
                    $"Decremented Balance of \"{_currencyQuantity.Currency.Id}\" by {_currencyQuantity.Quantity}")));

        [Button(ButtonSizes.Medium)]
        public void HasBalance() =>
            AsyncOperation(() => _walletService!.HasBalanceAsync(_currencyQuantity)
                .ContinueWith(hasBalance => Output(
                    $"Has {_currencyQuantity.Quantity} \"{_currencyQuantity.Currency.Id}\": {hasBalance}")));

        [Button(ButtonSizes.Medium)]
        public void GetTotalBalance() =>
            AsyncOperation(() => _walletService!.GetTotalBalanceAsync()
                .ContinueWith(totalBalance => Output(
                    $"Total Balance: {string.Join(", ", totalBalance.Select(q => q.ToString()))}")));

        [Button(ButtonSizes.Medium)]
        public void GetBalance() =>
            AsyncOperation(() => _walletService!.GetBalanceAsync(_currencyQuantity.Currency.Id)
                .ContinueWith(currencyQuantity => Output(
                    $"Balance: {currencyQuantity.ToString()}")));
    }
}