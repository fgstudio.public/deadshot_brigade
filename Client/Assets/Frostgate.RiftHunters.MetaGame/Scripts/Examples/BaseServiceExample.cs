﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Meta.Examples
{
    public abstract class BaseServiceExample : MonoBehaviour
    {
        [Title("Info")]
        [ShowInInspector, ReadOnly, Multiline] private string _executionOutput;

        protected async void AsyncOperation([NotNull] Func<UniTask> operation)
        {
            Output("...");

            try
            {
                await operation();
            }
            catch (Exception ex)
            {
                Output(ex.Message);
            }
        }

        protected void Output([NotNull] string message) =>
            _executionOutput = message;
    }
}