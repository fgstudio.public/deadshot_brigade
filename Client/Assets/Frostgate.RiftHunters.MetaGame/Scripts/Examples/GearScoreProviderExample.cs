using Zenject;
using UnityEngine;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Core.Battle;

namespace Frostgate.RiftHunters.Meta.Examples
{
    [DisallowMultipleComponent]
    [AddComponentMenu(MetaComponentMenus.Examples.Menu + "/" + nameof(GearScoreProviderExample))]
    public sealed class GearScoreProviderExample : MonoBehaviour
    {
        private GearScoreProvider _gearScoreProvider;
        private IConfigCollection<UnitConfig> _unitsCollection;

        private readonly ILogger<GearScoreProviderExample> _logger =
            LoggerFactory.CreateLogger<GearScoreProviderExample>();

        [Inject]
        private void MonoConstructor(
            [NotNull] GearScoreProvider gearScoreProvider,
            [NotNull] IConfigCollection<UnitConfig> unitsCollection)
        {
            _gearScoreProvider = gearScoreProvider;
            _unitsCollection = unitsCollection;
        }

        [Button]
        public void CalculateAllHeroesGearScore()
        {
            if (!Application.isPlaying)
            {
                _logger.LogError("Works in play mode only");
                return;
            }

            foreach (var unitConfig in _unitsCollection)
            {
                var gearScore = _gearScoreProvider.CalcTotalHeroGearScore(unitConfig.Id);

                _logger.Log($"Hero {unitConfig.Id} gearScore - {gearScore}");
            }
        }
    }
}