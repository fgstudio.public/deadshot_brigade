﻿using System.Linq;
using Cysharp.Threading.Tasks;
using Zenject;
using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta.Examples
{
    [DisallowMultipleComponent]
    [AddComponentMenu(MetaComponentMenus.Examples.Menu + "/" + nameof(EquipmentServiceExample))]
    public sealed class EquipmentServiceExample : BaseServiceExample
    {
        [Title("Settings")]
        [SerializeField, Required, AssetSelector] private Equipment _equipment;
        [SerializeField, Required, AssetSelector] private Hero _hero;

        [CanBeNull] private EquipmentService _equipmentService;

        [Inject]
        private void MonoConstructor([NotNull] EquipmentService equipmentService) =>
            _equipmentService = equipmentService;

        [Title("Buttons")]
        [Button(ButtonSizes.Medium)]
        public void IsEquipped() =>
            Output($"{nameof(_equipmentService.IsEquipped)} " +
                   $"{_equipment.Id} on {_hero.Id}: " +
                   $"{_equipmentService!.IsEquipped(_hero.Id, _equipment.Id)}");

        [Button(ButtonSizes.Medium)]
        public void CanEquip() =>
            AsyncOperation(() => _equipmentService!.CanEquip(_hero.Id, _equipment.Id)
                .ContinueWith(canEquip => Output(
                    $"{nameof(_equipmentService.CanEquip)} {_equipment.Id} for {_hero.Id}: {canEquip}")));

        [Button(ButtonSizes.Medium)]
        public void Equip() =>
            AsyncOperation(() => _equipmentService!.EquipAsync(_hero.Id, _equipment.Id)
                .ContinueWith(() => Output(
                    $"Equipped \"{_equipment.Id}\" for hero \"{_hero.Id}\"")));

        [Button(ButtonSizes.Medium)]
        public void Unequip() =>
            AsyncOperation(() => _equipmentService!.UnequipAsync(_hero.Id, _equipment.Id)
                .ContinueWith(() => Output(
                    $"Unequipped \"{_equipment.Id}\" for hero \"{_hero.Id}\"")));

        [Button(ButtonSizes.Medium)]
        public void UnequipFromAllHeroes() =>
            AsyncOperation(() => _equipmentService!.UnequipFromAllHeroesAsync(_equipment.Id)
                .ContinueWith(() => Output(
                    $"Unequipped \"{_equipment.Id}\" for all heroes")));

        [Button(ButtonSizes.Medium)]
        public void FindEquippedHeroes() =>
            Output($"Equipped with {_equipment.Id} Heroes: " +
                   $"{string.Join(", ", _equipmentService!.FindEquippedHeroes(_equipment.Id).Select(h => h.Id))}");

        [Button(ButtonSizes.Medium)]
        public void FindEquippedItems() =>
            Output($"Equipped Items of Hero {_hero.Id}: " +
                   $"{string.Join(", ", _equipmentService!.FindEquippedItems(_hero.Id).Select(e => e.Id))}");
    }
}