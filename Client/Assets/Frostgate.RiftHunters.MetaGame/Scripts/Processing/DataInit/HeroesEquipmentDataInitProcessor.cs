﻿using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta.Processing
{
    public sealed class HeroesEquipmentDataInitProcessor : BaseDataInitProcessor<HeroesEquipmentData>
    {
        [NotNull, ItemNotNull] private readonly IEnumerable<IEquipmentHeroData> _heroesData;

        public HeroesEquipmentDataInitProcessor(
            [NotNull] ISaveService saveService,
            [NotNull, ItemNotNull] IEnumerable<IEquipmentHeroData> heroesData)
            : base(saveService) =>
            _heroesData = heroesData;

        protected override void InitializeData(HeroesEquipmentData data)
        {
            foreach (IEquipmentHeroData heroData in _heroesData)
            {
                data[heroData.Id] = heroData.EquipmentIds.ToList();
                Logger.Log($"Initialized with [{heroData.Id}] = {string.Join(", ",heroData.EquipmentIds)}");
            }
        }
    }
}