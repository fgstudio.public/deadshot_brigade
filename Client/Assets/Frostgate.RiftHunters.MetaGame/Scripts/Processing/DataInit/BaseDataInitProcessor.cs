﻿using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Meta.Processing
{
    public abstract class BaseDataInitProcessor<TData> : IProcessor where TData : new()
    {
        [NotNull] protected readonly ILogger Logger;
        [NotNull] private readonly ISaveService _saveService;

        protected BaseDataInitProcessor(ISaveService saveService)
        {
            _saveService = saveService;
            Logger = LoggerFactory.CreateLogger(GetType().Name);
        }

        public async UniTask ExecuteAsync()
        {
            Logger.Log("Executing...");

            var data = await _saveService.GetData<TData>();
            if (data != null)
            {
                Logger.Log($"\"{typeof(TData).Name}\" is existing. Executing isn't required");
                return;
            }

            data = new TData();
            Logger.Log($"Created \"{typeof(TData).Name}\"");

            InitializeData(data);

            await _saveService.SaveData(data);
            Logger.Log("Executed");
        }

        protected abstract void InitializeData([NotNull] TData data);
    }
}