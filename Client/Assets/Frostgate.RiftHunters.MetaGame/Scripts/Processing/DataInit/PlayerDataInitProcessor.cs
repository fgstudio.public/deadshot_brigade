﻿using JetBrains.Annotations;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Player;

namespace Frostgate.RiftHunters.Meta.Processing
{
    public sealed class PlayerDataInitProcessor : BaseDataInitProcessor<PlayerData>
    {
        [NotNull] private readonly string _defaultHeroId;
        [NotNull] private readonly IUserNameGenerator _userNameGenerator = new DeviceUserNameGenerator();

        public PlayerDataInitProcessor(
            [NotNull] ISaveService saveService, [NotNull] string defaultHeroId)
            : base(saveService) =>
            _defaultHeroId = defaultHeroId;

        protected override void InitializeData(PlayerData data)
        {
            data.Name = _userNameGenerator.Generate();
            data.SelectedHeroId = _defaultHeroId;
            Logger.Log($"Initialized with \"{data}\"");
        }
    }
}