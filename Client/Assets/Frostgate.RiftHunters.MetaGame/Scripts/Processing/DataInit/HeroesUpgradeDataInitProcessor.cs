﻿using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta.Processing
{
    public sealed class HeroesUpgradeDataInitProcessor : BaseDataInitProcessor<HeroesUpgradeData>
    {
        [NotNull, ItemNotNull] private readonly IEnumerable<IUpgradeHeroData> _heroesData;

        public HeroesUpgradeDataInitProcessor(
            [NotNull] ISaveService saveService,
            [NotNull, ItemNotNull] IEnumerable<IUpgradeHeroData> heroesData)
            : base(saveService) =>
            _heroesData = heroesData;

        protected override void InitializeData(HeroesUpgradeData data)
        {
            foreach (IUpgradeHeroData heroData in _heroesData)
            {
                data[heroData.Id] = heroData.Level;
                Logger.Log($"Initialized with [{heroData.Id}] = {heroData.Level}");
            }
        }
    }
}