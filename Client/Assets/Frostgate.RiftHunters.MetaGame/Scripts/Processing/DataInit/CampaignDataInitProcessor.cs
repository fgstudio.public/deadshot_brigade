using System.Collections.Generic;
using System.Linq;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Infrastructure;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Meta.Processing
{
    public sealed class CampaignDataInitProcessor : BaseDataInitProcessor<CampaignData>
    {
        private readonly CampaignRoster _campaignRoster;

        public CampaignDataInitProcessor([NotNull] ISaveService saveService, Preferences preferences)
            : base(saveService)
        {
            _campaignRoster = preferences.CampaignRoster;
        }

        protected override void InitializeData(CampaignData data)
        {
            var phase = _campaignRoster.Regions.First().Phases.First();

            data.CurrentPhaseId = phase.Id;
            data.LastOpenedPhaseId = phase.Id;
            data.Phases = new Dictionary<string, CampaignPhaseData> {{phase.Id, new CampaignPhaseData()}};

            Logger.Log($"Initialized");
        }
    }
}