using UnityEngine.Device;

namespace Frostgate.RiftHunters.Meta
{
    public sealed class DeviceUserNameGenerator : IUserNameGenerator
    {
        public string Generate() => SystemInfo.deviceName;
    }
}