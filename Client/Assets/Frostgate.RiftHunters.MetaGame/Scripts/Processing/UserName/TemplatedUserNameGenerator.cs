using UnityEngine;

namespace Frostgate.RiftHunters.Meta
{
    public sealed class TemplatedUserNameGenerator : IUserNameGenerator
    {
        private const string TemplateUserName = "Player";

        public string Generate() =>
            $"{TemplateUserName}_{Random.Range(0, 999):D3}";
    }
}