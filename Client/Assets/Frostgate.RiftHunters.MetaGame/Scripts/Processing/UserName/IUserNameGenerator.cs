namespace Frostgate.RiftHunters.Meta
{
    public interface IUserNameGenerator
    {
        string Generate();
    }
}