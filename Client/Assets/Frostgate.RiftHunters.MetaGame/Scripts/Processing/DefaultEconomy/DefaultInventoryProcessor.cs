﻿using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Meta.Processing
{
    public sealed class DefaultInventoryProcessor : IProcessor
    {
        [NotNull] private readonly IEconomyApi _economyApi;
        [NotNull, ItemNotNull] private readonly IEnumerable<IEquipmentHeroData> _defaultHeroEquipments;

        [NotNull] private readonly ILogger _logger = LoggerFactory.CreateLogger<DefaultInventoryProcessor>();

        public DefaultInventoryProcessor(
            [NotNull] IEconomyApi economyApi,
            [NotNull, ItemNotNull] IEnumerable<IEquipmentHeroData> defaultHeroEquipments)
        {
            _economyApi = economyApi;
            _defaultHeroEquipments = defaultHeroEquipments;
        }

        public async UniTask ExecuteAsync()
        {
            _logger.Log("Executing...");

            _logger.Log("Checking player items...");
            IEnumerable<string> allItems = await _economyApi.PlayerInventory.GetAllItemsAsync();
            _logger.Log("Checked player items");

            if (allItems.Any())
            {
                _logger.Log("Player has items. Initialization isn't required.");
                return;
            }

            foreach (IEquipmentHeroData heroData in _defaultHeroEquipments)
            {
                _logger.Log($"Adding hero {heroData.Id}...");
                await _economyApi.PlayerInventory.AddInventoryItemAsync(heroData.Id);
                _logger.Log($"Added hero {heroData.Id}");

                foreach (string equipmentId in heroData.EquipmentIds)
                {
                    _logger.Log($"Adding equipment {equipmentId}...");
                    await _economyApi.PlayerInventory.AddInventoryItemAsync(equipmentId);
                    _logger.Log($"Added equipment {equipmentId}");
                }
            }

            _logger.Log("Executed");
        }
    }
}