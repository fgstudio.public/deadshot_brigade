﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Meta.Processing
{
    public interface IProcessor
    {
        UniTask ExecuteAsync();
    }
}