﻿using System;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Meta.Economy
{
    public interface IEconomyPlayerBalances
    {
        event Action<string> CurrencyChanged;

        UniTask<long> GetBalanceAsync(string currencyId);
        UniTask SetBalanceAsync(string currencyId, long balance);
        UniTask IncrementBalanceAsync(string currencyId, int amount);
        UniTask DecrementBalanceAsync(string currencyId, int amount);
    }
}