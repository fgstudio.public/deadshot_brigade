﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta
{
    public interface IUpgradeHeroData
    {
        string Id { get; }
        int Level { get; }
    }

    public interface IEquipmentHeroData
    {
        string Id { get; }
        IEnumerable<string> EquipmentIds { get; }
    }

    [Serializable]
    public sealed class HeroData : IUpgradeHeroData, IEquipmentHeroData
    {
        [SerializeField, Required, AssetSelector] private Hero _hero;
        [SerializeField, MinValue(1)] private int _level;
        [SerializeField, Required, AssetSelector] private Equipment[] _equipment;

        public string Id => _hero.Id;
        public int Level => _level;
        public IEnumerable<string> EquipmentIds => _equipment.Select(e => e.Id);
    }
}