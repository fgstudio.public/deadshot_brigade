namespace Frostgate.RiftHunters.Meta
{
    public enum HeroClass
    {
        None = 0,
        Tank = 1,
        Support = 2,
        DamageDealer = 3,
    }
}