﻿using System;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Meta.Economy.Inventory
{
    [Serializable]
    // Key - ID, Value - Level
    public sealed class HeroesUpgradeData : Dictionary<string, int>
    {
    }
}