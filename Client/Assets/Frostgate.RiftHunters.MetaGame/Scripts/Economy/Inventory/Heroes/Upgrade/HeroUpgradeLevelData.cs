﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Meta.Economy;

namespace Frostgate.RiftHunters.Meta
{
    public interface IHeroUpgradeLevelEditor
    {
        Color ItemColor { get; }
        void SetRank(int rank);
        void SetLevel(int level);
    }

    [Serializable]
    public sealed class HeroUpgradeLevelData : IHeroUpgradeLevelEditor
    {
        private static readonly Color keyColor = new(0.8f, 0.8f, 0.7f);
        private static readonly Color evenColor = new(0.8f, 0.8f, 0.8f);
        private static readonly Color oddColor = new(0.75f, 0.75f, 0.75f);

        [MinValue(0), ReadOnly]
        [SerializeField] private int _level;

        [MinValue(0), ReadOnly]
        [SerializeField] private int _rank;

        [SerializeField] private bool _isRankUp;

        [SerializeField, MinValue(0)] private int _gearScore;

        [ListDrawerSettings(ElementColor = nameof(ItemColor))]
        [SerializeField, Required] private CurrencyQuantity[] _cost;

        [ListDrawerSettings(ElementColor = nameof(ItemColor))]
        [SerializeField] private UnitPropertySetup[] _properties;

        [ShowIf(nameof(IsRankUp))]
        [SerializeField] private bool _areImpactsUpgrading;

        [ShowIf(nameof(AreImpactUpgrading)), HideLabel, FoldoutGroup("Impact Roster")]
        [SerializeField] private UnitImpactRoster _impactRoster;

        public int Rank => _rank;
        public int Level => _level;
        public bool IsRankUp => _isRankUp;
        public int GearScore => _gearScore;
        public IReadOnlyList<CurrencyQuantity> Cost => _cost;
        public IReadOnlyList<UnitPropertySetup> Properties => _properties;
        public bool AreImpactUpgrading => _isRankUp && _areImpactsUpgrading;
        public UnitImpactRoster ImpactRoster => AreImpactUpgrading ? _impactRoster : null;

        private Color ItemColor => IsRankUp ? keyColor : IsLevelEven ? evenColor : oddColor;
        private bool IsLevelEven => Level % 2 == 0;

        Color IHeroUpgradeLevelEditor.ItemColor => ItemColor;
        void IHeroUpgradeLevelEditor.SetRank(int rank) => _rank = rank;
        void IHeroUpgradeLevelEditor.SetLevel(int level) => _level = level;
    }
}