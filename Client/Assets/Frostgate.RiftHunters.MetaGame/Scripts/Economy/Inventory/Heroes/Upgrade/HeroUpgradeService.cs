﻿using System;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta.Economy
{
    public sealed class HeroUpgradeService
    {
        private const int DefaultRank = 1;
        private const int DefaultLevel = 1;

        private readonly ISaveService _saveService;
        private readonly WalletService _walletService;
        private readonly InventoryService _inventoryService;
        private readonly IConfigCollection<Hero> _heroCollection;
        private readonly ILogger _logger = LoggerFactory.CreateLogger<HeroUpgradeService>();

        private HeroesUpgradeData _data;

        [UsedImplicitly]
        public HeroUpgradeService(
            [NotNull] ISaveService saveService,
            [NotNull] WalletService walletService,
            [NotNull] InventoryService inventoryService,
            [NotNull] IConfigCollection<Hero> heroCollection)
        {
            _saveService = saveService;
            _walletService = walletService;
            _inventoryService = inventoryService;
            _heroCollection = heroCollection;
        }

        public async UniTask InitializeAsync()
        {
            if (_data != null)
                throw new InvalidOperationException("Service is already initialized");

            _logger.Log("Initializing...");

            _data = await _saveService.GetData<HeroesUpgradeData>();

            if (_data == null)
                throw new ArgumentNullException(
                    nameof(HeroesUpgradeData), "Isn't initialized with processor");

            _logger.Log("Initialized");
        }

        public int GetHeroLevel(string heroId) =>
            _data.TryGetValue(heroId, out int level) ? level : DefaultLevel;

        public int GetHeroRank(string heroId)
        {
            if (!_heroCollection.TryGet(heroId, out Hero heroConfig))
                return DefaultRank;

            if (!_data.TryGetValue(heroId, out int level))
                return DefaultRank;

            HeroUpgradeTable upgradeTable = heroConfig.Config.UpgradeTable;
            return upgradeTable.GetLevelData(level)?.Rank ?? DefaultRank;
        }

        public async UniTask UpgradeAsync(string heroId)
        {
            if (!await CanUpgradeAsync(heroId))
                throw new InvalidOperationException($"Can't upgrade hero {heroId}");

            HeroUpgradeLevelData nextLevelData = GetNextLevelData(heroId);

            _logger.Log($"Paying for upgrade \"{heroId}\" to level {nextLevelData.Level}...");
            await PayForUpgradeAsync(nextLevelData);
            _logger.Log($"Payed for upgrade \"{heroId}\" to level {nextLevelData.Level}");

            _data[heroId] = nextLevelData.Level;
            _logger.Log($"Upgraded hero \"{heroId}\" to level {nextLevelData.Level}]");

            await _saveService.SaveData(_data);
        }

        public async UniTask<bool> CanUpgradeAsync(string heroId)
        {
            HeroUpgradeLevelData nextLevelData = GetNextLevelData(heroId);
            if (nextLevelData == null)
                return false;

            if (!await _inventoryService.HasItem(heroId))
                return false;

            if (!await CanPayForUpgradeAsync(nextLevelData))
                return false;

            return true;
        }

        public HeroUpgradeLevelData GetNextLevelData(string heroId)
        {
            if (!_heroCollection.TryGet(heroId, out Hero heroConfig))
                return null;

            HeroUpgradeTable upgradeTable = heroConfig.Config.UpgradeTable;
            int heroLevel = GetHeroLevel(heroId);
            int nextLevel = heroLevel + 1;

            return upgradeTable.GetLevelData(nextLevel);
        }

        private async UniTask<bool> CanPayForUpgradeAsync(HeroUpgradeLevelData levelData)
        {
            // TODO: нужно сначала сложить одинаковые валюты
            foreach (CurrencyQuantity currencyQuantity in levelData.Cost)
                if (!await _walletService.HasBalanceAsync(currencyQuantity))
                    return false;

            return true;
        }

        private async UniTask PayForUpgradeAsync(HeroUpgradeLevelData levelData)
        {
            // TODO: нужно сначала сложить одинаковые валюты
            foreach (CurrencyQuantity currencyQuantity in levelData.Cost)
                await _walletService.DecrementBalanceAsync(currencyQuantity);
        }
    }
}