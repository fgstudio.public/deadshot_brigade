﻿using UnityEngine;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Meta
{
    [CreateAssetMenu(fileName = nameof(HeroUpgradeTable),
        menuName = MetaAssetMenus.Menu + "/" + nameof(HeroUpgradeTable))]
    public sealed class HeroUpgradeTable : ScriptableObject
    {
        // 0-го уровня нет. 1-ый уровень задан в UnitConfig. Отсчёт со 2-го.
        private const int LevelIndexDelta = 2;

        [ListDrawerSettings(Expanded = true, NumberOfItemsPerPage = 10,
            ElementColor = nameof(GetElementColor), ListElementLabelName = nameof(HeroUpgradeLevelData.Level))]
        [SerializeField] private HeroUpgradeLevelData[] _levels;

        public IReadOnlyList<HeroUpgradeLevelData> Levels => _levels;

        private void OnValidate()
        {
            int rank = 1;

            for (int i = 0; i < _levels.Length; i++)
            {
                if (_levels[i].IsRankUp) rank++;
                int level = ConvertIndexToLevel(i);
                ((IHeroUpgradeLevelEditor)_levels[i]).SetRank(rank);
                ((IHeroUpgradeLevelEditor)_levels[i]).SetLevel(level);
            }
        }

        [CanBeNull]
        public HeroUpgradeLevelData GetLevelData(int level)
        {
            int index = ConvertLevelToIndex(level);
            bool isContained = index.IsContained(0, _levels.Length - 1);
            return isContained ? _levels[index] : null;
        }

        private int ConvertIndexToLevel(int index) => index + LevelIndexDelta;
        private int ConvertLevelToIndex(int level) => level - LevelIndexDelta;
        private Color GetElementColor(int index) => ((IHeroUpgradeLevelEditor)_levels[index]).ItemColor;
    }
}