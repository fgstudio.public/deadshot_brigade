﻿namespace Frostgate.RiftHunters.Meta
{
    public enum HeroRarity
    {
        Base = 0,
        Advanced = 1,
        Unique = 2
    }
}