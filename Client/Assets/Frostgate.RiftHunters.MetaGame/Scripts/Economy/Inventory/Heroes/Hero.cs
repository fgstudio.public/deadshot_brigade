﻿using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core.Battle;

namespace Frostgate.RiftHunters.Meta.Economy.Inventory
{
    [CreateAssetMenu(fileName = nameof(Hero), menuName = MetaAssetMenus.Services.Economy + "/" + nameof(Hero))]
    public sealed class Hero : InventoryItem
    {
        private const string CoreDataName = "Core Data";

        [field: SerializeField, BoxGroup(CoreDataName)] private HeroRarity _rarity;
        [field: SerializeField, BoxGroup(CoreDataName), Required] private UnitConfig _config;

        public HeroRarity Rarity => _rarity;
        public HeroClass Class => _config.Class;
        public UnitConfig Config => _config;

        protected override bool IsIdPropertyEnabled => false;

        private void OnValidate()
        {
            _id = _config.Id;
        }
    }
}