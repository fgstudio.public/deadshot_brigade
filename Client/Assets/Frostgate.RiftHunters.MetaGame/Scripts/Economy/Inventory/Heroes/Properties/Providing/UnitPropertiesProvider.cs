﻿using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle;

namespace Frostgate.RiftHunters.Meta
{
    public sealed class UnitPropertiesProvider : IReadOnlyUnitPropertiesProvider
    {
        public float Health { get; set; }
        public float Stamina { get; set; }
        public float Energy { get; set; }
        public float Speed { get; set; }
        public float TurnSpeed { get; set; }

        public UnitImpactRoster Impacts { get; set; }

        public RangeWeaponConfig RangeWeapon { get; set; }
        [CanBeNull] public MeleeWeaponOverride MeleeWeaponOverride { get; set; }
        IMeleeWeapon IReadOnlyUnitPropertiesProvider.MeleeWeapon => MeleeWeaponOverride;

        public HpAutoRecoveryConfig HpAutoRecovery { get; set; }
        public EnergyAutoRecoveryConfig EnergyAutoRecovery { get; set; }
        public StaminaAutoRecoveryConfig StaminaAutoRecovery { get; set; }

        public UnitPropertiesProvider() { }

        public UnitPropertiesProvider([CanBeNull] UnitConfig unitConfig = null) : this()
        {
            if (unitConfig != null)
            {
                Health = unitConfig.Health;
                Stamina = unitConfig.Stamina;
                Energy = unitConfig.Energy;
                Speed = unitConfig.Speed;
                TurnSpeed = unitConfig.TurnSpeed;

                Impacts = unitConfig.Impacts;

                RangeWeapon = unitConfig.DefaultWeapon;
                MeleeWeaponOverride = unitConfig.DefaultMeleeWeapon != null
                    ? new MeleeWeaponOverride(unitConfig.DefaultMeleeWeapon)
                    : null;

                HpAutoRecovery = unitConfig.HpAutoRecovery;
                EnergyAutoRecovery = unitConfig.EnergyAutoRecovery;
                StaminaAutoRecovery = unitConfig.StaminaAutoRecovery;
            }
        }
    }
}