﻿namespace Frostgate.RiftHunters.Meta
{
    public enum UnitPropertyMeasure
    {
        Units = 0,
        Percents = 1
    }
}