﻿using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Frostgate.RiftHunters.Meta
{
    public sealed class UnitPropertiesUpgradeModifier
    {
        private readonly UnitPropertiesModifier _propertiesModifier;

        private readonly HashSet<UnitProperty> _upgradedProperties = new();
        private bool _areImpactsUpgraded;

        public UnitPropertiesUpgradeModifier(UnitPropertiesModifier propertiesModifier)
        {
            _propertiesModifier = propertiesModifier;
        }

        public void Modify([NotNull] UnitPropertiesProvider provider,
            [NotNull] HeroUpgradeTable upgradeTable, int heroLevel)
        {
            IEnumerable<HeroUpgradeLevelData> reversedLevelDatas = upgradeTable.Levels
                .Reverse().Where(d => d.Level <= heroLevel);

            foreach (HeroUpgradeLevelData levelData in reversedLevelDatas)
            {
                if (!_areImpactsUpgraded && levelData.AreImpactUpgrading)
                {
                    provider.Impacts = levelData.ImpactRoster;
                    _areImpactsUpgraded = true;
                }

                foreach (UnitPropertySetup setup in levelData.Properties)
                    if (!_upgradedProperties.Contains(setup.Property))
                    {
                        UnitPropertyModification modification = ConvertToModification(setup);
                        _propertiesModifier.Modify(provider, modification);
                        _upgradedProperties.Add(setup.Property);
                    }
            }
        }

        private UnitPropertyModification ConvertToModification(UnitPropertySetup setup) =>
            new(UnitPropertyOp.Set, setup.Property, setup.Value, UnitPropertyMeasure.Units);
    }
}