﻿using System;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle;

namespace Frostgate.RiftHunters.Meta
{
    public sealed class UnitPropertiesModifier
    {
        public void Modify([NotNull] UnitPropertiesProvider provider, UnitPropertyModification modification)
        {
            switch (modification.Property)
            {
                case UnitProperty.Health:
                    provider.Health = ModifyProperty(provider.Health, modification);
                    break;

                case UnitProperty.Energy:
                    provider.Energy = ModifyProperty(provider.Energy, modification);
                    break;

                case UnitProperty.Stamina:
                    provider.Stamina = ModifyProperty(provider.Stamina, modification);
                    break;

                case UnitProperty.MoveSpeed:
                    provider.Speed = ModifyProperty(provider.Speed, modification);
                    break;

                case UnitProperty.TurnSpeed:
                    provider.TurnSpeed = ModifyProperty(provider.TurnSpeed, modification);
                    break;

                case UnitProperty.HealthRecoverySpeed:
                    float damageCooldown = provider.HpAutoRecovery.DamageCooldown;
                    float hpRecoverySpeed = ModifyProperty(provider.HpAutoRecovery.PercentPerSecond, modification);
                    provider.HpAutoRecovery = new HpAutoRecoveryConfig(hpRecoverySpeed, damageCooldown);
                    break;

                case UnitProperty.EnergyRecoverySpeed:
                    float energyRecoverySpeed = ModifyProperty(provider.EnergyAutoRecovery.Speed, modification);
                    provider.EnergyAutoRecovery = new EnergyAutoRecoveryConfig(energyRecoverySpeed);
                    break;

                case UnitProperty.StaminaRecoverySpeed:
                    float staminaRecoverySpeed = ModifyProperty(provider.StaminaAutoRecovery.Speed, modification);
                    provider.StaminaAutoRecovery = new StaminaAutoRecoveryConfig(staminaRecoverySpeed);
                    break;

                case UnitProperty.MeleeDamage:
                    float meleeDamage = ModifyProperty(provider.MeleeWeaponOverride.GetDamage(), modification);
                    provider.MeleeWeaponOverride.OverrideDamage(meleeDamage);
                    break;

                case UnitProperty.MeleeAttackTime:
                    float meleeSpeed = ModifyProperty(provider.MeleeWeaponOverride.ShootTime, modification);
                    provider.MeleeWeaponOverride.OverrideAttackTime(meleeSpeed);
                    break;

                case UnitProperty.MeleeAttackCooldown:
                    float meleeCooldown = ModifyProperty(provider.MeleeWeaponOverride.Cooldown, modification);
                    provider.MeleeWeaponOverride.OverrideAttackCooldown(meleeCooldown);
                    break;

                default: throw new ArgumentOutOfRangeException(nameof(UnitProperty));
            }
        }

        private float ModifyProperty(float propertyValue, UnitPropertyModification modification)
        {
            float modificator = CalcModificator(propertyValue, modification.Value, modification.Measure);
            return ModifyProperty(propertyValue, modificator, modification.Operation);
        }

        private float CalcModificator(float propertyValue,
            float modificationValue, UnitPropertyMeasure modificationMeasure) =>
            modificationMeasure switch
            {
                UnitPropertyMeasure.Units => modificationValue,
                UnitPropertyMeasure.Percents => propertyValue * modificationValue,
                _ => throw new ArgumentOutOfRangeException(nameof(UnitPropertyMeasure))
            };

        private static float ModifyProperty(float property, float modificator, UnitPropertyOp operation) =>
            operation switch
            {
                UnitPropertyOp.None => property,
                UnitPropertyOp.Set => modificator,
                UnitPropertyOp.Add => property + modificator,
                UnitPropertyOp.Multiply => property * modificator,
                _ => throw new ArgumentOutOfRangeException(nameof(UnitPropertyOp))
            };
    }
}