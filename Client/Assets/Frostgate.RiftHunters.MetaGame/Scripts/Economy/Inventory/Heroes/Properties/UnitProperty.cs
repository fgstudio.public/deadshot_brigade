﻿namespace Frostgate.RiftHunters.Meta
{
    public enum UnitProperty
    {
        Health = 0,
        Energy = 1,
        Stamina = 2,
        MoveSpeed = 3,
        TurnSpeed = 4,
        HealthRecoverySpeed = 5,
        EnergyRecoverySpeed = 6,
        StaminaRecoverySpeed = 7,
        MeleeDamage = 8,
        MeleeAttackTime = 9,
        MeleeAttackCooldown = 10,
    }
}