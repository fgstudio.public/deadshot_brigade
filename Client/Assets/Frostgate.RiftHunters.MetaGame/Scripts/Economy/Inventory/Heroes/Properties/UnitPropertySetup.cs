﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Meta
{
    [Serializable, InlineProperty]
    public struct UnitPropertySetup
    {
        [HideLabel, HorizontalGroup]
        [SerializeField] private UnitProperty _property;

        [HideLabel, HorizontalGroup, SuffixLabel("@" + nameof(ValueSuffix), true)]
        [SerializeField] private float _value;

        public UnitProperty Property => _property;
        public float Value => _value;

        private string ValueSuffix => UnitPropertySuffixRoster.GetLabel(_property);

        public UnitPropertySetup(UnitProperty property, float value)
        {
            _property = property;
            _value = value;
        }
    }
}