﻿using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle;

namespace Frostgate.RiftHunters.Meta
{
    public interface IReadOnlyUnitPropertiesProvider
    {
        float Health { get; }
        float Stamina { get; }
        float Energy { get; }
        float Speed { get; }
        float TurnSpeed { get; }

        [CanBeNull] UnitImpactRoster Impacts { get; }

        [CanBeNull] IMeleeWeapon MeleeWeapon { get; }
        [CanBeNull] RangeWeaponConfig RangeWeapon { get; }

        [CanBeNull] HpAutoRecoveryConfig HpAutoRecovery { get; }
        [CanBeNull] EnergyAutoRecoveryConfig EnergyAutoRecovery { get; }
        [CanBeNull] StaminaAutoRecoveryConfig StaminaAutoRecovery { get; }
    }
}