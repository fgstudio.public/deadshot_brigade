﻿using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta
{
    public sealed class UnitPropertiesEquipmentModifier
    {
        private readonly UnitPropertiesModifier _propertiesModifier;

        public UnitPropertiesEquipmentModifier(UnitPropertiesModifier propertiesModifier)
        {
            _propertiesModifier = propertiesModifier;
        }

        public void Modify([NotNull] UnitPropertiesProvider provider,
            [NotNull, ItemNotNull] IEnumerable<Equipment> equipments)
        {
            foreach (Equipment equipment in equipments)
            foreach (UnitPropertyModification modification in equipment.Modifications)
                _propertiesModifier.Modify(provider, modification);
        }
    }
}