﻿using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta
{
    public sealed class UnitPropertiesWeaponModifier
    {
        public void Modify([NotNull] UnitPropertiesProvider provider,
            [NotNull, ItemNotNull] IReadOnlyList<Equipment> equipments)
        {
            IMeleeWeapon equippedMeleeWeapon = equipments.OfType<IMeleeWeapon>().FirstOrDefault();
            RangeWeaponConfig equippedRangeWeapon = equipments.OfType<RangeWeaponConfig>().FirstOrDefault();

            if (equippedRangeWeapon != null)
                provider.RangeWeapon = equippedRangeWeapon;

            if (equippedMeleeWeapon != null)
                if (provider.MeleeWeaponOverride != null)
                    provider.MeleeWeaponOverride.OverrideWeapon(equippedMeleeWeapon);
                else
                    provider.MeleeWeaponOverride = new MeleeWeaponOverride(equippedMeleeWeapon);
        }
    }
}