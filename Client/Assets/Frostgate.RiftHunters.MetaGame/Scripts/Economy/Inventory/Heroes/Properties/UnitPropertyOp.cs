﻿namespace Frostgate.RiftHunters.Meta
{
    public enum UnitPropertyOp
    {
        None = 0,
        Set = 1,
        Add = 2,
        Multiply = 3,
    }
}