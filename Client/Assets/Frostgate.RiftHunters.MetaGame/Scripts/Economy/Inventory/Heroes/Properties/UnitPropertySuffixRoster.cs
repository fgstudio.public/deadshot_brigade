﻿using System;

namespace Frostgate.RiftHunters.Meta
{
    public static class UnitPropertySuffixRoster
    {
        public const string HealthLabel = "points";
        public const string EnergyLabel = "points";
        public const string StaminaLabel = "points";
        public const string MoveSpeedLabel = "";
        public const string TurnSpeedLabel = "";
        public const string HealthRecoverySpeedLabel = "hp percent/sec";
        public const string EnergyRecoverySpeedLabel = "energy/sec";
        public const string StaminaRecoverySpeedLabel = "stamina/sec";
        public const string MeleeDamageLabel = "";
        public const string MeleeAttackTimeLabel = "sec";
        public const string MeleeAttackCooldownLabel = "sec";

        public static string GetLabel(UnitProperty unitProperty) =>
            unitProperty switch
            {
                UnitProperty.Health => HealthLabel,
                UnitProperty.Energy => EnergyLabel,
                UnitProperty.Stamina => StaminaLabel,
                UnitProperty.MoveSpeed => MoveSpeedLabel,
                UnitProperty.TurnSpeed => TurnSpeedLabel,
                UnitProperty.HealthRecoverySpeed => HealthRecoverySpeedLabel,
                UnitProperty.EnergyRecoverySpeed => EnergyRecoverySpeedLabel,
                UnitProperty.StaminaRecoverySpeed => StaminaRecoverySpeedLabel,
                UnitProperty.MeleeDamage => MeleeDamageLabel,
                UnitProperty.MeleeAttackTime => MeleeAttackTimeLabel,
                UnitProperty.MeleeAttackCooldown => MeleeAttackCooldownLabel,
                _ => throw new ArgumentOutOfRangeException(nameof(unitProperty), unitProperty, "Wrong " + nameof(UnitProperty))
            };
    }
}