﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Meta
{
    [Serializable, InlineProperty]
    public struct UnitPropertyModification
    {
        [HideLabel, HorizontalGroup, OnValueChanged(nameof(OnOperationChanged))]
        [SerializeField] private UnitPropertyOp _operation;

        [HideLabel, HorizontalGroup, EnableIf(nameof(IsPropertyEnabled))]
        [SerializeField] private UnitProperty _property;

        [HideLabel, HorizontalGroup, EnableIf(nameof(IsValueEnabled))]
        [SuffixLabel("@" + nameof(ValueSuffix), true)]
        [SerializeField] private float _value;

        [HideLabel, HorizontalGroup, EnableIf(nameof(IsMeasureEnabled))]
        [SerializeField] private UnitPropertyMeasure _measure;

        public UnitPropertyOp Operation => _operation;
        public UnitProperty Property => IsPropertyEnabled ? _property : default;
        public float Value => IsValueEnabled ? _value : default;
        public UnitPropertyMeasure Measure => IsMeasureEnabled ? _measure : default;

        private bool IsPropertyEnabled => _operation is not UnitPropertyOp.None;
        private bool IsValueEnabled => _operation is not UnitPropertyOp.None;
        private bool IsMeasureEnabled => _operation is UnitPropertyOp.Set or UnitPropertyOp.Add;
        private string ValueSuffix => _measure.ToString().ToLower();

        public UnitPropertyModification(
            UnitPropertyOp operation, UnitProperty property, float value, UnitPropertyMeasure measure)
        {
            _operation = operation;
            _property = property;
            _value = value;
            _measure = measure;

            OnOperationChanged();
        }

        private void OnOperationChanged()
        {
            if (!IsPropertyEnabled) _property = default;
            if (!IsValueEnabled) _value = default;
            if (!IsMeasureEnabled) _measure = default;
        }
    }
}