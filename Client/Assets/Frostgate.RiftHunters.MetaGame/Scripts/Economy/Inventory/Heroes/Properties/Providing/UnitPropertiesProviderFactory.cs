﻿using JetBrains.Annotations;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta
{
    public sealed class UnitPropertiesProviderFactory
    {
        private readonly UnitPropertiesWeaponModifier _weaponModifier;
        private readonly UnitPropertiesUpgradeModifier _upgradeModifier;
        private readonly UnitPropertiesEquipmentModifier _equipmentModifier;

        public UnitPropertiesProviderFactory()
        {
            var propertiesModifier = new UnitPropertiesModifier();

            _weaponModifier = new UnitPropertiesWeaponModifier();
            _upgradeModifier = new UnitPropertiesUpgradeModifier(propertiesModifier);
            _equipmentModifier = new UnitPropertiesEquipmentModifier(propertiesModifier);
        }

        [NotNull]
        public IReadOnlyUnitPropertiesProvider Create(
            [CanBeNull] UnitConfig unitConfig, int heroLevel,
            [CanBeNull, ItemNotNull] IReadOnlyList<Equipment> equipments = null)
        {
            UnitPropertiesProvider provider = new(unitConfig);

            if (unitConfig?.UpgradeTable != null)
            {
                _upgradeModifier.Modify(provider, unitConfig.UpgradeTable, heroLevel);
            }

            if (equipments is { Count: > 0 })
            {
                _weaponModifier.Modify(provider, equipments);
                _equipmentModifier.Modify(provider, equipments);
            }

            return provider;
        }
    }
}