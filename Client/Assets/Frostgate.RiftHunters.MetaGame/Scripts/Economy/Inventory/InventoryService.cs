﻿using System.Linq;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Meta.Economy.Inventory
{
    public sealed class InventoryService
    {
        private readonly IEconomyApi _economyApi;
        private readonly IConfigCollection<Hero> _heroCollection;
        private readonly IConfigCollection<InventoryItem> _itemCollection;
        private readonly IConfigCollection<Equipment> _equipmentCollection;

        [UsedImplicitly]
        public InventoryService(IEconomyApi economyApi,
            IConfigCollection<Hero> heroCollection, IConfigCollection<InventoryItem> itemCollection,
            IConfigCollection<Equipment> equipmentCollection)
        {
            _economyApi = economyApi;
            _heroCollection = heroCollection;
            _itemCollection = itemCollection;
            _equipmentCollection = equipmentCollection;
        }

        public async UniTask<IEnumerable<Equipment>> GetAllEquipment() =>
            (await _economyApi.PlayerInventory.GetAllItemsAsync())
            .Where(_equipmentCollection.Contains)
            .Select(id => _equipmentCollection[id]);

        public async UniTask<IEnumerable<Hero>> GetAllHeroes() =>
            (await _economyApi.PlayerInventory.GetAllItemsAsync())
            .Where(_heroCollection.Contains)
            .Select(id => _heroCollection[id]);

        public async UniTask<IEnumerable<InventoryItem>> GetAllItems() =>
            (await _economyApi.PlayerInventory.GetAllItemsAsync())
            .Select(id => _itemCollection[id]);

        public UniTask<IEnumerable<string>> GetAllPlayerInventoryItemIds(string inventoryItemId) =>
            _economyApi.PlayerInventory.GetAllPlayerInventoryItemIdsAsync(inventoryItemId);

        public UniTask<bool> HasItem(string inventoryItemId) =>
            _economyApi.PlayerInventory.HasItemAsync(inventoryItemId);

        public UniTask<int> GetItemsCount(string inventoryItemId) =>
            _economyApi.PlayerInventory.GetItemsCountAsync(inventoryItemId);

        public UniTask AddInventoryItem(string inventoryItemId) =>
            _economyApi.PlayerInventory.AddInventoryItemAsync(inventoryItemId);

        public UniTask DeleteInventoryItem(string inventoryItemId, string playerInventoryItemId) =>
            _economyApi.PlayerInventory.DeleteInventoryItemAsync(inventoryItemId, playerInventoryItemId);
    }
}