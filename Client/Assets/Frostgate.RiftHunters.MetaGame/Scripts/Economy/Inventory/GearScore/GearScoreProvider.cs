﻿using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Frostgate.RiftHunters.Core;
using Frostgate.RiftHunters.Core.Battle;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta
{
    public sealed class GearScoreProvider
    {
        [NotNull] private readonly EquipmentService _equipmentService;
        [NotNull] private readonly HeroUpgradeService _heroUpgradeService;
        [NotNull] private readonly IConfigCollection<Hero> _heroCollection;
        [NotNull] private readonly IConfigCollection<Equipment> _equipmentCollection;

        public GearScoreProvider(
            [NotNull] EquipmentService equipmentService,
            [NotNull] HeroUpgradeService heroUpgradeService,
            [NotNull] IConfigCollection<Hero> heroCollection,
            [NotNull] IConfigCollection<Equipment> equipmentCollection)
        {
            _equipmentService = equipmentService;
            _heroUpgradeService = heroUpgradeService;

            _heroCollection = heroCollection;
            _equipmentCollection = equipmentCollection;
        }

        public int CalcTotalHeroGearScore(string heroId)
        {
            int heroLevel = _heroUpgradeService.GetHeroLevel(heroId);
            int baseGearScore = CalcBaseHeroGearScore(heroId, heroLevel);
            int equipmentGearScore = CalcHeroEquipmentGearScore(heroId);

            return baseGearScore + equipmentGearScore;
        }

        public int CalcBaseHeroGearScore(string heroId, int level)
        {
            UnitConfig unitConfig = _heroCollection[heroId].Config;
            HeroUpgradeTable upgradeTable = unitConfig.UpgradeTable;
            HeroUpgradeLevelData levelData = upgradeTable.GetLevelData(level);

            return levelData?.GearScore ?? unitConfig.GearScore;
        }

        public int CalcEquipmentGearScore(string equipmentId) =>
            _equipmentCollection[equipmentId].GearScore;

        public int CalcHeroEquipmentGearScore(string heroId)
        {
            IEnumerable<Equipment> equippedItems = _equipmentService.FindEquippedItems(heroId);
            IEnumerable<int> gearScores = equippedItems.Select(item => item.GearScore);

            int count = 0;
            int totalGearScore = 0;

            foreach (int gearScore in gearScores)
            {
                count++;
                totalGearScore += gearScore;
            }

            return count > 0 ? totalGearScore / count : 0;
        }
    }
}