﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Meta.Economy.Inventory
{
    public abstract class InventoryItem : EconomyEntity
    {
        protected const string MetaDataName = "Meta Data";

        [SerializeField, BoxGroup(MetaDataName), Required] protected Sprite _icon;
        [SerializeField, BoxGroup(MetaDataName), Required] protected string _name;
        [SerializeField, BoxGroup(MetaDataName), Required, Multiline] protected string _description;

        public Sprite Icon => _icon;
        public string Name => _name;
        public string Description => _description;
    }
}