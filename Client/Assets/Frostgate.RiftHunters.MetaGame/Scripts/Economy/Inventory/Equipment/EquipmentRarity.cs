﻿namespace Frostgate.RiftHunters.Meta.Economy.Inventory
{
    public enum EquipmentRarity
    {
        Common = 0,
        Uncommon = 1,
        Rare = 2,
        Legendary = 3,
        Exotic = 4,
        Mythic = 5
    }
}