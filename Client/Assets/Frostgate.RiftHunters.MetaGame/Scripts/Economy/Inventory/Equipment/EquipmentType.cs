﻿namespace Frostgate.RiftHunters.Meta.Economy.Inventory
{
    public enum EquipmentType
    {
        Head = 0,
        Body = 1,
        Arms = 2,
        Legs = 3,
        RangeWeapon = 4,
    }
}