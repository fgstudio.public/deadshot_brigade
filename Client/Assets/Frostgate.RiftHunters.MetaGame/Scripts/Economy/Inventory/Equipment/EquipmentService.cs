﻿using System;
using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Meta.Economy.Inventory
{
    public sealed class EquipmentService
    {
        private readonly ISaveService _saveService;
        private readonly InventoryService _inventoryService;
        private readonly HeroUpgradeService _heroUpgradeService;
        private readonly IConfigCollection<Hero> _heroCollection;
        private readonly EquipmentLimitations _equipmentLimitations;
        private readonly IConfigCollection<Equipment> _equipmentCollection;
        private readonly ILogger _logger = LoggerFactory.CreateLogger<EquipmentService>();

        private HeroesEquipmentData _data;

        [UsedImplicitly]
        public EquipmentService(
            [NotNull] ISaveService saveService,
            [NotNull] InventoryService inventoryService,
            [NotNull] HeroUpgradeService heroUpgradeService,
            [NotNull] IConfigCollection<Hero> heroCollection,
            [NotNull] EquipmentLimitations equipmentLimitations,
            [NotNull] IConfigCollection<Equipment> equipmentCollection)
        {
            _saveService = saveService;
            _inventoryService = inventoryService;
            _heroUpgradeService = heroUpgradeService;
            _heroCollection = heroCollection;
            _equipmentCollection = equipmentCollection;
            _equipmentLimitations = equipmentLimitations;
        }

        public async UniTask InitializeAsync()
        {
            if (_data != null)
                throw new InvalidOperationException("Service is already initialized");

            _logger.Log("Initializing...");

            _data = await _saveService.GetData<HeroesEquipmentData>();

            if (_data == null)
                throw new ArgumentNullException(
                    nameof(HeroesEquipmentData), "Isn't initialized with processor");

            _logger.Log("Initialized");
        }

        public async UniTask EquipAsync(string heroId, string equipmentId)
        {
            if (!await CanEquip(heroId, equipmentId))
                throw new InvalidOperationException($"Can't equip item: " +
                                                    $"[{nameof(heroId)}: {heroId}], " +
                                                    $"[{nameof(equipmentId)}: {equipmentId}]");

            if (!_data.TryGetValue(heroId, out List<string> heroEquipment))
                _data[heroId] = heroEquipment = new List<string>();

            heroEquipment.Add(equipmentId);
            _logger.Log($"Equipped: [{nameof(heroId)}: {heroId}], [{nameof(equipmentId)}: {equipmentId}]");

            await _saveService.SaveData(_data);
        }

        public async UniTask UnequipAsync(string heroId, string equipmentId)
        {
            if (!IsEquipped(heroId, equipmentId))
                throw new InvalidOperationException($"Can't unequip not equipped item: " +
                                                    $"[{nameof(heroId)}: {heroId}], " +
                                                    $"[{nameof(equipmentId)}: {equipmentId}]");

            List<string> heroEquipment = _data[heroId];
            heroEquipment.Remove(equipmentId);
            if (heroEquipment.Count == 0) _data.Remove(heroId);
            _logger.Log($"Unequipped: [{nameof(heroId)}: {heroId}], [{nameof(equipmentId)}: {equipmentId}]");

            await _saveService.SaveData(_data);
        }

        public async UniTask UnequipFromAllHeroesAsync(string equipmentId)
        {
            foreach (string heroId in _data.Keys.ToArray())
                if (IsEquipped(heroId, equipmentId))
                    await UnequipAsync(heroId, equipmentId);
        }

        public async UniTask<bool> CanEquip(string heroId, string equipmentId)
        {
            if (IsEquipped(heroId, equipmentId))
                return false;

            Hero hero = FindHeroConfig(heroId);
            Equipment equipment = FindEquipmentConfig(equipmentId);

            if (equipment.Class != HeroClass.None && equipment.Class != hero.Class)
                return false;

            if (equipment.Rank > _heroUpgradeService.GetHeroRank(heroId))
                return false;

            IEnumerable<Equipment> equippedItems = FindEquippedItems(heroId);
            int maxQuantity = _equipmentLimitations.GetMaxQuantity(equipment.Type);
            int quantityOfType = equippedItems.Count(e => e.Type == equipment.Type);

            if (quantityOfType >= maxQuantity)
                return false;

            if (!await _inventoryService.HasItem(heroId))
                return false;

            if (!await _inventoryService.HasItem(equipmentId))
                return false;

            return true;
        }

        public IEnumerable<Hero> FindEquippedHeroes(string equipmentId) =>
            _data.Count == 0
                ? Enumerable.Empty<Hero>()
                : _data.Where(kv => kv.Value.Contains(equipmentId)).Select(kv => kv.Key).Select(FindHeroConfig);

        public IEnumerable<Equipment> FindEquippedItems(string heroId) =>
            _data.TryGetValue(heroId, out List<string> heroEquipment)
                ? heroEquipment.Select(FindEquipmentConfig)
                : Enumerable.Empty<Equipment>();

        public bool IsEquipped(string heroId, string equipmentId) =>
            _data.TryGetValue(heroId, out List<string> heroEquipment) && heroEquipment.Contains(equipmentId);

        private Equipment FindEquipmentConfig(string equipmentId) =>
            _equipmentCollection[equipmentId];

        private Hero FindHeroConfig(string heroId) =>
            _heroCollection[heroId];
    }
}