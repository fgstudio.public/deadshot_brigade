﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Meta.Economy.Inventory
{
    [CreateAssetMenu(fileName = nameof(Equipment),
        menuName = MetaAssetMenus.Services.Economy + "/" + nameof(Equipment))]
    public class Equipment : InventoryItem
    {
        private const string CoreDataName = "Core Data";

        [BoxGroup(CoreDataName), EnableIf(nameof(IsTypePropertyEnabled))]
        [SerializeField] protected EquipmentType _type;

        [BoxGroup(CoreDataName)]
        [SerializeField] protected HeroClass _class;

        [BoxGroup(CoreDataName)]
        [SerializeField] protected EquipmentRarity _rarity;

        [BoxGroup(CoreDataName)]
        [SerializeField, MinValue(0)] private int _gearScore;

        [BoxGroup(CoreDataName)]
        [SerializeField, MinValue(1)] private int _rank;

        [Required, BoxGroup(CoreDataName)]
        [SerializeField] protected UnitPropertyModification[] _modifications;

        public EquipmentType Type => _type;
        public HeroClass Class => _class;
        public EquipmentRarity Rarity => _rarity;
        public int GearScore => _gearScore;
        public int Rank => _rank;
        public IReadOnlyList<UnitPropertyModification> Modifications => _modifications;

        protected virtual bool IsTypePropertyEnabled => true;
    }
}