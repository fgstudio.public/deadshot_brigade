﻿using TMPro;
using Zenject;
using Sirenix.OdinInspector;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Frostgate.RiftHunters.Infrastructure.Services;

namespace Frostgate.RiftHunters.Meta.Economy.Inventory.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu(MetaComponentMenus.UI.Menu + "/" + nameof(UIInventoryCard))]
    public sealed class UIInventoryCard : MonoBehaviour
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Image _iconComponent;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _nameComponent;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _descriptionComponent;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _countComponent;

        [Header("Settings")]
        [SerializeField, Required, AssetSelector] private InventoryItem _item;

        [CanBeNull] private IEconomyApi _economyApi;

        [CanBeNull] private CancellationTokenSource _cts;
        [CanBeNull] private AsyncOperationHandle<Sprite>? _iconOpHandle;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() => _iconComponent ??= GetComponentInChildren<Image>();

        private void OnDestroy()
        {
            DisposeCts(_cts);

            if (_iconOpHandle.HasValue)
                Addressables.Release(_iconOpHandle.Value);
        }

        [Inject]
        private void MonoConstructor([NotNull] IEconomyApi economyApi)
        {
            _economyApi = economyApi;
            StartUpdatingCard();
        }

        [Button(ButtonSizes.Medium, Name = "Update Card")]
        private void StartUpdatingCard()
        {
            DisposeCts(_cts);

            if (_iconOpHandle.HasValue)
                Addressables.Release(_iconOpHandle.Value);

            _cts = new CancellationTokenSource();
            UpdatingRoutine(_item, _cts.Token).SuppressCancellationThrow();
        }

        private void DisposeCts([CanBeNull] CancellationTokenSource cts)
        {
            if (cts != null)
            {
                cts.Cancel();
                cts.Dispose();
            }
        }

        private async UniTask UpdatingRoutine(InventoryItem item, CancellationToken token)
        {
            _nameComponent.text = item.Name;
            _descriptionComponent.text = item.Description;
            _iconComponent.sprite = item.Icon;

            long count = await _economyApi!.PlayerInventory.GetItemsCountAsync(item.Id)
                .AttachExternalCancellation(token);

            _countComponent.text = count.ToString();
        }

        private async UniTask<Sprite> LoadIcon(AsyncOperationHandle<Sprite> opHandle, CancellationToken token)
        {
            await opHandle.WithCancellation(token);
            return opHandle.Result;
        }
    }
}