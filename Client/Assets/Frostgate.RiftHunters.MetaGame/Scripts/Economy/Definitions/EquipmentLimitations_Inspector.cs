﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta.Economy
{
    public partial class EquipmentLimitations
    {
        [Serializable, InlineProperty]
        private sealed class KeyValue
        {
            [SerializeField, HideLabel, HorizontalGroup] private EquipmentType _type;
            [SerializeField, HideLabel, HorizontalGroup, MinValue(0)] private int _maxQuantity;

            public EquipmentType Type => _type;
            public int MaxQuantity => _maxQuantity;

            public KeyValue(EquipmentType type) =>
                _type = type;
        }

        private const string AllUniqueError = "Has duplicating types!";
        private const string AllTypesError = "Doen't have all types!";

        private EquipmentType[] _typesCache;
        private IReadOnlyList<EquipmentType> Types =>
            _typesCache ??= Enum.GetValues(typeof(EquipmentType)) as EquipmentType[];

        private void OnValidate()
        {
            if (_quantityLimitations.Length == 0)
                _quantityLimitations = Types.Select(t => new KeyValue(t)).ToArray();
        }

        private bool AreAllUnique(KeyValue[] limitations)
        {
            if (limitations.Length < 2)
                return true;

            if (limitations.Length > Types.Count)
                return false;

            return limitations.Select(l => l.Type).Distinct().Count() == limitations.Select(l => l.Type).Count();
        }

        private bool HasAllTypes(KeyValue[] limitations)
        {
            if (limitations.Length < Types.Count)
                return false;

            return limitations.Select(l => l.Type).Distinct().Count() == Types.Count;
        }
    }
}