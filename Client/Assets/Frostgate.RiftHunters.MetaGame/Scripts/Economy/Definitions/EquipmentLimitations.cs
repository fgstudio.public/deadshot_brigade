﻿using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta.Economy
{
    [CreateAssetMenu(fileName = nameof(EquipmentLimitations),
        menuName = MetaAssetMenus.Menu + "/" + nameof(EquipmentLimitations))]
    public sealed partial class EquipmentLimitations : ScriptableObject
    {
        [ListDrawerSettings(Expanded = true)]
        [ValidateInput(nameof(HasAllTypes), AllTypesError)]
        [ValidateInput(nameof(AreAllUnique), AllUniqueError)]
        [SerializeField] private KeyValue[] _quantityLimitations;

        public int GetMaxQuantity(EquipmentType type)
        {
            KeyValue kv = _quantityLimitations.FirstOrDefault(ql => ql.Type == type);
            return kv?.MaxQuantity ?? default;
        }
    }
}