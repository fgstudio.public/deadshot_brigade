﻿using UnityEngine;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Meta.Economy
{
    public abstract class EconomyEntity : ScriptableObject, IConfig
    {
        [Required, EnableIf(nameof(IsIdPropertyEnabled))]
        [SerializeField] protected string _id;

        public string Id => _id;

        protected virtual bool IsIdPropertyEnabled => true;
    }
}