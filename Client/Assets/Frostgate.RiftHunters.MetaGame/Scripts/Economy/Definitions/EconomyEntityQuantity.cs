﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Meta.Economy
{
    [Serializable, InlineProperty]
    public sealed class EconomyEntityQuantity
    {
        [Required, AssetSelector, HideLabel, HorizontalGroup]
        [SerializeField] private EconomyEntity _entity;

        [MinValue(0), HideLabel, HorizontalGroup]
        [SerializeField] private int _quantity;

        public EconomyEntity Entity => _entity;
        public int Quantity => _quantity;
    }
}