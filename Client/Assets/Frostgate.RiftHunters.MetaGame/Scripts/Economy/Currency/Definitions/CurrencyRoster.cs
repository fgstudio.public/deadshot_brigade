﻿using UnityEngine;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using Frostgate.RiftHunters.Infrastructure.Services;

namespace Frostgate.RiftHunters.Meta.Economy
{
    [CreateAssetMenu(fileName = nameof(CurrencyRoster), menuName = MetaAssetMenus.Services.Economy + "/" + nameof(CurrencyRoster))]
    public sealed class CurrencyRoster : ScriptableObject
    {
        [SerializeField, Required, AssetSelector] private Currency _hard;
        [SerializeField, Required, AssetSelector] private Currency _softHero;
        [SerializeField, Required, AssetSelector] private Currency _softGacha;
        [SerializeField, Required, AssetSelector] private Currency _softEquipment;

        public Currency Hard => _hard;
        public Currency SoftHero => _softHero;
        public Currency SoftGacha => _softGacha;
        public Currency SoftEquipment => _softEquipment;

        public IEnumerable<Currency> All
        {
            get
            {
                yield return Hard;
                yield return SoftHero;
                yield return SoftGacha;
                yield return SoftEquipment;
            }
        }
    }
}