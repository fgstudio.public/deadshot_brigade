﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

namespace Frostgate.RiftHunters.Meta.Economy
{
    [Serializable, InlineProperty]
    public struct CurrencyQuantity
    {
        [Required, AssetSelector, HideLabel, HorizontalGroup]
        [SerializeField] private Currency _currency;

        [MinValue(0), HideLabel, HorizontalGroup]
        [SerializeField] private int _quantity;

        public Currency Currency => _currency;
        public int Quantity => _quantity;

        public CurrencyQuantity(Currency currency, int quantity)
        {
            _currency = currency;
            _quantity = quantity;
        }

        public override string ToString() =>
            $"\"{_currency.Id}\": {_quantity}";
    }
}