﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Frostgate.RiftHunters.Meta.Economy
{
    [CreateAssetMenu(fileName = nameof(Currency), menuName = MetaAssetMenus.Services.Economy + "/" + nameof(Currency))]
    public sealed class Currency : EconomyEntity
    {
        [SerializeField, Required] private Sprite _icon;
        [SerializeField, Required] private string _name;
        [SerializeField, Required, Multiline] private string _description;

        public Sprite Icon => _icon;
        public string Name => _name;
        public string Description => _description;
    }
}