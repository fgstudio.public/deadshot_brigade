﻿using System;
using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Cysharp.Threading.Tasks;
using Frostgate.RiftHunters.Core;

namespace Frostgate.RiftHunters.Meta.Economy
{
    public sealed class WalletService : IDisposable
    {
        public event Action<CurrencyQuantity> CurrencyChanged;

        private readonly IEconomyApi _economyApi;
        private readonly IConfigCollection<Currency> _currencyCollection;
        private readonly ILogger _logger = LoggerFactory.CreateLogger<WalletService>();

        [UsedImplicitly]
        public WalletService(
            [NotNull] IEconomyApi economyApi,
            [NotNull] IConfigCollection<Currency> currencyCollection)
        {
            _economyApi = economyApi;
            _currencyCollection = currencyCollection;

            _economyApi.PlayerBalances.CurrencyChanged += OnCurrencyChangedAsync;
            _logger.Log("Subscribed");
        }

        public void Dispose()
        {
            _economyApi.PlayerBalances.CurrencyChanged -= OnCurrencyChangedAsync;
            _logger.Log("Unsubscribed");
            _logger.Log("Disposed");
        }

        private async void OnCurrencyChangedAsync(string currencyId)
        {
            CurrencyQuantity quantity = await GetBalanceAsync(currencyId);
            CurrencyChanged?.Invoke(quantity);
        }

        public UniTask<bool> HasBalanceAsync(CurrencyQuantity quantity) =>
            HasBalanceAsync(quantity.Currency.Id, quantity.Quantity);

        public async UniTask<bool> HasBalanceAsync(string currencyId, int quantity)
        {
            CurrencyQuantity actualQuantity = await GetBalanceAsync(currencyId);
            return actualQuantity.Quantity >= quantity;
        }

        public async UniTask<IEnumerable<CurrencyQuantity>> GetTotalBalanceAsync()
        {
            List<CurrencyQuantity> quantities = new(_currencyCollection.Count);

            _logger.Log("Getting Total Balance...");
            foreach (Currency currency in _currencyCollection)
            {
                CurrencyQuantity quantity = await GetBalanceAsync(currency.Id);
                quantities.Add(quantity);
            }
            _logger.Log("Gotten Total Balance");

            return quantities.Where(q => q.Quantity > 0);
        }

        public async UniTask<CurrencyQuantity> GetBalanceAsync(string currencyId)
        {
            _logger.Log($"Getting balance for \"{currencyId}\"...");
            long balance = await _economyApi.PlayerBalances.GetBalanceAsync(currencyId);
            Currency currency = FindCurrencyConfig(currencyId);
            CurrencyQuantity quantity = new(currency, (int)balance);
            _logger.Log($"Gotten balance for \"{currencyId}\"...");

            return quantity;
        }

        public UniTask SetBalanceAsync(CurrencyQuantity currencyQuantity) =>
            SetBalanceAsync(currencyQuantity.Currency.Id, currencyQuantity.Quantity);

        public async UniTask SetBalanceAsync(string currencyId, long balance)
        {
            _logger.Log($"Setting Balance Async [\"{currencyId}\": {balance}]...");
            await _economyApi.PlayerBalances.SetBalanceAsync(currencyId, balance);
            _logger.Log($"Set Balance Async [\"{currencyId}\": {balance}]...");
        }

        public UniTask IncrementBalanceAsync(CurrencyQuantity currencyQuantity) =>
            IncrementBalanceAsync(currencyQuantity.Currency.Id, currencyQuantity.Quantity);

        public async UniTask IncrementBalanceAsync(string currencyId, int amount)
        {
            _logger.Log($"Incrementing Balance Async \"{currencyId}\" by {amount}...");
            await _economyApi.PlayerBalances.IncrementBalanceAsync(currencyId, amount);
            _logger.Log($"Incremented Balance Async \"{currencyId}\" by {amount}...");
        }

        public UniTask DecrementBalanceAsync(CurrencyQuantity currencyQuantity) =>
            DecrementBalanceAsync(currencyQuantity.Currency.Id, currencyQuantity.Quantity);

        public async UniTask DecrementBalanceAsync(string currencyId, int amount)
        {
            _logger.Log($"Decrementing Balance Async \"{currencyId}\" by {amount}...");
            await _economyApi.PlayerBalances.DecrementBalanceAsync(currencyId, amount);
            _logger.Log($"Decremented Balance Async \"{currencyId}\" by {amount}...");
        }

        private Currency FindCurrencyConfig(string currencyId) =>
            _currencyCollection[currencyId];
    }
}