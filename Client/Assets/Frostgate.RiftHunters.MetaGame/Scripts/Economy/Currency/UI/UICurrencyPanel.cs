﻿using TMPro;
using Zenject;
using System.Threading;
using Sirenix.OdinInspector;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Frostgate.RiftHunters.Meta.Economy.UI
{
    [DisallowMultipleComponent]
    [AddComponentMenu(MetaComponentMenus.UI.Menu + "/" + nameof(UICurrencyPanel))]
    public sealed class UICurrencyPanel : MonoBehaviour
    {
        [Header("References")]
        [SerializeField, Required, ChildGameObjectsOnly] private Image _iconComponent;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _nameComponent;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _descriptionComponent;
        [SerializeField, Required, ChildGameObjectsOnly] private TMP_Text _countComponent;

        [Header("Settings")]
        [SerializeField, Required, AssetSelector] private Currency _currency;

        [CanBeNull] private WalletService _walletService;
        [CanBeNull] private CancellationTokenSource _cts;
        [CanBeNull] private AsyncOperationHandle<Sprite>? _iconOpHandle;

        private void Awake() => InitReferences();
        private void OnValidate() => InitReferences();
        private void InitReferences() => _iconComponent ??= GetComponentInChildren<Image>();

        private void OnDestroy()
        {
            DisposeCts(_cts);

            if (_iconOpHandle.HasValue)
                Addressables.Release(_iconOpHandle.Value);

            if (_walletService != null)
                _walletService.CurrencyChanged -= OnCurrencyChanged;
        }

        [Inject]
        private void MonoConstructor([NotNull] WalletService walletService)
        {
            _walletService = walletService;
            _walletService.CurrencyChanged += OnCurrencyChanged;

            StartUpdatingPanel();
        }

        private void OnCurrencyChanged(CurrencyQuantity currencyQuantity)
        {
            if (_currency.Id == currencyQuantity.Currency.Id)
                _countComponent.text = currencyQuantity.Quantity.ToString();
        }

        [Button(ButtonSizes.Medium, Name = "Update Panel")]
        private void StartUpdatingPanel()
        {
            DisposeCts(_cts);

            if (_iconOpHandle.HasValue)
                Addressables.Release(_iconOpHandle.Value);

            _cts = new CancellationTokenSource();
            UpdatingRoutine(_currency, _cts.Token).SuppressCancellationThrow();
        }

        private void DisposeCts([CanBeNull] CancellationTokenSource cts)
        {
            if (cts != null)
            {
                cts.Cancel();
                cts.Dispose();
            }
        }

        private async UniTask UpdatingRoutine(Currency currency, CancellationToken token)
        {
            _nameComponent.text = currency.Name;
            _descriptionComponent.text = currency.Description;
            _iconComponent.sprite = currency.Icon;

            CurrencyQuantity balance = await LoadBalance(currency.Id, token);
            _countComponent.text = balance.Quantity.ToString();
        }

        private async UniTask<Sprite> LoadIcon(AsyncOperationHandle<Sprite> opHandle, CancellationToken token)
        {
            await opHandle.WithCancellation(token);
            return opHandle.Result;
        }

        private async UniTask<CurrencyQuantity> LoadBalance(string currencyId, CancellationToken token) =>
            await _walletService!.GetBalanceAsync(currencyId)
                .AttachExternalCancellation(token);
    }
}