﻿using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Meta.Economy
{
    public interface IEconomyApi
    {
        IEconomyConfiguration Configuration { get; }
        IEconomyPlayerBalances PlayerBalances { get; }
        IEconomyPlayerInventory PlayerInventory { get; }
        IEconomyPurchases Purchases { get; }
        UniTask InitializeAsync();
    }
}