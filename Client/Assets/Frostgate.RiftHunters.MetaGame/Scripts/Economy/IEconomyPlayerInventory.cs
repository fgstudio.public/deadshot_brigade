﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;

namespace Frostgate.RiftHunters.Meta.Economy
{
    public interface IEconomyPlayerInventory
    {
        UniTask<bool> HasItemAsync(string inventoryItemId);
        UniTask<int> GetItemsCountAsync(string inventoryItemId);
        UniTask<IEnumerable<string>> GetAllItemsAsync();
        UniTask<IEnumerable<string>> GetAllPlayerInventoryItemIdsAsync(string inventoryItemId);
        UniTask AddInventoryItemAsync(string inventoryItemId);
        UniTask DeleteInventoryItemAsync(string inventoryItemId, string playerInventoryItemId);
    }
}