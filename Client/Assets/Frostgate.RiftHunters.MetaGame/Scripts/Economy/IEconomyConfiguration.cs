﻿using System.Collections.Generic;
using Unity.Services.Economy.Model;

namespace Frostgate.RiftHunters.Meta.Economy
{
    public interface IEconomyConfiguration
    {
        string GetConfigAssignmentHash();
        IReadOnlyList<CurrencyDefinition> GetCurrencies();
        IReadOnlyList<InventoryItemDefinition> GetInventoryItems();
        IReadOnlyList<VirtualPurchaseDefinition> GetVirtualPurchases();
        IReadOnlyList<RealMoneyPurchaseDefinition> GetRealMoneyPurchases();
        CurrencyDefinition GetCurrency(string id);
        InventoryItemDefinition GetInventoryItem(string id);
        VirtualPurchaseDefinition GetVirtualPurchase(string id);
        RealMoneyPurchaseDefinition GetRealMoneyPurchase(string id);
    }
}