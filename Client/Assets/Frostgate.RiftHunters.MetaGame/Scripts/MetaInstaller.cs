﻿using Zenject;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;
using Frostgate.RiftHunters.Meta.Player;
using Frostgate.RiftHunters.Meta.Processing;
using Frostgate.RiftHunters.Meta.Economy;
using Frostgate.RiftHunters.Meta.Economy.Inventory;

namespace Frostgate.RiftHunters.Meta
{
    // TODO: игнорировать на Core-Server
    [DisallowMultipleComponent]
    [AddComponentMenu(MetaComponentMenus.Menu + "/" + nameof(MetaInstaller))]
    public sealed class MetaInstaller : MonoInstaller
    {
        [SerializeField, Required, AssetSelector] private CurrencyRoster _currencyRoster;
        [SerializeField, Required, AssetSelector] private EquipmentLimitations _equipmentLimitations;
        [SerializeField, Required, AssetSelector] private DefaultMetaData _defaultMetaData;

        public override void InstallBindings()
        {
            BindConfigs();
            BindServices();
            BindDataInitProcessors();
            BindDefaultEconomyProcessors();
        }

        private void BindConfigs()
        {
            Container.Bind<CurrencyRoster>().FromInstance(_currencyRoster);
            Container.Bind<EquipmentLimitations>().FromInstance(_equipmentLimitations);
        }

        private void BindServices()
        {
            // TODO: с IDisposable создание объекта проихсодит раньше, чем наполняется IConfigCollection
            Container.Bind(typeof(WalletService)/*, typeof(IDisposable)*/).To<WalletService>().AsSingle();

            Container.Bind<PlayerDataService>().AsSingle();
            Container.Bind<CampaignService>().AsSingle();
            Container.Bind<InventoryService>().AsSingle();
            Container.Bind<EquipmentService>().AsSingle();
            Container.Bind<HeroUpgradeService>().AsSingle();
        }

        private void BindDataInitProcessors()
        {
            Container.Bind<PlayerDataInitProcessor>().AsSingle()
                .WithArguments(_defaultMetaData.HeroesData.First().Id);

            Container.Bind<CampaignDataInitProcessor>().AsSingle();

            Container.Bind<HeroesEquipmentDataInitProcessor>().AsSingle()
                .WithArguments(_defaultMetaData.HeroesData.Select(d => (IEquipmentHeroData)d));

            Container.Bind<HeroesUpgradeDataInitProcessor>().AsSingle()
                .WithArguments(_defaultMetaData.HeroesData.Select(d => (IUpgradeHeroData)d));
        }

        private void BindDefaultEconomyProcessors()
        {
            Container.Bind<DefaultInventoryProcessor>().AsSingle()
                .WithArguments(_defaultMetaData.HeroesData.Select(d => (IEquipmentHeroData)d));
        }
    }
}