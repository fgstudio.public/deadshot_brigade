﻿namespace Frostgate.RiftHunters.Meta
{
    public static class MetaAssetMenus
    {
        public const string Menu = AssetMenus.RiftHunters.Menu + "/Meta";

        public static class Campaign { public const string Menu = MetaAssetMenus.Menu + "/" + nameof(Campaign); }

        public static class Services
        {
            public const string Menu = MetaAssetMenus.Menu + "/" + nameof(Services);
            public const string Economy = Menu + "/" + nameof(Economy);
        }
    }
}