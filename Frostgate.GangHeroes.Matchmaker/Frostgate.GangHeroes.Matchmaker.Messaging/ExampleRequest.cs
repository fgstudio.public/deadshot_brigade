namespace Frostgate.RiftHunters.Matchmaker.Messaging
{
    public sealed class ExampleRequest
    {
        public const string Route = "example";

        public string EchoString { get; set; }
    }
}
