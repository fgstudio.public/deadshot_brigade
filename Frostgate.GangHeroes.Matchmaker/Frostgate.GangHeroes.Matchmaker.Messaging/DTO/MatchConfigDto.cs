﻿namespace Frostgate.RiftHunters.Matchmaker.Messaging;

public sealed class MatchConfigDto : IMatchConfig
{
    public string MissionId { get; set; }
    public int MaxPlayersCount { get; set; }

    public static MatchConfigDto From(IMatchConfig config)
    {
        return new()
        {
            MissionId = config.MissionId,
            MaxPlayersCount = config.MaxPlayersCount
        };
    }
}