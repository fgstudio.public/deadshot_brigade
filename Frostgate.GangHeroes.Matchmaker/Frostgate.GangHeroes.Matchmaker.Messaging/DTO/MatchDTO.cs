﻿using System;

namespace Frostgate.RiftHunters.Matchmaker.Messaging
{
    public sealed class MatchDto : IMatch
    {
        public Guid Id { get; set; }
        public Uri Uri { get; set; }
        public MatchState State { get; set; }
        public MatchConfigDto Config { get; set; }
        public int PlayersRank { get; set; }
        IMatchConfig IMatch.Config => Config;

        public static MatchDto From(IMatch match)
        {
            return new MatchDto()
            {
                Id = match.Id,
                Uri = match.Uri,
                State = match.State,
                Config = MatchConfigDto.From(match.Config),
                PlayersRank = match.PlayersRank
            };
        }
    }
}