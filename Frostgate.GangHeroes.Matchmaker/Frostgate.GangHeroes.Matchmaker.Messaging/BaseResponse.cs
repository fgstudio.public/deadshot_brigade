using System;

namespace Frostgate.RiftHunters.Matchmaker.Messaging
{
    public abstract class BaseResponse : BaseMessage
    {
        public BaseResponse(Guid correlationId) : base()
        {
            CorrelationId = correlationId;
        }

        public BaseResponse()
        {
        }
    }
}
