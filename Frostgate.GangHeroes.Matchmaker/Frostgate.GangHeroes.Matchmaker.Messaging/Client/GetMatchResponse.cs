﻿using System;

namespace Frostgate.RiftHunters.Matchmaker.Messaging.Client;

public sealed class GetMatchResponse : BaseResponse
{
    public MatchDto Match { get; }

    public GetMatchResponse(MatchDto match, Guid correlationId) : base(correlationId)
    {
        Match = match;
    }
}