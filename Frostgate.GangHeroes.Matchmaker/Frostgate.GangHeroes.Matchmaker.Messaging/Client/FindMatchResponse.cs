﻿using System;

namespace Frostgate.RiftHunters.Matchmaker.Messaging.Client
{
    public sealed class FindMatchResponse : BaseResponse
    {
        public MatchDto Match { get; set; }

        public FindMatchResponse(MatchDto match, Guid correlationId) : base(correlationId)
        {
            Match = match;
        }
    }
}