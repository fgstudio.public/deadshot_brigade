﻿namespace Frostgate.RiftHunters.Matchmaker.Messaging.Client
{
    public sealed class FindMatchRequest : BaseRequest
    {
        public const string Route = Routes.Client.Name + "/" + "find-match";

        public MatchConfigDto MatchConfig { get; set; }
        public int PlayerRank { get; set; }
    }
}