namespace Frostgate.RiftHunters.Matchmaker.Messaging
{
    public static class Routes
    {
        public const string Name = "api";

        public static class Client
        {
            public const string Name = Routes.Name + "/" + "client";
        }

        public static class Server
        {
            public const string Name = Routes.Name + "/" + "server";
        }
    }
}
