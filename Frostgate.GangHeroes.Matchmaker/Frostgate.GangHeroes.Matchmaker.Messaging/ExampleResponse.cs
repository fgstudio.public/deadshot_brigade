namespace Frostgate.RiftHunters.Matchmaker
{
    public sealed class ExampleResponse
    {
        public string EchoString { get; }

        public ExampleResponse(string echoString)
        {
            EchoString = echoString;
        }
    }
}
