using System;

namespace Frostgate.RiftHunters.Matchmaker.Messaging
{
    public abstract class BaseMessage
    {
        protected Guid CorrelationId = Guid.NewGuid();

        public Guid GetCorrelationId() => CorrelationId;
    }
}
