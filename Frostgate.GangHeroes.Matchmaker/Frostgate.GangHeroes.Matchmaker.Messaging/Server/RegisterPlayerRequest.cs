﻿using System;

namespace Frostgate.RiftHunters.Matchmaker.Messaging.Server
{
    public sealed class RegisterPlayerRequest : BaseRequest
    {
        public const string Route = Routes.Server.Name + "/" + "register-player";

        public Guid MatchId { get; }

        public RegisterPlayerRequest(Guid matchId)
        {
            MatchId = matchId;
        }
    }
}