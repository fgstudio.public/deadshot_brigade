﻿using System;

namespace Frostgate.RiftHunters.Matchmaker.Messaging.Server
{
    public sealed class UpdateMatchStateRequest
    {
        public const string Route = Routes.Server.Name + "/" + "update-match-state";

        public Guid MatchId { get; }
        public MatchState State { get; }

        public UpdateMatchStateRequest(Guid matchId, MatchState state)
        {
            MatchId = matchId;
            State = state;
        }
    }
}
