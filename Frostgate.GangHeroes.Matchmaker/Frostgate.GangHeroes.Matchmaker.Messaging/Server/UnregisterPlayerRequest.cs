﻿using System;

namespace Frostgate.RiftHunters.Matchmaker.Messaging.Server
{
    public sealed class UnregisterPlayerRequest : BaseRequest
    {
        public const string Route = Routes.Server.Name + "/" + "unregister-player";

        public Guid MatchId { get; }

        public UnregisterPlayerRequest(Guid matchId)
        {
            MatchId = matchId;
        }
    }
}