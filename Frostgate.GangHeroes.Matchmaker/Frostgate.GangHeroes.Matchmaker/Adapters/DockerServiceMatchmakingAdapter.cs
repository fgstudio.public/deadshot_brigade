﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Docker.DotNet.Models;
using Frostgate.RiftHunters.Matchmaker.Services.Docker;
using Frostgate.RiftHunters.Matchmaker.Services.Matchmaking.Internal;

namespace Frostgate.RiftHunters.Matchmaker.Adapters
{
    public sealed class DockerServiceMatchmakingAdapter : IDockerService
    {
        private const string Environment = nameof(Environment);

        private readonly DockerService _dockerService;

        public DockerServiceMatchmakingAdapter(DockerService dockerService)
        {
            _dockerService = dockerService;
        }

        public Task<string> RunContainerAsync(Guid id, IReadOnlyDictionary<string, string> envArgs,
            int hostPort, int internalPort, string transportScheme, string imageName,
            CancellationToken token = new CancellationToken())
        {
            var createParams = new CreateContainerParameters
            {
                Name = $"Match-{id.ToString()}",
                Image = imageName,
                ExposedPorts = new Dictionary<string, EmptyStruct>
                {
                    { $"{internalPort}/udp", default }
                },
                Env = envArgs.Select(a => $"{a.Key}={a.Value}").ToList(),
                HostConfig = new HostConfig
                {
                    PortBindings = new Dictionary<string, IList<PortBinding>>
                    {
                        {
                            $"{internalPort}/udp", new List<PortBinding>
                            {
                                new PortBinding { HostPort = hostPort.ToString() }
                            }
                        }
                    },
                    Binds = new List<string>
                    {
                        $"/srv/game-cluster/{System.Environment.GetEnvironmentVariable(Environment) ?? "undefined-stage"}/logs:/logs"
                    }
                }
            };

            return _dockerService.RunContainerAsync(createParams, token);
        }

        public Task<bool> StopContainerAsync(string containerId, CancellationToken token = new CancellationToken()) =>
            _dockerService.StopContainerAsync(containerId, token);
    }
}