﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Docker.DotNet;
using Docker.DotNet.Models;

namespace Frostgate.RiftHunters.Matchmaker.Services.Docker
{
    public sealed class DockerService
    {
        private const string WinDaemonSocketPath = "npipe://./pipe/docker_engine";
        private const string LinuxDaemonSocketPath = "unix:///var/run/docker.sock";

        private readonly DockerClient _dockerClient;

        public DockerService(IDockerServiceConfiguration configuration)
        {
            _dockerClient = new DockerClientConfiguration(GetDaemonUri(),
                defaultTimeout: TimeSpan.FromSeconds(configuration.ConnectionTimeoutSeconds)).CreateClient();
        }

        public async Task<string> RunContainerAsync(CreateContainerParameters parameters,
            CancellationToken token = new CancellationToken())
        {
            await PullImageAsync(parameters, token);

            var createResp = await _dockerClient.Containers.CreateContainerAsync(parameters, token);
            await _dockerClient.Containers.StartContainerAsync(createResp.ID, new ContainerStartParameters(), token);

            return createResp.ID;
        }

        public async Task<bool> StopContainerAsync(string containerId,
            CancellationToken token = new CancellationToken())
        {
            var result =
                await _dockerClient.Containers.StopContainerAsync(containerId, new ContainerStopParameters(), token);

            if (result)
                await _dockerClient.Containers.RemoveContainerAsync(containerId, new ContainerRemoveParameters(),
                    token);

            return result;
        }

        private Task PullImageAsync(CreateContainerParameters parameters, CancellationToken token)
        {
            string imageName = parameters.Image;
            string[] split = imageName.Split(':', StringSplitOptions.RemoveEmptyEntries);

            return PullImageAsync(split[0], split[1], token);
        }

        private Task PullImageAsync(string fromImage, string tag, CancellationToken token)
        {
            return _dockerClient.Images.CreateImageAsync(new ImagesCreateParameters
                {
                    FromImage = fromImage,
                    Tag = tag
                },
                null, new Progress<JSONMessage>(), token);
        }

        private Uri GetDaemonUri()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                return new Uri(LinuxDaemonSocketPath);

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                return new Uri(WinDaemonSocketPath);

            throw new InvalidOperationException($"Support for platform '{OSPlatform.OSX}' not implemented.");
        }
    }
}
