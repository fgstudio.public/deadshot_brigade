﻿namespace Frostgate.RiftHunters.Matchmaker.Services.Docker
{
    public interface IDockerServiceConfiguration
    {
        double ConnectionTimeoutSeconds { get; }
    }
}
