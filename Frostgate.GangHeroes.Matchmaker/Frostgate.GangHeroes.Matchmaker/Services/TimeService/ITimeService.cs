using System;

namespace Frostgate.RiftHunters.Matchmaker.Services.Time
{
    public interface ITimeService
    {
        DateTime Now { get; }
    }
}