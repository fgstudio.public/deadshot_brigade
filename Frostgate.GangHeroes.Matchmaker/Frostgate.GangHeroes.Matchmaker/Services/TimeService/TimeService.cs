﻿using System;

namespace Frostgate.RiftHunters.Matchmaker.Services.Time
{
    public sealed class TimeService : ITimeService
    {
        public DateTime Now => DateTime.UtcNow;
    }
}