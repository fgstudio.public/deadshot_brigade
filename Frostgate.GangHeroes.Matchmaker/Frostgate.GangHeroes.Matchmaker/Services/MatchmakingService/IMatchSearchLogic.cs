﻿using System.Threading.Tasks;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking
{
    public interface IMatchSearchLogic
    {
        Task<Match?> FindAsync(IMatchRepository matchRepository, IMatchConfig matchConfig, int playerRank);
    }
}