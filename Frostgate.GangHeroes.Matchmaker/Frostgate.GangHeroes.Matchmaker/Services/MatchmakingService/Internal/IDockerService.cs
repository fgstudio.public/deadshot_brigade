﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking.Internal
{
    public interface IDockerService
    {
        Task<string> RunContainerAsync(Guid id, IReadOnlyDictionary<string, string> envArgs, int hostPort,
            int internalPort, string transportScheme,
            string imageName,
            CancellationToken token = new CancellationToken());

        Task<bool> StopContainerAsync(string containerId, CancellationToken token = new CancellationToken());
    }
}