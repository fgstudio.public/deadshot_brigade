﻿using System;
using System.Threading.Tasks;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking.Internal
{
    public sealed class MatchOnContainer : Match
    {
        public event ValueChanged StateChanged = (oldValue, newValue) => { };

        public override Guid Id { get; }
        public override DateTime CreationTime { get; }

        public override Uri Uri { get; }


        public override int PlayersRank { get; }

        public override Task<Match> OnReady => _onReadyTcs.Task;

        public override MatchState State
        {
            get => _state;
            set => SetState(value);
        }

        public override int PlayersCount
        {
            get => _playersCount;
            set
            {
                if (value > Config.MaxPlayersCount || value < 0)
                    throw new ArgumentException(
                        $"Invalid value for {nameof(PlayersCount)}. Must be positive and less than {Config.MaxPlayersCount}.");

                _playersCount = value;
            }
        }

        public override IMatchConfig Config { get; }

        public string ContainerId { get; }

        private int _playersCount;
        private MatchState _state;
        private readonly TaskCompletionSource<Match> _onReadyTcs = new TaskCompletionSource<Match>();

        public MatchOnContainer(Guid id, Uri uri, MatchState state, string containerId, DateTime creationTime, IMatchConfig config, int playersRank)
        {
            Id = id;
            Uri = uri;
            _state = state;
            ContainerId = containerId;
            CreationTime = creationTime;
            PlayersRank = playersRank;
            Config = config;
        }

        private void SetState(MatchState state)
        {
            if (state == _state)
                return;

            MatchState oldState = _state;
            _state = state;

            if (_state == MatchState.Ready)
                _onReadyTcs.TrySetResult(this);

            StateChanged.Invoke(oldState, _state);
        }
    }
}