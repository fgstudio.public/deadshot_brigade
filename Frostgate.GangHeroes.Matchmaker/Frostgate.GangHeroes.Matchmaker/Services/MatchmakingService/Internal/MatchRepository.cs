﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking.Internal
{
    public sealed class MatchRepository : IMatchRepository
    {
        public int Count => _matches.Count;
        public bool IsReadOnly => _matches.IsReadOnly;

        public IMatchAsyncHandle this[Guid key]
        {
            get => _matches[key];
            set => _matches[key] = value;
        }

        public ICollection<Guid> Keys => _matches.Keys;
        public ICollection<IMatchAsyncHandle> Values => _matches.Values;

        private IDictionary<Guid, IMatchAsyncHandle> _matches = new Dictionary<Guid, IMatchAsyncHandle>();

        public IEnumerator<KeyValuePair<Guid, IMatchAsyncHandle>> GetEnumerator() => _matches.GetEnumerator();

        public void Add(KeyValuePair<Guid, IMatchAsyncHandle> item) => _matches.Add(item);
        public void Clear() => _matches.Clear();
        public bool Contains(KeyValuePair<Guid, IMatchAsyncHandle> item) => _matches.Contains(item);

        public void CopyTo(KeyValuePair<Guid, IMatchAsyncHandle>[] array, int arrayIndex) =>
            _matches.CopyTo(array, arrayIndex);

        public bool Remove(KeyValuePair<Guid, IMatchAsyncHandle> item) => _matches.Remove(item);
        public void Add(Guid key, IMatchAsyncHandle value) => _matches.Add(key, value);
        public bool ContainsKey(Guid key) => _matches.ContainsKey(key);
        public bool Remove(Guid key) => _matches.Remove(key);
        public bool TryGetValue(Guid key, out IMatchAsyncHandle value) => _matches.TryGetValue(key, out value);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
