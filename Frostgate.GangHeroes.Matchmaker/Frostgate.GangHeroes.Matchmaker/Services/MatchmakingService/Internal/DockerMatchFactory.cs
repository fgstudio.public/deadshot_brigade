﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Frostgate.RiftHunters.Matchmaker.Services.Time;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking.Internal
{
    public sealed class DockerMatchFactory : IMatchFactory
    {
        private readonly IDockerService _dockerService;
        private readonly IDockerMatchFactoryConfiguration _configuration;
        private readonly ITimeService _timeService;

        private readonly PortPool _portPool;

        public DockerMatchFactory(IDockerService dockerService, IDockerMatchFactoryConfiguration configuration,
            ITimeService timeService)
        {
            _dockerService = dockerService;
            _configuration = configuration;
            _timeService = timeService;
            _portPool = new PortPool(_configuration.ExternalMinPort, _configuration.ExternalMaxPort);
        }

        public IMatchAsyncHandle CreateMatch(Guid matchId, IMatchConfig config, int playersRank) =>
            new DockerMatchAsyncHandle(matchId, config, playersRank, SpawnMatchAsync, UnSpawnMatchAsync);

        private async Task<MatchOnContainer> SpawnMatchAsync(Guid matchId, IMatchConfig config, int playersRank,
            CancellationToken token = new CancellationToken())
        {
            int hostPort = _portPool.GetPort();

            string containerId = await _dockerService.RunContainerAsync(
                matchId,
                CreateEnvArguments(matchId, config),
                hostPort,
                _configuration.InternalPort,
                _configuration.TransportScheme, _configuration.ImageName, token);

            var matchUri = new Uri($"{_configuration.TransportScheme}://{_configuration.ExternalIpAddr}:{hostPort}");

            return new MatchOnContainer(matchId,
                matchUri,
                MatchState.Registered,
                containerId,
                _timeService.Now,
                config,
                playersRank);
        }

        private IReadOnlyDictionary<string, string> CreateEnvArguments(Guid id, IMatchConfig config)
        {
            var args = new Dictionary<string, string>
            {
                ["MatchId"] = id.ToString(),
                ["MissionId"] = config.MissionId,
            };

            return args;
        }

        private async Task<bool> UnSpawnMatchAsync(MatchOnContainer match,
            CancellationToken token = new CancellationToken())
        {
            bool stopResult = await _dockerService.StopContainerAsync(match.ContainerId, token);
            if (stopResult)
                _portPool.ReleasePort(match.Uri.Port);

            return stopResult;
        }
    }
}