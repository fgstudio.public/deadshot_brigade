﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking.Internal
{
    public sealed class DockerMatchAsyncHandle : IMatchAsyncHandle
    {
        private readonly Guid _matchId;
        private readonly IMatchConfig _matchConfig;
        private readonly int _playersRank;
        private readonly Func<Guid, IMatchConfig, int, CancellationToken, Task<MatchOnContainer>> _createMatchDelegate;
        private readonly Func<MatchOnContainer, CancellationToken, Task<bool>> _destroyMatchDelegate;

        private Task<MatchOnContainer>? _creationTask;

        public DockerMatchAsyncHandle(Guid matchId, IMatchConfig matchConfig, int playersRank,
            Func<Guid, IMatchConfig, int, CancellationToken, Task<MatchOnContainer>> createMatchDelegate,
            Func<MatchOnContainer, CancellationToken, Task<bool>> destroyMatchDelegate)
        {
            _matchId = matchId;
            _matchConfig = matchConfig;
            _playersRank = playersRank;
            _createMatchDelegate = createMatchDelegate;
            _destroyMatchDelegate = destroyMatchDelegate;
        }

        public async Task<Match> GetMatchAsync(CancellationToken token = new CancellationToken()) =>
            await (_creationTask ??= _createMatchDelegate.Invoke(_matchId, _matchConfig, _playersRank, token));

        public async Task<bool> DestroyMatchAsync(CancellationToken token = new CancellationToken())
        {
            MatchOnContainer match =
                await (_creationTask ?? throw new InvalidOperationException("Match container not created."));

            return await _destroyMatchDelegate.Invoke(match, token);
        }
    }
}
