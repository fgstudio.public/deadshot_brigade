﻿namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking.Internal
{
    public interface IDockerMatchFactoryConfiguration
    {
        string ImageName { get; }
        string TransportScheme { get; }
        int InternalPort { get; }
        string ExternalIpAddr { get; }
        int ExternalMinPort { get; }
        int ExternalMaxPort { get; }
    }
}
