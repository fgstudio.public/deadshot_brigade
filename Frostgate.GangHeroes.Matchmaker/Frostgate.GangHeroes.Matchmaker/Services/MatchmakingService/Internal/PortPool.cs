﻿using System;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking.Internal
{
    public sealed class PortPool
    {
        private readonly int _minPort;
        private readonly int _maxPort;

        private readonly HashSet<int> _ports;

        public PortPool(int minPort, int maxPort)
        {
            _minPort = minPort;
            _maxPort = maxPort;

            _ports = new HashSet<int>(_maxPort - _minPort);
        }

        public int GetPort()
        {
            for (int port = _minPort; port <= _maxPort; port++)
            {
                if (IsBusy(port)) continue;
                _ports.Add(port);

                return port;
            }

            throw new InvalidOperationException($"All ports in range [{_minPort}-{_maxPort}] are busy.");
        }

        public void ReleasePort(int port)
        {
            if (IsBusy(port))
                _ports.Remove(port);
        }

        private bool IsBusy(int port) => _ports.Contains(port);
    }
}
