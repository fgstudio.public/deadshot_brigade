﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Frostgate.RiftHunters.Matchmaker.Services.Time;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking.Internal
{
    public sealed class StubSearchLogic : IMatchSearchLogic
    {
        private readonly ITimeService _timeService;

        public StubSearchLogic(ITimeService timeService)
        {
            _timeService = timeService;
        }

        public async Task<Match?> FindAsync(IMatchRepository matchRepository, IMatchConfig matchConfig, int playerRank)
        {
            foreach (IMatchAsyncHandle handle in matchRepository.Values)
            {
                Match match = await handle.GetMatchAsync(CancellationToken.None);
                await match.OnReady;

                if (IsMatchValid(match, matchConfig, playerRank))
                    return match;
            }

            return null;
        }

        private bool IsMatchValid(Match match, IMatchConfig config, int playerRank)
        {
            return IsMatchReady(match) &&
                   IsMatchNotClosed(match) &&
                   IsMatchReady(match) &&
                   IsMatchHasFreePlace(match) &&
                   match.PlayersRank == playerRank &&
                   match.Config.MissionId == config.MissionId;
        }

        private bool IsMatchReady(Match match) => match.State == MatchState.Ready;
        private bool IsMatchHasFreePlace(Match match) => match.PlayersCount < match.Config.MaxPlayersCount;

        private bool IsMatchNotClosed(Match match) => (_timeService.Now - match.CreationTime) <
                                                      TimeSpan.FromSeconds(Constants.OpenMatchDurationSec);
    }
}