﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking
{
    public sealed class MatchmakingService
    {
        private readonly IMatchRepository _matchRepository;
        private readonly IMatchSearchLogic _searchLogic;
        private readonly IMatchFactory _matchFactory;

        public MatchmakingService(IMatchRepository matchRepository, IMatchSearchLogic searchLogic,
            IMatchFactory matchFactory)
        {
            _matchRepository = matchRepository;
            _searchLogic = searchLogic;
            _matchFactory = matchFactory;
        }

        public async Task<IMatch> FindMatchAsync(IMatchConfig config, int playerRank)
        {
            Match match = await _searchLogic.FindAsync(_matchRepository, config, playerRank) ??
                          await CreateMatchAsync(Guid.NewGuid(), config, playerRank, CancellationToken.None);

            return match;
        }

        public async Task<IMatch?> GetMatchIfFreeByIdAsync(Guid id)
        {
            if (!_matchRepository.TryGetValue(id, out IMatchAsyncHandle? matchHandle))
                return null;

            Match match = await matchHandle.GetMatchAsync();

            return match.PlayersCount == match.Config.MaxPlayersCount ? null : match;
        }

        public async Task<bool> RegisterPlayerAsync(Guid matchId)
        {
            if (!_matchRepository.TryGetValue(matchId, out IMatchAsyncHandle handle))
                return false;

            Match match = await (await handle.GetMatchAsync(CancellationToken.None)).OnReady;
            match.PlayersCount++;

            return true;
        }


        public async Task<bool> UnregisterPlayerAsync(Guid matchId)
        {
            if (!_matchRepository.TryGetValue(matchId, out IMatchAsyncHandle handle))
                return false;

            Match match = await (await handle.GetMatchAsync(CancellationToken.None)).OnReady;
            match.PlayersCount--;

            return true;
        }

        public async Task<bool> UpdateMatchStateAsync(Guid matchId, MatchState state,
            CancellationToken token = new CancellationToken())
        {
            if (!_matchRepository.TryGetValue(matchId, out IMatchAsyncHandle matchHandle))
                return false;

            Match match = await matchHandle.GetMatchAsync(token);
            match.State = state;

            if (match.State == MatchState.Completed)
            {
                _matchRepository.Remove(match.Id);
                await matchHandle.DestroyMatchAsync(token);
            }

            return true;
        }

        private async Task<Match> CreateMatchAsync(Guid matchId, IMatchConfig config, int playerRank,
            CancellationToken token = new CancellationToken())
        {
            if (!_matchRepository.TryGetValue(matchId, out IMatchAsyncHandle matchHandle))
            {
                matchHandle = _matchFactory.CreateMatch(matchId, config, playerRank);
                _matchRepository[matchId] = matchHandle;
            }

            return await (await matchHandle.GetMatchAsync(token)).OnReady;
        }
    }
}