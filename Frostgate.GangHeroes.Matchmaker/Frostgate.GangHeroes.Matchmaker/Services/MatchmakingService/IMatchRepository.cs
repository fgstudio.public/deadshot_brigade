﻿using System;
using System.Collections.Generic;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking
{
    public interface IMatchRepository : IDictionary<Guid, IMatchAsyncHandle>
    {
    }
}
