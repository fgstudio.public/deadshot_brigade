﻿using System;
using System.Threading.Tasks;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking
{
    public abstract class Match : IMatch
    {
        public delegate void ValueChanged(MatchState oldValue, MatchState newValue);

        event ValueChanged StateChanged;

        public abstract DateTime CreationTime { get; }
        public abstract int PlayersCount { get; set; }
        public abstract Task<Match> OnReady { get; }
        public abstract Guid Id { get; }
        public abstract Uri Uri { get; }
        public abstract int PlayersRank { get; }
        public abstract MatchState State { get; set; }
        public abstract IMatchConfig Config { get; }
    }
}