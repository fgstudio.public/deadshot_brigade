﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking
{
    public interface IMatchFactory
    {
        IMatchAsyncHandle CreateMatch(Guid matchId, IMatchConfig config, int playersRank);
    }
}
