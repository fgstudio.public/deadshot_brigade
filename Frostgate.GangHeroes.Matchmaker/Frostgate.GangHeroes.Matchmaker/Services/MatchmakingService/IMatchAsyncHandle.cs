﻿using System.Threading;
using System.Threading.Tasks;

namespace Frostgate.RiftHunters.Matchmaker.Services.Matchmaking
{
    public interface IMatchAsyncHandle
    {
        Task<Match> GetMatchAsync(CancellationToken token = new CancellationToken());
        Task<bool> DestroyMatchAsync(CancellationToken token = new CancellationToken());
    }
}
