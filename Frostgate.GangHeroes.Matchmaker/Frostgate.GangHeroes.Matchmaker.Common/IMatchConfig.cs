﻿namespace Frostgate.RiftHunters.Matchmaker
{
    public interface IMatchConfig
    {
        string MissionId { get; }
        int MaxPlayersCount { get; }
    }
}