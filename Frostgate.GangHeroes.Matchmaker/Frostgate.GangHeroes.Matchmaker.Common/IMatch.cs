﻿using System;

namespace Frostgate.RiftHunters.Matchmaker
{
    public interface IMatch
    {
        Guid Id { get; }
        Uri Uri { get; }
        
        IMatchConfig Config { get; }
        int PlayersRank { get; }

        MatchState State { get; }
    }
}