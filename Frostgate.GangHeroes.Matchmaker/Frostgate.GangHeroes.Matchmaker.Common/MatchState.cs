﻿namespace Frostgate.RiftHunters.Matchmaker
{
    public enum MatchState
    {
        Undefined,
        Registered,
        Ready,
        Completing,
        Completed
    }
}
