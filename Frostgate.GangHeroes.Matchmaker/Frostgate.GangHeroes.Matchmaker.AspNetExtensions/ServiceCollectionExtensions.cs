﻿using Frostgate.RiftHunters.Matchmaker.Adapters;
using Frostgate.RiftHunters.Matchmaker.Services.Docker;
using Frostgate.RiftHunters.Matchmaker.Services.Matchmaking;
using Frostgate.RiftHunters.Matchmaker.Services.Matchmaking.Internal;
using Frostgate.RiftHunters.Matchmaker.Services.Time;
using Microsoft.Extensions.DependencyInjection;

namespace Frostgate.RiftHunters.Matchmaker
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMatchmaking(this IServiceCollection services,
            IDockerServiceConfiguration dockerConfiguration,
            IDockerMatchFactoryConfiguration matchFactoryConfiguration) =>
            services.AddDockerService(dockerConfiguration).AddMatchmakerService(matchFactoryConfiguration)
                .BindServicesWithAdapters();

        private static IServiceCollection AddDockerService(this IServiceCollection services,
            IDockerServiceConfiguration dockerServiceConfiguration)
        {
            services.AddSingleton(dockerServiceConfiguration);

            services.AddSingleton<DockerService>();

            return services;
        }

        private static IServiceCollection AddMatchmakerService(this IServiceCollection services,
            IDockerMatchFactoryConfiguration matchFactoryConfiguration)
        {
            services.AddSingleton(matchFactoryConfiguration);

            services.AddSingleton<MatchmakingService>();
            services.AddSingleton<IMatchSearchLogic, StubSearchLogic>();
            services.AddSingleton<IMatchRepository, MatchRepository>();
            services.AddSingleton<IMatchFactory, DockerMatchFactory>();
            services.AddSingleton<ITimeService, TimeService>();

            return services;
        }

        private static IServiceCollection BindServicesWithAdapters(this IServiceCollection services)
        {
            services.AddSingleton<IDockerService, DockerServiceMatchmakingAdapter>();

            return services;
        }
    }
}