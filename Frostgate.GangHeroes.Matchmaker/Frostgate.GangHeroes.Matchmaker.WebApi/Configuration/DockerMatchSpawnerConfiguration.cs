﻿using Frostgate.RiftHunters.Matchmaker.Services.Matchmaking.Internal;

namespace Frostgate.RiftHunters.Matchmaker.WebApi.Configuration
{
    public sealed class DockerMatchFactoryConfiguration : IDockerMatchFactoryConfiguration
    {
        public string ImageName { get; set; }
        public string TransportScheme { get; set; }
        public int InternalPort { get; set; }
        public string ExternalIpAddr { get; set; }
        public int ExternalMinPort { get; set; }
        public int ExternalMaxPort { get; set; }
    }
}
