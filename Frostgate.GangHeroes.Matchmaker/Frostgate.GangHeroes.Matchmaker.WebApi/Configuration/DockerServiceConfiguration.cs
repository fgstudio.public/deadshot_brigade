﻿using Frostgate.RiftHunters.Matchmaker.Services.Docker;

namespace Frostgate.RiftHunters.Matchmaker.WebApi.Configuration
{
    public sealed class DockerServiceConfiguration : IDockerServiceConfiguration
    {
        public double ConnectionTimeoutSeconds { get; set; }
    }
}
