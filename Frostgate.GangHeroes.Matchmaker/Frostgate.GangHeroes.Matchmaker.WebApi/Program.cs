using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Frostgate.RiftHunters.Matchmaker.WebApi
{
    public static class Program
    {
        private const string Environment = nameof(Environment);

        public static Task Main(string[] args) => CreateHostBuilder(args).Build().RunAsync();

        private static IHostBuilder CreateHostBuilder(string[] args)
        {
            IHostBuilder builder = Host.CreateDefaultBuilder(args);
            builder.ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });

            ConfigureApp(builder);

            return builder;
        }

        private static IHostBuilder ConfigureApp(IHostBuilder builder)
        {
            string? environment = System.Environment.GetEnvironmentVariable(Environment);
            if (string.IsNullOrWhiteSpace(environment))
                throw new InvalidOperationException($"Startup environment variable '{Environment}' not set.");

            builder.ConfigureAppConfiguration(confBuilder =>
                confBuilder.AddJsonFile($"appsettings.{environment}.json", optional: false));

            return builder;
        }
    }
}