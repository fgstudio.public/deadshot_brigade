﻿using System.Net.Mime;
using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Frostgate.RiftHunters.Matchmaker.Messaging;

namespace Frostgate.RiftHunters.Matchmaker.WebApi.Endpoints
{
    public class ExampleEndpoint : EndpointBaseSync.WithRequest<ExampleRequest>.WithResult<ExampleResponse>
    {
        [Route(ExampleRequest.Route)]
        [HttpGet]
        [Produces(typeof(ExampleResponse))]
        [Consumes(typeof(ExampleRequest), MediaTypeNames.Application.Json)]
        public override ExampleResponse Handle([FromQuery] ExampleRequest request)
        {
            return new ExampleResponse(request.EchoString);
        }
    }
}
