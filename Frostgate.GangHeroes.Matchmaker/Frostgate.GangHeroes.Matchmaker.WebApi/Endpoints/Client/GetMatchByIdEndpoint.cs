﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using Frostgate.RiftHunters.Matchmaker.Messaging;
using Frostgate.RiftHunters.Matchmaker.Messaging.Client;
using Frostgate.RiftHunters.Matchmaker.Services.Matchmaking;
using Microsoft.AspNetCore.Mvc;

namespace Frostgate.RiftHunters.Matchmaker.WebApi.Endpoints.Client;

public sealed class GetMatchByIdEndpoint : EndpointBaseAsync.WithRequest<Guid>.WithResult<GetMatchResponse>
{
    private readonly MatchmakingService _matchmaking;

    public GetMatchByIdEndpoint(MatchmakingService matchmaking)
    {
        _matchmaking = matchmaking;
    }

    [HttpGet(Routes.Client.Name + "/match" + "/{matchId}")]
    public override async Task<GetMatchResponse> HandleAsync(Guid matchId, CancellationToken cancellationToken = new())
    {
        IMatch? match = await _matchmaking.GetMatchIfFreeByIdAsync(matchId);
        MatchDto? dto = match == null ? null : MatchDto.From(match);

        return new GetMatchResponse(dto, Guid.NewGuid());
    }
}