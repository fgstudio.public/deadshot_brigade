﻿using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Frostgate.RiftHunters.Matchmaker.Messaging;
using Frostgate.RiftHunters.Matchmaker.Messaging.Client;
using Frostgate.RiftHunters.Matchmaker.Services.Matchmaking;

namespace Frostgate.RiftHunters.Matchmaker.WebApi.Endpoints.Client
{
    public sealed class FindMatchEndpoint : EndpointBaseAsync.WithRequest<FindMatchRequest>
        .WithActionResult<GetMatchResponse>
    {
        private readonly MatchmakingService _matchmaker;

        public FindMatchEndpoint(MatchmakingService matchmaker)
        {
            _matchmaker = matchmaker;
        }

        [Route(FindMatchRequest.Route)]
        [HttpPost]
        [Produces(typeof(GetMatchResponse))]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public override async Task<ActionResult<GetMatchResponse>> HandleAsync(FindMatchRequest request,
            CancellationToken cancellationToken = new CancellationToken())
        {
            IMatch match = await _matchmaker.FindMatchAsync(request.MatchConfig, request.PlayerRank);

            return Ok(new GetMatchResponse(MatchDto.From(match), request.GetCorrelationId()));
        }
    }
}