﻿using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using Frostgate.RiftHunters.Matchmaker.Messaging.Server;
using Frostgate.RiftHunters.Matchmaker.Services.Matchmaking;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Frostgate.RiftHunters.Matchmaker.WebApi.Endpoints.Server
{
    public sealed class
        UpdateMatchStateEndpoint : EndpointBaseAsync.WithRequest<UpdateMatchStateRequest>.WithActionResult
    {
        private readonly MatchmakingService _matchmaker;

        public UpdateMatchStateEndpoint(MatchmakingService matchmaker)
        {
            _matchmaker = matchmaker;
        }

        /// <summary>
        /// Обновляет состояние матча в хранилище.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [Route(UpdateMatchStateRequest.Route)]
        [HttpPost]
        [Consumes(typeof(UpdateMatchStateRequest), MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public override async Task<ActionResult> HandleAsync([FromBody] UpdateMatchStateRequest request,
            CancellationToken cancellationToken = new CancellationToken())
        {
            if (await _matchmaker.UpdateMatchStateAsync(request.MatchId, request.State, CancellationToken.None))
                return Ok();

            return BadRequest();
        }
    }
}