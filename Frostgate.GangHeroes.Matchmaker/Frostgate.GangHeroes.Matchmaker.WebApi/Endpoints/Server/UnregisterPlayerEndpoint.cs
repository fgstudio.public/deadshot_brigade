﻿using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using Frostgate.RiftHunters.Matchmaker.Messaging.Server;
using Frostgate.RiftHunters.Matchmaker.Services.Matchmaking;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Frostgate.RiftHunters.Matchmaker.WebApi.Endpoints.Server
{
    public sealed class
        UnregisterPlayerEndpoint : EndpointBaseAsync.WithRequest<UnregisterPlayerRequest>.WithActionResult
    {
        private readonly MatchmakingService _matchmaker;

        public UnregisterPlayerEndpoint(MatchmakingService matchmaker)
        {
            _matchmaker = matchmaker;
        }

        /// <summary>
        /// Отменяет регистрацию игрока в матче.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route(UnregisterPlayerRequest.Route)]
        [HttpPost]
        [Consumes(typeof(UnregisterPlayerRequest), MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public override async Task<ActionResult> HandleAsync(UnregisterPlayerRequest request,
            CancellationToken token = new CancellationToken())
        {
            bool result = await _matchmaker.UnregisterPlayerAsync(request.MatchId);

            return result ? Ok() : BadRequest();
        }
    }
}