﻿using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;
using Ardalis.ApiEndpoints;
using Frostgate.RiftHunters.Matchmaker.Messaging.Server;
using Frostgate.RiftHunters.Matchmaker.Services.Matchmaking;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Frostgate.RiftHunters.Matchmaker.WebApi.Endpoints.Server
{
    public sealed class RegisterPlayerEndpoint : EndpointBaseAsync.WithRequest<RegisterPlayerRequest>.WithActionResult
    {
        private readonly MatchmakingService _matchmaker;

        public RegisterPlayerEndpoint(MatchmakingService matchmaker)
        {
            _matchmaker = matchmaker;
        }

        /// <summary>
        /// Регистрирует игрока в матче.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Route(RegisterPlayerRequest.Route)]
        [HttpPost]
        [Consumes(typeof(RegisterPlayerRequest), MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public override async Task<ActionResult> HandleAsync(RegisterPlayerRequest request,
            CancellationToken token = new CancellationToken())
        {
            bool result = await _matchmaker.RegisterPlayerAsync(request.MatchId);
            
            return result ? Ok() : BadRequest();
        }
    }
}