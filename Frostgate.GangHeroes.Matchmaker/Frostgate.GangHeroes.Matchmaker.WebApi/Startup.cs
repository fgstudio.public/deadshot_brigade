﻿using System;
using System.IO;
using Frostgate.RiftHunters.Matchmaker.WebApi.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Frostgate.RiftHunters.Matchmaker.WebApi
{
    public sealed class Startup
    {
        private const string XmlDocsFileName = "Frostgate.GangHeroes.Matchmaker.WebApi.xml";

        private readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuration;

        public Startup(IWebHostEnvironment environment, IConfiguration configuration)
        {
            _environment = environment;
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddHttpLogging(options => options.LoggingFields = HttpLoggingFields.All);

            services.AddMatchmaking(GetConfiguration<DockerServiceConfiguration>(),
                GetConfiguration<DockerMatchFactoryConfiguration>());

            ConfigureSwaggerService(services);
        }

        public void Configure(IApplicationBuilder builder)
        {
            ConfigureSwagger(builder);

            builder.UseHttpLogging();
            builder.UseRouting();
            builder.UseEndpoints(routeBuilder => routeBuilder.MapControllers());
        }

        private void ConfigureSwaggerService(IServiceCollection services)
        {
            services.AddSwaggerGen(setup =>
            {
                string filePath = Path.Combine(AppContext.BaseDirectory, XmlDocsFileName);
                setup.IncludeXmlComments(filePath);

                setup.UseApiEndpoints();
            });
        }

        private void ConfigureSwagger(IApplicationBuilder builder)
        {
            if (!_environment.IsDevelopment())
                return;

            builder.UseSwagger();
            builder.UseSwaggerUI();
        }

        private T GetConfiguration<T>()
        {
            string typeName = typeof(T).Name;

            return _configuration.GetSection(typeName).Get<T>();
        }
    }
}